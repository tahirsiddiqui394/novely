using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;
using Spine;


public class FirstScene : MonoBehaviour {

	private float timer = 0;

	private float timerMax = 0;

	public string textShownOnScreen;
	public Text TextBackground;
	public string scene1Text = "Keep your cash in your wallet,\nAbby!";
	public float wordsPerSecond = 30; // speed of typewriter
	private float timeElapsed = 0; 
	Transform BoardImage;
	Transform BoardText;
	bool isShowNextScene = false;
	int textBoxCount = 0;
	Transform OptionA;
	Transform OptionB;
	static public string sceneNumber = "Scene1.2";
	// Use this for initialization
	void Start () {

		//Debug.Log ("Scene1.7");
		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Debug.Log (canvasObject);
		BoardImage = canvasObject.transform.Find("Image");
		BoardText = BoardImage.transform.Find ("Text");
		TextBackground = BoardText .GetComponent<Text>();
		BoardImage.transform.localScale = new Vector3(0, 0, 0);
		Debug.Log ("222222222222");

		if (sceneNumber == "Scene1.6") {
			BoardImage.transform.localScale = new Vector3(0, 0, 0);

		}

		if (sceneNumber == "Scene1.8") {
			OptionA = canvasObject.transform.Find ("OptionA");
			OptionB = canvasObject.transform.Find ("OptionB");
			OptionA.transform.localScale = new Vector3(0, 0, 0);
			OptionB.transform.localScale = new Vector3(0, 0, 0);

		}
		if (sceneNumber == "Scene1.10") {
			BoardImage.transform.localScale = new Vector3(0, 0, 0);

		}
		if (sceneNumber == "Scene1.13") {
			BoardImage.transform.localScale = new Vector3(0, 0, 0);

		}
		if (sceneNumber == "Scene1.14") {
			BoardImage.transform.localScale = new Vector3(0, 0, 0);

		}
		if (sceneNumber == "Scene1.15") {
			BoardImage.transform.localScale = new Vector3(0, 0, 0);

		}
		if (sceneNumber == "Scene1.16") {
			BoardImage.transform.localScale = new Vector3(0, 0, 0);

		}
		sceneNumber = "Scene1.2";
		playScene ();

	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log("5 seconds is lost forever");
		//Debug.Log(sceneNumber);
		if (sceneNumber == "Scene1.2") {
		
			if (!Waited (1))
				return;
			BoardImage.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
			//BoardImage.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			timeElapsed += Time.deltaTime;
			textShownOnScreen = GetWords (scene1Text, timeElapsed * wordsPerSecond);
			//Debug.Log(" seconds is lost forever");
		
		} else if (sceneNumber == "Scene1.3") {
		
			timeElapsed += Time.deltaTime;
		
		} else if (sceneNumber == "Scene1.4") {
			if (!Waited (1))
				return;
			BoardImage.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
			//BoardImage.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			timeElapsed += Time.deltaTime;
			textShownOnScreen = GetWords ("Stay close! It’ll get worse when\nthe fighting starts!", timeElapsed * wordsPerSecond);
			//Debug.Log(" seconds is lost forever");

		}
//		"Welcome to the blood bath! If you are looking for Economics 101, you are in the wrong place, my friend! " +
//			"If it’s the Circle you seek, this is Mecca. My name is Adam. I make the rules, and I call the fight. " +
//			"No touching the fighters, no bet switching, and no encroachment of the ring."

		else if (sceneNumber == "Scene1.5") {
			if (!Waited (2))
				return;
			BoardImage.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
			timeElapsed += Time.deltaTime;
			if (timeElapsed < 5.0 ) {
				textShownOnScreen = GetWords ("Welcome to the blood bath! If you are looking for Economics 101,", timeElapsed * wordsPerSecond);
			} else if (timeElapsed > 5.0 && timeElapsed < 5.2) {
				textShownOnScreen = "";
				TextBackground.text = textShownOnScreen;
				//textShownOnScreen = GetWords("my friend!",  timeElapsed * wordsPerSecond);
			
			} else if (timeElapsed >= 5.2 && timeElapsed < 10.0) {

				float tt = timeElapsed - (float)5.2;
			
				textShownOnScreen = GetWords ("you are in the wrong place, my friend!", tt * wordsPerSecond);
			} else if (timeElapsed > 10.0 && timeElapsed < 10.2) {
				textShownOnScreen = "";
				TextBackground.text = textShownOnScreen;
				//textShownOnScreen = GetWords("my friend!",  timeElapsed * wordsPerSecond);

			} else if (timeElapsed >= 10.2 && timeElapsed < 15.0) {

				float tt = timeElapsed - (float)10.2;

				textShownOnScreen = GetWords ("If it’s the Circle you seek, this is Mecca. My name is Adam.", tt * wordsPerSecond);
			} else if (timeElapsed > 15.0 && timeElapsed < 15.2) {
				textShownOnScreen = "";
				TextBackground.text = textShownOnScreen;
				//textShownOnScreen = GetWords("my friend!",  timeElapsed * wordsPerSecond);

			} else if (timeElapsed >= 15.2 && timeElapsed < 20.0) {

				float tt = timeElapsed - (float)15.2;

				textShownOnScreen = GetWords ("I make the rules, and I call the fight.", tt * wordsPerSecond);
			} else if (timeElapsed > 20.0 && timeElapsed < 20.2) {
				textShownOnScreen = "";
				TextBackground.text = textShownOnScreen;
				//textShownOnScreen = GetWords("my friend!",  timeElapsed * wordsPerSecond);

			} else if (timeElapsed >= 20.2 && timeElapsed < 25.2) {

				float tt = timeElapsed - (float)20.2;

				textShownOnScreen = GetWords ("No touching the fighters, no bet switching,", tt * wordsPerSecond);
			} else if (timeElapsed > 25.0 && timeElapsed < 25.2) {
				textShownOnScreen = "";
				TextBackground.text = textShownOnScreen;
				//textShownOnScreen = GetWords("my friend!",  timeElapsed * wordsPerSecond);

			} else if (timeElapsed >= 25.2) {

				float tt = timeElapsed - (float)25.2;

				textShownOnScreen = GetWords ("and no encroachment of the ring.", tt * wordsPerSecond);
			}

			Debug.Log (" scene 5 text");
		} else if (sceneNumber == "Scene1.6") {
			timeElapsed += Time.deltaTime;
		} else if (sceneNumber == "Scene1.7") {

			if (!Waited (1))
				return;
			BoardImage.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
			//BoardImage.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			timeElapsed += Time.deltaTime;
			if (timeElapsed < 5.0) {
				textShownOnScreen = GetWords ("She shouldn’t be up there. ", timeElapsed * wordsPerSecond);
			} else if (timeElapsed > 5.0 && timeElapsed < 5.2) {
				textShownOnScreen = "";
				TextBackground.text = textShownOnScreen;

			} else if (timeElapsed >= 5.2 && timeElapsed < 10) {
				float tt = timeElapsed - (float)5.2;
				textShownOnScreen = GetWords ("She could get shoved, or knocked down, or worse.", tt * wordsPerSecond);

			} else if (timeElapsed > 10.0 && timeElapsed < 13.0) {
				textShownOnScreen = "";
				TextBackground.text = textShownOnScreen;
				Debug.Log ("POSTION *******");
				Debug.Log (BoardImage.transform.position);
				BoardImage.transform.position = new Vector3 (BoardImage.transform.position.x + 1, BoardImage.transform.position.y - 1, 0.0f);
				BoardImage.transform.localScale = new Vector3 (0.0f, 0.0f, 0.0f);

			} else if (timeElapsed >= 13) {
				float tt = timeElapsed - (float)13;
				BoardImage.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
				//BoardImage.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
				//BoardImage.transform.position = new Vector3 (BoardImage.transform.position.x + 100, BoardImage.transform.position.y - 100, 1.0f);
				textShownOnScreen = GetWords ("Trust me, Abby can take care of herself.", tt * wordsPerSecond);

			}


		}

		else if (sceneNumber == "Scene1.8") {

			if (!Waited (2))
				return;
			BoardImage.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
//			BoardImage.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			timeElapsed += Time.deltaTime;
			textShownOnScreen = GetWords ("Hmmm...should I... ", timeElapsed * wordsPerSecond);
			if (timeElapsed >= 4) {
				OptionA.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
				OptionB.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
			}

		}

		else if (sceneNumber == "Scene1.9") {

			if (!Waited (1))
				return;
			//BoardImage.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			BoardImage.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
			timeElapsed += Time.deltaTime;
			if (timeElapsed < 5.0) {
				textShownOnScreen = GetWords ("Tonight, we have a new challenger!\nMarek Young!", timeElapsed * wordsPerSecond);
			} else if (timeElapsed > 5.0 && timeElapsed < 5.2) {
				textShownOnScreen = "";
				TextBackground.text = textShownOnScreen;
				BoardImage.transform.localScale = new Vector3 (0.0f, 0.0f, 0.0f);
				//textShownOnScreen = GetWords("my friend!",  timeElapsed * wordsPerSecond);

			} else if (timeElapsed >= 5.2 && timeElapsed < 10.0) {

				float tt = timeElapsed - (float)5.2;
				//BoardImage.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
				BoardImage.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
				textShownOnScreen = GetWords ("Our next fighter doesn’t need an introduction.", tt * wordsPerSecond);
			} else if (timeElapsed > 10.0 && timeElapsed < 12.2) {
				textShownOnScreen = "";
				TextBackground.text = textShownOnScreen;
				BoardImage.transform.localScale = new Vector3 (0.0f, 0.0f, 0.0f);

			}
			else if (timeElapsed >= 12.2 && timeElapsed < 16.0) {

				float tt = timeElapsed - (float)12.2;
//				BoardImage.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
				BoardImage.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
				textShownOnScreen = GetWords ("Shake in your boots,boys, and drop your panties,ladies!", tt * wordsPerSecond);
			}
			else if (timeElapsed > 16.0 && timeElapsed < 18.2) {
				textShownOnScreen = "";
				BoardImage.transform.localScale = new Vector3 (0.0f, 0.0f, 0.0f);
				TextBackground.text = textShownOnScreen;

			}
			else if (timeElapsed >= 18.2 /*&& timeElapsed < 15.0*/) {

				float tt = timeElapsed - (float)18.2;
//				BoardImage.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
				BoardImage.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
				textShownOnScreen = GetWords ("I give you, Travis 'Mad Dog' Maddox!", tt * wordsPerSecond);
			}
		}

		else if (sceneNumber == "Scene1.10")
		{
			timeElapsed += Time.deltaTime;

			//if (!Waited(2))
			//	return;
			//BoardImage.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
			//timeElapsed += Time.deltaTime;
			//textShownOnScreen = GetWords("Hmmm...should I... ", timeElapsed * wordsPerSecond);
			//if (timeElapsed >= 4)
			//{
			//	OptionA.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
			//	OptionB.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
			//}

		}
		else if (sceneNumber == "Scene1.12") {
			if (!Waited (5))
				return;
			BoardImage.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
			//BoardImage.transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
			timeElapsed += Time.deltaTime;
			textShownOnScreen = GetWords ("That’s your one.", timeElapsed * wordsPerSecond);
			//Debug.Log(" seconds is lost forever");

		}
		else if (sceneNumber == "Scene1.13")
		{
			timeElapsed += Time.deltaTime;


		}
		else if (sceneNumber == "Scene1.14")
		{
			timeElapsed += Time.deltaTime;


		}
		else if (sceneNumber == "Scene1.15")
		{
			timeElapsed += Time.deltaTime;


		}
		else if (sceneNumber == "Scene1.16")
		{
			timeElapsed += Time.deltaTime;


		}
	}



	public void playScene() {

		if (sceneNumber == "Scene1.2") {
		
			var test = GameObject.FindGameObjectWithTag ("Abby");
			Debug.Log ("TEST OBJECT");
			SkeletonAnimation playerAnim;
			playerAnim = (SkeletonAnimation)test.GetComponent ("SkeletonAnimation");
			//playerAnim.state.SetEmptyAnimation (0, 0);
			TrackEntry trackEntry = playerAnim.state.SetAnimation(0, "1.2", false);
			trackEntry.Complete += HandleDanceComplete;
	
		}

		else if (sceneNumber == "Scene1.4") {

			var test = GameObject.FindGameObjectWithTag ("Shaeeply");
			Debug.Log ("TEST OBJECT");
			SkeletonAnimation playerAnim;
			playerAnim = (SkeletonAnimation)test.GetComponent ("SkeletonAnimation");
			//playerAnim.state.SetEmptyAnimation (0, 0);
			TrackEntry trackEntry = playerAnim.state.SetAnimation(0, "1.4", false);
			trackEntry.Complete += HandleDanceComplete;

		}

		else if (sceneNumber == "Scene1.5") {

			var test = GameObject.FindGameObjectWithTag ("Adam");
			Debug.Log ("TEST OBJECT");
			SkeletonAnimation playerAnim;
			playerAnim = (SkeletonAnimation)test.GetComponent ("SkeletonAnimation");
			//playerAnim.state.SetEmptyAnimation (0, 0);
			//textBoxCount = 1;
			TrackEntry trackEntry = playerAnim.state.SetAnimation(0, "animation", true);
			trackEntry.Complete += HandleDanceComplete;

		}

		else if (sceneNumber == "Scene1.6") {

			var test = GameObject.FindGameObjectWithTag ("Abby");
			Debug.Log ("TEST OBJECT");
			SkeletonAnimation playerAnim;
			playerAnim = (SkeletonAnimation)test.GetComponent ("SkeletonAnimation");

			//playerAnim.state.SetEmptyAnimation (0, 0);
			//textBoxCount = 1;
			TrackEntry trackEntry = playerAnim.state.SetAnimation(0, "1.6", true);
			trackEntry.Complete += HandleDanceComplete;

		}

		else if (sceneNumber == "Scene1.7") {

			var shaeeply = GameObject.FindGameObjectWithTag ("Shaeeply");
			var america = GameObject.FindGameObjectWithTag ("America");
			Debug.Log ("TEST OBJECT");
			SkeletonAnimation shaeeplyAnim;
			SkeletonAnimation americaAnim;
			shaeeplyAnim = (SkeletonAnimation)shaeeply.GetComponent ("SkeletonAnimation");
			americaAnim = (SkeletonAnimation)america.GetComponent ("SkeletonAnimation");
			//shaeeplyAnim.state.SetEmptyAnimation (0, 0);
			americaAnim.state.SetAnimation(0, "animation", true);
			americaAnim.state.AddAnimation (0, "1.7", true, 10);

			americaAnim.state.AddAnimation (0, "animation", true, (float)16.667);
			shaeeplyAnim.state.SetAnimation(0, "1.7", true);
			//textBoxCount = 1;
			//TrackEntry trackEntry = playerAnim.state.SetAnimation(0, "1.6", true);
			//trackEntry.Complete += HandleDanceComplete;

		}

	}

	void HandleDanceComplete (TrackEntry entry) {
		// Dance completed!
		// Handle the complete event here.n
		if (sceneNumber == "Scene1.2") {
			if (entry.IsComplete == true) {
				Debug.Log("Play a footstep sound!");
				isShowNextScene = true;
			}

		}
		else if (sceneNumber == "Scene1.4") {
			if (entry.IsComplete == true) {
				Debug.Log("Play a footstep sound!");
				isShowNextScene = true;
			}

		}
		else if (sceneNumber == "Scene1.5") {
			if (entry.IsComplete == true) {
				Debug.Log("Play a footstep sound!");
				//isShowNextScene = true;

			}

		}
		else if (sceneNumber == "Scene1.6") {
			if (entry.IsComplete == true) {
				Debug.Log("Play a footstep sound!");
				isShowNextScene = true;
			}

		}
	}


	private string GetWords(string text, float wordCount) {
		float words = wordCount;
		// loop through each character in text
		for (int i = 0; i < text.Length; i++) { 


			words--;
			TextBackground.text = textShownOnScreen;

			if (words <= 0) {
				return text.Substring (0, i);
			}
		}
		return text;
	}

	private bool Waited(float seconds)
	{
		timerMax = seconds;

		timer += Time.deltaTime;

		if (timer >= timerMax)
		{
			return true; //max reached - waited x - seconds
		}

		return false;
	}
	public void ChangeMenuScene(string name)
	{
		FirstScene.sceneNumber = name;
		Application.LoadLevel (name);
	}	

	public void OptionSelected (string option)
	{
		FirstScene.sceneNumber = "Scene1.9";
		Application.LoadLevel (sceneNumber);
	}

	public void gotoMenuScreen() 
	{
		Debug.Log("MEUNU LIST");
		Application.LoadLevel ("MenuList");
	}

	public void ScreenTapped (string option)
	{
		Debug.Log("Tapped");
		Debug.Log(timeElapsed);
		Debug.Log(sceneNumber);
		//FirstScene.sceneNumber = option;
		//		Application.LoadLevel (option);


		if (sceneNumber == "Scene1.2") {

			if (isShowNextScene == true) {
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);
			
			}
		}
		else if (sceneNumber == "Scene1.3") {

			if (timeElapsed > 6.0) {
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);

			}
		}
		else if (sceneNumber == "Scene1.4") {

			if (isShowNextScene == true) {
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);

			}
		}
		else if (sceneNumber == "Scene1.5") {

			if (timeElapsed > 28.0) {
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);

			}
		}
		else if (sceneNumber == "Scene1.6") {

			if (timeElapsed > 5.0){
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);

			}
		}
		else if (sceneNumber == "Scene1.7") {

			if (timeElapsed > 18.0){
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);

			}
		}

		else if (sceneNumber == "Scene1.9") {

			if (timeElapsed > 23.0){
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);

			}
		}

		else if (sceneNumber == "Scene1.10") {

			if (timeElapsed > 10.0){
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);

			}
		}
		else if (sceneNumber == "Scene1.12") {

			if (timeElapsed > 8.0){
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);

			}
		}
		else if (sceneNumber == "Scene1.13") {

			if (timeElapsed > 7.0){
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);

			}
		}
		else if (sceneNumber == "Scene1.14") {

			if (timeElapsed > 10.0){
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);

			}
		}
		else if (sceneNumber == "Scene1.15") {

			if (timeElapsed > 7.0){
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);

			}
		}
		else if (sceneNumber == "Scene1.16") {

			if (timeElapsed > 4.0){
				FirstScene.sceneNumber = option;
				Application.LoadLevel (option);

			}
		}


	}

}
