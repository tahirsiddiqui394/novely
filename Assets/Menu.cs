﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Net.Mail;
using System.Text.RegularExpressions;


public class Menu : MonoBehaviour {

	private Button[] buttons;
	private Canvas canvas;
	Vector2 startPos;

	public float duration; //set the duration in the inspector
	float elapsedTime = Mathf.Infinity;
	bool isShowLogin = false;
	public Vector2 positionB;
	Vector2 positionA;
	bool test = false;
	float scrollSpeed = -400f;
	Transform Panel2;
	Transform Panel1;
	Transform Panel3;
	Transform PanelSplash;
	Transform PanelPopup;
	GameObject canvasObject;
	// Use this for initialization
	void Start () {

		//positionA = GetComponent<RectTransform>().anchoredPosition;
		canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
		Panel1 = canvasObject.transform.Find("Panel1");
		Panel2 = canvasObject.transform.Find("Panel2");
		Panel3 = canvasObject.transform.Find("Panel3");
		PanelSplash = canvasObject.transform.Find("Panel4");
		PanelPopup = canvasObject.transform.Find("PanelPopUp");

		//Panel1.transform.localScale = new Vector3(0, 0, 0);
		Panel2.transform.localScale = new Vector3(0, 0, 0);
		Panel3.transform.localScale = new Vector3(0, 0, 0);
		PanelPopup.transform.localScale = new Vector3(0, 0, 0);

		Panel2.position = Panel1.position;
		Panel3.position = Panel1.position;
		PanelSplash.position = Panel1.position;
		PanelPopup.position = Panel1.position;

		Panel1.transform.localScale = new Vector3(0, 0, 0);

		Button[] ButtonPanel1 = Panel1.GetComponentsInChildren<Button> ();
		Button[] ButtonPanel2 = Panel2.GetComponentsInChildren<Button> ();
		Button[] ButtonPanel3 = Panel3.GetComponentsInChildren<Button> ();

		Debug.Log ("*****");
//		Debug.Log (buttonsTest1.Length);
//		Debug.Log (buttonsTest2.Length);
		Debug.Log ("*****");

		startPos = transform.position;
		startPos.Set (startPos.x - Screen.width, startPos.y);
		Button[] buttons = this.GetComponentsInChildren<Button>();
		Debug.Log (buttons.Length);
		InputField[] inputFields = this.GetComponentsInChildren<InputField>();
		Button btnFacebook = ButtonPanel1[0];
		Button btnGoogle = ButtonPanel1[1];
		Button btnForgot = ButtonPanel1[2];
		Button btnCreateAccount = ButtonPanel1[3];
		btnForgot.image.color = Color.clear;
		btnCreateAccount.image.color = Color.clear;
		//btnFacebook.GetComponentsInChildren<Text> () [0].text = "New Super Cool Button Text";
		InputField[] inputFieldsPanel1 = Panel1.GetComponentsInChildren<InputField>();
		InputField txtUserName = inputFieldsPanel1[0];
		InputField txtPassword = inputFieldsPanel1[1];
		txtUserName.image.color = Color.clear;
		txtPassword.image.color = Color.clear;
		Debug.Log ("GotoCreateAccount");
		Debug.Log (buttons.Length);

		InputField[] inputFieldsPanel2 = Panel2.GetComponentsInChildren<InputField>();
		InputField txtUserName1 = inputFieldsPanel2[0];
		Debug.Log (inputFieldsPanel2.Length);
		txtUserName1.image.color = Color.clear;
		Button btnSignIn = ButtonPanel2[0];
		Button btnSendEmail = ButtonPanel2[1];
		btnSignIn.image.color = Color.clear;

		//PANEL 3
		InputField[] inputFieldsPanel3 = Panel3.GetComponentsInChildren<InputField>();
		Button btnSignIn2 = ButtonPanel3[0];
		Button btnSignUp = ButtonPanel3[1];
		btnSignIn2.image.color = Color.clear;
		//btnSignUp.image.color = Color.clear;

		InputField txtFirstName = inputFieldsPanel3[0];
		InputField txtLastName = inputFieldsPanel3[1];
		InputField txtEmail = inputFieldsPanel3[2];
		InputField txtPassword3 = inputFieldsPanel3[3];
		InputField txtRetypePassword = inputFieldsPanel3[4];
		InputField txtDOB = inputFieldsPanel3[5];

		txtFirstName.image.color = Color.clear;
		txtLastName.image.color = Color.clear;
		txtEmail.image.color = Color.clear;
		txtPassword3.image.color = Color.clear;
		txtRetypePassword.image.color = Color.clear;
		txtDOB.image.color = Color.clear;


		GameObject hand = GameObject.FindGameObjectWithTag("Logo");
//		Image[] Images = PanelSplash.GetComponentsInChildren<Image> ();
//		Image logo = Images[0];
		//iTween.MoveBy(hand, iTween.Hash("x", 400, "easeType", "easeInOutExpo", "loopType", "pingPong", "delay", .1));

		//iTween.FadeFrom (hand, iTween.Hash ("alpha", 1, "time", 2));
		//iTween.ScaleFrom (logo,{});
//		iTween.FadeFrom(hand, iTween.Hash ("alpha", 0f, "time", 2f,
//			"oncomplete", "ReturnToOriginalAlphaTransparencyTweenOnComplete", "oncompletetarget", hand));

	}
	
	// Update is called once per frame
	void Update () {


		//Debug.Log (Time.time);
		if (Time.time > 5.0 && isShowLogin == false) {
			isShowLogin = true;
			showLoginScreen ();
		}
//		if (test == false) {
//		
//			float newPos = Mathf.Repeat (Time.time * scrollSpeed,Screen.width);
//			transform.position = startPos + Vector2.right * newPos;
//			if (transform.position.x == Screen.width) {
//	//			test == false;
//			}
//		}
		
	}
	
	public void showLoginScreen() {
	
		PanelSplash.transform.localScale = new Vector3(0, 0, 0);
		Panel1.transform.localScale = new Vector3(1, 1, 1);
		Panel2.transform.localScale = new Vector3(0, 0, 0);
		Panel3.transform.localScale = new Vector3(0, 0, 0);
	}

	public void showForgotPassword() {
	 
		PanelSplash.transform.localScale = new Vector3(0, 0, 0);
		Panel1.transform.localScale = new Vector3(0, 0, 0);
		Panel2.transform.localScale = new Vector3(1, 1, 1);
		Panel3.transform.localScale = new Vector3(0, 0, 0);
	
	}

	public void showSignIn() {

		PanelSplash.transform.localScale = new Vector3(0, 0, 0);
		Panel1.transform.localScale = new Vector3(1, 1, 1);
		Panel2.transform.localScale = new Vector3(0, 0, 0);
		Panel3.transform.localScale = new Vector3(0, 0, 0);

	}

	public void showSignUp() {

		PanelSplash.transform.localScale = new Vector3(0, 0, 0);
		Panel3.transform.localScale = new Vector3(1, 1, 1);
		Panel2.transform.localScale = new Vector3(0, 0, 0);
		Panel1.transform.localScale = new Vector3(0, 0, 0);

	}
	public void GotoCreateAccount()
	{
		Debug.Log ("GotoCreateAccount");
//		test = true;
//		//Time.time * scrollSpeed
////		Debug.Log(startPos);
//		float newPos = Mathf.Repeat (Time.time * scrollSpeed, Screen.width*2);
////		Debug.Log (startPos);
////		Debug.Log (newPos);
////
////		//GetComponent<RectTransform>().anchoredPosition = positionB;
//		transform.position = startPos + Vector2.right * (Screen.width*2);

	}
	public void LoginWithFacebook() {
		Text[] TextArray = PanelPopup.GetComponentsInChildren<Text> ();
		Text DetailAlert = TextArray[2]; 
		DetailAlert.text = "Feature will be implement later";
		PanelPopup.transform.localScale = new Vector3(1, 1, 1);
	}

	public void LoginWithTwitter() {
		Text[] TextArray = PanelPopup.GetComponentsInChildren<Text> ();
		Text DetailAlert = TextArray[2]; 
		DetailAlert.text = "Feature will be implement later";
		PanelPopup.transform.localScale = new Vector3(1, 1, 1);
	}

	public void ChangeMenuScene(string name)
	{
		
		InputField[] FieldList = Panel1.GetComponentsInChildren<InputField> ();
		InputField UserName = FieldList[0];
		InputField Password = FieldList[1];

		string userName = UserName.text;
		string password = Password.text;
		 
		Text[] TextArray = PanelPopup.GetComponentsInChildren<Text> ();
		Text DetailAlert = TextArray[2]; 

		if (userName == "" ||
		    userName.Replace (" ", "").Length == 0) {

			DetailAlert.text = "Enter User name";
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			Debug.Log ("Enter User name");
			return;

		}
		if (password == "" ||
			password.Replace (" ", "").Length == 0) {
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			DetailAlert.text = "Enter Password ";
			Debug.Log ("Enter Password");
			return;
		} 
		if (password.Length < 6){
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			DetailAlert.text = "Password length must be greater than 6 characters";
			Debug.Log ("Password length must be greater than 6 characters");
			return;
		}

		Application.LoadLevel (name);
	}
	public void RemovePopUp() {

		PanelPopup.transform.localScale = new Vector3(0, 0, 0);
	
	}


	public void SignUpPressed(string name)
	{
		
		InputField[] FieldList = Panel3.GetComponentsInChildren<InputField> ();
		InputField FirstName = FieldList[0];
		InputField LastName = FieldList[1];
		InputField Email = FieldList[2];
		InputField Password = FieldList[3];
		InputField ConfirmPassword = FieldList[4];
		InputField DateOfBirth = FieldList[5];

		string firstName = FirstName.text;
		string lastName = LastName.text;
		string email = Email.text;
		string password = Password.text;
		string confirmPassword = ConfirmPassword.text;
		string dateOfBirth = DateOfBirth.text;

		Text[] TextArray = PanelPopup.GetComponentsInChildren<Text> ();
		Text DetailAlert = TextArray[2]; 

		if (firstName == "" ||
			firstName.Replace (" ", "").Length == 0) {

			DetailAlert.text = "Enter first name";
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			Debug.Log ("Enter first name");
			return;

		}
		if (lastName == "" ||
			lastName.Replace (" ", "").Length == 0) {

			DetailAlert.text = "Enter last name";
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			Debug.Log ("Enter last name");
			return;

		}
		if (email == "" ||
			email.Replace (" ", "").Length == 0) {

			DetailAlert.text = "Enter email name";
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			Debug.Log ("Enter email name");
			return;

		}

		Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
		Match match = regex.Match(email);
		if (match.Success) {
			
			Debug.Log (email + " is correct");
		}
		else {
			DetailAlert.text = "Enter valid email address";
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			Debug.Log(email + " is incorrect");
			return;
		}
		if (password == "" ||
			password.Replace (" ", "").Length == 0) {
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			DetailAlert.text = "Enter Password ";
			Debug.Log ("Enter Password");
			return;
		} 
		if (password.Length < 6){
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			DetailAlert.text = "Password length must be greater than 6 characters";
			Debug.Log ("Password length must be greater than 6 characters");
			return;
		}
		if (confirmPassword == "" ||
			password.Replace (" ", "").Length == 0) {
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			DetailAlert.text = "Enter confirm password ";
			Debug.Log ("Enter confirm Password");
			return;
		}
		if (confirmPassword != password) {
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			DetailAlert.text = "Retype password should be same as password ";
			Debug.Log ("Enter Retype not match Password");
			return;
		}



		Application.LoadLevel (name);
	}



	public void ForgotPassword() 
	{
		InputField[] FieldList = Panel2.GetComponentsInChildren<InputField> ();
		InputField Email = FieldList[0];
		string email = Email.text;

		Text[] TextArray = PanelPopup.GetComponentsInChildren<Text> ();
		Text DetailAlert = TextArray[2]; 

		if (email == "" ||
			email.Replace (" ", "").Length == 0) {

			DetailAlert.text = "Enter email name";
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			Debug.Log ("Enter email name");
			return;

		}

		Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
		Match match = regex.Match(email);
		if (match.Success) {

			Debug.Log (email + " is correct");
		}
		else {
			DetailAlert.text = "Enter valid email address";
			PanelPopup.transform.localScale = new Vector3(1, 1, 1);
			Debug.Log(email + " is incorrect");
			return;
		}

		Panel2.transform.localScale = new Vector3(0, 0, 0);

	}
}
/*

string email = txtemail.Text;
Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
Match match = regex.Match(email);
if (match.Success)
    Response.Write(email + " is correct");
else
    Response.Write(email + " is incorrect");

*/