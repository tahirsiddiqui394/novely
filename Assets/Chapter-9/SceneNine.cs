﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneNine : MonoBehaviour {

    public Text abbyText;
    public Text travisText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby;
    public Image travis;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation travisSkeleton;
    public Camera mainCamera;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    float pause = 1;

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
            abby.enabled = false;
            travis.enabled = false;


        if (SceneSix.isDirty)
            abbySkeleton.AnimationName = "9.1 idle dirty";
        else
            abbySkeleton.AnimationName = "9.1 idle (clean)";
    }

    // Update is called once per frame
    void Update()
    {

        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;

        if (level == "Scene9.1")
        {

            if (timeElapsed < 4.0)
            {
                float wordscount = timeElapsed * wordsPerSecond;
                conversation("A", "You’re a lunatic!", wordscount);
                pause = 4.1f;
            }
            else if (timeElapsed > 4 && timeElapsed < 4.2)
            {
                clearText();
            }
            else if (timeElapsed >= 4.2 && timeElapsed < 7.0)
            {
                float wordscount = (timeElapsed - (float)4.2) * wordsPerSecond;
                conversation("T", "I went the speed limit.", wordscount);
                pause = 7.1f;
            }
            else if (timeElapsed > 7.0 && timeElapsed < 7.2)
            {
                clearText();
            }
            else if (timeElapsed >= 7.2 && timeElapsed < 11.0)
            {
                float wordscount = (timeElapsed - (float)7.2) * wordsPerSecond;
                conversation("A", "Yeah, if we were on the Autobahn!", wordscount);
                pause = 11.1f;
            }
            else if (timeElapsed > 11.0 && timeElapsed < 11.2)
            {
                clearText();
            }
            else if (timeElapsed >= 11.2 && timeElapsed < 15.0)
            {
                float wordscount = (timeElapsed - (float)11.2) * wordsPerSecond;
                conversation("T", "I wouldn’t let anything happen to you, Pigeon.", wordscount);
                pause = 14.3f;
            }

            else if (timeElapsed >= 15.2 && timeElapsed < 17.8)
            {
                travis.enabled = false;
                clearText();
                travisSkeleton.AnimationName = "9.1 comb her hair";
                zoomIn(2.35f, new Vector3(0.52f, 0f, -10f));
            }

            else if (timeElapsed >= 17.8 && timeElapsed < 18.5)
            {
                zoomOut();
                travisSkeleton.AnimationName = "9.1 idle";
                next.enabled = true;
                clearText();
                travis.enabled = false;
            }
            else if (timeElapsed > 19.0)
            {

                nextScene();
            }
        }
    }

    public void nextScene()
    {
       level = "Scene10.1";
        //level = "Splash";
        SceneManager.LoadScene(level);
    }

    private void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
        if (travisText != null)
            travisText.text = textShownOnScreen;
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "A")
                abbyText.text = textShownOnScreen;
            else if (character == "T")
                travisText.text = textShownOnScreen;

            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }


    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;
        setAnimation(character, text);
        if (character == "A")
        {
            abby.enabled = true;
            if (travis != null)
                travis.enabled = false;
        }

        else if (character == "T")
        {
            abby.enabled = false;
            travis.enabled = true;
        }
        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private void setAnimation(string character, string text)
    {
    if (level == "Scene9.1")
        {
            if (character == "A")
            {
                travisSkeleton.AnimationName = "9.1 idle";
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "9.1 dialouge dirty";
                else
                    abbySkeleton.AnimationName = "9.1 dialouge (clean)";

            }

            else if (character == "T")
            {
                travisSkeleton.AnimationName = "9.1 dialogue";
                if(SceneSix.isDirty)
                    abbySkeleton.AnimationName = "9.1 idle dirty";
                else
                    abbySkeleton.AnimationName = "9.1 idle (clean)";
            }
        }
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }

    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setTexts(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }

    private void setTexts(string character, string text)
    {
        if (character == "A")
            abbyText.text = text;
        else if (character == "T")
            travisText.text = text;
    }

    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }
}
