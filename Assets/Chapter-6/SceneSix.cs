﻿using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSix : MonoBehaviour {

    public Text abbyText;
    public Text americaText;
    public float wordsPerSecond = 15; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby;
    public Image america;
    private float timer = 0;
    private float timerMax = 0;
    public string level ="";
    public Canvas c;
    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation americaSkeleton;
    public Canvas next;
    public string savetext = "";
    bool saveState;
    float pause = 0;
    bool textSet = false;

        public static bool isDirty;
    // Use this for initialization
    void Start () {
        Screen.orientation = ScreenOrientation.Portrait;
        if (level == "Scene6.1")
        {
            c.enabled = false;
        }
        else if (level == "Scene6.2")
        {
            textSet = true;
            abby.enabled = false;
            america.enabled = false;
          
            if (isDirty)
                abbySkeleton.AnimationName = "6.2 idle dirty dress";
            else
                abbySkeleton.AnimationName = "6.2 idle clean dress";
        }
        if(level == "Scene6.3")
        {
           
        }
        
    }
	
	// Update is called once per frame
	void Update () {

        if (!Waited(1))
            return;


        timeElapsed += Time.deltaTime;
        if(level == "Scene6.1")
        {
            if (timeElapsed > 3.0)
            {
                c.enabled = true;
            }
        }

        else if (level == "Scene6.2")
        {

            if (timeElapsed >=2 && timeElapsed < 3.0)
            {
                americaSkeleton.loop = true;
                float wordscount = timeElapsed * wordsPerSecond;
                conversation(false, "You look... lovely.", wordscount);
                pause = 3.1f;

            }
            else if (timeElapsed > 3.0 && timeElapsed < 3.2)
            {
                clearText();
            }
            else if (timeElapsed >3.2 && timeElapsed < 10.0)
            {
                float wordscount = (timeElapsed - (float)3.2) * wordsPerSecond;
                conversation(false, "Why didn’t you just go for the full effect and roll in dog shit,Abby ? Ew.", wordscount);
                pause = 10.1f;
            }
            else if (timeElapsed > 10.0 && timeElapsed < 10.2)
            {
                clearText();
            }
            else if (timeElapsed > 10.2 && timeElapsed < 15.0)
            {
                float wordscount = (timeElapsed - (float)10.2) * wordsPerSecond;
                conversation(true, "I’m not trying to impress anyone.", wordscount);
                pause = 15.1f;
            }
            else if (timeElapsed > 15.0 && timeElapsed < 15.2)
            {
                clearText();
            }
            else if (timeElapsed > 15.2 && timeElapsed < 18.0)
            {
                float wordscount = (timeElapsed - (float)15.2) * wordsPerSecond;
                conversation(false, "Clearly. You ready?", wordscount);
                pause = 18.1f;
            }
            else if (timeElapsed > 18.0 && timeElapsed < 18.2)
            {
                clearText();
            }
            else if (timeElapsed > 18.2 && timeElapsed < 25.5)
            {
                float wordscount = (timeElapsed - (float)18.2) * wordsPerSecond;
                conversation(true, "I knew it. You’ve been hoping we’d find brothers to date since we met.", wordscount);
                pause = 25.7f;
            }

            else if (timeElapsed > 25.5 && timeElapsed < 25.8)
            {
                clearText();
            }

            else if (timeElapsed > 25.8 && timeElapsed < 29.0)
            {
                float wordscount = (timeElapsed - (float)25.8) * wordsPerSecond;
                conversation(true, "This is the next best thing.", wordscount);
                pause = 29.1f;
            }
            else if (timeElapsed > 29.0 && timeElapsed < 29.2)
            {
                clearText();
            }

            else if (timeElapsed > 29.2 && timeElapsed < 30.0)
            {
                float wordscount = (timeElapsed - (float)29.2) * wordsPerSecond;
                conversation(false, "So?", wordscount);
                pause = 30.1f;
            }
            else if (timeElapsed > 30.0 && timeElapsed < 30.2)
            {
                clearText();
            }
            else if (timeElapsed > 30.2 && timeElapsed < 37.0)
            {
                float wordscount = (timeElapsed - (float)30.2) * wordsPerSecond;
                conversation(true, "It’s not going to happen, Mare.He’s exactly why you followed me here.", wordscount);
                pause = 37.1f;
            }

            else if (timeElapsed > 37.0 && timeElapsed < 37.2)
            {
                clearText();
            }

            else if (timeElapsed > 37.2 && timeElapsed < 43.0)
            {
                float wordscount = (timeElapsed - (float)37.2) * wordsPerSecond;
                conversation(true, "To make sure I didn’t fall for someone like my dad.", wordscount);
                pause = 43.1f;
            }

            else if (timeElapsed > 43.0 && timeElapsed < 43.2)
            {
                clearText();
            }

            else if (timeElapsed > 43.2 && timeElapsed < 48.5)
            {
                float wordscount = (timeElapsed - (float)43.2) * wordsPerSecond;
                conversation(false, "Travis is nothing like your dad. C’mon.You’ll see.", wordscount);
                pause = 48.5f;
            }

            else if (timeElapsed > 48.5)
            {
                clearText();
                abby.enabled = false;
                america.enabled = false;
                level = "Scene6.3";
                SceneManager.LoadScene(level);
            }
        }

        else if (level == "Scene6.3")
        {
            if (timeElapsed > 2 )
            {
                next.enabled = true;
                nextScene();
            }
        }

    }

    private void pauseUpdate(int v)
    {
        enabled = false;
        GetWords(saveState, savetext, 2000);
        timeElapsed = pause;
        Thread.Sleep(v);
        enabled = true;
    }

    public void nextScene()
    {
        level = "Scene7.1";
        SceneManager.LoadScene(level);
    }

    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setText(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }

    }

    private void setText(bool State, string text)
    {
        if (State)
            abbyText.text = text;
        else
            americaText.text = text;
    }

    private void setAnimationLoop(bool v)
    {
        enabled = v;
        if (abbySkeleton != null)
        {
            abbySkeleton.loop = v;
            if (isDirty)
                abbySkeleton.AnimationName = "6.2 idle dirty dress";
            else
                abbySkeleton.AnimationName = "6.2 idle clean dress";
        }

        if (americaSkeleton != null)
        {
            americaSkeleton.loop = v;
            americaSkeleton.AnimationName = "6.2 idle";
        }

    }

    private void clearText()
    {
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
        americaText.text = textShownOnScreen;
    }

    private string GetWords(bool isAbby,string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if(isAbby)
            abbyText.text = textShownOnScreen;
            else
                americaText.text = textShownOnScreen;

            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }


    public void conversation(bool isAbby, string text, float wordCount)
    {
        saveState = isAbby;
        savetext = text;

        if (isAbby) {
            abby.enabled = true;
            america.enabled = false;

            americaSkeleton.AnimationName = "6.2 idle";
            if (isDirty)
                abbySkeleton.AnimationName = "6.2 dialougue dirty dress";
            else
                abbySkeleton.AnimationName = "6.2 dialougue clean dress";
        }
        else {
            abby.enabled = false;
            america.enabled = true;

            if(text == "Clearly. You ready?")
                americaSkeleton.AnimationName = "6.2 smiles";
            else
                americaSkeleton.AnimationName = "6.2 dialogue";


            if (isDirty)
                abbySkeleton.AnimationName = "6.2 idle dirty dress";
            else
                abbySkeleton.AnimationName = "6.2 idle clean dress";
        }

        if(!textSet)
        textShownOnScreen = GetWords(isAbby, text, wordCount);
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }

    public void clicked(string str)
    {

            string [] arr = str.Split(',');
            level = arr[0];
            SceneManager.LoadScene(level);
            bool b = Convert.ToBoolean(arr[1]);
            isDirty = b;
       
    }

}
