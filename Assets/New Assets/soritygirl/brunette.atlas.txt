
brunette.png
size: 1348,1348
format: RGBA8888
filter: Linear,Linear
repeat: none
Burnette
  rotate: true
  xy: 589, 39
  size: 207, 203
  orig: 207, 203
  offset: 0, 0
  index: -1
Burnette_Excited Lips
  rotate: false
  xy: 800, 91
  size: 207, 203
  orig: 207, 203
  offset: 0, 0
  index: -1
Face
  rotate: true
  xy: 296, 8
  size: 238, 285
  orig: 238, 285
  offset: 0, 0
  index: -1
Hair Back
  rotate: false
  xy: 364, 896
  size: 326, 444
  orig: 326, 444
  offset: 0, 0
  index: -1
Hair Front
  rotate: true
  xy: 418, 505
  size: 383, 319
  orig: 383, 319
  offset: 0, 0
  index: -1
L Leg
  rotate: false
  xy: 8, 284
  size: 215, 586
  orig: 215, 586
  offset: 0, 0
  index: -1
L Shoe
  rotate: false
  xy: 8, 17
  size: 280, 259
  orig: 280, 259
  offset: 0, 0
  index: -1
L arm
  rotate: true
  xy: 418, 254
  size: 243, 321
  orig: 243, 321
  offset: 0, 0
  index: -1
L eye
  rotate: false
  xy: 800, 41
  size: 53, 42
  orig: 53, 42
  offset: 0, 0
  index: -1
L hand
  rotate: true
  xy: 1160, 181
  size: 155, 148
  orig: 155, 148
  offset: 0, 0
  index: -1
L pupil
  rotate: false
  xy: 747, 263
  size: 32, 31
  orig: 32, 31
  offset: 0, 0
  index: -1
L sholder
  rotate: true
  xy: 745, 511
  size: 126, 221
  orig: 126, 221
  offset: 0, 0
  index: -1
L thigh
  rotate: false
  xy: 231, 362
  size: 179, 508
  orig: 179, 508
  offset: 0, 0
  index: -1
Lip
  rotate: false
  xy: 1249, 1296
  size: 62, 44
  orig: 62, 44
  offset: 0, 0
  index: -1
Neck
  rotate: false
  xy: 1119, 479
  size: 177, 177
  orig: 177, 177
  offset: 0, 0
  index: -1
Pelvis
  rotate: false
  xy: 745, 645
  size: 366, 323
  orig: 366, 323
  offset: 0, 0
  index: -1
R Leg
  rotate: true
  xy: 698, 976
  size: 157, 533
  orig: 157, 533
  offset: 0, 0
  index: -1
R Thigh
  rotate: true
  xy: 698, 1141
  size: 199, 543
  orig: 199, 543
  offset: 0, 0
  index: -1
R arm
  rotate: false
  xy: 1119, 664
  size: 218, 304
  orig: 218, 304
  offset: 0, 0
  index: -1
R eye
  rotate: false
  xy: 231, 313
  size: 73, 41
  orig: 73, 41
  offset: 0, 0
  index: -1
R hand
  rotate: false
  xy: 1030, 344
  size: 177, 127
  orig: 177, 127
  offset: 0, 0
  index: -1
R pupil
  rotate: false
  xy: 698, 932
  size: 38, 36
  orig: 38, 36
  offset: 0, 0
  index: -1
R shoe
  rotate: true
  xy: 747, 302
  size: 201, 275
  orig: 201, 275
  offset: 0, 0
  index: -1
R sholder
  rotate: false
  xy: 1015, 50
  size: 137, 244
  orig: 137, 244
  offset: 0, 0
  index: -1
Torso
  rotate: false
  xy: 8, 878
  size: 348, 462
  orig: 348, 462
  offset: 0, 0
  index: -1
