﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuList : MonoBehaviour {

	Transform SideMenu;
	Transform PanelAbout;
	Transform PanelEditProfile;
	bool isShowSideMenu = false;
	bool isLoadFirstTime = false;
	GameObject ObjSideMenu;
	// Use this for initialization
	void Start () {

		if (isLoadFirstTime == false) {
			isLoadFirstTime = true;
			GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
			SideMenu = canvasObject.transform.Find("SideMenuPanel");
			PanelAbout = canvasObject.transform.Find("PanelAbout");
			PanelEditProfile = canvasObject.transform.Find("PanelEditProfile");
			Debug.Log ("sddsdjanjds");
			PanelAbout.transform.localScale = new Vector3(0, 0, 0);
			PanelEditProfile.transform.localScale = new Vector3(0, 0, 0);
			ObjSideMenu = GameObject.FindGameObjectWithTag("SideMenu");
			Debug.Log ("*******&&&&&&&*****");
			Debug.Log (ObjSideMenu.transform.position);
			//iTween.MoveBy(ObjSideMenu, iTween.Hash("x", -Screen.width , "easeType", "easeInOutExpo",  "delay", 0));
			//ObjSideMenu.transform.position = new Vector3 (-Screen.width,ObjSideMenu.transform.position.y,0);
			Debug.Log (ObjSideMenu.transform.position);
			//showSideMenu ();

//			Vector3 temp = new Vector3(7.0f,0,0);
//			myGameObject.transform.position += temp;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void showSideMenu()
	{
		Debug.Log (isShowSideMenu);
		if (isShowSideMenu == false) {
			//SideMenu.transform.localScale = new Vector3(1, 1, 1);
//			iTween.MoveBy(ObjSideMenu, iTween.Hash("x", Screen.width + 40, "easeType", "easeInOutExpo",  "delay", .1));
			iTween.MoveBy(ObjSideMenu, iTween.Hash("x",5 , "easeType", "easeInOutExpo",  "delay", .1));
			isShowSideMenu = true;
		} else {
			//iTween.MoveBy(ObjSideMenu, iTween.Hash("x", 400, "easeType", "easeInOutExpo", "loopType", "pingPong", "delay", .1));
			//SideMenu.transform.localScale = new Vector3(1, 1, 1);
			iTween.MoveBy(ObjSideMenu, iTween.Hash("x",-5 , "easeType", "easeOutExpo",  "delay", .1));
			Debug.Log (ObjSideMenu.transform.position);
			isShowSideMenu = false;
		}

	}

	public void DisableBoolAnimator(Animator anim)
	{
		anim.SetBool ("IsDisplayed",false);
	}

	public void EnableBoolAnimator(Animator anim)
	{
		anim.SetBool ("IsDisplayed",true);
	}
	public void NavigateTo(int scene)
	{
		//
	}
	public void ChangeMenuScene(string name)
	{
		Application.LoadLevel (name);
	}

	public void ShowHome()
	{
		showSideMenu ();
	}
	public void ShowAbout()
	{
		PanelAbout.transform.localScale = new Vector3(1, 1, 1);
		iTween.MoveBy(ObjSideMenu, iTween.Hash("x",-5 , "easeType", "easeOutExpo",  "delay",0));
		isShowSideMenu = false;
	}
	public void RateNovly()
	{

	}
	public void BackFromAbout()
	{
		PanelAbout.transform.localScale = new Vector3(0, 0, 0);
	}
	public void ShowEditProfileScreen() 
	{
		PanelEditProfile.transform.localScale = new Vector3(1, 1, 1);
		iTween.MoveBy(ObjSideMenu, iTween.Hash("x",-5 , "easeType", "easeOutExpo",  "delay",0));
		isShowSideMenu = false;
	}
	public void UpdateProfile() 
	{
		PanelEditProfile.transform.localScale = new Vector3(0, 0, 0);
	}
}
