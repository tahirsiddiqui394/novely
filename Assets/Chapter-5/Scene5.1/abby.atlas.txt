
abby.png
size: 2485,862
format: RGBA8888
filter: Linear,Linear
repeat: none
Hair L
  rotate: true
  xy: 2, 222
  size: 424, 744
  orig: 424, 744
  offset: 0, 0
  index: -1
Head
  rotate: true
  xy: 1838, 415
  size: 259, 395
  orig: 259, 395
  offset: 0, 0
  index: -1
Hip
  rotate: false
  xy: 2188, 35
  size: 283, 230
  orig: 283, 230
  offset: 0, 0
  index: -1
L-Arm
  rotate: true
  xy: 2, 67
  size: 153, 568
  orig: 153, 568
  offset: 0, 0
  index: -1
L-Eyebrow
  rotate: false
  xy: 2235, 415
  size: 51, 19
  orig: 51, 19
  offset: 0, 0
  index: -1
L-Hand
  rotate: true
  xy: 799, 6
  size: 150, 221
  orig: 150, 221
  offset: 0, 0
  index: -1
L-Leg
  rotate: true
  xy: 1150, 676
  size: 184, 1147
  orig: 184, 1147
  offset: 0, 0
  index: -1
L-Pupil
  rotate: false
  xy: 2, 2
  size: 31, 31
  orig: 31, 31
  offset: 0, 0
  index: -1
L-Shoe
  rotate: false
  xy: 2235, 462
  size: 221, 172
  orig: 221, 172
  offset: 0, 0
  index: -1
Lip
  rotate: true
  xy: 1097, 80
  size: 76, 38
  orig: 76, 38
  offset: 0, 0
  index: -1
Puf
  rotate: false
  xy: 1793, 25
  size: 393, 240
  orig: 393, 240
  offset: 0, 0
  index: -1
R-Arm
  rotate: true
  xy: 1838, 267
  size: 146, 573
  orig: 146, 573
  offset: 0, 0
  index: -1
R-Eye
  rotate: false
  xy: 572, 171
  size: 93, 49
  orig: 93, 49
  offset: 0, 0
  index: -1
R-Eyebrow
  rotate: false
  xy: 2, 35
  size: 109, 30
  orig: 109, 30
  offset: 0, 0
  index: -1
R-Hair
  rotate: true
  xy: 1150, 374
  size: 300, 686
  orig: 300, 686
  offset: 0, 0
  index: -1
R-Hand
  rotate: true
  xy: 572, 8
  size: 148, 225
  orig: 148, 225
  offset: 0, 0
  index: -1
R-Leg
  rotate: true
  xy: 2, 648
  size: 212, 1146
  orig: 212, 1146
  offset: 0, 0
  index: -1
R-Pupil
  rotate: true
  xy: 2235, 636
  size: 38, 40
  orig: 38, 40
  offset: 0, 0
  index: -1
R-Shoe
  rotate: true
  xy: 2299, 636
  size: 224, 184
  orig: 224, 184
  offset: 0, 0
  index: -1
Torso
  rotate: false
  xy: 748, 158
  size: 387, 488
  orig: 387, 488
  offset: 0, 0
  index: -1
_0000_Mouth-Aw
  rotate: false
  xy: 2235, 436
  size: 57, 24
  orig: 57, 24
  offset: 0, 0
  index: -1
_0001_lip-sad
  rotate: true
  xy: 1793, 309
  size: 63, 20
  orig: 63, 20
  offset: 0, 0
  index: -1
_0002_lip-laugh
  rotate: false
  xy: 1022, 110
  size: 73, 46
  orig: 73, 46
  offset: 0, 0
  index: -1
hair back
  rotate: true
  xy: 1137, 97
  size: 275, 654
  orig: 275, 654
  offset: 0, 0
  index: -1
l_Eye
  rotate: false
  xy: 667, 171
  size: 71, 49
  orig: 71, 49
  offset: 0, 0
  index: -1
