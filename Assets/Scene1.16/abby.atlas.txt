
abby.png
size: 2484,884
format: RGBA8888
filter: Linear,Linear
repeat: none
Hair L
  rotate: false
  xy: 2, 138
  size: 424, 744
  orig: 424, 744
  offset: 0, 0
  index: -1
Head
  rotate: false
  xy: 1310, 273
  size: 259, 395
  orig: 259, 395
  offset: 0, 0
  index: -1
Hip
  rotate: false
  xy: 1577, 43
  size: 283, 230
  orig: 283, 230
  offset: 0, 0
  index: -1
L-Arm
  rotate: false
  xy: 1155, 314
  size: 153, 568
  orig: 153, 568
  offset: 0, 0
  index: -1
L-Eyebrow
  rotate: false
  xy: 280, 44
  size: 51, 19
  orig: 51, 19
  offset: 0, 0
  index: -1
L-Hand
  rotate: false
  xy: 2198, 13
  size: 150, 221
  orig: 150, 221
  offset: 0, 0
  index: -1
L-Leg
  rotate: true
  xy: 428, 10
  size: 184, 1147
  orig: 184, 1147
  offset: 0, 0
  index: -1
L-Pupil
  rotate: false
  xy: 332, 11
  size: 31, 31
  orig: 31, 31
  offset: 0, 0
  index: -1
L-Shoe
  rotate: true
  xy: 2303, 236
  size: 221, 172
  orig: 221, 172
  offset: 0, 0
  index: -1
Lip
  rotate: false
  xy: 214, 3
  size: 76, 38
  orig: 76, 38
  offset: 0, 0
  index: -1
Puf
  rotate: true
  xy: 1571, 275
  size: 393, 240
  orig: 393, 240
  offset: 0, 0
  index: -1
R-Arm
  rotate: false
  xy: 1007, 309
  size: 146, 573
  orig: 146, 573
  offset: 0, 0
  index: -1
R-Eye
  rotate: true
  xy: 181, 43
  size: 93, 49
  orig: 93, 49
  offset: 0, 0
  index: -1
R-Eyebrow
  rotate: true
  xy: 149, 27
  size: 109, 30
  orig: 109, 30
  offset: 0, 0
  index: -1
R-Hair
  rotate: false
  xy: 428, 196
  size: 300, 686
  orig: 300, 686
  offset: 0, 0
  index: -1
R-Hand
  rotate: false
  xy: 1862, 9
  size: 148, 225
  orig: 148, 225
  offset: 0, 0
  index: -1
R-Leg
  rotate: true
  xy: 1310, 670
  size: 212, 1146
  orig: 212, 1146
  offset: 0, 0
  index: -1
R-Pupil
  rotate: false
  xy: 292, 2
  size: 38, 40
  orig: 38, 40
  offset: 0, 0
  index: -1
R-Shoe
  rotate: true
  xy: 2012, 10
  size: 224, 184
  orig: 224, 184
  offset: 0, 0
  index: -1
Torso
  rotate: true
  xy: 1813, 281
  size: 387, 488
  orig: 387, 488
  offset: 0, 0
  index: -1
_0000_Mouth-Aw
  rotate: true
  xy: 331, 79
  size: 57, 24
  orig: 57, 24
  offset: 0, 0
  index: -1
_0001_lip-sad
  rotate: false
  xy: 149, 5
  size: 63, 20
  orig: 63, 20
  offset: 0, 0
  index: -1
_0002_lip-laugh
  rotate: true
  xy: 232, 63
  size: 73, 46
  orig: 73, 46
  offset: 0, 0
  index: -1
_0003_money
  rotate: false
  xy: 2, 9
  size: 145, 127
  orig: 145, 127
  offset: 0, 0
  index: -1
blood
  rotate: true
  xy: 2303, 459
  size: 209, 179
  orig: 209, 179
  offset: 0, 0
  index: -1
hair back
  rotate: false
  xy: 730, 228
  size: 275, 654
  orig: 275, 654
  offset: 0, 0
  index: -1
l_Eye
  rotate: true
  xy: 280, 65
  size: 71, 49
  orig: 71, 49
  offset: 0, 0
  index: -1
