﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSixteen : MonoBehaviour
{

    public Text abbyText;
    public Text travisText;
    public Text waiterText;
    public Text dialogText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby;
    public Image waiter;
    public Image travis;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    String option = "";
    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation travisSkeleton;
    public SkeletonAnimation waiteressSkeleton;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    public Camera mainCamera;
    float pause = 1;
    public Button button;

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        abby.enabled = false;
        travis.enabled = false;
        waiter.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;

        if (level == "Scene16.1")
        {
                if (timeElapsed < 15.0)
                {
                    float wordscount = timeElapsed * wordsPerSecond;
                    conversation("TW", "Travis and Abby spend more time together, but everywhere they go, they’re asked the same question. In the beginning, they think nothing of it, but as time goes on, Abby begins to get annoyed, and Travis is apologetic.", wordscount);
                    pause = 15.1f;
                }
                else if (timeElapsed > 15.0 && timeElapsed < 15.2)
                {
                    waiter.enabled = false;
                    dialogText.text = "";
                    clearText();
                button.image.color = new Color(255f, 0f, 0f, .100f);
            }


                else if (timeElapsed > 15.3 && timeElapsed < 17.5)
                {
                    waiteressSkeleton.AnimationName = "10.2,10.8 enter";
                    waiteressSkeleton.timeScale = 1;
                    pause = 17.6f;
                }

            else if (timeElapsed > 17.5 && timeElapsed < 18.0)
            {
                waiteressSkeleton.AnimationName = "10.1 idle";
            }
            else if (timeElapsed > 18.0 && timeElapsed < 20.0)
            {
                float wordscount = (timeElapsed - (float)18.0) * wordsPerSecond;
                conversation("W", "Are you two dating?", wordscount);
                pause = 20.1f;
            }
            else if (timeElapsed > 20.0 && timeElapsed < 20.2)
            {
                clearText();
               
            }
            else if (timeElapsed > 20.0 && timeElapsed < 21.0)
            {
                float wordscount = (timeElapsed - (float)20.0) * wordsPerSecond;
                conversation("T", "No", wordscount);
                pause = 21.1f;
            }
            else if (timeElapsed > 21.0 && timeElapsed < 21.2)
            {
                clearText();
               
            }
            else if (timeElapsed > 21.0 && timeElapsed < 22.0)
            {
                float wordscount = (timeElapsed - (float)21.0) * wordsPerSecond;
                conversation("A", "No", wordscount);
                pause = 22.1f;
            }
            else if (timeElapsed > 22.0 )
            {
                clearText();
                nextScene();
            }


        }

    }

    public void nextScene()
    {
        level = "Scene16.2";
        SceneManager.LoadScene(level);
    }

    private void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
        dialogText.text = "";
        travisText.text = textShownOnScreen;
        waiterText.text = textShownOnScreen;
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "A")
                abbyText.text = textShownOnScreen;
            else if (character == "T")
                travisText.text = textShownOnScreen;

            else if (character == "W")
                waiterText.text = textShownOnScreen;
            else if (character == "TW")
                dialogText.text = textShownOnScreen;

            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }



    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;
        setAnimation(character, text);
        if (character == "TW")
        {
            abby.enabled = false;
            travis.enabled = false;
            waiter.enabled = false;
        }

       else if (character == "A")
        {
            abby.enabled = true;
            travis.enabled = false;
            waiter.enabled = false;
        }

        else if (character == "T")
        {
            abby.enabled = false;
            travis.enabled = true;
            waiter.enabled = false;
        }
        else if (character == "W")
        {
            abby.enabled = false;
            travis.enabled = false;
            waiter.enabled = true;
        }
        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }


    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setTexts(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }

    private void setTexts(string character, string text)
    {
        if (character == "A")
            abbyText.text = text;
        else if (character == "T")
            travisText.text = text;
        else if (character == "W")
            waiterText.text = text;
        else if (character == "TW")
            dialogText.text = text;
    }

    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }


    private void setAnimation(string character, string text)
    {
        if (character == "TW")
        {
        }
        else if (character == "A")
        {
            if (level == "Scene16.1")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 dialouge dirty";
                else
                    abbySkeleton.AnimationName = "10 dialouge clean";
                if (waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.1 idle";

                travisSkeleton.AnimationName = "10 idle";
            }
        }

        else if (character == "T")
        {
            if (level == "Scene16.1")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 idle dirty";
                else
                    abbySkeleton.AnimationName = "10 idle clean";
                if (waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.1 idle";
                travisSkeleton.AnimationName = "10.1 dialogue";
            }
        }
        else if (character == "W")
        {
            if (level == "Scene16.1")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 idle dirty";
                else
                    abbySkeleton.AnimationName = "10 idle clean";
                if (waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.1 idle";
                travisSkeleton.AnimationName = "10 idle";
            }
        }
    }
}
