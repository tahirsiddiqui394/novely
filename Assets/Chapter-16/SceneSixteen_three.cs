﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSixteen_three : MonoBehaviour
{

    public Text abbyText;
    public Text travisText;
    public Text brazilText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby;
    public Image brazil;
    public Image travis;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    String option = "";
    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation travisSkeleton;
    public SkeletonAnimation soritySkeleton;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    public Camera mainCamera;
    float pause = 1;
    public Button button;

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        abby.enabled = false;
        travis.enabled = false;
        brazil.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;

        if (level == "Scene16.3")
        {
                if (timeElapsed >3.0  && timeElapsed < 5.0)
                {
                    zoomIn(2.22f, new Vector3(1.03f, 0.5f, -10f));
                    pause = 5.1f;
                }

                else if (timeElapsed > 5.0 && timeElapsed < 13.0)
                {
                    float wordscount = (timeElapsed - (float)5.0) * wordsPerSecond;
                    conversation("S", "Trav, are you, like, dating her or something? You’re always with her, and you’re never always with one girl.", wordscount);
                    pause = 13.1f;
                }

            else if (timeElapsed > 13.0 && timeElapsed < 14.2)
            {
                clearText();
                brazil.enabled = false;
                zoomOut();
               
            }
            else if (timeElapsed >14.2 && timeElapsed < 15.7)
            {
                float wordscount = (timeElapsed - (float)14.2) * wordsPerSecond;
                conversation("T", "No", wordscount);
                pause =15.8f;
            }
            else if (timeElapsed > 15.7 && timeElapsed < 16.0)
            {
                clearText();
               
            }
            else if (timeElapsed > 16.0 && timeElapsed < 17.5)
            {
                float wordscount = (timeElapsed - (float)16.0) * wordsPerSecond;
                conversation("A", "No", wordscount);
                pause = 17.5f;
            }
            else if (timeElapsed > 17.5 )
            {
                clearText();
                nextScene();
            }


        }

    }

    public void nextScene()
    {
        level = "Scene16.7";
        SceneManager.LoadScene(level);
    }

    private void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
        travisText.text = textShownOnScreen;
        brazilText.text = textShownOnScreen;
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "A")
                abbyText.text = textShownOnScreen;
            else if (character == "T")
                travisText.text = textShownOnScreen;

            else if (character == "S")
                brazilText.text = textShownOnScreen;


            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }



    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;
        setAnimation(character, text);
        if (character == "A")
        {
            abby.enabled = true;
            travis.enabled = false;
            brazil.enabled = false;
        }

        else if (character == "T")
        {
            abby.enabled = false;
            travis.enabled = true;
            brazil.enabled = false;
        }
        else if (character == "S")
        {
            abby.enabled = false;
            travis.enabled = false;
            brazil.enabled = true;
        }
        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }


    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setTexts(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }

    private void setTexts(string character, string text)
    {
        if (character == "A")
            abbyText.text = text;
        else if (character == "T")
            travisText.text = text;
        else if (character == "S")
            brazilText.text = text;
    }

    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }


    private void setAnimation(string character, string text)
    {

        if (character == "A")
        {
            if (level == "Scene16.3")
            {

                abbySkeleton.AnimationName = "16.3 class room";
                soritySkeleton.AnimationName = "16.3 idle";
                travisSkeleton.AnimationName = "16.3 idle";
            }
        }

        else if (character == "T")
        {
            if (level == "Scene16.3")
            {
                abbySkeleton.AnimationName = "16.3 class room idle";
                soritySkeleton.AnimationName = "16.3 idle";
                travisSkeleton.AnimationName = "16.3 dialogue";
            }
        }
        else if (character == "S")
        {
            if (level == "Scene16.3")
            {
                abbySkeleton.AnimationName = "16.3 class room idle";
                soritySkeleton.AnimationName = "16.3 dialogue";
                travisSkeleton.AnimationName = "16.3 idle";
            }
        }
    }
}
