﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSixteen_Eight : MonoBehaviour
{

    public Text abbyText;
    public Text travisText;
    public Text sheeplyText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby;
    public Image brazil;
    public Image travis;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    String option = "";
    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation travisSkeleton;
    public SkeletonAnimation sheeplySkeleton;
    public SkeletonAnimation americaSkeleton;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    public Camera mainCamera;
    float pause = 1;
    public Button button;

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        abby.enabled = false;
        travis.enabled = false;
        brazil.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;

        if (level == "Scene16.8")
        {
            if (timeElapsed > 2.0 && timeElapsed < 5.0)
            {
                americaSkeleton.loop = true;
                sheeplySkeleton.loop = true;

                float wordscount = (timeElapsed - (float)2.0) * wordsPerSecond;
                conversation("B", "Don’t tell me you guys are dating.", wordscount);
                pause = 5.1f;
            }


            else if (timeElapsed > 5.0 && timeElapsed < 6.5)
            {
                clearText();
                brazil.enabled = false;
                zoomIn(2.07f, new Vector3(0.3f, 0.4f, -10f));
                pause = 6.6f;
            }

            else if (timeElapsed > 6.5 && timeElapsed < 8.0)
            {
                zoomOut();
                pause = 8.1f;
            }

            else if (timeElapsed > 8.0 && timeElapsed < 9.7)
            {
                float wordscount = (timeElapsed - (float)8.2) * wordsPerSecond;
                conversation("T", "No", wordscount);
                pause = 9.7f;
            }
            else if (timeElapsed > 9.7 && timeElapsed < 9.9)
            {
                clearText();

            }
            else if (timeElapsed > 9.9 && timeElapsed < 11.5)
            {
                float wordscount = (timeElapsed - (float)9.9) * wordsPerSecond;
                conversation("A", "No", wordscount);
                pause = 11.6f;
            }
            else if (timeElapsed > 11.5)
            {
                clearText();

                nextScene();
            }


        }


        }

    public void nextScene()
    {
        level = "Scene17.1";
        SceneManager.LoadScene(level);
    }

    private void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
        travisText.text = textShownOnScreen;
        sheeplyText.text = textShownOnScreen;
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "A")
                abbyText.text = textShownOnScreen;
            else if (character == "T")
                travisText.text = textShownOnScreen;

            else if (character == "B")
                sheeplyText.text = textShownOnScreen;


            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }



    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;
        setAnimation(character, text);
        if (character == "A")
        {
            abby.enabled = true;
            travis.enabled = false;
            brazil.enabled = false;
        }

        else if (character == "T")
        {
            abby.enabled = false;
            travis.enabled = true;
            brazil.enabled = false;
        }
        else if (character == "B")
        {
            abby.enabled = false;
            travis.enabled = false;
            brazil.enabled = true;
        }
        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }


    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setTexts(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }

    private void setTexts(string character, string text)
    {
        if (character == "A")
            abbyText.text = text;
        else if (character == "T")
            travisText.text = text;
        else if (character == "B")
            sheeplyText.text = text;
    }

    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }


    private void setAnimation(string character, string text)
    {

        if (character == "A")
        {
            if (level == "Scene16.8")
            {

                abbySkeleton.AnimationName = "16.8";
                sheeplySkeleton.AnimationName = "7.6 idle";
                americaSkeleton.AnimationName = "7.6 idle";
                travisSkeleton.AnimationName = "16.8 idle";
            }
        }

        else if (character == "T")
        {
            if (level == "Scene16.8")
            {
                abbySkeleton.AnimationName = "16.8";
                sheeplySkeleton.AnimationName = "7.6 idle";
                americaSkeleton.AnimationName = "7.6 idle";
                travisSkeleton.AnimationName = "16.8 dialogue";
            }
        }
        else if (character == "B")
        {
            if (level == "Scene16.8")
            {
                abbySkeleton.AnimationName = "16.8";
                sheeplySkeleton.AnimationName = "7.6 idle";
                americaSkeleton.AnimationName = "7.6 idle";
                travisSkeleton.AnimationName = "16.8 idle";
            }
        }
    }
}
