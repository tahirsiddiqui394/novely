﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSixteen_two : MonoBehaviour
{

    public Text abbyText;
    public Text travisText;
    public Text brazilText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby;
    public Image brazil;
    public Image travis;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    String option = "";
    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation travisSkeleton;
    public SkeletonAnimation brazilsSkeleton;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    public Camera mainCamera;
    float pause = 1;
    public Button button;

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        abby.enabled = false;
        travis.enabled = false;
        brazil.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;

        if (level == "Scene16.2")
        {
            if (timeElapsed > 3.0 && timeElapsed < 5.0)
            {
                zoomIn(2.07f, new Vector3(0.3f, 0.4f, -10f));
                pause = 5.1f;
            }

            else if (timeElapsed > 5.0 && timeElapsed < 7.0)
            {
                float wordscount = (timeElapsed - (float)5.0) * wordsPerSecond;
                conversation("B", "Are you guys dating or what?", wordscount);
                pause = 7.1f;
            }

            else if (timeElapsed > 7.0 && timeElapsed < 8.2)
            {
                clearText();
                brazil.enabled = false;
                zoomOut();

            }
            else if (timeElapsed > 8.2 && timeElapsed < 9.7)
            {
                float wordscount = (timeElapsed - (float)8.2) * wordsPerSecond;
                conversation("T", "No", wordscount);
                pause = 9.7f;
            }
            else if (timeElapsed > 9.7 && timeElapsed < 9.9)
            {
                clearText();

            }
            else if (timeElapsed > 9.9 && timeElapsed < 11.5)
            {
                float wordscount = (timeElapsed - (float)9.9) * wordsPerSecond;
                conversation("A", "No", wordscount);
                pause = 11.6f;
            }
            else if (timeElapsed > 11.5)
            {
                clearText();

                nextScene();
            }


        }
        else if (level == "Scene16.7")
        {
            if (timeElapsed > 1.3 && timeElapsed < 8.6)
            {
                brazilsSkeleton.AnimationName = "16.7 idle";
                float wordscount = (timeElapsed - (float)1.3) * wordsPerSecond;
                conversation("B", "Travis, are you nailing her? Tell me you’re nailing her. Surely she didn’t tame Eastern’s walking one-night stand? ", wordscount);
                pause = 8.6f;
            }

            else if (timeElapsed > 8.5)
            {
                clearText();
                nextScene();
            }
        }

        }

    public void nextScene()
    {
        if (level == "Scene16.7")
            level = "Scene16.8";
        else
            level = "Scene16.3";
        SceneManager.LoadScene(level);
    }

    private void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
        travisText.text = textShownOnScreen;
        brazilText.text = textShownOnScreen;
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "A")
                abbyText.text = textShownOnScreen;
            else if (character == "T")
                travisText.text = textShownOnScreen;

            else if (character == "B")
                brazilText.text = textShownOnScreen;


            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }



    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;
        setAnimation(character, text);
        if (character == "A")
        {
            abby.enabled = true;
            travis.enabled = false;
            brazil.enabled = false;
        }

        else if (character == "T")
        {
            abby.enabled = false;
            travis.enabled = true;
            brazil.enabled = false;
        }
        else if (character == "B")
        {
            abby.enabled = false;
            travis.enabled = false;
            brazil.enabled = true;
        }
        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }


    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setTexts(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }

    private void setTexts(string character, string text)
    {
        if (character == "A")
            abbyText.text = text;
        else if (character == "T")
            travisText.text = text;
        else if (character == "B")
            brazilText.text = text;
    }

    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }


    private void setAnimation(string character, string text)
    {

        if (character == "A")
        {
            if (level == "Scene16.2")
            {

                abbySkeleton.AnimationName = "16.2 cafeteria";
                brazilsSkeleton.AnimationName = "16.2 idle";
                travisSkeleton.AnimationName = "16.3 idle";
            }
        }

        else if (character == "T")
        {
            if (level == "Scene16.2")
            {
                abbySkeleton.AnimationName = "16.2 cafeteria idle";
                brazilsSkeleton.AnimationName = "16.2 idle";
                travisSkeleton.AnimationName = "16.3 dialogue";
            }
        }
        else if (character == "B")
        {
            if (level == "Scene16.2")
            {
                abbySkeleton.AnimationName = "16.2 cafeteria idle";
                brazilsSkeleton.AnimationName = "16.2";
                travisSkeleton.AnimationName = "16.3 idle";
            }
        }
    }
}
