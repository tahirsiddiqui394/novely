
Travis2.png
size: 1596,779
format: RGBA8888
filter: Linear,Linear
repeat: none
Head
  rotate: true
  xy: 1038, 211
  size: 154, 184
  orig: 154, 184
  offset: 0, 0
  index: -1
L arm
  rotate: true
  xy: 2, 77
  size: 179, 520
  orig: 179, 520
  offset: 0, 0
  index: -1
L arm_Shirt
  rotate: false
  xy: 946, 17
  size: 148, 181
  orig: 148, 181
  offset: 0, 0
  index: -1
L boot
  rotate: false
  xy: 524, 26
  size: 221, 172
  orig: 221, 172
  offset: 0, 0
  index: -1
R boot
  rotate: false
  xy: 524, 26
  size: 221, 172
  orig: 221, 172
  offset: 0, 0
  index: -1
L eye
  rotate: false
  xy: 1556, 79
  size: 27, 16
  orig: 27, 16
  offset: 0, 0
  index: -1
L eyebrow
  rotate: false
  xy: 125, 2
  size: 30, 10
  orig: 30, 10
  offset: 0, 0
  index: -1
L leg
  rotate: true
  xy: 510, 367
  size: 163, 712
  orig: 163, 712
  offset: 0, 0
  index: -1
L pupil
  rotate: false
  xy: 157, 2
  size: 11, 10
  orig: 11, 10
  offset: 0, 0
  index: -1
L wrist
  rotate: true
  xy: 125, 14
  size: 61, 121
  orig: 61, 121
  offset: 0, 0
  index: -1
L wrist 2
  rotate: true
  xy: 2, 3
  size: 72, 121
  orig: 72, 121
  offset: 0, 0
  index: -1
Lip1
  rotate: true
  xy: 510, 326
  size: 39, 11
  orig: 39, 11
  offset: 0, 0
  index: -1
Lip2
  rotate: false
  xy: 248, 56
  size: 39, 19
  orig: 39, 19
  offset: 0, 0
  index: -1
Lip3
  rotate: false
  xy: 747, 26
  size: 35, 11
  orig: 35, 11
  offset: 0, 0
  index: -1
Lip4
  rotate: false
  xy: 1510, 56
  size: 41, 14
  orig: 41, 14
  offset: 0, 0
  index: -1
Lip5
  rotate: true
  xy: 1194, 170
  size: 39, 18
  orig: 39, 18
  offset: 0, 0
  index: -1
Lip6
  rotate: false
  xy: 248, 37
  size: 37, 17
  orig: 37, 17
  offset: 0, 0
  index: -1
R arm
  rotate: true
  xy: 524, 200
  size: 165, 512
  orig: 165, 512
  offset: 0, 0
  index: -1
R eye
  rotate: false
  xy: 289, 56
  size: 35, 19
  orig: 35, 19
  offset: 0, 0
  index: -1
R eyebriw
  rotate: false
  xy: 1096, 95
  size: 43, 15
  orig: 43, 15
  offset: 0, 0
  index: -1
R leg
  rotate: true
  xy: 510, 532
  size: 245, 716
  orig: 245, 716
  offset: 0, 0
  index: -1
R pupil
  rotate: false
  xy: 1096, 82
  size: 13, 11
  orig: 13, 11
  offset: 0, 0
  index: -1
R wrist
  rotate: false
  xy: 1510, 97
  size: 80, 149
  orig: 80, 149
  offset: 0, 0
  index: -1
R wrist 2
  rotate: false
  xy: 1096, 112
  size: 96, 97
  orig: 96, 97
  offset: 0, 0
  index: -1
R-arm shirt
  rotate: true
  xy: 747, 39
  size: 159, 197
  orig: 159, 197
  offset: 0, 0
  index: -1
Torso
  rotate: false
  xy: 1228, 248
  size: 366, 529
  orig: 366, 529
  offset: 0, 0
  index: -1
Tshirt-Torso
  rotate: false
  xy: 2, 258
  size: 506, 519
  orig: 506, 519
  offset: 0, 0
  index: -1
hip
  rotate: false
  xy: 1224, 12
  size: 284, 234
  orig: 284, 234
  offset: 0, 0
  index: -1
lip default
  rotate: false
  xy: 1510, 72
  size: 44, 23
  orig: 44, 23
  offset: 0, 0
  index: -1
