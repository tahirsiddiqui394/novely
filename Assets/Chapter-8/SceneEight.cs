﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneEight : MonoBehaviour {

    public Camera mainCamera;
    public Text abbyText;
    public Text travisText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby;
    public Image travis;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation travisSkeleton;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    float pause = 1;

    // Use this for initialization
    void Start() {
        Screen.orientation = ScreenOrientation.Portrait;

        if (level == "Scene8.1")
        {
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "8.1 dirty dress";
            else
                abbySkeleton.AnimationName = "8.1 clean dress";
        }
        else if (level == "Scene8.3")
        {
            abby.enabled = false;
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "8.1 dirty dress";
            else
                abbySkeleton.AnimationName = "8.1 clean dress";
        }
        else if (level == "Scene8.5")
        {
            abby.enabled = false;
            travis.enabled = false;
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "8.1 dirty dress";
            else
                abbySkeleton.AnimationName = "8.1 clean dress";

        }

        else if (level == "Scene8.6")
        {
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "8.5 on bike dirty";
            else
                abbySkeleton.AnimationName = "8.5 on bike clean";

        } 


        }
	
	// Update is called once per frame
	void Update () {

        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;
        if (level == "Scene8.1")
        {
            if (timeElapsed > 2.0)
            {
                level = "Scene8.3";
                SceneManager.LoadScene(level);
            }
        }

        else if (level == "Scene8.6")
        {

            if (timeElapsed >= 0.5 && timeElapsed < 1.5)
            {
                zoomIn(2.52f, new Vector3(0.05f, 0f, -10f));
            }
            else if (timeElapsed >= 1.5 && timeElapsed > 2.0)
            {
                nextScene();
            }
        }


        else if (level == "Scene8.3")
        {
            if (timeElapsed >= 0.5 && timeElapsed < 1.5)
            {
                zoomIn(1.94f ,new Vector3(1.44f,0.19f,-10f));
            }
            else if (timeElapsed >= 1.5 && timeElapsed < 2.5)
            {
                float wordscount = (timeElapsed - (float)1.5) * wordsPerSecond;
                conversation("A", "Uh...", wordscount);
                pause = 2.1f;
            }

            if (timeElapsed >=2.5 && timeElapsed < 3.5)
            {
                zoomOut();
            }

            else if (timeElapsed > 3.0)
            {
                clearText();
                abby.enabled = false;
                level = "Scene8.5";
                SceneManager.LoadScene(level);
            }
        }

        else if (level == "Scene8.5")
        {

            if (timeElapsed < 3.0)
            {
                float wordscount = timeElapsed * wordsPerSecond;
                conversation("T", "Oh, get on. I’ll go slow.", wordscount);
                pause = 3.1f;
            }
            else if (timeElapsed > 3 && timeElapsed < 5.5)
            {
                zoomIn(2.99f,new Vector3(0.52f,0f,-10f));
                clearText();
                if(SceneSix.isDirty)
                    abbySkeleton.AnimationName = "8.2  dirty dress";
                else
                    abbySkeleton.AnimationName = "8.2  clean dress";
                abbySkeleton.loop = false;
                travis.enabled = false;

            }
            else if (timeElapsed >= 5.5 && timeElapsed < 7.0)
            {

                float wordscount = (timeElapsed - (float)5.5) * wordsPerSecond;
                abbySkeleton.loop = true;
                conversation("A", "What does that say?", wordscount);
                pause = 7.1f;
            }
            else if (timeElapsed > 7.0 && timeElapsed < 7.2)
            {
                clearText();
            }
            else if (timeElapsed >= 7.2 && timeElapsed < 14.0)
            {
                float wordscount = (timeElapsed - (float)7.2) * wordsPerSecond;
                conversation("T", "It’s a Harley Night Rod. She’s the love of my life, so don’t scratch the paint when you get on.", wordscount);
                pause = 14.1f;
            }
            else if (timeElapsed > 14.0 && timeElapsed < 14.2)
            {
                clearText();
            }
            else if (timeElapsed >= 14.2 && timeElapsed < 17.0)
            {
                float wordscount = (timeElapsed - (float)14.2) * wordsPerSecond;
                conversation("A", " I’m wearing flip flops!", wordscount);
                pause = 17.1f;
            }
            else if (timeElapsed > 17.0 && timeElapsed < 17.2)
            {
                clearText();
            }
            else if (timeElapsed >= 17.2 && timeElapsed < 20.0)
            {
                float wordscount = (timeElapsed - (float)17.2) * wordsPerSecond;
                conversation("T", "I’m wearing boots. Get on.", wordscount);
                pause = 20.1f;
            }
            else if (timeElapsed > 20.0 && timeElapsed < 20.2)
            {
                clearText();
            }
            else if (timeElapsed >= 20.2 && timeElapsed < 25.5)
            {
                float wordscount = (timeElapsed - (float)20.2) * wordsPerSecond;
                conversation("T", "There’s nothing to hold on to but me, Pidge.Don’t let go.", wordscount);
                pause = 25.1f;
            }
            else if (timeElapsed > 25.5)
            {

                clearText();
                travis.enabled = false;
                level = "Scene8.6";
                SceneManager.LoadScene(level);
            }
        }

    }

    public void nextScene()
    {
        level = "Scene9.1";
        SceneManager.LoadScene(level);
    }

     void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
        if(travisText !=null)
        travisText.text = textShownOnScreen;
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "A")
                abbyText.text = textShownOnScreen;
            else if (character == "T")
                travisText.text = textShownOnScreen;

            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }


    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;
        setAnimation(character, text);
        if (character == "A")
        {
            abby.enabled = true;
            if(travis != null)
            travis.enabled = false;
        }

        else if (character == "T")
        {
            abby.enabled = false;
            travis.enabled = true;
        }
        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private void setAnimation(string character, string text)
    {
        if (level == "Scene8.3")
        {
            //abbySkeleton.AnimationName = "8.5 dialogue clean dress";
        }
            else if (level == "Scene8.5") {
            if (character == "A")
            {
                travisSkeleton.AnimationName = "8.3,8.4,8.5 idle";
                    
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "8.5 dialogue dirty dress";
                else
                    abbySkeleton.AnimationName = "8.5 dialogue clean dress";

                abbySkeleton.loop = false;

            }

        else if (character == "T")
        {
                travisSkeleton.AnimationName = "8.3,8.5 dialogue";

            }
    }
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }


    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setText(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }




    private void setText(string character, string text)
    {
        if (character == "A")
            abbyText.text = text;
        else if (character == "T")
            travisText.text = text;
    }

    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }
}
