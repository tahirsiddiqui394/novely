﻿using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SceneSeventeen_eight : MonoBehaviour {

    public Camera mainCamera;
    public Text abbyText;
    public Text americaText;
    public Text sheplyText;
    public Text optAText;
    public Text optBText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby;
    public Image america;
    public Image sheply;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation americaSkeleton;
    public SkeletonAnimation sheeplySkeleton;
    public Boolean isOptionSelected;
    public Canvas buttonCanvas;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    float pause = 1;
    bool isOptA;
    string optionText = "";

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        abby.enabled = false;
        america.enabled = false;
        sheply.enabled = false;
        buttonCanvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;
    if (level == "Scene17.8")
        {
            if (isOptionSelected)
            {
                if (isOptA)
                {

                    if (timeElapsed < 5.0)
                    {
                        float wordscount = timeElapsed * wordsPerSecond;
                        conversation("S", "Seriously. It gets old after a while. He’s nicer than I would be.", wordscount);
                        pause = 5.1f;
                    }
                    else if (timeElapsed > 5.0 && timeElapsed < 5.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed > 5.2 && timeElapsed < 9.0)
                    {
                        float wordscount = (timeElapsed - (float)5.2) * wordsPerSecond;
                        conversation("Am", "Like you wouldn’t love it.", wordscount);
                        pause = 9.1f;
                    }
                    else if (timeElapsed > 9.0 && timeElapsed < 9.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed > 9.2 && timeElapsed < 15.0)
                    {
                        float wordscount = (timeElapsed - (float)9.2) * wordsPerSecond;
                        conversation("S", "Being called an asshole whether I sleep with someone or not? No thanks.", wordscount);
                        pause = 15.1f;
                    }
                    else if (timeElapsed > 15.0 && timeElapsed < 15.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed > 15.2 && timeElapsed < 18.0)
                    {
                        float wordscount = (timeElapsed - (float)15.2) * wordsPerSecond;
                        conversation("A", "I doubt it bothers him.", wordscount);
                        pause = 18.1f;
                    }

                    else if (timeElapsed > 18.0 && timeElapsed < 18.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed > 18.2 && timeElapsed < 23.0)
                    {
                        float wordscount = (timeElapsed - (float)18.2) * wordsPerSecond;
                        conversation("A", "What? Why are you making that face?", wordscount);
                        pause = 23.1f;
                    }
                    else if (timeElapsed > 23.0 && timeElapsed < 23.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed > 23.2 && timeElapsed < 29.0)
                    {
                        float wordscount = (timeElapsed - (float)23.2) * wordsPerSecond;
                        conversation("S", "I just figured you’d know him better than that by now.", wordscount);
                        pause = 29.1f;
                    }

                    else if (timeElapsed > 29.0 && timeElapsed < 29.2)
                    {
                        sheply.enabled = false;
                        optAText.text = "Why? Because we’ve went bowling a few times and we have in depth talks about biology?";
                        optBText.text = "I need to stop spending so much time with Travis.";
                        buttonCanvas.enabled = true;
                        clearText();
                    }
                }
                else
                {
                    if (timeElapsed < 10.0)
                    {
                        float wordscount = timeElapsed * wordsPerSecond;
                        conversation("S", "Kind of. You think it’s flattering until you’ve got someone to be or you’re trying to have a conversation", wordscount);
                        pause = 10.1f;
                    }
                    else if (timeElapsed > 10.0 && timeElapsed < 10.2)
                    {
                        clearText();
                    }

                    else if (timeElapsed > 10.2 && timeElapsed < 20.0)
                    {
                        float wordscount = (timeElapsed - (float)10.2) * wordsPerSecond;
                        conversation("S", "with someone, and a strange girl comes up giggling for no reason because she thinks it’s cute.", wordscount);
                        pause = 20.1f;
                    }
                    else if (timeElapsed > 20.0 && timeElapsed < 20.2)
                    {
                        clearText();
                    }


                    else if (timeElapsed > 20.2 && timeElapsed < 25.0)
                    {
                        float wordscount = (timeElapsed - (float)20.2) * wordsPerSecond;
                        conversation("Am", "Like you wouldn’t love it.", wordscount);
                        pause = 26f;
                    }
                    else if (timeElapsed > 26.0 && timeElapsed < 26.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed > 26.2 && timeElapsed < 33.0)
                    {
                        float wordscount = (timeElapsed - (float)26.2) * wordsPerSecond;
                        conversation("S", "Being called an asshole whether I sleep with someone or not? No thanks.", wordscount);
                        pause = 33.1f;
                    }
                    else if (timeElapsed > 33.0 && timeElapsed < 33.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed > 33.2 && timeElapsed < 36.0)
                    {
                        float wordscount = (timeElapsed - (float)33.2) * wordsPerSecond;
                        conversation("A", "I doubt it bothers him.", wordscount);
                        pause = 36.1f;
                    }

                    else if (timeElapsed > 36.0 && timeElapsed < 36.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed > 36.2 && timeElapsed <40.0)
                    {
                        float wordscount = (timeElapsed - (float)36.2) * wordsPerSecond;
                        conversation("A", "What? Why are you making that face?", wordscount);
                        pause = 40.1f;
                    }
                    else if (timeElapsed > 40.0 && timeElapsed < 40.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed > 40.2 && timeElapsed < 46.0)
                    {
                        float wordscount = (timeElapsed - (float)40.2) * wordsPerSecond;
                        conversation("S", "I just figured you’d know him better than that by now.", wordscount);
                        pause = 46.1f;
                    }

                    else if (timeElapsed > 46.0)
                    {
                        sheply.enabled = false;
                        optAText.text = "Why? Because we’ve went bowling a few times and we have in depth talks about biology?";
                        optBText.text = "I need to stop spending so much time with Travis.";
                        buttonCanvas.enabled = true;
                        clearText();
                    }
                }
            }
            else
            {
                if (timeElapsed < 4.0)
                {
                    float wordscount = timeElapsed * wordsPerSecond;
                    conversation("Am", "They’re so obvious. Look at the red head.", wordscount);
                    pause = 4.1f;
                }
                else if (timeElapsed > 4.0 && timeElapsed < 4.2)
                {
                    clearText();
                }
                else if (timeElapsed > 4.2 && timeElapsed < 9.0)
                {
                    float wordscount = (timeElapsed - (float)4.2) * wordsPerSecond;
                    conversation("Am", "She’s ran her fingers through her hair as many times as she’s blinked.", wordscount);
                    pause = 9.1f;
                }
                else if (timeElapsed > 9.0 && timeElapsed < 9.2)
                {
                    clearText();
                }
                else if (timeElapsed > 9.2 && timeElapsed < 13.0)
                {
                    float wordscount = (timeElapsed - (float)9.2) * wordsPerSecond;
                    conversation("Am", "I wonder if Travis gets tired of that.", wordscount);
                    pause = 13.1f;
                }
                else if (timeElapsed > 13.0 && timeElapsed < 13.2)
                {
                    clearText();
                }
                else if (timeElapsed > 13.2 && timeElapsed < 20.0)
                {
                    float wordscount = (timeElapsed - (float)13.2) * wordsPerSecond;
                    conversation("S", "He does. Everyone thinks he’s an asshole, but if they only knew how much patience", wordscount);
                    pause = 20.1f;
                }

                else if (timeElapsed > 20.0 && timeElapsed < 20.2)
                {
                    clearText();
                }

                else if (timeElapsed > 20.2 && timeElapsed < 27.0)
                {
                    float wordscount = (timeElapsed - (float)20.2) * wordsPerSecond;
                    conversation("S", "he shows dealing with every girl who wants to prove she can tame him.", wordscount);
                    pause = 27.1f;
                }
                else if (timeElapsed > 27.0 && timeElapsed < 27.2)
                {
                    clearText();
                }
                else if (timeElapsed > 27.2 && timeElapsed < 32.0)
                {
                    float wordscount = (timeElapsed - (float)27.2) * wordsPerSecond;
                    conversation("S", "He can’t go anywhere without being bugged.", wordscount);
                    pause = 32.1f;
                }

                else if (timeElapsed > 32.0 )
                {
                    sheply.enabled = false;
                    optAText.text = "First world problems.";
                    optBText.text = "Are you saying Travis is harassed?";
                    buttonCanvas.enabled = true;
                    clearText();
                }



            }
        }
        
    }


    public void nextScene()
    {
        level = "Scene17.9";
        SceneManager.LoadScene(level);
    }

    private void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
            americaText.text = textShownOnScreen;
            sheplyText.text = textShownOnScreen;
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "A")
                abbyText.text = textShownOnScreen;
            else if (character == "Am")
                americaText.text = textShownOnScreen;
            else if (character == "S")
                sheplyText.text = textShownOnScreen;
            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }


    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;

        setAnimation(character, text);
        if (character == "A")
        {
            abby.enabled = true;
            america.enabled = false;
            sheply.enabled = false;
        }
        else if (character == "Am")
        {
            abby.enabled = false;
            america.enabled = true;
            sheply.enabled = false;
        }
        else if (character == "S")
        {
            abby.enabled = false;
            america.enabled = false;
            sheply.enabled = true;
        }
        else if (character == "T")
        {
            abby.enabled = false;
            america.enabled = false;
            sheply.enabled = false;
        }
        else if (character == "B")
        {
            abby.enabled = false;
            america.enabled = false;
            sheply.enabled = false;
        }
        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private void setAnimation(string character, string text)
    {
        if (character == "A")
        {
            abbySkeleton.AnimationName = "17 dialogue";
            americaSkeleton.AnimationName = "17.1, 17.8  idle";
            sheeplySkeleton.AnimationName = "17.1 idle";
        }
        else if (character == "Am")
        {
            abbySkeleton.AnimationName = "17 idle";
            americaSkeleton.AnimationName = "17.7, 17.8 dialogue";
            sheeplySkeleton.AnimationName = "17.1 idle";
        }
        else if (character == "S")
        {
            abbySkeleton.AnimationName = "17 idle";
            americaSkeleton.AnimationName = "17.1, 17.8  idle";
            sheeplySkeleton.AnimationName = "17.1 dialogue";
        }
        else if (character == "T")
        {
            abbySkeleton.AnimationName = "17 idle";
            americaSkeleton.AnimationName = "17.1, 17.8  idle";
            sheeplySkeleton.AnimationName = "17.1 idle";
        }

        else if (character == "B")
        {
            abbySkeleton.AnimationName = "17 idle";
            americaSkeleton.AnimationName = "17.1, 17.8  idle";
            sheeplySkeleton.AnimationName = "17.1 idle";
        }
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }

    public void clickedA()
    {
        if (isOptionSelected)
            nextScene();
        else
        {
            isOptA = true;
            timeElapsed = 0;
            buttonCanvas.enabled = false;
            isOptionSelected = true;
        }
       
    }

    public void clickedB()
    {
        if (isOptionSelected)
            nextScene();
        else
        {
            isOptA = false;
            timeElapsed = 0;
            buttonCanvas.enabled = false;
            isOptionSelected = true;
        }
      
    }

    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setText(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }

    private void setText(string character, string text)
    {
        if (character == "A")
            abbyText.text = text;
        else if (character == "Am")
            americaText.text = text;
        else if (character == "S")
            sheplyText.text = text;
    }


    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }

}

