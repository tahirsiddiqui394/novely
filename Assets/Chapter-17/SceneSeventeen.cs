﻿using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SceneSeventeen : MonoBehaviour {

    public Camera mainCamera;
    public Text abbyText;
    public Text americaText;
    public Text sheplyText;
    public Text travisText;
    public Text brazilText;
    public Text optAText;
    public Text optBText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby;
    public Image america;
    public Image sheply;
    public Image travis;
    public Image brazil;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation americaSkeleton;
    public SkeletonAnimation sheeplySkeleton;
    public SkeletonAnimation travisSkeleton;
    public SkeletonAnimation brazilSkeleton;
    public Boolean isOptionSelected;
    public Canvas buttonCanvas;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    float pause = 1;

    string optionText = "";

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        travis.enabled = false;
        abby.enabled = false;
        america.enabled = false;
        sheply.enabled = false;
        brazil.enabled = false;
        buttonCanvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;
    if (level == "Scene17.1")
        {
            if (isOptionSelected)
            {
                if (timeElapsed < 4.0)
                {
                    float wordscount = timeElapsed * wordsPerSecond;
                    conversation("T",optionText, wordscount);
                    pause = 4.1f;
                }
                else if (timeElapsed > 4.0 && timeElapsed < 4.2)
                {
                    clearText();
                }
                else if (timeElapsed > 4.2 && timeElapsed < 7.0)
                {
                    float wordscount = (timeElapsed - (float)4.2) * wordsPerSecond;
                    conversation("B", "Did she turn you into a cabana boy, Travis? What’s next?", wordscount);
                    pause = 7.1f;
                }
                else if (timeElapsed > 7.0 && timeElapsed < 7.2)
                {
                    clearText();
                }
                else if (timeElapsed > 7.2 && timeElapsed < 12.0)
                {
                    float wordscount = (timeElapsed - (float)7.2) * wordsPerSecond;
                    conversation("B", "Fanning her with a palm tree leaf, wearing a speedo?", wordscount);
                    pause = 12.1f;
                }
                else if (timeElapsed > 12.0 && timeElapsed < 12.2)
                {
                    clearText();
                }
                else if (timeElapsed > 12.2 && timeElapsed < 16.0)
                {
                    float wordscount = (timeElapsed - (float)12.2) * wordsPerSecond;
                    conversation("A", "You couldn’t fill a Speedo, Brazil.Shut the hell up.", wordscount);
                    pause = 16.1f;
                }

                else if (timeElapsed > 16.0 && timeElapsed < 16.2)
                {
                    clearText();
                }
                else if (timeElapsed > 16.2 && timeElapsed < 20.0)
                {
                    float wordscount = (timeElapsed - (float)16.2) * wordsPerSecond;
                    conversation("B", "Easy, Abby! I was kidding!", wordscount);
                    pause = 20.1f;
                }
                else if (timeElapsed > 20.0 && timeElapsed < 20.2)
                {
                    clearText();
                }
                else if (timeElapsed > 20.2 && timeElapsed < 26.0)
                {
                    float wordscount = (timeElapsed - (float)20.2) * wordsPerSecond;
                    conversation("A", "Just... don’t talk to him like that.", wordscount);
                    pause = 26.1f;
                }

                else if (timeElapsed > 26.0 && timeElapsed < 26.2)
                {
                    clearText();
                }
                else if (timeElapsed > 26.2 && timeElapsed < 32.0)
                {
                    float wordscount = (timeElapsed - (float)26.2) * wordsPerSecond;
                    conversation("T", "Now I’ve seen it all. I was just defended by a girl.", wordscount);
                    pause = 32.1f;
                }
                else if (timeElapsed > 32.0 && timeElapsed < 32.2)
                {
                    clearText();
                }
                else if (timeElapsed > 32.2 && timeElapsed < 36.0)
                {
                    float wordscount = (timeElapsed - (float)32.2) * wordsPerSecond;
                    conversation("S", "That’s only because you don’t have sisters.", wordscount);
                    pause = 36.1f;
                }
                else if (timeElapsed > 36.0 && timeElapsed <36.2)
                {
                    clearText();
                }

                else if (timeElapsed > 36.2 && timeElapsed < 40.0)
                {
                    float wordscount = (timeElapsed - (float)36.2) * wordsPerSecond;
                    conversation("S", "Can you imagine a Maddox girl? Frightening.", wordscount);
                    pause = 40.1f;
                }
                else if (timeElapsed > 40.0 && timeElapsed < 40.2)
                {
                    clearText();
                }
                else if (timeElapsed > 40.2 && timeElapsed < 43.0)
                {
                    float wordscount = (timeElapsed - (float)40.2) * wordsPerSecond;
                    conversation("T", "I need a smoke.", wordscount);
                    pause = 43.1f;
                }
                else if (timeElapsed > 43.0 && timeElapsed < 44.2)
                {
                    clearText();
                    travis.enabled = false;
                    travisSkeleton.AnimationName = "17.5 exit";

                }
                else if (timeElapsed > 46.0 )
                {
                    travisSkeleton.loop = false;
                    travisSkeleton.timeScale = 0;
                    nextScene();
                }

            }
            else
            {
                Debug.Log(timeElapsed);
                if (timeElapsed > 1 && timeElapsed < 2.5)
                {
                    travisSkeleton.loop = true;
                    travisSkeleton.timeScale = 1;
                    pause = 2.6f;
                }
                else if (timeElapsed > 3.9 && timeElapsed < 4.0)
                {
                    travisSkeleton.AnimationName = "17.2 idle";
                    clearText();
                }

                else if (timeElapsed > 4.8)
                {
                    buttonCanvas.enabled = true;
                }
            }
        }
        
    }


    public void nextScene()
    {
        level = "Scene17.6";
        SceneManager.LoadScene(level);
    }

    private void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
            americaText.text = textShownOnScreen;
            sheplyText.text = textShownOnScreen;
            travisText.text = textShownOnScreen;
        brazilText.text = textShownOnScreen;
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "A")
                abbyText.text = textShownOnScreen;
            else if (character == "Am")
                americaText.text = textShownOnScreen;
            else if (character == "S")
                sheplyText.text = textShownOnScreen;
            else if (character == "T")
                travisText.text = textShownOnScreen;
            else if (character == "B")
                brazilText.text = textShownOnScreen;
            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }


    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;

        setAnimation(character, text);
        if (character == "A")
        {
            abby.enabled = true;
            travis.enabled = false;
            america.enabled = false;
            sheply.enabled = false;
            brazil.enabled = false;
        }
        else if (character == "Am")
        {
            abby.enabled = false;
            travis.enabled = false;
            america.enabled = true;
            sheply.enabled = false;
            brazil.enabled = false;
        }
        else if (character == "S")
        {
            abby.enabled = false;
            travis.enabled = false;
            america.enabled = false;
            sheply.enabled = true;
            brazil.enabled = false;
        }
        else if (character == "T")
        {
            abby.enabled = false;
            travis.enabled = true;
            america.enabled = false;
            sheply.enabled = false;
            brazil.enabled = false;
        }
        else if (character == "B")
        {
            abby.enabled = false;
            travis.enabled = false;
            america.enabled = false;
            sheply.enabled = false;
            brazil.enabled = true;
        }
        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private void setAnimation(string character, string text)
    {
        if (character == "A")
        {
            abbySkeleton.AnimationName = "17 dialogue";
            americaSkeleton.AnimationName = "17.1, 17.8  idle";
            travisSkeleton.AnimationName = "17.2 idle";
            sheeplySkeleton.AnimationName = "17.1 idle";
            brazilSkeleton.AnimationName = "17.1 idle";
        }
        else if (character == "Am")
        {
            abbySkeleton.AnimationName = "17 idle";
            americaSkeleton.AnimationName = "17.7, 17.8 dialogue";
            travisSkeleton.AnimationName = "17.2 idle";
            sheeplySkeleton.AnimationName = "17.1 idle";
            brazilSkeleton.AnimationName = "17.1 idle";
        }
        else if (character == "S")
        {
            abbySkeleton.AnimationName = "17 idle";
            americaSkeleton.AnimationName = "17.1, 17.8  idle";
            travisSkeleton.AnimationName = "17.2 idle";
            sheeplySkeleton.AnimationName = "17.1 dialogue";
            brazilSkeleton.AnimationName = "17.1 idle";
        }
        else if (character == "T")
        {
            abbySkeleton.AnimationName = "17 idle";
            americaSkeleton.AnimationName = "17.1, 17.8  idle";
            travisSkeleton.AnimationName = "17.2 dialogue";
            sheeplySkeleton.AnimationName = "17.1 idle";
            brazilSkeleton.AnimationName = "17.1 idle";
        }

        else if (character == "B")
        {
            abbySkeleton.AnimationName = "17 idle";
            americaSkeleton.AnimationName = "17.1, 17.8  idle";
            travisSkeleton.AnimationName = "17.2 idle";
            sheeplySkeleton.AnimationName = "17.1 idle";
            brazilSkeleton.AnimationName = "17.1 dialogue";
        }
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }

    public void clickedA()
    {
        optionText = "Well, now you don’t have to.";
        timeElapsed = 0;
        buttonCanvas.enabled = false;
       isOptionSelected = true;
       
    }

    public void clickedB()
    {
        optionText = "I know. But I wanted to.";
       timeElapsed = 0;
        buttonCanvas.enabled = false;
        isOptionSelected = true;
      
    }

    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setText(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }

    private void setText(string character, string text)
    {
        if (character == "A")
            abbyText.text = text;
        else if (character == "Am")
            americaText.text = text;
        else if (character == "S")
            sheplyText.text = text;
        else if (character == "T")
            travisText.text = text;
        else if (character == "B")
            brazilText.text = text;
    }


    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }

}

