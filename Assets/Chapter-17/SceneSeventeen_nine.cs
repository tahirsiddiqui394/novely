﻿using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SceneSeventeen_nine : MonoBehaviour {

    public Camera mainCamera;
    public Text abbyText;
    public Text TravisText;
    public Text optAText;
    public Text optBText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby;
    public Image travis;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation travisSkeleton;
    public SkeletonAnimation burneSkeleton;
    public Boolean isOptionSelected;
    public Canvas buttonCanvas;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    float pause = 1;
    bool isOptA;
    string optionText = "";

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        abby.enabled = false;
        travis.enabled = false;
        burneSkeleton.GetComponent<MeshRenderer>().sortingOrder = -1;
        buttonCanvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;
        if (level == "Scene17.9")
        {
            if (isOptionSelected)
            {
                if (isOptA)
                {
                    if (timeElapsed < 6.0)
                    {

                        float wordscount = timeElapsed * wordsPerSecond;
                        conversation("T", "We have the same class. I just thought we’d...", wordscount);
                        pause = 6.1f;
                    }
                    else if (timeElapsed > 6.0 && timeElapsed < 6.2)
                    {
                        burneSkeleton.GetComponent<MeshRenderer>().sortingOrder = 5;
                        burneSkeleton.timeScale = 1;
                        travis.enabled = false;
                        clearText();
                    }

                    else if (timeElapsed > 6.2 && timeElapsed < 10.0)
                    {
                        float wordscount = (timeElapsed - (float)6.2) * wordsPerSecond;
                        conversation("T", "I’ll catch up with you later, Pidge.", wordscount);
                        pause = 10.1f;
                    }
                    else if (timeElapsed > 10.0 && timeElapsed < 12.2)
                    {
                        burneSkeleton.GetComponent<MeshRenderer>().sortingOrder = -1;
                        burneSkeleton.timeScale = 0;
                        travisSkeleton.AnimationName = "17.11 exit";
                        travis.enabled = false;
                        clearText();
                    }

                    else if (timeElapsed > 12)
                    {
                        nextScene();
                    }

                }
                else
                {
                    if (timeElapsed < 8.0)
                    {

                        float wordscount = timeElapsed * wordsPerSecond;
                        conversation("T", "Talk about what? Us? We have the same class. Who cares if we walk there together? ", wordscount);
                        pause = 8.1f;
                    }
                    else if (timeElapsed > 8.0 && timeElapsed < 8.2)
                    {
                        travis.enabled = false;
                        clearText();
                    }

                    else if (timeElapsed > 8.2 && timeElapsed < 16.0)
                    {
                        float wordscount = (timeElapsed - (float)6.2) * wordsPerSecond;
                        conversation("A", "I do. I care. I don’t want anyone thinking I’m dating Eastern’s one-night-stand. ", wordscount);
                        pause = 16.1f;
                    }

                    else if (timeElapsed > 16.2 && timeElapsed < 20.0)
                    {

                        float wordscount = (timeElapsed - (float)16.2) * wordsPerSecond;
                        conversation("T", "I’ll catch you later, Pidge.", wordscount);
                        pause = 20.1f;
                    }

                  
                    else if (timeElapsed > 20.0 && timeElapsed <22.2)
                    {
                        travisSkeleton.AnimationName = "17.11 exit";
                        travis.enabled = false;
                        clearText();
                    }

                    else if (timeElapsed > 23)
                    {
                        nextScene();
                    }
                }
            }
            else
            {

                if (timeElapsed < 3.0)
                {
                    float wordscount = timeElapsed * wordsPerSecond;
                    conversation("T", "Pidge! Wait up! I’ll walk you.", wordscount);
                    pause = 5.1f;
                    abbySkeleton.AnimationName = "17.11 idle";
                }
                else if (timeElapsed > 4.0)
                {
                    abbySkeleton.AnimationName = "17.11 idle";
                    travisSkeleton.AnimationName = "17.11 idle";
                    travis.enabled = false;
                    buttonCanvas.enabled = true;
                    clearText();
                }
            }

        }

    }


    public void nextScene()
    {
        level = "Splash";
        SceneManager.LoadScene(level);
    }

    private void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
        TravisText.text = textShownOnScreen;
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "A")
                abbyText.text = textShownOnScreen;
            else if (character == "T")
                TravisText.text = textShownOnScreen;

            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }


    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;

        setAnimation(character, text);
        if (character == "A")
        {
            abby.enabled = true;
            travis.enabled = false;
        }

        else if (character == "T")
        {
            abby.enabled = false;
            travis.enabled = true;
        }

        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private void setAnimation(string character, string text)
    {
        if (character == "A")
        {
            abbySkeleton.AnimationName = "17.11 dialogue";
            travisSkeleton.AnimationName = "17.11 idle";
        }
       
        else if (character == "T")
        {
            abbySkeleton.AnimationName = "17.11 idle";
            if(text == "I’ll catch you later, Pidge.")
                travisSkeleton.AnimationName = "17.11 hurts";
            else
            travisSkeleton.AnimationName = "17.11 dialogue";
        }

       
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }

    public void clickedA()
    {
        if (isOptionSelected)
            nextScene();
        else
        {
            isOptA = true;
            timeElapsed = 0;
            buttonCanvas.enabled = false;
            isOptionSelected = true;
        }

    }

    public void clickedB()
    {
        if (isOptionSelected)
            nextScene();
        else
        {
            isOptA = false;
            timeElapsed = 0;
            buttonCanvas.enabled = false;
            isOptionSelected = true;
        }

    }

    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setText(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }

    private void setText(string character, string text)
    {
        if (character == "A")
            abbyText.text = text;
        else if (character == "T")
            TravisText.text = text;
    }


    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }

}

