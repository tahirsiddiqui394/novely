﻿using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SceneSeventeen_six : MonoBehaviour {

    public Camera mainCamera;
    public Text americaText;
    public Text optAText;
    public Text optBText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image america;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    public SkeletonAnimation americaSkeleton;
    public Boolean isOptionSelected;
    public Canvas buttonCanvas;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    float pause = 1;

    string optionText = "";

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        america.enabled = false;
        buttonCanvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;
        if (level == "Scene17.7")
        {
            if (timeElapsed > 1 && timeElapsed < 3)
            {
                float wordscount = (timeElapsed - (float)1) * wordsPerSecond;
                conversation("Am", "Whatcha lookin’ at, Abby?", wordscount);
                pause = 3.5f;
            }
            else if (timeElapsed > 3.4 )
            {
                clearText();
                americaSkeleton.AnimationName = "17.1, 17.8 idle";
                america.enabled = false;
                buttonCanvas.enabled = true;
            }
        }
    }


    public void nextScene()
    {
        level = "Scene17.8";
        SceneManager.LoadScene(level);
    }

    private void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        americaText.text = textShownOnScreen;
          
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "Am")
                americaText.text = textShownOnScreen;
          
            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }


    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;

        setAnimation(character, text);
        if (character == "Am")
        {
            america.enabled = true; 
        }
        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private void setAnimation(string character, string text)
    {

         if (character == "Am")
        {
            americaSkeleton.AnimationName = "17.7, 17.8 dialogue";
        }
       

    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }

    public void clickedA()
    {
        timeElapsed = 0;
        buttonCanvas.enabled = false;
       isOptionSelected = true;
        nextScene();
       
    }

    public void clickedB()
    {
        timeElapsed = 0;
        buttonCanvas.enabled = false;
        isOptionSelected = true;
        nextScene();

    }

    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setText(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }

    private void setText(string character, string text)
    {
       if (character == "Am")
            americaText.text = text;
    }


    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }

}

