﻿using Spine.Unity;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneSeven : MonoBehaviour
{

    public Camera mainCamera;
    public Text abbyText;
    public Text americaText;
    public Text sheplyText;
    public Text travisText;
    public Text optAText;
    public Text optBText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby; 
    public Image america;
    public Image sheply;
    public Image travis;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation americaSkeleton;
    public SkeletonAnimation sheeplySkeleton;
    public SkeletonAnimation travisSkeleton;
    public Boolean isOptionSelected, isOption2Selected;
    public Canvas buttonCanvas;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    float pause = 1;

    public ArrayList optionsA =
    new ArrayList { "Speaking of papers, have you started the one for history yet?",
        "Have you?",
        "I finished it this afternoon.",
        "It’s not due until next Wednesday.",
        "I just plugged it out to get it out of the way. I hate feeling like I have unfinished business.",
        "I’m a procrastinator. It’s a good thing we’re not going to be friends. It drives people like you nuts.",
    "I dunno. You haven’t yet, even though you’re trying your damnedest. But, if you need help with it, just let me know.",
    "You’re going to help me with my paper."};
    public ArrayList optionsB =
    new ArrayList { "I can help you with the history paper... if you need it.",
        "I don’t.",
        "Do you hate me or something?",
        "No.",
        "You’re just trying to make a point. Don’t worry. I get it.",
        "Then what’s your angle?",
    " You surprise me, that’s all. Is it possible I’m not a complete asshole and just want to help you with your paper?",
    "You’re going to help me with my paper."};

    public ArrayList optionsA7_6 =
new ArrayList { " You sure? I’m better at writing a paper than I am throwing a punch.",
        "For your sake, I hope not."};
    public ArrayList optionsB7_6 =
    new ArrayList { "It’s an easy way to make a buck. I can’t make that much working at the mall.",
        " I wouldn’t say it’s easy if you’re getting hit in the face."};

    public ArrayList optionsA7_6_2 =
new ArrayList { "I don’t think you’re like the others who come over here, if that’s what you think.",
"I don’t normally have to beg girls to come to my apartment."};
    public ArrayList optionsB7_6_2 =
    new ArrayList { "I’m already impressed.", "I don’t normally have to beg girls to come to my apartment." };

    public ArrayList optionsText = new ArrayList();

    // Use this for initialization
    void Start()
    {
        SceneSix.isDirty = false;
        Screen.orientation = ScreenOrientation.Portrait;

        if (level == "Scene7.1")
        {
            textSet = true;
        }
            if (level == "Scene7.3")
        {

            abby.enabled = false;
            america.enabled = false;
            sheply.enabled = false;
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "7.3 idle dirty dress";
            else
                abbySkeleton.AnimationName = "7.3 idle clean dress";
        }
        else if (level == "Scene7.4")
        {
            pause = 3;
            abby.enabled = false;
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "7.3 idle dirty dress";
            else
                abbySkeleton.AnimationName = "7.3 idle clean dress";
        }
        else if (level == "Scene7.5")
        {
            abby.enabled = false;
            travis.enabled = false;
            buttonCanvas.enabled = false;
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "7.3 idle dirty dress";
            else
                abbySkeleton.AnimationName = "7.3 idle clean dress";
        }
        else if (level == "Scene7.6")
        {
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "7.7 dirty idle";
            else
                abbySkeleton.AnimationName = "7.7 clean idle";
            abby.enabled = false;
            travis
                .enabled = false;
            america.enabled = false;
            sheply.enabled = false;
            buttonCanvas.enabled = false;
        }

        else if (level == "Scene7.7")
        {
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "7.7 dirty idle";
            else
                abbySkeleton.AnimationName = "7.7 clean idle";
            abby.enabled = false;
        }
        else if (level == "Scene7.9")
        {
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "7.7 dirty idle";
            else
                abbySkeleton.AnimationName = "7.7 clean idle";
            abby.enabled = false;
            travis.enabled = false;
            america.enabled = false;
            sheply.enabled = false;
            buttonCanvas.enabled = false;
        }
        else if (level == "Scene7.10")
        {
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "7.9 walk away and wave  (dirty)";
            else
                abbySkeleton.AnimationName = "7.9 walk away and wave  (clean)";

        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;
        if (level == "Scene7.1")
        {
            if (timeElapsed > 0.5 && timeElapsed < 1.5)
            {
                zoomIn(1.93f, new Vector3(1.3f, 0.32f, -10f));
                sheeplySkeleton.loop = true;
            }

            if (timeElapsed > 1.5 && timeElapsed < 3)
            {
                sheeplySkeleton.AnimationName = "7.1 laughing";
            }
            else if (timeElapsed > 3.1 && timeElapsed < 4.8)
            {
                zoomOut();
                sheeplySkeleton.AnimationName = "7.1 idle";
            }
            else if (timeElapsed > 4.8)
            {
                level = "Scene7.3";
                SceneManager.LoadScene(level);
            }
        }

        if (level == "Scene7.10")
        {
            if (timeElapsed > 2.0)
            {
                nextScene();
            }
        }

        else if (level == "Scene7.3")
        {
            if (timeElapsed < 2.0)
            {
                float wordscount = timeElapsed * wordsPerSecond;
                conversation("S", "What happened to you?", wordscount);
                pause = 2.1f;
            }
            else if (timeElapsed > 2 && timeElapsed < 2.2)
            {
                clearText();
            }
            else if (timeElapsed >= 2.2 && timeElapsed < 7.0)
            {
                float wordscount = (timeElapsed - (float)2.2) * wordsPerSecond;
                conversation("Am", "She’s trying to be unimpressive.", wordscount);
                pause = 6.1f;
            }
            else if (timeElapsed > 6.0 && timeElapsed < 6.2)
            {
                clearText();
            }
            else if (timeElapsed >= 7.2 && timeElapsed < 10.0)
            {
                textSet = true;
                float wordscount = (timeElapsed - (float)7.2) * wordsPerSecond;
                conversation("A", "It’s clean.", wordscount);
                pause = 10.1f;
            }
            else if (timeElapsed > 10.0 && timeElapsed < 10.2)
            {
                if(SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.3 idle dirty dress";
                else
                    abbySkeleton.AnimationName = "7.3 idle clean dress";
                clearText();
            }
            else if (timeElapsed >= 10.2 && timeElapsed < 17.0)
            {
                float wordscount = (timeElapsed - (float)10.2) * wordsPerSecond;
                conversation("A", " I just put on the first thing I saw and came over.", wordscount);
                pause = 17.1f;
            }
            else if (timeElapsed > 17.0 && timeElapsed < 17.2)
            {
                clearText();
            }
            else if (timeElapsed >= 17.2 && timeElapsed < 24.0)
            {
                float wordscount = (timeElapsed - (float)17.2) * wordsPerSecond;
                conversation("S", "It’s just that... girls don’t come over here dressed like that.", wordscount);
                pause = 24.1f;
            }
            else if (timeElapsed > 24.0 && timeElapsed < 24.2)
            {
                clearText();
            }
            else if (timeElapsed >= 24.2 && timeElapsed < 28.0)
            {
                float wordscount = (timeElapsed - (float)24.2) * wordsPerSecond;
                conversation("S", "Where did you see it? The trashcan.", wordscount);
                pause = 28.1f;
            }
            else if (timeElapsed > 28.0 && timeElapsed < 28.2)
            {
                clearText();
            }
            else if (timeElapsed >= 28.2 && timeElapsed < 34.0)
            {
                float wordscount = (timeElapsed - (float)28.2) * wordsPerSecond;
                conversation("A", "Something tells me I didn’t come here for the same reason.", wordscount);
                pause = 34.1f;

            }
            else if (timeElapsed >= 34.0 && timeElapsed < 35)
            {
                clearText();
                abby.enabled = false;
                zoomIn(1.97f, new Vector3(-1.44f, 0.6f, -10f));
            }
            else if (timeElapsed >= 35.0 && timeElapsed < 35.5)
            {

                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.3 tougue out dirty";
                else
                    abbySkeleton.AnimationName = "7.3 tougue out clean";

            }

            else if (timeElapsed >= 35.9 && timeElapsed < 37)
            {
                zoomOut();
            }
            else if (timeElapsed > 37.3)
            {
                clearText();
                abby.enabled = false;
                america.enabled = false;
                sheply.enabled = false;
                level = "Scene7.4";
                SceneManager.LoadScene(level);
            }
        }

        else if (level == "Scene7.4")
        {

            if (timeElapsed > 3 && timeElapsed < 4.8)
            {
                zoomIn(2.25f, new Vector3(-0.93f, 0.79f, -10f));
            }
            else if (timeElapsed > 5 && timeElapsed < 18)
            {
                float wordscount = (timeElapsed - (float)5) * wordsPerSecond;
                conversation("A", "Their apartment was more aesthetically pleasing than the typical bachelor pad. The predictable posters of half-naked women and stolen street signs were on the walls, but it was clean, the furniture was new, and the smell of stale beer and dirty clothes was notably absent.", wordscount);
                pause = 18.1f;
            }
            else if (timeElapsed > 18.2)
            {
                //mainCamera.orthographicSize = 5f;
                //mainCamera.transform.position = new Vector3(0f, 0f, 0f);
                //mainCamera.nearClipPlane = 0;


                level = "Scene7.5";
                SceneManager.LoadScene(level);
            }

        }

        else if (level == "Scene7.5")
        {
            if (isOptionSelected)
            {
                if (timeElapsed < 4.5)
                {
                    float wordscount = timeElapsed * wordsPerSecond;
                    conversation("T", optionsText[0].ToString(), wordscount);
                    pause = 4.7f;
                }
                else if (timeElapsed > 4.5 && timeElapsed < 4.8)
                {
                    clearText();
                }
                else if (timeElapsed >= 4.8 && timeElapsed < 7.0)
                {
                    float wordscount = (timeElapsed - (float)4.8) * wordsPerSecond;
                    conversation("A", optionsText[1].ToString(), wordscount);
                    pause = 7.1f;
                }
                else if (timeElapsed > 7.0 && timeElapsed < 7.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 7.2 && timeElapsed < 12.0)
                {
                    float wordscount = (timeElapsed - (float)7.2) * wordsPerSecond;
                    conversation("T", optionsText[2].ToString(), wordscount);
                    pause = 12.1f;
                }
                else if (timeElapsed > 12.0 && timeElapsed < 12.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 12.2 && timeElapsed < 15.0)
                {
                    float wordscount = (timeElapsed - (float)12.2) * wordsPerSecond;
                    conversation("A", optionsText[3].ToString(), wordscount);
                    pause = 15.1f;
                }

                else if (timeElapsed > 15.0 && timeElapsed < 15.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 15.2 && timeElapsed < 22.0)
                {
                    float wordscount = (timeElapsed - (float)15.2) * wordsPerSecond;
                    conversation("T", optionsText[4].ToString(), wordscount);
                    pause = 22.1f;
                }
                else if (timeElapsed > 22.0 && timeElapsed < 22.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 22.2 && timeElapsed < 29.0)
                {
                    float wordscount = (timeElapsed - (float)22.2) * wordsPerSecond;
                    conversation("A", optionsText[5].ToString(), wordscount);
                    pause = 29.1f;
                }

                else if (timeElapsed > 29.0 && timeElapsed < 29.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 29.2 && timeElapsed < 37.0)
                {
                    float wordscount = (timeElapsed - (float)29.2) * wordsPerSecond;
                    conversation("T", optionsText[6].ToString(), wordscount);
                    pause = 37.1f;
                }
                else if (timeElapsed > 37.0 && timeElapsed < 37.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 37.2 && timeElapsed < 42.0)
                {
                    float wordscount = (timeElapsed - (float)37.2) * wordsPerSecond;
                    conversation("A", optionsText[7].ToString(), wordscount);
                    pause = 42.1f;
                }
                else if (timeElapsed > 42.0)
                {
                    clearText();
                    level = "Scene7.6";
                    SceneManager.LoadScene(level);
                }

            }
            else
            {
                Debug.Log(timeElapsed);
                if (timeElapsed >= 1 && timeElapsed < 4.0)
                {
                    travisSkeleton.loop = true;
                    float wordscount = (timeElapsed - (float)1) * wordsPerSecond;
                    conversation("T", "It’s about time you showed up.", wordscount);
                    pause = 4.1f;
                }
                else if (timeElapsed > 4 && timeElapsed < 5)
                {
                    travis.enabled = false;
                    clearText();
                    zoomIn(2.03f, new Vector3(1.41f, 0.33f, -10.3f));
                }

                else if (timeElapsed >=5.5 && timeElapsed < 6.0)
                {
                    if (SceneSix.isDirty)
                        abbySkeleton.AnimationName = "7.5 look down (dirty)";
                    else
                        abbySkeleton.AnimationName = "7.5 look down (clean)";

                    abbySkeleton.loop = false;
                    pause = 6.1f;
                }

                else if (timeElapsed >= 7.2 && timeElapsed < 8.5)
                {
                    //mainCamera.orthographicSize = 2.55f;
                    //mainCamera.transform.position = new Vector3(-1.09f, 0.07f, -10f);
                    //mainCamera.nearClipPlane = 0;
                    //travisSkeleton.AnimationName = "7.6 couch chuckles";
                    abbySkeleton.loop = true;
                    zoomOut();

                }
                else if (timeElapsed > 8.5 && timeElapsed < 9.2)
                {
                    if (SceneSix.isDirty)
                        abbySkeleton.AnimationName = "7.5 disappointed (dirty)";
                    else
                        abbySkeleton.AnimationName = "7.5 disappointed (clean)";

                    pause = 9.2f;
                }


                else if (timeElapsed >= 9.2 && timeElapsed < 14.0)
                {
                    float wordscount = (timeElapsed - (float)9.2) * wordsPerSecond;
                    conversation("A", "America had a paper to finish. She was coming over here, anyway.", wordscount);
                    pause = 14.1f;
                }
                else if (timeElapsed > 14.0 && timeElapsed < 14.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 14.2 && timeElapsed < 19.0)
                {
                    float wordscount = (timeElapsed - (float)14.2) * wordsPerSecond;
                    conversation("A", "I don’t have a car, so seemed like it was the best choice.", wordscount);
                    pause = 19.1f;
                }
                else if (timeElapsed > 19.0 && timeElapsed < 19.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 19.2 && timeElapsed < 18.0)
                {
                    float wordscount = (timeElapsed - (float)19.2) * wordsPerSecond;
                    conversation("T", "To keep me away?", wordscount);
                    pause = 18.1f;
                }
                else if (timeElapsed > 18.0 && !isOptionSelected)
                {
                    clearText();
                    buttonCanvas.enabled = true;
                    abby.enabled = false;
                    travis.enabled = false;

                }


            }
        }

        else if (level == "Scene7.6")
        {
            if (isOptionSelected)
            {
                if (isOption2Selected)
                {
                    if (timeElapsed < 2.0)
                    {
                        zoomIn(2.33f, new Vector3(1.22f, 0f, -10.3f));
                    }
                        if (timeElapsed > 2 && timeElapsed < 10.0)
                    {
                        float wordscount = (timeElapsed - (float)2) * wordsPerSecond;
                        conversation("A", "He smiled his boyish, amused grin, and I turned up my anger a notch, hoping it would cover my unease. I didn’t know how most girls felt around him, but I’d seen how they behaved.", wordscount);
                        pause = 10.1f;
                    }
                    else if (timeElapsed > 10 && timeElapsed < 10.2)
                    {
                        clearText();
                    }

                    else if (timeElapsed >= 10.2 && timeElapsed < 20.0)
                    {
                        float wordscount = (timeElapsed - (float)10.2) * wordsPerSecond;
                        conversation("A", "I was experiencing more of a disorientate, nauseous feeling than giggly infatuation, and the harder he worked to make me smile, the more unsettled I felt.", wordscount);
                        pause = 20.1f;
                    }

                    else if (timeElapsed > 20.0 && timeElapsed < 23.2)
                    {
                        abby.enabled = false;
                        clearText();
                        zoomOut();

                    }
                    else if (timeElapsed >= 23.2 && timeElapsed < 29.0)
                    {
                        float wordscount = (timeElapsed - (float)23.2) * wordsPerSecond;
                        conversation("T", optionsText[0].ToString(), wordscount);
                        pause = 29.1f;
                    }

                    else if (timeElapsed > 29.0 && timeElapsed < 29.2)
                    {
                        clearText();
                    }

                    else if (timeElapsed >= 29.2 && timeElapsed < 36.0)
                    {
                        float wordscount = (timeElapsed - (float)29.2) * wordsPerSecond;
                        conversation("T", optionsText[1].ToString(), wordscount);
                        pause = 36.1f;
                    }

                    else if (timeElapsed > 36.0 && timeElapsed < 36.2)
                    {
                        clearText();
                    }

                    else if (timeElapsed >= 36.2 && timeElapsed < 39.0)
                    {

                        float wordscount = (timeElapsed - (float)36.2) * wordsPerSecond;
                        conversation("A", "I’m sure.", wordscount);
                        pause = 39.2f;
                    }



                    else if (timeElapsed > 39.2)
                    {
                        clearText();
                        level = "Scene7.7";
                        SceneManager.LoadScene(level);
                    }
                }
                else
                {
                    if (timeElapsed < 6.0)
                    {
                        float wordscount = timeElapsed * wordsPerSecond;
                        conversation("T", optionsText[0].ToString(), wordscount);
                        pause = 6.1f;
                    }
                    else if (timeElapsed > 6 && timeElapsed < 6.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed >= 6.2 && timeElapsed < 9.5)
                    {
                        float wordscount = (timeElapsed - (float)6.2) * wordsPerSecond;
                        conversation("A", optionsText[1].ToString(), wordscount);
                        pause = 9.7f;
                    }
                    else if (timeElapsed > 9.5 && timeElapsed < 9.8)
                    {
                        clearText();
                    }
                    else if (timeElapsed >= 9.8 && timeElapsed < 15.5)
                    {
                        float wordscount = (timeElapsed - (float)9.8) * wordsPerSecond;
                        conversation("T", "What? You’re worried about me? (winks) I don’t get hit that often.", wordscount);
                        pause = 15.7f;
                    }
                    else if (timeElapsed > 15.5 && timeElapsed < 15.8)
                    {
                        clearText();
                    }
                    else if (timeElapsed >= 15.8 && timeElapsed < 19.0)
                    {
                        float wordscount = (timeElapsed - (float)15.8) * wordsPerSecond;
                        conversation("T", " If they sing, I move.It’s not that hard.", wordscount);
                        pause = 19.1f;
                    }

                    else if (timeElapsed > 19.0 && timeElapsed < 19.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed >= 19.2 && timeElapsed < 25.0)
                    {
                        float wordscount = (timeElapsed - (float)19.2) * wordsPerSecond;
                        conversation("A", "You act as if no one else has come to that conclusion.", wordscount);
                        pause = 25.1f;
                    }
                    else if (timeElapsed > 25.0 && timeElapsed < 25.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed >= 25.2 && timeElapsed < 33.0)
                    {
                        float wordscount = (timeElapsed - (float)25.2) * wordsPerSecond;
                        conversation("T", "When I throw a punch, they take it and try to reciprocate. That’s not gonna win a fight.", wordscount);
                        pause = 33.1f;
                    }

                    else if (timeElapsed > 33.0 && timeElapsed < 33.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed >= 33.2 && timeElapsed < 36.0)
                    {
                        float wordscount = (timeElapsed - (float)33.2) * wordsPerSecond;
                        conversation("A", "What are you...the Karate Kid?", wordscount);
                        pause = 36.1f;
                    }
                    else if (timeElapsed > 36.0 && timeElapsed < 36.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed >= 36.2 && timeElapsed < 39.0)
                    {
                        float wordscount = (timeElapsed - (float)36.2) * wordsPerSecond;
                        conversation("A", "Where did you learn to fight ?", wordscount);
                        pause = 39.1f;
                    }



                    else if (timeElapsed > 39.0 && timeElapsed < 39.2)
                    {
                        clearText();
                    }
                    else if (timeElapsed >= 39.2 && timeElapsed < 49.0)
                    {
                        float wordscount = (timeElapsed - (float)39.2) * wordsPerSecond;
                        conversation("T", "I had a dad with a drinking problem and a bad temper, and four older brothers that carried the asshole gene.", wordscount);
                        pause = 49.1f;
                    }
                    else if (timeElapsed > 49.0 && timeElapsed < 49.2)
                    {
                        clearText();
                    }


                    else if (timeElapsed >= 49.2 && timeElapsed < 51.0)
                    {
                        float wordscount = (timeElapsed - (float)49.2) * wordsPerSecond;
                        conversation("A", "Oh.", wordscount);
                        pause = 51.1f;
                    }
                    else if (timeElapsed > 51.0 && timeElapsed < 51.2)
                    {
                        clearText();
                    }


                    else if (timeElapsed >= 51.2 && timeElapsed < 57.0)
                    {
                        float wordscount = (timeElapsed - (float)51.2) * wordsPerSecond;
                        conversation("T", "Don’t be embarrassed, Pidge. Dad quit drinking.The brothers grew up.", wordscount);
                        pause = 57.1f;
                    }
                    else if (timeElapsed > 57.0 && timeElapsed < 57.2)
                    {
                        clearText();
                    }

                    else if (timeElapsed >= 57.2 && timeElapsed < 61.0)
                    {
                        float wordscount = (timeElapsed - (float)57.2) * wordsPerSecond;
                        conversation("T", "They fought as hard for me as they did with me.", wordscount);
                        pause = 61.1f;
                    }
                    else if (timeElapsed > 61.0 && timeElapsed < 61.2)
                    {
                        clearText();
                    }

                    else if (timeElapsed >= 61.2 && timeElapsed < 63.5)
                    {
                        float wordscount = (timeElapsed - (float)61.2) * wordsPerSecond;
                        conversation("A", "I’m not embarrassed.", wordscount);
                        pause = 63.7f;
                    }
                    else if (timeElapsed > 63.5 && timeElapsed < 63.8)
                    {
                        clearText();
                    }

                    else if (timeElapsed >= 63.8 && timeElapsed < 71.0)
                    {
                        float wordscount = (timeElapsed - (float)63.8) * wordsPerSecond;
                        conversation("T", "I like the au natural thing you’ve got going on.Girls don’t come over here like that.", wordscount);
                        pause = 71.2f;
                    }
                    else if (timeElapsed > 71.0)
                    {
                        clearText();
                        optAText.text = "So I’ve heard.";
                        optBText.text = "I was coerced into coming here. It didn’t occur to me to try to impress you.";
                        buttonCanvas.enabled = true;
                        abby.enabled = false;
                        travis.enabled = false;
                    }
                }

            }
            else
            {

                if (timeElapsed > 2.0 && timeElapsed < 7.0)
                {
                    americaSkeleton.loop = true;
                    sheeplySkeleton.loop = true;

                    float wordscount = timeElapsed * wordsPerSecond;
                    conversation("A", "I have an A in that class.Pretty sure I’m the top grade.", wordscount);
                    pause = 7.1f;
                }
                else if (timeElapsed > 7 && timeElapsed < 7.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 7.2 && timeElapsed < 14.0)
                {
                    float wordscount = (timeElapsed - (float)7.2) * wordsPerSecond;
                    conversation("S", "He has an A in all his classes. He doesn’t even have to try. I hate him.", wordscount);
                    pause = 14.1f;
                }
                else if (timeElapsed > 14.0 && timeElapsed < 14.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 14.2 && timeElapsed < 23.0)
                {
                    float wordscount = (timeElapsed - (float)14.2) * wordsPerSecond;
                    conversation("T", "What? You don’t think a guy covered in tats and who trades punches for a living can get the grades ? ", wordscount);
                    pause = 23.1f;
                }
                else if (timeElapsed > 23.0 && timeElapsed < 23.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 23.2 && timeElapsed < 29.0)
                {
                    float wordscount = (timeElapsed - (float)23.2) * wordsPerSecond;
                    conversation("T", " I’m not in school because I have nothing better to do.", wordscount);
                    pause = 29.1f;
                }
                else if (timeElapsed > 29.0 && timeElapsed < 29.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 29.2 && timeElapsed < 35.0)
                {
                    float wordscount = (timeElapsed - (float)29.2) * wordsPerSecond;
                    conversation("A", "Why fight at all then? Why didn’t you try for scholarships ? ", wordscount);
                    pause = 35.1f;
                }


                else if (timeElapsed > 35.0 && timeElapsed < 35.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 35.2 && timeElapsed < 45.0)
                {
                    float wordscount = (timeElapsed - (float)35.2) * wordsPerSecond;
                    conversation("T", "I was awarded half my tuition. But,there are books, living expenses,and I gotta come up with the rest sometime.", wordscount);
                    pause = 45.1f;
                }


                else if (timeElapsed > 45.0 && timeElapsed < 45.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 45.2 && timeElapsed < 53.0)
                {
                    float wordscount = (timeElapsed - (float)45.2) * wordsPerSecond;
                    conversation("T", " I’m serious, Pidge. If you need help with anything, just ask.I’m a great tutor.", wordscount);
                    pause = 53.1f;
                }


                else if (timeElapsed > 53.0 && timeElapsed < 53.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 53.2 && timeElapsed < 56.0)
                {
                    float wordscount = (timeElapsed - (float)53.2) * wordsPerSecond;
                    conversation("S", "He is.", wordscount);
                    pause = 56.1f;
                }
                else if (timeElapsed > 56.0 && !isOptionSelected)
                {
                    clearText();
                    buttonCanvas.enabled = true;
                    abby.enabled = false;
                    travis.enabled = false;
                    sheply.enabled = false;
                }

            }
        }

        else if (level == "Scene7.7")
        {

            if (timeElapsed < 1)
            {
                zoomIn(2.33f, new Vector3(1.22f, 0f, -10.3f));
            }
            else if (timeElapsed >= 1 && timeElapsed < 10.0)
            {
                float wordscount = timeElapsed * wordsPerSecond;
                conversation("A", "He was the worst kind of confident. Not only was he shamelessly aware of his appeal,", wordscount);
                pause = 10.1f;
            }
            else if (timeElapsed > 10 && timeElapsed < 10.2)
            {
                clearText();
            }

            else if (timeElapsed >= 10.2 && timeElapsed < 20.0)
            {
                float wordscount = (timeElapsed - (float)10.2) * wordsPerSecond;
                conversation("A", "he was so used to women throwing themselves at him that he regarded my cool demeanor ads refreshing instead of an insult. I would have to change my strategy.", wordscount);
                pause = 20.1f;
            }
            else if (timeElapsed > 20.3 && timeElapsed < 21.2)
            { 
            zoomOut();
            }
            else if (timeElapsed > 21.3&& timeElapsed < 21.4)
            {
                level = "Scene7.9";
                SceneManager.LoadScene(level);
            }

        }

        else if (level == "Scene7.9")
        {
            if (isOptionSelected)
            {
                if (timeElapsed < 4.0)
                {
                    float wordscount = timeElapsed * wordsPerSecond;
                    conversation("T", "C’mon. You’ve gotta be hungry.", wordscount);
                    pause = 4.1f;
                }
                else if (timeElapsed > 4 && timeElapsed < 4.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 4.2 && timeElapsed < 8.0)
                {
                    float wordscount = (timeElapsed - (float)4.2) * wordsPerSecond;
                    conversation("A", "Where are you going?", wordscount);
                    pause = 8.1f;
                }
                else if (timeElapsed > 8.0 && timeElapsed < 8.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 8.2 && timeElapsed < 14.0)
                {
                    float wordscount = (timeElapsed - (float)8.2) * wordsPerSecond;
                    conversation("T", "Wherever you want. We can hit a pizza place.", wordscount);
                    pause = 14.1f;
                }
                else if (timeElapsed > 14.0 && timeElapsed < 14.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 14.2 && timeElapsed < 17.0)
                {
                    float wordscount = (timeElapsed - (float)14.2) * wordsPerSecond;
                    conversation("A", "I’m not really dressed.", wordscount);
                    pause = 17.1f;
                }

                else if (timeElapsed > 17.0 && timeElapsed < 17.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 17.2 && timeElapsed < 21.0)
                {
                    float wordscount = (timeElapsed - (float)17.2) * wordsPerSecond;
                    conversation("T", "You look fine. Let’s go, I’m starving.", wordscount);
                    pause = 21.1f;
                }
                else if (timeElapsed > 21.0)
                {
                    clearText();
                    travis.enabled = false;
                    next.enabled = true;
                    level = "Scene7.10";
                    SceneManager.LoadScene(level);


                }

            }
            else
            {

                if (timeElapsed < 6.0)
                {
                    float wordscount = timeElapsed * wordsPerSecond;
                    conversation("T", "I was just heading out to dinner.You hungry, Pidge ?", wordscount);
                    pause = 6.1f;
                }
                else if (timeElapsed > 6 && timeElapsed < 6.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 6.2 && timeElapsed < 8.0)
                {
                    float wordscount = (timeElapsed - (float)6.2) * wordsPerSecond;
                    conversation("A", "I already ate.", wordscount);
                    pause = 8.1f;
                }
                else if (timeElapsed > 8.0 && timeElapsed < 8.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 8.2 && timeElapsed < 14.0)
                {
                    float wordscount = (timeElapsed - (float)8.2) * wordsPerSecond;
                    conversation("Am", "No, you haven’t.  Oh.Uh...that’s right.I forgot.", wordscount);
                    pause = 14.1f;
                }
                else if (timeElapsed > 14.0 && timeElapsed < 14.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 14.2 && timeElapsed < 18.0)
                {
                    float wordscount = (timeElapsed - (float)14.2) * wordsPerSecond;
                    conversation("Am", " You grabbed a... pizza? Before we left.", wordscount);
                    pause = 18.1f;
                }
                else if (timeElapsed > 18.0 && !isOptionSelected)
                {
                    clearText();
                    buttonCanvas.enabled = true;
                    abby.enabled = false;
                    travis.enabled = false;
                    america.enabled = false;
                }
            }
        }

    }


    public void nextScene()
    {
        level = "Scene8.1";
        SceneManager.LoadScene(level);
    }

    private void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
        if (level == "Scene7.3" || level == "Scene7.6" || level == "Scene7.9")
        {
            americaText.text = textShownOnScreen;
            sheplyText.text = textShownOnScreen;
        }
        if (level == "Scene7.5" || level == "Scene7.6" || level == "Scene7.9")
            travisText.text = textShownOnScreen;
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "A")
                abbyText.text = textShownOnScreen;
            else if (character == "Am")
                americaText.text = textShownOnScreen;
            else if (character == "S")
                sheplyText.text = textShownOnScreen;
            else if (character == "T")
                travisText.text = textShownOnScreen;

            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }


    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;

        setAnimation(character, text);
        if (character == "A")
        {
            abby.enabled = true;



            if (level == "Scene7.3" || level == "Scene7.6" || level == "Scene7.9")
            {
                america.enabled = false;
                sheply.enabled = false;
            }

            if (level == "Scene7.5" || level == "Scene7.6" || level == "Scene7.9")
                travis.enabled = false;

        }
        else if (character == "Am")
        {

            abby.enabled = false;
            america.enabled = true;
            sheply.enabled = false;

            if (level == "Scene7.5" || level == "Scene7.6" || level == "Scene7.9")
                travis.enabled = false;

        }
        else if (character == "S")
        {
            abby.enabled = false;
            america.enabled = false;
            sheply.enabled = true;

            if (level == "Scene7.5" || level == "Scene7.6" || level == "Scene7.9")
                travis.enabled = false;
        }
        else if (character == "T")
        {
            // abbySkeleton.AnimationName = "7.3 idle clean dress";
            abby.enabled = false;
            travis.enabled = true;
            if (level == "Scene7.6")
            {
                america.enabled = false;
                sheply.enabled = false;
            }

        }
        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private void setAnimation(string character, string text)
    {
        if (character == "A")
        {
            if (level == "Scene7.3")
            {
                americaSkeleton.AnimationName = "7.3 idle";
                sheeplySkeleton.AnimationName = "7.1 idle";
                if (text.Equals("It’s clean."))
                {

                    if (SceneSix.isDirty)
                        abbySkeleton.AnimationName = "7.3 its clean (dirty dresss)";
                    else
                        abbySkeleton.AnimationName = "7.3 its clean";
                    textSet = false;
                }
                else
                {
                    if (SceneSix.isDirty)
                        abbySkeleton.AnimationName = "7.3  dirty dress dialouge";
                    else
                        abbySkeleton.AnimationName = "7.3  clean dress dialouge";
                }

            }

            else if (level == "Scene7.4")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.3  dirty dress dialouge";
                else
                    abbySkeleton.AnimationName = "7.3  clean dress dialouge";

            }
            else if (level == "Scene7.7")
            {

                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.7 dirty dialogue";
                else
                    abbySkeleton.AnimationName = "7.7 clean dialogue";
            }


            else if (level == "Scene7.5")
            {
                travisSkeleton.AnimationName = "7.6 couch idle";
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.3  dirty dress dialouge";
                else
                    abbySkeleton.AnimationName = "7.3  clean dress dialouge";

            }

            else if (level == "Scene7.6")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.7 dirty dialogue";
                else
                    abbySkeleton.AnimationName = "7.7 clean dialogue";
                americaSkeleton.AnimationName = "7.6 idle";
                travisSkeleton.AnimationName = "7.6 couch idle";
                sheeplySkeleton.AnimationName = "7.6 idle";
            }
            else if (level == "Scene7.9")
            {
                americaSkeleton.AnimationName = "7.6 idle";
                sheeplySkeleton.AnimationName = "7.6 idle";
                travisSkeleton.AnimationName = "7.9 stands idle";
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.7 dirty dialogue";
                else
                    abbySkeleton.AnimationName = "7.7 clean dialogue";
            }


        }
        else if (character == "Am")
        {
            if (level == "Scene7.3")
            {

                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.3 idle dirty dress";
                else
                    abbySkeleton.AnimationName = "7.3 idle clean dress";
                americaSkeleton.AnimationName = "7.3 dialogue";
                sheeplySkeleton.AnimationName = "7.1 idle";
            }

            else if (level == "Scene7.6")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.7 dirty idle";
                else
                    abbySkeleton.AnimationName = "7.7 clean idle";
                travisSkeleton.AnimationName = "7.6 couch idle";

                americaSkeleton.AnimationName = "7.6 dialogue";
                sheeplySkeleton.AnimationName = "7.6 idle";
            }

            else if (level == "Scene7.9")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.7 dirty idle";
                else
                    abbySkeleton.AnimationName = "7.7 clean idle";
                americaSkeleton.AnimationName = "7.6 dialogue";
                sheeplySkeleton.AnimationName = "7.6 idle";
                travisSkeleton.AnimationName = "7.9 stands idle";

            }

        }
        else if (character == "S")
        {

            if (level == "Scene7.3")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.3 idle dirty dress";
                else
                    abbySkeleton.AnimationName = "7.3 idle clean dress";

                americaSkeleton.AnimationName = "7.3 idle";
                sheeplySkeleton.AnimationName = "7.1 dialogue";
            }

            else if (level == "Scene7.6")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.7 dirty idle";
                else
                    abbySkeleton.AnimationName = "7.7 clean idle";
                travisSkeleton.AnimationName = "7.6 couch idle";
                americaSkeleton.AnimationName = "7.6 idle";
                sheeplySkeleton.AnimationName = "7.6 dialogue";
            }
            else if (level == "Scene7.9")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.7 dirty idle";
                else
                    abbySkeleton.AnimationName = "7.7 clean idle";
                americaSkeleton.AnimationName = "7.6 idle";
                sheeplySkeleton.AnimationName = "7.6 dialogue";
                travisSkeleton.AnimationName = "7.9 stands idle";
            }

        }
        else if (character == "T")
        {
            if (level == "Scene7.5")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.3 idle dirty dress";
                else
                    abbySkeleton.AnimationName = "7.3 idle clean dress";

                if(text == "Do you hate me or something?")
                {
                    travisSkeleton.AnimationName = "7.6 couch chuckles";
                }
                else
                    travisSkeleton.AnimationName = "7.6 couch dialogue";

            }

            else if (level == "Scene7.6")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.7 dirty idle";
                else
                    abbySkeleton.AnimationName = "7.7 clean idle";
                americaSkeleton.AnimationName = "7.6 idle";
                sheeplySkeleton.AnimationName = "7.6 idle";
                travisSkeleton.AnimationName = "7.6 couch dialogue";
            }

            else if (level == "Scene7.9")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "7.7 dirty idle";
                else
                    abbySkeleton.AnimationName = "7.7 clean idle";
                americaSkeleton.AnimationName = "7.6 idle";
                sheeplySkeleton.AnimationName = "7.6 idle";
                travisSkeleton.AnimationName = "7.9 stands dialogue";
            }

        }
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }

    public void clickedA()
    {

        timeElapsed = 0;
        buttonCanvas.enabled = false;
        if (level == "Scene7.5")
        {
            optionsText = optionsA;
            isOptionSelected = true;
        }

        else if (level == "Scene7.6")
        {
            if (isOptionSelected)
            {
                isOption2Selected = true;
                optionsText = optionsA7_6_2;
            }

            else
            {
                isOptionSelected = true;
                optionsText = optionsA7_6;
            }

        }

        else if (level == "Scene7.9")
        {
            isOptionSelected = true;
        }
    }

    public void clickedB()
    {
        timeElapsed = 0;
        buttonCanvas.enabled = false;
        if (level == "Scene7.5")
        {
            optionsText = optionsB;
            isOptionSelected = true;
        }

        else if (level == "Scene7.6")
        {
            if (isOptionSelected)
            {
                isOption2Selected = true;
                optionsText = optionsB7_6_2;
            }

            else
            {
                isOptionSelected = true;
                optionsText = optionsB7_6;
            }

        }

        else if (level == "Scene7.9")
        {
            isOptionSelected = true;
        }
    }

    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setText(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }

    private void setText(string character, string text)
    {
        if (character == "A")
            abbyText.text = text;
        else if (character == "Am")
            americaText.text = text;
        else if (character == "S")
            sheplyText.text = text;
        else if (character == "T")
            travisText.text = text;
    }


    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }

}
