﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Next : MonoBehaviour {

	// Use this for initialization
	private float timer = 0;

	private float timerMax = 0;

	public KeyCode beginDemo;
	public float someReason;

	public string textShownOnScreen;
	public Text TextBackground;
	public string fullText = "Keep your cash in your wallet,\nAbby!";
	public float wordsPerSecond = 30; // speed of typewriter
	private float timeElapsed = 0; 
	Transform BoardImage;
	Transform BoardText;

	public void ChangeMenuScene(string name)
	{
		//Application.LoadLevel (name);
	}

	void Start () {
		//Text[] buttons = this.GetComponentsInChildren<Text>();
		//GameObject[] textGOs = GameObject.FindGameObjectWithTag("MainCanvas").GetComponentsInChildren<GameObject>();
		//Debug.Log (buttons.Length);
		//TextBackground = buttons[0];

//		GameObject canvasObject = GameObject.FindGameObjectWithTag("MainCanvas");
//
////		GameObject Abby = GameObject.FindGameObjectWithTag("Abby");
////		Abby.
//		Debug.Log ("ABBY");
////		Debug.Log (Abby);
//		Debug.Log (canvasObject);
//		 BoardImage = canvasObject.transform.Find("Image");
//		BoardText = BoardImage.transform.Find ("Text");
//		TextBackground = BoardText .GetComponent<Text>();
//	//	BoardImage.hideFlags = HideFlags.HideAndDontSave;
//
//		BoardImage.transform.localScale = new Vector3(0, 0, 0);

	}
	
	// Update is called once per frame
	void Update () {
//		timeElapsed += Time.deltaTime;
//		textShownOnScreen = GetWords(fullText1,  timeElapsed * wordsPerSecond);

//		Debug.Log("5 seconds is lost forever");
//
//		if(!Waited(1)) return;
//	
//		BoardImage.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
//		timeElapsed += Time.deltaTime;
//		textShownOnScreen = GetWords(fullText,  timeElapsed * wordsPerSecond);
//		Debug.Log(" seconds is lost forever");
	}
	private string GetWords(string text, float wordCount) {
		float words = wordCount;
		// loop through each character in text
		for (int i = 0; i < text.Length; i++) { 


			words--;
			TextBackground.text = textShownOnScreen;

			if (words <= 0) {
				return text.Substring (0, i);
			}
		}
		return text;
	}

	public void Wait(float seconds, Action action){
		StartCoroutine(_wait(seconds, action));
	}
	IEnumerator _wait(float time, Action callback){
		yield return new WaitForSeconds(time);
		callback();
	}

	private bool Waited(float seconds)
	{
		timerMax = seconds;

		timer += Time.deltaTime;

		if (timer >= timerMax)
		{
			return true; //max reached - waited x - seconds
		}

		return false;
	}
}
