
Travis2.png
size: 2467,1394
format: RGBA8888
filter: Linear,Linear
repeat: none
Head
  rotate: false
  xy: 1339, 217
  size: 256, 306
  orig: 256, 306
  offset: 0, 0
  index: -1
L arm
  rotate: false
  xy: 1299, 525
  size: 299, 867
  orig: 299, 867
  offset: 0, 0
  index: -1
L arm_Shirt
  rotate: true
  xy: 1928, 23
  size: 247, 301
  orig: 247, 301
  offset: 0, 0
  index: -1
L boot
  rotate: true
  xy: 1050, 140
  size: 369, 287
  orig: 369, 287
  offset: 0, 0
  index: -1
R boot
  rotate: true
  xy: 1050, 140
  size: 369, 287
  orig: 369, 287
  offset: 0, 0
  index: -1
L eye
  rotate: false
  xy: 133, 6
  size: 45, 26
  orig: 45, 26
  offset: 0, 0
  index: -1
L eyebrow
  rotate: true
  xy: 201, 10
  size: 50, 17
  orig: 50, 17
  offset: 0, 0
  index: -1
L leg
  rotate: false
  xy: 413, 205
  size: 272, 1187
  orig: 272, 1187
  offset: 0, 0
  index: -1
L pupil
  rotate: false
  xy: 220, 2
  size: 18, 17
  orig: 18, 17
  offset: 0, 0
  index: -1
L wrist
  rotate: false
  xy: 537, 2
  size: 102, 201
  orig: 102, 201
  offset: 0, 0
  index: -1
L wrist 2
  rotate: false
  xy: 415, 2
  size: 120, 201
  orig: 120, 201
  offset: 0, 0
  index: -1
Lip1
  rotate: true
  xy: 641, 64
  size: 65, 18
  orig: 65, 18
  offset: 0, 0
  index: -1
Lip2
  rotate: false
  xy: 295, 28
  size: 65, 32
  orig: 65, 32
  offset: 0, 0
  index: -1
Lip3
  rotate: true
  xy: 180, 2
  size: 58, 19
  orig: 58, 19
  offset: 0, 0
  index: -1
Lip4
  rotate: false
  xy: 295, 3
  size: 69, 23
  orig: 69, 23
  offset: 0, 0
  index: -1
Lip5
  rotate: false
  xy: 2, 2
  size: 65, 30
  orig: 65, 30
  offset: 0, 0
  index: -1
Lip6
  rotate: false
  xy: 69, 4
  size: 62, 28
  orig: 62, 28
  offset: 0, 0
  index: -1
R arm
  rotate: true
  xy: 1600, 272
  size: 275, 853
  orig: 275, 853
  offset: 0, 0
  index: -1
R eye
  rotate: true
  xy: 641, 3
  size: 59, 32
  orig: 59, 32
  offset: 0, 0
  index: -1
R eyebriw
  rotate: true
  xy: 641, 131
  size: 72, 25
  orig: 72, 25
  offset: 0, 0
  index: -1
R leg
  rotate: false
  xy: 2, 198
  size: 409, 1194
  orig: 409, 1194
  offset: 0, 0
  index: -1
R pupil
  rotate: true
  xy: 362, 39
  size: 21, 19
  orig: 21, 19
  offset: 0, 0
  index: -1
R wrist
  rotate: true
  xy: 164, 62
  size: 134, 249
  orig: 134, 249
  offset: 0, 0
  index: -1
R wrist 2
  rotate: false
  xy: 2, 34
  size: 160, 162
  orig: 160, 162
  offset: 0, 0
  index: -1
R-arm shirt
  rotate: true
  xy: 1597, 5
  size: 265, 329
  orig: 265, 329
  offset: 0, 0
  index: -1
Torso
  rotate: false
  xy: 687, 511
  size: 610, 881
  orig: 610, 881
  offset: 0, 0
  index: -1
Tshirt-Torso
  rotate: true
  xy: 1600, 549
  size: 843, 865
  orig: 843, 865
  offset: 0, 0
  index: -1
hip
  rotate: true
  xy: 687, 50
  size: 459, 361
  orig: 459, 361
  offset: 0, 0
  index: -1
lip default
  rotate: false
  xy: 220, 21
  size: 73, 39
  orig: 73, 39
  offset: 0, 0
  index: -1
