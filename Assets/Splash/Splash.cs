﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splash : MonoBehaviour {

    public Canvas buttton;
    public float wordsPerSecond = 30; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";

    // Use this for initialization
    void Start () {
        Screen.orientation = ScreenOrientation.Portrait;
        buttton.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;

            if (timeElapsed > 2.0)
            {
            buttton.enabled = true;
            }
        
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }

    public void clicked()
    {
        level = "Scene6.1";
        Application.LoadLevel(level);
    }
    }
