﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class SceneTen_2 : MonoBehaviour {

    public Text abbyText;
    public Text travisText;
    public Text waiterText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby;
    public Image waiter;
    public Image travis;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    public Boolean isOptionSelected, isOptionSelected2;
    public Canvas multibuttonCanvas,buttonCanvas;

    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation travisSkeleton;
    public SkeletonAnimation waiteressSkeleton;
    public SkeletonAnimation blurSkeleton;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    public Camera mainCamera;
    float pause =0;
    bool activeButton = true;
    bool isoptionA = true;
    bool isoptionA2 = true;
    public Text optAText;
    public Text optBText;

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        if (level == "Scene10.2") {
            activeButton = false;
        abby.enabled = false;
        travis.enabled = false;
        waiter.enabled = false;
        multibuttonCanvas.enabled = false;
        if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "10 dialouge dirty";
            else
                abbySkeleton.AnimationName = "10 dialouge clean";

        }
        else if(level == "Scene10.3")
        {
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "10 dialouge dirty";
            else
                abbySkeleton.AnimationName = "10 dialouge clean";
        }
        else if (level == "Scene10.4")
        {
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "10 dialouge dirty";
            else
                abbySkeleton.AnimationName = "10 dialouge clean";
        }

        else if (level == "Scene10.5")
        {
            buttonCanvas.enabled = false;
            abby.enabled = false;
            travis.enabled = false;
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "10 dialouge dirty";
            else
                abbySkeleton.AnimationName = "10 dialouge clean";
        }
        else if (level == "Scene10.8")
        {
            buttonCanvas.enabled = false;
            abby.enabled = false;
            travis.enabled = false;
            waiter.enabled = false;
            if (SceneSix.isDirty)
                abbySkeleton.AnimationName = "10 dialouge dirty";
            else
                abbySkeleton.AnimationName = "10 dialouge clean";
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;

        if (level == "Scene10.2")
        {
            if (isOptionSelected)
            {

                if (timeElapsed < 5.0)
                {
                    activeButton = true;
                    float wordscount = timeElapsed * wordsPerSecond;
                    conversation("T", "Good choice. Make it a large, Jess. I’m starving.", wordscount);
                    pause = 5.1f;
                }
                else if (timeElapsed > 5 && timeElapsed < 5.2)
                {
                    clearText();

                }

                else if (timeElapsed >= 5.2 && timeElapsed < 7.5)
                {
                    float wordscount = (timeElapsed - (float)5.2) * wordsPerSecond;
                    conversation("W", "Just flag me down if you need anything else.", wordscount);
                    pause = 7.6f;
                }
                else if (timeElapsed > 7.5 && timeElapsed < 7.7)
                {
                    waiter.enabled = false;
                    clearText();

                }

                else if (timeElapsed >= 7.7 && timeElapsed < 8.4)
                {
                    waiteressSkeleton.AnimationName = "10.2,10.8 exit";
                }

                else if (timeElapsed >= 8.4 && timeElapsed < 12.0)
                {
                    float wordscount = (timeElapsed - (float)8.4) * wordsPerSecond;
                    conversation("T", "So, you don’t hate me. That’s a start.", wordscount);
                    pause = 12.1f;
                }
                else if (timeElapsed > 12.0 && timeElapsed < 12.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 12.2 && timeElapsed < 14.0)
                {
                    float wordscount = (timeElapsed - (float)12.2) * wordsPerSecond;
                    conversation("T", "You were trying awfully hard there for a while.", wordscount);
                    pause = 14.1f;
                }

                else if (timeElapsed > 14.0 && timeElapsed < 14.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 14.2 && timeElapsed < 16.0)
                {
                    float wordscount = (timeElapsed - (float)14.2) * wordsPerSecond;
                    conversation("A", "It wasn’t a ploy. I just don’t like you.", wordscount);
                    pause = 16.1f;
                }
                else if (timeElapsed > 16.0 && timeElapsed < 16.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 16.2 && timeElapsed < 19.0)
                {
                    float wordscount = (timeElapsed - (float)16.2) * wordsPerSecond;
                    conversation("T", "You wouldn’t be here if you didn’t like me.", wordscount);
                    pause = 19.1f;
                }

                else if (timeElapsed > 19.0 && timeElapsed < 19.2)
                {
                    clearText();
                }

                else if (timeElapsed >= 19.2 && timeElapsed < 22.0)
                {
                    float wordscount = (timeElapsed - (float)19.2) * wordsPerSecond;
                    conversation("A", "I don’t like you like that. Not even close.", wordscount);
                    pause = 22.1f;
                }

                else if (timeElapsed > 22.0 && timeElapsed < 22.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 22.2 && timeElapsed < 25.2)
                {
                    float wordscount = (timeElapsed - (float)22.2) * wordsPerSecond;
                    conversation("T", "You’re breaking my heart, Pidge!", wordscount);
                    pause = 25.4f;
                }

                else if (timeElapsed > 25.3 && timeElapsed < 25.5)
                {
                    clearText();
                }

                else if (timeElapsed >= 25.5 && timeElapsed < 30.0)
                {
                    float wordscount = (timeElapsed - (float)25.5) * wordsPerSecond;
                    conversation("T", " I’ll make you a deal. I won’t even think about your panties...", wordscount);
                    pause = 30.1f;
                }

                else if (timeElapsed > 30.0 && timeElapsed < 30.2)
                {
                    clearText();
                }

                else if (timeElapsed >= 30.2 && timeElapsed < 32.0)
                {
                    float wordscount = (timeElapsed - (float)30.2) * wordsPerSecond;
                    conversation("T", "unless you want me to.", wordscount);
                    pause = 32.1f;
                }

                else if (timeElapsed > 32.0 && timeElapsed < 32.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 32.2 && timeElapsed < 35.2)
                {
                    float wordscount = (timeElapsed - (float)32.2) * wordsPerSecond;
                    conversation("A", "And that won’t happen, so we can be friends.", wordscount);
                    pause = 35.3f;
                }

                else if (timeElapsed > 35.2 && timeElapsed < 35.4)
                {
                    clearText();
                }

                else if (timeElapsed >= 35.4 && timeElapsed < 37.0)
                {
                    float wordscount = (timeElapsed - (float)35.4) * wordsPerSecond;
                    conversation("T", "Never say never.", wordscount);
                    pause = 37.1f;
                }

                else if (timeElapsed > 37.0 && timeElapsed < 37.2)
                {
                    clearText();
                }

                else if (timeElapsed >= 37.2 && timeElapsed < 40.0)
                {
                    float wordscount = (timeElapsed - (float)37.2) * wordsPerSecond;
                    conversation("A", "So what’s your story?", wordscount);
                    pause = 40.1f;
                }

                else if (timeElapsed > 40.0 && timeElapsed < 40.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 40.2 && timeElapsed < 43.2)
                {
                    float wordscount = (timeElapsed - (float)40.2) * wordsPerSecond;
                    conversation("A", "Have you always been Travis Mad Dog Maddox", wordscount);
                    pause = 43.1f;
                }

                else if (timeElapsed > 43.0 && timeElapsed < 43.2)
                {
                    clearText();
                }

                else if (timeElapsed >= 43.2 && timeElapsed < 47.0)
                {
                    float wordscount = (timeElapsed - (float)43.2) * wordsPerSecond;
                    conversation("A", " or is that just since you started fighting in the Circle ?", wordscount);
                    pause = 47.1f;
                }

                else if (timeElapsed > 47.0 && timeElapsed < 47.2)
                {
                    clearText();
                }

                else if (timeElapsed >= 47.2 && timeElapsed < 52.0)
                {
                    float wordscount = (timeElapsed - (float)47.2) * wordsPerSecond;
                    conversation("T", "No. Adam started that after my first fight.He’s been doing this a long time.", wordscount);
                    pause = 52.1f;
                }

                else if (timeElapsed > 52.0 && timeElapsed < 52.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 52.2 && timeElapsed < 57.2)
                {
                    float wordscount = (timeElapsed - (float)52.2) * wordsPerSecond;
                    conversation("T", " He started it with my oldest brother, Tommy, back when they went here.", wordscount);
                    pause = 57.1f;
                }

                else if (timeElapsed > 57.0 && timeElapsed < 57.2)
                {
                    clearText();
                }

                else if (timeElapsed >= 57.2 && timeElapsed < 60.0)
                {
                    float wordscount = (timeElapsed - (float)57.2) * wordsPerSecond;
                    conversation("T", " He just knows what gets the crowd riled up.", wordscount);
                    pause = 60.1f;
                }

                else if (timeElapsed > 60.0 && timeElapsed < 60.2)
                {
                    clearText();
                }


                else if (timeElapsed >= 60.2 && timeElapsed < 62.0)
                {
                    float wordscount = (timeElapsed - (float)60.2) * wordsPerSecond;
                    conversation("A", "What else?", wordscount);
                    pause = 62.1f;
                }

                else if (timeElapsed > 62.0 && timeElapsed < 62.2)
                {
                    clearText();
                }

                else if (timeElapsed >= 62.2 && timeElapsed < 65.0)
                {
                    float wordscount = (timeElapsed - (float)62.2) * wordsPerSecond;
                    conversation("T", "I’m from here, born and raised. I’m a criminal justice major.", wordscount);
                    pause = 65.1f;
                }

                else if (timeElapsed > 65.0)
                {
                    clearText();
                    travis.enabled = false;
                    level = "Scene10.3";
                    SceneManager.LoadScene(level);
                }


            }
            else
            {
                if (timeElapsed > 1.4 && timeElapsed < 2.6)
                {
                    waiteressSkeleton.AnimationName = "10.2,10.8  idle";
                    waiteressSkeleton.loop = false;
                }

                else if (timeElapsed > 2.7 && timeElapsed < 3.2)
                {
                    travisSkeleton.AnimationName = "10  takes a swig";
                }

                else if (timeElapsed > 3.2 && timeElapsed < 8.0)
                {

                    float wordscount = (timeElapsed - (float)3.2) * wordsPerSecond;
                    conversation("T", "What d’ya say, Pidge? Pepperoni? Or are you a supreme kind of girl ?", wordscount);
                }
                else if (timeElapsed > 8 && timeElapsed < 8.2)
                {
                    clearText();
                    travis.enabled = false;
                    multibuttonCanvas.enabled = true;
                }
            }
        }

        else if (level == "Scene10.3")
        {
            if (timeElapsed > 3.0)
            {
                level = "Scene10.4";
                SceneManager.LoadScene(level);
            }
        }

        else if (level == "Scene10.4")
        {
            if (timeElapsed > 1.0 && timeElapsed < 2.3)
            {
                zoomIn(1.46f, new Vector3(-1.83f, 0.22f, -10f));
            }

            else if (timeElapsed > 2.3 && timeElapsed < 3.6)
            {
                travisSkeleton.AnimationName = "10 angry";
            }

            else if (timeElapsed > 3.6 && timeElapsed < 4.6)
            {
                zoomOut();
            }
            if (timeElapsed > 4.6)
            {
                level = "Scene10.5";
                SceneManager.LoadScene(level);
            }

        }

        else if (level == "Scene10.6")
        {
            if (timeElapsed > 3.6)
            {
                level = "Scene10.8";
                SceneManager.LoadScene(level);
            }

        }


        else if (level == "Scene10.5")
        {

            if (isOptionSelected)
            {
                if (isOptionSelected2)
                {
                    if (isoptionA2)
                    {
                        if (timeElapsed < 9.0)
                        {
                            float wordscount = timeElapsed * wordsPerSecond;
                            conversation("T", "A. That sucks. I’m sorry. What about American’s parents? I’ve heard her talk about them. They seem all right. ", wordscount);
                            pause = 9.1f;
                        }

                        else if (timeElapsed > 9.0 && timeElapsed < 9.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 9.2 && timeElapsed < 18.2)
                        {
                            float wordscount = (timeElapsed - (float)9.2) * wordsPerSecond;
                            conversation("A", "Mark and Pam are great. They’ve practically raised me since I was fourteen. America just sort of tagged along with me here. She didn’t want me to come alone. ", wordscount);
                            pause = 18.1f;
                        }

                        else if (timeElapsed > 18.0 && timeElapsed < 18.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 18.2 && timeElapsed < 23.2)
                        {
                            float wordscount = (timeElapsed - (float)18.2) * wordsPerSecond;
                            conversation("A", "So, why Eastern State?", wordscount);
                            pause = 23.1f;
                        }

                        else if (timeElapsed > 23.0)
                        {
                            abby.enabled = false;
                            clearText();
                            level = "Scene10.8";
                            SceneManager.LoadScene(level);
                        }
                    }
                    else
                    {
                        if (timeElapsed < 3.0)
                        {
                            float wordscount = timeElapsed * wordsPerSecond;
                            conversation("A", "They’re still talking about me?", wordscount);
                            pause = 3.1f;
                        }

                        else if (timeElapsed > 3.0 && timeElapsed < 3.2)
                        {
                            abby.enabled = false;
                            clearText();
                            level = "Scene10.6";
                            SceneManager.LoadScene(level);
                        }

                    }
                }
                else
                {

                    if (isoptionA)
                    {
                        if (timeElapsed < 3.0)
                        {

                            float wordscount = timeElapsed * wordsPerSecond;
                            conversation("T", "What do you mean?", wordscount);
                            pause = 3.1f;
                        }

                        else if (timeElapsed > 3.0 && timeElapsed < 3.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 3.2 && timeElapsed < 7.0)
                        {
                            float wordscount = (timeElapsed - (float)3.2) * wordsPerSecond;
                            conversation("A", "They’re not used to you being seen with someone who looks like me.", wordscount);
                            pause = 7.1f;
                        }

                        else if (timeElapsed > 7.0 && timeElapsed < 7.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 7.2 && timeElapsed < 10.0)
                        {
                            float wordscount = (timeElapsed - (float)7.2) * wordsPerSecond;
                            conversation("T", "Why wouldn’t I be seen with you?", wordscount);
                            pause = 10.1f;
                        }

                        else if (timeElapsed > 10.0 && timeElapsed < 10.2)
                        {
                            clearText();
                        }


                        else if (timeElapsed > 10.2 && timeElapsed < 14.0)
                        {
                            float wordscount = (timeElapsed - (float)10.2) * wordsPerSecond;
                            conversation("A", "Um... what were we talking about?", wordscount);
                            pause = 14.1f;
                        }

                        else if (timeElapsed > 14.0 && timeElapsed < 14.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 14.2 && timeElapsed < 17.0)
                        {
                            float wordscount = (timeElapsed - (float)14.2) * wordsPerSecond;
                            conversation("T", "You. What’s your major?", wordscount);
                            pause = 17.1f;
                        }

                        else if (timeElapsed > 17.0 && timeElapsed < 17.2)
                        {
                            clearText();
                        }


                        else if (timeElapsed > 17.2 && timeElapsed < 21.2)
                        {
                            float wordscount = (timeElapsed - (float)17.2) * wordsPerSecond;
                            conversation("A", "General ed for now. I’m still undecided.", wordscount);
                            pause = 21.1f;
                        }

                        else if (timeElapsed > 21.0 && timeElapsed < 21.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 21.2 && timeElapsed < 25.0)
                        {
                            float wordscount = (timeElapsed - (float)21.2) * wordsPerSecond;
                            conversation("T", "Where are you from ? Are youout of state ?", wordscount);
                            pause = 25.1f;
                        }

                        else if (timeElapsed > 25.0 && timeElapsed < 25.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 25.2 && timeElapsed < 29.0)
                        {
                            float wordscount = (timeElapsed - (float)25.2) * wordsPerSecond;
                            conversation("A", "Wichita, Kansas. Same as America.", wordscount);
                            pause = 29.1f;
                        }

                        else if (timeElapsed > 29.0 && timeElapsed < 29.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 29.2 && timeElapsed < 34.0)
                        {
                            float wordscount = (timeElapsed - (float)29.2) * wordsPerSecond;
                            conversation("T", "How did you end up here from Kansas?", wordscount);
                            pause = 34.1f;
                        }

                        else if (timeElapsed > 34.0 && timeElapsed < 34.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 34.2 && timeElapsed < 37.0)
                        {
                            float wordscount = (timeElapsed - (float)34.2) * wordsPerSecond;
                            conversation("A", "We just had to get away.", wordscount);
                            pause = 37.1f;
                        }

                        else if (timeElapsed > 37.0 && timeElapsed < 37.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 37.2 && timeElapsed < 39.0)
                        {
                            float wordscount = (timeElapsed - (float)37.2) * wordsPerSecond;
                            conversation("T", "From what?", wordscount);
                            pause = 39.1f;
                        }

                        else if (timeElapsed > 39.0)
                        {
                            clearText();
                            travis.enabled = false;
                            buttonCanvas.enabled = true;
                            optAText.text = "A. My parents.";
                            optBText.text = "B. My past.";
                        }

                    }
                    else
                    {
                        if (timeElapsed < 9.0)
                        {
                            float wordscount = timeElapsed * wordsPerSecond;
                            conversation("T", "I overheard some of them joking about me having to take you to dinner first. It’s not usually... my thing.", wordscount);
                            pause = 9.1f;
                        }

                        else if (timeElapsed > 9.0 && timeElapsed < 9.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 9.2 && timeElapsed < 18.0)
                        {
                            float wordscount = (timeElapsed - (float)9.2) * wordsPerSecond;
                            conversation("A", "First? They assume I’m going to sleep with you just because I’m sitting across a table from you? Wow. I’m getting a reputation already. Great.", wordscount);
                            pause = 18.1f;
                        }

                        else if (timeElapsed > 18.0 && timeElapsed < 18.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 18.2 && timeElapsed < 23.2)
                        {
                            float wordscount = (timeElapsed - (float)18.2) * wordsPerSecond;
                            conversation("A", "What were we talking about?", wordscount);
                            pause = 23.1f;
                        }

                        else if (timeElapsed > 23.0 && timeElapsed < 23.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 23.2 && timeElapsed < 27.0)
                        {
                            float wordscount = (timeElapsed - (float)23.2) * wordsPerSecond;
                            conversation("T", "You. What’s your major?", wordscount);
                            pause = 27.1f;
                        }

                        else if (timeElapsed > 27.0 && timeElapsed < 27.2)
                        {
                            clearText();
                        }


                        else if (timeElapsed > 27.2 && timeElapsed < 31.0)
                        {
                            float wordscount = (timeElapsed - (float)27.2) * wordsPerSecond;
                            conversation("A", "General ed for now. I’m still undecided.", wordscount);
                            pause = 31.1f;
                        }

                        else if (timeElapsed > 31.0 && timeElapsed < 31.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 31.2 && timeElapsed < 35.0)
                        {
                            float wordscount = (timeElapsed - (float)31.2) * wordsPerSecond;
                            conversation("T", "Where are you from ? Are youout of state ?", wordscount);
                            pause = 35.1f;
                        }

                        else if (timeElapsed > 35.0 && timeElapsed < 35.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 35.2 && timeElapsed < 39.0)
                        {
                            float wordscount = (timeElapsed - (float)35.2) * wordsPerSecond;
                            conversation("A", "Wichita, Kansas. Same as America.", wordscount);
                            pause = 39.1f;
                        }

                        else if (timeElapsed > 39.0 && timeElapsed < 39.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 39.2 && timeElapsed < 44.0)
                        {
                            float wordscount = (timeElapsed - (float)39.2) * wordsPerSecond;
                            conversation("T", "How did you end up here from Kansas?", wordscount);
                            pause = 44.1f;
                        }

                        else if (timeElapsed > 44.0 && timeElapsed < 44.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 44.2 && timeElapsed < 47.0)
                        {
                            float wordscount = (timeElapsed - (float)44.2) * wordsPerSecond;
                            conversation("A", "We just had to get away.", wordscount);
                            pause = 47.1f;
                        }

                        else if (timeElapsed > 47.0 && timeElapsed < 47.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed > 47.2 && timeElapsed < 49.0)
                        {
                            float wordscount = (timeElapsed - (float)47.2) * wordsPerSecond;
                            conversation("T", "From what?", wordscount);
                            pause = 49.1f;
                        }

                        else if (timeElapsed > 49.0)
                        {
                            clearText();
                            travis.enabled = false;
                            buttonCanvas.enabled = true;
                            optAText.text = "A. My parents.";
                            optBText.text = "B. My past.";
                        }
                    }

                }

            }

            else
            {
                if (timeElapsed < 3.0)
                {
                    float wordscount = timeElapsed * wordsPerSecond;
                    conversation("A", "You’re joking.", wordscount);
                    pause = 3.1f;
                }

                else if (timeElapsed > 3.0 && timeElapsed < 3.2)
                {
                    clearText();
                }

                else if (timeElapsed > 3.2 && timeElapsed < 5.6)
                {
                    float wordscount = (timeElapsed - (float)3.2) * wordsPerSecond;
                    conversation("T", "No, I’m a local.", wordscount);
                    pause = 5.7f;
                }
                else if (timeElapsed > 5.6 && timeElapsed < 5.8)
                {
                    clearText();
                }

                else if (timeElapsed > 5.8 && timeElapsed < 10.0)
                {
                    float wordscount = (timeElapsed - (float)5.8) * wordsPerSecond;
                    conversation("A", "I meant about your major. You don’t look like the criminal justice type.", wordscount);
                    pause = 10.1f;
                }
                else if (timeElapsed > 10.0 && timeElapsed < 10.2)
                {
                    activeButton = false;
                    abby.enabled = false;
                    clearText();
                }



                if (timeElapsed > 10.2 && timeElapsed < 11.3)
                {
                    zoomIn(2.41f, new Vector3(-1.09f, 0.53f, -10f));
                }

                else if (timeElapsed > 11.3 && timeElapsed < 11.9)
                {
                    travisSkeleton.AnimationName = "10 angry";
                    float wordscount = (timeElapsed - (float)11.3) * wordsPerSecond;
                    conversation("T", "Why?", wordscount);
                }

                else if (timeElapsed > 11.9 && timeElapsed < 13.4)
                {
                    zoomOut();
                }


                else if (timeElapsed > 13.4 && timeElapsed < 17.0)
                {
                    activeButton = true;
                    float wordscount = (timeElapsed - (float)13.4) * wordsPerSecond;
                    conversation("A", "I’ll just say that you seem more criminal and less justice.", wordscount);
                    pause = 17.1f;
                }
                else if (timeElapsed > 17.0 && timeElapsed < 17.2)
                {
                    clearText();
                }


                else if (timeElapsed > 17.2 && timeElapsed < 24.0)
                {

                    float wordscount = (timeElapsed - (float)17.2) * wordsPerSecond;
                    conversation("T", "I don’t get into any trouble...for the most part. Dad was pretty strict.", wordscount);
                    pause = 24.1f;
                }
                else if (timeElapsed > 24.0 && timeElapsed < 24.2)
                {
                    clearText();
                }

                else if (timeElapsed > 24.2 && timeElapsed < 27.0)
                {

                    float wordscount = (timeElapsed - (float)24.2) * wordsPerSecond;
                    conversation("A", "What about your mom?", wordscount);
                    pause = 27.1f;
                }
                else if (timeElapsed > 27.0 && timeElapsed < 27.2)
                {
                    clearText();
                }

                else if (timeElapsed > 27.2 && timeElapsed < 31.0)
                {

                    float wordscount = (timeElapsed - (float)27.2) * wordsPerSecond;
                    conversation("A", "She died when I was a kid.", wordscount);
                    pause = 31.1f;
                }
                else if (timeElapsed > 31.0 && timeElapsed < 31.2)
                {
                    clearText();
                }

                else if (timeElapsed > 31.2 && timeElapsed < 34.0)
                {

                    float wordscount = (timeElapsed - (float)31.2) * wordsPerSecond;
                    conversation("A", "Oh. I’m ... I’m sorry.", wordscount);
                    pause = 34.1f;
                }
                else if (timeElapsed > 34.0 && timeElapsed < 34.2)
                {
                    clearText();
                }

                else if (timeElapsed > 34.2 && timeElapsed < 39.0)
                {

                    float wordscount = (timeElapsed - (float)34.2) * wordsPerSecond;
                    conversation("T", "I don’t remember her, really. My brothers do, but I was just three when she died.", wordscount);
                    pause = 39.1f;
                }
                else if (timeElapsed > 39.0 && timeElapsed < 39.2)
                {
                    clearText();
                }

                else if (timeElapsed > 39.2 && timeElapsed < 43.0)
                {

                    float wordscount = (timeElapsed - (float)39.2) * wordsPerSecond;
                    conversation("A", "Four brothers, huh? How did you keep them straight?", wordscount);
                    pause = 43.1f;
                }
                else if (timeElapsed > 43.0 && timeElapsed < 43.2)
                {
                    clearText();
                }

                else if (timeElapsed > 43.2 && timeElapsed < 52.0)
                {

                    float wordscount = (timeElapsed - (float)43.2) * wordsPerSecond;
                    conversation("T", "By who hit the hardest, which also happened to be oldest to youngest. Thomas, the twins, Taylor and Tyler, and then Trenton.", wordscount);
                    pause = 52.1f;
                }
                else if (timeElapsed > 52.0 && timeElapsed < 52.2)
                {
                    clearText();
                }

                else if (timeElapsed > 52.2 && timeElapsed < 61.0)
                {

                    float wordscount = (timeElapsed - (float)52.2) * wordsPerSecond;
                    conversation("T", "You never, ever got caught alone in a room with Taylor and Ty. I learned half of what I do in the The Circle from them.", wordscount);
                    pause = 61.1f;
                }
                else if (timeElapsed > 61.0 && timeElapsed < 61.2)
                {
                    clearText();
                }

                else if (timeElapsed > 61.2 && timeElapsed < 70.0)
                {

                    float wordscount = (timeElapsed - (float)61.2) * wordsPerSecond;
                    conversation("T", "Trenton was the smallest, but he’s fast. He’s the only one who can land a punch on me now.", wordscount);
                    pause = 70.1f;
                }
                else if (timeElapsed > 70.0 && timeElapsed < 70.2)
                {
                    clearText();
                }

                else if (timeElapsed > 70.2 && timeElapsed < 74.0)
                {

                    float wordscount = (timeElapsed - (float)70.2) * wordsPerSecond;
                    conversation("A", "Do they all have tattoos like you?", wordscount);
                    pause = 74.1f;
                }
                else if (timeElapsed > 74.0 && timeElapsed < 74.2)
                {
                    clearText();
                }

                else if (timeElapsed > 74.2 && timeElapsed < 79.0)
                {

                    float wordscount = (timeElapsed - (float)74.2) * wordsPerSecond;
                    conversation("T", "Pretty much. Except Thomas. He’s an ad exec in California.", wordscount);
                    pause = 79.1f;
                }
                else if (timeElapsed > 79.0 && timeElapsed < 79.2)
                {
                    clearText();
                }

                else if (timeElapsed > 79.2 && timeElapsed < 84.0)
                {

                    float wordscount = (timeElapsed - (float)79.2) * wordsPerSecond;
                    conversation("A", "And your dad? Where’s he?", wordscount);
                    pause = 84.1f;
                }
                else if (timeElapsed > 84.0 && timeElapsed < 84.2)
                {
                    abby.enabled = false;
                    clearText();
                }

                else if (timeElapsed > 84.2 && timeElapsed < 85.0)
                {
                    blurSkeleton.GetComponent<MeshRenderer>().sortingOrder = 19;
                    blurSkeleton.AnimationName = "animation";
                }

                else if (timeElapsed > 85.2 && timeElapsed < 89.0)
                {
                    blurSkeleton.loop = false;
                    blurSkeleton.GetComponent<MeshRenderer>().sortingOrder = -1;
                    float wordscount = (timeElapsed - (float)85.2) * wordsPerSecond;
                    conversation("A", "What are they laughing about?", wordscount);
                    pause = 89.1f;
                }
                else if (timeElapsed > 89.0 && timeElapsed < 89.2)
                {
                    clearText();
                }

                else if (timeElapsed > 89.2 && timeElapsed < 93.0)
                {

                    float wordscount = (timeElapsed - (float)89.2) * wordsPerSecond;
                    conversation("T", "Just ignore them. I’ll deal with them later.", wordscount);
                    pause = 93.1f;
                }
                else if (timeElapsed > 93.0)
                {
                    clearText();
                    travis.enabled = false;
                    buttonCanvas.enabled = true;
                }

            }

        }


        else if (level == "Scene10.8")
        {
            if (isOptionSelected)
            {
                if (timeElapsed < 3.0)
                {
                    if (isoptionA2)
                    {
                        float wordscount = timeElapsed * wordsPerSecond;
                        conversation("T", "Why can’t you just answer?", wordscount);
                        pause = 3.1f;
                    }
                    else
                    {
                        float wordscount = timeElapsed * wordsPerSecond;
                        conversation("T", "C’mon. You can tell me", wordscount);
                        pause = 3.1f;
                    }
                }

                else if (timeElapsed > 3.0 && timeElapsed < 3.2)
                {
                    clearText();
                }

                else if (timeElapsed > 3.2 && timeElapsed < 6.0)
                {
                    float wordscount = (timeElapsed - (float)3.2) * wordsPerSecond;
                    conversation("A", "It’s hard to explain. I guess it just felt right.", wordscount);
                    pause = 6.1f;
                }

                else if (timeElapsed > 6.0 && timeElapsed < 6.2)
                {
                    clearText();
                }

                else if (timeElapsed > 6.2 && timeElapsed < 7.0)
                {
                    float wordscount = (timeElapsed - (float)6.2) * wordsPerSecond;
                    conversation("T", "I know what you mean.", wordscount);
                    pause = 7.1f;
                }

                else if (timeElapsed > 7.0)
                {
                    nextScene();
                }

            }
            else
            {
                if (timeElapsed > 1.0 && timeElapsed < 3.6)
                {
                    travisSkeleton.AnimationName = "10 idle";
                    waiteressSkeleton.AnimationName = "10.2,10.8 enter";
                    waiteressSkeleton.timeScale = 1;
                }

                else if (timeElapsed > 3.6 && timeElapsed < 3.8)
                {
                    waiteressSkeleton.AnimationName = "10.1 idle";
                }
                else if (timeElapsed > 3.8 && timeElapsed < 8.0)
                {
                    float wordscount = (timeElapsed - (float)3.8) * wordsPerSecond;
                    conversation("W", "Here you are. One large pizza. Two plates. Anything else.", wordscount);
                    pause = 8.1f;
                }
                else if (timeElapsed > 8.0 && timeElapsed < 8.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 8.2 && timeElapsed < 10.0)
                {
                    float wordscount = (timeElapsed - (float)8.2) * wordsPerSecond;
                    conversation("T", "Nah, this is great, Jess, thanks.", wordscount);
                    pause = 10.1f;
                }
                else if (timeElapsed > 10.0 && timeElapsed < 10.2)
                {
                    activeButton = false;
                    travis.enabled = false;
                    clearText();
                }
                else if (timeElapsed > 10.2 && timeElapsed < 12.2)
                {
                    waiteressSkeleton.AnimationName = "10.2,10.8 exit";
                }
                else if (timeElapsed > 12.2 && timeElapsed < 13.2)
                {

                    zoomIn(2.03f, new Vector3(1.31f, 0.48f, -10f));
                }
                else if (timeElapsed > 13.2 && timeElapsed < 14.6)
                {
                    waiteressSkeleton.timeScale = 0;
                    if (SceneSix.isDirty)
                        abbySkeleton.AnimationName = "10  takes bite dirty";
                    else
                        abbySkeleton.AnimationName = "10  takes bite";
                }
                else if (timeElapsed >= 14.6 && timeElapsed < 15.4)
                {
                    float wordscount = (timeElapsed - (float)14.6) * wordsPerSecond;
                    conversation("A", "hums. It’s good", wordscount);
                    pause = 15.5f;
                }
                else if (timeElapsed > 15.4 && timeElapsed < 15.6)
                {
                    abby.enabled = false;
                    clearText();
                }
                else if (timeElapsed > 15.6 && timeElapsed < 16.8)
                {
                    zoomOut();
                }
                else if (timeElapsed >= 16.8 && timeElapsed < 18.0)
                {
                    activeButton = true;
                    float wordscount = (timeElapsed - (float)16.6) * wordsPerSecond;
                    conversation("T", "Sorry about that.", wordscount);
                    pause = 18.1f;
                }
                else if (timeElapsed > 18.0 && timeElapsed < 18.2)
                {
                    clearText();
                }

                else if (timeElapsed >= 19.2 && timeElapsed < 21.0)
                {
                    float wordscount = (timeElapsed - (float)19.2) * wordsPerSecond;
                    conversation("A", "You should try some.", wordscount);
                    pause = 21.1f;
                }
                else if (timeElapsed > 21.0 && timeElapsed < 21.2)
                {
                    abby.enabled = false;
                    clearText();
                }
                else if (timeElapsed > 21.2 && timeElapsed < 22.2)
                {
                    if (SceneSix.isDirty)
                        abbySkeleton.AnimationName = "10  takes bite dirty";
                    else
                        abbySkeleton.AnimationName = "10  takes bite";
                }
                else if (timeElapsed >= 22.2 && timeElapsed < 24.0)
                {
                    float wordscount = (timeElapsed - (float)22.2) * wordsPerSecond;
                    conversation("T", "You didn’t answer my question.", wordscount);
                    pause = 24.1f;
                }
                else if (timeElapsed > 24.0 && timeElapsed < 24.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 24.2 && timeElapsed < 26.0)
                {
                    float wordscount = (timeElapsed - (float)24.2) * wordsPerSecond;
                    conversation("A", "Which one?", wordscount);
                    pause = 26.1f;
                }
                else if (timeElapsed > 26.0 && timeElapsed < 26.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 26.2 && timeElapsed < 28.0)
                {
                    float wordscount = (timeElapsed - (float)26.2) * wordsPerSecond;
                    conversation("T", "About why you chose Eastern.", wordscount);
                    pause = 28.1f;
                }
                else if (timeElapsed > 28.0 && timeElapsed < 28.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 28.2 && timeElapsed < 30.0)
                {
                    float wordscount = (timeElapsed - (float)28.2) * wordsPerSecond;
                    conversation("A", "That was on purpose.", wordscount);
                    pause = 30.1f;
                }
                else if (timeElapsed > 30.0 && timeElapsed < 30.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 30.2 && timeElapsed < 32.0)
                {
                    float wordscount = (timeElapsed - (float)30.2) * wordsPerSecond;
                    conversation("T", "What? It’s a secret?", wordscount);
                    pause = 32.1f;
                }
                else if (timeElapsed > 32.0 && timeElapsed < 32.2)
                {
                    clearText();
                    travis.enabled = false;
                    buttonCanvas.enabled = true;
                }

            }
        }

    }


    private void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
        travisText.text = textShownOnScreen;
        if(waiterText != null)
            waiterText.text = textShownOnScreen;
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "A")
                abbyText.text = textShownOnScreen;
            else if (character == "T")
                travisText.text = textShownOnScreen;

            else if (character == "W")
                waiterText.text = textShownOnScreen;

            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }

    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;
        setAnimation(character, text);
        if (character == "A")
        {
            abby.enabled = true;
            travis.enabled = false;
            if(waiter!=null)
                waiter.enabled = false;
        }

        else if (character == "T")
        {
            abby.enabled = false;
            travis.enabled = true;
            if (waiter != null)
                waiter.enabled = false;
        }
        else if (character == "W")
        {
            abby.enabled = false;
            travis.enabled = false;
            if (waiter != null)
                waiter.enabled = true;
        }
        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }

    public void nextScene()
    {
        level = "Scene16.1";
        SceneManager.LoadScene(level);
    }

    public void clicked()
    {
        timeElapsed = 0;
        isOptionSelected = true;
        multibuttonCanvas.enabled = false;
    }

    public void playPause()
    {
        if (activeButton) { 
        if (!textSet && savetext != "")
        {
            textSet = true;
            setTexts(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }
    }

    private void setTexts(string character, string text)
    {
        if (character == "A")
            abbyText.text = text;
        else if (character == "T")
            travisText.text = text;
        else if (character == "W")
            waiterText.text = text;
    }

    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }


    private void setAnimation(string character, string text)
    {
        if (character == "A")
        {
            if (level == "Scene10.1")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 dialouge dirty";
                else
                    abbySkeleton.AnimationName = "10 dialouge clean";
                if (waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.1 idle";

                travisSkeleton.AnimationName = "10 idle";
            }

            else if(level == "Scene10.2")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 dialouge dirty";
                else
                    abbySkeleton.AnimationName = "10 dialouge clean";
                if (waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.2,10.8  idle";

                travisSkeleton.AnimationName = "10  beer bottle idle";
            }

            else if (level == "Scene10.5")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 dialouge dirty";
                else
                    abbySkeleton.AnimationName = "10 dialouge clean";

                travisSkeleton.AnimationName = "10  beer bottle idle";
            }

            else if (level == "Scene10.8")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 dialouge dirty";
                else
                    abbySkeleton.AnimationName = "10 dialouge clean";

                travisSkeleton.AnimationName = "10  beer bottle dialogue";
            }
        }

        else if (character == "T")
        {
            if (level == "Scene10.1")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 idle dirty";
                else
                    abbySkeleton.AnimationName = "10 idle clean";
                if (waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.1 idle";
                travisSkeleton.AnimationName = "10.1 dialogue";
            }
            else if (level == "Scene10.2")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 dialouge dirty";
                else
                    abbySkeleton.AnimationName = "10 dialouge clean";
                if (waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.2,10.8  idle";

                if (text == "You’re breaking my heart, Pidge!")
                    travisSkeleton.AnimationName = "10. fist to chest";
                else
                    travisSkeleton.AnimationName = "10  beer bottle dialogue";
            }

            else if (level == "Scene10.5")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 dialouge dirty";
                else
                    abbySkeleton.AnimationName = "10 dialouge clean";

                travisSkeleton.AnimationName = "10  beer bottle dialogue";
            }

            else if (level == "Scene10.8")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 dialouge dirty";
                else
                    abbySkeleton.AnimationName = "10 dialouge clean";

                travisSkeleton.AnimationName = "10  beer bottle dialogue";
            }
        }
        else if (character == "W")
        {
            if (level == "Scene10.1")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 idle dirty";
                else
                    abbySkeleton.AnimationName = "10 idle clean";
                if (waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.1 idle";
                travisSkeleton.AnimationName = "10 idle";
            }
            else if (level == "Scene10.2")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 dialouge dirty";
                else
                    abbySkeleton.AnimationName = "10 dialouge clean";
                if (waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.2, 10.8 dialogue";

                travisSkeleton.AnimationName = "10  beer bottle idle";
            }
            else if (level == "Scene10.8")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 dialouge dirty";
                else
                    abbySkeleton.AnimationName = "10 dialouge clean";
                if (waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.2, 10.8 dialogue";

                travisSkeleton.AnimationName = "10  beer bottle idle";
            }
        }
    }


    public void clickedA()
    {
        timeElapsed = 0;
        buttonCanvas.enabled = false;

        if (isOptionSelected)
        {
            isOptionSelected2 = true;
            isoptionA2 = true;
        }

        else
        {
            isOptionSelected = true;
            isoptionA = true;
        }
    }

    public void clickedB()
    {
        timeElapsed = 0;
        buttonCanvas.enabled = false;

        if (isOptionSelected)
        {
            isOptionSelected2 = true;
            isoptionA2 = false;
        }

        else
        {
            isOptionSelected = true;
            isoptionA = false;
        }
    }
}
