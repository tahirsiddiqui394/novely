﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneTen : MonoBehaviour {

    public Text abbyText;
    public Text travisText;
    public Text waiterText;
    public float wordsPerSecond = 20; // speed of typewriter
    private float timeElapsed = 0;
    public string textShownOnScreen;
    public Image abby;
    public Image waiter;
    public Image travis;
    private float timer = 0;
    private float timerMax = 0;
    public string level = "";
    public Canvas next;
    public Text optAText;
    public Text optBText;
    public Boolean isOptionSelected, isOption2Selected , isOption3Selected;
    public Canvas buttonCanvas, multibuttonCanvas;
    String option = "";
    public SkeletonAnimation abbySkeleton;
    public SkeletonAnimation travisSkeleton;
    public SkeletonAnimation waiteressSkeleton;
    public string savetext = "";
    public string saveState = "";
    bool textSet = false;
    public Camera mainCamera;
    float pause = 1;

    // Use this for initialization
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        abby.enabled = false;
        travis.enabled = false;
        waiter.enabled = false;
        buttonCanvas.enabled = false;
        multibuttonCanvas.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (!Waited(1))
            return;

        timeElapsed += Time.deltaTime;

        if (level == "Scene10.1")
        {
            if (isOptionSelected)
            {
                if (isOption2Selected) {


                    if (isOption3Selected)
                    {
                        if(option == "A")
                        {
                            if (timeElapsed < 6.0)
                            {
                                float wordscount = timeElapsed * wordsPerSecond;
                                conversation("A", "I thought about trying out for a lifeguard at the aquatic center on campus.", wordscount);
                                pause = 6.1f;
                            }
                            else if (timeElapsed > 6 && timeElapsed < 6.2)
                            {
                                clearText();
                            }
                            else if (timeElapsed >= 6.2 && timeElapsed < 9.0)
                            {
                                float wordscount = (timeElapsed - (float)6.2) * wordsPerSecond;
                                conversation("T", "I’d rather you give me mouth to mouth than Pete.", wordscount);
                                pause = 9.1f;
                            }
                            else if (timeElapsed > 9.0 && timeElapsed < 9.2)
                            {
                                clearText();
                            }

                            else if (timeElapsed >= 9.2 && timeElapsed < 12.0)
                            {
                                float wordscount = (timeElapsed - (float)9.2) * wordsPerSecond;
                                conversation("T", "He’s there every time I go.", wordscount);
                                pause = 12.1f;
                            }
                            else if (timeElapsed >12.0 )
                            {
                                clearText();
                                travis.enabled = false;
                                level = "Scene10.2";
                                SceneManager.LoadScene(level);
                            }
                        }

                        else if(option == "B")
                        {
                            if (timeElapsed < 4.0)
                            {
                                float wordscount = timeElapsed * wordsPerSecond;
                                conversation("A", " Mostly card games, really.", wordscount);
                                pause = 4.1f;
                            }
                            else if (timeElapsed > 4 && timeElapsed < 4.2)
                            {
                                clearText();
                            }
                            else if (timeElapsed >= 4.2 && timeElapsed < 8.0)
                            {
                                float wordscount = (timeElapsed - (float)4.2) * wordsPerSecond;
                                conversation("T", "Oh yeah? My family has a poker night at least once a month.", wordscount);
                                pause = 8.1f;
                            }
                            else if (timeElapsed > 8.0 && timeElapsed < 8.2)
                            {
                                clearText();
                            }

                            else if (timeElapsed >= 8.2 && timeElapsed < 12.0)
                            {
                                float wordscount = (timeElapsed - (float)8.2) * wordsPerSecond;
                                conversation("T", " House full of smoke.My brothers are a bunch of cry babies.", wordscount);
                                pause = 12.1f;
                            }
                            else if (timeElapsed > 12.0)
                            {
                                clearText();
                                travis.enabled = false;
                                level = "Scene10.2";
                                SceneManager.LoadScene(level);
                            }
                        }

                        else if (option == "C")
                        {
                            if (timeElapsed < 4.0)
                            {
                                float wordscount = timeElapsed * wordsPerSecond;
                                conversation("A", " Lately I’m obsessed with portraits.", wordscount);
                                pause = 4.1f;
                            }
                            else if (timeElapsed > 4 && timeElapsed < 4.2)
                            {
                                clearText();
                            }
                            else if (timeElapsed >= 4.2 && timeElapsed < 10.0)
                            {
                                float wordscount = (timeElapsed - (float)4.2) * wordsPerSecond;
                                conversation("T", " I’d like to see some of your photos sometime.I like black and whites, mostly.", wordscount);
                                pause = 10.1f;
                            }
                            else if (timeElapsed > 10.0 && timeElapsed < 10.2)
                            {
                                clearText();
                            }

                            else if (timeElapsed >= 10.2 && timeElapsed < 16.0)
                            {
                                float wordscount = (timeElapsed - (float)10.2) * wordsPerSecond;
                                conversation("T", "Kind of like slow - mo, just makes everything look cooler.", wordscount);
                                pause = 13.1f;
                            }
                            else if (timeElapsed > 13.0)
                            {
                                clearText();
                                travis.enabled = false;
                                level = "Scene10.2";
                                SceneManager.LoadScene(level);
                            }
                        }

                        else if (option == "D")
                        {
                            if (timeElapsed < 4.0)
                            {
                                float wordscount = timeElapsed * wordsPerSecond;
                                conversation("A", " But I can’t sing.At all.", wordscount);
                                pause = 4.1f;
                            }
                            else if (timeElapsed > 4 && timeElapsed < 4.2)
                            {
                                clearText();
                            }
                            else if (timeElapsed >= 4.2 && timeElapsed < 19.0)
                            {
                                float wordscount = (timeElapsed - (float)4.2) * wordsPerSecond;
                                conversation("T", "So, you’re saying karaoke is not in our future ? ", wordscount);
                                pause = 9.1f;
                            }
                            else if (timeElapsed > 9.0)
                            {
                                clearText();
                                travis.enabled = false;
                                level = "Scene10.2";
                                SceneManager.LoadScene(level);
                            }
                        }

                        else if (option == "E")
                        {
                            if (timeElapsed < 5.0)
                            {
                                float wordscount = timeElapsed * wordsPerSecond;
                                conversation("A", "But the older I get, the less realistic it seems.", wordscount);
                                pause = 5.1f;
                            }
                            else if (timeElapsed > 5 && timeElapsed < 5.2)
                            {
                                clearText();
                            }
                            else if (timeElapsed >= 5.2 && timeElapsed < 11.0)
                            {
                                float wordscount = (timeElapsed - (float)5.2) * wordsPerSecond;
                                conversation("T", "You’re definitely rocking that heather gray cotton number Very grunge chic.", wordscount);
                                pause = 11.1f;
                            }
                            else if (timeElapsed > 11.0)
                            {
                                clearText();
                                travis.enabled = false;
                                level = "Scene10.2";
                                SceneManager.LoadScene(level);
                            }
                        }

                        else if (option == "F")
                        {
                            if (timeElapsed < 5.0)
                            {
                                float wordscount = timeElapsed * wordsPerSecond;
                                conversation("A", ".Differential equations are sort of my thing.", wordscount);
                                pause = 5.1f;
                            }
                            else if (timeElapsed > 5 && timeElapsed < 5.2)
                            {
                                clearText();
                            }
                            else if (timeElapsed >= 5.2 && timeElapsed < 10.0)
                            {
                                float wordscount = (timeElapsed - (float)5.2) * wordsPerSecond;
                                conversation("T", " Not my best subject, but I manage to keep up.Impressive.", wordscount);
                                pause = 10.1f;
                            }
                            else if (timeElapsed > 10.0)
                            {
                                clearText();
                                travis.enabled = false;
                                level = "Scene10.2";
                                SceneManager.LoadScene(level);
                            }
                        }

                    }
                    else
                    {
                        if (timeElapsed < 4.0)
                        {
                            float wordscount = timeElapsed * wordsPerSecond;
                            conversation("T", "Tell me something about yourself. I’m intrigued.", wordscount);
                            pause = 4.1f;
                        }
                        else if (timeElapsed > 4 && timeElapsed < 4.2)
                        {
                            clearText();
                        }
                        else if (timeElapsed >= 4.2 && timeElapsed < 7.0)
                        {
                            float wordscount = (timeElapsed - (float)4.2) * wordsPerSecond;
                            conversation("A", "I’m really into...", wordscount);
                            pause = 7.1f;
                        }
                        else if (timeElapsed > 7.0)
                        {
                            clearText();
                            abby.enabled = false;
                            buttonCanvas.enabled = false;
                            multibuttonCanvas.enabled = true;
                        }
                    }

                }

                else
                {
                    if(option == "A")
                    {
                        if (timeElapsed < 6.0)
                        {
                            float wordscount = timeElapsed * wordsPerSecond;
                            conversation("T", "I can’t figure you out. You’re the first girl who’s ever been disgusted with me before sex.", wordscount);
                            pause = 6.1f;
                        }
                        else if (timeElapsed > 6 && timeElapsed < 6.2)
                        {
                            clearText();
                        }
                        else if (timeElapsed >= 6.2 && timeElapsed < 12.0)
                        {
                            float wordscount = (timeElapsed - (float)6.2) * wordsPerSecond;
                            conversation("T", "You don’t get all flustered when you talk to me, and you don’t try to get my attention.", wordscount);
                            pause = 12.1f;
                        }
                        else if (timeElapsed > 12.0 && timeElapsed < 12.2)
                        {
                            clearText();
                        }
                        else if (timeElapsed >= 12.2 && timeElapsed < 16.0)
                        {
                            float wordscount = (timeElapsed - (float)12.2) * wordsPerSecond;
                            conversation("A", "I don’t think you’re a bad person.", wordscount);
                            pause = 16.1f;
                        }
                        else if (timeElapsed > 16.0 && timeElapsed < 16.2)
                        {
                            clearText();
                        }
                        else if (timeElapsed >= 16.2 && timeElapsed < 21.0)
                        {
                            float wordscount = (timeElapsed - (float)16.2) * wordsPerSecond;
                            conversation("A", "I just don’t like being a forgone conclusion for the sole reason of having a vagina.", wordscount);
                            pause = 21.1f;
                        }
                        else if (timeElapsed > 21.0 && timeElapsed < 21.2)
                        {
                            abby.enabled = false;
                            clearText();
                        }
                        else if (timeElapsed >= 21.2 && timeElapsed < 23.0)
                        {
                                travisSkeleton.AnimationName = "10.1 laugh";
                        }

                        else if (timeElapsed >= 23.2 && timeElapsed < 28.0)
                        {
                            float wordscount = (timeElapsed - (float)23.2) * wordsPerSecond;
                            conversation("T", "Oh, my God! You’re killing me!", wordscount);
                            pause = 28.1f;
                        }
                        else if (timeElapsed > 28.0 && timeElapsed < 28.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed >= 28.2 && timeElapsed < 31.0)
                        {
                            float wordscount = (timeElapsed - (float)28.2) * wordsPerSecond;
                            conversation("T", "That’s it. We have to be friends. I won’t take no for an answer.", wordscount);
                            pause = 31.1f;
                        }
                        else if (timeElapsed > 27.0 && timeElapsed < 27.2)
                        {
                            clearText();
                        }
                        else if (timeElapsed >= 27.2 && timeElapsed < 29.0)
                        {
                            float wordscount = (timeElapsed - (float)27.2) * wordsPerSecond;
                            conversation("A", "I told you...", wordscount);
                            pause = 29.1f;
                        }
                        else if (timeElapsed > 29.0 && timeElapsed < 29.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed >= 29.2 && timeElapsed < 32.0)
                        {
                            float wordscount = (timeElapsed - (float)29.2) * wordsPerSecond;
                            conversation("T", "You’re not sleeping with me, I get it.", wordscount);
                            pause = 32.1f;
                        }
                        else if (timeElapsed > 32.0)
                        {
                            clearText();
                            travis.enabled = false;
                            optAText.text = "Disappointed.";
                            optBText.text = "Content.";
                            buttonCanvas.enabled = true;
                        }

                    }

                    else
                    {
                        if (timeElapsed < 6.0)
                        {
                            float wordscount = timeElapsed * wordsPerSecond;
                            conversation("T", " What if I said I could ?", wordscount);
                            pause = 6.1f;
                        }
                        else if (timeElapsed > 6 && timeElapsed < 6.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed >= 6.2 && timeElapsed < 8.0)
                        {
                            float wordscount = (timeElapsed - (float)6.2) * wordsPerSecond;
                            conversation("A", "I don’t think you’re a bad person.", wordscount);
                            pause = 8.1f;
                        }
                        else if (timeElapsed > 8.0 && timeElapsed < 8.2)
                        {
                            clearText();
                        }
                        else if (timeElapsed >= 8.2 && timeElapsed < 13.0)
                        {
                            float wordscount = (timeElapsed - (float)8.2) * wordsPerSecond;
                            conversation("A", "I just don’t like being a forgone conclusion for the sole reason of having a vagina.", wordscount);
                            pause = 13.1f;
                        }
                        else if (timeElapsed > 13.0 && timeElapsed < 13.2)
                        {
                            abby.enabled = false;
                            clearText();
                        }

                        else if (timeElapsed >= 13.2 && timeElapsed < 13.9)
                        {
                            travisSkeleton.AnimationName = "10.1 laugh";
                        }


                        else if (timeElapsed >= 14 && timeElapsed < 16.0)
                        {
                            float wordscount = (timeElapsed - (float)14) * wordsPerSecond;
                            conversation("T", "Oh, my God! You’re killing me!", wordscount);
                            pause = 16.1f;
                        }
                        else if (timeElapsed > 16.0 && timeElapsed < 16.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed >= 16.2 && timeElapsed < 20.0)
                        {
                            float wordscount = (timeElapsed - (float)16.2) * wordsPerSecond;
                            conversation("T", "That’s it. We have to be friends. I won’t take no for an answer.", wordscount);
                            pause = 20.1f;
                        }
                        else if (timeElapsed > 20.0 && timeElapsed < 20.2)
                        {
                            clearText();
                        }
                        else if (timeElapsed >= 20.2 && timeElapsed < 22.0)
                        {
                            float wordscount = (timeElapsed - (float)20.2) * wordsPerSecond;
                            conversation("A", "I told you...", wordscount);
                            pause = 22.1f;
                        }
                        else if (timeElapsed > 22.0 && timeElapsed < 22.2)
                        {
                            clearText();
                        }

                        else if (timeElapsed >= 22.2 && timeElapsed < 25.0)
                        {
                            float wordscount = (timeElapsed - (float)22.2) * wordsPerSecond;
                            conversation("T", "You’re not sleeping with me, I get it.", wordscount);
                            pause = 25.1f;
                        }
                        else if (timeElapsed > 25.0 )
                        {
                            clearText();
                            travis.enabled = false;
                            optAText.text = "Disappointed.";
                            optBText.text = "Content.";
                            buttonCanvas.enabled = true;
                        }

                    }

                }
            }
            else
            {

                if (timeElapsed < 3.0)
                {
                    float wordscount = timeElapsed * wordsPerSecond;
                    conversation("W", "Two beers?", wordscount);
                    pause = 3.1f;
                }
                else if (timeElapsed > 3 && timeElapsed < 3.2)
                {
                    waiter.enabled = false;
                    clearText();
                }


                else if (timeElapsed > 3.3 && timeElapsed < 3.8)
                {
                    travisSkeleton.AnimationName = "10.1 two beer";
                    pause = 3.10f;
                }


                else if (timeElapsed >= 3.9 && timeElapsed < 5.5)
                {
                    float wordscount = (timeElapsed - (float)3.9) * wordsPerSecond;
                    conversation("T", "Please, Jess. Thanks.", wordscount);
                    pause = 5.6f;
                }
                else if (timeElapsed > 5.5 && timeElapsed < 5.7)
                {
                    clearText();
                }
                else if (timeElapsed >= 5.7 && timeElapsed < 7)
                {
                    float wordscount = (timeElapsed - (float)5.7) * wordsPerSecond;
                    conversation("W", "Sure, Travis.", wordscount);
                    waiteressSkeleton.AnimationName = "10.1 sure travis (exit)";
                    pause = 7.3f;
                }

                else if (timeElapsed >= 7.2 && timeElapsed < 7.5)
                {

                    clearText();
                    waiter.enabled = false;
                    pause = 11.8f;

                }

                else if (timeElapsed > 11.7 && timeElapsed < 12)
                {
                    waiteressSkeleton.GetComponent<MeshRenderer>().sortingOrder = -1;
                    clearText();
                }
                else if (timeElapsed >= 12.2 && timeElapsed < 16.0)
                {
                   
                    float wordscount = (timeElapsed - (float)12.2) * wordsPerSecond;
                    conversation("A", "They just serve alcohol to minors, huh?", wordscount);
                    pause = 16.3f;
                }

                else if (timeElapsed > 16.2 && timeElapsed < 16.4)
                {
                    clearText();
                }
                else if (timeElapsed >= 16.6 && timeElapsed < 19.0)
                {
                    float wordscount = (timeElapsed - (float)16.6) * wordsPerSecond;
                    conversation("T", "I tip her well.", wordscount);
                    pause = 19.3f;
                }
                else if (timeElapsed > 19.2 && timeElapsed < 19.4)
                {
                    clearText();
                }
                else if (timeElapsed >= 19.6 && timeElapsed < 23)
                {
                    float wordscount = (timeElapsed - (float)19.6) * wordsPerSecond;
                    conversation("A", "I’m sure that’s what it is.", wordscount);
                    pause = 23.1f;
                }

                else if (timeElapsed > 23 && timeElapsed < 23.2)
                {
                    clearText();
                }

                else if (timeElapsed >= 23.2 && timeElapsed < 26.0)
                {
                    float wordscount = (timeElapsed - (float)23.2) * wordsPerSecond;
                    conversation("T", "So, what’s your story Pidge?", wordscount);
                    pause = 26.1f;
                }

                else if (timeElapsed > 26.0 && timeElapsed < 26.2)
                {
                    clearText();
                }
                else if (timeElapsed >= 26.3 && timeElapsed < 31.0)
                {
                    float wordscount = (timeElapsed - (float)26.3) * wordsPerSecond;
                    conversation("T", "Are you a man-hater in general, or do you jus that me?", wordscount);
                    pause = 31.3f;
                }
                else if (timeElapsed > 31.0)
                {
                    clearText();
                    travis.enabled = false;
                    buttonCanvas.enabled = true;
                }

            }
        }

    }

    public void nextScene()
    {
        level = "Scene16.1";
        SceneManager.LoadScene(level);
    }

    private void clearText()
    {
        saveState = "";
        savetext = "";
        textSet = false;
        textShownOnScreen = "";
        abbyText.text = textShownOnScreen;
        travisText.text = textShownOnScreen;
        waiterText.text = textShownOnScreen;
    }

    private string GetWords(string character, string texts, float wordCount)
    {
        float words = wordCount;
        // loop through each character in text
        for (int i = 0; i < texts.Length; i++)
        {
            words--;
            if (character == "A")
                abbyText.text = textShownOnScreen;
            else if (character == "T")
                travisText.text = textShownOnScreen;

            else if (character == "W")
                waiterText.text = textShownOnScreen;

            if (words <= 0)
            {
                return texts.Substring(0, i);
            }
        }
        return texts;
    }

    public void conversation(string character, string text, float wordCount)
    {
        saveState = character;
        savetext = text;
        setAnimation(character, text);
        if (character == "A")
        {
            abby.enabled = true;
            travis.enabled = false;
            waiter.enabled = false;
        }

        else if (character == "T")
        {
            abby.enabled = false;
            travis.enabled = true;
            waiter.enabled = false;
        }
        else if (character == "W")
        {
            abby.enabled = false;
            travis.enabled = false;
            waiter.enabled = true;
        }
        if (!textSet)
            textShownOnScreen = GetWords(character, text, wordCount);
    }

    private bool Waited(float seconds)
    {
        timerMax = seconds;

        timer += Time.deltaTime;

        if (timer >= timerMax)
        {
            return true; //max reached - waited x - seconds
        }

        return false;
    }

    public void clickedA()
    {
        timeElapsed = 0;
        buttonCanvas.enabled = false;

        if (isOptionSelected)
        {
            isOption2Selected = true;
        }

        else
        {
            isOptionSelected = true;
            option = "A";
        }
    }

    public void clickedB()
    {
        timeElapsed = 0;
        buttonCanvas.enabled = false;

        if (isOptionSelected)
        {
            isOption2Selected = true;
        }

        else
        {
            isOptionSelected = true;
            option = "B";
        }
    }

    public void clickedOption(string str)
    {
        option = str;
        timeElapsed = 0;
        multibuttonCanvas.enabled = false;
        isOption3Selected = true;
    }



    public void playPause()
    {
        if (!textSet && savetext != "")
        {
            textSet = true;
            setTexts(saveState, savetext);
        }
        else
        {
            clearText();
            timeElapsed = pause;
        }
    }

    private void setTexts(string character, string text)
    {
        if (character == "A")
            abbyText.text = text;
        else if (character == "T")
            travisText.text = text;
        else if (character == "W")
            waiterText.text = text;
            }

    public void zoomIn(float orthographicSize, Vector3 v)
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, orthographicSize, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, v, 0.05f);
    }

    public void zoomOut()
    {
        mainCamera.orthographicSize = Mathf.Lerp(mainCamera.orthographicSize, 5, 0.05f);
        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(0f, 0f, -10), 0.05f);
    }


    private void setAnimation(string character, string text)
    {
        if (character == "A")
        {
            if (level == "Scene10.1")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 dialouge dirty";
                else
                    abbySkeleton.AnimationName = "10 dialouge clean";
                if(waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.1 idle";
           
                    travisSkeleton.AnimationName = "10 idle";
            }
        }

        else if (character == "T")
        {
            if (level == "Scene10.1")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 idle dirty";
                else
                    abbySkeleton.AnimationName = "10 idle clean";
                if (waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.1 idle";
                travisSkeleton.AnimationName = "10.1 dialogue";
            }
        }
        else if (character == "W")
        {
            if (level == "Scene10.1")
            {
                if (SceneSix.isDirty)
                    abbySkeleton.AnimationName = "10 idle dirty";
                else
                    abbySkeleton.AnimationName = "10 idle clean";
                if (waiteressSkeleton.loop)
                    waiteressSkeleton.AnimationName = "10.1 idle";
                travisSkeleton.AnimationName = "10 idle";
            }
        }
    }
}
