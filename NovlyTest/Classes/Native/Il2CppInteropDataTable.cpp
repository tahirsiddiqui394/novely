﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"








extern "C" void Context_t3877589556_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Context_t3877589556_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Context_t3877589556_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Context_t3877589556_0_0_0;
extern "C" void Escape_t1880027543_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Escape_t1880027543_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Escape_t1880027543_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Escape_t1880027543_0_0_0;
extern "C" void PreviousInfo_t3752465461_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PreviousInfo_t3752465461_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PreviousInfo_t3752465461_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PreviousInfo_t3752465461_0_0_0;
extern "C" void DelegatePInvokeWrapper_AppDomainInitializer_t4005868378();
extern const RuntimeType AppDomainInitializer_t4005868378_0_0_0;
extern "C" void DelegatePInvokeWrapper_Swapper_t1905224008();
extern const RuntimeType Swapper_t1905224008_0_0_0;
extern "C" void DictionaryEntry_t2732573845_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DictionaryEntry_t2732573845_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DictionaryEntry_t2732573845_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DictionaryEntry_t2732573845_0_0_0;
extern "C" void Slot_t2420837462_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t2420837462_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t2420837462_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t2420837462_0_0_0;
extern "C" void Slot_t1498351365_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Slot_t1498351365_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Slot_t1498351365_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Slot_t1498351365_0_0_0;
extern "C" void Enum_t1912704450_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Enum_t1912704450_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Enum_t1912704450_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Enum_t1912704450_0_0_0;
extern "C" void DelegatePInvokeWrapper_ReadDelegate_t498133763();
extern const RuntimeType ReadDelegate_t498133763_0_0_0;
extern "C" void DelegatePInvokeWrapper_WriteDelegate_t1654929309();
extern const RuntimeType WriteDelegate_t1654929309_0_0_0;
extern "C" void MonoIOStat_t296773048_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoIOStat_t296773048_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoIOStat_t296773048_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoIOStat_t296773048_0_0_0;
extern "C" void MonoEnumInfo_t495278953_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEnumInfo_t495278953_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEnumInfo_t495278953_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEnumInfo_t495278953_0_0_0;
extern "C" void CustomAttributeNamedArgument_t2964753189_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeNamedArgument_t2964753189_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeNamedArgument_t2964753189_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeNamedArgument_t2964753189_0_0_0;
extern "C" void CustomAttributeTypedArgument_t1279663203_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CustomAttributeTypedArgument_t1279663203_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CustomAttributeTypedArgument_t1279663203_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CustomAttributeTypedArgument_t1279663203_0_0_0;
extern "C" void ILTokenInfo_t511186550_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ILTokenInfo_t511186550_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ILTokenInfo_t511186550_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ILTokenInfo_t511186550_0_0_0;
extern "C" void MonoEventInfo_t4102204480_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoEventInfo_t4102204480_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoEventInfo_t4102204480_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoEventInfo_t4102204480_0_0_0;
extern "C" void MonoMethodInfo_t520497436_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoMethodInfo_t520497436_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoMethodInfo_t520497436_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoMethodInfo_t520497436_0_0_0;
extern "C" void MonoPropertyInfo_t3894631283_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MonoPropertyInfo_t3894631283_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MonoPropertyInfo_t3894631283_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MonoPropertyInfo_t3894631283_0_0_0;
extern "C" void ParameterModifier_t1293521645_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ParameterModifier_t1293521645_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ParameterModifier_t1293521645_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ParameterModifier_t1293521645_0_0_0;
extern "C" void ResourceCacheItem_t484540818_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceCacheItem_t484540818_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceCacheItem_t484540818_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceCacheItem_t484540818_0_0_0;
extern "C" void ResourceInfo_t2441261143_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceInfo_t2441261143_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceInfo_t2441261143_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceInfo_t2441261143_0_0_0;
extern "C" void DelegatePInvokeWrapper_CrossContextDelegate_t2164444586();
extern const RuntimeType CrossContextDelegate_t2164444586_0_0_0;
extern "C" void DelegatePInvokeWrapper_CallbackHandler_t661518983();
extern const RuntimeType CallbackHandler_t661518983_0_0_0;
extern "C" void SerializationEntry_t2008081023_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SerializationEntry_t2008081023_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SerializationEntry_t2008081023_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SerializationEntry_t2008081023_0_0_0;
extern "C" void StreamingContext_t4227869630_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void StreamingContext_t4227869630_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void StreamingContext_t4227869630_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType StreamingContext_t4227869630_0_0_0;
extern "C" void DSAParameters_t1524829693_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void DSAParameters_t1524829693_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void DSAParameters_t1524829693_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType DSAParameters_t1524829693_0_0_0;
extern "C" void RSAParameters_t2694455455_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RSAParameters_t2694455455_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RSAParameters_t2694455455_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RSAParameters_t2694455455_0_0_0;
extern "C" void SecurityFrame_t748984313_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SecurityFrame_t748984313_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SecurityFrame_t748984313_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SecurityFrame_t748984313_0_0_0;
extern "C" void DelegatePInvokeWrapper_ThreadStart_t815992611();
extern const RuntimeType ThreadStart_t815992611_0_0_0;
extern "C" void ValueType_t2697047229_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ValueType_t2697047229_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ValueType_t2697047229_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ValueType_t2697047229_0_0_0;
extern "C" void X509ChainStatus_t1809129440_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void X509ChainStatus_t1809129440_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void X509ChainStatus_t1809129440_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType X509ChainStatus_t1809129440_0_0_0;
extern "C" void IntStack_t546104939_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void IntStack_t546104939_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void IntStack_t546104939_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType IntStack_t546104939_0_0_0;
extern "C" void Interval_t3994977115_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Interval_t3994977115_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Interval_t3994977115_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Interval_t3994977115_0_0_0;
extern "C" void DelegatePInvokeWrapper_CostDelegate_t3359819672();
extern const RuntimeType CostDelegate_t3359819672_0_0_0;
extern "C" void UriScheme_t2335814732_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void UriScheme_t2335814732_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void UriScheme_t2335814732_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType UriScheme_t2335814732_0_0_0;
extern "C" void DelegatePInvokeWrapper_Action_t3158581658();
extern const RuntimeType Action_t3158581658_0_0_0;
extern "C" void AnimationCurve_t1417462441_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationCurve_t1417462441_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationCurve_t1417462441_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationCurve_t1417462441_0_0_0;
extern "C" void AnimationEvent_t3272212093_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationEvent_t3272212093_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationEvent_t3272212093_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationEvent_t3272212093_0_0_0;
extern "C" void AnimatorTransitionInfo_t3177004460_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimatorTransitionInfo_t3177004460_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimatorTransitionInfo_t3177004460_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimatorTransitionInfo_t3177004460_0_0_0;
extern "C" void DelegatePInvokeWrapper_LogCallback_t3941575221();
extern const RuntimeType LogCallback_t3941575221_0_0_0;
extern "C" void DelegatePInvokeWrapper_LowMemoryCallback_t1724019893();
extern const RuntimeType LowMemoryCallback_t1724019893_0_0_0;
extern "C" void AssetBundleRequest_t1950164907_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AssetBundleRequest_t1950164907_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AssetBundleRequest_t1950164907_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AssetBundleRequest_t1950164907_0_0_0;
extern "C" void AsyncOperation_t4233120411_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AsyncOperation_t4233120411_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AsyncOperation_t4233120411_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AsyncOperation_t4233120411_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMReaderCallback_t2906147478();
extern const RuntimeType PCMReaderCallback_t2906147478_0_0_0;
extern "C" void DelegatePInvokeWrapper_PCMSetPositionCallback_t2871811295();
extern const RuntimeType PCMSetPositionCallback_t2871811295_0_0_0;
extern "C" void DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2686124992();
extern const RuntimeType AudioConfigurationChangeHandler_t2686124992_0_0_0;
extern "C" void DelegatePInvokeWrapper_WillRenderCanvases_t3792041812();
extern const RuntimeType WillRenderCanvases_t3792041812_0_0_0;
extern "C" void Collision_t3944560651_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision_t3944560651_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision_t3944560651_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision_t3944560651_0_0_0;
extern "C" void Collision2D_t4030444074_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Collision2D_t4030444074_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Collision2D_t4030444074_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Collision2D_t4030444074_0_0_0;
extern "C" void ContactFilter2D_t1655875665_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ContactFilter2D_t1655875665_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ContactFilter2D_t1655875665_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ContactFilter2D_t1655875665_0_0_0;
extern "C" void ControllerColliderHit_t2636962803_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ControllerColliderHit_t2636962803_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ControllerColliderHit_t2636962803_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ControllerColliderHit_t2636962803_0_0_0;
extern "C" void Coroutine_t2982543926_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Coroutine_t2982543926_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Coroutine_t2982543926_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Coroutine_t2982543926_0_0_0;
extern "C" void DelegatePInvokeWrapper_CSSMeasureFunc_t1258559413();
extern const RuntimeType CSSMeasureFunc_t1258559413_0_0_0;
extern "C" void CullingGroup_t2308132715_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void CullingGroup_t2308132715_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void CullingGroup_t2308132715_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType CullingGroup_t2308132715_0_0_0;
extern "C" void DelegatePInvokeWrapper_StateChanged_t2350477799();
extern const RuntimeType StateChanged_t2350477799_0_0_0;
extern "C" void DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t1844255214();
extern const RuntimeType DisplaysUpdatedDelegate_t1844255214_0_0_0;
extern "C" void Event_t3002009274_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Event_t3002009274_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Event_t3002009274_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Event_t3002009274_0_0_0;
extern "C" void DelegatePInvokeWrapper_UnityAction_t4110260629();
extern const RuntimeType UnityAction_t4110260629_0_0_0;
extern "C" void FailedToLoadScriptObject_t2199506542_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FailedToLoadScriptObject_t2199506542_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FailedToLoadScriptObject_t2199506542_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FailedToLoadScriptObject_t2199506542_0_0_0;
extern "C" void DelegatePInvokeWrapper_FontTextureRebuildCallback_t3954027152();
extern const RuntimeType FontTextureRebuildCallback_t3954027152_0_0_0;
extern "C" void Gradient_t2638935012_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Gradient_t2638935012_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Gradient_t2638935012_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Gradient_t2638935012_0_0_0;
extern "C" void DelegatePInvokeWrapper_WindowFunction_t1670771669();
extern const RuntimeType WindowFunction_t1670771669_0_0_0;
extern "C" void GUIContent_t1852996497_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIContent_t1852996497_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIContent_t1852996497_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIContent_t1852996497_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkinChangedDelegate_t4226191565();
extern const RuntimeType SkinChangedDelegate_t4226191565_0_0_0;
extern "C" void GUIStyle_t2201526215_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyle_t2201526215_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyle_t2201526215_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyle_t2201526215_0_0_0;
extern "C" void GUIStyleState_t60144046_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GUIStyleState_t60144046_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GUIStyleState_t60144046_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GUIStyleState_t60144046_0_0_0;
extern "C" void HumanBone_t3493600746_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HumanBone_t3493600746_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HumanBone_t3493600746_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HumanBone_t3493600746_0_0_0;
extern "C" void Object_t350248726_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Object_t350248726_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Object_t350248726_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Object_t350248726_0_0_0;
extern "C" void PlayableBinding_t150998691_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void PlayableBinding_t150998691_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void PlayableBinding_t150998691_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType PlayableBinding_t150998691_0_0_0;
extern "C" void RaycastHit_t2126573776_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit_t2126573776_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit_t2126573776_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit_t2126573776_0_0_0;
extern "C" void RaycastHit2D_t3956163020_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastHit2D_t3956163020_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastHit2D_t3956163020_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastHit2D_t3956163020_0_0_0;
extern "C" void RectOffset_t2305860064_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RectOffset_t2305860064_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RectOffset_t2305860064_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RectOffset_t2305860064_0_0_0;
extern "C" void DelegatePInvokeWrapper_UpdatedEventHandler_t3509672523();
extern const RuntimeType UpdatedEventHandler_t3509672523_0_0_0;
extern "C" void ResourceRequest_t2260579732_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ResourceRequest_t2260579732_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ResourceRequest_t2260579732_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ResourceRequest_t2260579732_0_0_0;
extern "C" void ScriptableObject_t229319923_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ScriptableObject_t229319923_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ScriptableObject_t229319923_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ScriptableObject_t229319923_0_0_0;
extern "C" void HitInfo_t2706273657_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void HitInfo_t2706273657_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void HitInfo_t2706273657_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType HitInfo_t2706273657_0_0_0;
extern "C" void SkeletonBone_t3257985101_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SkeletonBone_t3257985101_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SkeletonBone_t3257985101_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SkeletonBone_t3257985101_0_0_0;
extern "C" void GcAchievementData_t3971162273_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementData_t3971162273_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementData_t3971162273_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementData_t3971162273_0_0_0;
extern "C" void GcAchievementDescriptionData_t964696278_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcAchievementDescriptionData_t964696278_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcAchievementDescriptionData_t964696278_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcAchievementDescriptionData_t964696278_0_0_0;
extern "C" void GcLeaderboard_t582328223_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcLeaderboard_t582328223_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcLeaderboard_t582328223_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcLeaderboard_t582328223_0_0_0;
extern "C" void GcScoreData_t3049994916_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcScoreData_t3049994916_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcScoreData_t3049994916_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcScoreData_t3049994916_0_0_0;
extern "C" void GcUserProfileData_t5225543_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void GcUserProfileData_t5225543_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void GcUserProfileData_t5225543_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType GcUserProfileData_t5225543_0_0_0;
extern "C" void TextGenerationSettings_t781948148_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerationSettings_t781948148_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerationSettings_t781948148_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerationSettings_t781948148_0_0_0;
extern "C" void TextGenerator_t1842200301_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TextGenerator_t1842200301_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TextGenerator_t1842200301_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TextGenerator_t1842200301_0_0_0;
extern "C" void TrackedReference_t4182582575_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TrackedReference_t4182582575_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TrackedReference_t4182582575_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TrackedReference_t4182582575_0_0_0;
extern "C" void DelegatePInvokeWrapper_RequestAtlasCallback_t2148362272();
extern const RuntimeType RequestAtlasCallback_t2148362272_0_0_0;
extern "C" void WorkRequest_t2264216812_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WorkRequest_t2264216812_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WorkRequest_t2264216812_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WorkRequest_t2264216812_0_0_0;
extern "C" void WaitForSeconds_t4091331090_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void WaitForSeconds_t4091331090_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void WaitForSeconds_t4091331090_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType WaitForSeconds_t4091331090_0_0_0;
extern "C" void YieldInstruction_t1400753137_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void YieldInstruction_t1400753137_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void YieldInstruction_t1400753137_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType YieldInstruction_t1400753137_0_0_0;
extern "C" void RaycastResult_t2363482100_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void RaycastResult_t2363482100_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void RaycastResult_t2363482100_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType RaycastResult_t2363482100_0_0_0;
extern "C" void ColorTween_t3858592911_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void ColorTween_t3858592911_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void ColorTween_t3858592911_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType ColorTween_t3858592911_0_0_0;
extern "C" void FloatTween_t1018971280_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void FloatTween_t1018971280_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void FloatTween_t1018971280_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType FloatTween_t1018971280_0_0_0;
extern "C" void Resources_t3317836149_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Resources_t3317836149_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Resources_t3317836149_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Resources_t3317836149_0_0_0;
extern "C" void DelegatePInvokeWrapper_OnValidateInput_t1653235578();
extern const RuntimeType OnValidateInput_t1653235578_0_0_0;
extern "C" void Navigation_t2074156770_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Navigation_t2074156770_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Navigation_t2074156770_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Navigation_t2074156770_0_0_0;
extern "C" void SpriteState_t4104548053_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SpriteState_t4104548053_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SpriteState_t4104548053_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SpriteState_t4104548053_0_0_0;
extern "C" void AnimationPair_t2435898854_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AnimationPair_t2435898854_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AnimationPair_t2435898854_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AnimationPair_t2435898854_0_0_0;
extern "C" void EventQueueEntry_t3756679084_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void EventQueueEntry_t3756679084_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void EventQueueEntry_t3756679084_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType EventQueueEntry_t3756679084_0_0_0;
extern "C" void AttachmentKeyTuple_t2283295499_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AttachmentKeyTuple_t2283295499_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AttachmentKeyTuple_t2283295499_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AttachmentKeyTuple_t2283295499_0_0_0;
extern "C" void Settings_t1339330787_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Settings_t1339330787_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Settings_t1339330787_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Settings_t1339330787_0_0_0;
extern "C" void MeshGeneratorBuffers_t1203282268_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MeshGeneratorBuffers_t1203282268_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MeshGeneratorBuffers_t1203282268_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MeshGeneratorBuffers_t1203282268_0_0_0;
extern "C" void AtlasMaterialOverride_t4245281532_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void AtlasMaterialOverride_t4245281532_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void AtlasMaterialOverride_t4245281532_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType AtlasMaterialOverride_t4245281532_0_0_0;
extern "C" void SlotMaterialOverride_t3080213539_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SlotMaterialOverride_t3080213539_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SlotMaterialOverride_t3080213539_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SlotMaterialOverride_t3080213539_0_0_0;
extern "C" void TransformPair_t1730302243_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void TransformPair_t1730302243_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void TransformPair_t1730302243_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType TransformPair_t1730302243_0_0_0;
extern "C" void MaterialTexturePair_t2729914733_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void MaterialTexturePair_t2729914733_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void MaterialTexturePair_t2729914733_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType MaterialTexturePair_t2729914733_0_0_0;
extern "C" void DelegatePInvokeWrapper_SkeletonUtilityDelegate_t1611778497();
extern const RuntimeType SkeletonUtilityDelegate_t1611778497_0_0_0;
extern "C" void Hierarchy_t2251599244_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void Hierarchy_t2251599244_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void Hierarchy_t2251599244_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType Hierarchy_t2251599244_0_0_0;
extern "C" void SubmeshInstruction_t3765917328_marshal_pinvoke(void* managedStructure, void* marshaledStructure);
extern "C" void SubmeshInstruction_t3765917328_marshal_pinvoke_back(void* marshaledStructure, void* managedStructure);
extern "C" void SubmeshInstruction_t3765917328_marshal_pinvoke_cleanup(void* marshaledStructure);
extern const RuntimeType SubmeshInstruction_t3765917328_0_0_0;
extern Il2CppInteropData g_Il2CppInteropData[110] = 
{
	{ NULL, Context_t3877589556_marshal_pinvoke, Context_t3877589556_marshal_pinvoke_back, Context_t3877589556_marshal_pinvoke_cleanup, NULL, NULL, &Context_t3877589556_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Context */,
	{ NULL, Escape_t1880027543_marshal_pinvoke, Escape_t1880027543_marshal_pinvoke_back, Escape_t1880027543_marshal_pinvoke_cleanup, NULL, NULL, &Escape_t1880027543_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/Escape */,
	{ NULL, PreviousInfo_t3752465461_marshal_pinvoke, PreviousInfo_t3752465461_marshal_pinvoke_back, PreviousInfo_t3752465461_marshal_pinvoke_cleanup, NULL, NULL, &PreviousInfo_t3752465461_0_0_0 } /* Mono.Globalization.Unicode.SimpleCollator/PreviousInfo */,
	{ DelegatePInvokeWrapper_AppDomainInitializer_t4005868378, NULL, NULL, NULL, NULL, NULL, &AppDomainInitializer_t4005868378_0_0_0 } /* System.AppDomainInitializer */,
	{ DelegatePInvokeWrapper_Swapper_t1905224008, NULL, NULL, NULL, NULL, NULL, &Swapper_t1905224008_0_0_0 } /* System.Array/Swapper */,
	{ NULL, DictionaryEntry_t2732573845_marshal_pinvoke, DictionaryEntry_t2732573845_marshal_pinvoke_back, DictionaryEntry_t2732573845_marshal_pinvoke_cleanup, NULL, NULL, &DictionaryEntry_t2732573845_0_0_0 } /* System.Collections.DictionaryEntry */,
	{ NULL, Slot_t2420837462_marshal_pinvoke, Slot_t2420837462_marshal_pinvoke_back, Slot_t2420837462_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t2420837462_0_0_0 } /* System.Collections.Hashtable/Slot */,
	{ NULL, Slot_t1498351365_marshal_pinvoke, Slot_t1498351365_marshal_pinvoke_back, Slot_t1498351365_marshal_pinvoke_cleanup, NULL, NULL, &Slot_t1498351365_0_0_0 } /* System.Collections.SortedList/Slot */,
	{ NULL, Enum_t1912704450_marshal_pinvoke, Enum_t1912704450_marshal_pinvoke_back, Enum_t1912704450_marshal_pinvoke_cleanup, NULL, NULL, &Enum_t1912704450_0_0_0 } /* System.Enum */,
	{ DelegatePInvokeWrapper_ReadDelegate_t498133763, NULL, NULL, NULL, NULL, NULL, &ReadDelegate_t498133763_0_0_0 } /* System.IO.FileStream/ReadDelegate */,
	{ DelegatePInvokeWrapper_WriteDelegate_t1654929309, NULL, NULL, NULL, NULL, NULL, &WriteDelegate_t1654929309_0_0_0 } /* System.IO.FileStream/WriteDelegate */,
	{ NULL, MonoIOStat_t296773048_marshal_pinvoke, MonoIOStat_t296773048_marshal_pinvoke_back, MonoIOStat_t296773048_marshal_pinvoke_cleanup, NULL, NULL, &MonoIOStat_t296773048_0_0_0 } /* System.IO.MonoIOStat */,
	{ NULL, MonoEnumInfo_t495278953_marshal_pinvoke, MonoEnumInfo_t495278953_marshal_pinvoke_back, MonoEnumInfo_t495278953_marshal_pinvoke_cleanup, NULL, NULL, &MonoEnumInfo_t495278953_0_0_0 } /* System.MonoEnumInfo */,
	{ NULL, CustomAttributeNamedArgument_t2964753189_marshal_pinvoke, CustomAttributeNamedArgument_t2964753189_marshal_pinvoke_back, CustomAttributeNamedArgument_t2964753189_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeNamedArgument_t2964753189_0_0_0 } /* System.Reflection.CustomAttributeNamedArgument */,
	{ NULL, CustomAttributeTypedArgument_t1279663203_marshal_pinvoke, CustomAttributeTypedArgument_t1279663203_marshal_pinvoke_back, CustomAttributeTypedArgument_t1279663203_marshal_pinvoke_cleanup, NULL, NULL, &CustomAttributeTypedArgument_t1279663203_0_0_0 } /* System.Reflection.CustomAttributeTypedArgument */,
	{ NULL, ILTokenInfo_t511186550_marshal_pinvoke, ILTokenInfo_t511186550_marshal_pinvoke_back, ILTokenInfo_t511186550_marshal_pinvoke_cleanup, NULL, NULL, &ILTokenInfo_t511186550_0_0_0 } /* System.Reflection.Emit.ILTokenInfo */,
	{ NULL, MonoEventInfo_t4102204480_marshal_pinvoke, MonoEventInfo_t4102204480_marshal_pinvoke_back, MonoEventInfo_t4102204480_marshal_pinvoke_cleanup, NULL, NULL, &MonoEventInfo_t4102204480_0_0_0 } /* System.Reflection.MonoEventInfo */,
	{ NULL, MonoMethodInfo_t520497436_marshal_pinvoke, MonoMethodInfo_t520497436_marshal_pinvoke_back, MonoMethodInfo_t520497436_marshal_pinvoke_cleanup, NULL, NULL, &MonoMethodInfo_t520497436_0_0_0 } /* System.Reflection.MonoMethodInfo */,
	{ NULL, MonoPropertyInfo_t3894631283_marshal_pinvoke, MonoPropertyInfo_t3894631283_marshal_pinvoke_back, MonoPropertyInfo_t3894631283_marshal_pinvoke_cleanup, NULL, NULL, &MonoPropertyInfo_t3894631283_0_0_0 } /* System.Reflection.MonoPropertyInfo */,
	{ NULL, ParameterModifier_t1293521645_marshal_pinvoke, ParameterModifier_t1293521645_marshal_pinvoke_back, ParameterModifier_t1293521645_marshal_pinvoke_cleanup, NULL, NULL, &ParameterModifier_t1293521645_0_0_0 } /* System.Reflection.ParameterModifier */,
	{ NULL, ResourceCacheItem_t484540818_marshal_pinvoke, ResourceCacheItem_t484540818_marshal_pinvoke_back, ResourceCacheItem_t484540818_marshal_pinvoke_cleanup, NULL, NULL, &ResourceCacheItem_t484540818_0_0_0 } /* System.Resources.ResourceReader/ResourceCacheItem */,
	{ NULL, ResourceInfo_t2441261143_marshal_pinvoke, ResourceInfo_t2441261143_marshal_pinvoke_back, ResourceInfo_t2441261143_marshal_pinvoke_cleanup, NULL, NULL, &ResourceInfo_t2441261143_0_0_0 } /* System.Resources.ResourceReader/ResourceInfo */,
	{ DelegatePInvokeWrapper_CrossContextDelegate_t2164444586, NULL, NULL, NULL, NULL, NULL, &CrossContextDelegate_t2164444586_0_0_0 } /* System.Runtime.Remoting.Contexts.CrossContextDelegate */,
	{ DelegatePInvokeWrapper_CallbackHandler_t661518983, NULL, NULL, NULL, NULL, NULL, &CallbackHandler_t661518983_0_0_0 } /* System.Runtime.Serialization.SerializationCallbacks/CallbackHandler */,
	{ NULL, SerializationEntry_t2008081023_marshal_pinvoke, SerializationEntry_t2008081023_marshal_pinvoke_back, SerializationEntry_t2008081023_marshal_pinvoke_cleanup, NULL, NULL, &SerializationEntry_t2008081023_0_0_0 } /* System.Runtime.Serialization.SerializationEntry */,
	{ NULL, StreamingContext_t4227869630_marshal_pinvoke, StreamingContext_t4227869630_marshal_pinvoke_back, StreamingContext_t4227869630_marshal_pinvoke_cleanup, NULL, NULL, &StreamingContext_t4227869630_0_0_0 } /* System.Runtime.Serialization.StreamingContext */,
	{ NULL, DSAParameters_t1524829693_marshal_pinvoke, DSAParameters_t1524829693_marshal_pinvoke_back, DSAParameters_t1524829693_marshal_pinvoke_cleanup, NULL, NULL, &DSAParameters_t1524829693_0_0_0 } /* System.Security.Cryptography.DSAParameters */,
	{ NULL, RSAParameters_t2694455455_marshal_pinvoke, RSAParameters_t2694455455_marshal_pinvoke_back, RSAParameters_t2694455455_marshal_pinvoke_cleanup, NULL, NULL, &RSAParameters_t2694455455_0_0_0 } /* System.Security.Cryptography.RSAParameters */,
	{ NULL, SecurityFrame_t748984313_marshal_pinvoke, SecurityFrame_t748984313_marshal_pinvoke_back, SecurityFrame_t748984313_marshal_pinvoke_cleanup, NULL, NULL, &SecurityFrame_t748984313_0_0_0 } /* System.Security.SecurityFrame */,
	{ DelegatePInvokeWrapper_ThreadStart_t815992611, NULL, NULL, NULL, NULL, NULL, &ThreadStart_t815992611_0_0_0 } /* System.Threading.ThreadStart */,
	{ NULL, ValueType_t2697047229_marshal_pinvoke, ValueType_t2697047229_marshal_pinvoke_back, ValueType_t2697047229_marshal_pinvoke_cleanup, NULL, NULL, &ValueType_t2697047229_0_0_0 } /* System.ValueType */,
	{ NULL, X509ChainStatus_t1809129440_marshal_pinvoke, X509ChainStatus_t1809129440_marshal_pinvoke_back, X509ChainStatus_t1809129440_marshal_pinvoke_cleanup, NULL, NULL, &X509ChainStatus_t1809129440_0_0_0 } /* System.Security.Cryptography.X509Certificates.X509ChainStatus */,
	{ NULL, IntStack_t546104939_marshal_pinvoke, IntStack_t546104939_marshal_pinvoke_back, IntStack_t546104939_marshal_pinvoke_cleanup, NULL, NULL, &IntStack_t546104939_0_0_0 } /* System.Text.RegularExpressions.Interpreter/IntStack */,
	{ NULL, Interval_t3994977115_marshal_pinvoke, Interval_t3994977115_marshal_pinvoke_back, Interval_t3994977115_marshal_pinvoke_cleanup, NULL, NULL, &Interval_t3994977115_0_0_0 } /* System.Text.RegularExpressions.Interval */,
	{ DelegatePInvokeWrapper_CostDelegate_t3359819672, NULL, NULL, NULL, NULL, NULL, &CostDelegate_t3359819672_0_0_0 } /* System.Text.RegularExpressions.IntervalCollection/CostDelegate */,
	{ NULL, UriScheme_t2335814732_marshal_pinvoke, UriScheme_t2335814732_marshal_pinvoke_back, UriScheme_t2335814732_marshal_pinvoke_cleanup, NULL, NULL, &UriScheme_t2335814732_0_0_0 } /* System.Uri/UriScheme */,
	{ DelegatePInvokeWrapper_Action_t3158581658, NULL, NULL, NULL, NULL, NULL, &Action_t3158581658_0_0_0 } /* System.Action */,
	{ NULL, AnimationCurve_t1417462441_marshal_pinvoke, AnimationCurve_t1417462441_marshal_pinvoke_back, AnimationCurve_t1417462441_marshal_pinvoke_cleanup, NULL, NULL, &AnimationCurve_t1417462441_0_0_0 } /* UnityEngine.AnimationCurve */,
	{ NULL, AnimationEvent_t3272212093_marshal_pinvoke, AnimationEvent_t3272212093_marshal_pinvoke_back, AnimationEvent_t3272212093_marshal_pinvoke_cleanup, NULL, NULL, &AnimationEvent_t3272212093_0_0_0 } /* UnityEngine.AnimationEvent */,
	{ NULL, AnimatorTransitionInfo_t3177004460_marshal_pinvoke, AnimatorTransitionInfo_t3177004460_marshal_pinvoke_back, AnimatorTransitionInfo_t3177004460_marshal_pinvoke_cleanup, NULL, NULL, &AnimatorTransitionInfo_t3177004460_0_0_0 } /* UnityEngine.AnimatorTransitionInfo */,
	{ DelegatePInvokeWrapper_LogCallback_t3941575221, NULL, NULL, NULL, NULL, NULL, &LogCallback_t3941575221_0_0_0 } /* UnityEngine.Application/LogCallback */,
	{ DelegatePInvokeWrapper_LowMemoryCallback_t1724019893, NULL, NULL, NULL, NULL, NULL, &LowMemoryCallback_t1724019893_0_0_0 } /* UnityEngine.Application/LowMemoryCallback */,
	{ NULL, AssetBundleRequest_t1950164907_marshal_pinvoke, AssetBundleRequest_t1950164907_marshal_pinvoke_back, AssetBundleRequest_t1950164907_marshal_pinvoke_cleanup, NULL, NULL, &AssetBundleRequest_t1950164907_0_0_0 } /* UnityEngine.AssetBundleRequest */,
	{ NULL, AsyncOperation_t4233120411_marshal_pinvoke, AsyncOperation_t4233120411_marshal_pinvoke_back, AsyncOperation_t4233120411_marshal_pinvoke_cleanup, NULL, NULL, &AsyncOperation_t4233120411_0_0_0 } /* UnityEngine.AsyncOperation */,
	{ DelegatePInvokeWrapper_PCMReaderCallback_t2906147478, NULL, NULL, NULL, NULL, NULL, &PCMReaderCallback_t2906147478_0_0_0 } /* UnityEngine.AudioClip/PCMReaderCallback */,
	{ DelegatePInvokeWrapper_PCMSetPositionCallback_t2871811295, NULL, NULL, NULL, NULL, NULL, &PCMSetPositionCallback_t2871811295_0_0_0 } /* UnityEngine.AudioClip/PCMSetPositionCallback */,
	{ DelegatePInvokeWrapper_AudioConfigurationChangeHandler_t2686124992, NULL, NULL, NULL, NULL, NULL, &AudioConfigurationChangeHandler_t2686124992_0_0_0 } /* UnityEngine.AudioSettings/AudioConfigurationChangeHandler */,
	{ DelegatePInvokeWrapper_WillRenderCanvases_t3792041812, NULL, NULL, NULL, NULL, NULL, &WillRenderCanvases_t3792041812_0_0_0 } /* UnityEngine.Canvas/WillRenderCanvases */,
	{ NULL, Collision_t3944560651_marshal_pinvoke, Collision_t3944560651_marshal_pinvoke_back, Collision_t3944560651_marshal_pinvoke_cleanup, NULL, NULL, &Collision_t3944560651_0_0_0 } /* UnityEngine.Collision */,
	{ NULL, Collision2D_t4030444074_marshal_pinvoke, Collision2D_t4030444074_marshal_pinvoke_back, Collision2D_t4030444074_marshal_pinvoke_cleanup, NULL, NULL, &Collision2D_t4030444074_0_0_0 } /* UnityEngine.Collision2D */,
	{ NULL, ContactFilter2D_t1655875665_marshal_pinvoke, ContactFilter2D_t1655875665_marshal_pinvoke_back, ContactFilter2D_t1655875665_marshal_pinvoke_cleanup, NULL, NULL, &ContactFilter2D_t1655875665_0_0_0 } /* UnityEngine.ContactFilter2D */,
	{ NULL, ControllerColliderHit_t2636962803_marshal_pinvoke, ControllerColliderHit_t2636962803_marshal_pinvoke_back, ControllerColliderHit_t2636962803_marshal_pinvoke_cleanup, NULL, NULL, &ControllerColliderHit_t2636962803_0_0_0 } /* UnityEngine.ControllerColliderHit */,
	{ NULL, Coroutine_t2982543926_marshal_pinvoke, Coroutine_t2982543926_marshal_pinvoke_back, Coroutine_t2982543926_marshal_pinvoke_cleanup, NULL, NULL, &Coroutine_t2982543926_0_0_0 } /* UnityEngine.Coroutine */,
	{ DelegatePInvokeWrapper_CSSMeasureFunc_t1258559413, NULL, NULL, NULL, NULL, NULL, &CSSMeasureFunc_t1258559413_0_0_0 } /* UnityEngine.CSSLayout.CSSMeasureFunc */,
	{ NULL, CullingGroup_t2308132715_marshal_pinvoke, CullingGroup_t2308132715_marshal_pinvoke_back, CullingGroup_t2308132715_marshal_pinvoke_cleanup, NULL, NULL, &CullingGroup_t2308132715_0_0_0 } /* UnityEngine.CullingGroup */,
	{ DelegatePInvokeWrapper_StateChanged_t2350477799, NULL, NULL, NULL, NULL, NULL, &StateChanged_t2350477799_0_0_0 } /* UnityEngine.CullingGroup/StateChanged */,
	{ DelegatePInvokeWrapper_DisplaysUpdatedDelegate_t1844255214, NULL, NULL, NULL, NULL, NULL, &DisplaysUpdatedDelegate_t1844255214_0_0_0 } /* UnityEngine.Display/DisplaysUpdatedDelegate */,
	{ NULL, Event_t3002009274_marshal_pinvoke, Event_t3002009274_marshal_pinvoke_back, Event_t3002009274_marshal_pinvoke_cleanup, NULL, NULL, &Event_t3002009274_0_0_0 } /* UnityEngine.Event */,
	{ DelegatePInvokeWrapper_UnityAction_t4110260629, NULL, NULL, NULL, NULL, NULL, &UnityAction_t4110260629_0_0_0 } /* UnityEngine.Events.UnityAction */,
	{ NULL, FailedToLoadScriptObject_t2199506542_marshal_pinvoke, FailedToLoadScriptObject_t2199506542_marshal_pinvoke_back, FailedToLoadScriptObject_t2199506542_marshal_pinvoke_cleanup, NULL, NULL, &FailedToLoadScriptObject_t2199506542_0_0_0 } /* UnityEngine.FailedToLoadScriptObject */,
	{ DelegatePInvokeWrapper_FontTextureRebuildCallback_t3954027152, NULL, NULL, NULL, NULL, NULL, &FontTextureRebuildCallback_t3954027152_0_0_0 } /* UnityEngine.Font/FontTextureRebuildCallback */,
	{ NULL, Gradient_t2638935012_marshal_pinvoke, Gradient_t2638935012_marshal_pinvoke_back, Gradient_t2638935012_marshal_pinvoke_cleanup, NULL, NULL, &Gradient_t2638935012_0_0_0 } /* UnityEngine.Gradient */,
	{ DelegatePInvokeWrapper_WindowFunction_t1670771669, NULL, NULL, NULL, NULL, NULL, &WindowFunction_t1670771669_0_0_0 } /* UnityEngine.GUI/WindowFunction */,
	{ NULL, GUIContent_t1852996497_marshal_pinvoke, GUIContent_t1852996497_marshal_pinvoke_back, GUIContent_t1852996497_marshal_pinvoke_cleanup, NULL, NULL, &GUIContent_t1852996497_0_0_0 } /* UnityEngine.GUIContent */,
	{ DelegatePInvokeWrapper_SkinChangedDelegate_t4226191565, NULL, NULL, NULL, NULL, NULL, &SkinChangedDelegate_t4226191565_0_0_0 } /* UnityEngine.GUISkin/SkinChangedDelegate */,
	{ NULL, GUIStyle_t2201526215_marshal_pinvoke, GUIStyle_t2201526215_marshal_pinvoke_back, GUIStyle_t2201526215_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyle_t2201526215_0_0_0 } /* UnityEngine.GUIStyle */,
	{ NULL, GUIStyleState_t60144046_marshal_pinvoke, GUIStyleState_t60144046_marshal_pinvoke_back, GUIStyleState_t60144046_marshal_pinvoke_cleanup, NULL, NULL, &GUIStyleState_t60144046_0_0_0 } /* UnityEngine.GUIStyleState */,
	{ NULL, HumanBone_t3493600746_marshal_pinvoke, HumanBone_t3493600746_marshal_pinvoke_back, HumanBone_t3493600746_marshal_pinvoke_cleanup, NULL, NULL, &HumanBone_t3493600746_0_0_0 } /* UnityEngine.HumanBone */,
	{ NULL, Object_t350248726_marshal_pinvoke, Object_t350248726_marshal_pinvoke_back, Object_t350248726_marshal_pinvoke_cleanup, NULL, NULL, &Object_t350248726_0_0_0 } /* UnityEngine.Object */,
	{ NULL, PlayableBinding_t150998691_marshal_pinvoke, PlayableBinding_t150998691_marshal_pinvoke_back, PlayableBinding_t150998691_marshal_pinvoke_cleanup, NULL, NULL, &PlayableBinding_t150998691_0_0_0 } /* UnityEngine.Playables.PlayableBinding */,
	{ NULL, RaycastHit_t2126573776_marshal_pinvoke, RaycastHit_t2126573776_marshal_pinvoke_back, RaycastHit_t2126573776_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit_t2126573776_0_0_0 } /* UnityEngine.RaycastHit */,
	{ NULL, RaycastHit2D_t3956163020_marshal_pinvoke, RaycastHit2D_t3956163020_marshal_pinvoke_back, RaycastHit2D_t3956163020_marshal_pinvoke_cleanup, NULL, NULL, &RaycastHit2D_t3956163020_0_0_0 } /* UnityEngine.RaycastHit2D */,
	{ NULL, RectOffset_t2305860064_marshal_pinvoke, RectOffset_t2305860064_marshal_pinvoke_back, RectOffset_t2305860064_marshal_pinvoke_cleanup, NULL, NULL, &RectOffset_t2305860064_0_0_0 } /* UnityEngine.RectOffset */,
	{ DelegatePInvokeWrapper_UpdatedEventHandler_t3509672523, NULL, NULL, NULL, NULL, NULL, &UpdatedEventHandler_t3509672523_0_0_0 } /* UnityEngine.RemoteSettings/UpdatedEventHandler */,
	{ NULL, ResourceRequest_t2260579732_marshal_pinvoke, ResourceRequest_t2260579732_marshal_pinvoke_back, ResourceRequest_t2260579732_marshal_pinvoke_cleanup, NULL, NULL, &ResourceRequest_t2260579732_0_0_0 } /* UnityEngine.ResourceRequest */,
	{ NULL, ScriptableObject_t229319923_marshal_pinvoke, ScriptableObject_t229319923_marshal_pinvoke_back, ScriptableObject_t229319923_marshal_pinvoke_cleanup, NULL, NULL, &ScriptableObject_t229319923_0_0_0 } /* UnityEngine.ScriptableObject */,
	{ NULL, HitInfo_t2706273657_marshal_pinvoke, HitInfo_t2706273657_marshal_pinvoke_back, HitInfo_t2706273657_marshal_pinvoke_cleanup, NULL, NULL, &HitInfo_t2706273657_0_0_0 } /* UnityEngine.SendMouseEvents/HitInfo */,
	{ NULL, SkeletonBone_t3257985101_marshal_pinvoke, SkeletonBone_t3257985101_marshal_pinvoke_back, SkeletonBone_t3257985101_marshal_pinvoke_cleanup, NULL, NULL, &SkeletonBone_t3257985101_0_0_0 } /* UnityEngine.SkeletonBone */,
	{ NULL, GcAchievementData_t3971162273_marshal_pinvoke, GcAchievementData_t3971162273_marshal_pinvoke_back, GcAchievementData_t3971162273_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementData_t3971162273_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementData */,
	{ NULL, GcAchievementDescriptionData_t964696278_marshal_pinvoke, GcAchievementDescriptionData_t964696278_marshal_pinvoke_back, GcAchievementDescriptionData_t964696278_marshal_pinvoke_cleanup, NULL, NULL, &GcAchievementDescriptionData_t964696278_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcAchievementDescriptionData */,
	{ NULL, GcLeaderboard_t582328223_marshal_pinvoke, GcLeaderboard_t582328223_marshal_pinvoke_back, GcLeaderboard_t582328223_marshal_pinvoke_cleanup, NULL, NULL, &GcLeaderboard_t582328223_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard */,
	{ NULL, GcScoreData_t3049994916_marshal_pinvoke, GcScoreData_t3049994916_marshal_pinvoke_back, GcScoreData_t3049994916_marshal_pinvoke_cleanup, NULL, NULL, &GcScoreData_t3049994916_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcScoreData */,
	{ NULL, GcUserProfileData_t5225543_marshal_pinvoke, GcUserProfileData_t5225543_marshal_pinvoke_back, GcUserProfileData_t5225543_marshal_pinvoke_cleanup, NULL, NULL, &GcUserProfileData_t5225543_0_0_0 } /* UnityEngine.SocialPlatforms.GameCenter.GcUserProfileData */,
	{ NULL, TextGenerationSettings_t781948148_marshal_pinvoke, TextGenerationSettings_t781948148_marshal_pinvoke_back, TextGenerationSettings_t781948148_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerationSettings_t781948148_0_0_0 } /* UnityEngine.TextGenerationSettings */,
	{ NULL, TextGenerator_t1842200301_marshal_pinvoke, TextGenerator_t1842200301_marshal_pinvoke_back, TextGenerator_t1842200301_marshal_pinvoke_cleanup, NULL, NULL, &TextGenerator_t1842200301_0_0_0 } /* UnityEngine.TextGenerator */,
	{ NULL, TrackedReference_t4182582575_marshal_pinvoke, TrackedReference_t4182582575_marshal_pinvoke_back, TrackedReference_t4182582575_marshal_pinvoke_cleanup, NULL, NULL, &TrackedReference_t4182582575_0_0_0 } /* UnityEngine.TrackedReference */,
	{ DelegatePInvokeWrapper_RequestAtlasCallback_t2148362272, NULL, NULL, NULL, NULL, NULL, &RequestAtlasCallback_t2148362272_0_0_0 } /* UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback */,
	{ NULL, WorkRequest_t2264216812_marshal_pinvoke, WorkRequest_t2264216812_marshal_pinvoke_back, WorkRequest_t2264216812_marshal_pinvoke_cleanup, NULL, NULL, &WorkRequest_t2264216812_0_0_0 } /* UnityEngine.UnitySynchronizationContext/WorkRequest */,
	{ NULL, WaitForSeconds_t4091331090_marshal_pinvoke, WaitForSeconds_t4091331090_marshal_pinvoke_back, WaitForSeconds_t4091331090_marshal_pinvoke_cleanup, NULL, NULL, &WaitForSeconds_t4091331090_0_0_0 } /* UnityEngine.WaitForSeconds */,
	{ NULL, YieldInstruction_t1400753137_marshal_pinvoke, YieldInstruction_t1400753137_marshal_pinvoke_back, YieldInstruction_t1400753137_marshal_pinvoke_cleanup, NULL, NULL, &YieldInstruction_t1400753137_0_0_0 } /* UnityEngine.YieldInstruction */,
	{ NULL, RaycastResult_t2363482100_marshal_pinvoke, RaycastResult_t2363482100_marshal_pinvoke_back, RaycastResult_t2363482100_marshal_pinvoke_cleanup, NULL, NULL, &RaycastResult_t2363482100_0_0_0 } /* UnityEngine.EventSystems.RaycastResult */,
	{ NULL, ColorTween_t3858592911_marshal_pinvoke, ColorTween_t3858592911_marshal_pinvoke_back, ColorTween_t3858592911_marshal_pinvoke_cleanup, NULL, NULL, &ColorTween_t3858592911_0_0_0 } /* UnityEngine.UI.CoroutineTween.ColorTween */,
	{ NULL, FloatTween_t1018971280_marshal_pinvoke, FloatTween_t1018971280_marshal_pinvoke_back, FloatTween_t1018971280_marshal_pinvoke_cleanup, NULL, NULL, &FloatTween_t1018971280_0_0_0 } /* UnityEngine.UI.CoroutineTween.FloatTween */,
	{ NULL, Resources_t3317836149_marshal_pinvoke, Resources_t3317836149_marshal_pinvoke_back, Resources_t3317836149_marshal_pinvoke_cleanup, NULL, NULL, &Resources_t3317836149_0_0_0 } /* UnityEngine.UI.DefaultControls/Resources */,
	{ DelegatePInvokeWrapper_OnValidateInput_t1653235578, NULL, NULL, NULL, NULL, NULL, &OnValidateInput_t1653235578_0_0_0 } /* UnityEngine.UI.InputField/OnValidateInput */,
	{ NULL, Navigation_t2074156770_marshal_pinvoke, Navigation_t2074156770_marshal_pinvoke_back, Navigation_t2074156770_marshal_pinvoke_cleanup, NULL, NULL, &Navigation_t2074156770_0_0_0 } /* UnityEngine.UI.Navigation */,
	{ NULL, SpriteState_t4104548053_marshal_pinvoke, SpriteState_t4104548053_marshal_pinvoke_back, SpriteState_t4104548053_marshal_pinvoke_cleanup, NULL, NULL, &SpriteState_t4104548053_0_0_0 } /* UnityEngine.UI.SpriteState */,
	{ NULL, AnimationPair_t2435898854_marshal_pinvoke, AnimationPair_t2435898854_marshal_pinvoke_back, AnimationPair_t2435898854_marshal_pinvoke_cleanup, NULL, NULL, &AnimationPair_t2435898854_0_0_0 } /* Spine.AnimationStateData/AnimationPair */,
	{ NULL, EventQueueEntry_t3756679084_marshal_pinvoke, EventQueueEntry_t3756679084_marshal_pinvoke_back, EventQueueEntry_t3756679084_marshal_pinvoke_cleanup, NULL, NULL, &EventQueueEntry_t3756679084_0_0_0 } /* Spine.EventQueue/EventQueueEntry */,
	{ NULL, AttachmentKeyTuple_t2283295499_marshal_pinvoke, AttachmentKeyTuple_t2283295499_marshal_pinvoke_back, AttachmentKeyTuple_t2283295499_marshal_pinvoke_cleanup, NULL, NULL, &AttachmentKeyTuple_t2283295499_0_0_0 } /* Spine.Skin/AttachmentKeyTuple */,
	{ NULL, Settings_t1339330787_marshal_pinvoke, Settings_t1339330787_marshal_pinvoke_back, Settings_t1339330787_marshal_pinvoke_cleanup, NULL, NULL, &Settings_t1339330787_0_0_0 } /* Spine.Unity.MeshGenerator/Settings */,
	{ NULL, MeshGeneratorBuffers_t1203282268_marshal_pinvoke, MeshGeneratorBuffers_t1203282268_marshal_pinvoke_back, MeshGeneratorBuffers_t1203282268_marshal_pinvoke_cleanup, NULL, NULL, &MeshGeneratorBuffers_t1203282268_0_0_0 } /* Spine.Unity.MeshGeneratorBuffers */,
	{ NULL, AtlasMaterialOverride_t4245281532_marshal_pinvoke, AtlasMaterialOverride_t4245281532_marshal_pinvoke_back, AtlasMaterialOverride_t4245281532_marshal_pinvoke_cleanup, NULL, NULL, &AtlasMaterialOverride_t4245281532_0_0_0 } /* Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride */,
	{ NULL, SlotMaterialOverride_t3080213539_marshal_pinvoke, SlotMaterialOverride_t3080213539_marshal_pinvoke_back, SlotMaterialOverride_t3080213539_marshal_pinvoke_cleanup, NULL, NULL, &SlotMaterialOverride_t3080213539_0_0_0 } /* Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride */,
	{ NULL, TransformPair_t1730302243_marshal_pinvoke, TransformPair_t1730302243_marshal_pinvoke_back, TransformPair_t1730302243_marshal_pinvoke_cleanup, NULL, NULL, &TransformPair_t1730302243_0_0_0 } /* Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair */,
	{ NULL, MaterialTexturePair_t2729914733_marshal_pinvoke, MaterialTexturePair_t2729914733_marshal_pinvoke_back, MaterialTexturePair_t2729914733_marshal_pinvoke_cleanup, NULL, NULL, &MaterialTexturePair_t2729914733_0_0_0 } /* Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair */,
	{ DelegatePInvokeWrapper_SkeletonUtilityDelegate_t1611778497, NULL, NULL, NULL, NULL, NULL, &SkeletonUtilityDelegate_t1611778497_0_0_0 } /* Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate */,
	{ NULL, Hierarchy_t2251599244_marshal_pinvoke, Hierarchy_t2251599244_marshal_pinvoke_back, Hierarchy_t2251599244_marshal_pinvoke_cleanup, NULL, NULL, &Hierarchy_t2251599244_0_0_0 } /* Spine.Unity.SpineAttachment/Hierarchy */,
	{ NULL, SubmeshInstruction_t3765917328_marshal_pinvoke, SubmeshInstruction_t3765917328_marshal_pinvoke_back, SubmeshInstruction_t3765917328_marshal_pinvoke_cleanup, NULL, NULL, &SubmeshInstruction_t3765917328_0_0_0 } /* Spine.Unity.SubmeshInstruction */,
	NULL,
};
