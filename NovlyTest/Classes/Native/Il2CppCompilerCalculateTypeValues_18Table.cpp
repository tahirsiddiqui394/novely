﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;
// Spine.EventData
struct EventData_t295981108;
// Spine.Unity.Modules.SpineEventUnityHandler/<Start>c__AnonStorey1
struct U3CStartU3Ec__AnonStorey1_t1282162354;
// Spine.IkConstraintData
struct IkConstraintData_t3215615709;
// Spine.ExposedList`1<Spine.Bone>
struct ExposedList_1_t3613858675;
// Spine.Bone
struct Bone_t1763862836;
// Spine.Animation
struct Animation_t2067143511;
// Spine.Unity.SkeletonAnimation
struct SkeletonAnimation_t2012766265;
// Spine.Unity.Examples.SpawnFromSkeletonDataExample
struct SpawnFromSkeletonDataExample_t3296636424;
// SharpJson.Lexer
struct Lexer_t2494922068;
// Spine.AnimationState
struct AnimationState_t2858486651;
// Spine.TrackEntry
struct TrackEntry_t4062297782;
// Spine.Unity.Examples.SpineboyPole
struct SpineboyPole_t3969514969;
// System.Char[]
struct CharU5BU5D_t3669675803;
// System.Collections.Generic.List`1<Spine.BoneData>
struct List_1_t3318075177;
// Spine.BoneData
struct BoneData_t1194659452;
// Spine.ExposedList`1<Spine.Timeline>
struct ExposedList_1_t1117166572;
// Spine.AnimationStateData
struct AnimationStateData_t1644285503;
// Spine.Pool`1<Spine.TrackEntry>
struct Pool_1_t4075013739;
// Spine.ExposedList`1<Spine.TrackEntry>
struct ExposedList_1_t1617326325;
// Spine.ExposedList`1<Spine.Event>
struct ExposedList_1_t1877711358;
// Spine.EventQueue
struct EventQueue_t2605283308;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t2711375974;
// Spine.AnimationState/TrackEntryDelegate
struct TrackEntryDelegate_t1807657294;
// Spine.AnimationState/TrackEntryEventDelegate
struct TrackEntryEventDelegate_t471240152;
// System.Single[]
struct SingleU5BU5D_t3989243465;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_t392103664;
// Spine.Event[]
struct EventU5BU5D_t1022697062;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t369092746;
// Spine.ExposedList`1<System.Int32>
struct ExposedList_1_t1671071859;
// Spine.ExposedList`1<System.Single>
struct ExposedList_1_t3384296343;
// Spine.Unity.Modules.SpineEventUnityHandler/EventPair
struct EventPair_t1359900303;
// Spine.SkeletonData
struct SkeletonData_t2665604974;
// System.Collections.Generic.Dictionary`2<Spine.AnimationStateData/AnimationPair,System.Single>
struct Dictionary_2_t2450703149;
// Spine.Unity.Examples.MixAndMatch
struct MixAndMatch_t1342663373;
// Spine.Skeleton
struct Skeleton_t1045203634;
// Spine.Unity.Examples.MixAndMatchGraphic
struct MixAndMatchGraphic_t1263195094;
// Spine.Unity.Examples.RaggedySpineboy
struct RaggedySpineboy_t30683967;
// System.Collections.Generic.List`1<Spine.EventQueue/EventQueueEntry>
struct List_1_t1585127513;
// System.Action
struct Action_t3158581658;
// System.String[]
struct StringU5BU5D_t656593794;
// Spine.AtlasPage
struct AtlasPage_t3729449121;
// System.Int32[]
struct Int32U5BU5D_t281224829;
// System.Collections.Generic.List`1<Spine.AtlasPage>
struct List_1_t1557897550;
// System.Collections.Generic.List`1<Spine.AtlasRegion>
struct List_1_t304512942;
// Spine.TextureLoader
struct TextureLoader_t3738630476;
// Spine.Atlas[]
struct AtlasU5BU5D_t2027208507;
// System.Void
struct Void_t3157035008;
// System.Single[][]
struct SingleU5BU5DU5BU5D_t2055639668;
// Spine.VertexAttachment
struct VertexAttachment_t1776545046;
// Spine.SlotData
struct SlotData_t587781850;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t878649875;
// UnityEngine.Collider2D
struct Collider2D_t179611260;
// Spine.Event
struct Event_t27715519;
// System.IAsyncResult
struct IAsyncResult_t911908533;
// System.AsyncCallback
struct AsyncCallback_t3972436406;
// UnityEngine.Material
struct Material_t2681358023;
// UnityEngine.Sprite
struct Sprite_t360607379;
// UnityEngine.Texture2D
struct Texture2D_t1431210461;
// Spine.Skin
struct Skin_t1695237131;
// Spine.Unity.Modules.SkeletonRagdoll2D
struct SkeletonRagdoll2D_t3940222091;
// Spine.Unity.ISkeletonAnimation
struct ISkeletonAnimation_t2072256521;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3668794496;
// Spine.Unity.AtlasAsset
struct AtlasAsset_t2525633250;
// System.Collections.Generic.List`1<Spine.Unity.Modules.AtlasRegionAttacher/SlotRegionPair>
struct List_1_t93758473;
// Spine.Atlas
struct Atlas_t604244430;
// Spine.RegionAttachment
struct RegionAttachment_t1431410254;
// Spine.Slot
struct Slot_t1804116343;
// System.Collections.Generic.Dictionary`2<UnityEngine.Texture,Spine.AtlasPage>
struct Dictionary_2_t2822557095;
// Spine.Unity.SkeletonRenderer
struct SkeletonRenderer_t1681389628;
// System.Collections.Generic.List`1<Spine.Unity.Examples.SkeletonColorInitialize/SlotSettings>
struct List_1_t979554255;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1621355309;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t2982817174;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SpineEventUnityHandler/EventPair>
struct List_1_t3483316028;
// Spine.Unity.ISkeletonComponent
struct ISkeletonComponent_t3641217750;
// Spine.Unity.IAnimationStateComponent
struct IAnimationStateComponent_t2249916977;
// Spine.Unity.SkeletonDataAsset
struct SkeletonDataAsset_t1706375087;
// Spine.Unity.Modules.SkeletonRenderSeparator
struct SkeletonRenderSeparator_t3391021243;
// System.Collections.Generic.List`1<Spine.Unity.SkeletonDataAsset>
struct List_1_t3829790812;
// System.Collections.Generic.List`1<Spine.Unity.SkeletonAnimation>
struct List_1_t4136181990;
// System.Collections.Generic.Dictionary`2<System.String,Spine.Animation>
struct Dictionary_2_t3710042347;
// System.Collections.Generic.Dictionary`2<Spine.Animation,Spine.Unity.SkeletonAnimation>
struct Dictionary_2_t2387518393;
// Spine.Unity.BoundingBoxFollower
struct BoundingBoxFollower_t836972383;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EVENTDATA_T295981108_H
#define EVENTDATA_T295981108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventData
struct  EventData_t295981108  : public RuntimeObject
{
public:
	// System.String Spine.EventData::name
	String_t* ___name_0;
	// System.Int32 Spine.EventData::<Int>k__BackingField
	int32_t ___U3CIntU3Ek__BackingField_1;
	// System.Single Spine.EventData::<Float>k__BackingField
	float ___U3CFloatU3Ek__BackingField_2;
	// System.String Spine.EventData::<String>k__BackingField
	String_t* ___U3CStringU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(EventData_t295981108, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_U3CIntU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EventData_t295981108, ___U3CIntU3Ek__BackingField_1)); }
	inline int32_t get_U3CIntU3Ek__BackingField_1() const { return ___U3CIntU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIntU3Ek__BackingField_1() { return &___U3CIntU3Ek__BackingField_1; }
	inline void set_U3CIntU3Ek__BackingField_1(int32_t value)
	{
		___U3CIntU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CFloatU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EventData_t295981108, ___U3CFloatU3Ek__BackingField_2)); }
	inline float get_U3CFloatU3Ek__BackingField_2() const { return ___U3CFloatU3Ek__BackingField_2; }
	inline float* get_address_of_U3CFloatU3Ek__BackingField_2() { return &___U3CFloatU3Ek__BackingField_2; }
	inline void set_U3CFloatU3Ek__BackingField_2(float value)
	{
		___U3CFloatU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CStringU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EventData_t295981108, ___U3CStringU3Ek__BackingField_3)); }
	inline String_t* get_U3CStringU3Ek__BackingField_3() const { return ___U3CStringU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CStringU3Ek__BackingField_3() { return &___U3CStringU3Ek__BackingField_3; }
	inline void set_U3CStringU3Ek__BackingField_3(String_t* value)
	{
		___U3CStringU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStringU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDATA_T295981108_H
#ifndef U3CSTARTU3EC__ANONSTOREY0_T3384789643_H
#define U3CSTARTU3EC__ANONSTOREY0_T3384789643_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SpineEventUnityHandler/<Start>c__AnonStorey0
struct  U3CStartU3Ec__AnonStorey0_t3384789643  : public RuntimeObject
{
public:
	// Spine.EventData Spine.Unity.Modules.SpineEventUnityHandler/<Start>c__AnonStorey0::eventData
	EventData_t295981108 * ___eventData_0;
	// Spine.Unity.Modules.SpineEventUnityHandler/<Start>c__AnonStorey1 Spine.Unity.Modules.SpineEventUnityHandler/<Start>c__AnonStorey0::<>f__ref$1
	U3CStartU3Ec__AnonStorey1_t1282162354 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_eventData_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t3384789643, ___eventData_0)); }
	inline EventData_t295981108 * get_eventData_0() const { return ___eventData_0; }
	inline EventData_t295981108 ** get_address_of_eventData_0() { return &___eventData_0; }
	inline void set_eventData_0(EventData_t295981108 * value)
	{
		___eventData_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventData_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey0_t3384789643, ___U3CU3Ef__refU241_1)); }
	inline U3CStartU3Ec__AnonStorey1_t1282162354 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CStartU3Ec__AnonStorey1_t1282162354 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CStartU3Ec__AnonStorey1_t1282162354 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ANONSTOREY0_T3384789643_H
#ifndef IKCONSTRAINT_T2514691977_H
#define IKCONSTRAINT_T2514691977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.IkConstraint
struct  IkConstraint_t2514691977  : public RuntimeObject
{
public:
	// Spine.IkConstraintData Spine.IkConstraint::data
	IkConstraintData_t3215615709 * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.IkConstraint::bones
	ExposedList_1_t3613858675 * ___bones_1;
	// Spine.Bone Spine.IkConstraint::target
	Bone_t1763862836 * ___target_2;
	// System.Single Spine.IkConstraint::mix
	float ___mix_3;
	// System.Int32 Spine.IkConstraint::bendDirection
	int32_t ___bendDirection_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(IkConstraint_t2514691977, ___data_0)); }
	inline IkConstraintData_t3215615709 * get_data_0() const { return ___data_0; }
	inline IkConstraintData_t3215615709 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(IkConstraintData_t3215615709 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(IkConstraint_t2514691977, ___bones_1)); }
	inline ExposedList_1_t3613858675 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t3613858675 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t3613858675 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(IkConstraint_t2514691977, ___target_2)); }
	inline Bone_t1763862836 * get_target_2() const { return ___target_2; }
	inline Bone_t1763862836 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Bone_t1763862836 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_mix_3() { return static_cast<int32_t>(offsetof(IkConstraint_t2514691977, ___mix_3)); }
	inline float get_mix_3() const { return ___mix_3; }
	inline float* get_address_of_mix_3() { return &___mix_3; }
	inline void set_mix_3(float value)
	{
		___mix_3 = value;
	}

	inline static int32_t get_offset_of_bendDirection_4() { return static_cast<int32_t>(offsetof(IkConstraint_t2514691977, ___bendDirection_4)); }
	inline int32_t get_bendDirection_4() const { return ___bendDirection_4; }
	inline int32_t* get_address_of_bendDirection_4() { return &___bendDirection_4; }
	inline void set_bendDirection_4(int32_t value)
	{
		___bendDirection_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCONSTRAINT_T2514691977_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2460598370_H
#define U3CSTARTU3EC__ITERATOR0_T2460598370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.SpawnFromSkeletonDataExample/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2460598370  : public RuntimeObject
{
public:
	// Spine.Animation Spine.Unity.Examples.SpawnFromSkeletonDataExample/<Start>c__Iterator0::<spineAnimation>__0
	Animation_t2067143511 * ___U3CspineAnimationU3E__0_0;
	// System.Int32 Spine.Unity.Examples.SpawnFromSkeletonDataExample/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_1;
	// Spine.Unity.SkeletonAnimation Spine.Unity.Examples.SpawnFromSkeletonDataExample/<Start>c__Iterator0::<sa>__2
	SkeletonAnimation_t2012766265 * ___U3CsaU3E__2_2;
	// Spine.Unity.Examples.SpawnFromSkeletonDataExample Spine.Unity.Examples.SpawnFromSkeletonDataExample/<Start>c__Iterator0::$this
	SpawnFromSkeletonDataExample_t3296636424 * ___U24this_3;
	// System.Object Spine.Unity.Examples.SpawnFromSkeletonDataExample/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Spine.Unity.Examples.SpawnFromSkeletonDataExample/<Start>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Spine.Unity.Examples.SpawnFromSkeletonDataExample/<Start>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CspineAnimationU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2460598370, ___U3CspineAnimationU3E__0_0)); }
	inline Animation_t2067143511 * get_U3CspineAnimationU3E__0_0() const { return ___U3CspineAnimationU3E__0_0; }
	inline Animation_t2067143511 ** get_address_of_U3CspineAnimationU3E__0_0() { return &___U3CspineAnimationU3E__0_0; }
	inline void set_U3CspineAnimationU3E__0_0(Animation_t2067143511 * value)
	{
		___U3CspineAnimationU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CspineAnimationU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CiU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2460598370, ___U3CiU3E__1_1)); }
	inline int32_t get_U3CiU3E__1_1() const { return ___U3CiU3E__1_1; }
	inline int32_t* get_address_of_U3CiU3E__1_1() { return &___U3CiU3E__1_1; }
	inline void set_U3CiU3E__1_1(int32_t value)
	{
		___U3CiU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CsaU3E__2_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2460598370, ___U3CsaU3E__2_2)); }
	inline SkeletonAnimation_t2012766265 * get_U3CsaU3E__2_2() const { return ___U3CsaU3E__2_2; }
	inline SkeletonAnimation_t2012766265 ** get_address_of_U3CsaU3E__2_2() { return &___U3CsaU3E__2_2; }
	inline void set_U3CsaU3E__2_2(SkeletonAnimation_t2012766265 * value)
	{
		___U3CsaU3E__2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsaU3E__2_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2460598370, ___U24this_3)); }
	inline SpawnFromSkeletonDataExample_t3296636424 * get_U24this_3() const { return ___U24this_3; }
	inline SpawnFromSkeletonDataExample_t3296636424 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SpawnFromSkeletonDataExample_t3296636424 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2460598370, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2460598370, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2460598370, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2460598370_H
#ifndef JSONDECODER_T4269206925_H
#define JSONDECODER_T4269206925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpJson.JsonDecoder
struct  JsonDecoder_t4269206925  : public RuntimeObject
{
public:
	// System.String SharpJson.JsonDecoder::<errorMessage>k__BackingField
	String_t* ___U3CerrorMessageU3Ek__BackingField_0;
	// System.Boolean SharpJson.JsonDecoder::<parseNumbersAsFloat>k__BackingField
	bool ___U3CparseNumbersAsFloatU3Ek__BackingField_1;
	// SharpJson.Lexer SharpJson.JsonDecoder::lexer
	Lexer_t2494922068 * ___lexer_2;

public:
	inline static int32_t get_offset_of_U3CerrorMessageU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonDecoder_t4269206925, ___U3CerrorMessageU3Ek__BackingField_0)); }
	inline String_t* get_U3CerrorMessageU3Ek__BackingField_0() const { return ___U3CerrorMessageU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CerrorMessageU3Ek__BackingField_0() { return &___U3CerrorMessageU3Ek__BackingField_0; }
	inline void set_U3CerrorMessageU3Ek__BackingField_0(String_t* value)
	{
		___U3CerrorMessageU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CerrorMessageU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonDecoder_t4269206925, ___U3CparseNumbersAsFloatU3Ek__BackingField_1)); }
	inline bool get_U3CparseNumbersAsFloatU3Ek__BackingField_1() const { return ___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return &___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline void set_U3CparseNumbersAsFloatU3Ek__BackingField_1(bool value)
	{
		___U3CparseNumbersAsFloatU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_lexer_2() { return static_cast<int32_t>(offsetof(JsonDecoder_t4269206925, ___lexer_2)); }
	inline Lexer_t2494922068 * get_lexer_2() const { return ___lexer_2; }
	inline Lexer_t2494922068 ** get_address_of_lexer_2() { return &___lexer_2; }
	inline void set_lexer_2(Lexer_t2494922068 * value)
	{
		___lexer_2 = value;
		Il2CppCodeGenWriteBarrier((&___lexer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDECODER_T4269206925_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1250445569_H
#define U3CSTARTU3EC__ITERATOR0_T1250445569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.SpineboyPole/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1250445569  : public RuntimeObject
{
public:
	// Spine.AnimationState Spine.Unity.Examples.SpineboyPole/<Start>c__Iterator0::<state>__0
	AnimationState_t2858486651 * ___U3CstateU3E__0_0;
	// Spine.TrackEntry Spine.Unity.Examples.SpineboyPole/<Start>c__Iterator0::<poleTrack>__1
	TrackEntry_t4062297782 * ___U3CpoleTrackU3E__1_1;
	// Spine.Unity.Examples.SpineboyPole Spine.Unity.Examples.SpineboyPole/<Start>c__Iterator0::$this
	SpineboyPole_t3969514969 * ___U24this_2;
	// System.Object Spine.Unity.Examples.SpineboyPole/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Spine.Unity.Examples.SpineboyPole/<Start>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Spine.Unity.Examples.SpineboyPole/<Start>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_U3CstateU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1250445569, ___U3CstateU3E__0_0)); }
	inline AnimationState_t2858486651 * get_U3CstateU3E__0_0() const { return ___U3CstateU3E__0_0; }
	inline AnimationState_t2858486651 ** get_address_of_U3CstateU3E__0_0() { return &___U3CstateU3E__0_0; }
	inline void set_U3CstateU3E__0_0(AnimationState_t2858486651 * value)
	{
		___U3CstateU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstateU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CpoleTrackU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1250445569, ___U3CpoleTrackU3E__1_1)); }
	inline TrackEntry_t4062297782 * get_U3CpoleTrackU3E__1_1() const { return ___U3CpoleTrackU3E__1_1; }
	inline TrackEntry_t4062297782 ** get_address_of_U3CpoleTrackU3E__1_1() { return &___U3CpoleTrackU3E__1_1; }
	inline void set_U3CpoleTrackU3E__1_1(TrackEntry_t4062297782 * value)
	{
		___U3CpoleTrackU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpoleTrackU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1250445569, ___U24this_2)); }
	inline SpineboyPole_t3969514969 * get_U24this_2() const { return ___U24this_2; }
	inline SpineboyPole_t3969514969 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(SpineboyPole_t3969514969 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1250445569, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1250445569, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1250445569, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1250445569_H
#ifndef LEXER_T2494922068_H
#define LEXER_T2494922068_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpJson.Lexer
struct  Lexer_t2494922068  : public RuntimeObject
{
public:
	// System.Int32 SharpJson.Lexer::<lineNumber>k__BackingField
	int32_t ___U3ClineNumberU3Ek__BackingField_0;
	// System.Boolean SharpJson.Lexer::<parseNumbersAsFloat>k__BackingField
	bool ___U3CparseNumbersAsFloatU3Ek__BackingField_1;
	// System.Char[] SharpJson.Lexer::json
	CharU5BU5D_t3669675803* ___json_2;
	// System.Int32 SharpJson.Lexer::index
	int32_t ___index_3;
	// System.Boolean SharpJson.Lexer::success
	bool ___success_4;
	// System.Char[] SharpJson.Lexer::stringBuffer
	CharU5BU5D_t3669675803* ___stringBuffer_5;

public:
	inline static int32_t get_offset_of_U3ClineNumberU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Lexer_t2494922068, ___U3ClineNumberU3Ek__BackingField_0)); }
	inline int32_t get_U3ClineNumberU3Ek__BackingField_0() const { return ___U3ClineNumberU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3ClineNumberU3Ek__BackingField_0() { return &___U3ClineNumberU3Ek__BackingField_0; }
	inline void set_U3ClineNumberU3Ek__BackingField_0(int32_t value)
	{
		___U3ClineNumberU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Lexer_t2494922068, ___U3CparseNumbersAsFloatU3Ek__BackingField_1)); }
	inline bool get_U3CparseNumbersAsFloatU3Ek__BackingField_1() const { return ___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CparseNumbersAsFloatU3Ek__BackingField_1() { return &___U3CparseNumbersAsFloatU3Ek__BackingField_1; }
	inline void set_U3CparseNumbersAsFloatU3Ek__BackingField_1(bool value)
	{
		___U3CparseNumbersAsFloatU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_json_2() { return static_cast<int32_t>(offsetof(Lexer_t2494922068, ___json_2)); }
	inline CharU5BU5D_t3669675803* get_json_2() const { return ___json_2; }
	inline CharU5BU5D_t3669675803** get_address_of_json_2() { return &___json_2; }
	inline void set_json_2(CharU5BU5D_t3669675803* value)
	{
		___json_2 = value;
		Il2CppCodeGenWriteBarrier((&___json_2), value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(Lexer_t2494922068, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_success_4() { return static_cast<int32_t>(offsetof(Lexer_t2494922068, ___success_4)); }
	inline bool get_success_4() const { return ___success_4; }
	inline bool* get_address_of_success_4() { return &___success_4; }
	inline void set_success_4(bool value)
	{
		___success_4 = value;
	}

	inline static int32_t get_offset_of_stringBuffer_5() { return static_cast<int32_t>(offsetof(Lexer_t2494922068, ___stringBuffer_5)); }
	inline CharU5BU5D_t3669675803* get_stringBuffer_5() const { return ___stringBuffer_5; }
	inline CharU5BU5D_t3669675803** get_address_of_stringBuffer_5() { return &___stringBuffer_5; }
	inline void set_stringBuffer_5(CharU5BU5D_t3669675803* value)
	{
		___stringBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___stringBuffer_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXER_T2494922068_H
#ifndef JSON_T605086765_H
#define JSON_T605086765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Json
struct  Json_t605086765  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T605086765_H
#ifndef IKCONSTRAINTDATA_T3215615709_H
#define IKCONSTRAINTDATA_T3215615709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.IkConstraintData
struct  IkConstraintData_t3215615709  : public RuntimeObject
{
public:
	// System.String Spine.IkConstraintData::name
	String_t* ___name_0;
	// System.Int32 Spine.IkConstraintData::order
	int32_t ___order_1;
	// System.Collections.Generic.List`1<Spine.BoneData> Spine.IkConstraintData::bones
	List_1_t3318075177 * ___bones_2;
	// Spine.BoneData Spine.IkConstraintData::target
	BoneData_t1194659452 * ___target_3;
	// System.Int32 Spine.IkConstraintData::bendDirection
	int32_t ___bendDirection_4;
	// System.Single Spine.IkConstraintData::mix
	float ___mix_5;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(IkConstraintData_t3215615709, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(IkConstraintData_t3215615709, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_bones_2() { return static_cast<int32_t>(offsetof(IkConstraintData_t3215615709, ___bones_2)); }
	inline List_1_t3318075177 * get_bones_2() const { return ___bones_2; }
	inline List_1_t3318075177 ** get_address_of_bones_2() { return &___bones_2; }
	inline void set_bones_2(List_1_t3318075177 * value)
	{
		___bones_2 = value;
		Il2CppCodeGenWriteBarrier((&___bones_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(IkConstraintData_t3215615709, ___target_3)); }
	inline BoneData_t1194659452 * get_target_3() const { return ___target_3; }
	inline BoneData_t1194659452 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(BoneData_t1194659452 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_bendDirection_4() { return static_cast<int32_t>(offsetof(IkConstraintData_t3215615709, ___bendDirection_4)); }
	inline int32_t get_bendDirection_4() const { return ___bendDirection_4; }
	inline int32_t* get_address_of_bendDirection_4() { return &___bendDirection_4; }
	inline void set_bendDirection_4(int32_t value)
	{
		___bendDirection_4 = value;
	}

	inline static int32_t get_offset_of_mix_5() { return static_cast<int32_t>(offsetof(IkConstraintData_t3215615709, ___mix_5)); }
	inline float get_mix_5() const { return ___mix_5; }
	inline float* get_address_of_mix_5() { return &___mix_5; }
	inline void set_mix_5(float value)
	{
		___mix_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCONSTRAINTDATA_T3215615709_H
#ifndef ANIMATION_T2067143511_H
#define ANIMATION_T2067143511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Animation
struct  Animation_t2067143511  : public RuntimeObject
{
public:
	// Spine.ExposedList`1<Spine.Timeline> Spine.Animation::timelines
	ExposedList_1_t1117166572 * ___timelines_0;
	// System.Single Spine.Animation::duration
	float ___duration_1;
	// System.String Spine.Animation::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_timelines_0() { return static_cast<int32_t>(offsetof(Animation_t2067143511, ___timelines_0)); }
	inline ExposedList_1_t1117166572 * get_timelines_0() const { return ___timelines_0; }
	inline ExposedList_1_t1117166572 ** get_address_of_timelines_0() { return &___timelines_0; }
	inline void set_timelines_0(ExposedList_1_t1117166572 * value)
	{
		___timelines_0 = value;
		Il2CppCodeGenWriteBarrier((&___timelines_0), value);
	}

	inline static int32_t get_offset_of_duration_1() { return static_cast<int32_t>(offsetof(Animation_t2067143511, ___duration_1)); }
	inline float get_duration_1() const { return ___duration_1; }
	inline float* get_address_of_duration_1() { return &___duration_1; }
	inline void set_duration_1(float value)
	{
		___duration_1 = value;
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Animation_t2067143511, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATION_T2067143511_H
#ifndef ANIMATIONSTATE_T2858486651_H
#define ANIMATIONSTATE_T2858486651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState
struct  AnimationState_t2858486651  : public RuntimeObject
{
public:
	// Spine.AnimationStateData Spine.AnimationState::data
	AnimationStateData_t1644285503 * ___data_5;
	// Spine.Pool`1<Spine.TrackEntry> Spine.AnimationState::trackEntryPool
	Pool_1_t4075013739 * ___trackEntryPool_6;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.AnimationState::tracks
	ExposedList_1_t1617326325 * ___tracks_7;
	// Spine.ExposedList`1<Spine.Event> Spine.AnimationState::events
	ExposedList_1_t1877711358 * ___events_8;
	// Spine.EventQueue Spine.AnimationState::queue
	EventQueue_t2605283308 * ___queue_9;
	// System.Collections.Generic.HashSet`1<System.Int32> Spine.AnimationState::propertyIDs
	HashSet_1_t2711375974 * ___propertyIDs_10;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.AnimationState::mixingTo
	ExposedList_1_t1617326325 * ___mixingTo_11;
	// System.Boolean Spine.AnimationState::animationsChanged
	bool ___animationsChanged_12;
	// System.Single Spine.AnimationState::timeScale
	float ___timeScale_13;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Interrupt
	TrackEntryDelegate_t1807657294 * ___Interrupt_14;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::End
	TrackEntryDelegate_t1807657294 * ___End_15;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Dispose
	TrackEntryDelegate_t1807657294 * ___Dispose_16;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Complete
	TrackEntryDelegate_t1807657294 * ___Complete_17;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Start
	TrackEntryDelegate_t1807657294 * ___Start_18;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.AnimationState::Event
	TrackEntryEventDelegate_t471240152 * ___Event_19;

public:
	inline static int32_t get_offset_of_data_5() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___data_5)); }
	inline AnimationStateData_t1644285503 * get_data_5() const { return ___data_5; }
	inline AnimationStateData_t1644285503 ** get_address_of_data_5() { return &___data_5; }
	inline void set_data_5(AnimationStateData_t1644285503 * value)
	{
		___data_5 = value;
		Il2CppCodeGenWriteBarrier((&___data_5), value);
	}

	inline static int32_t get_offset_of_trackEntryPool_6() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___trackEntryPool_6)); }
	inline Pool_1_t4075013739 * get_trackEntryPool_6() const { return ___trackEntryPool_6; }
	inline Pool_1_t4075013739 ** get_address_of_trackEntryPool_6() { return &___trackEntryPool_6; }
	inline void set_trackEntryPool_6(Pool_1_t4075013739 * value)
	{
		___trackEntryPool_6 = value;
		Il2CppCodeGenWriteBarrier((&___trackEntryPool_6), value);
	}

	inline static int32_t get_offset_of_tracks_7() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___tracks_7)); }
	inline ExposedList_1_t1617326325 * get_tracks_7() const { return ___tracks_7; }
	inline ExposedList_1_t1617326325 ** get_address_of_tracks_7() { return &___tracks_7; }
	inline void set_tracks_7(ExposedList_1_t1617326325 * value)
	{
		___tracks_7 = value;
		Il2CppCodeGenWriteBarrier((&___tracks_7), value);
	}

	inline static int32_t get_offset_of_events_8() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___events_8)); }
	inline ExposedList_1_t1877711358 * get_events_8() const { return ___events_8; }
	inline ExposedList_1_t1877711358 ** get_address_of_events_8() { return &___events_8; }
	inline void set_events_8(ExposedList_1_t1877711358 * value)
	{
		___events_8 = value;
		Il2CppCodeGenWriteBarrier((&___events_8), value);
	}

	inline static int32_t get_offset_of_queue_9() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___queue_9)); }
	inline EventQueue_t2605283308 * get_queue_9() const { return ___queue_9; }
	inline EventQueue_t2605283308 ** get_address_of_queue_9() { return &___queue_9; }
	inline void set_queue_9(EventQueue_t2605283308 * value)
	{
		___queue_9 = value;
		Il2CppCodeGenWriteBarrier((&___queue_9), value);
	}

	inline static int32_t get_offset_of_propertyIDs_10() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___propertyIDs_10)); }
	inline HashSet_1_t2711375974 * get_propertyIDs_10() const { return ___propertyIDs_10; }
	inline HashSet_1_t2711375974 ** get_address_of_propertyIDs_10() { return &___propertyIDs_10; }
	inline void set_propertyIDs_10(HashSet_1_t2711375974 * value)
	{
		___propertyIDs_10 = value;
		Il2CppCodeGenWriteBarrier((&___propertyIDs_10), value);
	}

	inline static int32_t get_offset_of_mixingTo_11() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___mixingTo_11)); }
	inline ExposedList_1_t1617326325 * get_mixingTo_11() const { return ___mixingTo_11; }
	inline ExposedList_1_t1617326325 ** get_address_of_mixingTo_11() { return &___mixingTo_11; }
	inline void set_mixingTo_11(ExposedList_1_t1617326325 * value)
	{
		___mixingTo_11 = value;
		Il2CppCodeGenWriteBarrier((&___mixingTo_11), value);
	}

	inline static int32_t get_offset_of_animationsChanged_12() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___animationsChanged_12)); }
	inline bool get_animationsChanged_12() const { return ___animationsChanged_12; }
	inline bool* get_address_of_animationsChanged_12() { return &___animationsChanged_12; }
	inline void set_animationsChanged_12(bool value)
	{
		___animationsChanged_12 = value;
	}

	inline static int32_t get_offset_of_timeScale_13() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___timeScale_13)); }
	inline float get_timeScale_13() const { return ___timeScale_13; }
	inline float* get_address_of_timeScale_13() { return &___timeScale_13; }
	inline void set_timeScale_13(float value)
	{
		___timeScale_13 = value;
	}

	inline static int32_t get_offset_of_Interrupt_14() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___Interrupt_14)); }
	inline TrackEntryDelegate_t1807657294 * get_Interrupt_14() const { return ___Interrupt_14; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Interrupt_14() { return &___Interrupt_14; }
	inline void set_Interrupt_14(TrackEntryDelegate_t1807657294 * value)
	{
		___Interrupt_14 = value;
		Il2CppCodeGenWriteBarrier((&___Interrupt_14), value);
	}

	inline static int32_t get_offset_of_End_15() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___End_15)); }
	inline TrackEntryDelegate_t1807657294 * get_End_15() const { return ___End_15; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_End_15() { return &___End_15; }
	inline void set_End_15(TrackEntryDelegate_t1807657294 * value)
	{
		___End_15 = value;
		Il2CppCodeGenWriteBarrier((&___End_15), value);
	}

	inline static int32_t get_offset_of_Dispose_16() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___Dispose_16)); }
	inline TrackEntryDelegate_t1807657294 * get_Dispose_16() const { return ___Dispose_16; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Dispose_16() { return &___Dispose_16; }
	inline void set_Dispose_16(TrackEntryDelegate_t1807657294 * value)
	{
		___Dispose_16 = value;
		Il2CppCodeGenWriteBarrier((&___Dispose_16), value);
	}

	inline static int32_t get_offset_of_Complete_17() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___Complete_17)); }
	inline TrackEntryDelegate_t1807657294 * get_Complete_17() const { return ___Complete_17; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Complete_17() { return &___Complete_17; }
	inline void set_Complete_17(TrackEntryDelegate_t1807657294 * value)
	{
		___Complete_17 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_17), value);
	}

	inline static int32_t get_offset_of_Start_18() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___Start_18)); }
	inline TrackEntryDelegate_t1807657294 * get_Start_18() const { return ___Start_18; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Start_18() { return &___Start_18; }
	inline void set_Start_18(TrackEntryDelegate_t1807657294 * value)
	{
		___Start_18 = value;
		Il2CppCodeGenWriteBarrier((&___Start_18), value);
	}

	inline static int32_t get_offset_of_Event_19() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___Event_19)); }
	inline TrackEntryEventDelegate_t471240152 * get_Event_19() const { return ___Event_19; }
	inline TrackEntryEventDelegate_t471240152 ** get_address_of_Event_19() { return &___Event_19; }
	inline void set_Event_19(TrackEntryEventDelegate_t471240152 * value)
	{
		___Event_19 = value;
		Il2CppCodeGenWriteBarrier((&___Event_19), value);
	}
};

struct AnimationState_t2858486651_StaticFields
{
public:
	// Spine.Animation Spine.AnimationState::EmptyAnimation
	Animation_t2067143511 * ___EmptyAnimation_0;

public:
	inline static int32_t get_offset_of_EmptyAnimation_0() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651_StaticFields, ___EmptyAnimation_0)); }
	inline Animation_t2067143511 * get_EmptyAnimation_0() const { return ___EmptyAnimation_0; }
	inline Animation_t2067143511 ** get_address_of_EmptyAnimation_0() { return &___EmptyAnimation_0; }
	inline void set_EmptyAnimation_0(Animation_t2067143511 * value)
	{
		___EmptyAnimation_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyAnimation_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATE_T2858486651_H
#ifndef CURVETIMELINE_T917256691_H
#define CURVETIMELINE_T917256691_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.CurveTimeline
struct  CurveTimeline_t917256691  : public RuntimeObject
{
public:
	// System.Single[] Spine.CurveTimeline::curves
	SingleU5BU5D_t3989243465* ___curves_4;

public:
	inline static int32_t get_offset_of_curves_4() { return static_cast<int32_t>(offsetof(CurveTimeline_t917256691, ___curves_4)); }
	inline SingleU5BU5D_t3989243465* get_curves_4() const { return ___curves_4; }
	inline SingleU5BU5D_t3989243465** get_address_of_curves_4() { return &___curves_4; }
	inline void set_curves_4(SingleU5BU5D_t3989243465* value)
	{
		___curves_4 = value;
		Il2CppCodeGenWriteBarrier((&___curves_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVETIMELINE_T917256691_H
#ifndef DRAWORDERTIMELINE_T2720399701_H
#define DRAWORDERTIMELINE_T2720399701_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.DrawOrderTimeline
struct  DrawOrderTimeline_t2720399701  : public RuntimeObject
{
public:
	// System.Single[] Spine.DrawOrderTimeline::frames
	SingleU5BU5D_t3989243465* ___frames_0;
	// System.Int32[][] Spine.DrawOrderTimeline::drawOrders
	Int32U5BU5DU5BU5D_t392103664* ___drawOrders_1;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(DrawOrderTimeline_t2720399701, ___frames_0)); }
	inline SingleU5BU5D_t3989243465* get_frames_0() const { return ___frames_0; }
	inline SingleU5BU5D_t3989243465** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(SingleU5BU5D_t3989243465* value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}

	inline static int32_t get_offset_of_drawOrders_1() { return static_cast<int32_t>(offsetof(DrawOrderTimeline_t2720399701, ___drawOrders_1)); }
	inline Int32U5BU5DU5BU5D_t392103664* get_drawOrders_1() const { return ___drawOrders_1; }
	inline Int32U5BU5DU5BU5D_t392103664** get_address_of_drawOrders_1() { return &___drawOrders_1; }
	inline void set_drawOrders_1(Int32U5BU5DU5BU5D_t392103664* value)
	{
		___drawOrders_1 = value;
		Il2CppCodeGenWriteBarrier((&___drawOrders_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAWORDERTIMELINE_T2720399701_H
#ifndef EVENTTIMELINE_T2506373614_H
#define EVENTTIMELINE_T2506373614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventTimeline
struct  EventTimeline_t2506373614  : public RuntimeObject
{
public:
	// System.Single[] Spine.EventTimeline::frames
	SingleU5BU5D_t3989243465* ___frames_0;
	// Spine.Event[] Spine.EventTimeline::events
	EventU5BU5D_t1022697062* ___events_1;

public:
	inline static int32_t get_offset_of_frames_0() { return static_cast<int32_t>(offsetof(EventTimeline_t2506373614, ___frames_0)); }
	inline SingleU5BU5D_t3989243465* get_frames_0() const { return ___frames_0; }
	inline SingleU5BU5D_t3989243465** get_address_of_frames_0() { return &___frames_0; }
	inline void set_frames_0(SingleU5BU5D_t3989243465* value)
	{
		___frames_0 = value;
		Il2CppCodeGenWriteBarrier((&___frames_0), value);
	}

	inline static int32_t get_offset_of_events_1() { return static_cast<int32_t>(offsetof(EventTimeline_t2506373614, ___events_1)); }
	inline EventU5BU5D_t1022697062* get_events_1() const { return ___events_1; }
	inline EventU5BU5D_t1022697062** get_address_of_events_1() { return &___events_1; }
	inline void set_events_1(EventU5BU5D_t1022697062* value)
	{
		___events_1 = value;
		Il2CppCodeGenWriteBarrier((&___events_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTIMELINE_T2506373614_H
#ifndef EVENT_T27715519_H
#define EVENT_T27715519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Event
struct  Event_t27715519  : public RuntimeObject
{
public:
	// Spine.EventData Spine.Event::data
	EventData_t295981108 * ___data_0;
	// System.Single Spine.Event::time
	float ___time_1;
	// System.Int32 Spine.Event::intValue
	int32_t ___intValue_2;
	// System.Single Spine.Event::floatValue
	float ___floatValue_3;
	// System.String Spine.Event::stringValue
	String_t* ___stringValue_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Event_t27715519, ___data_0)); }
	inline EventData_t295981108 * get_data_0() const { return ___data_0; }
	inline EventData_t295981108 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(EventData_t295981108 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(Event_t27715519, ___time_1)); }
	inline float get_time_1() const { return ___time_1; }
	inline float* get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(float value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_intValue_2() { return static_cast<int32_t>(offsetof(Event_t27715519, ___intValue_2)); }
	inline int32_t get_intValue_2() const { return ___intValue_2; }
	inline int32_t* get_address_of_intValue_2() { return &___intValue_2; }
	inline void set_intValue_2(int32_t value)
	{
		___intValue_2 = value;
	}

	inline static int32_t get_offset_of_floatValue_3() { return static_cast<int32_t>(offsetof(Event_t27715519, ___floatValue_3)); }
	inline float get_floatValue_3() const { return ___floatValue_3; }
	inline float* get_address_of_floatValue_3() { return &___floatValue_3; }
	inline void set_floatValue_3(float value)
	{
		___floatValue_3 = value;
	}

	inline static int32_t get_offset_of_stringValue_4() { return static_cast<int32_t>(offsetof(Event_t27715519, ___stringValue_4)); }
	inline String_t* get_stringValue_4() const { return ___stringValue_4; }
	inline String_t** get_address_of_stringValue_4() { return &___stringValue_4; }
	inline void set_stringValue_4(String_t* value)
	{
		___stringValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENT_T27715519_H
#ifndef EVENTPAIR_T1359900303_H
#define EVENTPAIR_T1359900303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SpineEventUnityHandler/EventPair
struct  EventPair_t1359900303  : public RuntimeObject
{
public:
	// System.String Spine.Unity.Modules.SpineEventUnityHandler/EventPair::spineEvent
	String_t* ___spineEvent_0;
	// UnityEngine.Events.UnityEvent Spine.Unity.Modules.SpineEventUnityHandler/EventPair::unityHandler
	UnityEvent_t369092746 * ___unityHandler_1;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.Unity.Modules.SpineEventUnityHandler/EventPair::eventDelegate
	TrackEntryEventDelegate_t471240152 * ___eventDelegate_2;

public:
	inline static int32_t get_offset_of_spineEvent_0() { return static_cast<int32_t>(offsetof(EventPair_t1359900303, ___spineEvent_0)); }
	inline String_t* get_spineEvent_0() const { return ___spineEvent_0; }
	inline String_t** get_address_of_spineEvent_0() { return &___spineEvent_0; }
	inline void set_spineEvent_0(String_t* value)
	{
		___spineEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___spineEvent_0), value);
	}

	inline static int32_t get_offset_of_unityHandler_1() { return static_cast<int32_t>(offsetof(EventPair_t1359900303, ___unityHandler_1)); }
	inline UnityEvent_t369092746 * get_unityHandler_1() const { return ___unityHandler_1; }
	inline UnityEvent_t369092746 ** get_address_of_unityHandler_1() { return &___unityHandler_1; }
	inline void set_unityHandler_1(UnityEvent_t369092746 * value)
	{
		___unityHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___unityHandler_1), value);
	}

	inline static int32_t get_offset_of_eventDelegate_2() { return static_cast<int32_t>(offsetof(EventPair_t1359900303, ___eventDelegate_2)); }
	inline TrackEntryEventDelegate_t471240152 * get_eventDelegate_2() const { return ___eventDelegate_2; }
	inline TrackEntryEventDelegate_t471240152 ** get_address_of_eventDelegate_2() { return &___eventDelegate_2; }
	inline void set_eventDelegate_2(TrackEntryEventDelegate_t471240152 * value)
	{
		___eventDelegate_2 = value;
		Il2CppCodeGenWriteBarrier((&___eventDelegate_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTPAIR_T1359900303_H
#ifndef TRACKENTRY_T4062297782_H
#define TRACKENTRY_T4062297782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TrackEntry
struct  TrackEntry_t4062297782  : public RuntimeObject
{
public:
	// Spine.Animation Spine.TrackEntry::animation
	Animation_t2067143511 * ___animation_0;
	// Spine.TrackEntry Spine.TrackEntry::next
	TrackEntry_t4062297782 * ___next_1;
	// Spine.TrackEntry Spine.TrackEntry::mixingFrom
	TrackEntry_t4062297782 * ___mixingFrom_2;
	// System.Int32 Spine.TrackEntry::trackIndex
	int32_t ___trackIndex_3;
	// System.Boolean Spine.TrackEntry::loop
	bool ___loop_4;
	// System.Single Spine.TrackEntry::eventThreshold
	float ___eventThreshold_5;
	// System.Single Spine.TrackEntry::attachmentThreshold
	float ___attachmentThreshold_6;
	// System.Single Spine.TrackEntry::drawOrderThreshold
	float ___drawOrderThreshold_7;
	// System.Single Spine.TrackEntry::animationStart
	float ___animationStart_8;
	// System.Single Spine.TrackEntry::animationEnd
	float ___animationEnd_9;
	// System.Single Spine.TrackEntry::animationLast
	float ___animationLast_10;
	// System.Single Spine.TrackEntry::nextAnimationLast
	float ___nextAnimationLast_11;
	// System.Single Spine.TrackEntry::delay
	float ___delay_12;
	// System.Single Spine.TrackEntry::trackTime
	float ___trackTime_13;
	// System.Single Spine.TrackEntry::trackLast
	float ___trackLast_14;
	// System.Single Spine.TrackEntry::nextTrackLast
	float ___nextTrackLast_15;
	// System.Single Spine.TrackEntry::trackEnd
	float ___trackEnd_16;
	// System.Single Spine.TrackEntry::timeScale
	float ___timeScale_17;
	// System.Single Spine.TrackEntry::alpha
	float ___alpha_18;
	// System.Single Spine.TrackEntry::mixTime
	float ___mixTime_19;
	// System.Single Spine.TrackEntry::mixDuration
	float ___mixDuration_20;
	// System.Single Spine.TrackEntry::interruptAlpha
	float ___interruptAlpha_21;
	// System.Single Spine.TrackEntry::totalAlpha
	float ___totalAlpha_22;
	// Spine.ExposedList`1<System.Int32> Spine.TrackEntry::timelineData
	ExposedList_1_t1671071859 * ___timelineData_23;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.TrackEntry::timelineDipMix
	ExposedList_1_t1617326325 * ___timelineDipMix_24;
	// Spine.ExposedList`1<System.Single> Spine.TrackEntry::timelinesRotation
	ExposedList_1_t3384296343 * ___timelinesRotation_25;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Interrupt
	TrackEntryDelegate_t1807657294 * ___Interrupt_26;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::End
	TrackEntryDelegate_t1807657294 * ___End_27;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Dispose
	TrackEntryDelegate_t1807657294 * ___Dispose_28;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Complete
	TrackEntryDelegate_t1807657294 * ___Complete_29;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Start
	TrackEntryDelegate_t1807657294 * ___Start_30;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.TrackEntry::Event
	TrackEntryEventDelegate_t471240152 * ___Event_31;

public:
	inline static int32_t get_offset_of_animation_0() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___animation_0)); }
	inline Animation_t2067143511 * get_animation_0() const { return ___animation_0; }
	inline Animation_t2067143511 ** get_address_of_animation_0() { return &___animation_0; }
	inline void set_animation_0(Animation_t2067143511 * value)
	{
		___animation_0 = value;
		Il2CppCodeGenWriteBarrier((&___animation_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___next_1)); }
	inline TrackEntry_t4062297782 * get_next_1() const { return ___next_1; }
	inline TrackEntry_t4062297782 ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(TrackEntry_t4062297782 * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((&___next_1), value);
	}

	inline static int32_t get_offset_of_mixingFrom_2() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___mixingFrom_2)); }
	inline TrackEntry_t4062297782 * get_mixingFrom_2() const { return ___mixingFrom_2; }
	inline TrackEntry_t4062297782 ** get_address_of_mixingFrom_2() { return &___mixingFrom_2; }
	inline void set_mixingFrom_2(TrackEntry_t4062297782 * value)
	{
		___mixingFrom_2 = value;
		Il2CppCodeGenWriteBarrier((&___mixingFrom_2), value);
	}

	inline static int32_t get_offset_of_trackIndex_3() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___trackIndex_3)); }
	inline int32_t get_trackIndex_3() const { return ___trackIndex_3; }
	inline int32_t* get_address_of_trackIndex_3() { return &___trackIndex_3; }
	inline void set_trackIndex_3(int32_t value)
	{
		___trackIndex_3 = value;
	}

	inline static int32_t get_offset_of_loop_4() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___loop_4)); }
	inline bool get_loop_4() const { return ___loop_4; }
	inline bool* get_address_of_loop_4() { return &___loop_4; }
	inline void set_loop_4(bool value)
	{
		___loop_4 = value;
	}

	inline static int32_t get_offset_of_eventThreshold_5() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___eventThreshold_5)); }
	inline float get_eventThreshold_5() const { return ___eventThreshold_5; }
	inline float* get_address_of_eventThreshold_5() { return &___eventThreshold_5; }
	inline void set_eventThreshold_5(float value)
	{
		___eventThreshold_5 = value;
	}

	inline static int32_t get_offset_of_attachmentThreshold_6() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___attachmentThreshold_6)); }
	inline float get_attachmentThreshold_6() const { return ___attachmentThreshold_6; }
	inline float* get_address_of_attachmentThreshold_6() { return &___attachmentThreshold_6; }
	inline void set_attachmentThreshold_6(float value)
	{
		___attachmentThreshold_6 = value;
	}

	inline static int32_t get_offset_of_drawOrderThreshold_7() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___drawOrderThreshold_7)); }
	inline float get_drawOrderThreshold_7() const { return ___drawOrderThreshold_7; }
	inline float* get_address_of_drawOrderThreshold_7() { return &___drawOrderThreshold_7; }
	inline void set_drawOrderThreshold_7(float value)
	{
		___drawOrderThreshold_7 = value;
	}

	inline static int32_t get_offset_of_animationStart_8() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___animationStart_8)); }
	inline float get_animationStart_8() const { return ___animationStart_8; }
	inline float* get_address_of_animationStart_8() { return &___animationStart_8; }
	inline void set_animationStart_8(float value)
	{
		___animationStart_8 = value;
	}

	inline static int32_t get_offset_of_animationEnd_9() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___animationEnd_9)); }
	inline float get_animationEnd_9() const { return ___animationEnd_9; }
	inline float* get_address_of_animationEnd_9() { return &___animationEnd_9; }
	inline void set_animationEnd_9(float value)
	{
		___animationEnd_9 = value;
	}

	inline static int32_t get_offset_of_animationLast_10() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___animationLast_10)); }
	inline float get_animationLast_10() const { return ___animationLast_10; }
	inline float* get_address_of_animationLast_10() { return &___animationLast_10; }
	inline void set_animationLast_10(float value)
	{
		___animationLast_10 = value;
	}

	inline static int32_t get_offset_of_nextAnimationLast_11() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___nextAnimationLast_11)); }
	inline float get_nextAnimationLast_11() const { return ___nextAnimationLast_11; }
	inline float* get_address_of_nextAnimationLast_11() { return &___nextAnimationLast_11; }
	inline void set_nextAnimationLast_11(float value)
	{
		___nextAnimationLast_11 = value;
	}

	inline static int32_t get_offset_of_delay_12() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___delay_12)); }
	inline float get_delay_12() const { return ___delay_12; }
	inline float* get_address_of_delay_12() { return &___delay_12; }
	inline void set_delay_12(float value)
	{
		___delay_12 = value;
	}

	inline static int32_t get_offset_of_trackTime_13() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___trackTime_13)); }
	inline float get_trackTime_13() const { return ___trackTime_13; }
	inline float* get_address_of_trackTime_13() { return &___trackTime_13; }
	inline void set_trackTime_13(float value)
	{
		___trackTime_13 = value;
	}

	inline static int32_t get_offset_of_trackLast_14() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___trackLast_14)); }
	inline float get_trackLast_14() const { return ___trackLast_14; }
	inline float* get_address_of_trackLast_14() { return &___trackLast_14; }
	inline void set_trackLast_14(float value)
	{
		___trackLast_14 = value;
	}

	inline static int32_t get_offset_of_nextTrackLast_15() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___nextTrackLast_15)); }
	inline float get_nextTrackLast_15() const { return ___nextTrackLast_15; }
	inline float* get_address_of_nextTrackLast_15() { return &___nextTrackLast_15; }
	inline void set_nextTrackLast_15(float value)
	{
		___nextTrackLast_15 = value;
	}

	inline static int32_t get_offset_of_trackEnd_16() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___trackEnd_16)); }
	inline float get_trackEnd_16() const { return ___trackEnd_16; }
	inline float* get_address_of_trackEnd_16() { return &___trackEnd_16; }
	inline void set_trackEnd_16(float value)
	{
		___trackEnd_16 = value;
	}

	inline static int32_t get_offset_of_timeScale_17() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___timeScale_17)); }
	inline float get_timeScale_17() const { return ___timeScale_17; }
	inline float* get_address_of_timeScale_17() { return &___timeScale_17; }
	inline void set_timeScale_17(float value)
	{
		___timeScale_17 = value;
	}

	inline static int32_t get_offset_of_alpha_18() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___alpha_18)); }
	inline float get_alpha_18() const { return ___alpha_18; }
	inline float* get_address_of_alpha_18() { return &___alpha_18; }
	inline void set_alpha_18(float value)
	{
		___alpha_18 = value;
	}

	inline static int32_t get_offset_of_mixTime_19() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___mixTime_19)); }
	inline float get_mixTime_19() const { return ___mixTime_19; }
	inline float* get_address_of_mixTime_19() { return &___mixTime_19; }
	inline void set_mixTime_19(float value)
	{
		___mixTime_19 = value;
	}

	inline static int32_t get_offset_of_mixDuration_20() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___mixDuration_20)); }
	inline float get_mixDuration_20() const { return ___mixDuration_20; }
	inline float* get_address_of_mixDuration_20() { return &___mixDuration_20; }
	inline void set_mixDuration_20(float value)
	{
		___mixDuration_20 = value;
	}

	inline static int32_t get_offset_of_interruptAlpha_21() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___interruptAlpha_21)); }
	inline float get_interruptAlpha_21() const { return ___interruptAlpha_21; }
	inline float* get_address_of_interruptAlpha_21() { return &___interruptAlpha_21; }
	inline void set_interruptAlpha_21(float value)
	{
		___interruptAlpha_21 = value;
	}

	inline static int32_t get_offset_of_totalAlpha_22() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___totalAlpha_22)); }
	inline float get_totalAlpha_22() const { return ___totalAlpha_22; }
	inline float* get_address_of_totalAlpha_22() { return &___totalAlpha_22; }
	inline void set_totalAlpha_22(float value)
	{
		___totalAlpha_22 = value;
	}

	inline static int32_t get_offset_of_timelineData_23() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___timelineData_23)); }
	inline ExposedList_1_t1671071859 * get_timelineData_23() const { return ___timelineData_23; }
	inline ExposedList_1_t1671071859 ** get_address_of_timelineData_23() { return &___timelineData_23; }
	inline void set_timelineData_23(ExposedList_1_t1671071859 * value)
	{
		___timelineData_23 = value;
		Il2CppCodeGenWriteBarrier((&___timelineData_23), value);
	}

	inline static int32_t get_offset_of_timelineDipMix_24() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___timelineDipMix_24)); }
	inline ExposedList_1_t1617326325 * get_timelineDipMix_24() const { return ___timelineDipMix_24; }
	inline ExposedList_1_t1617326325 ** get_address_of_timelineDipMix_24() { return &___timelineDipMix_24; }
	inline void set_timelineDipMix_24(ExposedList_1_t1617326325 * value)
	{
		___timelineDipMix_24 = value;
		Il2CppCodeGenWriteBarrier((&___timelineDipMix_24), value);
	}

	inline static int32_t get_offset_of_timelinesRotation_25() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___timelinesRotation_25)); }
	inline ExposedList_1_t3384296343 * get_timelinesRotation_25() const { return ___timelinesRotation_25; }
	inline ExposedList_1_t3384296343 ** get_address_of_timelinesRotation_25() { return &___timelinesRotation_25; }
	inline void set_timelinesRotation_25(ExposedList_1_t3384296343 * value)
	{
		___timelinesRotation_25 = value;
		Il2CppCodeGenWriteBarrier((&___timelinesRotation_25), value);
	}

	inline static int32_t get_offset_of_Interrupt_26() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___Interrupt_26)); }
	inline TrackEntryDelegate_t1807657294 * get_Interrupt_26() const { return ___Interrupt_26; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Interrupt_26() { return &___Interrupt_26; }
	inline void set_Interrupt_26(TrackEntryDelegate_t1807657294 * value)
	{
		___Interrupt_26 = value;
		Il2CppCodeGenWriteBarrier((&___Interrupt_26), value);
	}

	inline static int32_t get_offset_of_End_27() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___End_27)); }
	inline TrackEntryDelegate_t1807657294 * get_End_27() const { return ___End_27; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_End_27() { return &___End_27; }
	inline void set_End_27(TrackEntryDelegate_t1807657294 * value)
	{
		___End_27 = value;
		Il2CppCodeGenWriteBarrier((&___End_27), value);
	}

	inline static int32_t get_offset_of_Dispose_28() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___Dispose_28)); }
	inline TrackEntryDelegate_t1807657294 * get_Dispose_28() const { return ___Dispose_28; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Dispose_28() { return &___Dispose_28; }
	inline void set_Dispose_28(TrackEntryDelegate_t1807657294 * value)
	{
		___Dispose_28 = value;
		Il2CppCodeGenWriteBarrier((&___Dispose_28), value);
	}

	inline static int32_t get_offset_of_Complete_29() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___Complete_29)); }
	inline TrackEntryDelegate_t1807657294 * get_Complete_29() const { return ___Complete_29; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Complete_29() { return &___Complete_29; }
	inline void set_Complete_29(TrackEntryDelegate_t1807657294 * value)
	{
		___Complete_29 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_29), value);
	}

	inline static int32_t get_offset_of_Start_30() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___Start_30)); }
	inline TrackEntryDelegate_t1807657294 * get_Start_30() const { return ___Start_30; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Start_30() { return &___Start_30; }
	inline void set_Start_30(TrackEntryDelegate_t1807657294 * value)
	{
		___Start_30 = value;
		Il2CppCodeGenWriteBarrier((&___Start_30), value);
	}

	inline static int32_t get_offset_of_Event_31() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___Event_31)); }
	inline TrackEntryEventDelegate_t471240152 * get_Event_31() const { return ___Event_31; }
	inline TrackEntryEventDelegate_t471240152 ** get_address_of_Event_31() { return &___Event_31; }
	inline void set_Event_31(TrackEntryEventDelegate_t471240152 * value)
	{
		___Event_31 = value;
		Il2CppCodeGenWriteBarrier((&___Event_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRY_T4062297782_H
#ifndef U3CSTARTU3EC__ANONSTOREY1_T1282162354_H
#define U3CSTARTU3EC__ANONSTOREY1_T1282162354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SpineEventUnityHandler/<Start>c__AnonStorey1
struct  U3CStartU3Ec__AnonStorey1_t1282162354  : public RuntimeObject
{
public:
	// Spine.Unity.Modules.SpineEventUnityHandler/EventPair Spine.Unity.Modules.SpineEventUnityHandler/<Start>c__AnonStorey1::ep
	EventPair_t1359900303 * ___ep_0;

public:
	inline static int32_t get_offset_of_ep_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__AnonStorey1_t1282162354, ___ep_0)); }
	inline EventPair_t1359900303 * get_ep_0() const { return ___ep_0; }
	inline EventPair_t1359900303 ** get_address_of_ep_0() { return &___ep_0; }
	inline void set_ep_0(EventPair_t1359900303 * value)
	{
		___ep_0 = value;
		Il2CppCodeGenWriteBarrier((&___ep_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ANONSTOREY1_T1282162354_H
#ifndef ANIMATIONSTATEDATA_T1644285503_H
#define ANIMATIONSTATEDATA_T1644285503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationStateData
struct  AnimationStateData_t1644285503  : public RuntimeObject
{
public:
	// Spine.SkeletonData Spine.AnimationStateData::skeletonData
	SkeletonData_t2665604974 * ___skeletonData_0;
	// System.Collections.Generic.Dictionary`2<Spine.AnimationStateData/AnimationPair,System.Single> Spine.AnimationStateData::animationToMixTime
	Dictionary_2_t2450703149 * ___animationToMixTime_1;
	// System.Single Spine.AnimationStateData::defaultMix
	float ___defaultMix_2;

public:
	inline static int32_t get_offset_of_skeletonData_0() { return static_cast<int32_t>(offsetof(AnimationStateData_t1644285503, ___skeletonData_0)); }
	inline SkeletonData_t2665604974 * get_skeletonData_0() const { return ___skeletonData_0; }
	inline SkeletonData_t2665604974 ** get_address_of_skeletonData_0() { return &___skeletonData_0; }
	inline void set_skeletonData_0(SkeletonData_t2665604974 * value)
	{
		___skeletonData_0 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonData_0), value);
	}

	inline static int32_t get_offset_of_animationToMixTime_1() { return static_cast<int32_t>(offsetof(AnimationStateData_t1644285503, ___animationToMixTime_1)); }
	inline Dictionary_2_t2450703149 * get_animationToMixTime_1() const { return ___animationToMixTime_1; }
	inline Dictionary_2_t2450703149 ** get_address_of_animationToMixTime_1() { return &___animationToMixTime_1; }
	inline void set_animationToMixTime_1(Dictionary_2_t2450703149 * value)
	{
		___animationToMixTime_1 = value;
		Il2CppCodeGenWriteBarrier((&___animationToMixTime_1), value);
	}

	inline static int32_t get_offset_of_defaultMix_2() { return static_cast<int32_t>(offsetof(AnimationStateData_t1644285503, ___defaultMix_2)); }
	inline float get_defaultMix_2() const { return ___defaultMix_2; }
	inline float* get_address_of_defaultMix_2() { return &___defaultMix_2; }
	inline void set_defaultMix_2(float value)
	{
		___defaultMix_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATEDATA_T1644285503_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1482506192_H
#define U3CSTARTU3EC__ITERATOR0_T1482506192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.MixAndMatch/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1482506192  : public RuntimeObject
{
public:
	// Spine.Unity.Examples.MixAndMatch Spine.Unity.Examples.MixAndMatch/<Start>c__Iterator0::$this
	MixAndMatch_t1342663373 * ___U24this_0;
	// System.Object Spine.Unity.Examples.MixAndMatch/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Spine.Unity.Examples.MixAndMatch/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Spine.Unity.Examples.MixAndMatch/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1482506192, ___U24this_0)); }
	inline MixAndMatch_t1342663373 * get_U24this_0() const { return ___U24this_0; }
	inline MixAndMatch_t1342663373 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MixAndMatch_t1342663373 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1482506192, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1482506192, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1482506192, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1482506192_H
#ifndef BONE_T1763862836_H
#define BONE_T1763862836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Bone
struct  Bone_t1763862836  : public RuntimeObject
{
public:
	// Spine.BoneData Spine.Bone::data
	BoneData_t1194659452 * ___data_1;
	// Spine.Skeleton Spine.Bone::skeleton
	Skeleton_t1045203634 * ___skeleton_2;
	// Spine.Bone Spine.Bone::parent
	Bone_t1763862836 * ___parent_3;
	// Spine.ExposedList`1<Spine.Bone> Spine.Bone::children
	ExposedList_1_t3613858675 * ___children_4;
	// System.Single Spine.Bone::x
	float ___x_5;
	// System.Single Spine.Bone::y
	float ___y_6;
	// System.Single Spine.Bone::rotation
	float ___rotation_7;
	// System.Single Spine.Bone::scaleX
	float ___scaleX_8;
	// System.Single Spine.Bone::scaleY
	float ___scaleY_9;
	// System.Single Spine.Bone::shearX
	float ___shearX_10;
	// System.Single Spine.Bone::shearY
	float ___shearY_11;
	// System.Single Spine.Bone::ax
	float ___ax_12;
	// System.Single Spine.Bone::ay
	float ___ay_13;
	// System.Single Spine.Bone::arotation
	float ___arotation_14;
	// System.Single Spine.Bone::ascaleX
	float ___ascaleX_15;
	// System.Single Spine.Bone::ascaleY
	float ___ascaleY_16;
	// System.Single Spine.Bone::ashearX
	float ___ashearX_17;
	// System.Single Spine.Bone::ashearY
	float ___ashearY_18;
	// System.Boolean Spine.Bone::appliedValid
	bool ___appliedValid_19;
	// System.Single Spine.Bone::a
	float ___a_20;
	// System.Single Spine.Bone::b
	float ___b_21;
	// System.Single Spine.Bone::worldX
	float ___worldX_22;
	// System.Single Spine.Bone::c
	float ___c_23;
	// System.Single Spine.Bone::d
	float ___d_24;
	// System.Single Spine.Bone::worldY
	float ___worldY_25;
	// System.Boolean Spine.Bone::sorted
	bool ___sorted_26;

public:
	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___data_1)); }
	inline BoneData_t1194659452 * get_data_1() const { return ___data_1; }
	inline BoneData_t1194659452 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(BoneData_t1194659452 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_skeleton_2() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___skeleton_2)); }
	inline Skeleton_t1045203634 * get_skeleton_2() const { return ___skeleton_2; }
	inline Skeleton_t1045203634 ** get_address_of_skeleton_2() { return &___skeleton_2; }
	inline void set_skeleton_2(Skeleton_t1045203634 * value)
	{
		___skeleton_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_2), value);
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___parent_3)); }
	inline Bone_t1763862836 * get_parent_3() const { return ___parent_3; }
	inline Bone_t1763862836 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Bone_t1763862836 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((&___parent_3), value);
	}

	inline static int32_t get_offset_of_children_4() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___children_4)); }
	inline ExposedList_1_t3613858675 * get_children_4() const { return ___children_4; }
	inline ExposedList_1_t3613858675 ** get_address_of_children_4() { return &___children_4; }
	inline void set_children_4(ExposedList_1_t3613858675 * value)
	{
		___children_4 = value;
		Il2CppCodeGenWriteBarrier((&___children_4), value);
	}

	inline static int32_t get_offset_of_x_5() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___x_5)); }
	inline float get_x_5() const { return ___x_5; }
	inline float* get_address_of_x_5() { return &___x_5; }
	inline void set_x_5(float value)
	{
		___x_5 = value;
	}

	inline static int32_t get_offset_of_y_6() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___y_6)); }
	inline float get_y_6() const { return ___y_6; }
	inline float* get_address_of_y_6() { return &___y_6; }
	inline void set_y_6(float value)
	{
		___y_6 = value;
	}

	inline static int32_t get_offset_of_rotation_7() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___rotation_7)); }
	inline float get_rotation_7() const { return ___rotation_7; }
	inline float* get_address_of_rotation_7() { return &___rotation_7; }
	inline void set_rotation_7(float value)
	{
		___rotation_7 = value;
	}

	inline static int32_t get_offset_of_scaleX_8() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___scaleX_8)); }
	inline float get_scaleX_8() const { return ___scaleX_8; }
	inline float* get_address_of_scaleX_8() { return &___scaleX_8; }
	inline void set_scaleX_8(float value)
	{
		___scaleX_8 = value;
	}

	inline static int32_t get_offset_of_scaleY_9() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___scaleY_9)); }
	inline float get_scaleY_9() const { return ___scaleY_9; }
	inline float* get_address_of_scaleY_9() { return &___scaleY_9; }
	inline void set_scaleY_9(float value)
	{
		___scaleY_9 = value;
	}

	inline static int32_t get_offset_of_shearX_10() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___shearX_10)); }
	inline float get_shearX_10() const { return ___shearX_10; }
	inline float* get_address_of_shearX_10() { return &___shearX_10; }
	inline void set_shearX_10(float value)
	{
		___shearX_10 = value;
	}

	inline static int32_t get_offset_of_shearY_11() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___shearY_11)); }
	inline float get_shearY_11() const { return ___shearY_11; }
	inline float* get_address_of_shearY_11() { return &___shearY_11; }
	inline void set_shearY_11(float value)
	{
		___shearY_11 = value;
	}

	inline static int32_t get_offset_of_ax_12() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___ax_12)); }
	inline float get_ax_12() const { return ___ax_12; }
	inline float* get_address_of_ax_12() { return &___ax_12; }
	inline void set_ax_12(float value)
	{
		___ax_12 = value;
	}

	inline static int32_t get_offset_of_ay_13() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___ay_13)); }
	inline float get_ay_13() const { return ___ay_13; }
	inline float* get_address_of_ay_13() { return &___ay_13; }
	inline void set_ay_13(float value)
	{
		___ay_13 = value;
	}

	inline static int32_t get_offset_of_arotation_14() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___arotation_14)); }
	inline float get_arotation_14() const { return ___arotation_14; }
	inline float* get_address_of_arotation_14() { return &___arotation_14; }
	inline void set_arotation_14(float value)
	{
		___arotation_14 = value;
	}

	inline static int32_t get_offset_of_ascaleX_15() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___ascaleX_15)); }
	inline float get_ascaleX_15() const { return ___ascaleX_15; }
	inline float* get_address_of_ascaleX_15() { return &___ascaleX_15; }
	inline void set_ascaleX_15(float value)
	{
		___ascaleX_15 = value;
	}

	inline static int32_t get_offset_of_ascaleY_16() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___ascaleY_16)); }
	inline float get_ascaleY_16() const { return ___ascaleY_16; }
	inline float* get_address_of_ascaleY_16() { return &___ascaleY_16; }
	inline void set_ascaleY_16(float value)
	{
		___ascaleY_16 = value;
	}

	inline static int32_t get_offset_of_ashearX_17() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___ashearX_17)); }
	inline float get_ashearX_17() const { return ___ashearX_17; }
	inline float* get_address_of_ashearX_17() { return &___ashearX_17; }
	inline void set_ashearX_17(float value)
	{
		___ashearX_17 = value;
	}

	inline static int32_t get_offset_of_ashearY_18() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___ashearY_18)); }
	inline float get_ashearY_18() const { return ___ashearY_18; }
	inline float* get_address_of_ashearY_18() { return &___ashearY_18; }
	inline void set_ashearY_18(float value)
	{
		___ashearY_18 = value;
	}

	inline static int32_t get_offset_of_appliedValid_19() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___appliedValid_19)); }
	inline bool get_appliedValid_19() const { return ___appliedValid_19; }
	inline bool* get_address_of_appliedValid_19() { return &___appliedValid_19; }
	inline void set_appliedValid_19(bool value)
	{
		___appliedValid_19 = value;
	}

	inline static int32_t get_offset_of_a_20() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___a_20)); }
	inline float get_a_20() const { return ___a_20; }
	inline float* get_address_of_a_20() { return &___a_20; }
	inline void set_a_20(float value)
	{
		___a_20 = value;
	}

	inline static int32_t get_offset_of_b_21() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___b_21)); }
	inline float get_b_21() const { return ___b_21; }
	inline float* get_address_of_b_21() { return &___b_21; }
	inline void set_b_21(float value)
	{
		___b_21 = value;
	}

	inline static int32_t get_offset_of_worldX_22() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___worldX_22)); }
	inline float get_worldX_22() const { return ___worldX_22; }
	inline float* get_address_of_worldX_22() { return &___worldX_22; }
	inline void set_worldX_22(float value)
	{
		___worldX_22 = value;
	}

	inline static int32_t get_offset_of_c_23() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___c_23)); }
	inline float get_c_23() const { return ___c_23; }
	inline float* get_address_of_c_23() { return &___c_23; }
	inline void set_c_23(float value)
	{
		___c_23 = value;
	}

	inline static int32_t get_offset_of_d_24() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___d_24)); }
	inline float get_d_24() const { return ___d_24; }
	inline float* get_address_of_d_24() { return &___d_24; }
	inline void set_d_24(float value)
	{
		___d_24 = value;
	}

	inline static int32_t get_offset_of_worldY_25() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___worldY_25)); }
	inline float get_worldY_25() const { return ___worldY_25; }
	inline float* get_address_of_worldY_25() { return &___worldY_25; }
	inline void set_worldY_25(float value)
	{
		___worldY_25 = value;
	}

	inline static int32_t get_offset_of_sorted_26() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___sorted_26)); }
	inline bool get_sorted_26() const { return ___sorted_26; }
	inline bool* get_address_of_sorted_26() { return &___sorted_26; }
	inline void set_sorted_26(bool value)
	{
		___sorted_26 = value;
	}
};

struct Bone_t1763862836_StaticFields
{
public:
	// System.Boolean Spine.Bone::yDown
	bool ___yDown_0;

public:
	inline static int32_t get_offset_of_yDown_0() { return static_cast<int32_t>(offsetof(Bone_t1763862836_StaticFields, ___yDown_0)); }
	inline bool get_yDown_0() const { return ___yDown_0; }
	inline bool* get_address_of_yDown_0() { return &___yDown_0; }
	inline void set_yDown_0(bool value)
	{
		___yDown_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONE_T1763862836_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2675052689_H
#define U3CSTARTU3EC__ITERATOR0_T2675052689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.MixAndMatchGraphic/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2675052689  : public RuntimeObject
{
public:
	// Spine.Unity.Examples.MixAndMatchGraphic Spine.Unity.Examples.MixAndMatchGraphic/<Start>c__Iterator0::$this
	MixAndMatchGraphic_t1263195094 * ___U24this_0;
	// System.Object Spine.Unity.Examples.MixAndMatchGraphic/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Spine.Unity.Examples.MixAndMatchGraphic/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Spine.Unity.Examples.MixAndMatchGraphic/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2675052689, ___U24this_0)); }
	inline MixAndMatchGraphic_t1263195094 * get_U24this_0() const { return ___U24this_0; }
	inline MixAndMatchGraphic_t1263195094 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(MixAndMatchGraphic_t1263195094 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2675052689, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2675052689, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2675052689, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2675052689_H
#ifndef U3CWAITUNTILSTOPPEDU3EC__ITERATOR1_T1053025697_H
#define U3CWAITUNTILSTOPPEDU3EC__ITERATOR1_T1053025697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.RaggedySpineboy/<WaitUntilStopped>c__Iterator1
struct  U3CWaitUntilStoppedU3Ec__Iterator1_t1053025697  : public RuntimeObject
{
public:
	// System.Single Spine.Unity.Examples.RaggedySpineboy/<WaitUntilStopped>c__Iterator1::<t>__0
	float ___U3CtU3E__0_0;
	// Spine.Unity.Examples.RaggedySpineboy Spine.Unity.Examples.RaggedySpineboy/<WaitUntilStopped>c__Iterator1::$this
	RaggedySpineboy_t30683967 * ___U24this_1;
	// System.Object Spine.Unity.Examples.RaggedySpineboy/<WaitUntilStopped>c__Iterator1::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean Spine.Unity.Examples.RaggedySpineboy/<WaitUntilStopped>c__Iterator1::$disposing
	bool ___U24disposing_3;
	// System.Int32 Spine.Unity.Examples.RaggedySpineboy/<WaitUntilStopped>c__Iterator1::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CWaitUntilStoppedU3Ec__Iterator1_t1053025697, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CWaitUntilStoppedU3Ec__Iterator1_t1053025697, ___U24this_1)); }
	inline RaggedySpineboy_t30683967 * get_U24this_1() const { return ___U24this_1; }
	inline RaggedySpineboy_t30683967 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RaggedySpineboy_t30683967 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CWaitUntilStoppedU3Ec__Iterator1_t1053025697, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CWaitUntilStoppedU3Ec__Iterator1_t1053025697, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CWaitUntilStoppedU3Ec__Iterator1_t1053025697, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITUNTILSTOPPEDU3EC__ITERATOR1_T1053025697_H
#ifndef EVENTQUEUE_T2605283308_H
#define EVENTQUEUE_T2605283308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventQueue
struct  EventQueue_t2605283308  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Spine.EventQueue/EventQueueEntry> Spine.EventQueue::eventQueueEntries
	List_1_t1585127513 * ___eventQueueEntries_0;
	// System.Boolean Spine.EventQueue::drainDisabled
	bool ___drainDisabled_1;
	// Spine.AnimationState Spine.EventQueue::state
	AnimationState_t2858486651 * ___state_2;
	// Spine.Pool`1<Spine.TrackEntry> Spine.EventQueue::trackEntryPool
	Pool_1_t4075013739 * ___trackEntryPool_3;
	// System.Action Spine.EventQueue::AnimationsChanged
	Action_t3158581658 * ___AnimationsChanged_4;

public:
	inline static int32_t get_offset_of_eventQueueEntries_0() { return static_cast<int32_t>(offsetof(EventQueue_t2605283308, ___eventQueueEntries_0)); }
	inline List_1_t1585127513 * get_eventQueueEntries_0() const { return ___eventQueueEntries_0; }
	inline List_1_t1585127513 ** get_address_of_eventQueueEntries_0() { return &___eventQueueEntries_0; }
	inline void set_eventQueueEntries_0(List_1_t1585127513 * value)
	{
		___eventQueueEntries_0 = value;
		Il2CppCodeGenWriteBarrier((&___eventQueueEntries_0), value);
	}

	inline static int32_t get_offset_of_drainDisabled_1() { return static_cast<int32_t>(offsetof(EventQueue_t2605283308, ___drainDisabled_1)); }
	inline bool get_drainDisabled_1() const { return ___drainDisabled_1; }
	inline bool* get_address_of_drainDisabled_1() { return &___drainDisabled_1; }
	inline void set_drainDisabled_1(bool value)
	{
		___drainDisabled_1 = value;
	}

	inline static int32_t get_offset_of_state_2() { return static_cast<int32_t>(offsetof(EventQueue_t2605283308, ___state_2)); }
	inline AnimationState_t2858486651 * get_state_2() const { return ___state_2; }
	inline AnimationState_t2858486651 ** get_address_of_state_2() { return &___state_2; }
	inline void set_state_2(AnimationState_t2858486651 * value)
	{
		___state_2 = value;
		Il2CppCodeGenWriteBarrier((&___state_2), value);
	}

	inline static int32_t get_offset_of_trackEntryPool_3() { return static_cast<int32_t>(offsetof(EventQueue_t2605283308, ___trackEntryPool_3)); }
	inline Pool_1_t4075013739 * get_trackEntryPool_3() const { return ___trackEntryPool_3; }
	inline Pool_1_t4075013739 ** get_address_of_trackEntryPool_3() { return &___trackEntryPool_3; }
	inline void set_trackEntryPool_3(Pool_1_t4075013739 * value)
	{
		___trackEntryPool_3 = value;
		Il2CppCodeGenWriteBarrier((&___trackEntryPool_3), value);
	}

	inline static int32_t get_offset_of_AnimationsChanged_4() { return static_cast<int32_t>(offsetof(EventQueue_t2605283308, ___AnimationsChanged_4)); }
	inline Action_t3158581658 * get_AnimationsChanged_4() const { return ___AnimationsChanged_4; }
	inline Action_t3158581658 ** get_address_of_AnimationsChanged_4() { return &___AnimationsChanged_4; }
	inline void set_AnimationsChanged_4(Action_t3158581658 * value)
	{
		___AnimationsChanged_4 = value;
		Il2CppCodeGenWriteBarrier((&___AnimationsChanged_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTQUEUE_T2605283308_H
#ifndef VALUETYPE_T2697047229_H
#define VALUETYPE_T2697047229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2697047229  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2697047229_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2697047229_marshaled_com
{
};
#endif // VALUETYPE_T2697047229_H
#ifndef ATTACHMENT_T397756874_H
#define ATTACHMENT_T397756874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Attachment
struct  Attachment_t397756874  : public RuntimeObject
{
public:
	// System.String Spine.Attachment::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Attachment_t397756874, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENT_T397756874_H
#ifndef ATTACHMENTTIMELINE_T2136692098_H
#define ATTACHMENTTIMELINE_T2136692098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AttachmentTimeline
struct  AttachmentTimeline_t2136692098  : public RuntimeObject
{
public:
	// System.Int32 Spine.AttachmentTimeline::slotIndex
	int32_t ___slotIndex_0;
	// System.Single[] Spine.AttachmentTimeline::frames
	SingleU5BU5D_t3989243465* ___frames_1;
	// System.String[] Spine.AttachmentTimeline::attachmentNames
	StringU5BU5D_t656593794* ___attachmentNames_2;

public:
	inline static int32_t get_offset_of_slotIndex_0() { return static_cast<int32_t>(offsetof(AttachmentTimeline_t2136692098, ___slotIndex_0)); }
	inline int32_t get_slotIndex_0() const { return ___slotIndex_0; }
	inline int32_t* get_address_of_slotIndex_0() { return &___slotIndex_0; }
	inline void set_slotIndex_0(int32_t value)
	{
		___slotIndex_0 = value;
	}

	inline static int32_t get_offset_of_frames_1() { return static_cast<int32_t>(offsetof(AttachmentTimeline_t2136692098, ___frames_1)); }
	inline SingleU5BU5D_t3989243465* get_frames_1() const { return ___frames_1; }
	inline SingleU5BU5D_t3989243465** get_address_of_frames_1() { return &___frames_1; }
	inline void set_frames_1(SingleU5BU5D_t3989243465* value)
	{
		___frames_1 = value;
		Il2CppCodeGenWriteBarrier((&___frames_1), value);
	}

	inline static int32_t get_offset_of_attachmentNames_2() { return static_cast<int32_t>(offsetof(AttachmentTimeline_t2136692098, ___attachmentNames_2)); }
	inline StringU5BU5D_t656593794* get_attachmentNames_2() const { return ___attachmentNames_2; }
	inline StringU5BU5D_t656593794** get_address_of_attachmentNames_2() { return &___attachmentNames_2; }
	inline void set_attachmentNames_2(StringU5BU5D_t656593794* value)
	{
		___attachmentNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentNames_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTTIMELINE_T2136692098_H
#ifndef SLOTREGIONPAIR_T2265310044_H
#define SLOTREGIONPAIR_T2265310044_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AtlasRegionAttacher/SlotRegionPair
struct  SlotRegionPair_t2265310044  : public RuntimeObject
{
public:
	// System.String Spine.Unity.Modules.AtlasRegionAttacher/SlotRegionPair::slot
	String_t* ___slot_0;
	// System.String Spine.Unity.Modules.AtlasRegionAttacher/SlotRegionPair::region
	String_t* ___region_1;

public:
	inline static int32_t get_offset_of_slot_0() { return static_cast<int32_t>(offsetof(SlotRegionPair_t2265310044, ___slot_0)); }
	inline String_t* get_slot_0() const { return ___slot_0; }
	inline String_t** get_address_of_slot_0() { return &___slot_0; }
	inline void set_slot_0(String_t* value)
	{
		___slot_0 = value;
		Il2CppCodeGenWriteBarrier((&___slot_0), value);
	}

	inline static int32_t get_offset_of_region_1() { return static_cast<int32_t>(offsetof(SlotRegionPair_t2265310044, ___region_1)); }
	inline String_t* get_region_1() const { return ___region_1; }
	inline String_t** get_address_of_region_1() { return &___region_1; }
	inline void set_region_1(String_t* value)
	{
		___region_1 = value;
		Il2CppCodeGenWriteBarrier((&___region_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTREGIONPAIR_T2265310044_H
#ifndef ATLASREGION_T2476064513_H
#define ATLASREGION_T2476064513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AtlasRegion
struct  AtlasRegion_t2476064513  : public RuntimeObject
{
public:
	// Spine.AtlasPage Spine.AtlasRegion::page
	AtlasPage_t3729449121 * ___page_0;
	// System.String Spine.AtlasRegion::name
	String_t* ___name_1;
	// System.Int32 Spine.AtlasRegion::x
	int32_t ___x_2;
	// System.Int32 Spine.AtlasRegion::y
	int32_t ___y_3;
	// System.Int32 Spine.AtlasRegion::width
	int32_t ___width_4;
	// System.Int32 Spine.AtlasRegion::height
	int32_t ___height_5;
	// System.Single Spine.AtlasRegion::u
	float ___u_6;
	// System.Single Spine.AtlasRegion::v
	float ___v_7;
	// System.Single Spine.AtlasRegion::u2
	float ___u2_8;
	// System.Single Spine.AtlasRegion::v2
	float ___v2_9;
	// System.Single Spine.AtlasRegion::offsetX
	float ___offsetX_10;
	// System.Single Spine.AtlasRegion::offsetY
	float ___offsetY_11;
	// System.Int32 Spine.AtlasRegion::originalWidth
	int32_t ___originalWidth_12;
	// System.Int32 Spine.AtlasRegion::originalHeight
	int32_t ___originalHeight_13;
	// System.Int32 Spine.AtlasRegion::index
	int32_t ___index_14;
	// System.Boolean Spine.AtlasRegion::rotate
	bool ___rotate_15;
	// System.Int32[] Spine.AtlasRegion::splits
	Int32U5BU5D_t281224829* ___splits_16;
	// System.Int32[] Spine.AtlasRegion::pads
	Int32U5BU5D_t281224829* ___pads_17;

public:
	inline static int32_t get_offset_of_page_0() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___page_0)); }
	inline AtlasPage_t3729449121 * get_page_0() const { return ___page_0; }
	inline AtlasPage_t3729449121 ** get_address_of_page_0() { return &___page_0; }
	inline void set_page_0(AtlasPage_t3729449121 * value)
	{
		___page_0 = value;
		Il2CppCodeGenWriteBarrier((&___page_0), value);
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___x_2)); }
	inline int32_t get_x_2() const { return ___x_2; }
	inline int32_t* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(int32_t value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___y_3)); }
	inline int32_t get_y_3() const { return ___y_3; }
	inline int32_t* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(int32_t value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___width_4)); }
	inline int32_t get_width_4() const { return ___width_4; }
	inline int32_t* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(int32_t value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_height_5() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___height_5)); }
	inline int32_t get_height_5() const { return ___height_5; }
	inline int32_t* get_address_of_height_5() { return &___height_5; }
	inline void set_height_5(int32_t value)
	{
		___height_5 = value;
	}

	inline static int32_t get_offset_of_u_6() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___u_6)); }
	inline float get_u_6() const { return ___u_6; }
	inline float* get_address_of_u_6() { return &___u_6; }
	inline void set_u_6(float value)
	{
		___u_6 = value;
	}

	inline static int32_t get_offset_of_v_7() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___v_7)); }
	inline float get_v_7() const { return ___v_7; }
	inline float* get_address_of_v_7() { return &___v_7; }
	inline void set_v_7(float value)
	{
		___v_7 = value;
	}

	inline static int32_t get_offset_of_u2_8() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___u2_8)); }
	inline float get_u2_8() const { return ___u2_8; }
	inline float* get_address_of_u2_8() { return &___u2_8; }
	inline void set_u2_8(float value)
	{
		___u2_8 = value;
	}

	inline static int32_t get_offset_of_v2_9() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___v2_9)); }
	inline float get_v2_9() const { return ___v2_9; }
	inline float* get_address_of_v2_9() { return &___v2_9; }
	inline void set_v2_9(float value)
	{
		___v2_9 = value;
	}

	inline static int32_t get_offset_of_offsetX_10() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___offsetX_10)); }
	inline float get_offsetX_10() const { return ___offsetX_10; }
	inline float* get_address_of_offsetX_10() { return &___offsetX_10; }
	inline void set_offsetX_10(float value)
	{
		___offsetX_10 = value;
	}

	inline static int32_t get_offset_of_offsetY_11() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___offsetY_11)); }
	inline float get_offsetY_11() const { return ___offsetY_11; }
	inline float* get_address_of_offsetY_11() { return &___offsetY_11; }
	inline void set_offsetY_11(float value)
	{
		___offsetY_11 = value;
	}

	inline static int32_t get_offset_of_originalWidth_12() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___originalWidth_12)); }
	inline int32_t get_originalWidth_12() const { return ___originalWidth_12; }
	inline int32_t* get_address_of_originalWidth_12() { return &___originalWidth_12; }
	inline void set_originalWidth_12(int32_t value)
	{
		___originalWidth_12 = value;
	}

	inline static int32_t get_offset_of_originalHeight_13() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___originalHeight_13)); }
	inline int32_t get_originalHeight_13() const { return ___originalHeight_13; }
	inline int32_t* get_address_of_originalHeight_13() { return &___originalHeight_13; }
	inline void set_originalHeight_13(int32_t value)
	{
		___originalHeight_13 = value;
	}

	inline static int32_t get_offset_of_index_14() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___index_14)); }
	inline int32_t get_index_14() const { return ___index_14; }
	inline int32_t* get_address_of_index_14() { return &___index_14; }
	inline void set_index_14(int32_t value)
	{
		___index_14 = value;
	}

	inline static int32_t get_offset_of_rotate_15() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___rotate_15)); }
	inline bool get_rotate_15() const { return ___rotate_15; }
	inline bool* get_address_of_rotate_15() { return &___rotate_15; }
	inline void set_rotate_15(bool value)
	{
		___rotate_15 = value;
	}

	inline static int32_t get_offset_of_splits_16() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___splits_16)); }
	inline Int32U5BU5D_t281224829* get_splits_16() const { return ___splits_16; }
	inline Int32U5BU5D_t281224829** get_address_of_splits_16() { return &___splits_16; }
	inline void set_splits_16(Int32U5BU5D_t281224829* value)
	{
		___splits_16 = value;
		Il2CppCodeGenWriteBarrier((&___splits_16), value);
	}

	inline static int32_t get_offset_of_pads_17() { return static_cast<int32_t>(offsetof(AtlasRegion_t2476064513, ___pads_17)); }
	inline Int32U5BU5D_t281224829* get_pads_17() const { return ___pads_17; }
	inline Int32U5BU5D_t281224829** get_address_of_pads_17() { return &___pads_17; }
	inline void set_pads_17(Int32U5BU5D_t281224829* value)
	{
		___pads_17 = value;
		Il2CppCodeGenWriteBarrier((&___pads_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASREGION_T2476064513_H
#ifndef SPRITEATTACHMENTEXTENSIONS_T2253895941_H
#define SPRITEATTACHMENTEXTENSIONS_T2253895941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SpriteAttachmentExtensions
struct  SpriteAttachmentExtensions_t2253895941  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEATTACHMENTEXTENSIONS_T2253895941_H
#ifndef ATLAS_T604244430_H
#define ATLAS_T604244430_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Atlas
struct  Atlas_t604244430  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Spine.AtlasPage> Spine.Atlas::pages
	List_1_t1557897550 * ___pages_0;
	// System.Collections.Generic.List`1<Spine.AtlasRegion> Spine.Atlas::regions
	List_1_t304512942 * ___regions_1;
	// Spine.TextureLoader Spine.Atlas::textureLoader
	RuntimeObject* ___textureLoader_2;

public:
	inline static int32_t get_offset_of_pages_0() { return static_cast<int32_t>(offsetof(Atlas_t604244430, ___pages_0)); }
	inline List_1_t1557897550 * get_pages_0() const { return ___pages_0; }
	inline List_1_t1557897550 ** get_address_of_pages_0() { return &___pages_0; }
	inline void set_pages_0(List_1_t1557897550 * value)
	{
		___pages_0 = value;
		Il2CppCodeGenWriteBarrier((&___pages_0), value);
	}

	inline static int32_t get_offset_of_regions_1() { return static_cast<int32_t>(offsetof(Atlas_t604244430, ___regions_1)); }
	inline List_1_t304512942 * get_regions_1() const { return ___regions_1; }
	inline List_1_t304512942 ** get_address_of_regions_1() { return &___regions_1; }
	inline void set_regions_1(List_1_t304512942 * value)
	{
		___regions_1 = value;
		Il2CppCodeGenWriteBarrier((&___regions_1), value);
	}

	inline static int32_t get_offset_of_textureLoader_2() { return static_cast<int32_t>(offsetof(Atlas_t604244430, ___textureLoader_2)); }
	inline RuntimeObject* get_textureLoader_2() const { return ___textureLoader_2; }
	inline RuntimeObject** get_address_of_textureLoader_2() { return &___textureLoader_2; }
	inline void set_textureLoader_2(RuntimeObject* value)
	{
		___textureLoader_2 = value;
		Il2CppCodeGenWriteBarrier((&___textureLoader_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLAS_T604244430_H
#ifndef ANIMATIONPAIRCOMPARER_T3810224342_H
#define ANIMATIONPAIRCOMPARER_T3810224342_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationStateData/AnimationPairComparer
struct  AnimationPairComparer_t3810224342  : public RuntimeObject
{
public:

public:
};

struct AnimationPairComparer_t3810224342_StaticFields
{
public:
	// Spine.AnimationStateData/AnimationPairComparer Spine.AnimationStateData/AnimationPairComparer::Instance
	AnimationPairComparer_t3810224342 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(AnimationPairComparer_t3810224342_StaticFields, ___Instance_0)); }
	inline AnimationPairComparer_t3810224342 * get_Instance_0() const { return ___Instance_0; }
	inline AnimationPairComparer_t3810224342 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(AnimationPairComparer_t3810224342 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONPAIRCOMPARER_T3810224342_H
#ifndef ATLASATTACHMENTLOADER_T745839734_H
#define ATLASATTACHMENTLOADER_T745839734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AtlasAttachmentLoader
struct  AtlasAttachmentLoader_t745839734  : public RuntimeObject
{
public:
	// Spine.Atlas[] Spine.AtlasAttachmentLoader::atlasArray
	AtlasU5BU5D_t2027208507* ___atlasArray_0;

public:
	inline static int32_t get_offset_of_atlasArray_0() { return static_cast<int32_t>(offsetof(AtlasAttachmentLoader_t745839734, ___atlasArray_0)); }
	inline AtlasU5BU5D_t2027208507* get_atlasArray_0() const { return ___atlasArray_0; }
	inline AtlasU5BU5D_t2027208507** get_address_of_atlasArray_0() { return &___atlasArray_0; }
	inline void set_atlasArray_0(AtlasU5BU5D_t2027208507* value)
	{
		___atlasArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASATTACHMENTLOADER_T745839734_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef REGIONATTACHMENT_T1431410254_H
#define REGIONATTACHMENT_T1431410254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.RegionAttachment
struct  RegionAttachment_t1431410254  : public Attachment_t397756874
{
public:
	// System.Single Spine.RegionAttachment::x
	float ___x_9;
	// System.Single Spine.RegionAttachment::y
	float ___y_10;
	// System.Single Spine.RegionAttachment::rotation
	float ___rotation_11;
	// System.Single Spine.RegionAttachment::scaleX
	float ___scaleX_12;
	// System.Single Spine.RegionAttachment::scaleY
	float ___scaleY_13;
	// System.Single Spine.RegionAttachment::width
	float ___width_14;
	// System.Single Spine.RegionAttachment::height
	float ___height_15;
	// System.Single Spine.RegionAttachment::regionOffsetX
	float ___regionOffsetX_16;
	// System.Single Spine.RegionAttachment::regionOffsetY
	float ___regionOffsetY_17;
	// System.Single Spine.RegionAttachment::regionWidth
	float ___regionWidth_18;
	// System.Single Spine.RegionAttachment::regionHeight
	float ___regionHeight_19;
	// System.Single Spine.RegionAttachment::regionOriginalWidth
	float ___regionOriginalWidth_20;
	// System.Single Spine.RegionAttachment::regionOriginalHeight
	float ___regionOriginalHeight_21;
	// System.Single[] Spine.RegionAttachment::offset
	SingleU5BU5D_t3989243465* ___offset_22;
	// System.Single[] Spine.RegionAttachment::uvs
	SingleU5BU5D_t3989243465* ___uvs_23;
	// System.Single Spine.RegionAttachment::r
	float ___r_24;
	// System.Single Spine.RegionAttachment::g
	float ___g_25;
	// System.Single Spine.RegionAttachment::b
	float ___b_26;
	// System.Single Spine.RegionAttachment::a
	float ___a_27;
	// System.String Spine.RegionAttachment::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_28;
	// System.Object Spine.RegionAttachment::<RendererObject>k__BackingField
	RuntimeObject * ___U3CRendererObjectU3Ek__BackingField_29;

public:
	inline static int32_t get_offset_of_x_9() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___x_9)); }
	inline float get_x_9() const { return ___x_9; }
	inline float* get_address_of_x_9() { return &___x_9; }
	inline void set_x_9(float value)
	{
		___x_9 = value;
	}

	inline static int32_t get_offset_of_y_10() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___y_10)); }
	inline float get_y_10() const { return ___y_10; }
	inline float* get_address_of_y_10() { return &___y_10; }
	inline void set_y_10(float value)
	{
		___y_10 = value;
	}

	inline static int32_t get_offset_of_rotation_11() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___rotation_11)); }
	inline float get_rotation_11() const { return ___rotation_11; }
	inline float* get_address_of_rotation_11() { return &___rotation_11; }
	inline void set_rotation_11(float value)
	{
		___rotation_11 = value;
	}

	inline static int32_t get_offset_of_scaleX_12() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___scaleX_12)); }
	inline float get_scaleX_12() const { return ___scaleX_12; }
	inline float* get_address_of_scaleX_12() { return &___scaleX_12; }
	inline void set_scaleX_12(float value)
	{
		___scaleX_12 = value;
	}

	inline static int32_t get_offset_of_scaleY_13() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___scaleY_13)); }
	inline float get_scaleY_13() const { return ___scaleY_13; }
	inline float* get_address_of_scaleY_13() { return &___scaleY_13; }
	inline void set_scaleY_13(float value)
	{
		___scaleY_13 = value;
	}

	inline static int32_t get_offset_of_width_14() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___width_14)); }
	inline float get_width_14() const { return ___width_14; }
	inline float* get_address_of_width_14() { return &___width_14; }
	inline void set_width_14(float value)
	{
		___width_14 = value;
	}

	inline static int32_t get_offset_of_height_15() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___height_15)); }
	inline float get_height_15() const { return ___height_15; }
	inline float* get_address_of_height_15() { return &___height_15; }
	inline void set_height_15(float value)
	{
		___height_15 = value;
	}

	inline static int32_t get_offset_of_regionOffsetX_16() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___regionOffsetX_16)); }
	inline float get_regionOffsetX_16() const { return ___regionOffsetX_16; }
	inline float* get_address_of_regionOffsetX_16() { return &___regionOffsetX_16; }
	inline void set_regionOffsetX_16(float value)
	{
		___regionOffsetX_16 = value;
	}

	inline static int32_t get_offset_of_regionOffsetY_17() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___regionOffsetY_17)); }
	inline float get_regionOffsetY_17() const { return ___regionOffsetY_17; }
	inline float* get_address_of_regionOffsetY_17() { return &___regionOffsetY_17; }
	inline void set_regionOffsetY_17(float value)
	{
		___regionOffsetY_17 = value;
	}

	inline static int32_t get_offset_of_regionWidth_18() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___regionWidth_18)); }
	inline float get_regionWidth_18() const { return ___regionWidth_18; }
	inline float* get_address_of_regionWidth_18() { return &___regionWidth_18; }
	inline void set_regionWidth_18(float value)
	{
		___regionWidth_18 = value;
	}

	inline static int32_t get_offset_of_regionHeight_19() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___regionHeight_19)); }
	inline float get_regionHeight_19() const { return ___regionHeight_19; }
	inline float* get_address_of_regionHeight_19() { return &___regionHeight_19; }
	inline void set_regionHeight_19(float value)
	{
		___regionHeight_19 = value;
	}

	inline static int32_t get_offset_of_regionOriginalWidth_20() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___regionOriginalWidth_20)); }
	inline float get_regionOriginalWidth_20() const { return ___regionOriginalWidth_20; }
	inline float* get_address_of_regionOriginalWidth_20() { return &___regionOriginalWidth_20; }
	inline void set_regionOriginalWidth_20(float value)
	{
		___regionOriginalWidth_20 = value;
	}

	inline static int32_t get_offset_of_regionOriginalHeight_21() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___regionOriginalHeight_21)); }
	inline float get_regionOriginalHeight_21() const { return ___regionOriginalHeight_21; }
	inline float* get_address_of_regionOriginalHeight_21() { return &___regionOriginalHeight_21; }
	inline void set_regionOriginalHeight_21(float value)
	{
		___regionOriginalHeight_21 = value;
	}

	inline static int32_t get_offset_of_offset_22() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___offset_22)); }
	inline SingleU5BU5D_t3989243465* get_offset_22() const { return ___offset_22; }
	inline SingleU5BU5D_t3989243465** get_address_of_offset_22() { return &___offset_22; }
	inline void set_offset_22(SingleU5BU5D_t3989243465* value)
	{
		___offset_22 = value;
		Il2CppCodeGenWriteBarrier((&___offset_22), value);
	}

	inline static int32_t get_offset_of_uvs_23() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___uvs_23)); }
	inline SingleU5BU5D_t3989243465* get_uvs_23() const { return ___uvs_23; }
	inline SingleU5BU5D_t3989243465** get_address_of_uvs_23() { return &___uvs_23; }
	inline void set_uvs_23(SingleU5BU5D_t3989243465* value)
	{
		___uvs_23 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_23), value);
	}

	inline static int32_t get_offset_of_r_24() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___r_24)); }
	inline float get_r_24() const { return ___r_24; }
	inline float* get_address_of_r_24() { return &___r_24; }
	inline void set_r_24(float value)
	{
		___r_24 = value;
	}

	inline static int32_t get_offset_of_g_25() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___g_25)); }
	inline float get_g_25() const { return ___g_25; }
	inline float* get_address_of_g_25() { return &___g_25; }
	inline void set_g_25(float value)
	{
		___g_25 = value;
	}

	inline static int32_t get_offset_of_b_26() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___b_26)); }
	inline float get_b_26() const { return ___b_26; }
	inline float* get_address_of_b_26() { return &___b_26; }
	inline void set_b_26(float value)
	{
		___b_26 = value;
	}

	inline static int32_t get_offset_of_a_27() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___a_27)); }
	inline float get_a_27() const { return ___a_27; }
	inline float* get_address_of_a_27() { return &___a_27; }
	inline void set_a_27(float value)
	{
		___a_27 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___U3CPathU3Ek__BackingField_28)); }
	inline String_t* get_U3CPathU3Ek__BackingField_28() const { return ___U3CPathU3Ek__BackingField_28; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_28() { return &___U3CPathU3Ek__BackingField_28; }
	inline void set_U3CPathU3Ek__BackingField_28(String_t* value)
	{
		___U3CPathU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CRendererObjectU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(RegionAttachment_t1431410254, ___U3CRendererObjectU3Ek__BackingField_29)); }
	inline RuntimeObject * get_U3CRendererObjectU3Ek__BackingField_29() const { return ___U3CRendererObjectU3Ek__BackingField_29; }
	inline RuntimeObject ** get_address_of_U3CRendererObjectU3Ek__BackingField_29() { return &___U3CRendererObjectU3Ek__BackingField_29; }
	inline void set_U3CRendererObjectU3Ek__BackingField_29(RuntimeObject * value)
	{
		___U3CRendererObjectU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRendererObjectU3Ek__BackingField_29), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGIONATTACHMENT_T1431410254_H
#ifndef PATHCONSTRAINTMIXTIMELINE_T3043566903_H
#define PATHCONSTRAINTMIXTIMELINE_T3043566903_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintMixTimeline
struct  PathConstraintMixTimeline_t3043566903  : public CurveTimeline_t917256691
{
public:
	// System.Int32 Spine.PathConstraintMixTimeline::pathConstraintIndex
	int32_t ___pathConstraintIndex_11;
	// System.Single[] Spine.PathConstraintMixTimeline::frames
	SingleU5BU5D_t3989243465* ___frames_12;

public:
	inline static int32_t get_offset_of_pathConstraintIndex_11() { return static_cast<int32_t>(offsetof(PathConstraintMixTimeline_t3043566903, ___pathConstraintIndex_11)); }
	inline int32_t get_pathConstraintIndex_11() const { return ___pathConstraintIndex_11; }
	inline int32_t* get_address_of_pathConstraintIndex_11() { return &___pathConstraintIndex_11; }
	inline void set_pathConstraintIndex_11(int32_t value)
	{
		___pathConstraintIndex_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(PathConstraintMixTimeline_t3043566903, ___frames_12)); }
	inline SingleU5BU5D_t3989243465* get_frames_12() const { return ___frames_12; }
	inline SingleU5BU5D_t3989243465** get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(SingleU5BU5D_t3989243465* value)
	{
		___frames_12 = value;
		Il2CppCodeGenWriteBarrier((&___frames_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTMIXTIMELINE_T3043566903_H
#ifndef PATHCONSTRAINTPOSITIONTIMELINE_T2710737122_H
#define PATHCONSTRAINTPOSITIONTIMELINE_T2710737122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintPositionTimeline
struct  PathConstraintPositionTimeline_t2710737122  : public CurveTimeline_t917256691
{
public:
	// System.Int32 Spine.PathConstraintPositionTimeline::pathConstraintIndex
	int32_t ___pathConstraintIndex_9;
	// System.Single[] Spine.PathConstraintPositionTimeline::frames
	SingleU5BU5D_t3989243465* ___frames_10;

public:
	inline static int32_t get_offset_of_pathConstraintIndex_9() { return static_cast<int32_t>(offsetof(PathConstraintPositionTimeline_t2710737122, ___pathConstraintIndex_9)); }
	inline int32_t get_pathConstraintIndex_9() const { return ___pathConstraintIndex_9; }
	inline int32_t* get_address_of_pathConstraintIndex_9() { return &___pathConstraintIndex_9; }
	inline void set_pathConstraintIndex_9(int32_t value)
	{
		___pathConstraintIndex_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(PathConstraintPositionTimeline_t2710737122, ___frames_10)); }
	inline SingleU5BU5D_t3989243465* get_frames_10() const { return ___frames_10; }
	inline SingleU5BU5D_t3989243465** get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(SingleU5BU5D_t3989243465* value)
	{
		___frames_10 = value;
		Il2CppCodeGenWriteBarrier((&___frames_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTPOSITIONTIMELINE_T2710737122_H
#ifndef VERTEXATTACHMENT_T1776545046_H
#define VERTEXATTACHMENT_T1776545046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.VertexAttachment
struct  VertexAttachment_t1776545046  : public Attachment_t397756874
{
public:
	// System.Int32 Spine.VertexAttachment::id
	int32_t ___id_3;
	// System.Int32[] Spine.VertexAttachment::bones
	Int32U5BU5D_t281224829* ___bones_4;
	// System.Single[] Spine.VertexAttachment::vertices
	SingleU5BU5D_t3989243465* ___vertices_5;
	// System.Int32 Spine.VertexAttachment::worldVerticesLength
	int32_t ___worldVerticesLength_6;

public:
	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(VertexAttachment_t1776545046, ___id_3)); }
	inline int32_t get_id_3() const { return ___id_3; }
	inline int32_t* get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(int32_t value)
	{
		___id_3 = value;
	}

	inline static int32_t get_offset_of_bones_4() { return static_cast<int32_t>(offsetof(VertexAttachment_t1776545046, ___bones_4)); }
	inline Int32U5BU5D_t281224829* get_bones_4() const { return ___bones_4; }
	inline Int32U5BU5D_t281224829** get_address_of_bones_4() { return &___bones_4; }
	inline void set_bones_4(Int32U5BU5D_t281224829* value)
	{
		___bones_4 = value;
		Il2CppCodeGenWriteBarrier((&___bones_4), value);
	}

	inline static int32_t get_offset_of_vertices_5() { return static_cast<int32_t>(offsetof(VertexAttachment_t1776545046, ___vertices_5)); }
	inline SingleU5BU5D_t3989243465* get_vertices_5() const { return ___vertices_5; }
	inline SingleU5BU5D_t3989243465** get_address_of_vertices_5() { return &___vertices_5; }
	inline void set_vertices_5(SingleU5BU5D_t3989243465* value)
	{
		___vertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_5), value);
	}

	inline static int32_t get_offset_of_worldVerticesLength_6() { return static_cast<int32_t>(offsetof(VertexAttachment_t1776545046, ___worldVerticesLength_6)); }
	inline int32_t get_worldVerticesLength_6() const { return ___worldVerticesLength_6; }
	inline int32_t* get_address_of_worldVerticesLength_6() { return &___worldVerticesLength_6; }
	inline void set_worldVerticesLength_6(int32_t value)
	{
		___worldVerticesLength_6 = value;
	}
};

struct VertexAttachment_t1776545046_StaticFields
{
public:
	// System.Int32 Spine.VertexAttachment::nextID
	int32_t ___nextID_1;
	// System.Object Spine.VertexAttachment::nextIdLock
	RuntimeObject * ___nextIdLock_2;

public:
	inline static int32_t get_offset_of_nextID_1() { return static_cast<int32_t>(offsetof(VertexAttachment_t1776545046_StaticFields, ___nextID_1)); }
	inline int32_t get_nextID_1() const { return ___nextID_1; }
	inline int32_t* get_address_of_nextID_1() { return &___nextID_1; }
	inline void set_nextID_1(int32_t value)
	{
		___nextID_1 = value;
	}

	inline static int32_t get_offset_of_nextIdLock_2() { return static_cast<int32_t>(offsetof(VertexAttachment_t1776545046_StaticFields, ___nextIdLock_2)); }
	inline RuntimeObject * get_nextIdLock_2() const { return ___nextIdLock_2; }
	inline RuntimeObject ** get_address_of_nextIdLock_2() { return &___nextIdLock_2; }
	inline void set_nextIdLock_2(RuntimeObject * value)
	{
		___nextIdLock_2 = value;
		Il2CppCodeGenWriteBarrier((&___nextIdLock_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXATTACHMENT_T1776545046_H
#ifndef TRANSFORMCONSTRAINTTIMELINE_T1868878952_H
#define TRANSFORMCONSTRAINTTIMELINE_T1868878952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformConstraintTimeline
struct  TransformConstraintTimeline_t1868878952  : public CurveTimeline_t917256691
{
public:
	// System.Int32 Spine.TransformConstraintTimeline::transformConstraintIndex
	int32_t ___transformConstraintIndex_15;
	// System.Single[] Spine.TransformConstraintTimeline::frames
	SingleU5BU5D_t3989243465* ___frames_16;

public:
	inline static int32_t get_offset_of_transformConstraintIndex_15() { return static_cast<int32_t>(offsetof(TransformConstraintTimeline_t1868878952, ___transformConstraintIndex_15)); }
	inline int32_t get_transformConstraintIndex_15() const { return ___transformConstraintIndex_15; }
	inline int32_t* get_address_of_transformConstraintIndex_15() { return &___transformConstraintIndex_15; }
	inline void set_transformConstraintIndex_15(int32_t value)
	{
		___transformConstraintIndex_15 = value;
	}

	inline static int32_t get_offset_of_frames_16() { return static_cast<int32_t>(offsetof(TransformConstraintTimeline_t1868878952, ___frames_16)); }
	inline SingleU5BU5D_t3989243465* get_frames_16() const { return ___frames_16; }
	inline SingleU5BU5D_t3989243465** get_address_of_frames_16() { return &___frames_16; }
	inline void set_frames_16(SingleU5BU5D_t3989243465* value)
	{
		___frames_16 = value;
		Il2CppCodeGenWriteBarrier((&___frames_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONSTRAINTTIMELINE_T1868878952_H
#ifndef IKCONSTRAINTTIMELINE_T2394284159_H
#define IKCONSTRAINTTIMELINE_T2394284159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.IkConstraintTimeline
struct  IkConstraintTimeline_t2394284159  : public CurveTimeline_t917256691
{
public:
	// System.Int32 Spine.IkConstraintTimeline::ikConstraintIndex
	int32_t ___ikConstraintIndex_11;
	// System.Single[] Spine.IkConstraintTimeline::frames
	SingleU5BU5D_t3989243465* ___frames_12;

public:
	inline static int32_t get_offset_of_ikConstraintIndex_11() { return static_cast<int32_t>(offsetof(IkConstraintTimeline_t2394284159, ___ikConstraintIndex_11)); }
	inline int32_t get_ikConstraintIndex_11() const { return ___ikConstraintIndex_11; }
	inline int32_t* get_address_of_ikConstraintIndex_11() { return &___ikConstraintIndex_11; }
	inline void set_ikConstraintIndex_11(int32_t value)
	{
		___ikConstraintIndex_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(IkConstraintTimeline_t2394284159, ___frames_12)); }
	inline SingleU5BU5D_t3989243465* get_frames_12() const { return ___frames_12; }
	inline SingleU5BU5D_t3989243465** get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(SingleU5BU5D_t3989243465* value)
	{
		___frames_12 = value;
		Il2CppCodeGenWriteBarrier((&___frames_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IKCONSTRAINTTIMELINE_T2394284159_H
#ifndef DEFORMTIMELINE_T3829885991_H
#define DEFORMTIMELINE_T3829885991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.DeformTimeline
struct  DeformTimeline_t3829885991  : public CurveTimeline_t917256691
{
public:
	// System.Int32 Spine.DeformTimeline::slotIndex
	int32_t ___slotIndex_5;
	// System.Single[] Spine.DeformTimeline::frames
	SingleU5BU5D_t3989243465* ___frames_6;
	// System.Single[][] Spine.DeformTimeline::frameVertices
	SingleU5BU5DU5BU5D_t2055639668* ___frameVertices_7;
	// Spine.VertexAttachment Spine.DeformTimeline::attachment
	VertexAttachment_t1776545046 * ___attachment_8;

public:
	inline static int32_t get_offset_of_slotIndex_5() { return static_cast<int32_t>(offsetof(DeformTimeline_t3829885991, ___slotIndex_5)); }
	inline int32_t get_slotIndex_5() const { return ___slotIndex_5; }
	inline int32_t* get_address_of_slotIndex_5() { return &___slotIndex_5; }
	inline void set_slotIndex_5(int32_t value)
	{
		___slotIndex_5 = value;
	}

	inline static int32_t get_offset_of_frames_6() { return static_cast<int32_t>(offsetof(DeformTimeline_t3829885991, ___frames_6)); }
	inline SingleU5BU5D_t3989243465* get_frames_6() const { return ___frames_6; }
	inline SingleU5BU5D_t3989243465** get_address_of_frames_6() { return &___frames_6; }
	inline void set_frames_6(SingleU5BU5D_t3989243465* value)
	{
		___frames_6 = value;
		Il2CppCodeGenWriteBarrier((&___frames_6), value);
	}

	inline static int32_t get_offset_of_frameVertices_7() { return static_cast<int32_t>(offsetof(DeformTimeline_t3829885991, ___frameVertices_7)); }
	inline SingleU5BU5DU5BU5D_t2055639668* get_frameVertices_7() const { return ___frameVertices_7; }
	inline SingleU5BU5DU5BU5D_t2055639668** get_address_of_frameVertices_7() { return &___frameVertices_7; }
	inline void set_frameVertices_7(SingleU5BU5DU5BU5D_t2055639668* value)
	{
		___frameVertices_7 = value;
		Il2CppCodeGenWriteBarrier((&___frameVertices_7), value);
	}

	inline static int32_t get_offset_of_attachment_8() { return static_cast<int32_t>(offsetof(DeformTimeline_t3829885991, ___attachment_8)); }
	inline VertexAttachment_t1776545046 * get_attachment_8() const { return ___attachment_8; }
	inline VertexAttachment_t1776545046 ** get_address_of_attachment_8() { return &___attachment_8; }
	inline void set_attachment_8(VertexAttachment_t1776545046 * value)
	{
		___attachment_8 = value;
		Il2CppCodeGenWriteBarrier((&___attachment_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFORMTIMELINE_T3829885991_H
#ifndef ANIMATIONPAIR_T2435898854_H
#define ANIMATIONPAIR_T2435898854_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationStateData/AnimationPair
struct  AnimationPair_t2435898854 
{
public:
	// Spine.Animation Spine.AnimationStateData/AnimationPair::a1
	Animation_t2067143511 * ___a1_0;
	// Spine.Animation Spine.AnimationStateData/AnimationPair::a2
	Animation_t2067143511 * ___a2_1;

public:
	inline static int32_t get_offset_of_a1_0() { return static_cast<int32_t>(offsetof(AnimationPair_t2435898854, ___a1_0)); }
	inline Animation_t2067143511 * get_a1_0() const { return ___a1_0; }
	inline Animation_t2067143511 ** get_address_of_a1_0() { return &___a1_0; }
	inline void set_a1_0(Animation_t2067143511 * value)
	{
		___a1_0 = value;
		Il2CppCodeGenWriteBarrier((&___a1_0), value);
	}

	inline static int32_t get_offset_of_a2_1() { return static_cast<int32_t>(offsetof(AnimationPair_t2435898854, ___a2_1)); }
	inline Animation_t2067143511 * get_a2_1() const { return ___a2_1; }
	inline Animation_t2067143511 ** get_address_of_a2_1() { return &___a2_1; }
	inline void set_a2_1(Animation_t2067143511 * value)
	{
		___a2_1 = value;
		Il2CppCodeGenWriteBarrier((&___a2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.AnimationStateData/AnimationPair
struct AnimationPair_t2435898854_marshaled_pinvoke
{
	Animation_t2067143511 * ___a1_0;
	Animation_t2067143511 * ___a2_1;
};
// Native definition for COM marshalling of Spine.AnimationStateData/AnimationPair
struct AnimationPair_t2435898854_marshaled_com
{
	Animation_t2067143511 * ___a1_0;
	Animation_t2067143511 * ___a2_1;
};
#endif // ANIMATIONPAIR_T2435898854_H
#ifndef POINTATTACHMENT_T2896333818_H
#define POINTATTACHMENT_T2896333818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PointAttachment
struct  PointAttachment_t2896333818  : public Attachment_t397756874
{
public:
	// System.Single Spine.PointAttachment::x
	float ___x_1;
	// System.Single Spine.PointAttachment::y
	float ___y_2;
	// System.Single Spine.PointAttachment::rotation
	float ___rotation_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(PointAttachment_t2896333818, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(PointAttachment_t2896333818, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_rotation_3() { return static_cast<int32_t>(offsetof(PointAttachment_t2896333818, ___rotation_3)); }
	inline float get_rotation_3() const { return ___rotation_3; }
	inline float* get_address_of_rotation_3() { return &___rotation_3; }
	inline void set_rotation_3(float value)
	{
		___rotation_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTATTACHMENT_T2896333818_H
#ifndef COLORTIMELINE_T137362015_H
#define COLORTIMELINE_T137362015_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ColorTimeline
struct  ColorTimeline_t137362015  : public CurveTimeline_t917256691
{
public:
	// System.Int32 Spine.ColorTimeline::slotIndex
	int32_t ___slotIndex_15;
	// System.Single[] Spine.ColorTimeline::frames
	SingleU5BU5D_t3989243465* ___frames_16;

public:
	inline static int32_t get_offset_of_slotIndex_15() { return static_cast<int32_t>(offsetof(ColorTimeline_t137362015, ___slotIndex_15)); }
	inline int32_t get_slotIndex_15() const { return ___slotIndex_15; }
	inline int32_t* get_address_of_slotIndex_15() { return &___slotIndex_15; }
	inline void set_slotIndex_15(int32_t value)
	{
		___slotIndex_15 = value;
	}

	inline static int32_t get_offset_of_frames_16() { return static_cast<int32_t>(offsetof(ColorTimeline_t137362015, ___frames_16)); }
	inline SingleU5BU5D_t3989243465* get_frames_16() const { return ___frames_16; }
	inline SingleU5BU5D_t3989243465** get_address_of_frames_16() { return &___frames_16; }
	inline void set_frames_16(SingleU5BU5D_t3989243465* value)
	{
		___frames_16 = value;
		Il2CppCodeGenWriteBarrier((&___frames_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORTIMELINE_T137362015_H
#ifndef LAYERMASK_T3306677380_H
#define LAYERMASK_T3306677380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3306677380 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3306677380, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3306677380_H
#ifndef COLOR_T4183816058_H
#define COLOR_T4183816058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t4183816058 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t4183816058, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t4183816058, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t4183816058, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t4183816058, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T4183816058_H
#ifndef SETTINGS_T1339330787_H
#define SETTINGS_T1339330787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGenerator/Settings
struct  Settings_t1339330787 
{
public:
	// System.Boolean Spine.Unity.MeshGenerator/Settings::useClipping
	bool ___useClipping_0;
	// System.Single Spine.Unity.MeshGenerator/Settings::zSpacing
	float ___zSpacing_1;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::pmaVertexColors
	bool ___pmaVertexColors_2;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::tintBlack
	bool ___tintBlack_3;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::calculateTangents
	bool ___calculateTangents_4;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::addNormals
	bool ___addNormals_5;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::immutableTriangles
	bool ___immutableTriangles_6;

public:
	inline static int32_t get_offset_of_useClipping_0() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___useClipping_0)); }
	inline bool get_useClipping_0() const { return ___useClipping_0; }
	inline bool* get_address_of_useClipping_0() { return &___useClipping_0; }
	inline void set_useClipping_0(bool value)
	{
		___useClipping_0 = value;
	}

	inline static int32_t get_offset_of_zSpacing_1() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___zSpacing_1)); }
	inline float get_zSpacing_1() const { return ___zSpacing_1; }
	inline float* get_address_of_zSpacing_1() { return &___zSpacing_1; }
	inline void set_zSpacing_1(float value)
	{
		___zSpacing_1 = value;
	}

	inline static int32_t get_offset_of_pmaVertexColors_2() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___pmaVertexColors_2)); }
	inline bool get_pmaVertexColors_2() const { return ___pmaVertexColors_2; }
	inline bool* get_address_of_pmaVertexColors_2() { return &___pmaVertexColors_2; }
	inline void set_pmaVertexColors_2(bool value)
	{
		___pmaVertexColors_2 = value;
	}

	inline static int32_t get_offset_of_tintBlack_3() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___tintBlack_3)); }
	inline bool get_tintBlack_3() const { return ___tintBlack_3; }
	inline bool* get_address_of_tintBlack_3() { return &___tintBlack_3; }
	inline void set_tintBlack_3(bool value)
	{
		___tintBlack_3 = value;
	}

	inline static int32_t get_offset_of_calculateTangents_4() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___calculateTangents_4)); }
	inline bool get_calculateTangents_4() const { return ___calculateTangents_4; }
	inline bool* get_address_of_calculateTangents_4() { return &___calculateTangents_4; }
	inline void set_calculateTangents_4(bool value)
	{
		___calculateTangents_4 = value;
	}

	inline static int32_t get_offset_of_addNormals_5() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___addNormals_5)); }
	inline bool get_addNormals_5() const { return ___addNormals_5; }
	inline bool* get_address_of_addNormals_5() { return &___addNormals_5; }
	inline void set_addNormals_5(bool value)
	{
		___addNormals_5 = value;
	}

	inline static int32_t get_offset_of_immutableTriangles_6() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___immutableTriangles_6)); }
	inline bool get_immutableTriangles_6() const { return ___immutableTriangles_6; }
	inline bool* get_address_of_immutableTriangles_6() { return &___immutableTriangles_6; }
	inline void set_immutableTriangles_6(bool value)
	{
		___immutableTriangles_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.MeshGenerator/Settings
struct Settings_t1339330787_marshaled_pinvoke
{
	int32_t ___useClipping_0;
	float ___zSpacing_1;
	int32_t ___pmaVertexColors_2;
	int32_t ___tintBlack_3;
	int32_t ___calculateTangents_4;
	int32_t ___addNormals_5;
	int32_t ___immutableTriangles_6;
};
// Native definition for COM marshalling of Spine.Unity.MeshGenerator/Settings
struct Settings_t1339330787_marshaled_com
{
	int32_t ___useClipping_0;
	float ___zSpacing_1;
	int32_t ___pmaVertexColors_2;
	int32_t ___tintBlack_3;
	int32_t ___calculateTangents_4;
	int32_t ___addNormals_5;
	int32_t ___immutableTriangles_6;
};
#endif // SETTINGS_T1339330787_H
#ifndef ENUM_T1912704450_H
#define ENUM_T1912704450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t1912704450  : public ValueType_t2697047229
{
public:

public:
};

struct Enum_t1912704450_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3669675803* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t1912704450_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3669675803* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3669675803** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3669675803* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t1912704450_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t1912704450_marshaled_com
{
};
#endif // ENUM_T1912704450_H
#ifndef VOID_T3157035008_H
#define VOID_T3157035008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t3157035008 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T3157035008_H
#ifndef VECTOR3_T2825674791_H
#define VECTOR3_T2825674791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2825674791 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2825674791, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2825674791, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2825674791, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2825674791_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2825674791  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2825674791  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2825674791  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2825674791  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2825674791  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2825674791  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2825674791  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2825674791  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2825674791  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2825674791  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2825674791  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2825674791 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2825674791  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___oneVector_5)); }
	inline Vector3_t2825674791  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2825674791 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2825674791  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___upVector_6)); }
	inline Vector3_t2825674791  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2825674791 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2825674791  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___downVector_7)); }
	inline Vector3_t2825674791  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2825674791 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2825674791  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___leftVector_8)); }
	inline Vector3_t2825674791  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2825674791 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2825674791  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___rightVector_9)); }
	inline Vector3_t2825674791  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2825674791 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2825674791  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2825674791  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2825674791 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2825674791  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___backVector_11)); }
	inline Vector3_t2825674791  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2825674791 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2825674791  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2825674791  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2825674791 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2825674791  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2825674791  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2825674791 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2825674791  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2825674791_H
#ifndef VECTOR2_T1065050092_H
#define VECTOR2_T1065050092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t1065050092 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t1065050092, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t1065050092, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t1065050092_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1065050092  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1065050092  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1065050092  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1065050092  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1065050092  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1065050092  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1065050092  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1065050092  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___zeroVector_2)); }
	inline Vector2_t1065050092  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t1065050092 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t1065050092  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___oneVector_3)); }
	inline Vector2_t1065050092  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t1065050092 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t1065050092  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___upVector_4)); }
	inline Vector2_t1065050092  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t1065050092 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t1065050092  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___downVector_5)); }
	inline Vector2_t1065050092  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t1065050092 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t1065050092  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___leftVector_6)); }
	inline Vector2_t1065050092  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t1065050092 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t1065050092  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___rightVector_7)); }
	inline Vector2_t1065050092  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t1065050092 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t1065050092  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t1065050092  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t1065050092 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t1065050092  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t1065050092  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t1065050092 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t1065050092  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T1065050092_H
#ifndef TWOCOLORTIMELINE_T1895897344_H
#define TWOCOLORTIMELINE_T1895897344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TwoColorTimeline
struct  TwoColorTimeline_t1895897344  : public CurveTimeline_t917256691
{
public:
	// System.Single[] Spine.TwoColorTimeline::frames
	SingleU5BU5D_t3989243465* ___frames_21;
	// System.Int32 Spine.TwoColorTimeline::slotIndex
	int32_t ___slotIndex_22;

public:
	inline static int32_t get_offset_of_frames_21() { return static_cast<int32_t>(offsetof(TwoColorTimeline_t1895897344, ___frames_21)); }
	inline SingleU5BU5D_t3989243465* get_frames_21() const { return ___frames_21; }
	inline SingleU5BU5D_t3989243465** get_address_of_frames_21() { return &___frames_21; }
	inline void set_frames_21(SingleU5BU5D_t3989243465* value)
	{
		___frames_21 = value;
		Il2CppCodeGenWriteBarrier((&___frames_21), value);
	}

	inline static int32_t get_offset_of_slotIndex_22() { return static_cast<int32_t>(offsetof(TwoColorTimeline_t1895897344, ___slotIndex_22)); }
	inline int32_t get_slotIndex_22() const { return ___slotIndex_22; }
	inline int32_t* get_address_of_slotIndex_22() { return &___slotIndex_22; }
	inline void set_slotIndex_22(int32_t value)
	{
		___slotIndex_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TWOCOLORTIMELINE_T1895897344_H
#ifndef ROTATETIMELINE_T263101077_H
#define ROTATETIMELINE_T263101077_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.RotateTimeline
struct  RotateTimeline_t263101077  : public CurveTimeline_t917256691
{
public:
	// System.Int32 Spine.RotateTimeline::boneIndex
	int32_t ___boneIndex_9;
	// System.Single[] Spine.RotateTimeline::frames
	SingleU5BU5D_t3989243465* ___frames_10;

public:
	inline static int32_t get_offset_of_boneIndex_9() { return static_cast<int32_t>(offsetof(RotateTimeline_t263101077, ___boneIndex_9)); }
	inline int32_t get_boneIndex_9() const { return ___boneIndex_9; }
	inline int32_t* get_address_of_boneIndex_9() { return &___boneIndex_9; }
	inline void set_boneIndex_9(int32_t value)
	{
		___boneIndex_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(RotateTimeline_t263101077, ___frames_10)); }
	inline SingleU5BU5D_t3989243465* get_frames_10() const { return ___frames_10; }
	inline SingleU5BU5D_t3989243465** get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(SingleU5BU5D_t3989243465* value)
	{
		___frames_10 = value;
		Il2CppCodeGenWriteBarrier((&___frames_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATETIMELINE_T263101077_H
#ifndef TRANSLATETIMELINE_T2198274540_H
#define TRANSLATETIMELINE_T2198274540_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TranslateTimeline
struct  TranslateTimeline_t2198274540  : public CurveTimeline_t917256691
{
public:
	// System.Int32 Spine.TranslateTimeline::boneIndex
	int32_t ___boneIndex_11;
	// System.Single[] Spine.TranslateTimeline::frames
	SingleU5BU5D_t3989243465* ___frames_12;

public:
	inline static int32_t get_offset_of_boneIndex_11() { return static_cast<int32_t>(offsetof(TranslateTimeline_t2198274540, ___boneIndex_11)); }
	inline int32_t get_boneIndex_11() const { return ___boneIndex_11; }
	inline int32_t* get_address_of_boneIndex_11() { return &___boneIndex_11; }
	inline void set_boneIndex_11(int32_t value)
	{
		___boneIndex_11 = value;
	}

	inline static int32_t get_offset_of_frames_12() { return static_cast<int32_t>(offsetof(TranslateTimeline_t2198274540, ___frames_12)); }
	inline SingleU5BU5D_t3989243465* get_frames_12() const { return ___frames_12; }
	inline SingleU5BU5D_t3989243465** get_address_of_frames_12() { return &___frames_12; }
	inline void set_frames_12(SingleU5BU5D_t3989243465* value)
	{
		___frames_12 = value;
		Il2CppCodeGenWriteBarrier((&___frames_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATETIMELINE_T2198274540_H
#ifndef CLIPPINGATTACHMENT_T906347500_H
#define CLIPPINGATTACHMENT_T906347500_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ClippingAttachment
struct  ClippingAttachment_t906347500  : public VertexAttachment_t1776545046
{
public:
	// Spine.SlotData Spine.ClippingAttachment::endSlot
	SlotData_t587781850 * ___endSlot_7;

public:
	inline static int32_t get_offset_of_endSlot_7() { return static_cast<int32_t>(offsetof(ClippingAttachment_t906347500, ___endSlot_7)); }
	inline SlotData_t587781850 * get_endSlot_7() const { return ___endSlot_7; }
	inline SlotData_t587781850 ** get_address_of_endSlot_7() { return &___endSlot_7; }
	inline void set_endSlot_7(SlotData_t587781850 * value)
	{
		___endSlot_7 = value;
		Il2CppCodeGenWriteBarrier((&___endSlot_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLIPPINGATTACHMENT_T906347500_H
#ifndef TEXTUREWRAP_T684305347_H
#define TEXTUREWRAP_T684305347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TextureWrap
struct  TextureWrap_t684305347 
{
public:
	// System.Int32 Spine.TextureWrap::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrap_t684305347, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAP_T684305347_H
#ifndef PATHATTACHMENT_T357603812_H
#define PATHATTACHMENT_T357603812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathAttachment
struct  PathAttachment_t357603812  : public VertexAttachment_t1776545046
{
public:
	// System.Single[] Spine.PathAttachment::lengths
	SingleU5BU5D_t3989243465* ___lengths_7;
	// System.Boolean Spine.PathAttachment::closed
	bool ___closed_8;
	// System.Boolean Spine.PathAttachment::constantSpeed
	bool ___constantSpeed_9;

public:
	inline static int32_t get_offset_of_lengths_7() { return static_cast<int32_t>(offsetof(PathAttachment_t357603812, ___lengths_7)); }
	inline SingleU5BU5D_t3989243465* get_lengths_7() const { return ___lengths_7; }
	inline SingleU5BU5D_t3989243465** get_address_of_lengths_7() { return &___lengths_7; }
	inline void set_lengths_7(SingleU5BU5D_t3989243465* value)
	{
		___lengths_7 = value;
		Il2CppCodeGenWriteBarrier((&___lengths_7), value);
	}

	inline static int32_t get_offset_of_closed_8() { return static_cast<int32_t>(offsetof(PathAttachment_t357603812, ___closed_8)); }
	inline bool get_closed_8() const { return ___closed_8; }
	inline bool* get_address_of_closed_8() { return &___closed_8; }
	inline void set_closed_8(bool value)
	{
		___closed_8 = value;
	}

	inline static int32_t get_offset_of_constantSpeed_9() { return static_cast<int32_t>(offsetof(PathAttachment_t357603812, ___constantSpeed_9)); }
	inline bool get_constantSpeed_9() const { return ___constantSpeed_9; }
	inline bool* get_address_of_constantSpeed_9() { return &___constantSpeed_9; }
	inline void set_constantSpeed_9(bool value)
	{
		___constantSpeed_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHATTACHMENT_T357603812_H
#ifndef DELEGATE_T1310841479_H
#define DELEGATE_T1310841479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1310841479  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t878649875 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___data_8)); }
	inline DelegateData_t878649875 * get_data_8() const { return ___data_8; }
	inline DelegateData_t878649875 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t878649875 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1310841479_H
#ifndef BOUNDINGBOXATTACHMENT_T1026836171_H
#define BOUNDINGBOXATTACHMENT_T1026836171_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BoundingBoxAttachment
struct  BoundingBoxAttachment_t1026836171  : public VertexAttachment_t1776545046
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXATTACHMENT_T1026836171_H
#ifndef ATTACHMENTTYPE_T2811772594_H
#define ATTACHMENTTYPE_T2811772594_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AttachmentType
struct  AttachmentType_t2811772594 
{
public:
	// System.Int32 Spine.AttachmentType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(AttachmentType_t2811772594, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTTYPE_T2811772594_H
#ifndef BLENDMODE_T1090287593_H
#define BLENDMODE_T1090287593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BlendMode
struct  BlendMode_t1090287593 
{
public:
	// System.Int32 Spine.BlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlendMode_t1090287593, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDMODE_T1090287593_H
#ifndef TRANSFORMMODE_T1529364024_H
#define TRANSFORMMODE_T1529364024_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformMode
struct  TransformMode_t1529364024 
{
public:
	// System.Int32 Spine.TransformMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TransformMode_t1529364024, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMMODE_T1529364024_H
#ifndef MESHATTACHMENT_T3550597918_H
#define MESHATTACHMENT_T3550597918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MeshAttachment
struct  MeshAttachment_t3550597918  : public VertexAttachment_t1776545046
{
public:
	// System.Single Spine.MeshAttachment::regionOffsetX
	float ___regionOffsetX_7;
	// System.Single Spine.MeshAttachment::regionOffsetY
	float ___regionOffsetY_8;
	// System.Single Spine.MeshAttachment::regionWidth
	float ___regionWidth_9;
	// System.Single Spine.MeshAttachment::regionHeight
	float ___regionHeight_10;
	// System.Single Spine.MeshAttachment::regionOriginalWidth
	float ___regionOriginalWidth_11;
	// System.Single Spine.MeshAttachment::regionOriginalHeight
	float ___regionOriginalHeight_12;
	// Spine.MeshAttachment Spine.MeshAttachment::parentMesh
	MeshAttachment_t3550597918 * ___parentMesh_13;
	// System.Single[] Spine.MeshAttachment::uvs
	SingleU5BU5D_t3989243465* ___uvs_14;
	// System.Single[] Spine.MeshAttachment::regionUVs
	SingleU5BU5D_t3989243465* ___regionUVs_15;
	// System.Int32[] Spine.MeshAttachment::triangles
	Int32U5BU5D_t281224829* ___triangles_16;
	// System.Single Spine.MeshAttachment::r
	float ___r_17;
	// System.Single Spine.MeshAttachment::g
	float ___g_18;
	// System.Single Spine.MeshAttachment::b
	float ___b_19;
	// System.Single Spine.MeshAttachment::a
	float ___a_20;
	// System.Int32 Spine.MeshAttachment::hulllength
	int32_t ___hulllength_21;
	// System.Boolean Spine.MeshAttachment::inheritDeform
	bool ___inheritDeform_22;
	// System.String Spine.MeshAttachment::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_23;
	// System.Object Spine.MeshAttachment::<RendererObject>k__BackingField
	RuntimeObject * ___U3CRendererObjectU3Ek__BackingField_24;
	// System.Single Spine.MeshAttachment::<RegionU>k__BackingField
	float ___U3CRegionUU3Ek__BackingField_25;
	// System.Single Spine.MeshAttachment::<RegionV>k__BackingField
	float ___U3CRegionVU3Ek__BackingField_26;
	// System.Single Spine.MeshAttachment::<RegionU2>k__BackingField
	float ___U3CRegionU2U3Ek__BackingField_27;
	// System.Single Spine.MeshAttachment::<RegionV2>k__BackingField
	float ___U3CRegionV2U3Ek__BackingField_28;
	// System.Boolean Spine.MeshAttachment::<RegionRotate>k__BackingField
	bool ___U3CRegionRotateU3Ek__BackingField_29;
	// System.Int32[] Spine.MeshAttachment::<Edges>k__BackingField
	Int32U5BU5D_t281224829* ___U3CEdgesU3Ek__BackingField_30;
	// System.Single Spine.MeshAttachment::<Width>k__BackingField
	float ___U3CWidthU3Ek__BackingField_31;
	// System.Single Spine.MeshAttachment::<Height>k__BackingField
	float ___U3CHeightU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of_regionOffsetX_7() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___regionOffsetX_7)); }
	inline float get_regionOffsetX_7() const { return ___regionOffsetX_7; }
	inline float* get_address_of_regionOffsetX_7() { return &___regionOffsetX_7; }
	inline void set_regionOffsetX_7(float value)
	{
		___regionOffsetX_7 = value;
	}

	inline static int32_t get_offset_of_regionOffsetY_8() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___regionOffsetY_8)); }
	inline float get_regionOffsetY_8() const { return ___regionOffsetY_8; }
	inline float* get_address_of_regionOffsetY_8() { return &___regionOffsetY_8; }
	inline void set_regionOffsetY_8(float value)
	{
		___regionOffsetY_8 = value;
	}

	inline static int32_t get_offset_of_regionWidth_9() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___regionWidth_9)); }
	inline float get_regionWidth_9() const { return ___regionWidth_9; }
	inline float* get_address_of_regionWidth_9() { return &___regionWidth_9; }
	inline void set_regionWidth_9(float value)
	{
		___regionWidth_9 = value;
	}

	inline static int32_t get_offset_of_regionHeight_10() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___regionHeight_10)); }
	inline float get_regionHeight_10() const { return ___regionHeight_10; }
	inline float* get_address_of_regionHeight_10() { return &___regionHeight_10; }
	inline void set_regionHeight_10(float value)
	{
		___regionHeight_10 = value;
	}

	inline static int32_t get_offset_of_regionOriginalWidth_11() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___regionOriginalWidth_11)); }
	inline float get_regionOriginalWidth_11() const { return ___regionOriginalWidth_11; }
	inline float* get_address_of_regionOriginalWidth_11() { return &___regionOriginalWidth_11; }
	inline void set_regionOriginalWidth_11(float value)
	{
		___regionOriginalWidth_11 = value;
	}

	inline static int32_t get_offset_of_regionOriginalHeight_12() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___regionOriginalHeight_12)); }
	inline float get_regionOriginalHeight_12() const { return ___regionOriginalHeight_12; }
	inline float* get_address_of_regionOriginalHeight_12() { return &___regionOriginalHeight_12; }
	inline void set_regionOriginalHeight_12(float value)
	{
		___regionOriginalHeight_12 = value;
	}

	inline static int32_t get_offset_of_parentMesh_13() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___parentMesh_13)); }
	inline MeshAttachment_t3550597918 * get_parentMesh_13() const { return ___parentMesh_13; }
	inline MeshAttachment_t3550597918 ** get_address_of_parentMesh_13() { return &___parentMesh_13; }
	inline void set_parentMesh_13(MeshAttachment_t3550597918 * value)
	{
		___parentMesh_13 = value;
		Il2CppCodeGenWriteBarrier((&___parentMesh_13), value);
	}

	inline static int32_t get_offset_of_uvs_14() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___uvs_14)); }
	inline SingleU5BU5D_t3989243465* get_uvs_14() const { return ___uvs_14; }
	inline SingleU5BU5D_t3989243465** get_address_of_uvs_14() { return &___uvs_14; }
	inline void set_uvs_14(SingleU5BU5D_t3989243465* value)
	{
		___uvs_14 = value;
		Il2CppCodeGenWriteBarrier((&___uvs_14), value);
	}

	inline static int32_t get_offset_of_regionUVs_15() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___regionUVs_15)); }
	inline SingleU5BU5D_t3989243465* get_regionUVs_15() const { return ___regionUVs_15; }
	inline SingleU5BU5D_t3989243465** get_address_of_regionUVs_15() { return &___regionUVs_15; }
	inline void set_regionUVs_15(SingleU5BU5D_t3989243465* value)
	{
		___regionUVs_15 = value;
		Il2CppCodeGenWriteBarrier((&___regionUVs_15), value);
	}

	inline static int32_t get_offset_of_triangles_16() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___triangles_16)); }
	inline Int32U5BU5D_t281224829* get_triangles_16() const { return ___triangles_16; }
	inline Int32U5BU5D_t281224829** get_address_of_triangles_16() { return &___triangles_16; }
	inline void set_triangles_16(Int32U5BU5D_t281224829* value)
	{
		___triangles_16 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_16), value);
	}

	inline static int32_t get_offset_of_r_17() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___r_17)); }
	inline float get_r_17() const { return ___r_17; }
	inline float* get_address_of_r_17() { return &___r_17; }
	inline void set_r_17(float value)
	{
		___r_17 = value;
	}

	inline static int32_t get_offset_of_g_18() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___g_18)); }
	inline float get_g_18() const { return ___g_18; }
	inline float* get_address_of_g_18() { return &___g_18; }
	inline void set_g_18(float value)
	{
		___g_18 = value;
	}

	inline static int32_t get_offset_of_b_19() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___b_19)); }
	inline float get_b_19() const { return ___b_19; }
	inline float* get_address_of_b_19() { return &___b_19; }
	inline void set_b_19(float value)
	{
		___b_19 = value;
	}

	inline static int32_t get_offset_of_a_20() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___a_20)); }
	inline float get_a_20() const { return ___a_20; }
	inline float* get_address_of_a_20() { return &___a_20; }
	inline void set_a_20(float value)
	{
		___a_20 = value;
	}

	inline static int32_t get_offset_of_hulllength_21() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___hulllength_21)); }
	inline int32_t get_hulllength_21() const { return ___hulllength_21; }
	inline int32_t* get_address_of_hulllength_21() { return &___hulllength_21; }
	inline void set_hulllength_21(int32_t value)
	{
		___hulllength_21 = value;
	}

	inline static int32_t get_offset_of_inheritDeform_22() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___inheritDeform_22)); }
	inline bool get_inheritDeform_22() const { return ___inheritDeform_22; }
	inline bool* get_address_of_inheritDeform_22() { return &___inheritDeform_22; }
	inline void set_inheritDeform_22(bool value)
	{
		___inheritDeform_22 = value;
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___U3CPathU3Ek__BackingField_23)); }
	inline String_t* get_U3CPathU3Ek__BackingField_23() const { return ___U3CPathU3Ek__BackingField_23; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_23() { return &___U3CPathU3Ek__BackingField_23; }
	inline void set_U3CPathU3Ek__BackingField_23(String_t* value)
	{
		___U3CPathU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CRendererObjectU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___U3CRendererObjectU3Ek__BackingField_24)); }
	inline RuntimeObject * get_U3CRendererObjectU3Ek__BackingField_24() const { return ___U3CRendererObjectU3Ek__BackingField_24; }
	inline RuntimeObject ** get_address_of_U3CRendererObjectU3Ek__BackingField_24() { return &___U3CRendererObjectU3Ek__BackingField_24; }
	inline void set_U3CRendererObjectU3Ek__BackingField_24(RuntimeObject * value)
	{
		___U3CRendererObjectU3Ek__BackingField_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRendererObjectU3Ek__BackingField_24), value);
	}

	inline static int32_t get_offset_of_U3CRegionUU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___U3CRegionUU3Ek__BackingField_25)); }
	inline float get_U3CRegionUU3Ek__BackingField_25() const { return ___U3CRegionUU3Ek__BackingField_25; }
	inline float* get_address_of_U3CRegionUU3Ek__BackingField_25() { return &___U3CRegionUU3Ek__BackingField_25; }
	inline void set_U3CRegionUU3Ek__BackingField_25(float value)
	{
		___U3CRegionUU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CRegionVU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___U3CRegionVU3Ek__BackingField_26)); }
	inline float get_U3CRegionVU3Ek__BackingField_26() const { return ___U3CRegionVU3Ek__BackingField_26; }
	inline float* get_address_of_U3CRegionVU3Ek__BackingField_26() { return &___U3CRegionVU3Ek__BackingField_26; }
	inline void set_U3CRegionVU3Ek__BackingField_26(float value)
	{
		___U3CRegionVU3Ek__BackingField_26 = value;
	}

	inline static int32_t get_offset_of_U3CRegionU2U3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___U3CRegionU2U3Ek__BackingField_27)); }
	inline float get_U3CRegionU2U3Ek__BackingField_27() const { return ___U3CRegionU2U3Ek__BackingField_27; }
	inline float* get_address_of_U3CRegionU2U3Ek__BackingField_27() { return &___U3CRegionU2U3Ek__BackingField_27; }
	inline void set_U3CRegionU2U3Ek__BackingField_27(float value)
	{
		___U3CRegionU2U3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CRegionV2U3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___U3CRegionV2U3Ek__BackingField_28)); }
	inline float get_U3CRegionV2U3Ek__BackingField_28() const { return ___U3CRegionV2U3Ek__BackingField_28; }
	inline float* get_address_of_U3CRegionV2U3Ek__BackingField_28() { return &___U3CRegionV2U3Ek__BackingField_28; }
	inline void set_U3CRegionV2U3Ek__BackingField_28(float value)
	{
		___U3CRegionV2U3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CRegionRotateU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___U3CRegionRotateU3Ek__BackingField_29)); }
	inline bool get_U3CRegionRotateU3Ek__BackingField_29() const { return ___U3CRegionRotateU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CRegionRotateU3Ek__BackingField_29() { return &___U3CRegionRotateU3Ek__BackingField_29; }
	inline void set_U3CRegionRotateU3Ek__BackingField_29(bool value)
	{
		___U3CRegionRotateU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_U3CEdgesU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___U3CEdgesU3Ek__BackingField_30)); }
	inline Int32U5BU5D_t281224829* get_U3CEdgesU3Ek__BackingField_30() const { return ___U3CEdgesU3Ek__BackingField_30; }
	inline Int32U5BU5D_t281224829** get_address_of_U3CEdgesU3Ek__BackingField_30() { return &___U3CEdgesU3Ek__BackingField_30; }
	inline void set_U3CEdgesU3Ek__BackingField_30(Int32U5BU5D_t281224829* value)
	{
		___U3CEdgesU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEdgesU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___U3CWidthU3Ek__BackingField_31)); }
	inline float get_U3CWidthU3Ek__BackingField_31() const { return ___U3CWidthU3Ek__BackingField_31; }
	inline float* get_address_of_U3CWidthU3Ek__BackingField_31() { return &___U3CWidthU3Ek__BackingField_31; }
	inline void set_U3CWidthU3Ek__BackingField_31(float value)
	{
		___U3CWidthU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(MeshAttachment_t3550597918, ___U3CHeightU3Ek__BackingField_32)); }
	inline float get_U3CHeightU3Ek__BackingField_32() const { return ___U3CHeightU3Ek__BackingField_32; }
	inline float* get_address_of_U3CHeightU3Ek__BackingField_32() { return &___U3CHeightU3Ek__BackingField_32; }
	inline void set_U3CHeightU3Ek__BackingField_32(float value)
	{
		___U3CHeightU3Ek__BackingField_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHATTACHMENT_T3550597918_H
#ifndef SHEARTIMELINE_T3117059177_H
#define SHEARTIMELINE_T3117059177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ShearTimeline
struct  ShearTimeline_t3117059177  : public TranslateTimeline_t2198274540
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHEARTIMELINE_T3117059177_H
#ifndef TEXTUREFILTER_T3309269356_H
#define TEXTUREFILTER_T3309269356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TextureFilter
struct  TextureFilter_t3309269356 
{
public:
	// System.Int32 Spine.TextureFilter::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFilter_t3309269356, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFILTER_T3309269356_H
#ifndef MIXDIRECTION_T1513473484_H
#define MIXDIRECTION_T1513473484_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MixDirection
struct  MixDirection_t1513473484 
{
public:
	// System.Int32 Spine.MixDirection::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MixDirection_t1513473484, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXDIRECTION_T1513473484_H
#ifndef SCALETIMELINE_T977293153_H
#define SCALETIMELINE_T977293153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ScaleTimeline
struct  ScaleTimeline_t977293153  : public TranslateTimeline_t2198274540
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALETIMELINE_T977293153_H
#ifndef TIMELINETYPE_T3903614005_H
#define TIMELINETYPE_T3903614005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TimelineType
struct  TimelineType_t3903614005 
{
public:
	// System.Int32 Spine.TimelineType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TimelineType_t3903614005, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMELINETYPE_T3903614005_H
#ifndef PATHCONSTRAINTSPACINGTIMELINE_T2013794857_H
#define PATHCONSTRAINTSPACINGTIMELINE_T2013794857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintSpacingTimeline
struct  PathConstraintSpacingTimeline_t2013794857  : public PathConstraintPositionTimeline_t2710737122
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTSPACINGTIMELINE_T2013794857_H
#ifndef FORMAT_T2486158377_H
#define FORMAT_T2486158377_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Format
struct  Format_t2486158377 
{
public:
	// System.Int32 Spine.Format::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Format_t2486158377, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMAT_T2486158377_H
#ifndef MIXPOSE_T1042684880_H
#define MIXPOSE_T1042684880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MixPose
struct  MixPose_t1042684880 
{
public:
	// System.Int32 Spine.MixPose::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MixPose_t1042684880, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXPOSE_T1042684880_H
#ifndef OBJECT_T350248726_H
#define OBJECT_T350248726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t350248726  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t350248726, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t350248726_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t350248726_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t350248726_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t350248726_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T350248726_H
#ifndef EVENTTYPE_T3243473135_H
#define EVENTTYPE_T3243473135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventQueue/EventType
struct  EventType_t3243473135 
{
public:
	// System.Int32 Spine.EventQueue/EventType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EventType_t3243473135, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTYPE_T3243473135_H
#ifndef SLOTSETTINGS_T3151105826_H
#define SLOTSETTINGS_T3151105826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.SkeletonColorInitialize/SlotSettings
struct  SlotSettings_t3151105826  : public RuntimeObject
{
public:
	// System.String Spine.Unity.Examples.SkeletonColorInitialize/SlotSettings::slot
	String_t* ___slot_0;
	// UnityEngine.Color Spine.Unity.Examples.SkeletonColorInitialize/SlotSettings::color
	Color_t4183816058  ___color_1;

public:
	inline static int32_t get_offset_of_slot_0() { return static_cast<int32_t>(offsetof(SlotSettings_t3151105826, ___slot_0)); }
	inline String_t* get_slot_0() const { return ___slot_0; }
	inline String_t** get_address_of_slot_0() { return &___slot_0; }
	inline void set_slot_0(String_t* value)
	{
		___slot_0 = value;
		Il2CppCodeGenWriteBarrier((&___slot_0), value);
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(SlotSettings_t3151105826, ___color_1)); }
	inline Color_t4183816058  get_color_1() const { return ___color_1; }
	inline Color_t4183816058 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_t4183816058  value)
	{
		___color_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTSETTINGS_T3151105826_H
#ifndef RAYCASTHIT2D_T3956163020_H
#define RAYCASTHIT2D_T3956163020_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit2D
struct  RaycastHit2D_t3956163020 
{
public:
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Centroid
	Vector2_t1065050092  ___m_Centroid_0;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Point
	Vector2_t1065050092  ___m_Point_1;
	// UnityEngine.Vector2 UnityEngine.RaycastHit2D::m_Normal
	Vector2_t1065050092  ___m_Normal_2;
	// System.Single UnityEngine.RaycastHit2D::m_Distance
	float ___m_Distance_3;
	// System.Single UnityEngine.RaycastHit2D::m_Fraction
	float ___m_Fraction_4;
	// UnityEngine.Collider2D UnityEngine.RaycastHit2D::m_Collider
	Collider2D_t179611260 * ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Centroid_0() { return static_cast<int32_t>(offsetof(RaycastHit2D_t3956163020, ___m_Centroid_0)); }
	inline Vector2_t1065050092  get_m_Centroid_0() const { return ___m_Centroid_0; }
	inline Vector2_t1065050092 * get_address_of_m_Centroid_0() { return &___m_Centroid_0; }
	inline void set_m_Centroid_0(Vector2_t1065050092  value)
	{
		___m_Centroid_0 = value;
	}

	inline static int32_t get_offset_of_m_Point_1() { return static_cast<int32_t>(offsetof(RaycastHit2D_t3956163020, ___m_Point_1)); }
	inline Vector2_t1065050092  get_m_Point_1() const { return ___m_Point_1; }
	inline Vector2_t1065050092 * get_address_of_m_Point_1() { return &___m_Point_1; }
	inline void set_m_Point_1(Vector2_t1065050092  value)
	{
		___m_Point_1 = value;
	}

	inline static int32_t get_offset_of_m_Normal_2() { return static_cast<int32_t>(offsetof(RaycastHit2D_t3956163020, ___m_Normal_2)); }
	inline Vector2_t1065050092  get_m_Normal_2() const { return ___m_Normal_2; }
	inline Vector2_t1065050092 * get_address_of_m_Normal_2() { return &___m_Normal_2; }
	inline void set_m_Normal_2(Vector2_t1065050092  value)
	{
		___m_Normal_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit2D_t3956163020, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_Fraction_4() { return static_cast<int32_t>(offsetof(RaycastHit2D_t3956163020, ___m_Fraction_4)); }
	inline float get_m_Fraction_4() const { return ___m_Fraction_4; }
	inline float* get_address_of_m_Fraction_4() { return &___m_Fraction_4; }
	inline void set_m_Fraction_4(float value)
	{
		___m_Fraction_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit2D_t3956163020, ___m_Collider_5)); }
	inline Collider2D_t179611260 * get_m_Collider_5() const { return ___m_Collider_5; }
	inline Collider2D_t179611260 ** get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(Collider2D_t179611260 * value)
	{
		___m_Collider_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Collider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t3956163020_marshaled_pinvoke
{
	Vector2_t1065050092  ___m_Centroid_0;
	Vector2_t1065050092  ___m_Point_1;
	Vector2_t1065050092  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t179611260 * ___m_Collider_5;
};
// Native definition for COM marshalling of UnityEngine.RaycastHit2D
struct RaycastHit2D_t3956163020_marshaled_com
{
	Vector2_t1065050092  ___m_Centroid_0;
	Vector2_t1065050092  ___m_Point_1;
	Vector2_t1065050092  ___m_Normal_2;
	float ___m_Distance_3;
	float ___m_Fraction_4;
	Collider2D_t179611260 * ___m_Collider_5;
};
#endif // RAYCASTHIT2D_T3956163020_H
#ifndef TOKEN_T1548208502_H
#define TOKEN_T1548208502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SharpJson.Lexer/Token
struct  Token_t1548208502 
{
public:
	// System.Int32 SharpJson.Lexer/Token::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Token_t1548208502, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T1548208502_H
#ifndef EVENTQUEUEENTRY_T3756679084_H
#define EVENTQUEUEENTRY_T3756679084_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventQueue/EventQueueEntry
struct  EventQueueEntry_t3756679084 
{
public:
	// Spine.EventQueue/EventType Spine.EventQueue/EventQueueEntry::type
	int32_t ___type_0;
	// Spine.TrackEntry Spine.EventQueue/EventQueueEntry::entry
	TrackEntry_t4062297782 * ___entry_1;
	// Spine.Event Spine.EventQueue/EventQueueEntry::e
	Event_t27715519 * ___e_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(EventQueueEntry_t3756679084, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_entry_1() { return static_cast<int32_t>(offsetof(EventQueueEntry_t3756679084, ___entry_1)); }
	inline TrackEntry_t4062297782 * get_entry_1() const { return ___entry_1; }
	inline TrackEntry_t4062297782 ** get_address_of_entry_1() { return &___entry_1; }
	inline void set_entry_1(TrackEntry_t4062297782 * value)
	{
		___entry_1 = value;
		Il2CppCodeGenWriteBarrier((&___entry_1), value);
	}

	inline static int32_t get_offset_of_e_2() { return static_cast<int32_t>(offsetof(EventQueueEntry_t3756679084, ___e_2)); }
	inline Event_t27715519 * get_e_2() const { return ___e_2; }
	inline Event_t27715519 ** get_address_of_e_2() { return &___e_2; }
	inline void set_e_2(Event_t27715519 * value)
	{
		___e_2 = value;
		Il2CppCodeGenWriteBarrier((&___e_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.EventQueue/EventQueueEntry
struct EventQueueEntry_t3756679084_marshaled_pinvoke
{
	int32_t ___type_0;
	TrackEntry_t4062297782 * ___entry_1;
	Event_t27715519 * ___e_2;
};
// Native definition for COM marshalling of Spine.EventQueue/EventQueueEntry
struct EventQueueEntry_t3756679084_marshaled_com
{
	int32_t ___type_0;
	TrackEntry_t4062297782 * ___entry_1;
	Event_t27715519 * ___e_2;
};
#endif // EVENTQUEUEENTRY_T3756679084_H
#ifndef U3CRESTOREU3EC__ITERATOR0_T729631813_H
#define U3CRESTOREU3EC__ITERATOR0_T729631813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.RaggedySpineboy/<Restore>c__Iterator0
struct  U3CRestoreU3Ec__Iterator0_t729631813  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 Spine.Unity.Examples.RaggedySpineboy/<Restore>c__Iterator0::<estimatedPos>__0
	Vector3_t2825674791  ___U3CestimatedPosU3E__0_0;
	// UnityEngine.Vector3 Spine.Unity.Examples.RaggedySpineboy/<Restore>c__Iterator0::<rbPosition>__0
	Vector3_t2825674791  ___U3CrbPositionU3E__0_1;
	// UnityEngine.Vector3 Spine.Unity.Examples.RaggedySpineboy/<Restore>c__Iterator0::<skeletonPoint>__0
	Vector3_t2825674791  ___U3CskeletonPointU3E__0_2;
	// UnityEngine.RaycastHit2D Spine.Unity.Examples.RaggedySpineboy/<Restore>c__Iterator0::<hit>__0
	RaycastHit2D_t3956163020  ___U3ChitU3E__0_3;
	// Spine.Unity.Examples.RaggedySpineboy Spine.Unity.Examples.RaggedySpineboy/<Restore>c__Iterator0::$this
	RaggedySpineboy_t30683967 * ___U24this_4;
	// System.Object Spine.Unity.Examples.RaggedySpineboy/<Restore>c__Iterator0::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Spine.Unity.Examples.RaggedySpineboy/<Restore>c__Iterator0::$disposing
	bool ___U24disposing_6;
	// System.Int32 Spine.Unity.Examples.RaggedySpineboy/<Restore>c__Iterator0::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CestimatedPosU3E__0_0() { return static_cast<int32_t>(offsetof(U3CRestoreU3Ec__Iterator0_t729631813, ___U3CestimatedPosU3E__0_0)); }
	inline Vector3_t2825674791  get_U3CestimatedPosU3E__0_0() const { return ___U3CestimatedPosU3E__0_0; }
	inline Vector3_t2825674791 * get_address_of_U3CestimatedPosU3E__0_0() { return &___U3CestimatedPosU3E__0_0; }
	inline void set_U3CestimatedPosU3E__0_0(Vector3_t2825674791  value)
	{
		___U3CestimatedPosU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CrbPositionU3E__0_1() { return static_cast<int32_t>(offsetof(U3CRestoreU3Ec__Iterator0_t729631813, ___U3CrbPositionU3E__0_1)); }
	inline Vector3_t2825674791  get_U3CrbPositionU3E__0_1() const { return ___U3CrbPositionU3E__0_1; }
	inline Vector3_t2825674791 * get_address_of_U3CrbPositionU3E__0_1() { return &___U3CrbPositionU3E__0_1; }
	inline void set_U3CrbPositionU3E__0_1(Vector3_t2825674791  value)
	{
		___U3CrbPositionU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CskeletonPointU3E__0_2() { return static_cast<int32_t>(offsetof(U3CRestoreU3Ec__Iterator0_t729631813, ___U3CskeletonPointU3E__0_2)); }
	inline Vector3_t2825674791  get_U3CskeletonPointU3E__0_2() const { return ___U3CskeletonPointU3E__0_2; }
	inline Vector3_t2825674791 * get_address_of_U3CskeletonPointU3E__0_2() { return &___U3CskeletonPointU3E__0_2; }
	inline void set_U3CskeletonPointU3E__0_2(Vector3_t2825674791  value)
	{
		___U3CskeletonPointU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3ChitU3E__0_3() { return static_cast<int32_t>(offsetof(U3CRestoreU3Ec__Iterator0_t729631813, ___U3ChitU3E__0_3)); }
	inline RaycastHit2D_t3956163020  get_U3ChitU3E__0_3() const { return ___U3ChitU3E__0_3; }
	inline RaycastHit2D_t3956163020 * get_address_of_U3ChitU3E__0_3() { return &___U3ChitU3E__0_3; }
	inline void set_U3ChitU3E__0_3(RaycastHit2D_t3956163020  value)
	{
		___U3ChitU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CRestoreU3Ec__Iterator0_t729631813, ___U24this_4)); }
	inline RaggedySpineboy_t30683967 * get_U24this_4() const { return ___U24this_4; }
	inline RaggedySpineboy_t30683967 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(RaggedySpineboy_t30683967 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CRestoreU3Ec__Iterator0_t729631813, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CRestoreU3Ec__Iterator0_t729631813, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CRestoreU3Ec__Iterator0_t729631813, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESTOREU3EC__ITERATOR0_T729631813_H
#ifndef MULTICASTDELEGATE_T4207739222_H
#define MULTICASTDELEGATE_T4207739222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t4207739222  : public Delegate_t1310841479
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t4207739222 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t4207739222 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4207739222, ___prev_9)); }
	inline MulticastDelegate_t4207739222 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t4207739222 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t4207739222 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4207739222, ___kpm_next_10)); }
	inline MulticastDelegate_t4207739222 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t4207739222 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t4207739222 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T4207739222_H
#ifndef COMPONENT_T1852985663_H
#define COMPONENT_T1852985663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1852985663  : public Object_t350248726
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1852985663_H
#ifndef BONEDATA_T1194659452_H
#define BONEDATA_T1194659452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BoneData
struct  BoneData_t1194659452  : public RuntimeObject
{
public:
	// System.Int32 Spine.BoneData::index
	int32_t ___index_0;
	// System.String Spine.BoneData::name
	String_t* ___name_1;
	// Spine.BoneData Spine.BoneData::parent
	BoneData_t1194659452 * ___parent_2;
	// System.Single Spine.BoneData::length
	float ___length_3;
	// System.Single Spine.BoneData::x
	float ___x_4;
	// System.Single Spine.BoneData::y
	float ___y_5;
	// System.Single Spine.BoneData::rotation
	float ___rotation_6;
	// System.Single Spine.BoneData::scaleX
	float ___scaleX_7;
	// System.Single Spine.BoneData::scaleY
	float ___scaleY_8;
	// System.Single Spine.BoneData::shearX
	float ___shearX_9;
	// System.Single Spine.BoneData::shearY
	float ___shearY_10;
	// Spine.TransformMode Spine.BoneData::transformMode
	int32_t ___transformMode_11;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(BoneData_t1194659452, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(BoneData_t1194659452, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_parent_2() { return static_cast<int32_t>(offsetof(BoneData_t1194659452, ___parent_2)); }
	inline BoneData_t1194659452 * get_parent_2() const { return ___parent_2; }
	inline BoneData_t1194659452 ** get_address_of_parent_2() { return &___parent_2; }
	inline void set_parent_2(BoneData_t1194659452 * value)
	{
		___parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___parent_2), value);
	}

	inline static int32_t get_offset_of_length_3() { return static_cast<int32_t>(offsetof(BoneData_t1194659452, ___length_3)); }
	inline float get_length_3() const { return ___length_3; }
	inline float* get_address_of_length_3() { return &___length_3; }
	inline void set_length_3(float value)
	{
		___length_3 = value;
	}

	inline static int32_t get_offset_of_x_4() { return static_cast<int32_t>(offsetof(BoneData_t1194659452, ___x_4)); }
	inline float get_x_4() const { return ___x_4; }
	inline float* get_address_of_x_4() { return &___x_4; }
	inline void set_x_4(float value)
	{
		___x_4 = value;
	}

	inline static int32_t get_offset_of_y_5() { return static_cast<int32_t>(offsetof(BoneData_t1194659452, ___y_5)); }
	inline float get_y_5() const { return ___y_5; }
	inline float* get_address_of_y_5() { return &___y_5; }
	inline void set_y_5(float value)
	{
		___y_5 = value;
	}

	inline static int32_t get_offset_of_rotation_6() { return static_cast<int32_t>(offsetof(BoneData_t1194659452, ___rotation_6)); }
	inline float get_rotation_6() const { return ___rotation_6; }
	inline float* get_address_of_rotation_6() { return &___rotation_6; }
	inline void set_rotation_6(float value)
	{
		___rotation_6 = value;
	}

	inline static int32_t get_offset_of_scaleX_7() { return static_cast<int32_t>(offsetof(BoneData_t1194659452, ___scaleX_7)); }
	inline float get_scaleX_7() const { return ___scaleX_7; }
	inline float* get_address_of_scaleX_7() { return &___scaleX_7; }
	inline void set_scaleX_7(float value)
	{
		___scaleX_7 = value;
	}

	inline static int32_t get_offset_of_scaleY_8() { return static_cast<int32_t>(offsetof(BoneData_t1194659452, ___scaleY_8)); }
	inline float get_scaleY_8() const { return ___scaleY_8; }
	inline float* get_address_of_scaleY_8() { return &___scaleY_8; }
	inline void set_scaleY_8(float value)
	{
		___scaleY_8 = value;
	}

	inline static int32_t get_offset_of_shearX_9() { return static_cast<int32_t>(offsetof(BoneData_t1194659452, ___shearX_9)); }
	inline float get_shearX_9() const { return ___shearX_9; }
	inline float* get_address_of_shearX_9() { return &___shearX_9; }
	inline void set_shearX_9(float value)
	{
		___shearX_9 = value;
	}

	inline static int32_t get_offset_of_shearY_10() { return static_cast<int32_t>(offsetof(BoneData_t1194659452, ___shearY_10)); }
	inline float get_shearY_10() const { return ___shearY_10; }
	inline float* get_address_of_shearY_10() { return &___shearY_10; }
	inline void set_shearY_10(float value)
	{
		___shearY_10 = value;
	}

	inline static int32_t get_offset_of_transformMode_11() { return static_cast<int32_t>(offsetof(BoneData_t1194659452, ___transformMode_11)); }
	inline int32_t get_transformMode_11() const { return ___transformMode_11; }
	inline int32_t* get_address_of_transformMode_11() { return &___transformMode_11; }
	inline void set_transformMode_11(int32_t value)
	{
		___transformMode_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEDATA_T1194659452_H
#ifndef ATLASPAGE_T3729449121_H
#define ATLASPAGE_T3729449121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AtlasPage
struct  AtlasPage_t3729449121  : public RuntimeObject
{
public:
	// System.String Spine.AtlasPage::name
	String_t* ___name_0;
	// Spine.Format Spine.AtlasPage::format
	int32_t ___format_1;
	// Spine.TextureFilter Spine.AtlasPage::minFilter
	int32_t ___minFilter_2;
	// Spine.TextureFilter Spine.AtlasPage::magFilter
	int32_t ___magFilter_3;
	// Spine.TextureWrap Spine.AtlasPage::uWrap
	int32_t ___uWrap_4;
	// Spine.TextureWrap Spine.AtlasPage::vWrap
	int32_t ___vWrap_5;
	// System.Object Spine.AtlasPage::rendererObject
	RuntimeObject * ___rendererObject_6;
	// System.Int32 Spine.AtlasPage::width
	int32_t ___width_7;
	// System.Int32 Spine.AtlasPage::height
	int32_t ___height_8;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(AtlasPage_t3729449121, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_format_1() { return static_cast<int32_t>(offsetof(AtlasPage_t3729449121, ___format_1)); }
	inline int32_t get_format_1() const { return ___format_1; }
	inline int32_t* get_address_of_format_1() { return &___format_1; }
	inline void set_format_1(int32_t value)
	{
		___format_1 = value;
	}

	inline static int32_t get_offset_of_minFilter_2() { return static_cast<int32_t>(offsetof(AtlasPage_t3729449121, ___minFilter_2)); }
	inline int32_t get_minFilter_2() const { return ___minFilter_2; }
	inline int32_t* get_address_of_minFilter_2() { return &___minFilter_2; }
	inline void set_minFilter_2(int32_t value)
	{
		___minFilter_2 = value;
	}

	inline static int32_t get_offset_of_magFilter_3() { return static_cast<int32_t>(offsetof(AtlasPage_t3729449121, ___magFilter_3)); }
	inline int32_t get_magFilter_3() const { return ___magFilter_3; }
	inline int32_t* get_address_of_magFilter_3() { return &___magFilter_3; }
	inline void set_magFilter_3(int32_t value)
	{
		___magFilter_3 = value;
	}

	inline static int32_t get_offset_of_uWrap_4() { return static_cast<int32_t>(offsetof(AtlasPage_t3729449121, ___uWrap_4)); }
	inline int32_t get_uWrap_4() const { return ___uWrap_4; }
	inline int32_t* get_address_of_uWrap_4() { return &___uWrap_4; }
	inline void set_uWrap_4(int32_t value)
	{
		___uWrap_4 = value;
	}

	inline static int32_t get_offset_of_vWrap_5() { return static_cast<int32_t>(offsetof(AtlasPage_t3729449121, ___vWrap_5)); }
	inline int32_t get_vWrap_5() const { return ___vWrap_5; }
	inline int32_t* get_address_of_vWrap_5() { return &___vWrap_5; }
	inline void set_vWrap_5(int32_t value)
	{
		___vWrap_5 = value;
	}

	inline static int32_t get_offset_of_rendererObject_6() { return static_cast<int32_t>(offsetof(AtlasPage_t3729449121, ___rendererObject_6)); }
	inline RuntimeObject * get_rendererObject_6() const { return ___rendererObject_6; }
	inline RuntimeObject ** get_address_of_rendererObject_6() { return &___rendererObject_6; }
	inline void set_rendererObject_6(RuntimeObject * value)
	{
		___rendererObject_6 = value;
		Il2CppCodeGenWriteBarrier((&___rendererObject_6), value);
	}

	inline static int32_t get_offset_of_width_7() { return static_cast<int32_t>(offsetof(AtlasPage_t3729449121, ___width_7)); }
	inline int32_t get_width_7() const { return ___width_7; }
	inline int32_t* get_address_of_width_7() { return &___width_7; }
	inline void set_width_7(int32_t value)
	{
		___width_7 = value;
	}

	inline static int32_t get_offset_of_height_8() { return static_cast<int32_t>(offsetof(AtlasPage_t3729449121, ___height_8)); }
	inline int32_t get_height_8() const { return ___height_8; }
	inline int32_t* get_address_of_height_8() { return &___height_8; }
	inline void set_height_8(int32_t value)
	{
		___height_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASPAGE_T3729449121_H
#ifndef BEHAVIOUR_T2905267502_H
#define BEHAVIOUR_T2905267502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2905267502  : public Component_t1852985663
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2905267502_H
#ifndef TRACKENTRYDELEGATE_T1807657294_H
#define TRACKENTRYDELEGATE_T1807657294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState/TrackEntryDelegate
struct  TrackEntryDelegate_t1807657294  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRYDELEGATE_T1807657294_H
#ifndef TRACKENTRYEVENTDELEGATE_T471240152_H
#define TRACKENTRYEVENTDELEGATE_T471240152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState/TrackEntryEventDelegate
struct  TrackEntryEventDelegate_t471240152  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRYEVENTDELEGATE_T471240152_H
#ifndef MONOBEHAVIOUR_T3755042618_H
#define MONOBEHAVIOUR_T3755042618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3755042618  : public Behaviour_t2905267502
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3755042618_H
#ifndef MAINLOGIN_T2109370316_H
#define MAINLOGIN_T2109370316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MainLogin
struct  MainLogin_t2109370316  : public MonoBehaviour_t3755042618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINLOGIN_T2109370316_H
#ifndef MIXANDMATCHGRAPHIC_T1263195094_H
#define MIXANDMATCHGRAPHIC_T1263195094_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.MixAndMatchGraphic
struct  MixAndMatchGraphic_t1263195094  : public MonoBehaviour_t3755042618
{
public:
	// System.String Spine.Unity.Examples.MixAndMatchGraphic::baseSkinName
	String_t* ___baseSkinName_2;
	// UnityEngine.Material Spine.Unity.Examples.MixAndMatchGraphic::sourceMaterial
	Material_t2681358023 * ___sourceMaterial_3;
	// UnityEngine.Sprite Spine.Unity.Examples.MixAndMatchGraphic::visorSprite
	Sprite_t360607379 * ___visorSprite_4;
	// System.String Spine.Unity.Examples.MixAndMatchGraphic::visorSlot
	String_t* ___visorSlot_5;
	// System.String Spine.Unity.Examples.MixAndMatchGraphic::visorKey
	String_t* ___visorKey_6;
	// UnityEngine.Sprite Spine.Unity.Examples.MixAndMatchGraphic::gunSprite
	Sprite_t360607379 * ___gunSprite_7;
	// System.String Spine.Unity.Examples.MixAndMatchGraphic::gunSlot
	String_t* ___gunSlot_8;
	// System.String Spine.Unity.Examples.MixAndMatchGraphic::gunKey
	String_t* ___gunKey_9;
	// System.Boolean Spine.Unity.Examples.MixAndMatchGraphic::repack
	bool ___repack_10;
	// UnityEngine.Texture2D Spine.Unity.Examples.MixAndMatchGraphic::runtimeAtlas
	Texture2D_t1431210461 * ___runtimeAtlas_11;
	// UnityEngine.Material Spine.Unity.Examples.MixAndMatchGraphic::runtimeMaterial
	Material_t2681358023 * ___runtimeMaterial_12;
	// Spine.Skin Spine.Unity.Examples.MixAndMatchGraphic::customSkin
	Skin_t1695237131 * ___customSkin_13;

public:
	inline static int32_t get_offset_of_baseSkinName_2() { return static_cast<int32_t>(offsetof(MixAndMatchGraphic_t1263195094, ___baseSkinName_2)); }
	inline String_t* get_baseSkinName_2() const { return ___baseSkinName_2; }
	inline String_t** get_address_of_baseSkinName_2() { return &___baseSkinName_2; }
	inline void set_baseSkinName_2(String_t* value)
	{
		___baseSkinName_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseSkinName_2), value);
	}

	inline static int32_t get_offset_of_sourceMaterial_3() { return static_cast<int32_t>(offsetof(MixAndMatchGraphic_t1263195094, ___sourceMaterial_3)); }
	inline Material_t2681358023 * get_sourceMaterial_3() const { return ___sourceMaterial_3; }
	inline Material_t2681358023 ** get_address_of_sourceMaterial_3() { return &___sourceMaterial_3; }
	inline void set_sourceMaterial_3(Material_t2681358023 * value)
	{
		___sourceMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___sourceMaterial_3), value);
	}

	inline static int32_t get_offset_of_visorSprite_4() { return static_cast<int32_t>(offsetof(MixAndMatchGraphic_t1263195094, ___visorSprite_4)); }
	inline Sprite_t360607379 * get_visorSprite_4() const { return ___visorSprite_4; }
	inline Sprite_t360607379 ** get_address_of_visorSprite_4() { return &___visorSprite_4; }
	inline void set_visorSprite_4(Sprite_t360607379 * value)
	{
		___visorSprite_4 = value;
		Il2CppCodeGenWriteBarrier((&___visorSprite_4), value);
	}

	inline static int32_t get_offset_of_visorSlot_5() { return static_cast<int32_t>(offsetof(MixAndMatchGraphic_t1263195094, ___visorSlot_5)); }
	inline String_t* get_visorSlot_5() const { return ___visorSlot_5; }
	inline String_t** get_address_of_visorSlot_5() { return &___visorSlot_5; }
	inline void set_visorSlot_5(String_t* value)
	{
		___visorSlot_5 = value;
		Il2CppCodeGenWriteBarrier((&___visorSlot_5), value);
	}

	inline static int32_t get_offset_of_visorKey_6() { return static_cast<int32_t>(offsetof(MixAndMatchGraphic_t1263195094, ___visorKey_6)); }
	inline String_t* get_visorKey_6() const { return ___visorKey_6; }
	inline String_t** get_address_of_visorKey_6() { return &___visorKey_6; }
	inline void set_visorKey_6(String_t* value)
	{
		___visorKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___visorKey_6), value);
	}

	inline static int32_t get_offset_of_gunSprite_7() { return static_cast<int32_t>(offsetof(MixAndMatchGraphic_t1263195094, ___gunSprite_7)); }
	inline Sprite_t360607379 * get_gunSprite_7() const { return ___gunSprite_7; }
	inline Sprite_t360607379 ** get_address_of_gunSprite_7() { return &___gunSprite_7; }
	inline void set_gunSprite_7(Sprite_t360607379 * value)
	{
		___gunSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___gunSprite_7), value);
	}

	inline static int32_t get_offset_of_gunSlot_8() { return static_cast<int32_t>(offsetof(MixAndMatchGraphic_t1263195094, ___gunSlot_8)); }
	inline String_t* get_gunSlot_8() const { return ___gunSlot_8; }
	inline String_t** get_address_of_gunSlot_8() { return &___gunSlot_8; }
	inline void set_gunSlot_8(String_t* value)
	{
		___gunSlot_8 = value;
		Il2CppCodeGenWriteBarrier((&___gunSlot_8), value);
	}

	inline static int32_t get_offset_of_gunKey_9() { return static_cast<int32_t>(offsetof(MixAndMatchGraphic_t1263195094, ___gunKey_9)); }
	inline String_t* get_gunKey_9() const { return ___gunKey_9; }
	inline String_t** get_address_of_gunKey_9() { return &___gunKey_9; }
	inline void set_gunKey_9(String_t* value)
	{
		___gunKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___gunKey_9), value);
	}

	inline static int32_t get_offset_of_repack_10() { return static_cast<int32_t>(offsetof(MixAndMatchGraphic_t1263195094, ___repack_10)); }
	inline bool get_repack_10() const { return ___repack_10; }
	inline bool* get_address_of_repack_10() { return &___repack_10; }
	inline void set_repack_10(bool value)
	{
		___repack_10 = value;
	}

	inline static int32_t get_offset_of_runtimeAtlas_11() { return static_cast<int32_t>(offsetof(MixAndMatchGraphic_t1263195094, ___runtimeAtlas_11)); }
	inline Texture2D_t1431210461 * get_runtimeAtlas_11() const { return ___runtimeAtlas_11; }
	inline Texture2D_t1431210461 ** get_address_of_runtimeAtlas_11() { return &___runtimeAtlas_11; }
	inline void set_runtimeAtlas_11(Texture2D_t1431210461 * value)
	{
		___runtimeAtlas_11 = value;
		Il2CppCodeGenWriteBarrier((&___runtimeAtlas_11), value);
	}

	inline static int32_t get_offset_of_runtimeMaterial_12() { return static_cast<int32_t>(offsetof(MixAndMatchGraphic_t1263195094, ___runtimeMaterial_12)); }
	inline Material_t2681358023 * get_runtimeMaterial_12() const { return ___runtimeMaterial_12; }
	inline Material_t2681358023 ** get_address_of_runtimeMaterial_12() { return &___runtimeMaterial_12; }
	inline void set_runtimeMaterial_12(Material_t2681358023 * value)
	{
		___runtimeMaterial_12 = value;
		Il2CppCodeGenWriteBarrier((&___runtimeMaterial_12), value);
	}

	inline static int32_t get_offset_of_customSkin_13() { return static_cast<int32_t>(offsetof(MixAndMatchGraphic_t1263195094, ___customSkin_13)); }
	inline Skin_t1695237131 * get_customSkin_13() const { return ___customSkin_13; }
	inline Skin_t1695237131 ** get_address_of_customSkin_13() { return &___customSkin_13; }
	inline void set_customSkin_13(Skin_t1695237131 * value)
	{
		___customSkin_13 = value;
		Il2CppCodeGenWriteBarrier((&___customSkin_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXANDMATCHGRAPHIC_T1263195094_H
#ifndef RAGGEDYSPINEBOY_T30683967_H
#define RAGGEDYSPINEBOY_T30683967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.RaggedySpineboy
struct  RaggedySpineboy_t30683967  : public MonoBehaviour_t3755042618
{
public:
	// UnityEngine.LayerMask Spine.Unity.Examples.RaggedySpineboy::groundMask
	LayerMask_t3306677380  ___groundMask_2;
	// System.Single Spine.Unity.Examples.RaggedySpineboy::restoreDuration
	float ___restoreDuration_3;
	// UnityEngine.Vector2 Spine.Unity.Examples.RaggedySpineboy::launchVelocity
	Vector2_t1065050092  ___launchVelocity_4;
	// Spine.Unity.Modules.SkeletonRagdoll2D Spine.Unity.Examples.RaggedySpineboy::ragdoll
	SkeletonRagdoll2D_t3940222091 * ___ragdoll_5;
	// UnityEngine.Collider2D Spine.Unity.Examples.RaggedySpineboy::naturalCollider
	Collider2D_t179611260 * ___naturalCollider_6;

public:
	inline static int32_t get_offset_of_groundMask_2() { return static_cast<int32_t>(offsetof(RaggedySpineboy_t30683967, ___groundMask_2)); }
	inline LayerMask_t3306677380  get_groundMask_2() const { return ___groundMask_2; }
	inline LayerMask_t3306677380 * get_address_of_groundMask_2() { return &___groundMask_2; }
	inline void set_groundMask_2(LayerMask_t3306677380  value)
	{
		___groundMask_2 = value;
	}

	inline static int32_t get_offset_of_restoreDuration_3() { return static_cast<int32_t>(offsetof(RaggedySpineboy_t30683967, ___restoreDuration_3)); }
	inline float get_restoreDuration_3() const { return ___restoreDuration_3; }
	inline float* get_address_of_restoreDuration_3() { return &___restoreDuration_3; }
	inline void set_restoreDuration_3(float value)
	{
		___restoreDuration_3 = value;
	}

	inline static int32_t get_offset_of_launchVelocity_4() { return static_cast<int32_t>(offsetof(RaggedySpineboy_t30683967, ___launchVelocity_4)); }
	inline Vector2_t1065050092  get_launchVelocity_4() const { return ___launchVelocity_4; }
	inline Vector2_t1065050092 * get_address_of_launchVelocity_4() { return &___launchVelocity_4; }
	inline void set_launchVelocity_4(Vector2_t1065050092  value)
	{
		___launchVelocity_4 = value;
	}

	inline static int32_t get_offset_of_ragdoll_5() { return static_cast<int32_t>(offsetof(RaggedySpineboy_t30683967, ___ragdoll_5)); }
	inline SkeletonRagdoll2D_t3940222091 * get_ragdoll_5() const { return ___ragdoll_5; }
	inline SkeletonRagdoll2D_t3940222091 ** get_address_of_ragdoll_5() { return &___ragdoll_5; }
	inline void set_ragdoll_5(SkeletonRagdoll2D_t3940222091 * value)
	{
		___ragdoll_5 = value;
		Il2CppCodeGenWriteBarrier((&___ragdoll_5), value);
	}

	inline static int32_t get_offset_of_naturalCollider_6() { return static_cast<int32_t>(offsetof(RaggedySpineboy_t30683967, ___naturalCollider_6)); }
	inline Collider2D_t179611260 * get_naturalCollider_6() const { return ___naturalCollider_6; }
	inline Collider2D_t179611260 ** get_address_of_naturalCollider_6() { return &___naturalCollider_6; }
	inline void set_naturalCollider_6(Collider2D_t179611260 * value)
	{
		___naturalCollider_6 = value;
		Il2CppCodeGenWriteBarrier((&___naturalCollider_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAGGEDYSPINEBOY_T30683967_H
#ifndef ROTATOR_T2221798354_H
#define ROTATOR_T2221798354_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.Rotator
struct  Rotator_t2221798354  : public MonoBehaviour_t3755042618
{
public:
	// UnityEngine.Vector3 Spine.Unity.Examples.Rotator::direction
	Vector3_t2825674791  ___direction_2;
	// System.Single Spine.Unity.Examples.Rotator::speed
	float ___speed_3;

public:
	inline static int32_t get_offset_of_direction_2() { return static_cast<int32_t>(offsetof(Rotator_t2221798354, ___direction_2)); }
	inline Vector3_t2825674791  get_direction_2() const { return ___direction_2; }
	inline Vector3_t2825674791 * get_address_of_direction_2() { return &___direction_2; }
	inline void set_direction_2(Vector3_t2825674791  value)
	{
		___direction_2 = value;
	}

	inline static int32_t get_offset_of_speed_3() { return static_cast<int32_t>(offsetof(Rotator_t2221798354, ___speed_3)); }
	inline float get_speed_3() const { return ___speed_3; }
	inline float* get_address_of_speed_3() { return &___speed_3; }
	inline void set_speed_3(float value)
	{
		___speed_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATOR_T2221798354_H
#ifndef BONELOCALOVERRIDE_T2136445371_H
#define BONELOCALOVERRIDE_T2136445371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.BoneLocalOverride
struct  BoneLocalOverride_t2136445371  : public MonoBehaviour_t3755042618
{
public:
	// System.String Spine.Unity.Examples.BoneLocalOverride::boneName
	String_t* ___boneName_2;
	// System.Single Spine.Unity.Examples.BoneLocalOverride::alpha
	float ___alpha_3;
	// System.Boolean Spine.Unity.Examples.BoneLocalOverride::overridePosition
	bool ___overridePosition_4;
	// UnityEngine.Vector2 Spine.Unity.Examples.BoneLocalOverride::localPosition
	Vector2_t1065050092  ___localPosition_5;
	// System.Boolean Spine.Unity.Examples.BoneLocalOverride::overrideRotation
	bool ___overrideRotation_6;
	// System.Single Spine.Unity.Examples.BoneLocalOverride::rotation
	float ___rotation_7;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.Examples.BoneLocalOverride::spineComponent
	RuntimeObject* ___spineComponent_8;
	// Spine.Bone Spine.Unity.Examples.BoneLocalOverride::bone
	Bone_t1763862836 * ___bone_9;

public:
	inline static int32_t get_offset_of_boneName_2() { return static_cast<int32_t>(offsetof(BoneLocalOverride_t2136445371, ___boneName_2)); }
	inline String_t* get_boneName_2() const { return ___boneName_2; }
	inline String_t** get_address_of_boneName_2() { return &___boneName_2; }
	inline void set_boneName_2(String_t* value)
	{
		___boneName_2 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_2), value);
	}

	inline static int32_t get_offset_of_alpha_3() { return static_cast<int32_t>(offsetof(BoneLocalOverride_t2136445371, ___alpha_3)); }
	inline float get_alpha_3() const { return ___alpha_3; }
	inline float* get_address_of_alpha_3() { return &___alpha_3; }
	inline void set_alpha_3(float value)
	{
		___alpha_3 = value;
	}

	inline static int32_t get_offset_of_overridePosition_4() { return static_cast<int32_t>(offsetof(BoneLocalOverride_t2136445371, ___overridePosition_4)); }
	inline bool get_overridePosition_4() const { return ___overridePosition_4; }
	inline bool* get_address_of_overridePosition_4() { return &___overridePosition_4; }
	inline void set_overridePosition_4(bool value)
	{
		___overridePosition_4 = value;
	}

	inline static int32_t get_offset_of_localPosition_5() { return static_cast<int32_t>(offsetof(BoneLocalOverride_t2136445371, ___localPosition_5)); }
	inline Vector2_t1065050092  get_localPosition_5() const { return ___localPosition_5; }
	inline Vector2_t1065050092 * get_address_of_localPosition_5() { return &___localPosition_5; }
	inline void set_localPosition_5(Vector2_t1065050092  value)
	{
		___localPosition_5 = value;
	}

	inline static int32_t get_offset_of_overrideRotation_6() { return static_cast<int32_t>(offsetof(BoneLocalOverride_t2136445371, ___overrideRotation_6)); }
	inline bool get_overrideRotation_6() const { return ___overrideRotation_6; }
	inline bool* get_address_of_overrideRotation_6() { return &___overrideRotation_6; }
	inline void set_overrideRotation_6(bool value)
	{
		___overrideRotation_6 = value;
	}

	inline static int32_t get_offset_of_rotation_7() { return static_cast<int32_t>(offsetof(BoneLocalOverride_t2136445371, ___rotation_7)); }
	inline float get_rotation_7() const { return ___rotation_7; }
	inline float* get_address_of_rotation_7() { return &___rotation_7; }
	inline void set_rotation_7(float value)
	{
		___rotation_7 = value;
	}

	inline static int32_t get_offset_of_spineComponent_8() { return static_cast<int32_t>(offsetof(BoneLocalOverride_t2136445371, ___spineComponent_8)); }
	inline RuntimeObject* get_spineComponent_8() const { return ___spineComponent_8; }
	inline RuntimeObject** get_address_of_spineComponent_8() { return &___spineComponent_8; }
	inline void set_spineComponent_8(RuntimeObject* value)
	{
		___spineComponent_8 = value;
		Il2CppCodeGenWriteBarrier((&___spineComponent_8), value);
	}

	inline static int32_t get_offset_of_bone_9() { return static_cast<int32_t>(offsetof(BoneLocalOverride_t2136445371, ___bone_9)); }
	inline Bone_t1763862836 * get_bone_9() const { return ___bone_9; }
	inline Bone_t1763862836 ** get_address_of_bone_9() { return &___bone_9; }
	inline void set_bone_9(Bone_t1763862836 * value)
	{
		___bone_9 = value;
		Il2CppCodeGenWriteBarrier((&___bone_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONELOCALOVERRIDE_T2136445371_H
#ifndef COMBINEDSKIN_T4123893441_H
#define COMBINEDSKIN_T4123893441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.CombinedSkin
struct  CombinedSkin_t4123893441  : public MonoBehaviour_t3755042618
{
public:
	// System.Collections.Generic.List`1<System.String> Spine.Unity.Examples.CombinedSkin::skinsToCombine
	List_1_t3668794496 * ___skinsToCombine_2;
	// Spine.Skin Spine.Unity.Examples.CombinedSkin::combinedSkin
	Skin_t1695237131 * ___combinedSkin_3;

public:
	inline static int32_t get_offset_of_skinsToCombine_2() { return static_cast<int32_t>(offsetof(CombinedSkin_t4123893441, ___skinsToCombine_2)); }
	inline List_1_t3668794496 * get_skinsToCombine_2() const { return ___skinsToCombine_2; }
	inline List_1_t3668794496 ** get_address_of_skinsToCombine_2() { return &___skinsToCombine_2; }
	inline void set_skinsToCombine_2(List_1_t3668794496 * value)
	{
		___skinsToCombine_2 = value;
		Il2CppCodeGenWriteBarrier((&___skinsToCombine_2), value);
	}

	inline static int32_t get_offset_of_combinedSkin_3() { return static_cast<int32_t>(offsetof(CombinedSkin_t4123893441, ___combinedSkin_3)); }
	inline Skin_t1695237131 * get_combinedSkin_3() const { return ___combinedSkin_3; }
	inline Skin_t1695237131 ** get_address_of_combinedSkin_3() { return &___combinedSkin_3; }
	inline void set_combinedSkin_3(Skin_t1695237131 * value)
	{
		___combinedSkin_3 = value;
		Il2CppCodeGenWriteBarrier((&___combinedSkin_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMBINEDSKIN_T4123893441_H
#ifndef ATLASREGIONATTACHER_T437517080_H
#define ATLASREGIONATTACHER_T437517080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AtlasRegionAttacher
struct  AtlasRegionAttacher_t437517080  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.AtlasAsset Spine.Unity.Modules.AtlasRegionAttacher::atlasAsset
	AtlasAsset_t2525633250 * ___atlasAsset_2;
	// System.Boolean Spine.Unity.Modules.AtlasRegionAttacher::inheritProperties
	bool ___inheritProperties_3;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.AtlasRegionAttacher/SlotRegionPair> Spine.Unity.Modules.AtlasRegionAttacher::attachments
	List_1_t93758473 * ___attachments_4;
	// Spine.Atlas Spine.Unity.Modules.AtlasRegionAttacher::atlas
	Atlas_t604244430 * ___atlas_5;

public:
	inline static int32_t get_offset_of_atlasAsset_2() { return static_cast<int32_t>(offsetof(AtlasRegionAttacher_t437517080, ___atlasAsset_2)); }
	inline AtlasAsset_t2525633250 * get_atlasAsset_2() const { return ___atlasAsset_2; }
	inline AtlasAsset_t2525633250 ** get_address_of_atlasAsset_2() { return &___atlasAsset_2; }
	inline void set_atlasAsset_2(AtlasAsset_t2525633250 * value)
	{
		___atlasAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAsset_2), value);
	}

	inline static int32_t get_offset_of_inheritProperties_3() { return static_cast<int32_t>(offsetof(AtlasRegionAttacher_t437517080, ___inheritProperties_3)); }
	inline bool get_inheritProperties_3() const { return ___inheritProperties_3; }
	inline bool* get_address_of_inheritProperties_3() { return &___inheritProperties_3; }
	inline void set_inheritProperties_3(bool value)
	{
		___inheritProperties_3 = value;
	}

	inline static int32_t get_offset_of_attachments_4() { return static_cast<int32_t>(offsetof(AtlasRegionAttacher_t437517080, ___attachments_4)); }
	inline List_1_t93758473 * get_attachments_4() const { return ___attachments_4; }
	inline List_1_t93758473 ** get_address_of_attachments_4() { return &___attachments_4; }
	inline void set_attachments_4(List_1_t93758473 * value)
	{
		___attachments_4 = value;
		Il2CppCodeGenWriteBarrier((&___attachments_4), value);
	}

	inline static int32_t get_offset_of_atlas_5() { return static_cast<int32_t>(offsetof(AtlasRegionAttacher_t437517080, ___atlas_5)); }
	inline Atlas_t604244430 * get_atlas_5() const { return ___atlas_5; }
	inline Atlas_t604244430 ** get_address_of_atlas_5() { return &___atlas_5; }
	inline void set_atlas_5(Atlas_t604244430 * value)
	{
		___atlas_5 = value;
		Il2CppCodeGenWriteBarrier((&___atlas_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASREGIONATTACHER_T437517080_H
#ifndef SPRITEATTACHER_T2181857634_H
#define SPRITEATTACHER_T2181857634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SpriteAttacher
struct  SpriteAttacher_t2181857634  : public MonoBehaviour_t3755042618
{
public:
	// System.Boolean Spine.Unity.Modules.SpriteAttacher::attachOnStart
	bool ___attachOnStart_4;
	// System.Boolean Spine.Unity.Modules.SpriteAttacher::overrideAnimation
	bool ___overrideAnimation_5;
	// UnityEngine.Sprite Spine.Unity.Modules.SpriteAttacher::sprite
	Sprite_t360607379 * ___sprite_6;
	// System.String Spine.Unity.Modules.SpriteAttacher::slot
	String_t* ___slot_7;
	// Spine.RegionAttachment Spine.Unity.Modules.SpriteAttacher::attachment
	RegionAttachment_t1431410254 * ___attachment_8;
	// Spine.Slot Spine.Unity.Modules.SpriteAttacher::spineSlot
	Slot_t1804116343 * ___spineSlot_9;
	// System.Boolean Spine.Unity.Modules.SpriteAttacher::applyPMA
	bool ___applyPMA_10;

public:
	inline static int32_t get_offset_of_attachOnStart_4() { return static_cast<int32_t>(offsetof(SpriteAttacher_t2181857634, ___attachOnStart_4)); }
	inline bool get_attachOnStart_4() const { return ___attachOnStart_4; }
	inline bool* get_address_of_attachOnStart_4() { return &___attachOnStart_4; }
	inline void set_attachOnStart_4(bool value)
	{
		___attachOnStart_4 = value;
	}

	inline static int32_t get_offset_of_overrideAnimation_5() { return static_cast<int32_t>(offsetof(SpriteAttacher_t2181857634, ___overrideAnimation_5)); }
	inline bool get_overrideAnimation_5() const { return ___overrideAnimation_5; }
	inline bool* get_address_of_overrideAnimation_5() { return &___overrideAnimation_5; }
	inline void set_overrideAnimation_5(bool value)
	{
		___overrideAnimation_5 = value;
	}

	inline static int32_t get_offset_of_sprite_6() { return static_cast<int32_t>(offsetof(SpriteAttacher_t2181857634, ___sprite_6)); }
	inline Sprite_t360607379 * get_sprite_6() const { return ___sprite_6; }
	inline Sprite_t360607379 ** get_address_of_sprite_6() { return &___sprite_6; }
	inline void set_sprite_6(Sprite_t360607379 * value)
	{
		___sprite_6 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_6), value);
	}

	inline static int32_t get_offset_of_slot_7() { return static_cast<int32_t>(offsetof(SpriteAttacher_t2181857634, ___slot_7)); }
	inline String_t* get_slot_7() const { return ___slot_7; }
	inline String_t** get_address_of_slot_7() { return &___slot_7; }
	inline void set_slot_7(String_t* value)
	{
		___slot_7 = value;
		Il2CppCodeGenWriteBarrier((&___slot_7), value);
	}

	inline static int32_t get_offset_of_attachment_8() { return static_cast<int32_t>(offsetof(SpriteAttacher_t2181857634, ___attachment_8)); }
	inline RegionAttachment_t1431410254 * get_attachment_8() const { return ___attachment_8; }
	inline RegionAttachment_t1431410254 ** get_address_of_attachment_8() { return &___attachment_8; }
	inline void set_attachment_8(RegionAttachment_t1431410254 * value)
	{
		___attachment_8 = value;
		Il2CppCodeGenWriteBarrier((&___attachment_8), value);
	}

	inline static int32_t get_offset_of_spineSlot_9() { return static_cast<int32_t>(offsetof(SpriteAttacher_t2181857634, ___spineSlot_9)); }
	inline Slot_t1804116343 * get_spineSlot_9() const { return ___spineSlot_9; }
	inline Slot_t1804116343 ** get_address_of_spineSlot_9() { return &___spineSlot_9; }
	inline void set_spineSlot_9(Slot_t1804116343 * value)
	{
		___spineSlot_9 = value;
		Il2CppCodeGenWriteBarrier((&___spineSlot_9), value);
	}

	inline static int32_t get_offset_of_applyPMA_10() { return static_cast<int32_t>(offsetof(SpriteAttacher_t2181857634, ___applyPMA_10)); }
	inline bool get_applyPMA_10() const { return ___applyPMA_10; }
	inline bool* get_address_of_applyPMA_10() { return &___applyPMA_10; }
	inline void set_applyPMA_10(bool value)
	{
		___applyPMA_10 = value;
	}
};

struct SpriteAttacher_t2181857634_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.Texture,Spine.AtlasPage> Spine.Unity.Modules.SpriteAttacher::atlasPageCache
	Dictionary_2_t2822557095 * ___atlasPageCache_11;

public:
	inline static int32_t get_offset_of_atlasPageCache_11() { return static_cast<int32_t>(offsetof(SpriteAttacher_t2181857634_StaticFields, ___atlasPageCache_11)); }
	inline Dictionary_2_t2822557095 * get_atlasPageCache_11() const { return ___atlasPageCache_11; }
	inline Dictionary_2_t2822557095 ** get_address_of_atlasPageCache_11() { return &___atlasPageCache_11; }
	inline void set_atlasPageCache_11(Dictionary_2_t2822557095 * value)
	{
		___atlasPageCache_11 = value;
		Il2CppCodeGenWriteBarrier((&___atlasPageCache_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEATTACHER_T2181857634_H
#ifndef JITTEREFFECTEXAMPLE_T518848748_H
#define JITTEREFFECTEXAMPLE_T518848748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.JitterEffectExample
struct  JitterEffectExample_t518848748  : public MonoBehaviour_t3755042618
{
public:
	// System.Single Spine.Unity.Examples.JitterEffectExample::jitterMagnitude
	float ___jitterMagnitude_2;
	// Spine.Unity.SkeletonRenderer Spine.Unity.Examples.JitterEffectExample::skeletonRenderer
	SkeletonRenderer_t1681389628 * ___skeletonRenderer_3;

public:
	inline static int32_t get_offset_of_jitterMagnitude_2() { return static_cast<int32_t>(offsetof(JitterEffectExample_t518848748, ___jitterMagnitude_2)); }
	inline float get_jitterMagnitude_2() const { return ___jitterMagnitude_2; }
	inline float* get_address_of_jitterMagnitude_2() { return &___jitterMagnitude_2; }
	inline void set_jitterMagnitude_2(float value)
	{
		___jitterMagnitude_2 = value;
	}

	inline static int32_t get_offset_of_skeletonRenderer_3() { return static_cast<int32_t>(offsetof(JitterEffectExample_t518848748, ___skeletonRenderer_3)); }
	inline SkeletonRenderer_t1681389628 * get_skeletonRenderer_3() const { return ___skeletonRenderer_3; }
	inline SkeletonRenderer_t1681389628 ** get_address_of_skeletonRenderer_3() { return &___skeletonRenderer_3; }
	inline void set_skeletonRenderer_3(SkeletonRenderer_t1681389628 * value)
	{
		___skeletonRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JITTEREFFECTEXAMPLE_T518848748_H
#ifndef SKELETONCOLORINITIALIZE_T1957958085_H
#define SKELETONCOLORINITIALIZE_T1957958085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.SkeletonColorInitialize
struct  SkeletonColorInitialize_t1957958085  : public MonoBehaviour_t3755042618
{
public:
	// UnityEngine.Color Spine.Unity.Examples.SkeletonColorInitialize::skeletonColor
	Color_t4183816058  ___skeletonColor_2;
	// System.Collections.Generic.List`1<Spine.Unity.Examples.SkeletonColorInitialize/SlotSettings> Spine.Unity.Examples.SkeletonColorInitialize::slotSettings
	List_1_t979554255 * ___slotSettings_3;

public:
	inline static int32_t get_offset_of_skeletonColor_2() { return static_cast<int32_t>(offsetof(SkeletonColorInitialize_t1957958085, ___skeletonColor_2)); }
	inline Color_t4183816058  get_skeletonColor_2() const { return ___skeletonColor_2; }
	inline Color_t4183816058 * get_address_of_skeletonColor_2() { return &___skeletonColor_2; }
	inline void set_skeletonColor_2(Color_t4183816058  value)
	{
		___skeletonColor_2 = value;
	}

	inline static int32_t get_offset_of_slotSettings_3() { return static_cast<int32_t>(offsetof(SkeletonColorInitialize_t1957958085, ___slotSettings_3)); }
	inline List_1_t979554255 * get_slotSettings_3() const { return ___slotSettings_3; }
	inline List_1_t979554255 ** get_address_of_slotSettings_3() { return &___slotSettings_3; }
	inline void set_slotSettings_3(List_1_t979554255 * value)
	{
		___slotSettings_3 = value;
		Il2CppCodeGenWriteBarrier((&___slotSettings_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONCOLORINITIALIZE_T1957958085_H
#ifndef SLOTTINTBLACKFOLLOWER_T719666413_H
#define SLOTTINTBLACKFOLLOWER_T719666413_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.SlotTintBlackFollower
struct  SlotTintBlackFollower_t719666413  : public MonoBehaviour_t3755042618
{
public:
	// System.String Spine.Unity.Examples.SlotTintBlackFollower::slotName
	String_t* ___slotName_2;
	// System.String Spine.Unity.Examples.SlotTintBlackFollower::colorPropertyName
	String_t* ___colorPropertyName_3;
	// System.String Spine.Unity.Examples.SlotTintBlackFollower::blackPropertyName
	String_t* ___blackPropertyName_4;
	// Spine.Slot Spine.Unity.Examples.SlotTintBlackFollower::slot
	Slot_t1804116343 * ___slot_5;
	// UnityEngine.MeshRenderer Spine.Unity.Examples.SlotTintBlackFollower::mr
	MeshRenderer_t1621355309 * ___mr_6;
	// UnityEngine.MaterialPropertyBlock Spine.Unity.Examples.SlotTintBlackFollower::mb
	MaterialPropertyBlock_t2982817174 * ___mb_7;
	// System.Int32 Spine.Unity.Examples.SlotTintBlackFollower::colorPropertyId
	int32_t ___colorPropertyId_8;
	// System.Int32 Spine.Unity.Examples.SlotTintBlackFollower::blackPropertyId
	int32_t ___blackPropertyId_9;

public:
	inline static int32_t get_offset_of_slotName_2() { return static_cast<int32_t>(offsetof(SlotTintBlackFollower_t719666413, ___slotName_2)); }
	inline String_t* get_slotName_2() const { return ___slotName_2; }
	inline String_t** get_address_of_slotName_2() { return &___slotName_2; }
	inline void set_slotName_2(String_t* value)
	{
		___slotName_2 = value;
		Il2CppCodeGenWriteBarrier((&___slotName_2), value);
	}

	inline static int32_t get_offset_of_colorPropertyName_3() { return static_cast<int32_t>(offsetof(SlotTintBlackFollower_t719666413, ___colorPropertyName_3)); }
	inline String_t* get_colorPropertyName_3() const { return ___colorPropertyName_3; }
	inline String_t** get_address_of_colorPropertyName_3() { return &___colorPropertyName_3; }
	inline void set_colorPropertyName_3(String_t* value)
	{
		___colorPropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___colorPropertyName_3), value);
	}

	inline static int32_t get_offset_of_blackPropertyName_4() { return static_cast<int32_t>(offsetof(SlotTintBlackFollower_t719666413, ___blackPropertyName_4)); }
	inline String_t* get_blackPropertyName_4() const { return ___blackPropertyName_4; }
	inline String_t** get_address_of_blackPropertyName_4() { return &___blackPropertyName_4; }
	inline void set_blackPropertyName_4(String_t* value)
	{
		___blackPropertyName_4 = value;
		Il2CppCodeGenWriteBarrier((&___blackPropertyName_4), value);
	}

	inline static int32_t get_offset_of_slot_5() { return static_cast<int32_t>(offsetof(SlotTintBlackFollower_t719666413, ___slot_5)); }
	inline Slot_t1804116343 * get_slot_5() const { return ___slot_5; }
	inline Slot_t1804116343 ** get_address_of_slot_5() { return &___slot_5; }
	inline void set_slot_5(Slot_t1804116343 * value)
	{
		___slot_5 = value;
		Il2CppCodeGenWriteBarrier((&___slot_5), value);
	}

	inline static int32_t get_offset_of_mr_6() { return static_cast<int32_t>(offsetof(SlotTintBlackFollower_t719666413, ___mr_6)); }
	inline MeshRenderer_t1621355309 * get_mr_6() const { return ___mr_6; }
	inline MeshRenderer_t1621355309 ** get_address_of_mr_6() { return &___mr_6; }
	inline void set_mr_6(MeshRenderer_t1621355309 * value)
	{
		___mr_6 = value;
		Il2CppCodeGenWriteBarrier((&___mr_6), value);
	}

	inline static int32_t get_offset_of_mb_7() { return static_cast<int32_t>(offsetof(SlotTintBlackFollower_t719666413, ___mb_7)); }
	inline MaterialPropertyBlock_t2982817174 * get_mb_7() const { return ___mb_7; }
	inline MaterialPropertyBlock_t2982817174 ** get_address_of_mb_7() { return &___mb_7; }
	inline void set_mb_7(MaterialPropertyBlock_t2982817174 * value)
	{
		___mb_7 = value;
		Il2CppCodeGenWriteBarrier((&___mb_7), value);
	}

	inline static int32_t get_offset_of_colorPropertyId_8() { return static_cast<int32_t>(offsetof(SlotTintBlackFollower_t719666413, ___colorPropertyId_8)); }
	inline int32_t get_colorPropertyId_8() const { return ___colorPropertyId_8; }
	inline int32_t* get_address_of_colorPropertyId_8() { return &___colorPropertyId_8; }
	inline void set_colorPropertyId_8(int32_t value)
	{
		___colorPropertyId_8 = value;
	}

	inline static int32_t get_offset_of_blackPropertyId_9() { return static_cast<int32_t>(offsetof(SlotTintBlackFollower_t719666413, ___blackPropertyId_9)); }
	inline int32_t get_blackPropertyId_9() const { return ___blackPropertyId_9; }
	inline int32_t* get_address_of_blackPropertyId_9() { return &___blackPropertyId_9; }
	inline void set_blackPropertyId_9(int32_t value)
	{
		___blackPropertyId_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTTINTBLACKFOLLOWER_T719666413_H
#ifndef SPINEEVENTUNITYHANDLER_T3250816684_H
#define SPINEEVENTUNITYHANDLER_T3250816684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SpineEventUnityHandler
struct  SpineEventUnityHandler_t3250816684  : public MonoBehaviour_t3755042618
{
public:
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SpineEventUnityHandler/EventPair> Spine.Unity.Modules.SpineEventUnityHandler::events
	List_1_t3483316028 * ___events_2;
	// Spine.Unity.ISkeletonComponent Spine.Unity.Modules.SpineEventUnityHandler::skeletonComponent
	RuntimeObject* ___skeletonComponent_3;
	// Spine.Unity.IAnimationStateComponent Spine.Unity.Modules.SpineEventUnityHandler::animationStateComponent
	RuntimeObject* ___animationStateComponent_4;

public:
	inline static int32_t get_offset_of_events_2() { return static_cast<int32_t>(offsetof(SpineEventUnityHandler_t3250816684, ___events_2)); }
	inline List_1_t3483316028 * get_events_2() const { return ___events_2; }
	inline List_1_t3483316028 ** get_address_of_events_2() { return &___events_2; }
	inline void set_events_2(List_1_t3483316028 * value)
	{
		___events_2 = value;
		Il2CppCodeGenWriteBarrier((&___events_2), value);
	}

	inline static int32_t get_offset_of_skeletonComponent_3() { return static_cast<int32_t>(offsetof(SpineEventUnityHandler_t3250816684, ___skeletonComponent_3)); }
	inline RuntimeObject* get_skeletonComponent_3() const { return ___skeletonComponent_3; }
	inline RuntimeObject** get_address_of_skeletonComponent_3() { return &___skeletonComponent_3; }
	inline void set_skeletonComponent_3(RuntimeObject* value)
	{
		___skeletonComponent_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonComponent_3), value);
	}

	inline static int32_t get_offset_of_animationStateComponent_4() { return static_cast<int32_t>(offsetof(SpineEventUnityHandler_t3250816684, ___animationStateComponent_4)); }
	inline RuntimeObject* get_animationStateComponent_4() const { return ___animationStateComponent_4; }
	inline RuntimeObject** get_address_of_animationStateComponent_4() { return &___animationStateComponent_4; }
	inline void set_animationStateComponent_4(RuntimeObject* value)
	{
		___animationStateComponent_4 = value;
		Il2CppCodeGenWriteBarrier((&___animationStateComponent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEEVENTUNITYHANDLER_T3250816684_H
#ifndef SPAWNFROMSKELETONDATAEXAMPLE_T3296636424_H
#define SPAWNFROMSKELETONDATAEXAMPLE_T3296636424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.SpawnFromSkeletonDataExample
struct  SpawnFromSkeletonDataExample_t3296636424  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.SkeletonDataAsset Spine.Unity.Examples.SpawnFromSkeletonDataExample::skeletonDataAsset
	SkeletonDataAsset_t1706375087 * ___skeletonDataAsset_2;
	// System.Int32 Spine.Unity.Examples.SpawnFromSkeletonDataExample::count
	int32_t ___count_3;
	// System.String Spine.Unity.Examples.SpawnFromSkeletonDataExample::startingAnimation
	String_t* ___startingAnimation_4;

public:
	inline static int32_t get_offset_of_skeletonDataAsset_2() { return static_cast<int32_t>(offsetof(SpawnFromSkeletonDataExample_t3296636424, ___skeletonDataAsset_2)); }
	inline SkeletonDataAsset_t1706375087 * get_skeletonDataAsset_2() const { return ___skeletonDataAsset_2; }
	inline SkeletonDataAsset_t1706375087 ** get_address_of_skeletonDataAsset_2() { return &___skeletonDataAsset_2; }
	inline void set_skeletonDataAsset_2(SkeletonDataAsset_t1706375087 * value)
	{
		___skeletonDataAsset_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_2), value);
	}

	inline static int32_t get_offset_of_count_3() { return static_cast<int32_t>(offsetof(SpawnFromSkeletonDataExample_t3296636424, ___count_3)); }
	inline int32_t get_count_3() const { return ___count_3; }
	inline int32_t* get_address_of_count_3() { return &___count_3; }
	inline void set_count_3(int32_t value)
	{
		___count_3 = value;
	}

	inline static int32_t get_offset_of_startingAnimation_4() { return static_cast<int32_t>(offsetof(SpawnFromSkeletonDataExample_t3296636424, ___startingAnimation_4)); }
	inline String_t* get_startingAnimation_4() const { return ___startingAnimation_4; }
	inline String_t** get_address_of_startingAnimation_4() { return &___startingAnimation_4; }
	inline void set_startingAnimation_4(String_t* value)
	{
		___startingAnimation_4 = value;
		Il2CppCodeGenWriteBarrier((&___startingAnimation_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPAWNFROMSKELETONDATAEXAMPLE_T3296636424_H
#ifndef SPINEBOY_T3944353744_H
#define SPINEBOY_T3944353744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.Spineboy
struct  Spineboy_t3944353744  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.SkeletonAnimation Spine.Unity.Examples.Spineboy::skeletonAnimation
	SkeletonAnimation_t2012766265 * ___skeletonAnimation_2;

public:
	inline static int32_t get_offset_of_skeletonAnimation_2() { return static_cast<int32_t>(offsetof(Spineboy_t3944353744, ___skeletonAnimation_2)); }
	inline SkeletonAnimation_t2012766265 * get_skeletonAnimation_2() const { return ___skeletonAnimation_2; }
	inline SkeletonAnimation_t2012766265 ** get_address_of_skeletonAnimation_2() { return &___skeletonAnimation_2; }
	inline void set_skeletonAnimation_2(SkeletonAnimation_t2012766265 * value)
	{
		___skeletonAnimation_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonAnimation_2), value);
	}
};

struct Spineboy_t3944353744_StaticFields
{
public:
	// Spine.AnimationState/TrackEntryDelegate Spine.Unity.Examples.Spineboy::<>f__am$cache0
	TrackEntryDelegate_t1807657294 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(Spineboy_t3944353744_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline TrackEntryDelegate_t1807657294 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(TrackEntryDelegate_t1807657294 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEBOY_T3944353744_H
#ifndef SPINEBOYPOLE_T3969514969_H
#define SPINEBOYPOLE_T3969514969_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.SpineboyPole
struct  SpineboyPole_t3969514969  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.SkeletonAnimation Spine.Unity.Examples.SpineboyPole::skeletonAnimation
	SkeletonAnimation_t2012766265 * ___skeletonAnimation_2;
	// Spine.Unity.Modules.SkeletonRenderSeparator Spine.Unity.Examples.SpineboyPole::separator
	SkeletonRenderSeparator_t3391021243 * ___separator_3;
	// System.String Spine.Unity.Examples.SpineboyPole::run
	String_t* ___run_4;
	// System.String Spine.Unity.Examples.SpineboyPole::pole
	String_t* ___pole_5;
	// System.Single Spine.Unity.Examples.SpineboyPole::startX
	float ___startX_6;
	// System.Single Spine.Unity.Examples.SpineboyPole::endX
	float ___endX_7;

public:
	inline static int32_t get_offset_of_skeletonAnimation_2() { return static_cast<int32_t>(offsetof(SpineboyPole_t3969514969, ___skeletonAnimation_2)); }
	inline SkeletonAnimation_t2012766265 * get_skeletonAnimation_2() const { return ___skeletonAnimation_2; }
	inline SkeletonAnimation_t2012766265 ** get_address_of_skeletonAnimation_2() { return &___skeletonAnimation_2; }
	inline void set_skeletonAnimation_2(SkeletonAnimation_t2012766265 * value)
	{
		___skeletonAnimation_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonAnimation_2), value);
	}

	inline static int32_t get_offset_of_separator_3() { return static_cast<int32_t>(offsetof(SpineboyPole_t3969514969, ___separator_3)); }
	inline SkeletonRenderSeparator_t3391021243 * get_separator_3() const { return ___separator_3; }
	inline SkeletonRenderSeparator_t3391021243 ** get_address_of_separator_3() { return &___separator_3; }
	inline void set_separator_3(SkeletonRenderSeparator_t3391021243 * value)
	{
		___separator_3 = value;
		Il2CppCodeGenWriteBarrier((&___separator_3), value);
	}

	inline static int32_t get_offset_of_run_4() { return static_cast<int32_t>(offsetof(SpineboyPole_t3969514969, ___run_4)); }
	inline String_t* get_run_4() const { return ___run_4; }
	inline String_t** get_address_of_run_4() { return &___run_4; }
	inline void set_run_4(String_t* value)
	{
		___run_4 = value;
		Il2CppCodeGenWriteBarrier((&___run_4), value);
	}

	inline static int32_t get_offset_of_pole_5() { return static_cast<int32_t>(offsetof(SpineboyPole_t3969514969, ___pole_5)); }
	inline String_t* get_pole_5() const { return ___pole_5; }
	inline String_t** get_address_of_pole_5() { return &___pole_5; }
	inline void set_pole_5(String_t* value)
	{
		___pole_5 = value;
		Il2CppCodeGenWriteBarrier((&___pole_5), value);
	}

	inline static int32_t get_offset_of_startX_6() { return static_cast<int32_t>(offsetof(SpineboyPole_t3969514969, ___startX_6)); }
	inline float get_startX_6() const { return ___startX_6; }
	inline float* get_address_of_startX_6() { return &___startX_6; }
	inline void set_startX_6(float value)
	{
		___startX_6 = value;
	}

	inline static int32_t get_offset_of_endX_7() { return static_cast<int32_t>(offsetof(SpineboyPole_t3969514969, ___endX_7)); }
	inline float get_endX_7() const { return ___endX_7; }
	inline float* get_address_of_endX_7() { return &___endX_7; }
	inline void set_endX_7(float value)
	{
		___endX_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEBOYPOLE_T3969514969_H
#ifndef SPINEGAUGE_T3177630563_H
#define SPINEGAUGE_T3177630563_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.SpineGauge
struct  SpineGauge_t3177630563  : public MonoBehaviour_t3755042618
{
public:
	// System.Single Spine.Unity.Examples.SpineGauge::fillPercent
	float ___fillPercent_2;
	// System.String Spine.Unity.Examples.SpineGauge::fillAnimationName
	String_t* ___fillAnimationName_3;
	// Spine.Unity.SkeletonRenderer Spine.Unity.Examples.SpineGauge::skeletonRenderer
	SkeletonRenderer_t1681389628 * ___skeletonRenderer_4;
	// Spine.Animation Spine.Unity.Examples.SpineGauge::fillAnimation
	Animation_t2067143511 * ___fillAnimation_5;

public:
	inline static int32_t get_offset_of_fillPercent_2() { return static_cast<int32_t>(offsetof(SpineGauge_t3177630563, ___fillPercent_2)); }
	inline float get_fillPercent_2() const { return ___fillPercent_2; }
	inline float* get_address_of_fillPercent_2() { return &___fillPercent_2; }
	inline void set_fillPercent_2(float value)
	{
		___fillPercent_2 = value;
	}

	inline static int32_t get_offset_of_fillAnimationName_3() { return static_cast<int32_t>(offsetof(SpineGauge_t3177630563, ___fillAnimationName_3)); }
	inline String_t* get_fillAnimationName_3() const { return ___fillAnimationName_3; }
	inline String_t** get_address_of_fillAnimationName_3() { return &___fillAnimationName_3; }
	inline void set_fillAnimationName_3(String_t* value)
	{
		___fillAnimationName_3 = value;
		Il2CppCodeGenWriteBarrier((&___fillAnimationName_3), value);
	}

	inline static int32_t get_offset_of_skeletonRenderer_4() { return static_cast<int32_t>(offsetof(SpineGauge_t3177630563, ___skeletonRenderer_4)); }
	inline SkeletonRenderer_t1681389628 * get_skeletonRenderer_4() const { return ___skeletonRenderer_4; }
	inline SkeletonRenderer_t1681389628 ** get_address_of_skeletonRenderer_4() { return &___skeletonRenderer_4; }
	inline void set_skeletonRenderer_4(SkeletonRenderer_t1681389628 * value)
	{
		___skeletonRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_4), value);
	}

	inline static int32_t get_offset_of_fillAnimation_5() { return static_cast<int32_t>(offsetof(SpineGauge_t3177630563, ___fillAnimation_5)); }
	inline Animation_t2067143511 * get_fillAnimation_5() const { return ___fillAnimation_5; }
	inline Animation_t2067143511 ** get_address_of_fillAnimation_5() { return &___fillAnimation_5; }
	inline void set_fillAnimation_5(Animation_t2067143511 * value)
	{
		___fillAnimation_5 = value;
		Il2CppCodeGenWriteBarrier((&___fillAnimation_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEGAUGE_T3177630563_H
#ifndef NEXT_T4037784904_H
#define NEXT_T4037784904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Next
struct  Next_t4037784904  : public MonoBehaviour_t3755042618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEXT_T4037784904_H
#ifndef SCENE1_T2445523997_H
#define SCENE1_T2445523997_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Scene1
struct  Scene1_t2445523997  : public MonoBehaviour_t3755042618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENE1_T2445523997_H
#ifndef SKELETONANIMATIONMULTI_T3809275503_H
#define SKELETONANIMATIONMULTI_T3809275503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimationMulti
struct  SkeletonAnimationMulti_t3809275503  : public MonoBehaviour_t3755042618
{
public:
	// System.Boolean Spine.Unity.SkeletonAnimationMulti::initialFlipX
	bool ___initialFlipX_3;
	// System.Boolean Spine.Unity.SkeletonAnimationMulti::initialFlipY
	bool ___initialFlipY_4;
	// System.String Spine.Unity.SkeletonAnimationMulti::initialAnimation
	String_t* ___initialAnimation_5;
	// System.Boolean Spine.Unity.SkeletonAnimationMulti::initialLoop
	bool ___initialLoop_6;
	// System.Collections.Generic.List`1<Spine.Unity.SkeletonDataAsset> Spine.Unity.SkeletonAnimationMulti::skeletonDataAssets
	List_1_t3829790812 * ___skeletonDataAssets_7;
	// Spine.Unity.MeshGenerator/Settings Spine.Unity.SkeletonAnimationMulti::meshGeneratorSettings
	Settings_t1339330787  ___meshGeneratorSettings_8;
	// System.Collections.Generic.List`1<Spine.Unity.SkeletonAnimation> Spine.Unity.SkeletonAnimationMulti::skeletonAnimations
	List_1_t4136181990 * ___skeletonAnimations_9;
	// System.Collections.Generic.Dictionary`2<System.String,Spine.Animation> Spine.Unity.SkeletonAnimationMulti::animationNameTable
	Dictionary_2_t3710042347 * ___animationNameTable_10;
	// System.Collections.Generic.Dictionary`2<Spine.Animation,Spine.Unity.SkeletonAnimation> Spine.Unity.SkeletonAnimationMulti::animationSkeletonTable
	Dictionary_2_t2387518393 * ___animationSkeletonTable_11;
	// Spine.Unity.SkeletonAnimation Spine.Unity.SkeletonAnimationMulti::currentSkeletonAnimation
	SkeletonAnimation_t2012766265 * ___currentSkeletonAnimation_12;

public:
	inline static int32_t get_offset_of_initialFlipX_3() { return static_cast<int32_t>(offsetof(SkeletonAnimationMulti_t3809275503, ___initialFlipX_3)); }
	inline bool get_initialFlipX_3() const { return ___initialFlipX_3; }
	inline bool* get_address_of_initialFlipX_3() { return &___initialFlipX_3; }
	inline void set_initialFlipX_3(bool value)
	{
		___initialFlipX_3 = value;
	}

	inline static int32_t get_offset_of_initialFlipY_4() { return static_cast<int32_t>(offsetof(SkeletonAnimationMulti_t3809275503, ___initialFlipY_4)); }
	inline bool get_initialFlipY_4() const { return ___initialFlipY_4; }
	inline bool* get_address_of_initialFlipY_4() { return &___initialFlipY_4; }
	inline void set_initialFlipY_4(bool value)
	{
		___initialFlipY_4 = value;
	}

	inline static int32_t get_offset_of_initialAnimation_5() { return static_cast<int32_t>(offsetof(SkeletonAnimationMulti_t3809275503, ___initialAnimation_5)); }
	inline String_t* get_initialAnimation_5() const { return ___initialAnimation_5; }
	inline String_t** get_address_of_initialAnimation_5() { return &___initialAnimation_5; }
	inline void set_initialAnimation_5(String_t* value)
	{
		___initialAnimation_5 = value;
		Il2CppCodeGenWriteBarrier((&___initialAnimation_5), value);
	}

	inline static int32_t get_offset_of_initialLoop_6() { return static_cast<int32_t>(offsetof(SkeletonAnimationMulti_t3809275503, ___initialLoop_6)); }
	inline bool get_initialLoop_6() const { return ___initialLoop_6; }
	inline bool* get_address_of_initialLoop_6() { return &___initialLoop_6; }
	inline void set_initialLoop_6(bool value)
	{
		___initialLoop_6 = value;
	}

	inline static int32_t get_offset_of_skeletonDataAssets_7() { return static_cast<int32_t>(offsetof(SkeletonAnimationMulti_t3809275503, ___skeletonDataAssets_7)); }
	inline List_1_t3829790812 * get_skeletonDataAssets_7() const { return ___skeletonDataAssets_7; }
	inline List_1_t3829790812 ** get_address_of_skeletonDataAssets_7() { return &___skeletonDataAssets_7; }
	inline void set_skeletonDataAssets_7(List_1_t3829790812 * value)
	{
		___skeletonDataAssets_7 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAssets_7), value);
	}

	inline static int32_t get_offset_of_meshGeneratorSettings_8() { return static_cast<int32_t>(offsetof(SkeletonAnimationMulti_t3809275503, ___meshGeneratorSettings_8)); }
	inline Settings_t1339330787  get_meshGeneratorSettings_8() const { return ___meshGeneratorSettings_8; }
	inline Settings_t1339330787 * get_address_of_meshGeneratorSettings_8() { return &___meshGeneratorSettings_8; }
	inline void set_meshGeneratorSettings_8(Settings_t1339330787  value)
	{
		___meshGeneratorSettings_8 = value;
	}

	inline static int32_t get_offset_of_skeletonAnimations_9() { return static_cast<int32_t>(offsetof(SkeletonAnimationMulti_t3809275503, ___skeletonAnimations_9)); }
	inline List_1_t4136181990 * get_skeletonAnimations_9() const { return ___skeletonAnimations_9; }
	inline List_1_t4136181990 ** get_address_of_skeletonAnimations_9() { return &___skeletonAnimations_9; }
	inline void set_skeletonAnimations_9(List_1_t4136181990 * value)
	{
		___skeletonAnimations_9 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonAnimations_9), value);
	}

	inline static int32_t get_offset_of_animationNameTable_10() { return static_cast<int32_t>(offsetof(SkeletonAnimationMulti_t3809275503, ___animationNameTable_10)); }
	inline Dictionary_2_t3710042347 * get_animationNameTable_10() const { return ___animationNameTable_10; }
	inline Dictionary_2_t3710042347 ** get_address_of_animationNameTable_10() { return &___animationNameTable_10; }
	inline void set_animationNameTable_10(Dictionary_2_t3710042347 * value)
	{
		___animationNameTable_10 = value;
		Il2CppCodeGenWriteBarrier((&___animationNameTable_10), value);
	}

	inline static int32_t get_offset_of_animationSkeletonTable_11() { return static_cast<int32_t>(offsetof(SkeletonAnimationMulti_t3809275503, ___animationSkeletonTable_11)); }
	inline Dictionary_2_t2387518393 * get_animationSkeletonTable_11() const { return ___animationSkeletonTable_11; }
	inline Dictionary_2_t2387518393 ** get_address_of_animationSkeletonTable_11() { return &___animationSkeletonTable_11; }
	inline void set_animationSkeletonTable_11(Dictionary_2_t2387518393 * value)
	{
		___animationSkeletonTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___animationSkeletonTable_11), value);
	}

	inline static int32_t get_offset_of_currentSkeletonAnimation_12() { return static_cast<int32_t>(offsetof(SkeletonAnimationMulti_t3809275503, ___currentSkeletonAnimation_12)); }
	inline SkeletonAnimation_t2012766265 * get_currentSkeletonAnimation_12() const { return ___currentSkeletonAnimation_12; }
	inline SkeletonAnimation_t2012766265 ** get_address_of_currentSkeletonAnimation_12() { return &___currentSkeletonAnimation_12; }
	inline void set_currentSkeletonAnimation_12(SkeletonAnimation_t2012766265 * value)
	{
		___currentSkeletonAnimation_12 = value;
		Il2CppCodeGenWriteBarrier((&___currentSkeletonAnimation_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONANIMATIONMULTI_T3809275503_H
#ifndef MIXANDMATCH_T1342663373_H
#define MIXANDMATCH_T1342663373_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Examples.MixAndMatch
struct  MixAndMatch_t1342663373  : public MonoBehaviour_t3755042618
{
public:
	// System.String Spine.Unity.Examples.MixAndMatch::templateAttachmentsSkin
	String_t* ___templateAttachmentsSkin_2;
	// UnityEngine.Material Spine.Unity.Examples.MixAndMatch::sourceMaterial
	Material_t2681358023 * ___sourceMaterial_3;
	// UnityEngine.Sprite Spine.Unity.Examples.MixAndMatch::visorSprite
	Sprite_t360607379 * ___visorSprite_4;
	// System.String Spine.Unity.Examples.MixAndMatch::visorSlot
	String_t* ___visorSlot_5;
	// System.String Spine.Unity.Examples.MixAndMatch::visorKey
	String_t* ___visorKey_6;
	// UnityEngine.Sprite Spine.Unity.Examples.MixAndMatch::gunSprite
	Sprite_t360607379 * ___gunSprite_7;
	// System.String Spine.Unity.Examples.MixAndMatch::gunSlot
	String_t* ___gunSlot_8;
	// System.String Spine.Unity.Examples.MixAndMatch::gunKey
	String_t* ___gunKey_9;
	// System.Boolean Spine.Unity.Examples.MixAndMatch::repack
	bool ___repack_10;
	// Spine.Unity.BoundingBoxFollower Spine.Unity.Examples.MixAndMatch::bbFollower
	BoundingBoxFollower_t836972383 * ___bbFollower_11;
	// UnityEngine.Texture2D Spine.Unity.Examples.MixAndMatch::runtimeAtlas
	Texture2D_t1431210461 * ___runtimeAtlas_12;
	// UnityEngine.Material Spine.Unity.Examples.MixAndMatch::runtimeMaterial
	Material_t2681358023 * ___runtimeMaterial_13;
	// Spine.Skin Spine.Unity.Examples.MixAndMatch::customSkin
	Skin_t1695237131 * ___customSkin_14;

public:
	inline static int32_t get_offset_of_templateAttachmentsSkin_2() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___templateAttachmentsSkin_2)); }
	inline String_t* get_templateAttachmentsSkin_2() const { return ___templateAttachmentsSkin_2; }
	inline String_t** get_address_of_templateAttachmentsSkin_2() { return &___templateAttachmentsSkin_2; }
	inline void set_templateAttachmentsSkin_2(String_t* value)
	{
		___templateAttachmentsSkin_2 = value;
		Il2CppCodeGenWriteBarrier((&___templateAttachmentsSkin_2), value);
	}

	inline static int32_t get_offset_of_sourceMaterial_3() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___sourceMaterial_3)); }
	inline Material_t2681358023 * get_sourceMaterial_3() const { return ___sourceMaterial_3; }
	inline Material_t2681358023 ** get_address_of_sourceMaterial_3() { return &___sourceMaterial_3; }
	inline void set_sourceMaterial_3(Material_t2681358023 * value)
	{
		___sourceMaterial_3 = value;
		Il2CppCodeGenWriteBarrier((&___sourceMaterial_3), value);
	}

	inline static int32_t get_offset_of_visorSprite_4() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___visorSprite_4)); }
	inline Sprite_t360607379 * get_visorSprite_4() const { return ___visorSprite_4; }
	inline Sprite_t360607379 ** get_address_of_visorSprite_4() { return &___visorSprite_4; }
	inline void set_visorSprite_4(Sprite_t360607379 * value)
	{
		___visorSprite_4 = value;
		Il2CppCodeGenWriteBarrier((&___visorSprite_4), value);
	}

	inline static int32_t get_offset_of_visorSlot_5() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___visorSlot_5)); }
	inline String_t* get_visorSlot_5() const { return ___visorSlot_5; }
	inline String_t** get_address_of_visorSlot_5() { return &___visorSlot_5; }
	inline void set_visorSlot_5(String_t* value)
	{
		___visorSlot_5 = value;
		Il2CppCodeGenWriteBarrier((&___visorSlot_5), value);
	}

	inline static int32_t get_offset_of_visorKey_6() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___visorKey_6)); }
	inline String_t* get_visorKey_6() const { return ___visorKey_6; }
	inline String_t** get_address_of_visorKey_6() { return &___visorKey_6; }
	inline void set_visorKey_6(String_t* value)
	{
		___visorKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___visorKey_6), value);
	}

	inline static int32_t get_offset_of_gunSprite_7() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___gunSprite_7)); }
	inline Sprite_t360607379 * get_gunSprite_7() const { return ___gunSprite_7; }
	inline Sprite_t360607379 ** get_address_of_gunSprite_7() { return &___gunSprite_7; }
	inline void set_gunSprite_7(Sprite_t360607379 * value)
	{
		___gunSprite_7 = value;
		Il2CppCodeGenWriteBarrier((&___gunSprite_7), value);
	}

	inline static int32_t get_offset_of_gunSlot_8() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___gunSlot_8)); }
	inline String_t* get_gunSlot_8() const { return ___gunSlot_8; }
	inline String_t** get_address_of_gunSlot_8() { return &___gunSlot_8; }
	inline void set_gunSlot_8(String_t* value)
	{
		___gunSlot_8 = value;
		Il2CppCodeGenWriteBarrier((&___gunSlot_8), value);
	}

	inline static int32_t get_offset_of_gunKey_9() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___gunKey_9)); }
	inline String_t* get_gunKey_9() const { return ___gunKey_9; }
	inline String_t** get_address_of_gunKey_9() { return &___gunKey_9; }
	inline void set_gunKey_9(String_t* value)
	{
		___gunKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___gunKey_9), value);
	}

	inline static int32_t get_offset_of_repack_10() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___repack_10)); }
	inline bool get_repack_10() const { return ___repack_10; }
	inline bool* get_address_of_repack_10() { return &___repack_10; }
	inline void set_repack_10(bool value)
	{
		___repack_10 = value;
	}

	inline static int32_t get_offset_of_bbFollower_11() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___bbFollower_11)); }
	inline BoundingBoxFollower_t836972383 * get_bbFollower_11() const { return ___bbFollower_11; }
	inline BoundingBoxFollower_t836972383 ** get_address_of_bbFollower_11() { return &___bbFollower_11; }
	inline void set_bbFollower_11(BoundingBoxFollower_t836972383 * value)
	{
		___bbFollower_11 = value;
		Il2CppCodeGenWriteBarrier((&___bbFollower_11), value);
	}

	inline static int32_t get_offset_of_runtimeAtlas_12() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___runtimeAtlas_12)); }
	inline Texture2D_t1431210461 * get_runtimeAtlas_12() const { return ___runtimeAtlas_12; }
	inline Texture2D_t1431210461 ** get_address_of_runtimeAtlas_12() { return &___runtimeAtlas_12; }
	inline void set_runtimeAtlas_12(Texture2D_t1431210461 * value)
	{
		___runtimeAtlas_12 = value;
		Il2CppCodeGenWriteBarrier((&___runtimeAtlas_12), value);
	}

	inline static int32_t get_offset_of_runtimeMaterial_13() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___runtimeMaterial_13)); }
	inline Material_t2681358023 * get_runtimeMaterial_13() const { return ___runtimeMaterial_13; }
	inline Material_t2681358023 ** get_address_of_runtimeMaterial_13() { return &___runtimeMaterial_13; }
	inline void set_runtimeMaterial_13(Material_t2681358023 * value)
	{
		___runtimeMaterial_13 = value;
		Il2CppCodeGenWriteBarrier((&___runtimeMaterial_13), value);
	}

	inline static int32_t get_offset_of_customSkin_14() { return static_cast<int32_t>(offsetof(MixAndMatch_t1342663373, ___customSkin_14)); }
	inline Skin_t1695237131 * get_customSkin_14() const { return ___customSkin_14; }
	inline Skin_t1695237131 ** get_address_of_customSkin_14() { return &___customSkin_14; }
	inline void set_customSkin_14(Skin_t1695237131 * value)
	{
		___customSkin_14 = value;
		Il2CppCodeGenWriteBarrier((&___customSkin_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXANDMATCH_T1342663373_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (MixAndMatch_t1342663373), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1800[13] = 
{
	MixAndMatch_t1342663373::get_offset_of_templateAttachmentsSkin_2(),
	MixAndMatch_t1342663373::get_offset_of_sourceMaterial_3(),
	MixAndMatch_t1342663373::get_offset_of_visorSprite_4(),
	MixAndMatch_t1342663373::get_offset_of_visorSlot_5(),
	MixAndMatch_t1342663373::get_offset_of_visorKey_6(),
	MixAndMatch_t1342663373::get_offset_of_gunSprite_7(),
	MixAndMatch_t1342663373::get_offset_of_gunSlot_8(),
	MixAndMatch_t1342663373::get_offset_of_gunKey_9(),
	MixAndMatch_t1342663373::get_offset_of_repack_10(),
	MixAndMatch_t1342663373::get_offset_of_bbFollower_11(),
	MixAndMatch_t1342663373::get_offset_of_runtimeAtlas_12(),
	MixAndMatch_t1342663373::get_offset_of_runtimeMaterial_13(),
	MixAndMatch_t1342663373::get_offset_of_customSkin_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (U3CStartU3Ec__Iterator0_t1482506192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1801[4] = 
{
	U3CStartU3Ec__Iterator0_t1482506192::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t1482506192::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t1482506192::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t1482506192::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (MixAndMatchGraphic_t1263195094), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1802[12] = 
{
	MixAndMatchGraphic_t1263195094::get_offset_of_baseSkinName_2(),
	MixAndMatchGraphic_t1263195094::get_offset_of_sourceMaterial_3(),
	MixAndMatchGraphic_t1263195094::get_offset_of_visorSprite_4(),
	MixAndMatchGraphic_t1263195094::get_offset_of_visorSlot_5(),
	MixAndMatchGraphic_t1263195094::get_offset_of_visorKey_6(),
	MixAndMatchGraphic_t1263195094::get_offset_of_gunSprite_7(),
	MixAndMatchGraphic_t1263195094::get_offset_of_gunSlot_8(),
	MixAndMatchGraphic_t1263195094::get_offset_of_gunKey_9(),
	MixAndMatchGraphic_t1263195094::get_offset_of_repack_10(),
	MixAndMatchGraphic_t1263195094::get_offset_of_runtimeAtlas_11(),
	MixAndMatchGraphic_t1263195094::get_offset_of_runtimeMaterial_12(),
	MixAndMatchGraphic_t1263195094::get_offset_of_customSkin_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (U3CStartU3Ec__Iterator0_t2675052689), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[4] = 
{
	U3CStartU3Ec__Iterator0_t2675052689::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t2675052689::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t2675052689::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t2675052689::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (RaggedySpineboy_t30683967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1804[5] = 
{
	RaggedySpineboy_t30683967::get_offset_of_groundMask_2(),
	RaggedySpineboy_t30683967::get_offset_of_restoreDuration_3(),
	RaggedySpineboy_t30683967::get_offset_of_launchVelocity_4(),
	RaggedySpineboy_t30683967::get_offset_of_ragdoll_5(),
	RaggedySpineboy_t30683967::get_offset_of_naturalCollider_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (U3CRestoreU3Ec__Iterator0_t729631813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1805[8] = 
{
	U3CRestoreU3Ec__Iterator0_t729631813::get_offset_of_U3CestimatedPosU3E__0_0(),
	U3CRestoreU3Ec__Iterator0_t729631813::get_offset_of_U3CrbPositionU3E__0_1(),
	U3CRestoreU3Ec__Iterator0_t729631813::get_offset_of_U3CskeletonPointU3E__0_2(),
	U3CRestoreU3Ec__Iterator0_t729631813::get_offset_of_U3ChitU3E__0_3(),
	U3CRestoreU3Ec__Iterator0_t729631813::get_offset_of_U24this_4(),
	U3CRestoreU3Ec__Iterator0_t729631813::get_offset_of_U24current_5(),
	U3CRestoreU3Ec__Iterator0_t729631813::get_offset_of_U24disposing_6(),
	U3CRestoreU3Ec__Iterator0_t729631813::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (U3CWaitUntilStoppedU3Ec__Iterator1_t1053025697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1806[5] = 
{
	U3CWaitUntilStoppedU3Ec__Iterator1_t1053025697::get_offset_of_U3CtU3E__0_0(),
	U3CWaitUntilStoppedU3Ec__Iterator1_t1053025697::get_offset_of_U24this_1(),
	U3CWaitUntilStoppedU3Ec__Iterator1_t1053025697::get_offset_of_U24current_2(),
	U3CWaitUntilStoppedU3Ec__Iterator1_t1053025697::get_offset_of_U24disposing_3(),
	U3CWaitUntilStoppedU3Ec__Iterator1_t1053025697::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (Rotator_t2221798354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[2] = 
{
	Rotator_t2221798354::get_offset_of_direction_2(),
	Rotator_t2221798354::get_offset_of_speed_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (BoneLocalOverride_t2136445371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1808[8] = 
{
	BoneLocalOverride_t2136445371::get_offset_of_boneName_2(),
	BoneLocalOverride_t2136445371::get_offset_of_alpha_3(),
	BoneLocalOverride_t2136445371::get_offset_of_overridePosition_4(),
	BoneLocalOverride_t2136445371::get_offset_of_localPosition_5(),
	BoneLocalOverride_t2136445371::get_offset_of_overrideRotation_6(),
	BoneLocalOverride_t2136445371::get_offset_of_rotation_7(),
	BoneLocalOverride_t2136445371::get_offset_of_spineComponent_8(),
	BoneLocalOverride_t2136445371::get_offset_of_bone_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (CombinedSkin_t4123893441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[2] = 
{
	CombinedSkin_t4123893441::get_offset_of_skinsToCombine_2(),
	CombinedSkin_t4123893441::get_offset_of_combinedSkin_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (AtlasRegionAttacher_t437517080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1810[4] = 
{
	AtlasRegionAttacher_t437517080::get_offset_of_atlasAsset_2(),
	AtlasRegionAttacher_t437517080::get_offset_of_inheritProperties_3(),
	AtlasRegionAttacher_t437517080::get_offset_of_attachments_4(),
	AtlasRegionAttacher_t437517080::get_offset_of_atlas_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (SlotRegionPair_t2265310044), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[2] = 
{
	SlotRegionPair_t2265310044::get_offset_of_slot_0(),
	SlotRegionPair_t2265310044::get_offset_of_region_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (SpriteAttacher_t2181857634), -1, sizeof(SpriteAttacher_t2181857634_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1812[10] = 
{
	0,
	0,
	SpriteAttacher_t2181857634::get_offset_of_attachOnStart_4(),
	SpriteAttacher_t2181857634::get_offset_of_overrideAnimation_5(),
	SpriteAttacher_t2181857634::get_offset_of_sprite_6(),
	SpriteAttacher_t2181857634::get_offset_of_slot_7(),
	SpriteAttacher_t2181857634::get_offset_of_attachment_8(),
	SpriteAttacher_t2181857634::get_offset_of_spineSlot_9(),
	SpriteAttacher_t2181857634::get_offset_of_applyPMA_10(),
	SpriteAttacher_t2181857634_StaticFields::get_offset_of_atlasPageCache_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (SpriteAttachmentExtensions_t2253895941), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (JitterEffectExample_t518848748), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1814[2] = 
{
	JitterEffectExample_t518848748::get_offset_of_jitterMagnitude_2(),
	JitterEffectExample_t518848748::get_offset_of_skeletonRenderer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (SkeletonAnimationMulti_t3809275503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1815[11] = 
{
	0,
	SkeletonAnimationMulti_t3809275503::get_offset_of_initialFlipX_3(),
	SkeletonAnimationMulti_t3809275503::get_offset_of_initialFlipY_4(),
	SkeletonAnimationMulti_t3809275503::get_offset_of_initialAnimation_5(),
	SkeletonAnimationMulti_t3809275503::get_offset_of_initialLoop_6(),
	SkeletonAnimationMulti_t3809275503::get_offset_of_skeletonDataAssets_7(),
	SkeletonAnimationMulti_t3809275503::get_offset_of_meshGeneratorSettings_8(),
	SkeletonAnimationMulti_t3809275503::get_offset_of_skeletonAnimations_9(),
	SkeletonAnimationMulti_t3809275503::get_offset_of_animationNameTable_10(),
	SkeletonAnimationMulti_t3809275503::get_offset_of_animationSkeletonTable_11(),
	SkeletonAnimationMulti_t3809275503::get_offset_of_currentSkeletonAnimation_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (SkeletonColorInitialize_t1957958085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1816[2] = 
{
	SkeletonColorInitialize_t1957958085::get_offset_of_skeletonColor_2(),
	SkeletonColorInitialize_t1957958085::get_offset_of_slotSettings_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { sizeof (SlotSettings_t3151105826), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1817[2] = 
{
	SlotSettings_t3151105826::get_offset_of_slot_0(),
	SlotSettings_t3151105826::get_offset_of_color_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { sizeof (SlotTintBlackFollower_t719666413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1818[8] = 
{
	SlotTintBlackFollower_t719666413::get_offset_of_slotName_2(),
	SlotTintBlackFollower_t719666413::get_offset_of_colorPropertyName_3(),
	SlotTintBlackFollower_t719666413::get_offset_of_blackPropertyName_4(),
	SlotTintBlackFollower_t719666413::get_offset_of_slot_5(),
	SlotTintBlackFollower_t719666413::get_offset_of_mr_6(),
	SlotTintBlackFollower_t719666413::get_offset_of_mb_7(),
	SlotTintBlackFollower_t719666413::get_offset_of_colorPropertyId_8(),
	SlotTintBlackFollower_t719666413::get_offset_of_blackPropertyId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (SpineEventUnityHandler_t3250816684), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[3] = 
{
	SpineEventUnityHandler_t3250816684::get_offset_of_events_2(),
	SpineEventUnityHandler_t3250816684::get_offset_of_skeletonComponent_3(),
	SpineEventUnityHandler_t3250816684::get_offset_of_animationStateComponent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (EventPair_t1359900303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[3] = 
{
	EventPair_t1359900303::get_offset_of_spineEvent_0(),
	EventPair_t1359900303::get_offset_of_unityHandler_1(),
	EventPair_t1359900303::get_offset_of_eventDelegate_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (U3CStartU3Ec__AnonStorey1_t1282162354), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1821[1] = 
{
	U3CStartU3Ec__AnonStorey1_t1282162354::get_offset_of_ep_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (U3CStartU3Ec__AnonStorey0_t3384789643), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[2] = 
{
	U3CStartU3Ec__AnonStorey0_t3384789643::get_offset_of_eventData_0(),
	U3CStartU3Ec__AnonStorey0_t3384789643::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (SpawnFromSkeletonDataExample_t3296636424), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1823[3] = 
{
	SpawnFromSkeletonDataExample_t3296636424::get_offset_of_skeletonDataAsset_2(),
	SpawnFromSkeletonDataExample_t3296636424::get_offset_of_count_3(),
	SpawnFromSkeletonDataExample_t3296636424::get_offset_of_startingAnimation_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (U3CStartU3Ec__Iterator0_t2460598370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1824[7] = 
{
	U3CStartU3Ec__Iterator0_t2460598370::get_offset_of_U3CspineAnimationU3E__0_0(),
	U3CStartU3Ec__Iterator0_t2460598370::get_offset_of_U3CiU3E__1_1(),
	U3CStartU3Ec__Iterator0_t2460598370::get_offset_of_U3CsaU3E__2_2(),
	U3CStartU3Ec__Iterator0_t2460598370::get_offset_of_U24this_3(),
	U3CStartU3Ec__Iterator0_t2460598370::get_offset_of_U24current_4(),
	U3CStartU3Ec__Iterator0_t2460598370::get_offset_of_U24disposing_5(),
	U3CStartU3Ec__Iterator0_t2460598370::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (Spineboy_t3944353744), -1, sizeof(Spineboy_t3944353744_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1825[2] = 
{
	Spineboy_t3944353744::get_offset_of_skeletonAnimation_2(),
	Spineboy_t3944353744_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (SpineboyPole_t3969514969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[8] = 
{
	SpineboyPole_t3969514969::get_offset_of_skeletonAnimation_2(),
	SpineboyPole_t3969514969::get_offset_of_separator_3(),
	SpineboyPole_t3969514969::get_offset_of_run_4(),
	SpineboyPole_t3969514969::get_offset_of_pole_5(),
	SpineboyPole_t3969514969::get_offset_of_startX_6(),
	SpineboyPole_t3969514969::get_offset_of_endX_7(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (U3CStartU3Ec__Iterator0_t1250445569), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1827[6] = 
{
	U3CStartU3Ec__Iterator0_t1250445569::get_offset_of_U3CstateU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1250445569::get_offset_of_U3CpoleTrackU3E__1_1(),
	U3CStartU3Ec__Iterator0_t1250445569::get_offset_of_U24this_2(),
	U3CStartU3Ec__Iterator0_t1250445569::get_offset_of_U24current_3(),
	U3CStartU3Ec__Iterator0_t1250445569::get_offset_of_U24disposing_4(),
	U3CStartU3Ec__Iterator0_t1250445569::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (SpineGauge_t3177630563), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[4] = 
{
	SpineGauge_t3177630563::get_offset_of_fillPercent_2(),
	SpineGauge_t3177630563::get_offset_of_fillAnimationName_3(),
	SpineGauge_t3177630563::get_offset_of_skeletonRenderer_4(),
	SpineGauge_t3177630563::get_offset_of_fillAnimation_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (Next_t4037784904), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (Scene1_t2445523997), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (MainLogin_t2109370316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (Animation_t2067143511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1832[3] = 
{
	Animation_t2067143511::get_offset_of_timelines_0(),
	Animation_t2067143511::get_offset_of_duration_1(),
	Animation_t2067143511::get_offset_of_name_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { sizeof (MixPose_t1042684880)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1834[4] = 
{
	MixPose_t1042684880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { sizeof (MixDirection_t1513473484)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1835[3] = 
{
	MixDirection_t1513473484::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { sizeof (TimelineType_t3903614005)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1836[16] = 
{
	TimelineType_t3903614005::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { sizeof (CurveTimeline_t917256691), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1837[5] = 
{
	0,
	0,
	0,
	0,
	CurveTimeline_t917256691::get_offset_of_curves_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { sizeof (RotateTimeline_t263101077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1838[6] = 
{
	0,
	0,
	0,
	0,
	RotateTimeline_t263101077::get_offset_of_boneIndex_9(),
	RotateTimeline_t263101077::get_offset_of_frames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (TranslateTimeline_t2198274540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	TranslateTimeline_t2198274540::get_offset_of_boneIndex_11(),
	TranslateTimeline_t2198274540::get_offset_of_frames_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (ScaleTimeline_t977293153), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (ShearTimeline_t3117059177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (ColorTimeline_t137362015), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1842[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	ColorTimeline_t137362015::get_offset_of_slotIndex_15(),
	ColorTimeline_t137362015::get_offset_of_frames_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (TwoColorTimeline_t1895897344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1843[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TwoColorTimeline_t1895897344::get_offset_of_frames_21(),
	TwoColorTimeline_t1895897344::get_offset_of_slotIndex_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (AttachmentTimeline_t2136692098), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1844[3] = 
{
	AttachmentTimeline_t2136692098::get_offset_of_slotIndex_0(),
	AttachmentTimeline_t2136692098::get_offset_of_frames_1(),
	AttachmentTimeline_t2136692098::get_offset_of_attachmentNames_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { sizeof (DeformTimeline_t3829885991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1845[4] = 
{
	DeformTimeline_t3829885991::get_offset_of_slotIndex_5(),
	DeformTimeline_t3829885991::get_offset_of_frames_6(),
	DeformTimeline_t3829885991::get_offset_of_frameVertices_7(),
	DeformTimeline_t3829885991::get_offset_of_attachment_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { sizeof (EventTimeline_t2506373614), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[2] = 
{
	EventTimeline_t2506373614::get_offset_of_frames_0(),
	EventTimeline_t2506373614::get_offset_of_events_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { sizeof (DrawOrderTimeline_t2720399701), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[2] = 
{
	DrawOrderTimeline_t2720399701::get_offset_of_frames_0(),
	DrawOrderTimeline_t2720399701::get_offset_of_drawOrders_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { sizeof (IkConstraintTimeline_t2394284159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	IkConstraintTimeline_t2394284159::get_offset_of_ikConstraintIndex_11(),
	IkConstraintTimeline_t2394284159::get_offset_of_frames_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (TransformConstraintTimeline_t1868878952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1849[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	TransformConstraintTimeline_t1868878952::get_offset_of_transformConstraintIndex_15(),
	TransformConstraintTimeline_t1868878952::get_offset_of_frames_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (PathConstraintPositionTimeline_t2710737122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1850[6] = 
{
	0,
	0,
	0,
	0,
	PathConstraintPositionTimeline_t2710737122::get_offset_of_pathConstraintIndex_9(),
	PathConstraintPositionTimeline_t2710737122::get_offset_of_frames_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (PathConstraintSpacingTimeline_t2013794857), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (PathConstraintMixTimeline_t3043566903), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1852[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	PathConstraintMixTimeline_t3043566903::get_offset_of_pathConstraintIndex_11(),
	PathConstraintMixTimeline_t3043566903::get_offset_of_frames_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (AnimationState_t2858486651), -1, sizeof(AnimationState_t2858486651_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1853[20] = 
{
	AnimationState_t2858486651_StaticFields::get_offset_of_EmptyAnimation_0(),
	0,
	0,
	0,
	0,
	AnimationState_t2858486651::get_offset_of_data_5(),
	AnimationState_t2858486651::get_offset_of_trackEntryPool_6(),
	AnimationState_t2858486651::get_offset_of_tracks_7(),
	AnimationState_t2858486651::get_offset_of_events_8(),
	AnimationState_t2858486651::get_offset_of_queue_9(),
	AnimationState_t2858486651::get_offset_of_propertyIDs_10(),
	AnimationState_t2858486651::get_offset_of_mixingTo_11(),
	AnimationState_t2858486651::get_offset_of_animationsChanged_12(),
	AnimationState_t2858486651::get_offset_of_timeScale_13(),
	AnimationState_t2858486651::get_offset_of_Interrupt_14(),
	AnimationState_t2858486651::get_offset_of_End_15(),
	AnimationState_t2858486651::get_offset_of_Dispose_16(),
	AnimationState_t2858486651::get_offset_of_Complete_17(),
	AnimationState_t2858486651::get_offset_of_Start_18(),
	AnimationState_t2858486651::get_offset_of_Event_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (TrackEntryDelegate_t1807657294), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (TrackEntryEventDelegate_t471240152), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (TrackEntry_t4062297782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[32] = 
{
	TrackEntry_t4062297782::get_offset_of_animation_0(),
	TrackEntry_t4062297782::get_offset_of_next_1(),
	TrackEntry_t4062297782::get_offset_of_mixingFrom_2(),
	TrackEntry_t4062297782::get_offset_of_trackIndex_3(),
	TrackEntry_t4062297782::get_offset_of_loop_4(),
	TrackEntry_t4062297782::get_offset_of_eventThreshold_5(),
	TrackEntry_t4062297782::get_offset_of_attachmentThreshold_6(),
	TrackEntry_t4062297782::get_offset_of_drawOrderThreshold_7(),
	TrackEntry_t4062297782::get_offset_of_animationStart_8(),
	TrackEntry_t4062297782::get_offset_of_animationEnd_9(),
	TrackEntry_t4062297782::get_offset_of_animationLast_10(),
	TrackEntry_t4062297782::get_offset_of_nextAnimationLast_11(),
	TrackEntry_t4062297782::get_offset_of_delay_12(),
	TrackEntry_t4062297782::get_offset_of_trackTime_13(),
	TrackEntry_t4062297782::get_offset_of_trackLast_14(),
	TrackEntry_t4062297782::get_offset_of_nextTrackLast_15(),
	TrackEntry_t4062297782::get_offset_of_trackEnd_16(),
	TrackEntry_t4062297782::get_offset_of_timeScale_17(),
	TrackEntry_t4062297782::get_offset_of_alpha_18(),
	TrackEntry_t4062297782::get_offset_of_mixTime_19(),
	TrackEntry_t4062297782::get_offset_of_mixDuration_20(),
	TrackEntry_t4062297782::get_offset_of_interruptAlpha_21(),
	TrackEntry_t4062297782::get_offset_of_totalAlpha_22(),
	TrackEntry_t4062297782::get_offset_of_timelineData_23(),
	TrackEntry_t4062297782::get_offset_of_timelineDipMix_24(),
	TrackEntry_t4062297782::get_offset_of_timelinesRotation_25(),
	TrackEntry_t4062297782::get_offset_of_Interrupt_26(),
	TrackEntry_t4062297782::get_offset_of_End_27(),
	TrackEntry_t4062297782::get_offset_of_Dispose_28(),
	TrackEntry_t4062297782::get_offset_of_Complete_29(),
	TrackEntry_t4062297782::get_offset_of_Start_30(),
	TrackEntry_t4062297782::get_offset_of_Event_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { sizeof (EventQueue_t2605283308), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1857[5] = 
{
	EventQueue_t2605283308::get_offset_of_eventQueueEntries_0(),
	EventQueue_t2605283308::get_offset_of_drainDisabled_1(),
	EventQueue_t2605283308::get_offset_of_state_2(),
	EventQueue_t2605283308::get_offset_of_trackEntryPool_3(),
	EventQueue_t2605283308::get_offset_of_AnimationsChanged_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { sizeof (EventQueueEntry_t3756679084)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1858[3] = 
{
	EventQueueEntry_t3756679084::get_offset_of_type_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventQueueEntry_t3756679084::get_offset_of_entry_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventQueueEntry_t3756679084::get_offset_of_e_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (EventType_t3243473135)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1859[7] = 
{
	EventType_t3243473135::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1860[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (AnimationStateData_t1644285503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1862[3] = 
{
	AnimationStateData_t1644285503::get_offset_of_skeletonData_0(),
	AnimationStateData_t1644285503::get_offset_of_animationToMixTime_1(),
	AnimationStateData_t1644285503::get_offset_of_defaultMix_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (AnimationPair_t2435898854)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1863[2] = 
{
	AnimationPair_t2435898854::get_offset_of_a1_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AnimationPair_t2435898854::get_offset_of_a2_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (AnimationPairComparer_t3810224342), -1, sizeof(AnimationPairComparer_t3810224342_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1864[1] = 
{
	AnimationPairComparer_t3810224342_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (Atlas_t604244430), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1865[3] = 
{
	Atlas_t604244430::get_offset_of_pages_0(),
	Atlas_t604244430::get_offset_of_regions_1(),
	Atlas_t604244430::get_offset_of_textureLoader_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (Format_t2486158377)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1866[8] = 
{
	Format_t2486158377::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (TextureFilter_t3309269356)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1867[8] = 
{
	TextureFilter_t3309269356::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (TextureWrap_t684305347)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1868[4] = 
{
	TextureWrap_t684305347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (AtlasPage_t3729449121), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1869[9] = 
{
	AtlasPage_t3729449121::get_offset_of_name_0(),
	AtlasPage_t3729449121::get_offset_of_format_1(),
	AtlasPage_t3729449121::get_offset_of_minFilter_2(),
	AtlasPage_t3729449121::get_offset_of_magFilter_3(),
	AtlasPage_t3729449121::get_offset_of_uWrap_4(),
	AtlasPage_t3729449121::get_offset_of_vWrap_5(),
	AtlasPage_t3729449121::get_offset_of_rendererObject_6(),
	AtlasPage_t3729449121::get_offset_of_width_7(),
	AtlasPage_t3729449121::get_offset_of_height_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (AtlasRegion_t2476064513), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1870[18] = 
{
	AtlasRegion_t2476064513::get_offset_of_page_0(),
	AtlasRegion_t2476064513::get_offset_of_name_1(),
	AtlasRegion_t2476064513::get_offset_of_x_2(),
	AtlasRegion_t2476064513::get_offset_of_y_3(),
	AtlasRegion_t2476064513::get_offset_of_width_4(),
	AtlasRegion_t2476064513::get_offset_of_height_5(),
	AtlasRegion_t2476064513::get_offset_of_u_6(),
	AtlasRegion_t2476064513::get_offset_of_v_7(),
	AtlasRegion_t2476064513::get_offset_of_u2_8(),
	AtlasRegion_t2476064513::get_offset_of_v2_9(),
	AtlasRegion_t2476064513::get_offset_of_offsetX_10(),
	AtlasRegion_t2476064513::get_offset_of_offsetY_11(),
	AtlasRegion_t2476064513::get_offset_of_originalWidth_12(),
	AtlasRegion_t2476064513::get_offset_of_originalHeight_13(),
	AtlasRegion_t2476064513::get_offset_of_index_14(),
	AtlasRegion_t2476064513::get_offset_of_rotate_15(),
	AtlasRegion_t2476064513::get_offset_of_splits_16(),
	AtlasRegion_t2476064513::get_offset_of_pads_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (AtlasAttachmentLoader_t745839734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1872[1] = 
{
	AtlasAttachmentLoader_t745839734::get_offset_of_atlasArray_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (Attachment_t397756874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[1] = 
{
	Attachment_t397756874::get_offset_of_U3CNameU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (AttachmentType_t2811772594)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1876[8] = 
{
	AttachmentType_t2811772594::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (BoundingBoxAttachment_t1026836171), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (ClippingAttachment_t906347500), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[1] = 
{
	ClippingAttachment_t906347500::get_offset_of_endSlot_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (MeshAttachment_t3550597918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[26] = 
{
	MeshAttachment_t3550597918::get_offset_of_regionOffsetX_7(),
	MeshAttachment_t3550597918::get_offset_of_regionOffsetY_8(),
	MeshAttachment_t3550597918::get_offset_of_regionWidth_9(),
	MeshAttachment_t3550597918::get_offset_of_regionHeight_10(),
	MeshAttachment_t3550597918::get_offset_of_regionOriginalWidth_11(),
	MeshAttachment_t3550597918::get_offset_of_regionOriginalHeight_12(),
	MeshAttachment_t3550597918::get_offset_of_parentMesh_13(),
	MeshAttachment_t3550597918::get_offset_of_uvs_14(),
	MeshAttachment_t3550597918::get_offset_of_regionUVs_15(),
	MeshAttachment_t3550597918::get_offset_of_triangles_16(),
	MeshAttachment_t3550597918::get_offset_of_r_17(),
	MeshAttachment_t3550597918::get_offset_of_g_18(),
	MeshAttachment_t3550597918::get_offset_of_b_19(),
	MeshAttachment_t3550597918::get_offset_of_a_20(),
	MeshAttachment_t3550597918::get_offset_of_hulllength_21(),
	MeshAttachment_t3550597918::get_offset_of_inheritDeform_22(),
	MeshAttachment_t3550597918::get_offset_of_U3CPathU3Ek__BackingField_23(),
	MeshAttachment_t3550597918::get_offset_of_U3CRendererObjectU3Ek__BackingField_24(),
	MeshAttachment_t3550597918::get_offset_of_U3CRegionUU3Ek__BackingField_25(),
	MeshAttachment_t3550597918::get_offset_of_U3CRegionVU3Ek__BackingField_26(),
	MeshAttachment_t3550597918::get_offset_of_U3CRegionU2U3Ek__BackingField_27(),
	MeshAttachment_t3550597918::get_offset_of_U3CRegionV2U3Ek__BackingField_28(),
	MeshAttachment_t3550597918::get_offset_of_U3CRegionRotateU3Ek__BackingField_29(),
	MeshAttachment_t3550597918::get_offset_of_U3CEdgesU3Ek__BackingField_30(),
	MeshAttachment_t3550597918::get_offset_of_U3CWidthU3Ek__BackingField_31(),
	MeshAttachment_t3550597918::get_offset_of_U3CHeightU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (PathAttachment_t357603812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1880[3] = 
{
	PathAttachment_t357603812::get_offset_of_lengths_7(),
	PathAttachment_t357603812::get_offset_of_closed_8(),
	PathAttachment_t357603812::get_offset_of_constantSpeed_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (PointAttachment_t2896333818), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[3] = 
{
	PointAttachment_t2896333818::get_offset_of_x_1(),
	PointAttachment_t2896333818::get_offset_of_y_2(),
	PointAttachment_t2896333818::get_offset_of_rotation_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (RegionAttachment_t1431410254), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[29] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	RegionAttachment_t1431410254::get_offset_of_x_9(),
	RegionAttachment_t1431410254::get_offset_of_y_10(),
	RegionAttachment_t1431410254::get_offset_of_rotation_11(),
	RegionAttachment_t1431410254::get_offset_of_scaleX_12(),
	RegionAttachment_t1431410254::get_offset_of_scaleY_13(),
	RegionAttachment_t1431410254::get_offset_of_width_14(),
	RegionAttachment_t1431410254::get_offset_of_height_15(),
	RegionAttachment_t1431410254::get_offset_of_regionOffsetX_16(),
	RegionAttachment_t1431410254::get_offset_of_regionOffsetY_17(),
	RegionAttachment_t1431410254::get_offset_of_regionWidth_18(),
	RegionAttachment_t1431410254::get_offset_of_regionHeight_19(),
	RegionAttachment_t1431410254::get_offset_of_regionOriginalWidth_20(),
	RegionAttachment_t1431410254::get_offset_of_regionOriginalHeight_21(),
	RegionAttachment_t1431410254::get_offset_of_offset_22(),
	RegionAttachment_t1431410254::get_offset_of_uvs_23(),
	RegionAttachment_t1431410254::get_offset_of_r_24(),
	RegionAttachment_t1431410254::get_offset_of_g_25(),
	RegionAttachment_t1431410254::get_offset_of_b_26(),
	RegionAttachment_t1431410254::get_offset_of_a_27(),
	RegionAttachment_t1431410254::get_offset_of_U3CPathU3Ek__BackingField_28(),
	RegionAttachment_t1431410254::get_offset_of_U3CRendererObjectU3Ek__BackingField_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (VertexAttachment_t1776545046), -1, sizeof(VertexAttachment_t1776545046_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1883[6] = 
{
	VertexAttachment_t1776545046_StaticFields::get_offset_of_nextID_1(),
	VertexAttachment_t1776545046_StaticFields::get_offset_of_nextIdLock_2(),
	VertexAttachment_t1776545046::get_offset_of_id_3(),
	VertexAttachment_t1776545046::get_offset_of_bones_4(),
	VertexAttachment_t1776545046::get_offset_of_vertices_5(),
	VertexAttachment_t1776545046::get_offset_of_worldVerticesLength_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (BlendMode_t1090287593)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1884[5] = 
{
	BlendMode_t1090287593::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (Bone_t1763862836), -1, sizeof(Bone_t1763862836_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1885[27] = 
{
	Bone_t1763862836_StaticFields::get_offset_of_yDown_0(),
	Bone_t1763862836::get_offset_of_data_1(),
	Bone_t1763862836::get_offset_of_skeleton_2(),
	Bone_t1763862836::get_offset_of_parent_3(),
	Bone_t1763862836::get_offset_of_children_4(),
	Bone_t1763862836::get_offset_of_x_5(),
	Bone_t1763862836::get_offset_of_y_6(),
	Bone_t1763862836::get_offset_of_rotation_7(),
	Bone_t1763862836::get_offset_of_scaleX_8(),
	Bone_t1763862836::get_offset_of_scaleY_9(),
	Bone_t1763862836::get_offset_of_shearX_10(),
	Bone_t1763862836::get_offset_of_shearY_11(),
	Bone_t1763862836::get_offset_of_ax_12(),
	Bone_t1763862836::get_offset_of_ay_13(),
	Bone_t1763862836::get_offset_of_arotation_14(),
	Bone_t1763862836::get_offset_of_ascaleX_15(),
	Bone_t1763862836::get_offset_of_ascaleY_16(),
	Bone_t1763862836::get_offset_of_ashearX_17(),
	Bone_t1763862836::get_offset_of_ashearY_18(),
	Bone_t1763862836::get_offset_of_appliedValid_19(),
	Bone_t1763862836::get_offset_of_a_20(),
	Bone_t1763862836::get_offset_of_b_21(),
	Bone_t1763862836::get_offset_of_worldX_22(),
	Bone_t1763862836::get_offset_of_c_23(),
	Bone_t1763862836::get_offset_of_d_24(),
	Bone_t1763862836::get_offset_of_worldY_25(),
	Bone_t1763862836::get_offset_of_sorted_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (BoneData_t1194659452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1886[12] = 
{
	BoneData_t1194659452::get_offset_of_index_0(),
	BoneData_t1194659452::get_offset_of_name_1(),
	BoneData_t1194659452::get_offset_of_parent_2(),
	BoneData_t1194659452::get_offset_of_length_3(),
	BoneData_t1194659452::get_offset_of_x_4(),
	BoneData_t1194659452::get_offset_of_y_5(),
	BoneData_t1194659452::get_offset_of_rotation_6(),
	BoneData_t1194659452::get_offset_of_scaleX_7(),
	BoneData_t1194659452::get_offset_of_scaleY_8(),
	BoneData_t1194659452::get_offset_of_shearX_9(),
	BoneData_t1194659452::get_offset_of_shearY_10(),
	BoneData_t1194659452::get_offset_of_transformMode_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (TransformMode_t1529364024)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1887[6] = 
{
	TransformMode_t1529364024::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (Event_t27715519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1888[5] = 
{
	Event_t27715519::get_offset_of_data_0(),
	Event_t27715519::get_offset_of_time_1(),
	Event_t27715519::get_offset_of_intValue_2(),
	Event_t27715519::get_offset_of_floatValue_3(),
	Event_t27715519::get_offset_of_stringValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (EventData_t295981108), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[4] = 
{
	EventData_t295981108::get_offset_of_name_0(),
	EventData_t295981108::get_offset_of_U3CIntU3Ek__BackingField_1(),
	EventData_t295981108::get_offset_of_U3CFloatU3Ek__BackingField_2(),
	EventData_t295981108::get_offset_of_U3CStringU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1891[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (IkConstraint_t2514691977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1893[5] = 
{
	IkConstraint_t2514691977::get_offset_of_data_0(),
	IkConstraint_t2514691977::get_offset_of_bones_1(),
	IkConstraint_t2514691977::get_offset_of_target_2(),
	IkConstraint_t2514691977::get_offset_of_mix_3(),
	IkConstraint_t2514691977::get_offset_of_bendDirection_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (IkConstraintData_t3215615709), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1894[6] = 
{
	IkConstraintData_t3215615709::get_offset_of_name_0(),
	IkConstraintData_t3215615709::get_offset_of_order_1(),
	IkConstraintData_t3215615709::get_offset_of_bones_2(),
	IkConstraintData_t3215615709::get_offset_of_target_3(),
	IkConstraintData_t3215615709::get_offset_of_bendDirection_4(),
	IkConstraintData_t3215615709::get_offset_of_mix_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (Json_t605086765), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (Lexer_t2494922068), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[6] = 
{
	Lexer_t2494922068::get_offset_of_U3ClineNumberU3Ek__BackingField_0(),
	Lexer_t2494922068::get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1(),
	Lexer_t2494922068::get_offset_of_json_2(),
	Lexer_t2494922068::get_offset_of_index_3(),
	Lexer_t2494922068::get_offset_of_success_4(),
	Lexer_t2494922068::get_offset_of_stringBuffer_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (Token_t1548208502)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1898[13] = 
{
	Token_t1548208502::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (JsonDecoder_t4269206925), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[3] = 
{
	JsonDecoder_t4269206925::get_offset_of_U3CerrorMessageU3Ek__BackingField_0(),
	JsonDecoder_t4269206925::get_offset_of_U3CparseNumbersAsFloatU3Ek__BackingField_1(),
	JsonDecoder_t4269206925::get_offset_of_lexer_2(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
