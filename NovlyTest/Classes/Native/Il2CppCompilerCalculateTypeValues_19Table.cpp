﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.Single[]
struct SingleU5BU5D_t3989243465;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t3688299677;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.AnimationClip>
struct IEqualityComparer_1_t3378665565;
// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh>
struct DoubleBuffered_1_t310388224;
// Spine.ExposedList`1<UnityEngine.Material>
struct ExposedList_1_t236386566;
// UnityEngine.Material[]
struct MaterialU5BU5D_t2885843518;
// UnityEngine.Mesh
struct Mesh_t3526862104;
// Spine.Unity.SkeletonRendererInstruction
struct SkeletonRendererInstruction_t672271390;
// Spine.Unity.Modules.SkeletonRagdoll
struct SkeletonRagdoll_t1253535559;
// Spine.ExposedList`1<Spine.Unity.SubmeshInstruction>
struct ExposedList_1_t1320945871;
// Spine.ExposedList`1<Spine.Attachment>
struct ExposedList_1_t2247752713;
// Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode[]
struct MixModeU5BU5D_t2391046855;
// System.Collections.Generic.Dictionary`2<System.Int32,Spine.Animation>
struct Dictionary_2_t3334673382;
// System.Collections.Generic.Dictionary`2<UnityEngine.AnimationClip,System.Int32>
struct Dictionary_2_t1084869635;
// System.Collections.Generic.List`1<Spine.Animation>
struct List_1_t4190559236;
// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo>
struct List_1_t1772203023;
// UnityEngine.Animator
struct Animator_t126418246;
// Spine.EventData
struct EventData_t295981108;
// System.String
struct String_t;
// Spine.AnimationState
struct AnimationState_t2858486651;
// Spine.Unity.Modules.SkeletonRagdoll2D
struct SkeletonRagdoll2D_t3940222091;
// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>>
struct ExposedList_1_t939324886;
// Spine.ExposedList`1<Spine.ExposedList`1<System.Int32>>
struct ExposedList_1_t3521067698;
// Spine.ExposedList`1<System.Int32>
struct ExposedList_1_t1671071859;
// Spine.ExposedList`1<System.Boolean>
struct ExposedList_1_t2889235099;
// Spine.Pool`1<Spine.ExposedList`1<System.Single>>
struct Pool_1_t3397012300;
// Spine.Pool`1<Spine.ExposedList`1<System.Int32>>
struct Pool_1_t1683787816;
// Spine.Unity.AtlasAsset
struct AtlasAsset_t2525633250;
// Spine.TransformConstraintData
struct TransformConstraintData_t1381040281;
// Spine.ExposedList`1<Spine.Bone>
struct ExposedList_1_t3613858675;
// Spine.Bone
struct Bone_t1763862836;
// Spine.PathConstraintData
struct PathConstraintData_t3185350348;
// Spine.Slot
struct Slot_t1804116343;
// Spine.ExposedList`1<System.Single>
struct ExposedList_1_t3384296343;
// Spine.SkeletonData
struct SkeletonData_t2665604974;
// Spine.ExposedList`1<Spine.Slot>
struct ExposedList_1_t3654112182;
// Spine.ExposedList`1<Spine.IkConstraint>
struct ExposedList_1_t69720520;
// Spine.ExposedList`1<Spine.TransformConstraint>
struct ExposedList_1_t2359534843;
// Spine.ExposedList`1<Spine.PathConstraint>
struct ExposedList_1_t3704659244;
// Spine.ExposedList`1<Spine.IUpdatable>
struct ExposedList_1_t1975705998;
// Spine.Skin
struct Skin_t1695237131;
// Spine.AttachmentLoader
struct AttachmentLoader_t3450253188;
// System.Byte[]
struct ByteU5BU5D_t371269159;
// System.Collections.Generic.List`1<Spine.SkeletonJson/LinkedMesh>
struct List_1_t1253086401;
// Spine.TransformMode[]
struct TransformModeU5BU5D_t1434233321;
// System.Int32[]
struct Int32U5BU5D_t281224829;
// Spine.ExposedList`1<Spine.Polygon>
struct ExposedList_1_t965638450;
// Spine.ExposedList`1<Spine.BoundingBoxAttachment>
struct ExposedList_1_t2876832010;
// Spine.ExposedList`1<Spine.BoneData>
struct ExposedList_1_t3044655291;
// Spine.BoneData
struct BoneData_t1194659452;
// Spine.ExposedList`1<Spine.SlotData>
struct ExposedList_1_t2437777689;
// Spine.ExposedList`1<Spine.Skin>
struct ExposedList_1_t3545232970;
// Spine.ExposedList`1<Spine.EventData>
struct ExposedList_1_t2145976947;
// Spine.ExposedList`1<Spine.Animation>
struct ExposedList_1_t3917139350;
// Spine.ExposedList`1<Spine.IkConstraintData>
struct ExposedList_1_t770644252;
// Spine.ExposedList`1<Spine.TransformConstraintData>
struct ExposedList_1_t3231036120;
// Spine.ExposedList`1<Spine.PathConstraintData>
struct ExposedList_1_t740378891;
// Spine.Triangulator
struct Triangulator_t1509316840;
// Spine.ClippingAttachment
struct ClippingAttachment_t906347500;
// System.Collections.Generic.Dictionary`2<Spine.Skin/AttachmentKeyTuple,Spine.Attachment>
struct Dictionary_2_t1889082438;
// Spine.SlotData
struct SlotData_t587781850;
// Spine.Attachment
struct Attachment_t397756874;
// Spine.MeshAttachment
struct MeshAttachment_t3550597918;
// System.Char[]
struct CharU5BU5D_t3669675803;
// UnityEngine.Texture2D
struct Texture2D_t1431210461;
// UnityEngine.Material
struct Material_t2681358023;
// UnityEngine.Transform
struct Transform_t2735953680;
// System.Void
struct Void_t3157035008;
// Spine.Skeleton
struct Skeleton_t1045203634;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t3550801758;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t3719718629;
// UnityEngine.Color32[]
struct Color32U5BU5D_t3058208114;
// Spine.Unity.MeshGenerator
struct MeshGenerator_t557654067;
// Spine.ExposedList`1<UnityEngine.Vector3>
struct ExposedList_1_t380703334;
// Spine.ExposedList`1<UnityEngine.Vector2>
struct ExposedList_1_t2915045931;
// Spine.ExposedList`1<UnityEngine.Color32>
struct ExposedList_1_t4261283554;
// Spine.SkeletonClipping
struct SkeletonClipping_t2504359873;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t829909980;
// Spine.Unity.Modules.SkeletonGhostRenderer
struct SkeletonGhostRenderer_t108436892;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t878649875;
// System.Collections.Generic.Dictionary`2<Spine.AtlasRegion,UnityEngine.Texture2D>
struct Dictionary_2_t923493323;
// System.Collections.Generic.List`1<UnityEngine.Texture2D>
struct List_1_t3554626186;
// System.IAsyncResult
struct IAsyncResult_t911908533;
// System.AsyncCallback
struct AsyncCallback_t3972436406;
// Spine.Unity.SkeletonRenderer
struct SkeletonRenderer_t1681389628;
// Spine.Unity.AtlasAsset[]
struct AtlasAssetU5BU5D_t635175767;
// UnityEngine.TextAsset
struct TextAsset_t3707725843;
// System.String[]
struct StringU5BU5D_t656593794;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t180203524;
// Spine.AnimationStateData
struct AnimationStateData_t1644285503;
// Spine.Atlas
struct Atlas_t604244430;
// Spine.Unity.ISkeletonAnimation
struct ISkeletonAnimation_t2072256521;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3668794496;
// System.Collections.Generic.Dictionary`2<Spine.Bone,UnityEngine.Transform>
struct Dictionary_2_t27791519;
// UnityEngine.Rigidbody
struct Rigidbody_t2418109227;
// Spine.Unity.SkeletonUtility
struct SkeletonUtility_t373332496;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t4047763079;
// UnityEngine.MeshFilter
struct MeshFilter_t36322911;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1621355309;
// Spine.Unity.SkeletonGraphic
struct SkeletonGraphic_t1867541356;
// Spine.Unity.MeshRendererBuffers
struct MeshRendererBuffers_t384051837;
// Spine.Unity.SkeletonUtilityBone
struct SkeletonUtilityBone_t1490890952;
// UnityEngine.GameObject
struct GameObject_t3093992626;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair>
struct List_1_t3853717968;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonPartsRenderer>
struct List_1_t1165396151;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t2982817174;
// System.Collections.Generic.Dictionary`2<Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair,UnityEngine.Material>
struct Dictionary_2_t722230041;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride>
struct List_1_t908661968;
// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride>
struct List_1_t2073729961;
// Spine.BoundingBoxAttachment
struct BoundingBoxAttachment_t1026836171;
// UnityEngine.PolygonCollider2D
struct PolygonCollider2D_t542995269;
// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,UnityEngine.PolygonCollider2D>
struct Dictionary_2_t569002753;
// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,System.String>
struct Dictionary_2_t1571386255;
// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate
struct SkeletonRendererDelegate_t878065760;
// Spine.Unity.MeshGeneratorDelegate
struct MeshGeneratorDelegate_t2067952305;
// Spine.Unity.SkeletonDataAsset
struct SkeletonDataAsset_t1706375087;
// System.Collections.Generic.List`1<Spine.Slot>
struct List_1_t3927532068;
// Spine.Unity.SkeletonRenderer/InstructionDelegate
struct InstructionDelegate_t1951844748;
// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material>
struct Dictionary_2_t2258539287;
// System.Collections.Generic.Dictionary`2<Spine.Slot,UnityEngine.Material>
struct Dictionary_2_t14780327;
// Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate
struct SkeletonUtilityDelegate_t1611778497;
// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityBone>
struct List_1_t3614306677;
// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityConstraint>
struct List_1_t4137673178;
// UnityEngine.Shader
struct Shader_t2404284061;
// Spine.Unity.Modules.SkeletonGhostRenderer[]
struct SkeletonGhostRendererU5BU5D_t2441972853;
// UnityEngine.Transform[]
struct TransformU5BU5D_t2219023025;
// Spine.Unity.UpdateBonesDelegate
struct UpdateBonesDelegate_t500272946;
// Spine.Unity.SkeletonAnimator/MecanimTranslator
struct MecanimTranslator_t3470518188;
// UnityEngine.RectTransform
struct RectTransform_t1087788885;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t123344510;
// UnityEngine.Canvas
struct Canvas_t452078485;
// UnityEngine.Events.UnityAction
struct UnityAction_t4110260629;
// UnityEngine.UI.VertexHelper
struct VertexHelper_t3002916242;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_t223451261;
// UnityEngine.UI.RectMask2D
struct RectMask2D_t475906393;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t3312162645;
// UnityEngine.Texture
struct Texture_t4046637001;

struct Vector3_t2825674791 ;
struct Vector2_t1065050092 ;
struct Color32_t2411287715 ;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef MATHUTILS_T2185402769_H
#define MATHUTILS_T2185402769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.MathUtils
struct  MathUtils_t2185402769  : public RuntimeObject
{
public:

public:
};

struct MathUtils_t2185402769_StaticFields
{
public:
	// System.Single[] Spine.MathUtils::sin
	SingleU5BU5D_t3989243465* ___sin_11;

public:
	inline static int32_t get_offset_of_sin_11() { return static_cast<int32_t>(offsetof(MathUtils_t2185402769_StaticFields, ___sin_11)); }
	inline SingleU5BU5D_t3989243465* get_sin_11() const { return ___sin_11; }
	inline SingleU5BU5D_t3989243465** get_address_of_sin_11() { return &___sin_11; }
	inline void set_sin_11(SingleU5BU5D_t3989243465* value)
	{
		___sin_11 = value;
		Il2CppCodeGenWriteBarrier((&___sin_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_T2185402769_H
#ifndef VALUETYPE_T2697047229_H
#define VALUETYPE_T2697047229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2697047229  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2697047229_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2697047229_marshaled_com
{
};
#endif // VALUETYPE_T2697047229_H
#ifndef SKELETONEXTENSIONS_T549969845_H
#define SKELETONEXTENSIONS_T549969845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonExtensions
struct  SkeletonExtensions_t549969845  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONEXTENSIONS_T549969845_H
#ifndef SKELETONEXTENSIONS_T1411540216_H
#define SKELETONEXTENSIONS_T1411540216_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonExtensions
struct  SkeletonExtensions_t1411540216  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONEXTENSIONS_T1411540216_H
#ifndef INTEQUALITYCOMPARER_T1300331196_H
#define INTEQUALITYCOMPARER_T1300331196_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/IntEqualityComparer
struct  IntEqualityComparer_t1300331196  : public RuntimeObject
{
public:

public:
};

struct IntEqualityComparer_t1300331196_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<System.Int32> Spine.Unity.SkeletonAnimator/MecanimTranslator/IntEqualityComparer::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(IntEqualityComparer_t1300331196_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTEQUALITYCOMPARER_T1300331196_H
#ifndef ANIMATIONCLIPEQUALITYCOMPARER_T1265784511_H
#define ANIMATIONCLIPEQUALITYCOMPARER_T1265784511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/AnimationClipEqualityComparer
struct  AnimationClipEqualityComparer_t1265784511  : public RuntimeObject
{
public:

public:
};

struct AnimationClipEqualityComparer_t1265784511_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<UnityEngine.AnimationClip> Spine.Unity.SkeletonAnimator/MecanimTranslator/AnimationClipEqualityComparer::Instance
	RuntimeObject* ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(AnimationClipEqualityComparer_t1265784511_StaticFields, ___Instance_0)); }
	inline RuntimeObject* get_Instance_0() const { return ___Instance_0; }
	inline RuntimeObject** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(RuntimeObject* value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONCLIPEQUALITYCOMPARER_T1265784511_H
#ifndef MESHRENDERERBUFFERS_T384051837_H
#define MESHRENDERERBUFFERS_T384051837_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshRendererBuffers
struct  MeshRendererBuffers_t384051837  : public RuntimeObject
{
public:
	// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh> Spine.Unity.MeshRendererBuffers::doubleBufferedMesh
	DoubleBuffered_1_t310388224 * ___doubleBufferedMesh_0;
	// Spine.ExposedList`1<UnityEngine.Material> Spine.Unity.MeshRendererBuffers::submeshMaterials
	ExposedList_1_t236386566 * ___submeshMaterials_1;
	// UnityEngine.Material[] Spine.Unity.MeshRendererBuffers::sharedMaterials
	MaterialU5BU5D_t2885843518* ___sharedMaterials_2;

public:
	inline static int32_t get_offset_of_doubleBufferedMesh_0() { return static_cast<int32_t>(offsetof(MeshRendererBuffers_t384051837, ___doubleBufferedMesh_0)); }
	inline DoubleBuffered_1_t310388224 * get_doubleBufferedMesh_0() const { return ___doubleBufferedMesh_0; }
	inline DoubleBuffered_1_t310388224 ** get_address_of_doubleBufferedMesh_0() { return &___doubleBufferedMesh_0; }
	inline void set_doubleBufferedMesh_0(DoubleBuffered_1_t310388224 * value)
	{
		___doubleBufferedMesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___doubleBufferedMesh_0), value);
	}

	inline static int32_t get_offset_of_submeshMaterials_1() { return static_cast<int32_t>(offsetof(MeshRendererBuffers_t384051837, ___submeshMaterials_1)); }
	inline ExposedList_1_t236386566 * get_submeshMaterials_1() const { return ___submeshMaterials_1; }
	inline ExposedList_1_t236386566 ** get_address_of_submeshMaterials_1() { return &___submeshMaterials_1; }
	inline void set_submeshMaterials_1(ExposedList_1_t236386566 * value)
	{
		___submeshMaterials_1 = value;
		Il2CppCodeGenWriteBarrier((&___submeshMaterials_1), value);
	}

	inline static int32_t get_offset_of_sharedMaterials_2() { return static_cast<int32_t>(offsetof(MeshRendererBuffers_t384051837, ___sharedMaterials_2)); }
	inline MaterialU5BU5D_t2885843518* get_sharedMaterials_2() const { return ___sharedMaterials_2; }
	inline MaterialU5BU5D_t2885843518** get_address_of_sharedMaterials_2() { return &___sharedMaterials_2; }
	inline void set_sharedMaterials_2(MaterialU5BU5D_t2885843518* value)
	{
		___sharedMaterials_2 = value;
		Il2CppCodeGenWriteBarrier((&___sharedMaterials_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHRENDERERBUFFERS_T384051837_H
#ifndef SMARTMESH_T2574154657_H
#define SMARTMESH_T2574154657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshRendererBuffers/SmartMesh
struct  SmartMesh_t2574154657  : public RuntimeObject
{
public:
	// UnityEngine.Mesh Spine.Unity.MeshRendererBuffers/SmartMesh::mesh
	Mesh_t3526862104 * ___mesh_0;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.MeshRendererBuffers/SmartMesh::instructionUsed
	SkeletonRendererInstruction_t672271390 * ___instructionUsed_1;

public:
	inline static int32_t get_offset_of_mesh_0() { return static_cast<int32_t>(offsetof(SmartMesh_t2574154657, ___mesh_0)); }
	inline Mesh_t3526862104 * get_mesh_0() const { return ___mesh_0; }
	inline Mesh_t3526862104 ** get_address_of_mesh_0() { return &___mesh_0; }
	inline void set_mesh_0(Mesh_t3526862104 * value)
	{
		___mesh_0 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_0), value);
	}

	inline static int32_t get_offset_of_instructionUsed_1() { return static_cast<int32_t>(offsetof(SmartMesh_t2574154657, ___instructionUsed_1)); }
	inline SkeletonRendererInstruction_t672271390 * get_instructionUsed_1() const { return ___instructionUsed_1; }
	inline SkeletonRendererInstruction_t672271390 ** get_address_of_instructionUsed_1() { return &___instructionUsed_1; }
	inline void set_instructionUsed_1(SkeletonRendererInstruction_t672271390 * value)
	{
		___instructionUsed_1 = value;
		Il2CppCodeGenWriteBarrier((&___instructionUsed_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMARTMESH_T2574154657_H
#ifndef U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T447401827_H
#define U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T447401827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1
struct  U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827  : public RuntimeObject
{
public:
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::<startMix>__0
	float ___U3CstartMixU3E__0_1;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::target
	float ___target_2;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::duration
	float ___duration_3;
	// Spine.Unity.Modules.SkeletonRagdoll Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::$this
	SkeletonRagdoll_t1253535559 * ___U24this_4;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll/<SmoothMixCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartMixU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827, ___U3CstartMixU3E__0_1)); }
	inline float get_U3CstartMixU3E__0_1() const { return ___U3CstartMixU3E__0_1; }
	inline float* get_address_of_U3CstartMixU3E__0_1() { return &___U3CstartMixU3E__0_1; }
	inline void set_U3CstartMixU3E__0_1(float value)
	{
		___U3CstartMixU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827, ___target_2)); }
	inline float get_target_2() const { return ___target_2; }
	inline float* get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(float value)
	{
		___target_2 = value;
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827, ___U24this_4)); }
	inline SkeletonRagdoll_t1253535559 * get_U24this_4() const { return ___U24this_4; }
	inline SkeletonRagdoll_t1253535559 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SkeletonRagdoll_t1253535559 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T447401827_H
#ifndef SKELETONRENDERERINSTRUCTION_T672271390_H
#define SKELETONRENDERERINSTRUCTION_T672271390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRendererInstruction
struct  SkeletonRendererInstruction_t672271390  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.SkeletonRendererInstruction::immutableTriangles
	bool ___immutableTriangles_0;
	// Spine.ExposedList`1<Spine.Unity.SubmeshInstruction> Spine.Unity.SkeletonRendererInstruction::submeshInstructions
	ExposedList_1_t1320945871 * ___submeshInstructions_1;
	// System.Boolean Spine.Unity.SkeletonRendererInstruction::hasActiveClipping
	bool ___hasActiveClipping_2;
	// System.Int32 Spine.Unity.SkeletonRendererInstruction::rawVertexCount
	int32_t ___rawVertexCount_3;
	// Spine.ExposedList`1<Spine.Attachment> Spine.Unity.SkeletonRendererInstruction::attachments
	ExposedList_1_t2247752713 * ___attachments_4;

public:
	inline static int32_t get_offset_of_immutableTriangles_0() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t672271390, ___immutableTriangles_0)); }
	inline bool get_immutableTriangles_0() const { return ___immutableTriangles_0; }
	inline bool* get_address_of_immutableTriangles_0() { return &___immutableTriangles_0; }
	inline void set_immutableTriangles_0(bool value)
	{
		___immutableTriangles_0 = value;
	}

	inline static int32_t get_offset_of_submeshInstructions_1() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t672271390, ___submeshInstructions_1)); }
	inline ExposedList_1_t1320945871 * get_submeshInstructions_1() const { return ___submeshInstructions_1; }
	inline ExposedList_1_t1320945871 ** get_address_of_submeshInstructions_1() { return &___submeshInstructions_1; }
	inline void set_submeshInstructions_1(ExposedList_1_t1320945871 * value)
	{
		___submeshInstructions_1 = value;
		Il2CppCodeGenWriteBarrier((&___submeshInstructions_1), value);
	}

	inline static int32_t get_offset_of_hasActiveClipping_2() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t672271390, ___hasActiveClipping_2)); }
	inline bool get_hasActiveClipping_2() const { return ___hasActiveClipping_2; }
	inline bool* get_address_of_hasActiveClipping_2() { return &___hasActiveClipping_2; }
	inline void set_hasActiveClipping_2(bool value)
	{
		___hasActiveClipping_2 = value;
	}

	inline static int32_t get_offset_of_rawVertexCount_3() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t672271390, ___rawVertexCount_3)); }
	inline int32_t get_rawVertexCount_3() const { return ___rawVertexCount_3; }
	inline int32_t* get_address_of_rawVertexCount_3() { return &___rawVertexCount_3; }
	inline void set_rawVertexCount_3(int32_t value)
	{
		___rawVertexCount_3 = value;
	}

	inline static int32_t get_offset_of_attachments_4() { return static_cast<int32_t>(offsetof(SkeletonRendererInstruction_t672271390, ___attachments_4)); }
	inline ExposedList_1_t2247752713 * get_attachments_4() const { return ___attachments_4; }
	inline ExposedList_1_t2247752713 ** get_address_of_attachments_4() { return &___attachments_4; }
	inline void set_attachments_4(ExposedList_1_t2247752713 * value)
	{
		___attachments_4 = value;
		Il2CppCodeGenWriteBarrier((&___attachments_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERERINSTRUCTION_T672271390_H
#ifndef MECANIMTRANSLATOR_T3470518188_H
#define MECANIMTRANSLATOR_T3470518188_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator
struct  MecanimTranslator_t3470518188  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.SkeletonAnimator/MecanimTranslator::autoReset
	bool ___autoReset_0;
	// Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode[] Spine.Unity.SkeletonAnimator/MecanimTranslator::layerMixModes
	MixModeU5BU5D_t2391046855* ___layerMixModes_1;
	// System.Collections.Generic.Dictionary`2<System.Int32,Spine.Animation> Spine.Unity.SkeletonAnimator/MecanimTranslator::animationTable
	Dictionary_2_t3334673382 * ___animationTable_2;
	// System.Collections.Generic.Dictionary`2<UnityEngine.AnimationClip,System.Int32> Spine.Unity.SkeletonAnimator/MecanimTranslator::clipNameHashCodeTable
	Dictionary_2_t1084869635 * ___clipNameHashCodeTable_3;
	// System.Collections.Generic.List`1<Spine.Animation> Spine.Unity.SkeletonAnimator/MecanimTranslator::previousAnimations
	List_1_t4190559236 * ___previousAnimations_4;
	// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo> Spine.Unity.SkeletonAnimator/MecanimTranslator::clipInfoCache
	List_1_t1772203023 * ___clipInfoCache_5;
	// System.Collections.Generic.List`1<UnityEngine.AnimatorClipInfo> Spine.Unity.SkeletonAnimator/MecanimTranslator::nextClipInfoCache
	List_1_t1772203023 * ___nextClipInfoCache_6;
	// UnityEngine.Animator Spine.Unity.SkeletonAnimator/MecanimTranslator::animator
	Animator_t126418246 * ___animator_7;

public:
	inline static int32_t get_offset_of_autoReset_0() { return static_cast<int32_t>(offsetof(MecanimTranslator_t3470518188, ___autoReset_0)); }
	inline bool get_autoReset_0() const { return ___autoReset_0; }
	inline bool* get_address_of_autoReset_0() { return &___autoReset_0; }
	inline void set_autoReset_0(bool value)
	{
		___autoReset_0 = value;
	}

	inline static int32_t get_offset_of_layerMixModes_1() { return static_cast<int32_t>(offsetof(MecanimTranslator_t3470518188, ___layerMixModes_1)); }
	inline MixModeU5BU5D_t2391046855* get_layerMixModes_1() const { return ___layerMixModes_1; }
	inline MixModeU5BU5D_t2391046855** get_address_of_layerMixModes_1() { return &___layerMixModes_1; }
	inline void set_layerMixModes_1(MixModeU5BU5D_t2391046855* value)
	{
		___layerMixModes_1 = value;
		Il2CppCodeGenWriteBarrier((&___layerMixModes_1), value);
	}

	inline static int32_t get_offset_of_animationTable_2() { return static_cast<int32_t>(offsetof(MecanimTranslator_t3470518188, ___animationTable_2)); }
	inline Dictionary_2_t3334673382 * get_animationTable_2() const { return ___animationTable_2; }
	inline Dictionary_2_t3334673382 ** get_address_of_animationTable_2() { return &___animationTable_2; }
	inline void set_animationTable_2(Dictionary_2_t3334673382 * value)
	{
		___animationTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___animationTable_2), value);
	}

	inline static int32_t get_offset_of_clipNameHashCodeTable_3() { return static_cast<int32_t>(offsetof(MecanimTranslator_t3470518188, ___clipNameHashCodeTable_3)); }
	inline Dictionary_2_t1084869635 * get_clipNameHashCodeTable_3() const { return ___clipNameHashCodeTable_3; }
	inline Dictionary_2_t1084869635 ** get_address_of_clipNameHashCodeTable_3() { return &___clipNameHashCodeTable_3; }
	inline void set_clipNameHashCodeTable_3(Dictionary_2_t1084869635 * value)
	{
		___clipNameHashCodeTable_3 = value;
		Il2CppCodeGenWriteBarrier((&___clipNameHashCodeTable_3), value);
	}

	inline static int32_t get_offset_of_previousAnimations_4() { return static_cast<int32_t>(offsetof(MecanimTranslator_t3470518188, ___previousAnimations_4)); }
	inline List_1_t4190559236 * get_previousAnimations_4() const { return ___previousAnimations_4; }
	inline List_1_t4190559236 ** get_address_of_previousAnimations_4() { return &___previousAnimations_4; }
	inline void set_previousAnimations_4(List_1_t4190559236 * value)
	{
		___previousAnimations_4 = value;
		Il2CppCodeGenWriteBarrier((&___previousAnimations_4), value);
	}

	inline static int32_t get_offset_of_clipInfoCache_5() { return static_cast<int32_t>(offsetof(MecanimTranslator_t3470518188, ___clipInfoCache_5)); }
	inline List_1_t1772203023 * get_clipInfoCache_5() const { return ___clipInfoCache_5; }
	inline List_1_t1772203023 ** get_address_of_clipInfoCache_5() { return &___clipInfoCache_5; }
	inline void set_clipInfoCache_5(List_1_t1772203023 * value)
	{
		___clipInfoCache_5 = value;
		Il2CppCodeGenWriteBarrier((&___clipInfoCache_5), value);
	}

	inline static int32_t get_offset_of_nextClipInfoCache_6() { return static_cast<int32_t>(offsetof(MecanimTranslator_t3470518188, ___nextClipInfoCache_6)); }
	inline List_1_t1772203023 * get_nextClipInfoCache_6() const { return ___nextClipInfoCache_6; }
	inline List_1_t1772203023 ** get_address_of_nextClipInfoCache_6() { return &___nextClipInfoCache_6; }
	inline void set_nextClipInfoCache_6(List_1_t1772203023 * value)
	{
		___nextClipInfoCache_6 = value;
		Il2CppCodeGenWriteBarrier((&___nextClipInfoCache_6), value);
	}

	inline static int32_t get_offset_of_animator_7() { return static_cast<int32_t>(offsetof(MecanimTranslator_t3470518188, ___animator_7)); }
	inline Animator_t126418246 * get_animator_7() const { return ___animator_7; }
	inline Animator_t126418246 ** get_address_of_animator_7() { return &___animator_7; }
	inline void set_animator_7(Animator_t126418246 * value)
	{
		___animator_7 = value;
		Il2CppCodeGenWriteBarrier((&___animator_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MECANIMTRANSLATOR_T3470518188_H
#ifndef SKINUTILITIES_T4185041917_H
#define SKINUTILITIES_T4185041917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.SkinUtilities
struct  SkinUtilities_t4185041917  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKINUTILITIES_T4185041917_H
#ifndef ATTACHMENTCLONEEXTENSIONS_T2643942632_H
#define ATTACHMENTCLONEEXTENSIONS_T2643942632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.AttachmentCloneExtensions
struct  AttachmentCloneExtensions_t2643942632  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTCLONEEXTENSIONS_T2643942632_H
#ifndef WAITFORSPINETRACKENTRYEND_T2837745715_H
#define WAITFORSPINETRACKENTRYEND_T2837745715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineTrackEntryEnd
struct  WaitForSpineTrackEntryEnd_t2837745715  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.WaitForSpineTrackEntryEnd::m_WasFired
	bool ___m_WasFired_0;

public:
	inline static int32_t get_offset_of_m_WasFired_0() { return static_cast<int32_t>(offsetof(WaitForSpineTrackEntryEnd_t2837745715, ___m_WasFired_0)); }
	inline bool get_m_WasFired_0() const { return ___m_WasFired_0; }
	inline bool* get_address_of_m_WasFired_0() { return &___m_WasFired_0; }
	inline void set_m_WasFired_0(bool value)
	{
		___m_WasFired_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINETRACKENTRYEND_T2837745715_H
#ifndef WAITFORSPINEEVENT_T3387552986_H
#define WAITFORSPINEEVENT_T3387552986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineEvent
struct  WaitForSpineEvent_t3387552986  : public RuntimeObject
{
public:
	// Spine.EventData Spine.Unity.WaitForSpineEvent::m_TargetEvent
	EventData_t295981108 * ___m_TargetEvent_0;
	// System.String Spine.Unity.WaitForSpineEvent::m_EventName
	String_t* ___m_EventName_1;
	// Spine.AnimationState Spine.Unity.WaitForSpineEvent::m_AnimationState
	AnimationState_t2858486651 * ___m_AnimationState_2;
	// System.Boolean Spine.Unity.WaitForSpineEvent::m_WasFired
	bool ___m_WasFired_3;
	// System.Boolean Spine.Unity.WaitForSpineEvent::m_unsubscribeAfterFiring
	bool ___m_unsubscribeAfterFiring_4;

public:
	inline static int32_t get_offset_of_m_TargetEvent_0() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3387552986, ___m_TargetEvent_0)); }
	inline EventData_t295981108 * get_m_TargetEvent_0() const { return ___m_TargetEvent_0; }
	inline EventData_t295981108 ** get_address_of_m_TargetEvent_0() { return &___m_TargetEvent_0; }
	inline void set_m_TargetEvent_0(EventData_t295981108 * value)
	{
		___m_TargetEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetEvent_0), value);
	}

	inline static int32_t get_offset_of_m_EventName_1() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3387552986, ___m_EventName_1)); }
	inline String_t* get_m_EventName_1() const { return ___m_EventName_1; }
	inline String_t** get_address_of_m_EventName_1() { return &___m_EventName_1; }
	inline void set_m_EventName_1(String_t* value)
	{
		___m_EventName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_1), value);
	}

	inline static int32_t get_offset_of_m_AnimationState_2() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3387552986, ___m_AnimationState_2)); }
	inline AnimationState_t2858486651 * get_m_AnimationState_2() const { return ___m_AnimationState_2; }
	inline AnimationState_t2858486651 ** get_address_of_m_AnimationState_2() { return &___m_AnimationState_2; }
	inline void set_m_AnimationState_2(AnimationState_t2858486651 * value)
	{
		___m_AnimationState_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationState_2), value);
	}

	inline static int32_t get_offset_of_m_WasFired_3() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3387552986, ___m_WasFired_3)); }
	inline bool get_m_WasFired_3() const { return ___m_WasFired_3; }
	inline bool* get_address_of_m_WasFired_3() { return &___m_WasFired_3; }
	inline void set_m_WasFired_3(bool value)
	{
		___m_WasFired_3 = value;
	}

	inline static int32_t get_offset_of_m_unsubscribeAfterFiring_4() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3387552986, ___m_unsubscribeAfterFiring_4)); }
	inline bool get_m_unsubscribeAfterFiring_4() const { return ___m_unsubscribeAfterFiring_4; }
	inline bool* get_address_of_m_unsubscribeAfterFiring_4() { return &___m_unsubscribeAfterFiring_4; }
	inline void set_m_unsubscribeAfterFiring_4(bool value)
	{
		___m_unsubscribeAfterFiring_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINEEVENT_T3387552986_H
#ifndef WAITFORSPINEANIMATIONCOMPLETE_T694865537_H
#define WAITFORSPINEANIMATIONCOMPLETE_T694865537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineAnimationComplete
struct  WaitForSpineAnimationComplete_t694865537  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.WaitForSpineAnimationComplete::m_WasFired
	bool ___m_WasFired_0;

public:
	inline static int32_t get_offset_of_m_WasFired_0() { return static_cast<int32_t>(offsetof(WaitForSpineAnimationComplete_t694865537, ___m_WasFired_0)); }
	inline bool get_m_WasFired_0() const { return ___m_WasFired_0; }
	inline bool* get_address_of_m_WasFired_0() { return &___m_WasFired_0; }
	inline void set_m_WasFired_0(bool value)
	{
		___m_WasFired_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINEANIMATIONCOMPLETE_T694865537_H
#ifndef U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T4243939462_H
#define U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T4243939462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1
struct  U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462  : public RuntimeObject
{
public:
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::<startTime>__0
	float ___U3CstartTimeU3E__0_0;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::<startMix>__0
	float ___U3CstartMixU3E__0_1;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::target
	float ___target_2;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::duration
	float ___duration_3;
	// Spine.Unity.Modules.SkeletonRagdoll2D Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::$this
	SkeletonRagdoll2D_t3940222091 * ___U24this_4;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll2D/<SmoothMixCoroutine>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CstartTimeU3E__0_0() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462, ___U3CstartTimeU3E__0_0)); }
	inline float get_U3CstartTimeU3E__0_0() const { return ___U3CstartTimeU3E__0_0; }
	inline float* get_address_of_U3CstartTimeU3E__0_0() { return &___U3CstartTimeU3E__0_0; }
	inline void set_U3CstartTimeU3E__0_0(float value)
	{
		___U3CstartTimeU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CstartMixU3E__0_1() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462, ___U3CstartMixU3E__0_1)); }
	inline float get_U3CstartMixU3E__0_1() const { return ___U3CstartMixU3E__0_1; }
	inline float* get_address_of_U3CstartMixU3E__0_1() { return &___U3CstartMixU3E__0_1; }
	inline void set_U3CstartMixU3E__0_1(float value)
	{
		___U3CstartMixU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462, ___target_2)); }
	inline float get_target_2() const { return ___target_2; }
	inline float* get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(float value)
	{
		___target_2 = value;
	}

	inline static int32_t get_offset_of_duration_3() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462, ___duration_3)); }
	inline float get_duration_3() const { return ___duration_3; }
	inline float* get_address_of_duration_3() { return &___duration_3; }
	inline void set_duration_3(float value)
	{
		___duration_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462, ___U24this_4)); }
	inline SkeletonRagdoll2D_t3940222091 * get_U24this_4() const { return ___U24this_4; }
	inline SkeletonRagdoll2D_t3940222091 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SkeletonRagdoll2D_t3940222091 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSMOOTHMIXCOROUTINEU3EC__ITERATOR1_T4243939462_H
#ifndef U3CSTARTU3EC__ITERATOR0_T534358340_H
#define U3CSTARTU3EC__ITERATOR0_T534358340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t534358340  : public RuntimeObject
{
public:
	// Spine.Unity.Modules.SkeletonRagdoll2D Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0::$this
	SkeletonRagdoll2D_t3940222091 * ___U24this_0;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll2D/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t534358340, ___U24this_0)); }
	inline SkeletonRagdoll2D_t3940222091 * get_U24this_0() const { return ___U24this_0; }
	inline SkeletonRagdoll2D_t3940222091 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SkeletonRagdoll2D_t3940222091 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t534358340, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t534358340, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t534358340, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T534358340_H
#ifndef ATTACHMENTREGIONEXTENSIONS_T3872463489_H
#define ATTACHMENTREGIONEXTENSIONS_T3872463489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.AttachmentRegionExtensions
struct  AttachmentRegionExtensions_t3872463489  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTREGIONEXTENSIONS_T3872463489_H
#ifndef TRIANGULATOR_T1509316840_H
#define TRIANGULATOR_T1509316840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Triangulator
struct  Triangulator_t1509316840  : public RuntimeObject
{
public:
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>> Spine.Triangulator::convexPolygons
	ExposedList_1_t939324886 * ___convexPolygons_0;
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Int32>> Spine.Triangulator::convexPolygonsIndices
	ExposedList_1_t3521067698 * ___convexPolygonsIndices_1;
	// Spine.ExposedList`1<System.Int32> Spine.Triangulator::indicesArray
	ExposedList_1_t1671071859 * ___indicesArray_2;
	// Spine.ExposedList`1<System.Boolean> Spine.Triangulator::isConcaveArray
	ExposedList_1_t2889235099 * ___isConcaveArray_3;
	// Spine.ExposedList`1<System.Int32> Spine.Triangulator::triangles
	ExposedList_1_t1671071859 * ___triangles_4;
	// Spine.Pool`1<Spine.ExposedList`1<System.Single>> Spine.Triangulator::polygonPool
	Pool_1_t3397012300 * ___polygonPool_5;
	// Spine.Pool`1<Spine.ExposedList`1<System.Int32>> Spine.Triangulator::polygonIndicesPool
	Pool_1_t1683787816 * ___polygonIndicesPool_6;

public:
	inline static int32_t get_offset_of_convexPolygons_0() { return static_cast<int32_t>(offsetof(Triangulator_t1509316840, ___convexPolygons_0)); }
	inline ExposedList_1_t939324886 * get_convexPolygons_0() const { return ___convexPolygons_0; }
	inline ExposedList_1_t939324886 ** get_address_of_convexPolygons_0() { return &___convexPolygons_0; }
	inline void set_convexPolygons_0(ExposedList_1_t939324886 * value)
	{
		___convexPolygons_0 = value;
		Il2CppCodeGenWriteBarrier((&___convexPolygons_0), value);
	}

	inline static int32_t get_offset_of_convexPolygonsIndices_1() { return static_cast<int32_t>(offsetof(Triangulator_t1509316840, ___convexPolygonsIndices_1)); }
	inline ExposedList_1_t3521067698 * get_convexPolygonsIndices_1() const { return ___convexPolygonsIndices_1; }
	inline ExposedList_1_t3521067698 ** get_address_of_convexPolygonsIndices_1() { return &___convexPolygonsIndices_1; }
	inline void set_convexPolygonsIndices_1(ExposedList_1_t3521067698 * value)
	{
		___convexPolygonsIndices_1 = value;
		Il2CppCodeGenWriteBarrier((&___convexPolygonsIndices_1), value);
	}

	inline static int32_t get_offset_of_indicesArray_2() { return static_cast<int32_t>(offsetof(Triangulator_t1509316840, ___indicesArray_2)); }
	inline ExposedList_1_t1671071859 * get_indicesArray_2() const { return ___indicesArray_2; }
	inline ExposedList_1_t1671071859 ** get_address_of_indicesArray_2() { return &___indicesArray_2; }
	inline void set_indicesArray_2(ExposedList_1_t1671071859 * value)
	{
		___indicesArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___indicesArray_2), value);
	}

	inline static int32_t get_offset_of_isConcaveArray_3() { return static_cast<int32_t>(offsetof(Triangulator_t1509316840, ___isConcaveArray_3)); }
	inline ExposedList_1_t2889235099 * get_isConcaveArray_3() const { return ___isConcaveArray_3; }
	inline ExposedList_1_t2889235099 ** get_address_of_isConcaveArray_3() { return &___isConcaveArray_3; }
	inline void set_isConcaveArray_3(ExposedList_1_t2889235099 * value)
	{
		___isConcaveArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___isConcaveArray_3), value);
	}

	inline static int32_t get_offset_of_triangles_4() { return static_cast<int32_t>(offsetof(Triangulator_t1509316840, ___triangles_4)); }
	inline ExposedList_1_t1671071859 * get_triangles_4() const { return ___triangles_4; }
	inline ExposedList_1_t1671071859 ** get_address_of_triangles_4() { return &___triangles_4; }
	inline void set_triangles_4(ExposedList_1_t1671071859 * value)
	{
		___triangles_4 = value;
		Il2CppCodeGenWriteBarrier((&___triangles_4), value);
	}

	inline static int32_t get_offset_of_polygonPool_5() { return static_cast<int32_t>(offsetof(Triangulator_t1509316840, ___polygonPool_5)); }
	inline Pool_1_t3397012300 * get_polygonPool_5() const { return ___polygonPool_5; }
	inline Pool_1_t3397012300 ** get_address_of_polygonPool_5() { return &___polygonPool_5; }
	inline void set_polygonPool_5(Pool_1_t3397012300 * value)
	{
		___polygonPool_5 = value;
		Il2CppCodeGenWriteBarrier((&___polygonPool_5), value);
	}

	inline static int32_t get_offset_of_polygonIndicesPool_6() { return static_cast<int32_t>(offsetof(Triangulator_t1509316840, ___polygonIndicesPool_6)); }
	inline Pool_1_t1683787816 * get_polygonIndicesPool_6() const { return ___polygonIndicesPool_6; }
	inline Pool_1_t1683787816 ** get_address_of_polygonIndicesPool_6() { return &___polygonIndicesPool_6; }
	inline void set_polygonIndicesPool_6(Pool_1_t1683787816 * value)
	{
		___polygonIndicesPool_6 = value;
		Il2CppCodeGenWriteBarrier((&___polygonIndicesPool_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIANGULATOR_T1509316840_H
#ifndef MATERIALSTEXTURELOADER_T3752126477_H
#define MATERIALSTEXTURELOADER_T3752126477_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MaterialsTextureLoader
struct  MaterialsTextureLoader_t3752126477  : public RuntimeObject
{
public:
	// Spine.Unity.AtlasAsset Spine.Unity.MaterialsTextureLoader::atlasAsset
	AtlasAsset_t2525633250 * ___atlasAsset_0;

public:
	inline static int32_t get_offset_of_atlasAsset_0() { return static_cast<int32_t>(offsetof(MaterialsTextureLoader_t3752126477, ___atlasAsset_0)); }
	inline AtlasAsset_t2525633250 * get_atlasAsset_0() const { return ___atlasAsset_0; }
	inline AtlasAsset_t2525633250 ** get_address_of_atlasAsset_0() { return &___atlasAsset_0; }
	inline void set_atlasAsset_0(AtlasAsset_t2525633250 * value)
	{
		___atlasAsset_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAsset_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALSTEXTURELOADER_T3752126477_H
#ifndef TRANSFORMCONSTRAINT_T509539004_H
#define TRANSFORMCONSTRAINT_T509539004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformConstraint
struct  TransformConstraint_t509539004  : public RuntimeObject
{
public:
	// Spine.TransformConstraintData Spine.TransformConstraint::data
	TransformConstraintData_t1381040281 * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.TransformConstraint::bones
	ExposedList_1_t3613858675 * ___bones_1;
	// Spine.Bone Spine.TransformConstraint::target
	Bone_t1763862836 * ___target_2;
	// System.Single Spine.TransformConstraint::rotateMix
	float ___rotateMix_3;
	// System.Single Spine.TransformConstraint::translateMix
	float ___translateMix_4;
	// System.Single Spine.TransformConstraint::scaleMix
	float ___scaleMix_5;
	// System.Single Spine.TransformConstraint::shearMix
	float ___shearMix_6;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(TransformConstraint_t509539004, ___data_0)); }
	inline TransformConstraintData_t1381040281 * get_data_0() const { return ___data_0; }
	inline TransformConstraintData_t1381040281 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(TransformConstraintData_t1381040281 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(TransformConstraint_t509539004, ___bones_1)); }
	inline ExposedList_1_t3613858675 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t3613858675 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t3613858675 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(TransformConstraint_t509539004, ___target_2)); }
	inline Bone_t1763862836 * get_target_2() const { return ___target_2; }
	inline Bone_t1763862836 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Bone_t1763862836 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_rotateMix_3() { return static_cast<int32_t>(offsetof(TransformConstraint_t509539004, ___rotateMix_3)); }
	inline float get_rotateMix_3() const { return ___rotateMix_3; }
	inline float* get_address_of_rotateMix_3() { return &___rotateMix_3; }
	inline void set_rotateMix_3(float value)
	{
		___rotateMix_3 = value;
	}

	inline static int32_t get_offset_of_translateMix_4() { return static_cast<int32_t>(offsetof(TransformConstraint_t509539004, ___translateMix_4)); }
	inline float get_translateMix_4() const { return ___translateMix_4; }
	inline float* get_address_of_translateMix_4() { return &___translateMix_4; }
	inline void set_translateMix_4(float value)
	{
		___translateMix_4 = value;
	}

	inline static int32_t get_offset_of_scaleMix_5() { return static_cast<int32_t>(offsetof(TransformConstraint_t509539004, ___scaleMix_5)); }
	inline float get_scaleMix_5() const { return ___scaleMix_5; }
	inline float* get_address_of_scaleMix_5() { return &___scaleMix_5; }
	inline void set_scaleMix_5(float value)
	{
		___scaleMix_5 = value;
	}

	inline static int32_t get_offset_of_shearMix_6() { return static_cast<int32_t>(offsetof(TransformConstraint_t509539004, ___shearMix_6)); }
	inline float get_shearMix_6() const { return ___shearMix_6; }
	inline float* get_address_of_shearMix_6() { return &___shearMix_6; }
	inline void set_shearMix_6(float value)
	{
		___shearMix_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONSTRAINT_T509539004_H
#ifndef PATHCONSTRAINT_T1854663405_H
#define PATHCONSTRAINT_T1854663405_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraint
struct  PathConstraint_t1854663405  : public RuntimeObject
{
public:
	// Spine.PathConstraintData Spine.PathConstraint::data
	PathConstraintData_t3185350348 * ___data_4;
	// Spine.ExposedList`1<Spine.Bone> Spine.PathConstraint::bones
	ExposedList_1_t3613858675 * ___bones_5;
	// Spine.Slot Spine.PathConstraint::target
	Slot_t1804116343 * ___target_6;
	// System.Single Spine.PathConstraint::position
	float ___position_7;
	// System.Single Spine.PathConstraint::spacing
	float ___spacing_8;
	// System.Single Spine.PathConstraint::rotateMix
	float ___rotateMix_9;
	// System.Single Spine.PathConstraint::translateMix
	float ___translateMix_10;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::spaces
	ExposedList_1_t3384296343 * ___spaces_11;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::positions
	ExposedList_1_t3384296343 * ___positions_12;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::world
	ExposedList_1_t3384296343 * ___world_13;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::curves
	ExposedList_1_t3384296343 * ___curves_14;
	// Spine.ExposedList`1<System.Single> Spine.PathConstraint::lengths
	ExposedList_1_t3384296343 * ___lengths_15;
	// System.Single[] Spine.PathConstraint::segments
	SingleU5BU5D_t3989243465* ___segments_16;

public:
	inline static int32_t get_offset_of_data_4() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___data_4)); }
	inline PathConstraintData_t3185350348 * get_data_4() const { return ___data_4; }
	inline PathConstraintData_t3185350348 ** get_address_of_data_4() { return &___data_4; }
	inline void set_data_4(PathConstraintData_t3185350348 * value)
	{
		___data_4 = value;
		Il2CppCodeGenWriteBarrier((&___data_4), value);
	}

	inline static int32_t get_offset_of_bones_5() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___bones_5)); }
	inline ExposedList_1_t3613858675 * get_bones_5() const { return ___bones_5; }
	inline ExposedList_1_t3613858675 ** get_address_of_bones_5() { return &___bones_5; }
	inline void set_bones_5(ExposedList_1_t3613858675 * value)
	{
		___bones_5 = value;
		Il2CppCodeGenWriteBarrier((&___bones_5), value);
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___target_6)); }
	inline Slot_t1804116343 * get_target_6() const { return ___target_6; }
	inline Slot_t1804116343 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Slot_t1804116343 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_position_7() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___position_7)); }
	inline float get_position_7() const { return ___position_7; }
	inline float* get_address_of_position_7() { return &___position_7; }
	inline void set_position_7(float value)
	{
		___position_7 = value;
	}

	inline static int32_t get_offset_of_spacing_8() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___spacing_8)); }
	inline float get_spacing_8() const { return ___spacing_8; }
	inline float* get_address_of_spacing_8() { return &___spacing_8; }
	inline void set_spacing_8(float value)
	{
		___spacing_8 = value;
	}

	inline static int32_t get_offset_of_rotateMix_9() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___rotateMix_9)); }
	inline float get_rotateMix_9() const { return ___rotateMix_9; }
	inline float* get_address_of_rotateMix_9() { return &___rotateMix_9; }
	inline void set_rotateMix_9(float value)
	{
		___rotateMix_9 = value;
	}

	inline static int32_t get_offset_of_translateMix_10() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___translateMix_10)); }
	inline float get_translateMix_10() const { return ___translateMix_10; }
	inline float* get_address_of_translateMix_10() { return &___translateMix_10; }
	inline void set_translateMix_10(float value)
	{
		___translateMix_10 = value;
	}

	inline static int32_t get_offset_of_spaces_11() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___spaces_11)); }
	inline ExposedList_1_t3384296343 * get_spaces_11() const { return ___spaces_11; }
	inline ExposedList_1_t3384296343 ** get_address_of_spaces_11() { return &___spaces_11; }
	inline void set_spaces_11(ExposedList_1_t3384296343 * value)
	{
		___spaces_11 = value;
		Il2CppCodeGenWriteBarrier((&___spaces_11), value);
	}

	inline static int32_t get_offset_of_positions_12() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___positions_12)); }
	inline ExposedList_1_t3384296343 * get_positions_12() const { return ___positions_12; }
	inline ExposedList_1_t3384296343 ** get_address_of_positions_12() { return &___positions_12; }
	inline void set_positions_12(ExposedList_1_t3384296343 * value)
	{
		___positions_12 = value;
		Il2CppCodeGenWriteBarrier((&___positions_12), value);
	}

	inline static int32_t get_offset_of_world_13() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___world_13)); }
	inline ExposedList_1_t3384296343 * get_world_13() const { return ___world_13; }
	inline ExposedList_1_t3384296343 ** get_address_of_world_13() { return &___world_13; }
	inline void set_world_13(ExposedList_1_t3384296343 * value)
	{
		___world_13 = value;
		Il2CppCodeGenWriteBarrier((&___world_13), value);
	}

	inline static int32_t get_offset_of_curves_14() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___curves_14)); }
	inline ExposedList_1_t3384296343 * get_curves_14() const { return ___curves_14; }
	inline ExposedList_1_t3384296343 ** get_address_of_curves_14() { return &___curves_14; }
	inline void set_curves_14(ExposedList_1_t3384296343 * value)
	{
		___curves_14 = value;
		Il2CppCodeGenWriteBarrier((&___curves_14), value);
	}

	inline static int32_t get_offset_of_lengths_15() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___lengths_15)); }
	inline ExposedList_1_t3384296343 * get_lengths_15() const { return ___lengths_15; }
	inline ExposedList_1_t3384296343 ** get_address_of_lengths_15() { return &___lengths_15; }
	inline void set_lengths_15(ExposedList_1_t3384296343 * value)
	{
		___lengths_15 = value;
		Il2CppCodeGenWriteBarrier((&___lengths_15), value);
	}

	inline static int32_t get_offset_of_segments_16() { return static_cast<int32_t>(offsetof(PathConstraint_t1854663405, ___segments_16)); }
	inline SingleU5BU5D_t3989243465* get_segments_16() const { return ___segments_16; }
	inline SingleU5BU5D_t3989243465** get_address_of_segments_16() { return &___segments_16; }
	inline void set_segments_16(SingleU5BU5D_t3989243465* value)
	{
		___segments_16 = value;
		Il2CppCodeGenWriteBarrier((&___segments_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINT_T1854663405_H
#ifndef ATTRIBUTE_T2169343654_H
#define ATTRIBUTE_T2169343654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t2169343654  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T2169343654_H
#ifndef SKELETON_T1045203634_H
#define SKELETON_T1045203634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skeleton
struct  Skeleton_t1045203634  : public RuntimeObject
{
public:
	// Spine.SkeletonData Spine.Skeleton::data
	SkeletonData_t2665604974 * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.Skeleton::bones
	ExposedList_1_t3613858675 * ___bones_1;
	// Spine.ExposedList`1<Spine.Slot> Spine.Skeleton::slots
	ExposedList_1_t3654112182 * ___slots_2;
	// Spine.ExposedList`1<Spine.Slot> Spine.Skeleton::drawOrder
	ExposedList_1_t3654112182 * ___drawOrder_3;
	// Spine.ExposedList`1<Spine.IkConstraint> Spine.Skeleton::ikConstraints
	ExposedList_1_t69720520 * ___ikConstraints_4;
	// Spine.ExposedList`1<Spine.TransformConstraint> Spine.Skeleton::transformConstraints
	ExposedList_1_t2359534843 * ___transformConstraints_5;
	// Spine.ExposedList`1<Spine.PathConstraint> Spine.Skeleton::pathConstraints
	ExposedList_1_t3704659244 * ___pathConstraints_6;
	// Spine.ExposedList`1<Spine.IUpdatable> Spine.Skeleton::updateCache
	ExposedList_1_t1975705998 * ___updateCache_7;
	// Spine.ExposedList`1<Spine.Bone> Spine.Skeleton::updateCacheReset
	ExposedList_1_t3613858675 * ___updateCacheReset_8;
	// Spine.Skin Spine.Skeleton::skin
	Skin_t1695237131 * ___skin_9;
	// System.Single Spine.Skeleton::r
	float ___r_10;
	// System.Single Spine.Skeleton::g
	float ___g_11;
	// System.Single Spine.Skeleton::b
	float ___b_12;
	// System.Single Spine.Skeleton::a
	float ___a_13;
	// System.Single Spine.Skeleton::time
	float ___time_14;
	// System.Boolean Spine.Skeleton::flipX
	bool ___flipX_15;
	// System.Boolean Spine.Skeleton::flipY
	bool ___flipY_16;
	// System.Single Spine.Skeleton::x
	float ___x_17;
	// System.Single Spine.Skeleton::y
	float ___y_18;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___data_0)); }
	inline SkeletonData_t2665604974 * get_data_0() const { return ___data_0; }
	inline SkeletonData_t2665604974 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(SkeletonData_t2665604974 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___bones_1)); }
	inline ExposedList_1_t3613858675 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t3613858675 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t3613858675 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_slots_2() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___slots_2)); }
	inline ExposedList_1_t3654112182 * get_slots_2() const { return ___slots_2; }
	inline ExposedList_1_t3654112182 ** get_address_of_slots_2() { return &___slots_2; }
	inline void set_slots_2(ExposedList_1_t3654112182 * value)
	{
		___slots_2 = value;
		Il2CppCodeGenWriteBarrier((&___slots_2), value);
	}

	inline static int32_t get_offset_of_drawOrder_3() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___drawOrder_3)); }
	inline ExposedList_1_t3654112182 * get_drawOrder_3() const { return ___drawOrder_3; }
	inline ExposedList_1_t3654112182 ** get_address_of_drawOrder_3() { return &___drawOrder_3; }
	inline void set_drawOrder_3(ExposedList_1_t3654112182 * value)
	{
		___drawOrder_3 = value;
		Il2CppCodeGenWriteBarrier((&___drawOrder_3), value);
	}

	inline static int32_t get_offset_of_ikConstraints_4() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___ikConstraints_4)); }
	inline ExposedList_1_t69720520 * get_ikConstraints_4() const { return ___ikConstraints_4; }
	inline ExposedList_1_t69720520 ** get_address_of_ikConstraints_4() { return &___ikConstraints_4; }
	inline void set_ikConstraints_4(ExposedList_1_t69720520 * value)
	{
		___ikConstraints_4 = value;
		Il2CppCodeGenWriteBarrier((&___ikConstraints_4), value);
	}

	inline static int32_t get_offset_of_transformConstraints_5() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___transformConstraints_5)); }
	inline ExposedList_1_t2359534843 * get_transformConstraints_5() const { return ___transformConstraints_5; }
	inline ExposedList_1_t2359534843 ** get_address_of_transformConstraints_5() { return &___transformConstraints_5; }
	inline void set_transformConstraints_5(ExposedList_1_t2359534843 * value)
	{
		___transformConstraints_5 = value;
		Il2CppCodeGenWriteBarrier((&___transformConstraints_5), value);
	}

	inline static int32_t get_offset_of_pathConstraints_6() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___pathConstraints_6)); }
	inline ExposedList_1_t3704659244 * get_pathConstraints_6() const { return ___pathConstraints_6; }
	inline ExposedList_1_t3704659244 ** get_address_of_pathConstraints_6() { return &___pathConstraints_6; }
	inline void set_pathConstraints_6(ExposedList_1_t3704659244 * value)
	{
		___pathConstraints_6 = value;
		Il2CppCodeGenWriteBarrier((&___pathConstraints_6), value);
	}

	inline static int32_t get_offset_of_updateCache_7() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___updateCache_7)); }
	inline ExposedList_1_t1975705998 * get_updateCache_7() const { return ___updateCache_7; }
	inline ExposedList_1_t1975705998 ** get_address_of_updateCache_7() { return &___updateCache_7; }
	inline void set_updateCache_7(ExposedList_1_t1975705998 * value)
	{
		___updateCache_7 = value;
		Il2CppCodeGenWriteBarrier((&___updateCache_7), value);
	}

	inline static int32_t get_offset_of_updateCacheReset_8() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___updateCacheReset_8)); }
	inline ExposedList_1_t3613858675 * get_updateCacheReset_8() const { return ___updateCacheReset_8; }
	inline ExposedList_1_t3613858675 ** get_address_of_updateCacheReset_8() { return &___updateCacheReset_8; }
	inline void set_updateCacheReset_8(ExposedList_1_t3613858675 * value)
	{
		___updateCacheReset_8 = value;
		Il2CppCodeGenWriteBarrier((&___updateCacheReset_8), value);
	}

	inline static int32_t get_offset_of_skin_9() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___skin_9)); }
	inline Skin_t1695237131 * get_skin_9() const { return ___skin_9; }
	inline Skin_t1695237131 ** get_address_of_skin_9() { return &___skin_9; }
	inline void set_skin_9(Skin_t1695237131 * value)
	{
		___skin_9 = value;
		Il2CppCodeGenWriteBarrier((&___skin_9), value);
	}

	inline static int32_t get_offset_of_r_10() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___r_10)); }
	inline float get_r_10() const { return ___r_10; }
	inline float* get_address_of_r_10() { return &___r_10; }
	inline void set_r_10(float value)
	{
		___r_10 = value;
	}

	inline static int32_t get_offset_of_g_11() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___g_11)); }
	inline float get_g_11() const { return ___g_11; }
	inline float* get_address_of_g_11() { return &___g_11; }
	inline void set_g_11(float value)
	{
		___g_11 = value;
	}

	inline static int32_t get_offset_of_b_12() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___b_12)); }
	inline float get_b_12() const { return ___b_12; }
	inline float* get_address_of_b_12() { return &___b_12; }
	inline void set_b_12(float value)
	{
		___b_12 = value;
	}

	inline static int32_t get_offset_of_a_13() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___a_13)); }
	inline float get_a_13() const { return ___a_13; }
	inline float* get_address_of_a_13() { return &___a_13; }
	inline void set_a_13(float value)
	{
		___a_13 = value;
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___time_14)); }
	inline float get_time_14() const { return ___time_14; }
	inline float* get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(float value)
	{
		___time_14 = value;
	}

	inline static int32_t get_offset_of_flipX_15() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___flipX_15)); }
	inline bool get_flipX_15() const { return ___flipX_15; }
	inline bool* get_address_of_flipX_15() { return &___flipX_15; }
	inline void set_flipX_15(bool value)
	{
		___flipX_15 = value;
	}

	inline static int32_t get_offset_of_flipY_16() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___flipY_16)); }
	inline bool get_flipY_16() const { return ___flipY_16; }
	inline bool* get_address_of_flipY_16() { return &___flipY_16; }
	inline void set_flipY_16(bool value)
	{
		___flipY_16 = value;
	}

	inline static int32_t get_offset_of_x_17() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___x_17)); }
	inline float get_x_17() const { return ___x_17; }
	inline float* get_address_of_x_17() { return &___x_17; }
	inline void set_x_17(float value)
	{
		___x_17 = value;
	}

	inline static int32_t get_offset_of_y_18() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___y_18)); }
	inline float get_y_18() const { return ___y_18; }
	inline float* get_address_of_y_18() { return &___y_18; }
	inline void set_y_18(float value)
	{
		___y_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETON_T1045203634_H
#ifndef SKELETONBINARY_T698135267_H
#define SKELETONBINARY_T698135267_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonBinary
struct  SkeletonBinary_t698135267  : public RuntimeObject
{
public:
	// System.Single Spine.SkeletonBinary::<Scale>k__BackingField
	float ___U3CScaleU3Ek__BackingField_13;
	// Spine.AttachmentLoader Spine.SkeletonBinary::attachmentLoader
	RuntimeObject* ___attachmentLoader_14;
	// System.Byte[] Spine.SkeletonBinary::buffer
	ByteU5BU5D_t371269159* ___buffer_15;
	// System.Collections.Generic.List`1<Spine.SkeletonJson/LinkedMesh> Spine.SkeletonBinary::linkedMeshes
	List_1_t1253086401 * ___linkedMeshes_16;

public:
	inline static int32_t get_offset_of_U3CScaleU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(SkeletonBinary_t698135267, ___U3CScaleU3Ek__BackingField_13)); }
	inline float get_U3CScaleU3Ek__BackingField_13() const { return ___U3CScaleU3Ek__BackingField_13; }
	inline float* get_address_of_U3CScaleU3Ek__BackingField_13() { return &___U3CScaleU3Ek__BackingField_13; }
	inline void set_U3CScaleU3Ek__BackingField_13(float value)
	{
		___U3CScaleU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_attachmentLoader_14() { return static_cast<int32_t>(offsetof(SkeletonBinary_t698135267, ___attachmentLoader_14)); }
	inline RuntimeObject* get_attachmentLoader_14() const { return ___attachmentLoader_14; }
	inline RuntimeObject** get_address_of_attachmentLoader_14() { return &___attachmentLoader_14; }
	inline void set_attachmentLoader_14(RuntimeObject* value)
	{
		___attachmentLoader_14 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentLoader_14), value);
	}

	inline static int32_t get_offset_of_buffer_15() { return static_cast<int32_t>(offsetof(SkeletonBinary_t698135267, ___buffer_15)); }
	inline ByteU5BU5D_t371269159* get_buffer_15() const { return ___buffer_15; }
	inline ByteU5BU5D_t371269159** get_address_of_buffer_15() { return &___buffer_15; }
	inline void set_buffer_15(ByteU5BU5D_t371269159* value)
	{
		___buffer_15 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_15), value);
	}

	inline static int32_t get_offset_of_linkedMeshes_16() { return static_cast<int32_t>(offsetof(SkeletonBinary_t698135267, ___linkedMeshes_16)); }
	inline List_1_t1253086401 * get_linkedMeshes_16() const { return ___linkedMeshes_16; }
	inline List_1_t1253086401 ** get_address_of_linkedMeshes_16() { return &___linkedMeshes_16; }
	inline void set_linkedMeshes_16(List_1_t1253086401 * value)
	{
		___linkedMeshes_16 = value;
		Il2CppCodeGenWriteBarrier((&___linkedMeshes_16), value);
	}
};

struct SkeletonBinary_t698135267_StaticFields
{
public:
	// Spine.TransformMode[] Spine.SkeletonBinary::TransformModeValues
	TransformModeU5BU5D_t1434233321* ___TransformModeValues_17;

public:
	inline static int32_t get_offset_of_TransformModeValues_17() { return static_cast<int32_t>(offsetof(SkeletonBinary_t698135267_StaticFields, ___TransformModeValues_17)); }
	inline TransformModeU5BU5D_t1434233321* get_TransformModeValues_17() const { return ___TransformModeValues_17; }
	inline TransformModeU5BU5D_t1434233321** get_address_of_TransformModeValues_17() { return &___TransformModeValues_17; }
	inline void set_TransformModeValues_17(TransformModeU5BU5D_t1434233321* value)
	{
		___TransformModeValues_17 = value;
		Il2CppCodeGenWriteBarrier((&___TransformModeValues_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONBINARY_T698135267_H
#ifndef VERTICES_T2244423585_H
#define VERTICES_T2244423585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonBinary/Vertices
struct  Vertices_t2244423585  : public RuntimeObject
{
public:
	// System.Int32[] Spine.SkeletonBinary/Vertices::bones
	Int32U5BU5D_t281224829* ___bones_0;
	// System.Single[] Spine.SkeletonBinary/Vertices::vertices
	SingleU5BU5D_t3989243465* ___vertices_1;

public:
	inline static int32_t get_offset_of_bones_0() { return static_cast<int32_t>(offsetof(Vertices_t2244423585, ___bones_0)); }
	inline Int32U5BU5D_t281224829* get_bones_0() const { return ___bones_0; }
	inline Int32U5BU5D_t281224829** get_address_of_bones_0() { return &___bones_0; }
	inline void set_bones_0(Int32U5BU5D_t281224829* value)
	{
		___bones_0 = value;
		Il2CppCodeGenWriteBarrier((&___bones_0), value);
	}

	inline static int32_t get_offset_of_vertices_1() { return static_cast<int32_t>(offsetof(Vertices_t2244423585, ___vertices_1)); }
	inline SingleU5BU5D_t3989243465* get_vertices_1() const { return ___vertices_1; }
	inline SingleU5BU5D_t3989243465** get_address_of_vertices_1() { return &___vertices_1; }
	inline void set_vertices_1(SingleU5BU5D_t3989243465* value)
	{
		___vertices_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICES_T2244423585_H
#ifndef SKELETONBOUNDS_T948042636_H
#define SKELETONBOUNDS_T948042636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonBounds
struct  SkeletonBounds_t948042636  : public RuntimeObject
{
public:
	// Spine.ExposedList`1<Spine.Polygon> Spine.SkeletonBounds::polygonPool
	ExposedList_1_t965638450 * ___polygonPool_0;
	// System.Single Spine.SkeletonBounds::minX
	float ___minX_1;
	// System.Single Spine.SkeletonBounds::minY
	float ___minY_2;
	// System.Single Spine.SkeletonBounds::maxX
	float ___maxX_3;
	// System.Single Spine.SkeletonBounds::maxY
	float ___maxY_4;
	// Spine.ExposedList`1<Spine.BoundingBoxAttachment> Spine.SkeletonBounds::<BoundingBoxes>k__BackingField
	ExposedList_1_t2876832010 * ___U3CBoundingBoxesU3Ek__BackingField_5;
	// Spine.ExposedList`1<Spine.Polygon> Spine.SkeletonBounds::<Polygons>k__BackingField
	ExposedList_1_t965638450 * ___U3CPolygonsU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_polygonPool_0() { return static_cast<int32_t>(offsetof(SkeletonBounds_t948042636, ___polygonPool_0)); }
	inline ExposedList_1_t965638450 * get_polygonPool_0() const { return ___polygonPool_0; }
	inline ExposedList_1_t965638450 ** get_address_of_polygonPool_0() { return &___polygonPool_0; }
	inline void set_polygonPool_0(ExposedList_1_t965638450 * value)
	{
		___polygonPool_0 = value;
		Il2CppCodeGenWriteBarrier((&___polygonPool_0), value);
	}

	inline static int32_t get_offset_of_minX_1() { return static_cast<int32_t>(offsetof(SkeletonBounds_t948042636, ___minX_1)); }
	inline float get_minX_1() const { return ___minX_1; }
	inline float* get_address_of_minX_1() { return &___minX_1; }
	inline void set_minX_1(float value)
	{
		___minX_1 = value;
	}

	inline static int32_t get_offset_of_minY_2() { return static_cast<int32_t>(offsetof(SkeletonBounds_t948042636, ___minY_2)); }
	inline float get_minY_2() const { return ___minY_2; }
	inline float* get_address_of_minY_2() { return &___minY_2; }
	inline void set_minY_2(float value)
	{
		___minY_2 = value;
	}

	inline static int32_t get_offset_of_maxX_3() { return static_cast<int32_t>(offsetof(SkeletonBounds_t948042636, ___maxX_3)); }
	inline float get_maxX_3() const { return ___maxX_3; }
	inline float* get_address_of_maxX_3() { return &___maxX_3; }
	inline void set_maxX_3(float value)
	{
		___maxX_3 = value;
	}

	inline static int32_t get_offset_of_maxY_4() { return static_cast<int32_t>(offsetof(SkeletonBounds_t948042636, ___maxY_4)); }
	inline float get_maxY_4() const { return ___maxY_4; }
	inline float* get_address_of_maxY_4() { return &___maxY_4; }
	inline void set_maxY_4(float value)
	{
		___maxY_4 = value;
	}

	inline static int32_t get_offset_of_U3CBoundingBoxesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(SkeletonBounds_t948042636, ___U3CBoundingBoxesU3Ek__BackingField_5)); }
	inline ExposedList_1_t2876832010 * get_U3CBoundingBoxesU3Ek__BackingField_5() const { return ___U3CBoundingBoxesU3Ek__BackingField_5; }
	inline ExposedList_1_t2876832010 ** get_address_of_U3CBoundingBoxesU3Ek__BackingField_5() { return &___U3CBoundingBoxesU3Ek__BackingField_5; }
	inline void set_U3CBoundingBoxesU3Ek__BackingField_5(ExposedList_1_t2876832010 * value)
	{
		___U3CBoundingBoxesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoundingBoxesU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CPolygonsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SkeletonBounds_t948042636, ___U3CPolygonsU3Ek__BackingField_6)); }
	inline ExposedList_1_t965638450 * get_U3CPolygonsU3Ek__BackingField_6() const { return ___U3CPolygonsU3Ek__BackingField_6; }
	inline ExposedList_1_t965638450 ** get_address_of_U3CPolygonsU3Ek__BackingField_6() { return &___U3CPolygonsU3Ek__BackingField_6; }
	inline void set_U3CPolygonsU3Ek__BackingField_6(ExposedList_1_t965638450 * value)
	{
		___U3CPolygonsU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPolygonsU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONBOUNDS_T948042636_H
#ifndef POLYGON_T3410609907_H
#define POLYGON_T3410609907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Polygon
struct  Polygon_t3410609907  : public RuntimeObject
{
public:
	// System.Single[] Spine.Polygon::<Vertices>k__BackingField
	SingleU5BU5D_t3989243465* ___U3CVerticesU3Ek__BackingField_0;
	// System.Int32 Spine.Polygon::<Count>k__BackingField
	int32_t ___U3CCountU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CVerticesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Polygon_t3410609907, ___U3CVerticesU3Ek__BackingField_0)); }
	inline SingleU5BU5D_t3989243465* get_U3CVerticesU3Ek__BackingField_0() const { return ___U3CVerticesU3Ek__BackingField_0; }
	inline SingleU5BU5D_t3989243465** get_address_of_U3CVerticesU3Ek__BackingField_0() { return &___U3CVerticesU3Ek__BackingField_0; }
	inline void set_U3CVerticesU3Ek__BackingField_0(SingleU5BU5D_t3989243465* value)
	{
		___U3CVerticesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVerticesU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCountU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Polygon_t3410609907, ___U3CCountU3Ek__BackingField_1)); }
	inline int32_t get_U3CCountU3Ek__BackingField_1() const { return ___U3CCountU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCountU3Ek__BackingField_1() { return &___U3CCountU3Ek__BackingField_1; }
	inline void set_U3CCountU3Ek__BackingField_1(int32_t value)
	{
		___U3CCountU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POLYGON_T3410609907_H
#ifndef TRANSFORMCONSTRAINTDATA_T1381040281_H
#define TRANSFORMCONSTRAINTDATA_T1381040281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TransformConstraintData
struct  TransformConstraintData_t1381040281  : public RuntimeObject
{
public:
	// System.String Spine.TransformConstraintData::name
	String_t* ___name_0;
	// System.Int32 Spine.TransformConstraintData::order
	int32_t ___order_1;
	// Spine.ExposedList`1<Spine.BoneData> Spine.TransformConstraintData::bones
	ExposedList_1_t3044655291 * ___bones_2;
	// Spine.BoneData Spine.TransformConstraintData::target
	BoneData_t1194659452 * ___target_3;
	// System.Single Spine.TransformConstraintData::rotateMix
	float ___rotateMix_4;
	// System.Single Spine.TransformConstraintData::translateMix
	float ___translateMix_5;
	// System.Single Spine.TransformConstraintData::scaleMix
	float ___scaleMix_6;
	// System.Single Spine.TransformConstraintData::shearMix
	float ___shearMix_7;
	// System.Single Spine.TransformConstraintData::offsetRotation
	float ___offsetRotation_8;
	// System.Single Spine.TransformConstraintData::offsetX
	float ___offsetX_9;
	// System.Single Spine.TransformConstraintData::offsetY
	float ___offsetY_10;
	// System.Single Spine.TransformConstraintData::offsetScaleX
	float ___offsetScaleX_11;
	// System.Single Spine.TransformConstraintData::offsetScaleY
	float ___offsetScaleY_12;
	// System.Single Spine.TransformConstraintData::offsetShearY
	float ___offsetShearY_13;
	// System.Boolean Spine.TransformConstraintData::relative
	bool ___relative_14;
	// System.Boolean Spine.TransformConstraintData::local
	bool ___local_15;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_bones_2() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___bones_2)); }
	inline ExposedList_1_t3044655291 * get_bones_2() const { return ___bones_2; }
	inline ExposedList_1_t3044655291 ** get_address_of_bones_2() { return &___bones_2; }
	inline void set_bones_2(ExposedList_1_t3044655291 * value)
	{
		___bones_2 = value;
		Il2CppCodeGenWriteBarrier((&___bones_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___target_3)); }
	inline BoneData_t1194659452 * get_target_3() const { return ___target_3; }
	inline BoneData_t1194659452 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(BoneData_t1194659452 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_rotateMix_4() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___rotateMix_4)); }
	inline float get_rotateMix_4() const { return ___rotateMix_4; }
	inline float* get_address_of_rotateMix_4() { return &___rotateMix_4; }
	inline void set_rotateMix_4(float value)
	{
		___rotateMix_4 = value;
	}

	inline static int32_t get_offset_of_translateMix_5() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___translateMix_5)); }
	inline float get_translateMix_5() const { return ___translateMix_5; }
	inline float* get_address_of_translateMix_5() { return &___translateMix_5; }
	inline void set_translateMix_5(float value)
	{
		___translateMix_5 = value;
	}

	inline static int32_t get_offset_of_scaleMix_6() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___scaleMix_6)); }
	inline float get_scaleMix_6() const { return ___scaleMix_6; }
	inline float* get_address_of_scaleMix_6() { return &___scaleMix_6; }
	inline void set_scaleMix_6(float value)
	{
		___scaleMix_6 = value;
	}

	inline static int32_t get_offset_of_shearMix_7() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___shearMix_7)); }
	inline float get_shearMix_7() const { return ___shearMix_7; }
	inline float* get_address_of_shearMix_7() { return &___shearMix_7; }
	inline void set_shearMix_7(float value)
	{
		___shearMix_7 = value;
	}

	inline static int32_t get_offset_of_offsetRotation_8() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___offsetRotation_8)); }
	inline float get_offsetRotation_8() const { return ___offsetRotation_8; }
	inline float* get_address_of_offsetRotation_8() { return &___offsetRotation_8; }
	inline void set_offsetRotation_8(float value)
	{
		___offsetRotation_8 = value;
	}

	inline static int32_t get_offset_of_offsetX_9() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___offsetX_9)); }
	inline float get_offsetX_9() const { return ___offsetX_9; }
	inline float* get_address_of_offsetX_9() { return &___offsetX_9; }
	inline void set_offsetX_9(float value)
	{
		___offsetX_9 = value;
	}

	inline static int32_t get_offset_of_offsetY_10() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___offsetY_10)); }
	inline float get_offsetY_10() const { return ___offsetY_10; }
	inline float* get_address_of_offsetY_10() { return &___offsetY_10; }
	inline void set_offsetY_10(float value)
	{
		___offsetY_10 = value;
	}

	inline static int32_t get_offset_of_offsetScaleX_11() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___offsetScaleX_11)); }
	inline float get_offsetScaleX_11() const { return ___offsetScaleX_11; }
	inline float* get_address_of_offsetScaleX_11() { return &___offsetScaleX_11; }
	inline void set_offsetScaleX_11(float value)
	{
		___offsetScaleX_11 = value;
	}

	inline static int32_t get_offset_of_offsetScaleY_12() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___offsetScaleY_12)); }
	inline float get_offsetScaleY_12() const { return ___offsetScaleY_12; }
	inline float* get_address_of_offsetScaleY_12() { return &___offsetScaleY_12; }
	inline void set_offsetScaleY_12(float value)
	{
		___offsetScaleY_12 = value;
	}

	inline static int32_t get_offset_of_offsetShearY_13() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___offsetShearY_13)); }
	inline float get_offsetShearY_13() const { return ___offsetShearY_13; }
	inline float* get_address_of_offsetShearY_13() { return &___offsetShearY_13; }
	inline void set_offsetShearY_13(float value)
	{
		___offsetShearY_13 = value;
	}

	inline static int32_t get_offset_of_relative_14() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___relative_14)); }
	inline bool get_relative_14() const { return ___relative_14; }
	inline bool* get_address_of_relative_14() { return &___relative_14; }
	inline void set_relative_14(bool value)
	{
		___relative_14 = value;
	}

	inline static int32_t get_offset_of_local_15() { return static_cast<int32_t>(offsetof(TransformConstraintData_t1381040281, ___local_15)); }
	inline bool get_local_15() const { return ___local_15; }
	inline bool* get_address_of_local_15() { return &___local_15; }
	inline void set_local_15(bool value)
	{
		___local_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONSTRAINTDATA_T1381040281_H
#ifndef SKELETONDATA_T2665604974_H
#define SKELETONDATA_T2665604974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonData
struct  SkeletonData_t2665604974  : public RuntimeObject
{
public:
	// System.String Spine.SkeletonData::name
	String_t* ___name_0;
	// Spine.ExposedList`1<Spine.BoneData> Spine.SkeletonData::bones
	ExposedList_1_t3044655291 * ___bones_1;
	// Spine.ExposedList`1<Spine.SlotData> Spine.SkeletonData::slots
	ExposedList_1_t2437777689 * ___slots_2;
	// Spine.ExposedList`1<Spine.Skin> Spine.SkeletonData::skins
	ExposedList_1_t3545232970 * ___skins_3;
	// Spine.Skin Spine.SkeletonData::defaultSkin
	Skin_t1695237131 * ___defaultSkin_4;
	// Spine.ExposedList`1<Spine.EventData> Spine.SkeletonData::events
	ExposedList_1_t2145976947 * ___events_5;
	// Spine.ExposedList`1<Spine.Animation> Spine.SkeletonData::animations
	ExposedList_1_t3917139350 * ___animations_6;
	// Spine.ExposedList`1<Spine.IkConstraintData> Spine.SkeletonData::ikConstraints
	ExposedList_1_t770644252 * ___ikConstraints_7;
	// Spine.ExposedList`1<Spine.TransformConstraintData> Spine.SkeletonData::transformConstraints
	ExposedList_1_t3231036120 * ___transformConstraints_8;
	// Spine.ExposedList`1<Spine.PathConstraintData> Spine.SkeletonData::pathConstraints
	ExposedList_1_t740378891 * ___pathConstraints_9;
	// System.Single Spine.SkeletonData::width
	float ___width_10;
	// System.Single Spine.SkeletonData::height
	float ___height_11;
	// System.String Spine.SkeletonData::version
	String_t* ___version_12;
	// System.String Spine.SkeletonData::hash
	String_t* ___hash_13;
	// System.Single Spine.SkeletonData::fps
	float ___fps_14;
	// System.String Spine.SkeletonData::imagesPath
	String_t* ___imagesPath_15;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___bones_1)); }
	inline ExposedList_1_t3044655291 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t3044655291 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t3044655291 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_slots_2() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___slots_2)); }
	inline ExposedList_1_t2437777689 * get_slots_2() const { return ___slots_2; }
	inline ExposedList_1_t2437777689 ** get_address_of_slots_2() { return &___slots_2; }
	inline void set_slots_2(ExposedList_1_t2437777689 * value)
	{
		___slots_2 = value;
		Il2CppCodeGenWriteBarrier((&___slots_2), value);
	}

	inline static int32_t get_offset_of_skins_3() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___skins_3)); }
	inline ExposedList_1_t3545232970 * get_skins_3() const { return ___skins_3; }
	inline ExposedList_1_t3545232970 ** get_address_of_skins_3() { return &___skins_3; }
	inline void set_skins_3(ExposedList_1_t3545232970 * value)
	{
		___skins_3 = value;
		Il2CppCodeGenWriteBarrier((&___skins_3), value);
	}

	inline static int32_t get_offset_of_defaultSkin_4() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___defaultSkin_4)); }
	inline Skin_t1695237131 * get_defaultSkin_4() const { return ___defaultSkin_4; }
	inline Skin_t1695237131 ** get_address_of_defaultSkin_4() { return &___defaultSkin_4; }
	inline void set_defaultSkin_4(Skin_t1695237131 * value)
	{
		___defaultSkin_4 = value;
		Il2CppCodeGenWriteBarrier((&___defaultSkin_4), value);
	}

	inline static int32_t get_offset_of_events_5() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___events_5)); }
	inline ExposedList_1_t2145976947 * get_events_5() const { return ___events_5; }
	inline ExposedList_1_t2145976947 ** get_address_of_events_5() { return &___events_5; }
	inline void set_events_5(ExposedList_1_t2145976947 * value)
	{
		___events_5 = value;
		Il2CppCodeGenWriteBarrier((&___events_5), value);
	}

	inline static int32_t get_offset_of_animations_6() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___animations_6)); }
	inline ExposedList_1_t3917139350 * get_animations_6() const { return ___animations_6; }
	inline ExposedList_1_t3917139350 ** get_address_of_animations_6() { return &___animations_6; }
	inline void set_animations_6(ExposedList_1_t3917139350 * value)
	{
		___animations_6 = value;
		Il2CppCodeGenWriteBarrier((&___animations_6), value);
	}

	inline static int32_t get_offset_of_ikConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___ikConstraints_7)); }
	inline ExposedList_1_t770644252 * get_ikConstraints_7() const { return ___ikConstraints_7; }
	inline ExposedList_1_t770644252 ** get_address_of_ikConstraints_7() { return &___ikConstraints_7; }
	inline void set_ikConstraints_7(ExposedList_1_t770644252 * value)
	{
		___ikConstraints_7 = value;
		Il2CppCodeGenWriteBarrier((&___ikConstraints_7), value);
	}

	inline static int32_t get_offset_of_transformConstraints_8() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___transformConstraints_8)); }
	inline ExposedList_1_t3231036120 * get_transformConstraints_8() const { return ___transformConstraints_8; }
	inline ExposedList_1_t3231036120 ** get_address_of_transformConstraints_8() { return &___transformConstraints_8; }
	inline void set_transformConstraints_8(ExposedList_1_t3231036120 * value)
	{
		___transformConstraints_8 = value;
		Il2CppCodeGenWriteBarrier((&___transformConstraints_8), value);
	}

	inline static int32_t get_offset_of_pathConstraints_9() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___pathConstraints_9)); }
	inline ExposedList_1_t740378891 * get_pathConstraints_9() const { return ___pathConstraints_9; }
	inline ExposedList_1_t740378891 ** get_address_of_pathConstraints_9() { return &___pathConstraints_9; }
	inline void set_pathConstraints_9(ExposedList_1_t740378891 * value)
	{
		___pathConstraints_9 = value;
		Il2CppCodeGenWriteBarrier((&___pathConstraints_9), value);
	}

	inline static int32_t get_offset_of_width_10() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___width_10)); }
	inline float get_width_10() const { return ___width_10; }
	inline float* get_address_of_width_10() { return &___width_10; }
	inline void set_width_10(float value)
	{
		___width_10 = value;
	}

	inline static int32_t get_offset_of_height_11() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___height_11)); }
	inline float get_height_11() const { return ___height_11; }
	inline float* get_address_of_height_11() { return &___height_11; }
	inline void set_height_11(float value)
	{
		___height_11 = value;
	}

	inline static int32_t get_offset_of_version_12() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___version_12)); }
	inline String_t* get_version_12() const { return ___version_12; }
	inline String_t** get_address_of_version_12() { return &___version_12; }
	inline void set_version_12(String_t* value)
	{
		___version_12 = value;
		Il2CppCodeGenWriteBarrier((&___version_12), value);
	}

	inline static int32_t get_offset_of_hash_13() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___hash_13)); }
	inline String_t* get_hash_13() const { return ___hash_13; }
	inline String_t** get_address_of_hash_13() { return &___hash_13; }
	inline void set_hash_13(String_t* value)
	{
		___hash_13 = value;
		Il2CppCodeGenWriteBarrier((&___hash_13), value);
	}

	inline static int32_t get_offset_of_fps_14() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___fps_14)); }
	inline float get_fps_14() const { return ___fps_14; }
	inline float* get_address_of_fps_14() { return &___fps_14; }
	inline void set_fps_14(float value)
	{
		___fps_14 = value;
	}

	inline static int32_t get_offset_of_imagesPath_15() { return static_cast<int32_t>(offsetof(SkeletonData_t2665604974, ___imagesPath_15)); }
	inline String_t* get_imagesPath_15() const { return ___imagesPath_15; }
	inline String_t** get_address_of_imagesPath_15() { return &___imagesPath_15; }
	inline void set_imagesPath_15(String_t* value)
	{
		___imagesPath_15 = value;
		Il2CppCodeGenWriteBarrier((&___imagesPath_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONDATA_T2665604974_H
#ifndef SKELETONJSON_T3645594475_H
#define SKELETONJSON_T3645594475_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonJson
struct  SkeletonJson_t3645594475  : public RuntimeObject
{
public:
	// System.Single Spine.SkeletonJson::<Scale>k__BackingField
	float ___U3CScaleU3Ek__BackingField_0;
	// Spine.AttachmentLoader Spine.SkeletonJson::attachmentLoader
	RuntimeObject* ___attachmentLoader_1;
	// System.Collections.Generic.List`1<Spine.SkeletonJson/LinkedMesh> Spine.SkeletonJson::linkedMeshes
	List_1_t1253086401 * ___linkedMeshes_2;

public:
	inline static int32_t get_offset_of_U3CScaleU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SkeletonJson_t3645594475, ___U3CScaleU3Ek__BackingField_0)); }
	inline float get_U3CScaleU3Ek__BackingField_0() const { return ___U3CScaleU3Ek__BackingField_0; }
	inline float* get_address_of_U3CScaleU3Ek__BackingField_0() { return &___U3CScaleU3Ek__BackingField_0; }
	inline void set_U3CScaleU3Ek__BackingField_0(float value)
	{
		___U3CScaleU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_attachmentLoader_1() { return static_cast<int32_t>(offsetof(SkeletonJson_t3645594475, ___attachmentLoader_1)); }
	inline RuntimeObject* get_attachmentLoader_1() const { return ___attachmentLoader_1; }
	inline RuntimeObject** get_address_of_attachmentLoader_1() { return &___attachmentLoader_1; }
	inline void set_attachmentLoader_1(RuntimeObject* value)
	{
		___attachmentLoader_1 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentLoader_1), value);
	}

	inline static int32_t get_offset_of_linkedMeshes_2() { return static_cast<int32_t>(offsetof(SkeletonJson_t3645594475, ___linkedMeshes_2)); }
	inline List_1_t1253086401 * get_linkedMeshes_2() const { return ___linkedMeshes_2; }
	inline List_1_t1253086401 ** get_address_of_linkedMeshes_2() { return &___linkedMeshes_2; }
	inline void set_linkedMeshes_2(List_1_t1253086401 * value)
	{
		___linkedMeshes_2 = value;
		Il2CppCodeGenWriteBarrier((&___linkedMeshes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONJSON_T3645594475_H
#ifndef SKELETONCLIPPING_T2504359873_H
#define SKELETONCLIPPING_T2504359873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonClipping
struct  SkeletonClipping_t2504359873  : public RuntimeObject
{
public:
	// Spine.Triangulator Spine.SkeletonClipping::triangulator
	Triangulator_t1509316840 * ___triangulator_0;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clippingPolygon
	ExposedList_1_t3384296343 * ___clippingPolygon_1;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clipOutput
	ExposedList_1_t3384296343 * ___clipOutput_2;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clippedVertices
	ExposedList_1_t3384296343 * ___clippedVertices_3;
	// Spine.ExposedList`1<System.Int32> Spine.SkeletonClipping::clippedTriangles
	ExposedList_1_t1671071859 * ___clippedTriangles_4;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::clippedUVs
	ExposedList_1_t3384296343 * ___clippedUVs_5;
	// Spine.ExposedList`1<System.Single> Spine.SkeletonClipping::scratch
	ExposedList_1_t3384296343 * ___scratch_6;
	// Spine.ClippingAttachment Spine.SkeletonClipping::clipAttachment
	ClippingAttachment_t906347500 * ___clipAttachment_7;
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Single>> Spine.SkeletonClipping::clippingPolygons
	ExposedList_1_t939324886 * ___clippingPolygons_8;

public:
	inline static int32_t get_offset_of_triangulator_0() { return static_cast<int32_t>(offsetof(SkeletonClipping_t2504359873, ___triangulator_0)); }
	inline Triangulator_t1509316840 * get_triangulator_0() const { return ___triangulator_0; }
	inline Triangulator_t1509316840 ** get_address_of_triangulator_0() { return &___triangulator_0; }
	inline void set_triangulator_0(Triangulator_t1509316840 * value)
	{
		___triangulator_0 = value;
		Il2CppCodeGenWriteBarrier((&___triangulator_0), value);
	}

	inline static int32_t get_offset_of_clippingPolygon_1() { return static_cast<int32_t>(offsetof(SkeletonClipping_t2504359873, ___clippingPolygon_1)); }
	inline ExposedList_1_t3384296343 * get_clippingPolygon_1() const { return ___clippingPolygon_1; }
	inline ExposedList_1_t3384296343 ** get_address_of_clippingPolygon_1() { return &___clippingPolygon_1; }
	inline void set_clippingPolygon_1(ExposedList_1_t3384296343 * value)
	{
		___clippingPolygon_1 = value;
		Il2CppCodeGenWriteBarrier((&___clippingPolygon_1), value);
	}

	inline static int32_t get_offset_of_clipOutput_2() { return static_cast<int32_t>(offsetof(SkeletonClipping_t2504359873, ___clipOutput_2)); }
	inline ExposedList_1_t3384296343 * get_clipOutput_2() const { return ___clipOutput_2; }
	inline ExposedList_1_t3384296343 ** get_address_of_clipOutput_2() { return &___clipOutput_2; }
	inline void set_clipOutput_2(ExposedList_1_t3384296343 * value)
	{
		___clipOutput_2 = value;
		Il2CppCodeGenWriteBarrier((&___clipOutput_2), value);
	}

	inline static int32_t get_offset_of_clippedVertices_3() { return static_cast<int32_t>(offsetof(SkeletonClipping_t2504359873, ___clippedVertices_3)); }
	inline ExposedList_1_t3384296343 * get_clippedVertices_3() const { return ___clippedVertices_3; }
	inline ExposedList_1_t3384296343 ** get_address_of_clippedVertices_3() { return &___clippedVertices_3; }
	inline void set_clippedVertices_3(ExposedList_1_t3384296343 * value)
	{
		___clippedVertices_3 = value;
		Il2CppCodeGenWriteBarrier((&___clippedVertices_3), value);
	}

	inline static int32_t get_offset_of_clippedTriangles_4() { return static_cast<int32_t>(offsetof(SkeletonClipping_t2504359873, ___clippedTriangles_4)); }
	inline ExposedList_1_t1671071859 * get_clippedTriangles_4() const { return ___clippedTriangles_4; }
	inline ExposedList_1_t1671071859 ** get_address_of_clippedTriangles_4() { return &___clippedTriangles_4; }
	inline void set_clippedTriangles_4(ExposedList_1_t1671071859 * value)
	{
		___clippedTriangles_4 = value;
		Il2CppCodeGenWriteBarrier((&___clippedTriangles_4), value);
	}

	inline static int32_t get_offset_of_clippedUVs_5() { return static_cast<int32_t>(offsetof(SkeletonClipping_t2504359873, ___clippedUVs_5)); }
	inline ExposedList_1_t3384296343 * get_clippedUVs_5() const { return ___clippedUVs_5; }
	inline ExposedList_1_t3384296343 ** get_address_of_clippedUVs_5() { return &___clippedUVs_5; }
	inline void set_clippedUVs_5(ExposedList_1_t3384296343 * value)
	{
		___clippedUVs_5 = value;
		Il2CppCodeGenWriteBarrier((&___clippedUVs_5), value);
	}

	inline static int32_t get_offset_of_scratch_6() { return static_cast<int32_t>(offsetof(SkeletonClipping_t2504359873, ___scratch_6)); }
	inline ExposedList_1_t3384296343 * get_scratch_6() const { return ___scratch_6; }
	inline ExposedList_1_t3384296343 ** get_address_of_scratch_6() { return &___scratch_6; }
	inline void set_scratch_6(ExposedList_1_t3384296343 * value)
	{
		___scratch_6 = value;
		Il2CppCodeGenWriteBarrier((&___scratch_6), value);
	}

	inline static int32_t get_offset_of_clipAttachment_7() { return static_cast<int32_t>(offsetof(SkeletonClipping_t2504359873, ___clipAttachment_7)); }
	inline ClippingAttachment_t906347500 * get_clipAttachment_7() const { return ___clipAttachment_7; }
	inline ClippingAttachment_t906347500 ** get_address_of_clipAttachment_7() { return &___clipAttachment_7; }
	inline void set_clipAttachment_7(ClippingAttachment_t906347500 * value)
	{
		___clipAttachment_7 = value;
		Il2CppCodeGenWriteBarrier((&___clipAttachment_7), value);
	}

	inline static int32_t get_offset_of_clippingPolygons_8() { return static_cast<int32_t>(offsetof(SkeletonClipping_t2504359873, ___clippingPolygons_8)); }
	inline ExposedList_1_t939324886 * get_clippingPolygons_8() const { return ___clippingPolygons_8; }
	inline ExposedList_1_t939324886 ** get_address_of_clippingPolygons_8() { return &___clippingPolygons_8; }
	inline void set_clippingPolygons_8(ExposedList_1_t939324886 * value)
	{
		___clippingPolygons_8 = value;
		Il2CppCodeGenWriteBarrier((&___clippingPolygons_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONCLIPPING_T2504359873_H
#ifndef SKIN_T1695237131_H
#define SKIN_T1695237131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skin
struct  Skin_t1695237131  : public RuntimeObject
{
public:
	// System.String Spine.Skin::name
	String_t* ___name_0;
	// System.Collections.Generic.Dictionary`2<Spine.Skin/AttachmentKeyTuple,Spine.Attachment> Spine.Skin::attachments
	Dictionary_2_t1889082438 * ___attachments_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Skin_t1695237131, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_attachments_1() { return static_cast<int32_t>(offsetof(Skin_t1695237131, ___attachments_1)); }
	inline Dictionary_2_t1889082438 * get_attachments_1() const { return ___attachments_1; }
	inline Dictionary_2_t1889082438 ** get_address_of_attachments_1() { return &___attachments_1; }
	inline void set_attachments_1(Dictionary_2_t1889082438 * value)
	{
		___attachments_1 = value;
		Il2CppCodeGenWriteBarrier((&___attachments_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKIN_T1695237131_H
#ifndef ATTACHMENTKEYTUPLECOMPARER_T2248732946_H
#define ATTACHMENTKEYTUPLECOMPARER_T2248732946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skin/AttachmentKeyTupleComparer
struct  AttachmentKeyTupleComparer_t2248732946  : public RuntimeObject
{
public:

public:
};

struct AttachmentKeyTupleComparer_t2248732946_StaticFields
{
public:
	// Spine.Skin/AttachmentKeyTupleComparer Spine.Skin/AttachmentKeyTupleComparer::Instance
	AttachmentKeyTupleComparer_t2248732946 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(AttachmentKeyTupleComparer_t2248732946_StaticFields, ___Instance_0)); }
	inline AttachmentKeyTupleComparer_t2248732946 * get_Instance_0() const { return ___Instance_0; }
	inline AttachmentKeyTupleComparer_t2248732946 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(AttachmentKeyTupleComparer_t2248732946 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENTKEYTUPLECOMPARER_T2248732946_H
#ifndef SLOT_T1804116343_H
#define SLOT_T1804116343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Slot
struct  Slot_t1804116343  : public RuntimeObject
{
public:
	// Spine.SlotData Spine.Slot::data
	SlotData_t587781850 * ___data_0;
	// Spine.Bone Spine.Slot::bone
	Bone_t1763862836 * ___bone_1;
	// System.Single Spine.Slot::r
	float ___r_2;
	// System.Single Spine.Slot::g
	float ___g_3;
	// System.Single Spine.Slot::b
	float ___b_4;
	// System.Single Spine.Slot::a
	float ___a_5;
	// System.Single Spine.Slot::r2
	float ___r2_6;
	// System.Single Spine.Slot::g2
	float ___g2_7;
	// System.Single Spine.Slot::b2
	float ___b2_8;
	// System.Boolean Spine.Slot::hasSecondColor
	bool ___hasSecondColor_9;
	// Spine.Attachment Spine.Slot::attachment
	Attachment_t397756874 * ___attachment_10;
	// System.Single Spine.Slot::attachmentTime
	float ___attachmentTime_11;
	// Spine.ExposedList`1<System.Single> Spine.Slot::attachmentVertices
	ExposedList_1_t3384296343 * ___attachmentVertices_12;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___data_0)); }
	inline SlotData_t587781850 * get_data_0() const { return ___data_0; }
	inline SlotData_t587781850 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(SlotData_t587781850 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bone_1() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___bone_1)); }
	inline Bone_t1763862836 * get_bone_1() const { return ___bone_1; }
	inline Bone_t1763862836 ** get_address_of_bone_1() { return &___bone_1; }
	inline void set_bone_1(Bone_t1763862836 * value)
	{
		___bone_1 = value;
		Il2CppCodeGenWriteBarrier((&___bone_1), value);
	}

	inline static int32_t get_offset_of_r_2() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___r_2)); }
	inline float get_r_2() const { return ___r_2; }
	inline float* get_address_of_r_2() { return &___r_2; }
	inline void set_r_2(float value)
	{
		___r_2 = value;
	}

	inline static int32_t get_offset_of_g_3() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___g_3)); }
	inline float get_g_3() const { return ___g_3; }
	inline float* get_address_of_g_3() { return &___g_3; }
	inline void set_g_3(float value)
	{
		___g_3 = value;
	}

	inline static int32_t get_offset_of_b_4() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___b_4)); }
	inline float get_b_4() const { return ___b_4; }
	inline float* get_address_of_b_4() { return &___b_4; }
	inline void set_b_4(float value)
	{
		___b_4 = value;
	}

	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___a_5)); }
	inline float get_a_5() const { return ___a_5; }
	inline float* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(float value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_r2_6() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___r2_6)); }
	inline float get_r2_6() const { return ___r2_6; }
	inline float* get_address_of_r2_6() { return &___r2_6; }
	inline void set_r2_6(float value)
	{
		___r2_6 = value;
	}

	inline static int32_t get_offset_of_g2_7() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___g2_7)); }
	inline float get_g2_7() const { return ___g2_7; }
	inline float* get_address_of_g2_7() { return &___g2_7; }
	inline void set_g2_7(float value)
	{
		___g2_7 = value;
	}

	inline static int32_t get_offset_of_b2_8() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___b2_8)); }
	inline float get_b2_8() const { return ___b2_8; }
	inline float* get_address_of_b2_8() { return &___b2_8; }
	inline void set_b2_8(float value)
	{
		___b2_8 = value;
	}

	inline static int32_t get_offset_of_hasSecondColor_9() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___hasSecondColor_9)); }
	inline bool get_hasSecondColor_9() const { return ___hasSecondColor_9; }
	inline bool* get_address_of_hasSecondColor_9() { return &___hasSecondColor_9; }
	inline void set_hasSecondColor_9(bool value)
	{
		___hasSecondColor_9 = value;
	}

	inline static int32_t get_offset_of_attachment_10() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___attachment_10)); }
	inline Attachment_t397756874 * get_attachment_10() const { return ___attachment_10; }
	inline Attachment_t397756874 ** get_address_of_attachment_10() { return &___attachment_10; }
	inline void set_attachment_10(Attachment_t397756874 * value)
	{
		___attachment_10 = value;
		Il2CppCodeGenWriteBarrier((&___attachment_10), value);
	}

	inline static int32_t get_offset_of_attachmentTime_11() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___attachmentTime_11)); }
	inline float get_attachmentTime_11() const { return ___attachmentTime_11; }
	inline float* get_address_of_attachmentTime_11() { return &___attachmentTime_11; }
	inline void set_attachmentTime_11(float value)
	{
		___attachmentTime_11 = value;
	}

	inline static int32_t get_offset_of_attachmentVertices_12() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___attachmentVertices_12)); }
	inline ExposedList_1_t3384296343 * get_attachmentVertices_12() const { return ___attachmentVertices_12; }
	inline ExposedList_1_t3384296343 ** get_address_of_attachmentVertices_12() { return &___attachmentVertices_12; }
	inline void set_attachmentVertices_12(ExposedList_1_t3384296343 * value)
	{
		___attachmentVertices_12 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentVertices_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T1804116343_H
#ifndef LINKEDMESH_T3424637972_H
#define LINKEDMESH_T3424637972_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SkeletonJson/LinkedMesh
struct  LinkedMesh_t3424637972  : public RuntimeObject
{
public:
	// System.String Spine.SkeletonJson/LinkedMesh::parent
	String_t* ___parent_0;
	// System.String Spine.SkeletonJson/LinkedMesh::skin
	String_t* ___skin_1;
	// System.Int32 Spine.SkeletonJson/LinkedMesh::slotIndex
	int32_t ___slotIndex_2;
	// Spine.MeshAttachment Spine.SkeletonJson/LinkedMesh::mesh
	MeshAttachment_t3550597918 * ___mesh_3;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(LinkedMesh_t3424637972, ___parent_0)); }
	inline String_t* get_parent_0() const { return ___parent_0; }
	inline String_t** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(String_t* value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_skin_1() { return static_cast<int32_t>(offsetof(LinkedMesh_t3424637972, ___skin_1)); }
	inline String_t* get_skin_1() const { return ___skin_1; }
	inline String_t** get_address_of_skin_1() { return &___skin_1; }
	inline void set_skin_1(String_t* value)
	{
		___skin_1 = value;
		Il2CppCodeGenWriteBarrier((&___skin_1), value);
	}

	inline static int32_t get_offset_of_slotIndex_2() { return static_cast<int32_t>(offsetof(LinkedMesh_t3424637972, ___slotIndex_2)); }
	inline int32_t get_slotIndex_2() const { return ___slotIndex_2; }
	inline int32_t* get_address_of_slotIndex_2() { return &___slotIndex_2; }
	inline void set_slotIndex_2(int32_t value)
	{
		___slotIndex_2 = value;
	}

	inline static int32_t get_offset_of_mesh_3() { return static_cast<int32_t>(offsetof(LinkedMesh_t3424637972, ___mesh_3)); }
	inline MeshAttachment_t3550597918 * get_mesh_3() const { return ___mesh_3; }
	inline MeshAttachment_t3550597918 ** get_address_of_mesh_3() { return &___mesh_3; }
	inline void set_mesh_3(MeshAttachment_t3550597918 * value)
	{
		___mesh_3 = value;
		Il2CppCodeGenWriteBarrier((&___mesh_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDMESH_T3424637972_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2092556417_H
#define U3CSTARTU3EC__ITERATOR0_T2092556417_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2092556417  : public RuntimeObject
{
public:
	// Spine.Unity.Modules.SkeletonRagdoll Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0::$this
	SkeletonRagdoll_t1253535559 * ___U24this_0;
	// System.Object Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll/<Start>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2092556417, ___U24this_0)); }
	inline SkeletonRagdoll_t1253535559 * get_U24this_0() const { return ___U24this_0; }
	inline SkeletonRagdoll_t1253535559 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SkeletonRagdoll_t1253535559 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2092556417, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2092556417, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2092556417, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2092556417_H
#ifndef VECTOR3_T2825674791_H
#define VECTOR3_T2825674791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2825674791 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2825674791, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2825674791, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2825674791, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2825674791_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2825674791  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2825674791  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2825674791  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2825674791  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2825674791  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2825674791  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2825674791  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2825674791  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2825674791  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2825674791  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2825674791  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2825674791 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2825674791  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___oneVector_5)); }
	inline Vector3_t2825674791  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2825674791 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2825674791  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___upVector_6)); }
	inline Vector3_t2825674791  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2825674791 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2825674791  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___downVector_7)); }
	inline Vector3_t2825674791  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2825674791 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2825674791  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___leftVector_8)); }
	inline Vector3_t2825674791  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2825674791 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2825674791  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___rightVector_9)); }
	inline Vector3_t2825674791  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2825674791 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2825674791  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2825674791  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2825674791 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2825674791  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___backVector_11)); }
	inline Vector3_t2825674791  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2825674791 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2825674791  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2825674791  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2825674791 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2825674791  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2825674791  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2825674791 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2825674791  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2825674791_H
#ifndef HIERARCHY_T2251599244_H
#define HIERARCHY_T2251599244_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttachment/Hierarchy
struct  Hierarchy_t2251599244 
{
public:
	// System.String Spine.Unity.SpineAttachment/Hierarchy::skin
	String_t* ___skin_0;
	// System.String Spine.Unity.SpineAttachment/Hierarchy::slot
	String_t* ___slot_1;
	// System.String Spine.Unity.SpineAttachment/Hierarchy::name
	String_t* ___name_2;

public:
	inline static int32_t get_offset_of_skin_0() { return static_cast<int32_t>(offsetof(Hierarchy_t2251599244, ___skin_0)); }
	inline String_t* get_skin_0() const { return ___skin_0; }
	inline String_t** get_address_of_skin_0() { return &___skin_0; }
	inline void set_skin_0(String_t* value)
	{
		___skin_0 = value;
		Il2CppCodeGenWriteBarrier((&___skin_0), value);
	}

	inline static int32_t get_offset_of_slot_1() { return static_cast<int32_t>(offsetof(Hierarchy_t2251599244, ___slot_1)); }
	inline String_t* get_slot_1() const { return ___slot_1; }
	inline String_t** get_address_of_slot_1() { return &___slot_1; }
	inline void set_slot_1(String_t* value)
	{
		___slot_1 = value;
		Il2CppCodeGenWriteBarrier((&___slot_1), value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(Hierarchy_t2251599244, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.SpineAttachment/Hierarchy
struct Hierarchy_t2251599244_marshaled_pinvoke
{
	char* ___skin_0;
	char* ___slot_1;
	char* ___name_2;
};
// Native definition for COM marshalling of Spine.Unity.SpineAttachment/Hierarchy
struct Hierarchy_t2251599244_marshaled_com
{
	Il2CppChar* ___skin_0;
	Il2CppChar* ___slot_1;
	Il2CppChar* ___name_2;
};
#endif // HIERARCHY_T2251599244_H
#ifndef ENUM_T1912704450_H
#define ENUM_T1912704450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t1912704450  : public ValueType_t2697047229
{
public:

public:
};

struct Enum_t1912704450_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3669675803* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t1912704450_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3669675803* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3669675803** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3669675803* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t1912704450_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t1912704450_marshaled_com
{
};
#endif // ENUM_T1912704450_H
#ifndef VOID_T3157035008_H
#define VOID_T3157035008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t3157035008 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T3157035008_H
#ifndef PROPERTYATTRIBUTE_T1226012101_H
#define PROPERTYATTRIBUTE_T1226012101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t1226012101  : public Attribute_t2169343654
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T1226012101_H
#ifndef LAYERMASK_T3306677380_H
#define LAYERMASK_T3306677380_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_t3306677380 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t3306677380, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_T3306677380_H
#ifndef COLOR32_T2411287715_H
#define COLOR32_T2411287715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t2411287715 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t2411287715, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t2411287715, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t2411287715, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t2411287715, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2411287715_H
#ifndef MATERIALTEXTUREPAIR_T2729914733_H
#define MATERIALTEXTUREPAIR_T2729914733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair
struct  MaterialTexturePair_t2729914733 
{
public:
	// UnityEngine.Texture2D Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair::texture2D
	Texture2D_t1431210461 * ___texture2D_0;
	// UnityEngine.Material Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair::material
	Material_t2681358023 * ___material_1;

public:
	inline static int32_t get_offset_of_texture2D_0() { return static_cast<int32_t>(offsetof(MaterialTexturePair_t2729914733, ___texture2D_0)); }
	inline Texture2D_t1431210461 * get_texture2D_0() const { return ___texture2D_0; }
	inline Texture2D_t1431210461 ** get_address_of_texture2D_0() { return &___texture2D_0; }
	inline void set_texture2D_0(Texture2D_t1431210461 * value)
	{
		___texture2D_0 = value;
		Il2CppCodeGenWriteBarrier((&___texture2D_0), value);
	}

	inline static int32_t get_offset_of_material_1() { return static_cast<int32_t>(offsetof(MaterialTexturePair_t2729914733, ___material_1)); }
	inline Material_t2681358023 * get_material_1() const { return ___material_1; }
	inline Material_t2681358023 ** get_address_of_material_1() { return &___material_1; }
	inline void set_material_1(Material_t2681358023 * value)
	{
		___material_1 = value;
		Il2CppCodeGenWriteBarrier((&___material_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair
struct MaterialTexturePair_t2729914733_marshaled_pinvoke
{
	Texture2D_t1431210461 * ___texture2D_0;
	Material_t2681358023 * ___material_1;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair
struct MaterialTexturePair_t2729914733_marshaled_com
{
	Texture2D_t1431210461 * ___texture2D_0;
	Material_t2681358023 * ___material_1;
};
#endif // MATERIALTEXTUREPAIR_T2729914733_H
#ifndef TRANSFORMPAIR_T1730302243_H
#define TRANSFORMPAIR_T1730302243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair
struct  TransformPair_t1730302243 
{
public:
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair::dest
	Transform_t2735953680 * ___dest_0;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair::src
	Transform_t2735953680 * ___src_1;

public:
	inline static int32_t get_offset_of_dest_0() { return static_cast<int32_t>(offsetof(TransformPair_t1730302243, ___dest_0)); }
	inline Transform_t2735953680 * get_dest_0() const { return ___dest_0; }
	inline Transform_t2735953680 ** get_address_of_dest_0() { return &___dest_0; }
	inline void set_dest_0(Transform_t2735953680 * value)
	{
		___dest_0 = value;
		Il2CppCodeGenWriteBarrier((&___dest_0), value);
	}

	inline static int32_t get_offset_of_src_1() { return static_cast<int32_t>(offsetof(TransformPair_t1730302243, ___src_1)); }
	inline Transform_t2735953680 * get_src_1() const { return ___src_1; }
	inline Transform_t2735953680 ** get_address_of_src_1() { return &___src_1; }
	inline void set_src_1(Transform_t2735953680 * value)
	{
		___src_1 = value;
		Il2CppCodeGenWriteBarrier((&___src_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair
struct TransformPair_t1730302243_marshaled_pinvoke
{
	Transform_t2735953680 * ___dest_0;
	Transform_t2735953680 * ___src_1;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair
struct TransformPair_t1730302243_marshaled_com
{
	Transform_t2735953680 * ___dest_0;
	Transform_t2735953680 * ___src_1;
};
#endif // TRANSFORMPAIR_T1730302243_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR2_T1065050092_H
#define VECTOR2_T1065050092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t1065050092 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t1065050092, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t1065050092, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t1065050092_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1065050092  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1065050092  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1065050092  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1065050092  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1065050092  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1065050092  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1065050092  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1065050092  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___zeroVector_2)); }
	inline Vector2_t1065050092  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t1065050092 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t1065050092  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___oneVector_3)); }
	inline Vector2_t1065050092  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t1065050092 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t1065050092  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___upVector_4)); }
	inline Vector2_t1065050092  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t1065050092 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t1065050092  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___downVector_5)); }
	inline Vector2_t1065050092  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t1065050092 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t1065050092  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___leftVector_6)); }
	inline Vector2_t1065050092  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t1065050092 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t1065050092  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___rightVector_7)); }
	inline Vector2_t1065050092  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t1065050092 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t1065050092  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t1065050092  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t1065050092 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t1065050092  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t1065050092  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t1065050092 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t1065050092  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T1065050092_H
#ifndef COLOR_T4183816058_H
#define COLOR_T4183816058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t4183816058 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t4183816058, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t4183816058, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t4183816058, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t4183816058, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T4183816058_H
#ifndef SETTINGS_T1339330787_H
#define SETTINGS_T1339330787_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGenerator/Settings
struct  Settings_t1339330787 
{
public:
	// System.Boolean Spine.Unity.MeshGenerator/Settings::useClipping
	bool ___useClipping_0;
	// System.Single Spine.Unity.MeshGenerator/Settings::zSpacing
	float ___zSpacing_1;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::pmaVertexColors
	bool ___pmaVertexColors_2;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::tintBlack
	bool ___tintBlack_3;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::calculateTangents
	bool ___calculateTangents_4;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::addNormals
	bool ___addNormals_5;
	// System.Boolean Spine.Unity.MeshGenerator/Settings::immutableTriangles
	bool ___immutableTriangles_6;

public:
	inline static int32_t get_offset_of_useClipping_0() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___useClipping_0)); }
	inline bool get_useClipping_0() const { return ___useClipping_0; }
	inline bool* get_address_of_useClipping_0() { return &___useClipping_0; }
	inline void set_useClipping_0(bool value)
	{
		___useClipping_0 = value;
	}

	inline static int32_t get_offset_of_zSpacing_1() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___zSpacing_1)); }
	inline float get_zSpacing_1() const { return ___zSpacing_1; }
	inline float* get_address_of_zSpacing_1() { return &___zSpacing_1; }
	inline void set_zSpacing_1(float value)
	{
		___zSpacing_1 = value;
	}

	inline static int32_t get_offset_of_pmaVertexColors_2() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___pmaVertexColors_2)); }
	inline bool get_pmaVertexColors_2() const { return ___pmaVertexColors_2; }
	inline bool* get_address_of_pmaVertexColors_2() { return &___pmaVertexColors_2; }
	inline void set_pmaVertexColors_2(bool value)
	{
		___pmaVertexColors_2 = value;
	}

	inline static int32_t get_offset_of_tintBlack_3() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___tintBlack_3)); }
	inline bool get_tintBlack_3() const { return ___tintBlack_3; }
	inline bool* get_address_of_tintBlack_3() { return &___tintBlack_3; }
	inline void set_tintBlack_3(bool value)
	{
		___tintBlack_3 = value;
	}

	inline static int32_t get_offset_of_calculateTangents_4() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___calculateTangents_4)); }
	inline bool get_calculateTangents_4() const { return ___calculateTangents_4; }
	inline bool* get_address_of_calculateTangents_4() { return &___calculateTangents_4; }
	inline void set_calculateTangents_4(bool value)
	{
		___calculateTangents_4 = value;
	}

	inline static int32_t get_offset_of_addNormals_5() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___addNormals_5)); }
	inline bool get_addNormals_5() const { return ___addNormals_5; }
	inline bool* get_address_of_addNormals_5() { return &___addNormals_5; }
	inline void set_addNormals_5(bool value)
	{
		___addNormals_5 = value;
	}

	inline static int32_t get_offset_of_immutableTriangles_6() { return static_cast<int32_t>(offsetof(Settings_t1339330787, ___immutableTriangles_6)); }
	inline bool get_immutableTriangles_6() const { return ___immutableTriangles_6; }
	inline bool* get_address_of_immutableTriangles_6() { return &___immutableTriangles_6; }
	inline void set_immutableTriangles_6(bool value)
	{
		___immutableTriangles_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.MeshGenerator/Settings
struct Settings_t1339330787_marshaled_pinvoke
{
	int32_t ___useClipping_0;
	float ___zSpacing_1;
	int32_t ___pmaVertexColors_2;
	int32_t ___tintBlack_3;
	int32_t ___calculateTangents_4;
	int32_t ___addNormals_5;
	int32_t ___immutableTriangles_6;
};
// Native definition for COM marshalling of Spine.Unity.MeshGenerator/Settings
struct Settings_t1339330787_marshaled_com
{
	int32_t ___useClipping_0;
	float ___zSpacing_1;
	int32_t ___pmaVertexColors_2;
	int32_t ___tintBlack_3;
	int32_t ___calculateTangents_4;
	int32_t ___addNormals_5;
	int32_t ___immutableTriangles_6;
};
#endif // SETTINGS_T1339330787_H
#ifndef SUBMESHINSTRUCTION_T3765917328_H
#define SUBMESHINSTRUCTION_T3765917328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SubmeshInstruction
struct  SubmeshInstruction_t3765917328 
{
public:
	// Spine.Skeleton Spine.Unity.SubmeshInstruction::skeleton
	Skeleton_t1045203634 * ___skeleton_0;
	// System.Int32 Spine.Unity.SubmeshInstruction::startSlot
	int32_t ___startSlot_1;
	// System.Int32 Spine.Unity.SubmeshInstruction::endSlot
	int32_t ___endSlot_2;
	// UnityEngine.Material Spine.Unity.SubmeshInstruction::material
	Material_t2681358023 * ___material_3;
	// System.Boolean Spine.Unity.SubmeshInstruction::forceSeparate
	bool ___forceSeparate_4;
	// System.Int32 Spine.Unity.SubmeshInstruction::preActiveClippingSlotSource
	int32_t ___preActiveClippingSlotSource_5;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawTriangleCount
	int32_t ___rawTriangleCount_6;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawVertexCount
	int32_t ___rawVertexCount_7;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawFirstVertexIndex
	int32_t ___rawFirstVertexIndex_8;
	// System.Boolean Spine.Unity.SubmeshInstruction::hasClipping
	bool ___hasClipping_9;

public:
	inline static int32_t get_offset_of_skeleton_0() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___skeleton_0)); }
	inline Skeleton_t1045203634 * get_skeleton_0() const { return ___skeleton_0; }
	inline Skeleton_t1045203634 ** get_address_of_skeleton_0() { return &___skeleton_0; }
	inline void set_skeleton_0(Skeleton_t1045203634 * value)
	{
		___skeleton_0 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_0), value);
	}

	inline static int32_t get_offset_of_startSlot_1() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___startSlot_1)); }
	inline int32_t get_startSlot_1() const { return ___startSlot_1; }
	inline int32_t* get_address_of_startSlot_1() { return &___startSlot_1; }
	inline void set_startSlot_1(int32_t value)
	{
		___startSlot_1 = value;
	}

	inline static int32_t get_offset_of_endSlot_2() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___endSlot_2)); }
	inline int32_t get_endSlot_2() const { return ___endSlot_2; }
	inline int32_t* get_address_of_endSlot_2() { return &___endSlot_2; }
	inline void set_endSlot_2(int32_t value)
	{
		___endSlot_2 = value;
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___material_3)); }
	inline Material_t2681358023 * get_material_3() const { return ___material_3; }
	inline Material_t2681358023 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t2681358023 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_forceSeparate_4() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___forceSeparate_4)); }
	inline bool get_forceSeparate_4() const { return ___forceSeparate_4; }
	inline bool* get_address_of_forceSeparate_4() { return &___forceSeparate_4; }
	inline void set_forceSeparate_4(bool value)
	{
		___forceSeparate_4 = value;
	}

	inline static int32_t get_offset_of_preActiveClippingSlotSource_5() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___preActiveClippingSlotSource_5)); }
	inline int32_t get_preActiveClippingSlotSource_5() const { return ___preActiveClippingSlotSource_5; }
	inline int32_t* get_address_of_preActiveClippingSlotSource_5() { return &___preActiveClippingSlotSource_5; }
	inline void set_preActiveClippingSlotSource_5(int32_t value)
	{
		___preActiveClippingSlotSource_5 = value;
	}

	inline static int32_t get_offset_of_rawTriangleCount_6() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___rawTriangleCount_6)); }
	inline int32_t get_rawTriangleCount_6() const { return ___rawTriangleCount_6; }
	inline int32_t* get_address_of_rawTriangleCount_6() { return &___rawTriangleCount_6; }
	inline void set_rawTriangleCount_6(int32_t value)
	{
		___rawTriangleCount_6 = value;
	}

	inline static int32_t get_offset_of_rawVertexCount_7() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___rawVertexCount_7)); }
	inline int32_t get_rawVertexCount_7() const { return ___rawVertexCount_7; }
	inline int32_t* get_address_of_rawVertexCount_7() { return &___rawVertexCount_7; }
	inline void set_rawVertexCount_7(int32_t value)
	{
		___rawVertexCount_7 = value;
	}

	inline static int32_t get_offset_of_rawFirstVertexIndex_8() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___rawFirstVertexIndex_8)); }
	inline int32_t get_rawFirstVertexIndex_8() const { return ___rawFirstVertexIndex_8; }
	inline int32_t* get_address_of_rawFirstVertexIndex_8() { return &___rawFirstVertexIndex_8; }
	inline void set_rawFirstVertexIndex_8(int32_t value)
	{
		___rawFirstVertexIndex_8 = value;
	}

	inline static int32_t get_offset_of_hasClipping_9() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___hasClipping_9)); }
	inline bool get_hasClipping_9() const { return ___hasClipping_9; }
	inline bool* get_address_of_hasClipping_9() { return &___hasClipping_9; }
	inline void set_hasClipping_9(bool value)
	{
		___hasClipping_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.SubmeshInstruction
struct SubmeshInstruction_t3765917328_marshaled_pinvoke
{
	Skeleton_t1045203634 * ___skeleton_0;
	int32_t ___startSlot_1;
	int32_t ___endSlot_2;
	Material_t2681358023 * ___material_3;
	int32_t ___forceSeparate_4;
	int32_t ___preActiveClippingSlotSource_5;
	int32_t ___rawTriangleCount_6;
	int32_t ___rawVertexCount_7;
	int32_t ___rawFirstVertexIndex_8;
	int32_t ___hasClipping_9;
};
// Native definition for COM marshalling of Spine.Unity.SubmeshInstruction
struct SubmeshInstruction_t3765917328_marshaled_com
{
	Skeleton_t1045203634 * ___skeleton_0;
	int32_t ___startSlot_1;
	int32_t ___endSlot_2;
	Material_t2681358023 * ___material_3;
	int32_t ___forceSeparate_4;
	int32_t ___preActiveClippingSlotSource_5;
	int32_t ___rawTriangleCount_6;
	int32_t ___rawVertexCount_7;
	int32_t ___rawFirstVertexIndex_8;
	int32_t ___hasClipping_9;
};
#endif // SUBMESHINSTRUCTION_T3765917328_H
#ifndef MESHGENERATORBUFFERS_T1203282268_H
#define MESHGENERATORBUFFERS_T1203282268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGeneratorBuffers
struct  MeshGeneratorBuffers_t1203282268 
{
public:
	// System.Int32 Spine.Unity.MeshGeneratorBuffers::vertexCount
	int32_t ___vertexCount_0;
	// UnityEngine.Vector3[] Spine.Unity.MeshGeneratorBuffers::vertexBuffer
	Vector3U5BU5D_t3550801758* ___vertexBuffer_1;
	// UnityEngine.Vector2[] Spine.Unity.MeshGeneratorBuffers::uvBuffer
	Vector2U5BU5D_t3719718629* ___uvBuffer_2;
	// UnityEngine.Color32[] Spine.Unity.MeshGeneratorBuffers::colorBuffer
	Color32U5BU5D_t3058208114* ___colorBuffer_3;
	// Spine.Unity.MeshGenerator Spine.Unity.MeshGeneratorBuffers::meshGenerator
	MeshGenerator_t557654067 * ___meshGenerator_4;

public:
	inline static int32_t get_offset_of_vertexCount_0() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1203282268, ___vertexCount_0)); }
	inline int32_t get_vertexCount_0() const { return ___vertexCount_0; }
	inline int32_t* get_address_of_vertexCount_0() { return &___vertexCount_0; }
	inline void set_vertexCount_0(int32_t value)
	{
		___vertexCount_0 = value;
	}

	inline static int32_t get_offset_of_vertexBuffer_1() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1203282268, ___vertexBuffer_1)); }
	inline Vector3U5BU5D_t3550801758* get_vertexBuffer_1() const { return ___vertexBuffer_1; }
	inline Vector3U5BU5D_t3550801758** get_address_of_vertexBuffer_1() { return &___vertexBuffer_1; }
	inline void set_vertexBuffer_1(Vector3U5BU5D_t3550801758* value)
	{
		___vertexBuffer_1 = value;
		Il2CppCodeGenWriteBarrier((&___vertexBuffer_1), value);
	}

	inline static int32_t get_offset_of_uvBuffer_2() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1203282268, ___uvBuffer_2)); }
	inline Vector2U5BU5D_t3719718629* get_uvBuffer_2() const { return ___uvBuffer_2; }
	inline Vector2U5BU5D_t3719718629** get_address_of_uvBuffer_2() { return &___uvBuffer_2; }
	inline void set_uvBuffer_2(Vector2U5BU5D_t3719718629* value)
	{
		___uvBuffer_2 = value;
		Il2CppCodeGenWriteBarrier((&___uvBuffer_2), value);
	}

	inline static int32_t get_offset_of_colorBuffer_3() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1203282268, ___colorBuffer_3)); }
	inline Color32U5BU5D_t3058208114* get_colorBuffer_3() const { return ___colorBuffer_3; }
	inline Color32U5BU5D_t3058208114** get_address_of_colorBuffer_3() { return &___colorBuffer_3; }
	inline void set_colorBuffer_3(Color32U5BU5D_t3058208114* value)
	{
		___colorBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___colorBuffer_3), value);
	}

	inline static int32_t get_offset_of_meshGenerator_4() { return static_cast<int32_t>(offsetof(MeshGeneratorBuffers_t1203282268, ___meshGenerator_4)); }
	inline MeshGenerator_t557654067 * get_meshGenerator_4() const { return ___meshGenerator_4; }
	inline MeshGenerator_t557654067 ** get_address_of_meshGenerator_4() { return &___meshGenerator_4; }
	inline void set_meshGenerator_4(MeshGenerator_t557654067 * value)
	{
		___meshGenerator_4 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.MeshGeneratorBuffers
struct MeshGeneratorBuffers_t1203282268_marshaled_pinvoke
{
	int32_t ___vertexCount_0;
	Vector3_t2825674791 * ___vertexBuffer_1;
	Vector2_t1065050092 * ___uvBuffer_2;
	Color32_t2411287715 * ___colorBuffer_3;
	MeshGenerator_t557654067 * ___meshGenerator_4;
};
// Native definition for COM marshalling of Spine.Unity.MeshGeneratorBuffers
struct MeshGeneratorBuffers_t1203282268_marshaled_com
{
	int32_t ___vertexCount_0;
	Vector3_t2825674791 * ___vertexBuffer_1;
	Vector2_t1065050092 * ___uvBuffer_2;
	Color32_t2411287715 * ___colorBuffer_3;
	MeshGenerator_t557654067 * ___meshGenerator_4;
};
#endif // MESHGENERATORBUFFERS_T1203282268_H
#ifndef SLOTMATERIALOVERRIDE_T3080213539_H
#define SLOTMATERIALOVERRIDE_T3080213539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride
struct  SlotMaterialOverride_t3080213539 
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride::overrideDisabled
	bool ___overrideDisabled_0;
	// System.String Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride::slotName
	String_t* ___slotName_1;
	// UnityEngine.Material Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride::material
	Material_t2681358023 * ___material_2;

public:
	inline static int32_t get_offset_of_overrideDisabled_0() { return static_cast<int32_t>(offsetof(SlotMaterialOverride_t3080213539, ___overrideDisabled_0)); }
	inline bool get_overrideDisabled_0() const { return ___overrideDisabled_0; }
	inline bool* get_address_of_overrideDisabled_0() { return &___overrideDisabled_0; }
	inline void set_overrideDisabled_0(bool value)
	{
		___overrideDisabled_0 = value;
	}

	inline static int32_t get_offset_of_slotName_1() { return static_cast<int32_t>(offsetof(SlotMaterialOverride_t3080213539, ___slotName_1)); }
	inline String_t* get_slotName_1() const { return ___slotName_1; }
	inline String_t** get_address_of_slotName_1() { return &___slotName_1; }
	inline void set_slotName_1(String_t* value)
	{
		___slotName_1 = value;
		Il2CppCodeGenWriteBarrier((&___slotName_1), value);
	}

	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(SlotMaterialOverride_t3080213539, ___material_2)); }
	inline Material_t2681358023 * get_material_2() const { return ___material_2; }
	inline Material_t2681358023 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t2681358023 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride
struct SlotMaterialOverride_t3080213539_marshaled_pinvoke
{
	int32_t ___overrideDisabled_0;
	char* ___slotName_1;
	Material_t2681358023 * ___material_2;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride
struct SlotMaterialOverride_t3080213539_marshaled_com
{
	int32_t ___overrideDisabled_0;
	Il2CppChar* ___slotName_1;
	Material_t2681358023 * ___material_2;
};
#endif // SLOTMATERIALOVERRIDE_T3080213539_H
#ifndef ATLASMATERIALOVERRIDE_T4245281532_H
#define ATLASMATERIALOVERRIDE_T4245281532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride
struct  AtlasMaterialOverride_t4245281532 
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride::overrideDisabled
	bool ___overrideDisabled_0;
	// UnityEngine.Material Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride::originalMaterial
	Material_t2681358023 * ___originalMaterial_1;
	// UnityEngine.Material Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride::replacementMaterial
	Material_t2681358023 * ___replacementMaterial_2;

public:
	inline static int32_t get_offset_of_overrideDisabled_0() { return static_cast<int32_t>(offsetof(AtlasMaterialOverride_t4245281532, ___overrideDisabled_0)); }
	inline bool get_overrideDisabled_0() const { return ___overrideDisabled_0; }
	inline bool* get_address_of_overrideDisabled_0() { return &___overrideDisabled_0; }
	inline void set_overrideDisabled_0(bool value)
	{
		___overrideDisabled_0 = value;
	}

	inline static int32_t get_offset_of_originalMaterial_1() { return static_cast<int32_t>(offsetof(AtlasMaterialOverride_t4245281532, ___originalMaterial_1)); }
	inline Material_t2681358023 * get_originalMaterial_1() const { return ___originalMaterial_1; }
	inline Material_t2681358023 ** get_address_of_originalMaterial_1() { return &___originalMaterial_1; }
	inline void set_originalMaterial_1(Material_t2681358023 * value)
	{
		___originalMaterial_1 = value;
		Il2CppCodeGenWriteBarrier((&___originalMaterial_1), value);
	}

	inline static int32_t get_offset_of_replacementMaterial_2() { return static_cast<int32_t>(offsetof(AtlasMaterialOverride_t4245281532, ___replacementMaterial_2)); }
	inline Material_t2681358023 * get_replacementMaterial_2() const { return ___replacementMaterial_2; }
	inline Material_t2681358023 ** get_address_of_replacementMaterial_2() { return &___replacementMaterial_2; }
	inline void set_replacementMaterial_2(Material_t2681358023 * value)
	{
		___replacementMaterial_2 = value;
		Il2CppCodeGenWriteBarrier((&___replacementMaterial_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride
struct AtlasMaterialOverride_t4245281532_marshaled_pinvoke
{
	int32_t ___overrideDisabled_0;
	Material_t2681358023 * ___originalMaterial_1;
	Material_t2681358023 * ___replacementMaterial_2;
};
// Native definition for COM marshalling of Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride
struct AtlasMaterialOverride_t4245281532_marshaled_com
{
	int32_t ___overrideDisabled_0;
	Material_t2681358023 * ___originalMaterial_1;
	Material_t2681358023 * ___replacementMaterial_2;
};
#endif // ATLASMATERIALOVERRIDE_T4245281532_H
#ifndef ATTACHMENTKEYTUPLE_T2283295499_H
#define ATTACHMENTKEYTUPLE_T2283295499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skin/AttachmentKeyTuple
struct  AttachmentKeyTuple_t2283295499 
{
public:
	// System.Int32 Spine.Skin/AttachmentKeyTuple::slotIndex
	int32_t ___slotIndex_0;
	// System.String Spine.Skin/AttachmentKeyTuple::name
	String_t* ___name_1;
	// System.Int32 Spine.Skin/AttachmentKeyTuple::nameHashCode
	int32_t ___nameHashCode_2;

public:
	inline static int32_t get_offset_of_slotIndex_0() { return static_cast<int32_t>(offsetof(AttachmentKeyTuple_t2283295499, ___slotIndex_0)); }
	inline int32_t get_slotIndex_0() const { return ___slotIndex_0; }
	inline int32_t* get_address_of_slotIndex_0() { return &___slotIndex_0; }
	inline void set_slotIndex_0(int32_t value)
	{
		___slotIndex_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(AttachmentKeyTuple_t2283295499, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_nameHashCode_2() { return static_cast<int32_t>(offsetof(AttachmentKeyTuple_t2283295499, ___nameHashCode_2)); }
	inline int32_t get_nameHashCode_2() const { return ___nameHashCode_2; }
	inline int32_t* get_address_of_nameHashCode_2() { return &___nameHashCode_2; }
	inline void set_nameHashCode_2(int32_t value)
	{
		___nameHashCode_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Skin/AttachmentKeyTuple
struct AttachmentKeyTuple_t2283295499_marshaled_pinvoke
{
	int32_t ___slotIndex_0;
	char* ___name_1;
	int32_t ___nameHashCode_2;
};
// Native definition for COM marshalling of Spine.Skin/AttachmentKeyTuple
struct AttachmentKeyTuple_t2283295499_marshaled_com
{
	int32_t ___slotIndex_0;
	Il2CppChar* ___name_1;
	int32_t ___nameHashCode_2;
};
#endif // ATTACHMENTKEYTUPLE_T2283295499_H
#ifndef LAYERFIELDATTRIBUTE_T2281748996_H
#define LAYERFIELDATTRIBUTE_T2281748996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll/LayerFieldAttribute
struct  LayerFieldAttribute_t2281748996  : public PropertyAttribute_t1226012101
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERFIELDATTRIBUTE_T2281748996_H
#ifndef MODE_T3269804131_H
#define MODE_T3269804131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityBone/Mode
struct  Mode_t3269804131 
{
public:
	// System.Int32 Spine.Unity.SkeletonUtilityBone/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3269804131, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3269804131_H
#ifndef BLENDMODE_T1090287593_H
#define BLENDMODE_T1090287593_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.BlendMode
struct  BlendMode_t1090287593 
{
public:
	// System.Int32 Spine.BlendMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BlendMode_t1090287593, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDMODE_T1090287593_H
#ifndef MESHGENERATOR_T557654067_H
#define MESHGENERATOR_T557654067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGenerator
struct  MeshGenerator_t557654067  : public RuntimeObject
{
public:
	// Spine.Unity.MeshGenerator/Settings Spine.Unity.MeshGenerator::settings
	Settings_t1339330787  ___settings_0;
	// Spine.ExposedList`1<UnityEngine.Vector3> Spine.Unity.MeshGenerator::vertexBuffer
	ExposedList_1_t380703334 * ___vertexBuffer_3;
	// Spine.ExposedList`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::uvBuffer
	ExposedList_1_t2915045931 * ___uvBuffer_4;
	// Spine.ExposedList`1<UnityEngine.Color32> Spine.Unity.MeshGenerator::colorBuffer
	ExposedList_1_t4261283554 * ___colorBuffer_5;
	// Spine.ExposedList`1<Spine.ExposedList`1<System.Int32>> Spine.Unity.MeshGenerator::submeshes
	ExposedList_1_t3521067698 * ___submeshes_6;
	// UnityEngine.Vector2 Spine.Unity.MeshGenerator::meshBoundsMin
	Vector2_t1065050092  ___meshBoundsMin_7;
	// UnityEngine.Vector2 Spine.Unity.MeshGenerator::meshBoundsMax
	Vector2_t1065050092  ___meshBoundsMax_8;
	// System.Single Spine.Unity.MeshGenerator::meshBoundsThickness
	float ___meshBoundsThickness_9;
	// System.Int32 Spine.Unity.MeshGenerator::submeshIndex
	int32_t ___submeshIndex_10;
	// Spine.SkeletonClipping Spine.Unity.MeshGenerator::clipper
	SkeletonClipping_t2504359873 * ___clipper_11;
	// System.Single[] Spine.Unity.MeshGenerator::tempVerts
	SingleU5BU5D_t3989243465* ___tempVerts_12;
	// System.Int32[] Spine.Unity.MeshGenerator::regionTriangles
	Int32U5BU5D_t281224829* ___regionTriangles_13;
	// UnityEngine.Vector3[] Spine.Unity.MeshGenerator::normals
	Vector3U5BU5D_t3550801758* ___normals_14;
	// UnityEngine.Vector4[] Spine.Unity.MeshGenerator::tangents
	Vector4U5BU5D_t829909980* ___tangents_15;
	// UnityEngine.Vector2[] Spine.Unity.MeshGenerator::tempTanBuffer
	Vector2U5BU5D_t3719718629* ___tempTanBuffer_16;
	// Spine.ExposedList`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::uv2
	ExposedList_1_t2915045931 * ___uv2_17;
	// Spine.ExposedList`1<UnityEngine.Vector2> Spine.Unity.MeshGenerator::uv3
	ExposedList_1_t2915045931 * ___uv3_18;

public:
	inline static int32_t get_offset_of_settings_0() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___settings_0)); }
	inline Settings_t1339330787  get_settings_0() const { return ___settings_0; }
	inline Settings_t1339330787 * get_address_of_settings_0() { return &___settings_0; }
	inline void set_settings_0(Settings_t1339330787  value)
	{
		___settings_0 = value;
	}

	inline static int32_t get_offset_of_vertexBuffer_3() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___vertexBuffer_3)); }
	inline ExposedList_1_t380703334 * get_vertexBuffer_3() const { return ___vertexBuffer_3; }
	inline ExposedList_1_t380703334 ** get_address_of_vertexBuffer_3() { return &___vertexBuffer_3; }
	inline void set_vertexBuffer_3(ExposedList_1_t380703334 * value)
	{
		___vertexBuffer_3 = value;
		Il2CppCodeGenWriteBarrier((&___vertexBuffer_3), value);
	}

	inline static int32_t get_offset_of_uvBuffer_4() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___uvBuffer_4)); }
	inline ExposedList_1_t2915045931 * get_uvBuffer_4() const { return ___uvBuffer_4; }
	inline ExposedList_1_t2915045931 ** get_address_of_uvBuffer_4() { return &___uvBuffer_4; }
	inline void set_uvBuffer_4(ExposedList_1_t2915045931 * value)
	{
		___uvBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___uvBuffer_4), value);
	}

	inline static int32_t get_offset_of_colorBuffer_5() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___colorBuffer_5)); }
	inline ExposedList_1_t4261283554 * get_colorBuffer_5() const { return ___colorBuffer_5; }
	inline ExposedList_1_t4261283554 ** get_address_of_colorBuffer_5() { return &___colorBuffer_5; }
	inline void set_colorBuffer_5(ExposedList_1_t4261283554 * value)
	{
		___colorBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((&___colorBuffer_5), value);
	}

	inline static int32_t get_offset_of_submeshes_6() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___submeshes_6)); }
	inline ExposedList_1_t3521067698 * get_submeshes_6() const { return ___submeshes_6; }
	inline ExposedList_1_t3521067698 ** get_address_of_submeshes_6() { return &___submeshes_6; }
	inline void set_submeshes_6(ExposedList_1_t3521067698 * value)
	{
		___submeshes_6 = value;
		Il2CppCodeGenWriteBarrier((&___submeshes_6), value);
	}

	inline static int32_t get_offset_of_meshBoundsMin_7() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___meshBoundsMin_7)); }
	inline Vector2_t1065050092  get_meshBoundsMin_7() const { return ___meshBoundsMin_7; }
	inline Vector2_t1065050092 * get_address_of_meshBoundsMin_7() { return &___meshBoundsMin_7; }
	inline void set_meshBoundsMin_7(Vector2_t1065050092  value)
	{
		___meshBoundsMin_7 = value;
	}

	inline static int32_t get_offset_of_meshBoundsMax_8() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___meshBoundsMax_8)); }
	inline Vector2_t1065050092  get_meshBoundsMax_8() const { return ___meshBoundsMax_8; }
	inline Vector2_t1065050092 * get_address_of_meshBoundsMax_8() { return &___meshBoundsMax_8; }
	inline void set_meshBoundsMax_8(Vector2_t1065050092  value)
	{
		___meshBoundsMax_8 = value;
	}

	inline static int32_t get_offset_of_meshBoundsThickness_9() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___meshBoundsThickness_9)); }
	inline float get_meshBoundsThickness_9() const { return ___meshBoundsThickness_9; }
	inline float* get_address_of_meshBoundsThickness_9() { return &___meshBoundsThickness_9; }
	inline void set_meshBoundsThickness_9(float value)
	{
		___meshBoundsThickness_9 = value;
	}

	inline static int32_t get_offset_of_submeshIndex_10() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___submeshIndex_10)); }
	inline int32_t get_submeshIndex_10() const { return ___submeshIndex_10; }
	inline int32_t* get_address_of_submeshIndex_10() { return &___submeshIndex_10; }
	inline void set_submeshIndex_10(int32_t value)
	{
		___submeshIndex_10 = value;
	}

	inline static int32_t get_offset_of_clipper_11() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___clipper_11)); }
	inline SkeletonClipping_t2504359873 * get_clipper_11() const { return ___clipper_11; }
	inline SkeletonClipping_t2504359873 ** get_address_of_clipper_11() { return &___clipper_11; }
	inline void set_clipper_11(SkeletonClipping_t2504359873 * value)
	{
		___clipper_11 = value;
		Il2CppCodeGenWriteBarrier((&___clipper_11), value);
	}

	inline static int32_t get_offset_of_tempVerts_12() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___tempVerts_12)); }
	inline SingleU5BU5D_t3989243465* get_tempVerts_12() const { return ___tempVerts_12; }
	inline SingleU5BU5D_t3989243465** get_address_of_tempVerts_12() { return &___tempVerts_12; }
	inline void set_tempVerts_12(SingleU5BU5D_t3989243465* value)
	{
		___tempVerts_12 = value;
		Il2CppCodeGenWriteBarrier((&___tempVerts_12), value);
	}

	inline static int32_t get_offset_of_regionTriangles_13() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___regionTriangles_13)); }
	inline Int32U5BU5D_t281224829* get_regionTriangles_13() const { return ___regionTriangles_13; }
	inline Int32U5BU5D_t281224829** get_address_of_regionTriangles_13() { return &___regionTriangles_13; }
	inline void set_regionTriangles_13(Int32U5BU5D_t281224829* value)
	{
		___regionTriangles_13 = value;
		Il2CppCodeGenWriteBarrier((&___regionTriangles_13), value);
	}

	inline static int32_t get_offset_of_normals_14() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___normals_14)); }
	inline Vector3U5BU5D_t3550801758* get_normals_14() const { return ___normals_14; }
	inline Vector3U5BU5D_t3550801758** get_address_of_normals_14() { return &___normals_14; }
	inline void set_normals_14(Vector3U5BU5D_t3550801758* value)
	{
		___normals_14 = value;
		Il2CppCodeGenWriteBarrier((&___normals_14), value);
	}

	inline static int32_t get_offset_of_tangents_15() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___tangents_15)); }
	inline Vector4U5BU5D_t829909980* get_tangents_15() const { return ___tangents_15; }
	inline Vector4U5BU5D_t829909980** get_address_of_tangents_15() { return &___tangents_15; }
	inline void set_tangents_15(Vector4U5BU5D_t829909980* value)
	{
		___tangents_15 = value;
		Il2CppCodeGenWriteBarrier((&___tangents_15), value);
	}

	inline static int32_t get_offset_of_tempTanBuffer_16() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___tempTanBuffer_16)); }
	inline Vector2U5BU5D_t3719718629* get_tempTanBuffer_16() const { return ___tempTanBuffer_16; }
	inline Vector2U5BU5D_t3719718629** get_address_of_tempTanBuffer_16() { return &___tempTanBuffer_16; }
	inline void set_tempTanBuffer_16(Vector2U5BU5D_t3719718629* value)
	{
		___tempTanBuffer_16 = value;
		Il2CppCodeGenWriteBarrier((&___tempTanBuffer_16), value);
	}

	inline static int32_t get_offset_of_uv2_17() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___uv2_17)); }
	inline ExposedList_1_t2915045931 * get_uv2_17() const { return ___uv2_17; }
	inline ExposedList_1_t2915045931 ** get_address_of_uv2_17() { return &___uv2_17; }
	inline void set_uv2_17(ExposedList_1_t2915045931 * value)
	{
		___uv2_17 = value;
		Il2CppCodeGenWriteBarrier((&___uv2_17), value);
	}

	inline static int32_t get_offset_of_uv3_18() { return static_cast<int32_t>(offsetof(MeshGenerator_t557654067, ___uv3_18)); }
	inline ExposedList_1_t2915045931 * get_uv3_18() const { return ___uv3_18; }
	inline ExposedList_1_t2915045931 ** get_address_of_uv3_18() { return &___uv3_18; }
	inline void set_uv3_18(ExposedList_1_t2915045931 * value)
	{
		___uv3_18 = value;
		Il2CppCodeGenWriteBarrier((&___uv3_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHGENERATOR_T557654067_H
#ifndef MIXMODE_T2985542706_H
#define MIXMODE_T2985542706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode
struct  MixMode_t2985542706 
{
public:
	// System.Int32 Spine.Unity.SkeletonAnimator/MecanimTranslator/MixMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MixMode_t2985542706, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MIXMODE_T2985542706_H
#ifndef HIDEFLAGS_T1317629733_H
#define HIDEFLAGS_T1317629733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t1317629733 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t1317629733, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T1317629733_H
#ifndef TEXTUREFORMAT_T2543749494_H
#define TEXTUREFORMAT_T2543749494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2543749494 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2543749494, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2543749494_H
#ifndef SPINEATTRIBUTEBASE_T3125339574_H
#define SPINEATTRIBUTEBASE_T3125339574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttributeBase
struct  SpineAttributeBase_t3125339574  : public PropertyAttribute_t1226012101
{
public:
	// System.String Spine.Unity.SpineAttributeBase::dataField
	String_t* ___dataField_0;
	// System.String Spine.Unity.SpineAttributeBase::startsWith
	String_t* ___startsWith_1;
	// System.Boolean Spine.Unity.SpineAttributeBase::includeNone
	bool ___includeNone_2;
	// System.Boolean Spine.Unity.SpineAttributeBase::fallbackToTextField
	bool ___fallbackToTextField_3;

public:
	inline static int32_t get_offset_of_dataField_0() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t3125339574, ___dataField_0)); }
	inline String_t* get_dataField_0() const { return ___dataField_0; }
	inline String_t** get_address_of_dataField_0() { return &___dataField_0; }
	inline void set_dataField_0(String_t* value)
	{
		___dataField_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataField_0), value);
	}

	inline static int32_t get_offset_of_startsWith_1() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t3125339574, ___startsWith_1)); }
	inline String_t* get_startsWith_1() const { return ___startsWith_1; }
	inline String_t** get_address_of_startsWith_1() { return &___startsWith_1; }
	inline void set_startsWith_1(String_t* value)
	{
		___startsWith_1 = value;
		Il2CppCodeGenWriteBarrier((&___startsWith_1), value);
	}

	inline static int32_t get_offset_of_includeNone_2() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t3125339574, ___includeNone_2)); }
	inline bool get_includeNone_2() const { return ___includeNone_2; }
	inline bool* get_address_of_includeNone_2() { return &___includeNone_2; }
	inline void set_includeNone_2(bool value)
	{
		___includeNone_2 = value;
	}

	inline static int32_t get_offset_of_fallbackToTextField_3() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t3125339574, ___fallbackToTextField_3)); }
	inline bool get_fallbackToTextField_3() const { return ___fallbackToTextField_3; }
	inline bool* get_address_of_fallbackToTextField_3() { return &___fallbackToTextField_3; }
	inline void set_fallbackToTextField_3(bool value)
	{
		___fallbackToTextField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATTRIBUTEBASE_T3125339574_H
#ifndef ROTATEMODE_T1288973915_H
#define ROTATEMODE_T1288973915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.RotateMode
struct  RotateMode_t1288973915 
{
public:
	// System.Int32 Spine.RotateMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RotateMode_t1288973915, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEMODE_T1288973915_H
#ifndef SPACINGMODE_T2585861093_H
#define SPACINGMODE_T2585861093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SpacingMode
struct  SpacingMode_t2585861093 
{
public:
	// System.Int32 Spine.SpacingMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SpacingMode_t2585861093, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACINGMODE_T2585861093_H
#ifndef U3CFADEADDITIVEU3EC__ITERATOR1_T1481999734_H
#define U3CFADEADDITIVEU3EC__ITERATOR1_T1481999734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1
struct  U3CFadeAdditiveU3Ec__Iterator1_t1481999734  : public RuntimeObject
{
public:
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::<black>__0
	Color32_t2411287715  ___U3CblackU3E__0_0;
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::<t>__1
	int32_t ___U3CtU3E__1_1;
	// System.Boolean Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::<breakout>__2
	bool ___U3CbreakoutU3E__2_2;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::<c>__3
	Color32_t2411287715  ___U3CcU3E__3_3;
	// Spine.Unity.Modules.SkeletonGhostRenderer Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::$this
	SkeletonGhostRenderer_t108436892 * ___U24this_4;
	// System.Object Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::$current
	RuntimeObject * ___U24current_5;
	// System.Boolean Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::$disposing
	bool ___U24disposing_6;
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<FadeAdditive>c__Iterator1::$PC
	int32_t ___U24PC_7;

public:
	inline static int32_t get_offset_of_U3CblackU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t1481999734, ___U3CblackU3E__0_0)); }
	inline Color32_t2411287715  get_U3CblackU3E__0_0() const { return ___U3CblackU3E__0_0; }
	inline Color32_t2411287715 * get_address_of_U3CblackU3E__0_0() { return &___U3CblackU3E__0_0; }
	inline void set_U3CblackU3E__0_0(Color32_t2411287715  value)
	{
		___U3CblackU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E__1_1() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t1481999734, ___U3CtU3E__1_1)); }
	inline int32_t get_U3CtU3E__1_1() const { return ___U3CtU3E__1_1; }
	inline int32_t* get_address_of_U3CtU3E__1_1() { return &___U3CtU3E__1_1; }
	inline void set_U3CtU3E__1_1(int32_t value)
	{
		___U3CtU3E__1_1 = value;
	}

	inline static int32_t get_offset_of_U3CbreakoutU3E__2_2() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t1481999734, ___U3CbreakoutU3E__2_2)); }
	inline bool get_U3CbreakoutU3E__2_2() const { return ___U3CbreakoutU3E__2_2; }
	inline bool* get_address_of_U3CbreakoutU3E__2_2() { return &___U3CbreakoutU3E__2_2; }
	inline void set_U3CbreakoutU3E__2_2(bool value)
	{
		___U3CbreakoutU3E__2_2 = value;
	}

	inline static int32_t get_offset_of_U3CcU3E__3_3() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t1481999734, ___U3CcU3E__3_3)); }
	inline Color32_t2411287715  get_U3CcU3E__3_3() const { return ___U3CcU3E__3_3; }
	inline Color32_t2411287715 * get_address_of_U3CcU3E__3_3() { return &___U3CcU3E__3_3; }
	inline void set_U3CcU3E__3_3(Color32_t2411287715  value)
	{
		___U3CcU3E__3_3 = value;
	}

	inline static int32_t get_offset_of_U24this_4() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t1481999734, ___U24this_4)); }
	inline SkeletonGhostRenderer_t108436892 * get_U24this_4() const { return ___U24this_4; }
	inline SkeletonGhostRenderer_t108436892 ** get_address_of_U24this_4() { return &___U24this_4; }
	inline void set_U24this_4(SkeletonGhostRenderer_t108436892 * value)
	{
		___U24this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_4), value);
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t1481999734, ___U24current_5)); }
	inline RuntimeObject * get_U24current_5() const { return ___U24current_5; }
	inline RuntimeObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(RuntimeObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_5), value);
	}

	inline static int32_t get_offset_of_U24disposing_6() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t1481999734, ___U24disposing_6)); }
	inline bool get_U24disposing_6() const { return ___U24disposing_6; }
	inline bool* get_address_of_U24disposing_6() { return &___U24disposing_6; }
	inline void set_U24disposing_6(bool value)
	{
		___U24disposing_6 = value;
	}

	inline static int32_t get_offset_of_U24PC_7() { return static_cast<int32_t>(offsetof(U3CFadeAdditiveU3Ec__Iterator1_t1481999734, ___U24PC_7)); }
	inline int32_t get_U24PC_7() const { return ___U24PC_7; }
	inline int32_t* get_address_of_U24PC_7() { return &___U24PC_7; }
	inline void set_U24PC_7(int32_t value)
	{
		___U24PC_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEADDITIVEU3EC__ITERATOR1_T1481999734_H
#ifndef OBJECT_T350248726_H
#define OBJECT_T350248726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t350248726  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t350248726, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t350248726_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t350248726_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t350248726_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t350248726_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T350248726_H
#ifndef U3CFADEU3EC__ITERATOR0_T2035475871_H
#define U3CFADEU3EC__ITERATOR0_T2035475871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0
struct  U3CFadeU3Ec__Iterator0_t2035475871  : public RuntimeObject
{
public:
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::<t>__1
	int32_t ___U3CtU3E__1_0;
	// System.Boolean Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::<breakout>__2
	bool ___U3CbreakoutU3E__2_1;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::<c>__3
	Color32_t2411287715  ___U3CcU3E__3_2;
	// Spine.Unity.Modules.SkeletonGhostRenderer Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::$this
	SkeletonGhostRenderer_t108436892 * ___U24this_3;
	// System.Object Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Spine.Unity.Modules.SkeletonGhostRenderer/<Fade>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CtU3E__1_0() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t2035475871, ___U3CtU3E__1_0)); }
	inline int32_t get_U3CtU3E__1_0() const { return ___U3CtU3E__1_0; }
	inline int32_t* get_address_of_U3CtU3E__1_0() { return &___U3CtU3E__1_0; }
	inline void set_U3CtU3E__1_0(int32_t value)
	{
		___U3CtU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U3CbreakoutU3E__2_1() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t2035475871, ___U3CbreakoutU3E__2_1)); }
	inline bool get_U3CbreakoutU3E__2_1() const { return ___U3CbreakoutU3E__2_1; }
	inline bool* get_address_of_U3CbreakoutU3E__2_1() { return &___U3CbreakoutU3E__2_1; }
	inline void set_U3CbreakoutU3E__2_1(bool value)
	{
		___U3CbreakoutU3E__2_1 = value;
	}

	inline static int32_t get_offset_of_U3CcU3E__3_2() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t2035475871, ___U3CcU3E__3_2)); }
	inline Color32_t2411287715  get_U3CcU3E__3_2() const { return ___U3CcU3E__3_2; }
	inline Color32_t2411287715 * get_address_of_U3CcU3E__3_2() { return &___U3CcU3E__3_2; }
	inline void set_U3CcU3E__3_2(Color32_t2411287715  value)
	{
		___U3CcU3E__3_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t2035475871, ___U24this_3)); }
	inline SkeletonGhostRenderer_t108436892 * get_U24this_3() const { return ___U24this_3; }
	inline SkeletonGhostRenderer_t108436892 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SkeletonGhostRenderer_t108436892 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t2035475871, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t2035475871, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t2035475871, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFADEU3EC__ITERATOR0_T2035475871_H
#ifndef DELEGATE_T1310841479_H
#define DELEGATE_T1310841479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1310841479  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t878649875 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___data_8)); }
	inline DelegateData_t878649875 * get_data_8() const { return ___data_8; }
	inline DelegateData_t878649875 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t878649875 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1310841479_H
#ifndef POSITIONMODE_T2631516897_H
#define POSITIONMODE_T2631516897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PositionMode
struct  PositionMode_t2631516897 
{
public:
	// System.Int32 Spine.PositionMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PositionMode_t2631516897, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONMODE_T2631516897_H
#ifndef COMPONENT_T1852985663_H
#define COMPONENT_T1852985663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1852985663  : public Object_t350248726
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1852985663_H
#ifndef SPINESLOT_T3243618918_H
#define SPINESLOT_T3243618918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineSlot
struct  SpineSlot_t3243618918  : public SpineAttributeBase_t3125339574
{
public:
	// System.Boolean Spine.Unity.SpineSlot::containsBoundingBoxes
	bool ___containsBoundingBoxes_4;

public:
	inline static int32_t get_offset_of_containsBoundingBoxes_4() { return static_cast<int32_t>(offsetof(SpineSlot_t3243618918, ___containsBoundingBoxes_4)); }
	inline bool get_containsBoundingBoxes_4() const { return ___containsBoundingBoxes_4; }
	inline bool* get_address_of_containsBoundingBoxes_4() { return &___containsBoundingBoxes_4; }
	inline void set_containsBoundingBoxes_4(bool value)
	{
		___containsBoundingBoxes_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESLOT_T3243618918_H
#ifndef SPINEEVENT_T1674004351_H
#define SPINEEVENT_T1674004351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineEvent
struct  SpineEvent_t1674004351  : public SpineAttributeBase_t3125339574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEEVENT_T1674004351_H
#ifndef SPINEIKCONSTRAINT_T203333819_H
#define SPINEIKCONSTRAINT_T203333819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineIkConstraint
struct  SpineIkConstraint_t203333819  : public SpineAttributeBase_t3125339574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEIKCONSTRAINT_T203333819_H
#ifndef SPINEPATHCONSTRAINT_T1221980072_H
#define SPINEPATHCONSTRAINT_T1221980072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpinePathConstraint
struct  SpinePathConstraint_t1221980072  : public SpineAttributeBase_t3125339574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEPATHCONSTRAINT_T1221980072_H
#ifndef SPINESKIN_T1537368333_H
#define SPINESKIN_T1537368333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineSkin
struct  SpineSkin_t1537368333  : public SpineAttributeBase_t3125339574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESKIN_T1537368333_H
#ifndef SPINEANIMATION_T529128464_H
#define SPINEANIMATION_T529128464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAnimation
struct  SpineAnimation_t529128464  : public SpineAttributeBase_t3125339574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEANIMATION_T529128464_H
#ifndef SPINEATTACHMENT_T4141084984_H
#define SPINEATTACHMENT_T4141084984_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttachment
struct  SpineAttachment_t4141084984  : public SpineAttributeBase_t3125339574
{
public:
	// System.Boolean Spine.Unity.SpineAttachment::returnAttachmentPath
	bool ___returnAttachmentPath_4;
	// System.Boolean Spine.Unity.SpineAttachment::currentSkinOnly
	bool ___currentSkinOnly_5;
	// System.Boolean Spine.Unity.SpineAttachment::placeholdersOnly
	bool ___placeholdersOnly_6;
	// System.String Spine.Unity.SpineAttachment::skinField
	String_t* ___skinField_7;
	// System.String Spine.Unity.SpineAttachment::slotField
	String_t* ___slotField_8;

public:
	inline static int32_t get_offset_of_returnAttachmentPath_4() { return static_cast<int32_t>(offsetof(SpineAttachment_t4141084984, ___returnAttachmentPath_4)); }
	inline bool get_returnAttachmentPath_4() const { return ___returnAttachmentPath_4; }
	inline bool* get_address_of_returnAttachmentPath_4() { return &___returnAttachmentPath_4; }
	inline void set_returnAttachmentPath_4(bool value)
	{
		___returnAttachmentPath_4 = value;
	}

	inline static int32_t get_offset_of_currentSkinOnly_5() { return static_cast<int32_t>(offsetof(SpineAttachment_t4141084984, ___currentSkinOnly_5)); }
	inline bool get_currentSkinOnly_5() const { return ___currentSkinOnly_5; }
	inline bool* get_address_of_currentSkinOnly_5() { return &___currentSkinOnly_5; }
	inline void set_currentSkinOnly_5(bool value)
	{
		___currentSkinOnly_5 = value;
	}

	inline static int32_t get_offset_of_placeholdersOnly_6() { return static_cast<int32_t>(offsetof(SpineAttachment_t4141084984, ___placeholdersOnly_6)); }
	inline bool get_placeholdersOnly_6() const { return ___placeholdersOnly_6; }
	inline bool* get_address_of_placeholdersOnly_6() { return &___placeholdersOnly_6; }
	inline void set_placeholdersOnly_6(bool value)
	{
		___placeholdersOnly_6 = value;
	}

	inline static int32_t get_offset_of_skinField_7() { return static_cast<int32_t>(offsetof(SpineAttachment_t4141084984, ___skinField_7)); }
	inline String_t* get_skinField_7() const { return ___skinField_7; }
	inline String_t** get_address_of_skinField_7() { return &___skinField_7; }
	inline void set_skinField_7(String_t* value)
	{
		___skinField_7 = value;
		Il2CppCodeGenWriteBarrier((&___skinField_7), value);
	}

	inline static int32_t get_offset_of_slotField_8() { return static_cast<int32_t>(offsetof(SpineAttachment_t4141084984, ___slotField_8)); }
	inline String_t* get_slotField_8() const { return ___slotField_8; }
	inline String_t** get_address_of_slotField_8() { return &___slotField_8; }
	inline void set_slotField_8(String_t* value)
	{
		___slotField_8 = value;
		Il2CppCodeGenWriteBarrier((&___slotField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATTACHMENT_T4141084984_H
#ifndef SPINETRANSFORMCONSTRAINT_T2634477230_H
#define SPINETRANSFORMCONSTRAINT_T2634477230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineTransformConstraint
struct  SpineTransformConstraint_t2634477230  : public SpineAttributeBase_t3125339574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINETRANSFORMCONSTRAINT_T2634477230_H
#ifndef PATHCONSTRAINTDATA_T3185350348_H
#define PATHCONSTRAINTDATA_T3185350348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.PathConstraintData
struct  PathConstraintData_t3185350348  : public RuntimeObject
{
public:
	// System.String Spine.PathConstraintData::name
	String_t* ___name_0;
	// System.Int32 Spine.PathConstraintData::order
	int32_t ___order_1;
	// Spine.ExposedList`1<Spine.BoneData> Spine.PathConstraintData::bones
	ExposedList_1_t3044655291 * ___bones_2;
	// Spine.SlotData Spine.PathConstraintData::target
	SlotData_t587781850 * ___target_3;
	// Spine.PositionMode Spine.PathConstraintData::positionMode
	int32_t ___positionMode_4;
	// Spine.SpacingMode Spine.PathConstraintData::spacingMode
	int32_t ___spacingMode_5;
	// Spine.RotateMode Spine.PathConstraintData::rotateMode
	int32_t ___rotateMode_6;
	// System.Single Spine.PathConstraintData::offsetRotation
	float ___offsetRotation_7;
	// System.Single Spine.PathConstraintData::position
	float ___position_8;
	// System.Single Spine.PathConstraintData::spacing
	float ___spacing_9;
	// System.Single Spine.PathConstraintData::rotateMix
	float ___rotateMix_10;
	// System.Single Spine.PathConstraintData::translateMix
	float ___translateMix_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(PathConstraintData_t3185350348, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_order_1() { return static_cast<int32_t>(offsetof(PathConstraintData_t3185350348, ___order_1)); }
	inline int32_t get_order_1() const { return ___order_1; }
	inline int32_t* get_address_of_order_1() { return &___order_1; }
	inline void set_order_1(int32_t value)
	{
		___order_1 = value;
	}

	inline static int32_t get_offset_of_bones_2() { return static_cast<int32_t>(offsetof(PathConstraintData_t3185350348, ___bones_2)); }
	inline ExposedList_1_t3044655291 * get_bones_2() const { return ___bones_2; }
	inline ExposedList_1_t3044655291 ** get_address_of_bones_2() { return &___bones_2; }
	inline void set_bones_2(ExposedList_1_t3044655291 * value)
	{
		___bones_2 = value;
		Il2CppCodeGenWriteBarrier((&___bones_2), value);
	}

	inline static int32_t get_offset_of_target_3() { return static_cast<int32_t>(offsetof(PathConstraintData_t3185350348, ___target_3)); }
	inline SlotData_t587781850 * get_target_3() const { return ___target_3; }
	inline SlotData_t587781850 ** get_address_of_target_3() { return &___target_3; }
	inline void set_target_3(SlotData_t587781850 * value)
	{
		___target_3 = value;
		Il2CppCodeGenWriteBarrier((&___target_3), value);
	}

	inline static int32_t get_offset_of_positionMode_4() { return static_cast<int32_t>(offsetof(PathConstraintData_t3185350348, ___positionMode_4)); }
	inline int32_t get_positionMode_4() const { return ___positionMode_4; }
	inline int32_t* get_address_of_positionMode_4() { return &___positionMode_4; }
	inline void set_positionMode_4(int32_t value)
	{
		___positionMode_4 = value;
	}

	inline static int32_t get_offset_of_spacingMode_5() { return static_cast<int32_t>(offsetof(PathConstraintData_t3185350348, ___spacingMode_5)); }
	inline int32_t get_spacingMode_5() const { return ___spacingMode_5; }
	inline int32_t* get_address_of_spacingMode_5() { return &___spacingMode_5; }
	inline void set_spacingMode_5(int32_t value)
	{
		___spacingMode_5 = value;
	}

	inline static int32_t get_offset_of_rotateMode_6() { return static_cast<int32_t>(offsetof(PathConstraintData_t3185350348, ___rotateMode_6)); }
	inline int32_t get_rotateMode_6() const { return ___rotateMode_6; }
	inline int32_t* get_address_of_rotateMode_6() { return &___rotateMode_6; }
	inline void set_rotateMode_6(int32_t value)
	{
		___rotateMode_6 = value;
	}

	inline static int32_t get_offset_of_offsetRotation_7() { return static_cast<int32_t>(offsetof(PathConstraintData_t3185350348, ___offsetRotation_7)); }
	inline float get_offsetRotation_7() const { return ___offsetRotation_7; }
	inline float* get_address_of_offsetRotation_7() { return &___offsetRotation_7; }
	inline void set_offsetRotation_7(float value)
	{
		___offsetRotation_7 = value;
	}

	inline static int32_t get_offset_of_position_8() { return static_cast<int32_t>(offsetof(PathConstraintData_t3185350348, ___position_8)); }
	inline float get_position_8() const { return ___position_8; }
	inline float* get_address_of_position_8() { return &___position_8; }
	inline void set_position_8(float value)
	{
		___position_8 = value;
	}

	inline static int32_t get_offset_of_spacing_9() { return static_cast<int32_t>(offsetof(PathConstraintData_t3185350348, ___spacing_9)); }
	inline float get_spacing_9() const { return ___spacing_9; }
	inline float* get_address_of_spacing_9() { return &___spacing_9; }
	inline void set_spacing_9(float value)
	{
		___spacing_9 = value;
	}

	inline static int32_t get_offset_of_rotateMix_10() { return static_cast<int32_t>(offsetof(PathConstraintData_t3185350348, ___rotateMix_10)); }
	inline float get_rotateMix_10() const { return ___rotateMix_10; }
	inline float* get_address_of_rotateMix_10() { return &___rotateMix_10; }
	inline void set_rotateMix_10(float value)
	{
		___rotateMix_10 = value;
	}

	inline static int32_t get_offset_of_translateMix_11() { return static_cast<int32_t>(offsetof(PathConstraintData_t3185350348, ___translateMix_11)); }
	inline float get_translateMix_11() const { return ___translateMix_11; }
	inline float* get_address_of_translateMix_11() { return &___translateMix_11; }
	inline void set_translateMix_11(float value)
	{
		___translateMix_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PATHCONSTRAINTDATA_T3185350348_H
#ifndef SCRIPTABLEOBJECT_T229319923_H
#define SCRIPTABLEOBJECT_T229319923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t229319923  : public Object_t350248726
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t229319923_marshaled_pinvoke : public Object_t350248726_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t229319923_marshaled_com : public Object_t350248726_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T229319923_H
#ifndef SLOTDATA_T587781850_H
#define SLOTDATA_T587781850_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.SlotData
struct  SlotData_t587781850  : public RuntimeObject
{
public:
	// System.Int32 Spine.SlotData::index
	int32_t ___index_0;
	// System.String Spine.SlotData::name
	String_t* ___name_1;
	// Spine.BoneData Spine.SlotData::boneData
	BoneData_t1194659452 * ___boneData_2;
	// System.Single Spine.SlotData::r
	float ___r_3;
	// System.Single Spine.SlotData::g
	float ___g_4;
	// System.Single Spine.SlotData::b
	float ___b_5;
	// System.Single Spine.SlotData::a
	float ___a_6;
	// System.Single Spine.SlotData::r2
	float ___r2_7;
	// System.Single Spine.SlotData::g2
	float ___g2_8;
	// System.Single Spine.SlotData::b2
	float ___b2_9;
	// System.Boolean Spine.SlotData::hasSecondColor
	bool ___hasSecondColor_10;
	// System.String Spine.SlotData::attachmentName
	String_t* ___attachmentName_11;
	// Spine.BlendMode Spine.SlotData::blendMode
	int32_t ___blendMode_12;

public:
	inline static int32_t get_offset_of_index_0() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___index_0)); }
	inline int32_t get_index_0() const { return ___index_0; }
	inline int32_t* get_address_of_index_0() { return &___index_0; }
	inline void set_index_0(int32_t value)
	{
		___index_0 = value;
	}

	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___name_1)); }
	inline String_t* get_name_1() const { return ___name_1; }
	inline String_t** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(String_t* value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_boneData_2() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___boneData_2)); }
	inline BoneData_t1194659452 * get_boneData_2() const { return ___boneData_2; }
	inline BoneData_t1194659452 ** get_address_of_boneData_2() { return &___boneData_2; }
	inline void set_boneData_2(BoneData_t1194659452 * value)
	{
		___boneData_2 = value;
		Il2CppCodeGenWriteBarrier((&___boneData_2), value);
	}

	inline static int32_t get_offset_of_r_3() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___r_3)); }
	inline float get_r_3() const { return ___r_3; }
	inline float* get_address_of_r_3() { return &___r_3; }
	inline void set_r_3(float value)
	{
		___r_3 = value;
	}

	inline static int32_t get_offset_of_g_4() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___g_4)); }
	inline float get_g_4() const { return ___g_4; }
	inline float* get_address_of_g_4() { return &___g_4; }
	inline void set_g_4(float value)
	{
		___g_4 = value;
	}

	inline static int32_t get_offset_of_b_5() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___b_5)); }
	inline float get_b_5() const { return ___b_5; }
	inline float* get_address_of_b_5() { return &___b_5; }
	inline void set_b_5(float value)
	{
		___b_5 = value;
	}

	inline static int32_t get_offset_of_a_6() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___a_6)); }
	inline float get_a_6() const { return ___a_6; }
	inline float* get_address_of_a_6() { return &___a_6; }
	inline void set_a_6(float value)
	{
		___a_6 = value;
	}

	inline static int32_t get_offset_of_r2_7() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___r2_7)); }
	inline float get_r2_7() const { return ___r2_7; }
	inline float* get_address_of_r2_7() { return &___r2_7; }
	inline void set_r2_7(float value)
	{
		___r2_7 = value;
	}

	inline static int32_t get_offset_of_g2_8() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___g2_8)); }
	inline float get_g2_8() const { return ___g2_8; }
	inline float* get_address_of_g2_8() { return &___g2_8; }
	inline void set_g2_8(float value)
	{
		___g2_8 = value;
	}

	inline static int32_t get_offset_of_b2_9() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___b2_9)); }
	inline float get_b2_9() const { return ___b2_9; }
	inline float* get_address_of_b2_9() { return &___b2_9; }
	inline void set_b2_9(float value)
	{
		___b2_9 = value;
	}

	inline static int32_t get_offset_of_hasSecondColor_10() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___hasSecondColor_10)); }
	inline bool get_hasSecondColor_10() const { return ___hasSecondColor_10; }
	inline bool* get_address_of_hasSecondColor_10() { return &___hasSecondColor_10; }
	inline void set_hasSecondColor_10(bool value)
	{
		___hasSecondColor_10 = value;
	}

	inline static int32_t get_offset_of_attachmentName_11() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___attachmentName_11)); }
	inline String_t* get_attachmentName_11() const { return ___attachmentName_11; }
	inline String_t** get_address_of_attachmentName_11() { return &___attachmentName_11; }
	inline void set_attachmentName_11(String_t* value)
	{
		___attachmentName_11 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentName_11), value);
	}

	inline static int32_t get_offset_of_blendMode_12() { return static_cast<int32_t>(offsetof(SlotData_t587781850, ___blendMode_12)); }
	inline int32_t get_blendMode_12() const { return ___blendMode_12; }
	inline int32_t* get_address_of_blendMode_12() { return &___blendMode_12; }
	inline void set_blendMode_12(int32_t value)
	{
		___blendMode_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTDATA_T587781850_H
#ifndef SPINEMESH_T4270445991_H
#define SPINEMESH_T4270445991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineMesh
struct  SpineMesh_t4270445991  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEMESH_T4270445991_H
#ifndef ATLASUTILITIES_T3407187445_H
#define ATLASUTILITIES_T3407187445_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.AttachmentTools.AtlasUtilities
struct  AtlasUtilities_t3407187445  : public RuntimeObject
{
public:

public:
};

struct AtlasUtilities_t3407187445_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Spine.AtlasRegion,UnityEngine.Texture2D> Spine.Unity.Modules.AttachmentTools.AtlasUtilities::CachedRegionTextures
	Dictionary_2_t923493323 * ___CachedRegionTextures_4;
	// System.Collections.Generic.List`1<UnityEngine.Texture2D> Spine.Unity.Modules.AttachmentTools.AtlasUtilities::CachedRegionTexturesList
	List_1_t3554626186 * ___CachedRegionTexturesList_5;

public:
	inline static int32_t get_offset_of_CachedRegionTextures_4() { return static_cast<int32_t>(offsetof(AtlasUtilities_t3407187445_StaticFields, ___CachedRegionTextures_4)); }
	inline Dictionary_2_t923493323 * get_CachedRegionTextures_4() const { return ___CachedRegionTextures_4; }
	inline Dictionary_2_t923493323 ** get_address_of_CachedRegionTextures_4() { return &___CachedRegionTextures_4; }
	inline void set_CachedRegionTextures_4(Dictionary_2_t923493323 * value)
	{
		___CachedRegionTextures_4 = value;
		Il2CppCodeGenWriteBarrier((&___CachedRegionTextures_4), value);
	}

	inline static int32_t get_offset_of_CachedRegionTexturesList_5() { return static_cast<int32_t>(offsetof(AtlasUtilities_t3407187445_StaticFields, ___CachedRegionTexturesList_5)); }
	inline List_1_t3554626186 * get_CachedRegionTexturesList_5() const { return ___CachedRegionTexturesList_5; }
	inline List_1_t3554626186 ** get_address_of_CachedRegionTexturesList_5() { return &___CachedRegionTexturesList_5; }
	inline void set_CachedRegionTexturesList_5(List_1_t3554626186 * value)
	{
		___CachedRegionTexturesList_5 = value;
		Il2CppCodeGenWriteBarrier((&___CachedRegionTexturesList_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASUTILITIES_T3407187445_H
#ifndef MULTICASTDELEGATE_T4207739222_H
#define MULTICASTDELEGATE_T4207739222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t4207739222  : public Delegate_t1310841479
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t4207739222 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t4207739222 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4207739222, ___prev_9)); }
	inline MulticastDelegate_t4207739222 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t4207739222 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t4207739222 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4207739222, ___kpm_next_10)); }
	inline MulticastDelegate_t4207739222 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t4207739222 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t4207739222 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T4207739222_H
#ifndef INSTRUCTIONDELEGATE_T1951844748_H
#define INSTRUCTIONDELEGATE_T1951844748_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer/InstructionDelegate
struct  InstructionDelegate_t1951844748  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTRUCTIONDELEGATE_T1951844748_H
#ifndef SKELETONRENDERERDELEGATE_T878065760_H
#define SKELETONRENDERERDELEGATE_T878065760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate
struct  SkeletonRendererDelegate_t878065760  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERERDELEGATE_T878065760_H
#ifndef MESHGENERATORDELEGATE_T2067952305_H
#define MESHGENERATORDELEGATE_T2067952305_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.MeshGeneratorDelegate
struct  MeshGeneratorDelegate_t2067952305  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESHGENERATORDELEGATE_T2067952305_H
#ifndef BEHAVIOUR_T2905267502_H
#define BEHAVIOUR_T2905267502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2905267502  : public Component_t1852985663
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2905267502_H
#ifndef SKELETONDATAASSET_T1706375087_H
#define SKELETONDATAASSET_T1706375087_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonDataAsset
struct  SkeletonDataAsset_t1706375087  : public ScriptableObject_t229319923
{
public:
	// Spine.Unity.AtlasAsset[] Spine.Unity.SkeletonDataAsset::atlasAssets
	AtlasAssetU5BU5D_t635175767* ___atlasAssets_2;
	// System.Single Spine.Unity.SkeletonDataAsset::scale
	float ___scale_3;
	// UnityEngine.TextAsset Spine.Unity.SkeletonDataAsset::skeletonJSON
	TextAsset_t3707725843 * ___skeletonJSON_4;
	// System.String[] Spine.Unity.SkeletonDataAsset::fromAnimation
	StringU5BU5D_t656593794* ___fromAnimation_5;
	// System.String[] Spine.Unity.SkeletonDataAsset::toAnimation
	StringU5BU5D_t656593794* ___toAnimation_6;
	// System.Single[] Spine.Unity.SkeletonDataAsset::duration
	SingleU5BU5D_t3989243465* ___duration_7;
	// System.Single Spine.Unity.SkeletonDataAsset::defaultMix
	float ___defaultMix_8;
	// UnityEngine.RuntimeAnimatorController Spine.Unity.SkeletonDataAsset::controller
	RuntimeAnimatorController_t180203524 * ___controller_9;
	// Spine.SkeletonData Spine.Unity.SkeletonDataAsset::skeletonData
	SkeletonData_t2665604974 * ___skeletonData_10;
	// Spine.AnimationStateData Spine.Unity.SkeletonDataAsset::stateData
	AnimationStateData_t1644285503 * ___stateData_11;

public:
	inline static int32_t get_offset_of_atlasAssets_2() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t1706375087, ___atlasAssets_2)); }
	inline AtlasAssetU5BU5D_t635175767* get_atlasAssets_2() const { return ___atlasAssets_2; }
	inline AtlasAssetU5BU5D_t635175767** get_address_of_atlasAssets_2() { return &___atlasAssets_2; }
	inline void set_atlasAssets_2(AtlasAssetU5BU5D_t635175767* value)
	{
		___atlasAssets_2 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAssets_2), value);
	}

	inline static int32_t get_offset_of_scale_3() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t1706375087, ___scale_3)); }
	inline float get_scale_3() const { return ___scale_3; }
	inline float* get_address_of_scale_3() { return &___scale_3; }
	inline void set_scale_3(float value)
	{
		___scale_3 = value;
	}

	inline static int32_t get_offset_of_skeletonJSON_4() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t1706375087, ___skeletonJSON_4)); }
	inline TextAsset_t3707725843 * get_skeletonJSON_4() const { return ___skeletonJSON_4; }
	inline TextAsset_t3707725843 ** get_address_of_skeletonJSON_4() { return &___skeletonJSON_4; }
	inline void set_skeletonJSON_4(TextAsset_t3707725843 * value)
	{
		___skeletonJSON_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonJSON_4), value);
	}

	inline static int32_t get_offset_of_fromAnimation_5() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t1706375087, ___fromAnimation_5)); }
	inline StringU5BU5D_t656593794* get_fromAnimation_5() const { return ___fromAnimation_5; }
	inline StringU5BU5D_t656593794** get_address_of_fromAnimation_5() { return &___fromAnimation_5; }
	inline void set_fromAnimation_5(StringU5BU5D_t656593794* value)
	{
		___fromAnimation_5 = value;
		Il2CppCodeGenWriteBarrier((&___fromAnimation_5), value);
	}

	inline static int32_t get_offset_of_toAnimation_6() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t1706375087, ___toAnimation_6)); }
	inline StringU5BU5D_t656593794* get_toAnimation_6() const { return ___toAnimation_6; }
	inline StringU5BU5D_t656593794** get_address_of_toAnimation_6() { return &___toAnimation_6; }
	inline void set_toAnimation_6(StringU5BU5D_t656593794* value)
	{
		___toAnimation_6 = value;
		Il2CppCodeGenWriteBarrier((&___toAnimation_6), value);
	}

	inline static int32_t get_offset_of_duration_7() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t1706375087, ___duration_7)); }
	inline SingleU5BU5D_t3989243465* get_duration_7() const { return ___duration_7; }
	inline SingleU5BU5D_t3989243465** get_address_of_duration_7() { return &___duration_7; }
	inline void set_duration_7(SingleU5BU5D_t3989243465* value)
	{
		___duration_7 = value;
		Il2CppCodeGenWriteBarrier((&___duration_7), value);
	}

	inline static int32_t get_offset_of_defaultMix_8() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t1706375087, ___defaultMix_8)); }
	inline float get_defaultMix_8() const { return ___defaultMix_8; }
	inline float* get_address_of_defaultMix_8() { return &___defaultMix_8; }
	inline void set_defaultMix_8(float value)
	{
		___defaultMix_8 = value;
	}

	inline static int32_t get_offset_of_controller_9() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t1706375087, ___controller_9)); }
	inline RuntimeAnimatorController_t180203524 * get_controller_9() const { return ___controller_9; }
	inline RuntimeAnimatorController_t180203524 ** get_address_of_controller_9() { return &___controller_9; }
	inline void set_controller_9(RuntimeAnimatorController_t180203524 * value)
	{
		___controller_9 = value;
		Il2CppCodeGenWriteBarrier((&___controller_9), value);
	}

	inline static int32_t get_offset_of_skeletonData_10() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t1706375087, ___skeletonData_10)); }
	inline SkeletonData_t2665604974 * get_skeletonData_10() const { return ___skeletonData_10; }
	inline SkeletonData_t2665604974 ** get_address_of_skeletonData_10() { return &___skeletonData_10; }
	inline void set_skeletonData_10(SkeletonData_t2665604974 * value)
	{
		___skeletonData_10 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonData_10), value);
	}

	inline static int32_t get_offset_of_stateData_11() { return static_cast<int32_t>(offsetof(SkeletonDataAsset_t1706375087, ___stateData_11)); }
	inline AnimationStateData_t1644285503 * get_stateData_11() const { return ___stateData_11; }
	inline AnimationStateData_t1644285503 ** get_address_of_stateData_11() { return &___stateData_11; }
	inline void set_stateData_11(AnimationStateData_t1644285503 * value)
	{
		___stateData_11 = value;
		Il2CppCodeGenWriteBarrier((&___stateData_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONDATAASSET_T1706375087_H
#ifndef ATLASASSET_T2525633250_H
#define ATLASASSET_T2525633250_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.AtlasAsset
struct  AtlasAsset_t2525633250  : public ScriptableObject_t229319923
{
public:
	// UnityEngine.TextAsset Spine.Unity.AtlasAsset::atlasFile
	TextAsset_t3707725843 * ___atlasFile_2;
	// UnityEngine.Material[] Spine.Unity.AtlasAsset::materials
	MaterialU5BU5D_t2885843518* ___materials_3;
	// Spine.Atlas Spine.Unity.AtlasAsset::atlas
	Atlas_t604244430 * ___atlas_4;

public:
	inline static int32_t get_offset_of_atlasFile_2() { return static_cast<int32_t>(offsetof(AtlasAsset_t2525633250, ___atlasFile_2)); }
	inline TextAsset_t3707725843 * get_atlasFile_2() const { return ___atlasFile_2; }
	inline TextAsset_t3707725843 ** get_address_of_atlasFile_2() { return &___atlasFile_2; }
	inline void set_atlasFile_2(TextAsset_t3707725843 * value)
	{
		___atlasFile_2 = value;
		Il2CppCodeGenWriteBarrier((&___atlasFile_2), value);
	}

	inline static int32_t get_offset_of_materials_3() { return static_cast<int32_t>(offsetof(AtlasAsset_t2525633250, ___materials_3)); }
	inline MaterialU5BU5D_t2885843518* get_materials_3() const { return ___materials_3; }
	inline MaterialU5BU5D_t2885843518** get_address_of_materials_3() { return &___materials_3; }
	inline void set_materials_3(MaterialU5BU5D_t2885843518* value)
	{
		___materials_3 = value;
		Il2CppCodeGenWriteBarrier((&___materials_3), value);
	}

	inline static int32_t get_offset_of_atlas_4() { return static_cast<int32_t>(offsetof(AtlasAsset_t2525633250, ___atlas_4)); }
	inline Atlas_t604244430 * get_atlas_4() const { return ___atlas_4; }
	inline Atlas_t604244430 ** get_address_of_atlas_4() { return &___atlas_4; }
	inline void set_atlas_4(Atlas_t604244430 * value)
	{
		___atlas_4 = value;
		Il2CppCodeGenWriteBarrier((&___atlas_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATLASASSET_T2525633250_H
#ifndef SKELETONUTILITYDELEGATE_T1611778497_H
#define SKELETONUTILITYDELEGATE_T1611778497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate
struct  SkeletonUtilityDelegate_t1611778497  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYDELEGATE_T1611778497_H
#ifndef UPDATEBONESDELEGATE_T500272946_H
#define UPDATEBONESDELEGATE_T500272946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.UpdateBonesDelegate
struct  UpdateBonesDelegate_t500272946  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEBONESDELEGATE_T500272946_H
#ifndef MONOBEHAVIOUR_T3755042618_H
#define MONOBEHAVIOUR_T3755042618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3755042618  : public Behaviour_t2905267502
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3755042618_H
#ifndef BONEFOLLOWER_T3311442535_H
#define BONEFOLLOWER_T3311442535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.BoneFollower
struct  BoneFollower_t3311442535  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.BoneFollower::skeletonRenderer
	SkeletonRenderer_t1681389628 * ___skeletonRenderer_2;
	// System.String Spine.Unity.BoneFollower::boneName
	String_t* ___boneName_3;
	// System.Boolean Spine.Unity.BoneFollower::followZPosition
	bool ___followZPosition_4;
	// System.Boolean Spine.Unity.BoneFollower::followBoneRotation
	bool ___followBoneRotation_5;
	// System.Boolean Spine.Unity.BoneFollower::followSkeletonFlip
	bool ___followSkeletonFlip_6;
	// System.Boolean Spine.Unity.BoneFollower::followLocalScale
	bool ___followLocalScale_7;
	// System.Boolean Spine.Unity.BoneFollower::initializeOnAwake
	bool ___initializeOnAwake_8;
	// System.Boolean Spine.Unity.BoneFollower::valid
	bool ___valid_9;
	// Spine.Bone Spine.Unity.BoneFollower::bone
	Bone_t1763862836 * ___bone_10;
	// UnityEngine.Transform Spine.Unity.BoneFollower::skeletonTransform
	Transform_t2735953680 * ___skeletonTransform_11;
	// System.Boolean Spine.Unity.BoneFollower::skeletonTransformIsParent
	bool ___skeletonTransformIsParent_12;

public:
	inline static int32_t get_offset_of_skeletonRenderer_2() { return static_cast<int32_t>(offsetof(BoneFollower_t3311442535, ___skeletonRenderer_2)); }
	inline SkeletonRenderer_t1681389628 * get_skeletonRenderer_2() const { return ___skeletonRenderer_2; }
	inline SkeletonRenderer_t1681389628 ** get_address_of_skeletonRenderer_2() { return &___skeletonRenderer_2; }
	inline void set_skeletonRenderer_2(SkeletonRenderer_t1681389628 * value)
	{
		___skeletonRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_2), value);
	}

	inline static int32_t get_offset_of_boneName_3() { return static_cast<int32_t>(offsetof(BoneFollower_t3311442535, ___boneName_3)); }
	inline String_t* get_boneName_3() const { return ___boneName_3; }
	inline String_t** get_address_of_boneName_3() { return &___boneName_3; }
	inline void set_boneName_3(String_t* value)
	{
		___boneName_3 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_3), value);
	}

	inline static int32_t get_offset_of_followZPosition_4() { return static_cast<int32_t>(offsetof(BoneFollower_t3311442535, ___followZPosition_4)); }
	inline bool get_followZPosition_4() const { return ___followZPosition_4; }
	inline bool* get_address_of_followZPosition_4() { return &___followZPosition_4; }
	inline void set_followZPosition_4(bool value)
	{
		___followZPosition_4 = value;
	}

	inline static int32_t get_offset_of_followBoneRotation_5() { return static_cast<int32_t>(offsetof(BoneFollower_t3311442535, ___followBoneRotation_5)); }
	inline bool get_followBoneRotation_5() const { return ___followBoneRotation_5; }
	inline bool* get_address_of_followBoneRotation_5() { return &___followBoneRotation_5; }
	inline void set_followBoneRotation_5(bool value)
	{
		___followBoneRotation_5 = value;
	}

	inline static int32_t get_offset_of_followSkeletonFlip_6() { return static_cast<int32_t>(offsetof(BoneFollower_t3311442535, ___followSkeletonFlip_6)); }
	inline bool get_followSkeletonFlip_6() const { return ___followSkeletonFlip_6; }
	inline bool* get_address_of_followSkeletonFlip_6() { return &___followSkeletonFlip_6; }
	inline void set_followSkeletonFlip_6(bool value)
	{
		___followSkeletonFlip_6 = value;
	}

	inline static int32_t get_offset_of_followLocalScale_7() { return static_cast<int32_t>(offsetof(BoneFollower_t3311442535, ___followLocalScale_7)); }
	inline bool get_followLocalScale_7() const { return ___followLocalScale_7; }
	inline bool* get_address_of_followLocalScale_7() { return &___followLocalScale_7; }
	inline void set_followLocalScale_7(bool value)
	{
		___followLocalScale_7 = value;
	}

	inline static int32_t get_offset_of_initializeOnAwake_8() { return static_cast<int32_t>(offsetof(BoneFollower_t3311442535, ___initializeOnAwake_8)); }
	inline bool get_initializeOnAwake_8() const { return ___initializeOnAwake_8; }
	inline bool* get_address_of_initializeOnAwake_8() { return &___initializeOnAwake_8; }
	inline void set_initializeOnAwake_8(bool value)
	{
		___initializeOnAwake_8 = value;
	}

	inline static int32_t get_offset_of_valid_9() { return static_cast<int32_t>(offsetof(BoneFollower_t3311442535, ___valid_9)); }
	inline bool get_valid_9() const { return ___valid_9; }
	inline bool* get_address_of_valid_9() { return &___valid_9; }
	inline void set_valid_9(bool value)
	{
		___valid_9 = value;
	}

	inline static int32_t get_offset_of_bone_10() { return static_cast<int32_t>(offsetof(BoneFollower_t3311442535, ___bone_10)); }
	inline Bone_t1763862836 * get_bone_10() const { return ___bone_10; }
	inline Bone_t1763862836 ** get_address_of_bone_10() { return &___bone_10; }
	inline void set_bone_10(Bone_t1763862836 * value)
	{
		___bone_10 = value;
		Il2CppCodeGenWriteBarrier((&___bone_10), value);
	}

	inline static int32_t get_offset_of_skeletonTransform_11() { return static_cast<int32_t>(offsetof(BoneFollower_t3311442535, ___skeletonTransform_11)); }
	inline Transform_t2735953680 * get_skeletonTransform_11() const { return ___skeletonTransform_11; }
	inline Transform_t2735953680 ** get_address_of_skeletonTransform_11() { return &___skeletonTransform_11; }
	inline void set_skeletonTransform_11(Transform_t2735953680 * value)
	{
		___skeletonTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_11), value);
	}

	inline static int32_t get_offset_of_skeletonTransformIsParent_12() { return static_cast<int32_t>(offsetof(BoneFollower_t3311442535, ___skeletonTransformIsParent_12)); }
	inline bool get_skeletonTransformIsParent_12() const { return ___skeletonTransformIsParent_12; }
	inline bool* get_address_of_skeletonTransformIsParent_12() { return &___skeletonTransformIsParent_12; }
	inline void set_skeletonTransformIsParent_12(bool value)
	{
		___skeletonTransformIsParent_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEFOLLOWER_T3311442535_H
#ifndef SKELETONRAGDOLL_T1253535559_H
#define SKELETONRAGDOLL_T1253535559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll
struct  SkeletonRagdoll_t1253535559  : public MonoBehaviour_t3755042618
{
public:
	// System.String Spine.Unity.Modules.SkeletonRagdoll::startingBoneName
	String_t* ___startingBoneName_3;
	// System.Collections.Generic.List`1<System.String> Spine.Unity.Modules.SkeletonRagdoll::stopBoneNames
	List_1_t3668794496 * ___stopBoneNames_4;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::applyOnStart
	bool ___applyOnStart_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::disableIK
	bool ___disableIK_6;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::disableOtherConstraints
	bool ___disableOtherConstraints_7;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::pinStartBone
	bool ___pinStartBone_8;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::enableJointCollision
	bool ___enableJointCollision_9;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::useGravity
	bool ___useGravity_10;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::thickness
	float ___thickness_11;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::rotationLimit
	float ___rotationLimit_12;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::rootMass
	float ___rootMass_13;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::massFalloffFactor
	float ___massFalloffFactor_14;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll::colliderLayer
	int32_t ___colliderLayer_15;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll::mix
	float ___mix_16;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.Modules.SkeletonRagdoll::targetSkeletonComponent
	RuntimeObject* ___targetSkeletonComponent_17;
	// Spine.Skeleton Spine.Unity.Modules.SkeletonRagdoll::skeleton
	Skeleton_t1045203634 * ___skeleton_18;
	// System.Collections.Generic.Dictionary`2<Spine.Bone,UnityEngine.Transform> Spine.Unity.Modules.SkeletonRagdoll::boneTable
	Dictionary_2_t27791519 * ___boneTable_19;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll::ragdollRoot
	Transform_t2735953680 * ___ragdollRoot_20;
	// UnityEngine.Rigidbody Spine.Unity.Modules.SkeletonRagdoll::<RootRigidbody>k__BackingField
	Rigidbody_t2418109227 * ___U3CRootRigidbodyU3Ek__BackingField_21;
	// Spine.Bone Spine.Unity.Modules.SkeletonRagdoll::<StartingBone>k__BackingField
	Bone_t1763862836 * ___U3CStartingBoneU3Ek__BackingField_22;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonRagdoll::rootOffset
	Vector3_t2825674791  ___rootOffset_23;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll::isActive
	bool ___isActive_24;

public:
	inline static int32_t get_offset_of_startingBoneName_3() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___startingBoneName_3)); }
	inline String_t* get_startingBoneName_3() const { return ___startingBoneName_3; }
	inline String_t** get_address_of_startingBoneName_3() { return &___startingBoneName_3; }
	inline void set_startingBoneName_3(String_t* value)
	{
		___startingBoneName_3 = value;
		Il2CppCodeGenWriteBarrier((&___startingBoneName_3), value);
	}

	inline static int32_t get_offset_of_stopBoneNames_4() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___stopBoneNames_4)); }
	inline List_1_t3668794496 * get_stopBoneNames_4() const { return ___stopBoneNames_4; }
	inline List_1_t3668794496 ** get_address_of_stopBoneNames_4() { return &___stopBoneNames_4; }
	inline void set_stopBoneNames_4(List_1_t3668794496 * value)
	{
		___stopBoneNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___stopBoneNames_4), value);
	}

	inline static int32_t get_offset_of_applyOnStart_5() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___applyOnStart_5)); }
	inline bool get_applyOnStart_5() const { return ___applyOnStart_5; }
	inline bool* get_address_of_applyOnStart_5() { return &___applyOnStart_5; }
	inline void set_applyOnStart_5(bool value)
	{
		___applyOnStart_5 = value;
	}

	inline static int32_t get_offset_of_disableIK_6() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___disableIK_6)); }
	inline bool get_disableIK_6() const { return ___disableIK_6; }
	inline bool* get_address_of_disableIK_6() { return &___disableIK_6; }
	inline void set_disableIK_6(bool value)
	{
		___disableIK_6 = value;
	}

	inline static int32_t get_offset_of_disableOtherConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___disableOtherConstraints_7)); }
	inline bool get_disableOtherConstraints_7() const { return ___disableOtherConstraints_7; }
	inline bool* get_address_of_disableOtherConstraints_7() { return &___disableOtherConstraints_7; }
	inline void set_disableOtherConstraints_7(bool value)
	{
		___disableOtherConstraints_7 = value;
	}

	inline static int32_t get_offset_of_pinStartBone_8() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___pinStartBone_8)); }
	inline bool get_pinStartBone_8() const { return ___pinStartBone_8; }
	inline bool* get_address_of_pinStartBone_8() { return &___pinStartBone_8; }
	inline void set_pinStartBone_8(bool value)
	{
		___pinStartBone_8 = value;
	}

	inline static int32_t get_offset_of_enableJointCollision_9() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___enableJointCollision_9)); }
	inline bool get_enableJointCollision_9() const { return ___enableJointCollision_9; }
	inline bool* get_address_of_enableJointCollision_9() { return &___enableJointCollision_9; }
	inline void set_enableJointCollision_9(bool value)
	{
		___enableJointCollision_9 = value;
	}

	inline static int32_t get_offset_of_useGravity_10() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___useGravity_10)); }
	inline bool get_useGravity_10() const { return ___useGravity_10; }
	inline bool* get_address_of_useGravity_10() { return &___useGravity_10; }
	inline void set_useGravity_10(bool value)
	{
		___useGravity_10 = value;
	}

	inline static int32_t get_offset_of_thickness_11() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___thickness_11)); }
	inline float get_thickness_11() const { return ___thickness_11; }
	inline float* get_address_of_thickness_11() { return &___thickness_11; }
	inline void set_thickness_11(float value)
	{
		___thickness_11 = value;
	}

	inline static int32_t get_offset_of_rotationLimit_12() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___rotationLimit_12)); }
	inline float get_rotationLimit_12() const { return ___rotationLimit_12; }
	inline float* get_address_of_rotationLimit_12() { return &___rotationLimit_12; }
	inline void set_rotationLimit_12(float value)
	{
		___rotationLimit_12 = value;
	}

	inline static int32_t get_offset_of_rootMass_13() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___rootMass_13)); }
	inline float get_rootMass_13() const { return ___rootMass_13; }
	inline float* get_address_of_rootMass_13() { return &___rootMass_13; }
	inline void set_rootMass_13(float value)
	{
		___rootMass_13 = value;
	}

	inline static int32_t get_offset_of_massFalloffFactor_14() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___massFalloffFactor_14)); }
	inline float get_massFalloffFactor_14() const { return ___massFalloffFactor_14; }
	inline float* get_address_of_massFalloffFactor_14() { return &___massFalloffFactor_14; }
	inline void set_massFalloffFactor_14(float value)
	{
		___massFalloffFactor_14 = value;
	}

	inline static int32_t get_offset_of_colliderLayer_15() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___colliderLayer_15)); }
	inline int32_t get_colliderLayer_15() const { return ___colliderLayer_15; }
	inline int32_t* get_address_of_colliderLayer_15() { return &___colliderLayer_15; }
	inline void set_colliderLayer_15(int32_t value)
	{
		___colliderLayer_15 = value;
	}

	inline static int32_t get_offset_of_mix_16() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___mix_16)); }
	inline float get_mix_16() const { return ___mix_16; }
	inline float* get_address_of_mix_16() { return &___mix_16; }
	inline void set_mix_16(float value)
	{
		___mix_16 = value;
	}

	inline static int32_t get_offset_of_targetSkeletonComponent_17() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___targetSkeletonComponent_17)); }
	inline RuntimeObject* get_targetSkeletonComponent_17() const { return ___targetSkeletonComponent_17; }
	inline RuntimeObject** get_address_of_targetSkeletonComponent_17() { return &___targetSkeletonComponent_17; }
	inline void set_targetSkeletonComponent_17(RuntimeObject* value)
	{
		___targetSkeletonComponent_17 = value;
		Il2CppCodeGenWriteBarrier((&___targetSkeletonComponent_17), value);
	}

	inline static int32_t get_offset_of_skeleton_18() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___skeleton_18)); }
	inline Skeleton_t1045203634 * get_skeleton_18() const { return ___skeleton_18; }
	inline Skeleton_t1045203634 ** get_address_of_skeleton_18() { return &___skeleton_18; }
	inline void set_skeleton_18(Skeleton_t1045203634 * value)
	{
		___skeleton_18 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_18), value);
	}

	inline static int32_t get_offset_of_boneTable_19() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___boneTable_19)); }
	inline Dictionary_2_t27791519 * get_boneTable_19() const { return ___boneTable_19; }
	inline Dictionary_2_t27791519 ** get_address_of_boneTable_19() { return &___boneTable_19; }
	inline void set_boneTable_19(Dictionary_2_t27791519 * value)
	{
		___boneTable_19 = value;
		Il2CppCodeGenWriteBarrier((&___boneTable_19), value);
	}

	inline static int32_t get_offset_of_ragdollRoot_20() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___ragdollRoot_20)); }
	inline Transform_t2735953680 * get_ragdollRoot_20() const { return ___ragdollRoot_20; }
	inline Transform_t2735953680 ** get_address_of_ragdollRoot_20() { return &___ragdollRoot_20; }
	inline void set_ragdollRoot_20(Transform_t2735953680 * value)
	{
		___ragdollRoot_20 = value;
		Il2CppCodeGenWriteBarrier((&___ragdollRoot_20), value);
	}

	inline static int32_t get_offset_of_U3CRootRigidbodyU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___U3CRootRigidbodyU3Ek__BackingField_21)); }
	inline Rigidbody_t2418109227 * get_U3CRootRigidbodyU3Ek__BackingField_21() const { return ___U3CRootRigidbodyU3Ek__BackingField_21; }
	inline Rigidbody_t2418109227 ** get_address_of_U3CRootRigidbodyU3Ek__BackingField_21() { return &___U3CRootRigidbodyU3Ek__BackingField_21; }
	inline void set_U3CRootRigidbodyU3Ek__BackingField_21(Rigidbody_t2418109227 * value)
	{
		___U3CRootRigidbodyU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootRigidbodyU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_U3CStartingBoneU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___U3CStartingBoneU3Ek__BackingField_22)); }
	inline Bone_t1763862836 * get_U3CStartingBoneU3Ek__BackingField_22() const { return ___U3CStartingBoneU3Ek__BackingField_22; }
	inline Bone_t1763862836 ** get_address_of_U3CStartingBoneU3Ek__BackingField_22() { return &___U3CStartingBoneU3Ek__BackingField_22; }
	inline void set_U3CStartingBoneU3Ek__BackingField_22(Bone_t1763862836 * value)
	{
		___U3CStartingBoneU3Ek__BackingField_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStartingBoneU3Ek__BackingField_22), value);
	}

	inline static int32_t get_offset_of_rootOffset_23() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___rootOffset_23)); }
	inline Vector3_t2825674791  get_rootOffset_23() const { return ___rootOffset_23; }
	inline Vector3_t2825674791 * get_address_of_rootOffset_23() { return &___rootOffset_23; }
	inline void set_rootOffset_23(Vector3_t2825674791  value)
	{
		___rootOffset_23 = value;
	}

	inline static int32_t get_offset_of_isActive_24() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559, ___isActive_24)); }
	inline bool get_isActive_24() const { return ___isActive_24; }
	inline bool* get_address_of_isActive_24() { return &___isActive_24; }
	inline void set_isActive_24(bool value)
	{
		___isActive_24 = value;
	}
};

struct SkeletonRagdoll_t1253535559_StaticFields
{
public:
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll::parentSpaceHelper
	Transform_t2735953680 * ___parentSpaceHelper_2;

public:
	inline static int32_t get_offset_of_parentSpaceHelper_2() { return static_cast<int32_t>(offsetof(SkeletonRagdoll_t1253535559_StaticFields, ___parentSpaceHelper_2)); }
	inline Transform_t2735953680 * get_parentSpaceHelper_2() const { return ___parentSpaceHelper_2; }
	inline Transform_t2735953680 ** get_address_of_parentSpaceHelper_2() { return &___parentSpaceHelper_2; }
	inline void set_parentSpaceHelper_2(Transform_t2735953680 * value)
	{
		___parentSpaceHelper_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentSpaceHelper_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRAGDOLL_T1253535559_H
#ifndef SKELETONUTILITYBONE_T1490890952_H
#define SKELETONUTILITYBONE_T1490890952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityBone
struct  SkeletonUtilityBone_t1490890952  : public MonoBehaviour_t3755042618
{
public:
	// System.String Spine.Unity.SkeletonUtilityBone::boneName
	String_t* ___boneName_2;
	// UnityEngine.Transform Spine.Unity.SkeletonUtilityBone::parentReference
	Transform_t2735953680 * ___parentReference_3;
	// Spine.Unity.SkeletonUtilityBone/Mode Spine.Unity.SkeletonUtilityBone::mode
	int32_t ___mode_4;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::position
	bool ___position_5;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::rotation
	bool ___rotation_6;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::scale
	bool ___scale_7;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::zPosition
	bool ___zPosition_8;
	// System.Single Spine.Unity.SkeletonUtilityBone::overrideAlpha
	float ___overrideAlpha_9;
	// Spine.Unity.SkeletonUtility Spine.Unity.SkeletonUtilityBone::skeletonUtility
	SkeletonUtility_t373332496 * ___skeletonUtility_10;
	// Spine.Bone Spine.Unity.SkeletonUtilityBone::bone
	Bone_t1763862836 * ___bone_11;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::transformLerpComplete
	bool ___transformLerpComplete_12;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::valid
	bool ___valid_13;
	// UnityEngine.Transform Spine.Unity.SkeletonUtilityBone::cachedTransform
	Transform_t2735953680 * ___cachedTransform_14;
	// UnityEngine.Transform Spine.Unity.SkeletonUtilityBone::skeletonTransform
	Transform_t2735953680 * ___skeletonTransform_15;
	// System.Boolean Spine.Unity.SkeletonUtilityBone::incompatibleTransformMode
	bool ___incompatibleTransformMode_16;

public:
	inline static int32_t get_offset_of_boneName_2() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___boneName_2)); }
	inline String_t* get_boneName_2() const { return ___boneName_2; }
	inline String_t** get_address_of_boneName_2() { return &___boneName_2; }
	inline void set_boneName_2(String_t* value)
	{
		___boneName_2 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_2), value);
	}

	inline static int32_t get_offset_of_parentReference_3() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___parentReference_3)); }
	inline Transform_t2735953680 * get_parentReference_3() const { return ___parentReference_3; }
	inline Transform_t2735953680 ** get_address_of_parentReference_3() { return &___parentReference_3; }
	inline void set_parentReference_3(Transform_t2735953680 * value)
	{
		___parentReference_3 = value;
		Il2CppCodeGenWriteBarrier((&___parentReference_3), value);
	}

	inline static int32_t get_offset_of_mode_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___mode_4)); }
	inline int32_t get_mode_4() const { return ___mode_4; }
	inline int32_t* get_address_of_mode_4() { return &___mode_4; }
	inline void set_mode_4(int32_t value)
	{
		___mode_4 = value;
	}

	inline static int32_t get_offset_of_position_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___position_5)); }
	inline bool get_position_5() const { return ___position_5; }
	inline bool* get_address_of_position_5() { return &___position_5; }
	inline void set_position_5(bool value)
	{
		___position_5 = value;
	}

	inline static int32_t get_offset_of_rotation_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___rotation_6)); }
	inline bool get_rotation_6() const { return ___rotation_6; }
	inline bool* get_address_of_rotation_6() { return &___rotation_6; }
	inline void set_rotation_6(bool value)
	{
		___rotation_6 = value;
	}

	inline static int32_t get_offset_of_scale_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___scale_7)); }
	inline bool get_scale_7() const { return ___scale_7; }
	inline bool* get_address_of_scale_7() { return &___scale_7; }
	inline void set_scale_7(bool value)
	{
		___scale_7 = value;
	}

	inline static int32_t get_offset_of_zPosition_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___zPosition_8)); }
	inline bool get_zPosition_8() const { return ___zPosition_8; }
	inline bool* get_address_of_zPosition_8() { return &___zPosition_8; }
	inline void set_zPosition_8(bool value)
	{
		___zPosition_8 = value;
	}

	inline static int32_t get_offset_of_overrideAlpha_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___overrideAlpha_9)); }
	inline float get_overrideAlpha_9() const { return ___overrideAlpha_9; }
	inline float* get_address_of_overrideAlpha_9() { return &___overrideAlpha_9; }
	inline void set_overrideAlpha_9(float value)
	{
		___overrideAlpha_9 = value;
	}

	inline static int32_t get_offset_of_skeletonUtility_10() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___skeletonUtility_10)); }
	inline SkeletonUtility_t373332496 * get_skeletonUtility_10() const { return ___skeletonUtility_10; }
	inline SkeletonUtility_t373332496 ** get_address_of_skeletonUtility_10() { return &___skeletonUtility_10; }
	inline void set_skeletonUtility_10(SkeletonUtility_t373332496 * value)
	{
		___skeletonUtility_10 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonUtility_10), value);
	}

	inline static int32_t get_offset_of_bone_11() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___bone_11)); }
	inline Bone_t1763862836 * get_bone_11() const { return ___bone_11; }
	inline Bone_t1763862836 ** get_address_of_bone_11() { return &___bone_11; }
	inline void set_bone_11(Bone_t1763862836 * value)
	{
		___bone_11 = value;
		Il2CppCodeGenWriteBarrier((&___bone_11), value);
	}

	inline static int32_t get_offset_of_transformLerpComplete_12() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___transformLerpComplete_12)); }
	inline bool get_transformLerpComplete_12() const { return ___transformLerpComplete_12; }
	inline bool* get_address_of_transformLerpComplete_12() { return &___transformLerpComplete_12; }
	inline void set_transformLerpComplete_12(bool value)
	{
		___transformLerpComplete_12 = value;
	}

	inline static int32_t get_offset_of_valid_13() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___valid_13)); }
	inline bool get_valid_13() const { return ___valid_13; }
	inline bool* get_address_of_valid_13() { return &___valid_13; }
	inline void set_valid_13(bool value)
	{
		___valid_13 = value;
	}

	inline static int32_t get_offset_of_cachedTransform_14() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___cachedTransform_14)); }
	inline Transform_t2735953680 * get_cachedTransform_14() const { return ___cachedTransform_14; }
	inline Transform_t2735953680 ** get_address_of_cachedTransform_14() { return &___cachedTransform_14; }
	inline void set_cachedTransform_14(Transform_t2735953680 * value)
	{
		___cachedTransform_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedTransform_14), value);
	}

	inline static int32_t get_offset_of_skeletonTransform_15() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___skeletonTransform_15)); }
	inline Transform_t2735953680 * get_skeletonTransform_15() const { return ___skeletonTransform_15; }
	inline Transform_t2735953680 ** get_address_of_skeletonTransform_15() { return &___skeletonTransform_15; }
	inline void set_skeletonTransform_15(Transform_t2735953680 * value)
	{
		___skeletonTransform_15 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_15), value);
	}

	inline static int32_t get_offset_of_incompatibleTransformMode_16() { return static_cast<int32_t>(offsetof(SkeletonUtilityBone_t1490890952, ___incompatibleTransformMode_16)); }
	inline bool get_incompatibleTransformMode_16() const { return ___incompatibleTransformMode_16; }
	inline bool* get_address_of_incompatibleTransformMode_16() { return &___incompatibleTransformMode_16; }
	inline void set_incompatibleTransformMode_16(bool value)
	{
		___incompatibleTransformMode_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYBONE_T1490890952_H
#ifndef SKELETONRAGDOLL2D_T3940222091_H
#define SKELETONRAGDOLL2D_T3940222091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRagdoll2D
struct  SkeletonRagdoll2D_t3940222091  : public MonoBehaviour_t3755042618
{
public:
	// System.String Spine.Unity.Modules.SkeletonRagdoll2D::startingBoneName
	String_t* ___startingBoneName_3;
	// System.Collections.Generic.List`1<System.String> Spine.Unity.Modules.SkeletonRagdoll2D::stopBoneNames
	List_1_t3668794496 * ___stopBoneNames_4;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::applyOnStart
	bool ___applyOnStart_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::disableIK
	bool ___disableIK_6;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::disableOtherConstraints
	bool ___disableOtherConstraints_7;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::pinStartBone
	bool ___pinStartBone_8;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::gravityScale
	float ___gravityScale_9;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::thickness
	float ___thickness_10;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::rotationLimit
	float ___rotationLimit_11;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::rootMass
	float ___rootMass_12;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::massFalloffFactor
	float ___massFalloffFactor_13;
	// System.Int32 Spine.Unity.Modules.SkeletonRagdoll2D::colliderLayer
	int32_t ___colliderLayer_14;
	// System.Single Spine.Unity.Modules.SkeletonRagdoll2D::mix
	float ___mix_15;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.Modules.SkeletonRagdoll2D::targetSkeletonComponent
	RuntimeObject* ___targetSkeletonComponent_16;
	// Spine.Skeleton Spine.Unity.Modules.SkeletonRagdoll2D::skeleton
	Skeleton_t1045203634 * ___skeleton_17;
	// System.Collections.Generic.Dictionary`2<Spine.Bone,UnityEngine.Transform> Spine.Unity.Modules.SkeletonRagdoll2D::boneTable
	Dictionary_2_t27791519 * ___boneTable_18;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll2D::ragdollRoot
	Transform_t2735953680 * ___ragdollRoot_19;
	// UnityEngine.Rigidbody2D Spine.Unity.Modules.SkeletonRagdoll2D::<RootRigidbody>k__BackingField
	Rigidbody2D_t4047763079 * ___U3CRootRigidbodyU3Ek__BackingField_20;
	// Spine.Bone Spine.Unity.Modules.SkeletonRagdoll2D::<StartingBone>k__BackingField
	Bone_t1763862836 * ___U3CStartingBoneU3Ek__BackingField_21;
	// UnityEngine.Vector2 Spine.Unity.Modules.SkeletonRagdoll2D::rootOffset
	Vector2_t1065050092  ___rootOffset_22;
	// System.Boolean Spine.Unity.Modules.SkeletonRagdoll2D::isActive
	bool ___isActive_23;

public:
	inline static int32_t get_offset_of_startingBoneName_3() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___startingBoneName_3)); }
	inline String_t* get_startingBoneName_3() const { return ___startingBoneName_3; }
	inline String_t** get_address_of_startingBoneName_3() { return &___startingBoneName_3; }
	inline void set_startingBoneName_3(String_t* value)
	{
		___startingBoneName_3 = value;
		Il2CppCodeGenWriteBarrier((&___startingBoneName_3), value);
	}

	inline static int32_t get_offset_of_stopBoneNames_4() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___stopBoneNames_4)); }
	inline List_1_t3668794496 * get_stopBoneNames_4() const { return ___stopBoneNames_4; }
	inline List_1_t3668794496 ** get_address_of_stopBoneNames_4() { return &___stopBoneNames_4; }
	inline void set_stopBoneNames_4(List_1_t3668794496 * value)
	{
		___stopBoneNames_4 = value;
		Il2CppCodeGenWriteBarrier((&___stopBoneNames_4), value);
	}

	inline static int32_t get_offset_of_applyOnStart_5() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___applyOnStart_5)); }
	inline bool get_applyOnStart_5() const { return ___applyOnStart_5; }
	inline bool* get_address_of_applyOnStart_5() { return &___applyOnStart_5; }
	inline void set_applyOnStart_5(bool value)
	{
		___applyOnStart_5 = value;
	}

	inline static int32_t get_offset_of_disableIK_6() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___disableIK_6)); }
	inline bool get_disableIK_6() const { return ___disableIK_6; }
	inline bool* get_address_of_disableIK_6() { return &___disableIK_6; }
	inline void set_disableIK_6(bool value)
	{
		___disableIK_6 = value;
	}

	inline static int32_t get_offset_of_disableOtherConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___disableOtherConstraints_7)); }
	inline bool get_disableOtherConstraints_7() const { return ___disableOtherConstraints_7; }
	inline bool* get_address_of_disableOtherConstraints_7() { return &___disableOtherConstraints_7; }
	inline void set_disableOtherConstraints_7(bool value)
	{
		___disableOtherConstraints_7 = value;
	}

	inline static int32_t get_offset_of_pinStartBone_8() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___pinStartBone_8)); }
	inline bool get_pinStartBone_8() const { return ___pinStartBone_8; }
	inline bool* get_address_of_pinStartBone_8() { return &___pinStartBone_8; }
	inline void set_pinStartBone_8(bool value)
	{
		___pinStartBone_8 = value;
	}

	inline static int32_t get_offset_of_gravityScale_9() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___gravityScale_9)); }
	inline float get_gravityScale_9() const { return ___gravityScale_9; }
	inline float* get_address_of_gravityScale_9() { return &___gravityScale_9; }
	inline void set_gravityScale_9(float value)
	{
		___gravityScale_9 = value;
	}

	inline static int32_t get_offset_of_thickness_10() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___thickness_10)); }
	inline float get_thickness_10() const { return ___thickness_10; }
	inline float* get_address_of_thickness_10() { return &___thickness_10; }
	inline void set_thickness_10(float value)
	{
		___thickness_10 = value;
	}

	inline static int32_t get_offset_of_rotationLimit_11() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___rotationLimit_11)); }
	inline float get_rotationLimit_11() const { return ___rotationLimit_11; }
	inline float* get_address_of_rotationLimit_11() { return &___rotationLimit_11; }
	inline void set_rotationLimit_11(float value)
	{
		___rotationLimit_11 = value;
	}

	inline static int32_t get_offset_of_rootMass_12() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___rootMass_12)); }
	inline float get_rootMass_12() const { return ___rootMass_12; }
	inline float* get_address_of_rootMass_12() { return &___rootMass_12; }
	inline void set_rootMass_12(float value)
	{
		___rootMass_12 = value;
	}

	inline static int32_t get_offset_of_massFalloffFactor_13() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___massFalloffFactor_13)); }
	inline float get_massFalloffFactor_13() const { return ___massFalloffFactor_13; }
	inline float* get_address_of_massFalloffFactor_13() { return &___massFalloffFactor_13; }
	inline void set_massFalloffFactor_13(float value)
	{
		___massFalloffFactor_13 = value;
	}

	inline static int32_t get_offset_of_colliderLayer_14() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___colliderLayer_14)); }
	inline int32_t get_colliderLayer_14() const { return ___colliderLayer_14; }
	inline int32_t* get_address_of_colliderLayer_14() { return &___colliderLayer_14; }
	inline void set_colliderLayer_14(int32_t value)
	{
		___colliderLayer_14 = value;
	}

	inline static int32_t get_offset_of_mix_15() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___mix_15)); }
	inline float get_mix_15() const { return ___mix_15; }
	inline float* get_address_of_mix_15() { return &___mix_15; }
	inline void set_mix_15(float value)
	{
		___mix_15 = value;
	}

	inline static int32_t get_offset_of_targetSkeletonComponent_16() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___targetSkeletonComponent_16)); }
	inline RuntimeObject* get_targetSkeletonComponent_16() const { return ___targetSkeletonComponent_16; }
	inline RuntimeObject** get_address_of_targetSkeletonComponent_16() { return &___targetSkeletonComponent_16; }
	inline void set_targetSkeletonComponent_16(RuntimeObject* value)
	{
		___targetSkeletonComponent_16 = value;
		Il2CppCodeGenWriteBarrier((&___targetSkeletonComponent_16), value);
	}

	inline static int32_t get_offset_of_skeleton_17() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___skeleton_17)); }
	inline Skeleton_t1045203634 * get_skeleton_17() const { return ___skeleton_17; }
	inline Skeleton_t1045203634 ** get_address_of_skeleton_17() { return &___skeleton_17; }
	inline void set_skeleton_17(Skeleton_t1045203634 * value)
	{
		___skeleton_17 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_17), value);
	}

	inline static int32_t get_offset_of_boneTable_18() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___boneTable_18)); }
	inline Dictionary_2_t27791519 * get_boneTable_18() const { return ___boneTable_18; }
	inline Dictionary_2_t27791519 ** get_address_of_boneTable_18() { return &___boneTable_18; }
	inline void set_boneTable_18(Dictionary_2_t27791519 * value)
	{
		___boneTable_18 = value;
		Il2CppCodeGenWriteBarrier((&___boneTable_18), value);
	}

	inline static int32_t get_offset_of_ragdollRoot_19() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___ragdollRoot_19)); }
	inline Transform_t2735953680 * get_ragdollRoot_19() const { return ___ragdollRoot_19; }
	inline Transform_t2735953680 ** get_address_of_ragdollRoot_19() { return &___ragdollRoot_19; }
	inline void set_ragdollRoot_19(Transform_t2735953680 * value)
	{
		___ragdollRoot_19 = value;
		Il2CppCodeGenWriteBarrier((&___ragdollRoot_19), value);
	}

	inline static int32_t get_offset_of_U3CRootRigidbodyU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___U3CRootRigidbodyU3Ek__BackingField_20)); }
	inline Rigidbody2D_t4047763079 * get_U3CRootRigidbodyU3Ek__BackingField_20() const { return ___U3CRootRigidbodyU3Ek__BackingField_20; }
	inline Rigidbody2D_t4047763079 ** get_address_of_U3CRootRigidbodyU3Ek__BackingField_20() { return &___U3CRootRigidbodyU3Ek__BackingField_20; }
	inline void set_U3CRootRigidbodyU3Ek__BackingField_20(Rigidbody2D_t4047763079 * value)
	{
		___U3CRootRigidbodyU3Ek__BackingField_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootRigidbodyU3Ek__BackingField_20), value);
	}

	inline static int32_t get_offset_of_U3CStartingBoneU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___U3CStartingBoneU3Ek__BackingField_21)); }
	inline Bone_t1763862836 * get_U3CStartingBoneU3Ek__BackingField_21() const { return ___U3CStartingBoneU3Ek__BackingField_21; }
	inline Bone_t1763862836 ** get_address_of_U3CStartingBoneU3Ek__BackingField_21() { return &___U3CStartingBoneU3Ek__BackingField_21; }
	inline void set_U3CStartingBoneU3Ek__BackingField_21(Bone_t1763862836 * value)
	{
		___U3CStartingBoneU3Ek__BackingField_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStartingBoneU3Ek__BackingField_21), value);
	}

	inline static int32_t get_offset_of_rootOffset_22() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___rootOffset_22)); }
	inline Vector2_t1065050092  get_rootOffset_22() const { return ___rootOffset_22; }
	inline Vector2_t1065050092 * get_address_of_rootOffset_22() { return &___rootOffset_22; }
	inline void set_rootOffset_22(Vector2_t1065050092  value)
	{
		___rootOffset_22 = value;
	}

	inline static int32_t get_offset_of_isActive_23() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091, ___isActive_23)); }
	inline bool get_isActive_23() const { return ___isActive_23; }
	inline bool* get_address_of_isActive_23() { return &___isActive_23; }
	inline void set_isActive_23(bool value)
	{
		___isActive_23 = value;
	}
};

struct SkeletonRagdoll2D_t3940222091_StaticFields
{
public:
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonRagdoll2D::parentSpaceHelper
	Transform_t2735953680 * ___parentSpaceHelper_2;

public:
	inline static int32_t get_offset_of_parentSpaceHelper_2() { return static_cast<int32_t>(offsetof(SkeletonRagdoll2D_t3940222091_StaticFields, ___parentSpaceHelper_2)); }
	inline Transform_t2735953680 * get_parentSpaceHelper_2() const { return ___parentSpaceHelper_2; }
	inline Transform_t2735953680 ** get_address_of_parentSpaceHelper_2() { return &___parentSpaceHelper_2; }
	inline void set_parentSpaceHelper_2(Transform_t2735953680 * value)
	{
		___parentSpaceHelper_2 = value;
		Il2CppCodeGenWriteBarrier((&___parentSpaceHelper_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRAGDOLL2D_T3940222091_H
#ifndef SKELETONGHOSTRENDERER_T108436892_H
#define SKELETONGHOSTRENDERER_T108436892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhostRenderer
struct  SkeletonGhostRenderer_t108436892  : public MonoBehaviour_t3755042618
{
public:
	// System.Single Spine.Unity.Modules.SkeletonGhostRenderer::fadeSpeed
	float ___fadeSpeed_2;
	// UnityEngine.Color32[] Spine.Unity.Modules.SkeletonGhostRenderer::colors
	Color32U5BU5D_t3058208114* ___colors_3;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhostRenderer::black
	Color32_t2411287715  ___black_4;
	// UnityEngine.MeshFilter Spine.Unity.Modules.SkeletonGhostRenderer::meshFilter
	MeshFilter_t36322911 * ___meshFilter_5;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonGhostRenderer::meshRenderer
	MeshRenderer_t1621355309 * ___meshRenderer_6;

public:
	inline static int32_t get_offset_of_fadeSpeed_2() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t108436892, ___fadeSpeed_2)); }
	inline float get_fadeSpeed_2() const { return ___fadeSpeed_2; }
	inline float* get_address_of_fadeSpeed_2() { return &___fadeSpeed_2; }
	inline void set_fadeSpeed_2(float value)
	{
		___fadeSpeed_2 = value;
	}

	inline static int32_t get_offset_of_colors_3() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t108436892, ___colors_3)); }
	inline Color32U5BU5D_t3058208114* get_colors_3() const { return ___colors_3; }
	inline Color32U5BU5D_t3058208114** get_address_of_colors_3() { return &___colors_3; }
	inline void set_colors_3(Color32U5BU5D_t3058208114* value)
	{
		___colors_3 = value;
		Il2CppCodeGenWriteBarrier((&___colors_3), value);
	}

	inline static int32_t get_offset_of_black_4() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t108436892, ___black_4)); }
	inline Color32_t2411287715  get_black_4() const { return ___black_4; }
	inline Color32_t2411287715 * get_address_of_black_4() { return &___black_4; }
	inline void set_black_4(Color32_t2411287715  value)
	{
		___black_4 = value;
	}

	inline static int32_t get_offset_of_meshFilter_5() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t108436892, ___meshFilter_5)); }
	inline MeshFilter_t36322911 * get_meshFilter_5() const { return ___meshFilter_5; }
	inline MeshFilter_t36322911 ** get_address_of_meshFilter_5() { return &___meshFilter_5; }
	inline void set_meshFilter_5(MeshFilter_t36322911 * value)
	{
		___meshFilter_5 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_5), value);
	}

	inline static int32_t get_offset_of_meshRenderer_6() { return static_cast<int32_t>(offsetof(SkeletonGhostRenderer_t108436892, ___meshRenderer_6)); }
	inline MeshRenderer_t1621355309 * get_meshRenderer_6() const { return ___meshRenderer_6; }
	inline MeshRenderer_t1621355309 ** get_address_of_meshRenderer_6() { return &___meshRenderer_6; }
	inline void set_meshRenderer_6(MeshRenderer_t1621355309 * value)
	{
		___meshRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGHOSTRENDERER_T108436892_H
#ifndef BONEFOLLOWERGRAPHIC_T865397923_H
#define BONEFOLLOWERGRAPHIC_T865397923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.BoneFollowerGraphic
struct  BoneFollowerGraphic_t865397923  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.SkeletonGraphic Spine.Unity.BoneFollowerGraphic::skeletonGraphic
	SkeletonGraphic_t1867541356 * ___skeletonGraphic_2;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::initializeOnAwake
	bool ___initializeOnAwake_3;
	// System.String Spine.Unity.BoneFollowerGraphic::boneName
	String_t* ___boneName_4;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followBoneRotation
	bool ___followBoneRotation_5;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followSkeletonFlip
	bool ___followSkeletonFlip_6;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followLocalScale
	bool ___followLocalScale_7;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::followZPosition
	bool ___followZPosition_8;
	// Spine.Bone Spine.Unity.BoneFollowerGraphic::bone
	Bone_t1763862836 * ___bone_9;
	// UnityEngine.Transform Spine.Unity.BoneFollowerGraphic::skeletonTransform
	Transform_t2735953680 * ___skeletonTransform_10;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::skeletonTransformIsParent
	bool ___skeletonTransformIsParent_11;
	// System.Boolean Spine.Unity.BoneFollowerGraphic::valid
	bool ___valid_12;

public:
	inline static int32_t get_offset_of_skeletonGraphic_2() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t865397923, ___skeletonGraphic_2)); }
	inline SkeletonGraphic_t1867541356 * get_skeletonGraphic_2() const { return ___skeletonGraphic_2; }
	inline SkeletonGraphic_t1867541356 ** get_address_of_skeletonGraphic_2() { return &___skeletonGraphic_2; }
	inline void set_skeletonGraphic_2(SkeletonGraphic_t1867541356 * value)
	{
		___skeletonGraphic_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonGraphic_2), value);
	}

	inline static int32_t get_offset_of_initializeOnAwake_3() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t865397923, ___initializeOnAwake_3)); }
	inline bool get_initializeOnAwake_3() const { return ___initializeOnAwake_3; }
	inline bool* get_address_of_initializeOnAwake_3() { return &___initializeOnAwake_3; }
	inline void set_initializeOnAwake_3(bool value)
	{
		___initializeOnAwake_3 = value;
	}

	inline static int32_t get_offset_of_boneName_4() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t865397923, ___boneName_4)); }
	inline String_t* get_boneName_4() const { return ___boneName_4; }
	inline String_t** get_address_of_boneName_4() { return &___boneName_4; }
	inline void set_boneName_4(String_t* value)
	{
		___boneName_4 = value;
		Il2CppCodeGenWriteBarrier((&___boneName_4), value);
	}

	inline static int32_t get_offset_of_followBoneRotation_5() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t865397923, ___followBoneRotation_5)); }
	inline bool get_followBoneRotation_5() const { return ___followBoneRotation_5; }
	inline bool* get_address_of_followBoneRotation_5() { return &___followBoneRotation_5; }
	inline void set_followBoneRotation_5(bool value)
	{
		___followBoneRotation_5 = value;
	}

	inline static int32_t get_offset_of_followSkeletonFlip_6() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t865397923, ___followSkeletonFlip_6)); }
	inline bool get_followSkeletonFlip_6() const { return ___followSkeletonFlip_6; }
	inline bool* get_address_of_followSkeletonFlip_6() { return &___followSkeletonFlip_6; }
	inline void set_followSkeletonFlip_6(bool value)
	{
		___followSkeletonFlip_6 = value;
	}

	inline static int32_t get_offset_of_followLocalScale_7() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t865397923, ___followLocalScale_7)); }
	inline bool get_followLocalScale_7() const { return ___followLocalScale_7; }
	inline bool* get_address_of_followLocalScale_7() { return &___followLocalScale_7; }
	inline void set_followLocalScale_7(bool value)
	{
		___followLocalScale_7 = value;
	}

	inline static int32_t get_offset_of_followZPosition_8() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t865397923, ___followZPosition_8)); }
	inline bool get_followZPosition_8() const { return ___followZPosition_8; }
	inline bool* get_address_of_followZPosition_8() { return &___followZPosition_8; }
	inline void set_followZPosition_8(bool value)
	{
		___followZPosition_8 = value;
	}

	inline static int32_t get_offset_of_bone_9() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t865397923, ___bone_9)); }
	inline Bone_t1763862836 * get_bone_9() const { return ___bone_9; }
	inline Bone_t1763862836 ** get_address_of_bone_9() { return &___bone_9; }
	inline void set_bone_9(Bone_t1763862836 * value)
	{
		___bone_9 = value;
		Il2CppCodeGenWriteBarrier((&___bone_9), value);
	}

	inline static int32_t get_offset_of_skeletonTransform_10() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t865397923, ___skeletonTransform_10)); }
	inline Transform_t2735953680 * get_skeletonTransform_10() const { return ___skeletonTransform_10; }
	inline Transform_t2735953680 ** get_address_of_skeletonTransform_10() { return &___skeletonTransform_10; }
	inline void set_skeletonTransform_10(Transform_t2735953680 * value)
	{
		___skeletonTransform_10 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonTransform_10), value);
	}

	inline static int32_t get_offset_of_skeletonTransformIsParent_11() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t865397923, ___skeletonTransformIsParent_11)); }
	inline bool get_skeletonTransformIsParent_11() const { return ___skeletonTransformIsParent_11; }
	inline bool* get_address_of_skeletonTransformIsParent_11() { return &___skeletonTransformIsParent_11; }
	inline void set_skeletonTransformIsParent_11(bool value)
	{
		___skeletonTransformIsParent_11 = value;
	}

	inline static int32_t get_offset_of_valid_12() { return static_cast<int32_t>(offsetof(BoneFollowerGraphic_t865397923, ___valid_12)); }
	inline bool get_valid_12() const { return ___valid_12; }
	inline bool* get_address_of_valid_12() { return &___valid_12; }
	inline void set_valid_12(bool value)
	{
		___valid_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONEFOLLOWERGRAPHIC_T865397923_H
#ifndef SKELETONPARTSRENDERER_T3336947722_H
#define SKELETONPARTSRENDERER_T3336947722_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonPartsRenderer
struct  SkeletonPartsRenderer_t3336947722  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.MeshGenerator Spine.Unity.Modules.SkeletonPartsRenderer::meshGenerator
	MeshGenerator_t557654067 * ___meshGenerator_2;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonPartsRenderer::meshRenderer
	MeshRenderer_t1621355309 * ___meshRenderer_3;
	// UnityEngine.MeshFilter Spine.Unity.Modules.SkeletonPartsRenderer::meshFilter
	MeshFilter_t36322911 * ___meshFilter_4;
	// Spine.Unity.MeshRendererBuffers Spine.Unity.Modules.SkeletonPartsRenderer::buffers
	MeshRendererBuffers_t384051837 * ___buffers_5;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.Modules.SkeletonPartsRenderer::currentInstructions
	SkeletonRendererInstruction_t672271390 * ___currentInstructions_6;

public:
	inline static int32_t get_offset_of_meshGenerator_2() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t3336947722, ___meshGenerator_2)); }
	inline MeshGenerator_t557654067 * get_meshGenerator_2() const { return ___meshGenerator_2; }
	inline MeshGenerator_t557654067 ** get_address_of_meshGenerator_2() { return &___meshGenerator_2; }
	inline void set_meshGenerator_2(MeshGenerator_t557654067 * value)
	{
		___meshGenerator_2 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_2), value);
	}

	inline static int32_t get_offset_of_meshRenderer_3() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t3336947722, ___meshRenderer_3)); }
	inline MeshRenderer_t1621355309 * get_meshRenderer_3() const { return ___meshRenderer_3; }
	inline MeshRenderer_t1621355309 ** get_address_of_meshRenderer_3() { return &___meshRenderer_3; }
	inline void set_meshRenderer_3(MeshRenderer_t1621355309 * value)
	{
		___meshRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_3), value);
	}

	inline static int32_t get_offset_of_meshFilter_4() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t3336947722, ___meshFilter_4)); }
	inline MeshFilter_t36322911 * get_meshFilter_4() const { return ___meshFilter_4; }
	inline MeshFilter_t36322911 ** get_address_of_meshFilter_4() { return &___meshFilter_4; }
	inline void set_meshFilter_4(MeshFilter_t36322911 * value)
	{
		___meshFilter_4 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_4), value);
	}

	inline static int32_t get_offset_of_buffers_5() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t3336947722, ___buffers_5)); }
	inline MeshRendererBuffers_t384051837 * get_buffers_5() const { return ___buffers_5; }
	inline MeshRendererBuffers_t384051837 ** get_address_of_buffers_5() { return &___buffers_5; }
	inline void set_buffers_5(MeshRendererBuffers_t384051837 * value)
	{
		___buffers_5 = value;
		Il2CppCodeGenWriteBarrier((&___buffers_5), value);
	}

	inline static int32_t get_offset_of_currentInstructions_6() { return static_cast<int32_t>(offsetof(SkeletonPartsRenderer_t3336947722, ___currentInstructions_6)); }
	inline SkeletonRendererInstruction_t672271390 * get_currentInstructions_6() const { return ___currentInstructions_6; }
	inline SkeletonRendererInstruction_t672271390 ** get_address_of_currentInstructions_6() { return &___currentInstructions_6; }
	inline void set_currentInstructions_6(SkeletonRendererInstruction_t672271390 * value)
	{
		___currentInstructions_6 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONPARTSRENDERER_T3336947722_H
#ifndef UIBEHAVIOUR_T3749981382_H
#define UIBEHAVIOUR_T3749981382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_t3749981382  : public MonoBehaviour_t3755042618
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIBEHAVIOUR_T3749981382_H
#ifndef SKELETONUTILITYCONSTRAINT_T2014257453_H
#define SKELETONUTILITYCONSTRAINT_T2014257453_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtilityConstraint
struct  SkeletonUtilityConstraint_t2014257453  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.SkeletonUtilityBone Spine.Unity.SkeletonUtilityConstraint::utilBone
	SkeletonUtilityBone_t1490890952 * ___utilBone_2;
	// Spine.Unity.SkeletonUtility Spine.Unity.SkeletonUtilityConstraint::skeletonUtility
	SkeletonUtility_t373332496 * ___skeletonUtility_3;

public:
	inline static int32_t get_offset_of_utilBone_2() { return static_cast<int32_t>(offsetof(SkeletonUtilityConstraint_t2014257453, ___utilBone_2)); }
	inline SkeletonUtilityBone_t1490890952 * get_utilBone_2() const { return ___utilBone_2; }
	inline SkeletonUtilityBone_t1490890952 ** get_address_of_utilBone_2() { return &___utilBone_2; }
	inline void set_utilBone_2(SkeletonUtilityBone_t1490890952 * value)
	{
		___utilBone_2 = value;
		Il2CppCodeGenWriteBarrier((&___utilBone_2), value);
	}

	inline static int32_t get_offset_of_skeletonUtility_3() { return static_cast<int32_t>(offsetof(SkeletonUtilityConstraint_t2014257453, ___skeletonUtility_3)); }
	inline SkeletonUtility_t373332496 * get_skeletonUtility_3() const { return ___skeletonUtility_3; }
	inline SkeletonUtility_t373332496 ** get_address_of_skeletonUtility_3() { return &___skeletonUtility_3; }
	inline void set_skeletonUtility_3(SkeletonUtility_t373332496 * value)
	{
		___skeletonUtility_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonUtility_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYCONSTRAINT_T2014257453_H
#ifndef SKELETONUTILITYKINEMATICSHADOW_T2751461930_H
#define SKELETONUTILITYKINEMATICSHADOW_T2751461930_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityKinematicShadow
struct  SkeletonUtilityKinematicShadow_t2751461930  : public MonoBehaviour_t3755042618
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityKinematicShadow::detachedShadow
	bool ___detachedShadow_2;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityKinematicShadow::parent
	Transform_t2735953680 * ___parent_3;
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityKinematicShadow::hideShadow
	bool ___hideShadow_4;
	// UnityEngine.GameObject Spine.Unity.Modules.SkeletonUtilityKinematicShadow::shadowRoot
	GameObject_t3093992626 * ___shadowRoot_5;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonUtilityKinematicShadow/TransformPair> Spine.Unity.Modules.SkeletonUtilityKinematicShadow::shadowTable
	List_1_t3853717968 * ___shadowTable_6;

public:
	inline static int32_t get_offset_of_detachedShadow_2() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t2751461930, ___detachedShadow_2)); }
	inline bool get_detachedShadow_2() const { return ___detachedShadow_2; }
	inline bool* get_address_of_detachedShadow_2() { return &___detachedShadow_2; }
	inline void set_detachedShadow_2(bool value)
	{
		___detachedShadow_2 = value;
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t2751461930, ___parent_3)); }
	inline Transform_t2735953680 * get_parent_3() const { return ___parent_3; }
	inline Transform_t2735953680 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Transform_t2735953680 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((&___parent_3), value);
	}

	inline static int32_t get_offset_of_hideShadow_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t2751461930, ___hideShadow_4)); }
	inline bool get_hideShadow_4() const { return ___hideShadow_4; }
	inline bool* get_address_of_hideShadow_4() { return &___hideShadow_4; }
	inline void set_hideShadow_4(bool value)
	{
		___hideShadow_4 = value;
	}

	inline static int32_t get_offset_of_shadowRoot_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t2751461930, ___shadowRoot_5)); }
	inline GameObject_t3093992626 * get_shadowRoot_5() const { return ___shadowRoot_5; }
	inline GameObject_t3093992626 ** get_address_of_shadowRoot_5() { return &___shadowRoot_5; }
	inline void set_shadowRoot_5(GameObject_t3093992626 * value)
	{
		___shadowRoot_5 = value;
		Il2CppCodeGenWriteBarrier((&___shadowRoot_5), value);
	}

	inline static int32_t get_offset_of_shadowTable_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityKinematicShadow_t2751461930, ___shadowTable_6)); }
	inline List_1_t3853717968 * get_shadowTable_6() const { return ___shadowTable_6; }
	inline List_1_t3853717968 ** get_address_of_shadowTable_6() { return &___shadowTable_6; }
	inline void set_shadowTable_6(List_1_t3853717968 * value)
	{
		___shadowTable_6 = value;
		Il2CppCodeGenWriteBarrier((&___shadowTable_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYKINEMATICSHADOW_T2751461930_H
#ifndef SKELETONRENDERSEPARATOR_T3391021243_H
#define SKELETONRENDERSEPARATOR_T3391021243_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRenderSeparator
struct  SkeletonRenderSeparator_t3391021243  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonRenderSeparator::skeletonRenderer
	SkeletonRenderer_t1681389628 * ___skeletonRenderer_3;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonRenderSeparator::mainMeshRenderer
	MeshRenderer_t1621355309 * ___mainMeshRenderer_4;
	// System.Boolean Spine.Unity.Modules.SkeletonRenderSeparator::copyPropertyBlock
	bool ___copyPropertyBlock_5;
	// System.Boolean Spine.Unity.Modules.SkeletonRenderSeparator::copyMeshRendererFlags
	bool ___copyMeshRendererFlags_6;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonPartsRenderer> Spine.Unity.Modules.SkeletonRenderSeparator::partsRenderers
	List_1_t1165396151 * ___partsRenderers_7;
	// UnityEngine.MaterialPropertyBlock Spine.Unity.Modules.SkeletonRenderSeparator::copiedBlock
	MaterialPropertyBlock_t2982817174 * ___copiedBlock_8;

public:
	inline static int32_t get_offset_of_skeletonRenderer_3() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t3391021243, ___skeletonRenderer_3)); }
	inline SkeletonRenderer_t1681389628 * get_skeletonRenderer_3() const { return ___skeletonRenderer_3; }
	inline SkeletonRenderer_t1681389628 ** get_address_of_skeletonRenderer_3() { return &___skeletonRenderer_3; }
	inline void set_skeletonRenderer_3(SkeletonRenderer_t1681389628 * value)
	{
		___skeletonRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_3), value);
	}

	inline static int32_t get_offset_of_mainMeshRenderer_4() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t3391021243, ___mainMeshRenderer_4)); }
	inline MeshRenderer_t1621355309 * get_mainMeshRenderer_4() const { return ___mainMeshRenderer_4; }
	inline MeshRenderer_t1621355309 ** get_address_of_mainMeshRenderer_4() { return &___mainMeshRenderer_4; }
	inline void set_mainMeshRenderer_4(MeshRenderer_t1621355309 * value)
	{
		___mainMeshRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainMeshRenderer_4), value);
	}

	inline static int32_t get_offset_of_copyPropertyBlock_5() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t3391021243, ___copyPropertyBlock_5)); }
	inline bool get_copyPropertyBlock_5() const { return ___copyPropertyBlock_5; }
	inline bool* get_address_of_copyPropertyBlock_5() { return &___copyPropertyBlock_5; }
	inline void set_copyPropertyBlock_5(bool value)
	{
		___copyPropertyBlock_5 = value;
	}

	inline static int32_t get_offset_of_copyMeshRendererFlags_6() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t3391021243, ___copyMeshRendererFlags_6)); }
	inline bool get_copyMeshRendererFlags_6() const { return ___copyMeshRendererFlags_6; }
	inline bool* get_address_of_copyMeshRendererFlags_6() { return &___copyMeshRendererFlags_6; }
	inline void set_copyMeshRendererFlags_6(bool value)
	{
		___copyMeshRendererFlags_6 = value;
	}

	inline static int32_t get_offset_of_partsRenderers_7() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t3391021243, ___partsRenderers_7)); }
	inline List_1_t1165396151 * get_partsRenderers_7() const { return ___partsRenderers_7; }
	inline List_1_t1165396151 ** get_address_of_partsRenderers_7() { return &___partsRenderers_7; }
	inline void set_partsRenderers_7(List_1_t1165396151 * value)
	{
		___partsRenderers_7 = value;
		Il2CppCodeGenWriteBarrier((&___partsRenderers_7), value);
	}

	inline static int32_t get_offset_of_copiedBlock_8() { return static_cast<int32_t>(offsetof(SkeletonRenderSeparator_t3391021243, ___copiedBlock_8)); }
	inline MaterialPropertyBlock_t2982817174 * get_copiedBlock_8() const { return ___copiedBlock_8; }
	inline MaterialPropertyBlock_t2982817174 ** get_address_of_copiedBlock_8() { return &___copiedBlock_8; }
	inline void set_copiedBlock_8(MaterialPropertyBlock_t2982817174 * value)
	{
		___copiedBlock_8 = value;
		Il2CppCodeGenWriteBarrier((&___copiedBlock_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERSEPARATOR_T3391021243_H
#ifndef SLOTBLENDMODES_T3822096134_H
#define SLOTBLENDMODES_T3822096134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SlotBlendModes
struct  SlotBlendModes_t3822096134  : public MonoBehaviour_t3755042618
{
public:
	// UnityEngine.Material Spine.Unity.Modules.SlotBlendModes::multiplyMaterialSource
	Material_t2681358023 * ___multiplyMaterialSource_3;
	// UnityEngine.Material Spine.Unity.Modules.SlotBlendModes::screenMaterialSource
	Material_t2681358023 * ___screenMaterialSource_4;
	// UnityEngine.Texture2D Spine.Unity.Modules.SlotBlendModes::texture
	Texture2D_t1431210461 * ___texture_5;
	// System.Boolean Spine.Unity.Modules.SlotBlendModes::<Applied>k__BackingField
	bool ___U3CAppliedU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_multiplyMaterialSource_3() { return static_cast<int32_t>(offsetof(SlotBlendModes_t3822096134, ___multiplyMaterialSource_3)); }
	inline Material_t2681358023 * get_multiplyMaterialSource_3() const { return ___multiplyMaterialSource_3; }
	inline Material_t2681358023 ** get_address_of_multiplyMaterialSource_3() { return &___multiplyMaterialSource_3; }
	inline void set_multiplyMaterialSource_3(Material_t2681358023 * value)
	{
		___multiplyMaterialSource_3 = value;
		Il2CppCodeGenWriteBarrier((&___multiplyMaterialSource_3), value);
	}

	inline static int32_t get_offset_of_screenMaterialSource_4() { return static_cast<int32_t>(offsetof(SlotBlendModes_t3822096134, ___screenMaterialSource_4)); }
	inline Material_t2681358023 * get_screenMaterialSource_4() const { return ___screenMaterialSource_4; }
	inline Material_t2681358023 ** get_address_of_screenMaterialSource_4() { return &___screenMaterialSource_4; }
	inline void set_screenMaterialSource_4(Material_t2681358023 * value)
	{
		___screenMaterialSource_4 = value;
		Il2CppCodeGenWriteBarrier((&___screenMaterialSource_4), value);
	}

	inline static int32_t get_offset_of_texture_5() { return static_cast<int32_t>(offsetof(SlotBlendModes_t3822096134, ___texture_5)); }
	inline Texture2D_t1431210461 * get_texture_5() const { return ___texture_5; }
	inline Texture2D_t1431210461 ** get_address_of_texture_5() { return &___texture_5; }
	inline void set_texture_5(Texture2D_t1431210461 * value)
	{
		___texture_5 = value;
		Il2CppCodeGenWriteBarrier((&___texture_5), value);
	}

	inline static int32_t get_offset_of_U3CAppliedU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(SlotBlendModes_t3822096134, ___U3CAppliedU3Ek__BackingField_6)); }
	inline bool get_U3CAppliedU3Ek__BackingField_6() const { return ___U3CAppliedU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CAppliedU3Ek__BackingField_6() { return &___U3CAppliedU3Ek__BackingField_6; }
	inline void set_U3CAppliedU3Ek__BackingField_6(bool value)
	{
		___U3CAppliedU3Ek__BackingField_6 = value;
	}
};

struct SlotBlendModes_t3822096134_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<Spine.Unity.Modules.SlotBlendModes/MaterialTexturePair,UnityEngine.Material> Spine.Unity.Modules.SlotBlendModes::materialTable
	Dictionary_2_t722230041 * ___materialTable_2;

public:
	inline static int32_t get_offset_of_materialTable_2() { return static_cast<int32_t>(offsetof(SlotBlendModes_t3822096134_StaticFields, ___materialTable_2)); }
	inline Dictionary_2_t722230041 * get_materialTable_2() const { return ___materialTable_2; }
	inline Dictionary_2_t722230041 ** get_address_of_materialTable_2() { return &___materialTable_2; }
	inline void set_materialTable_2(Dictionary_2_t722230041 * value)
	{
		___materialTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___materialTable_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTBLENDMODES_T3822096134_H
#ifndef SKELETONRENDERERCUSTOMMATERIALS_T2116292039_H
#define SKELETONRENDERERCUSTOMMATERIALS_T2116292039_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonRendererCustomMaterials
struct  SkeletonRendererCustomMaterials_t2116292039  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonRendererCustomMaterials::skeletonRenderer
	SkeletonRenderer_t1681389628 * ___skeletonRenderer_2;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/SlotMaterialOverride> Spine.Unity.Modules.SkeletonRendererCustomMaterials::customSlotMaterials
	List_1_t908661968 * ___customSlotMaterials_3;
	// System.Collections.Generic.List`1<Spine.Unity.Modules.SkeletonRendererCustomMaterials/AtlasMaterialOverride> Spine.Unity.Modules.SkeletonRendererCustomMaterials::customMaterialOverrides
	List_1_t2073729961 * ___customMaterialOverrides_4;

public:
	inline static int32_t get_offset_of_skeletonRenderer_2() { return static_cast<int32_t>(offsetof(SkeletonRendererCustomMaterials_t2116292039, ___skeletonRenderer_2)); }
	inline SkeletonRenderer_t1681389628 * get_skeletonRenderer_2() const { return ___skeletonRenderer_2; }
	inline SkeletonRenderer_t1681389628 ** get_address_of_skeletonRenderer_2() { return &___skeletonRenderer_2; }
	inline void set_skeletonRenderer_2(SkeletonRenderer_t1681389628 * value)
	{
		___skeletonRenderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_2), value);
	}

	inline static int32_t get_offset_of_customSlotMaterials_3() { return static_cast<int32_t>(offsetof(SkeletonRendererCustomMaterials_t2116292039, ___customSlotMaterials_3)); }
	inline List_1_t908661968 * get_customSlotMaterials_3() const { return ___customSlotMaterials_3; }
	inline List_1_t908661968 ** get_address_of_customSlotMaterials_3() { return &___customSlotMaterials_3; }
	inline void set_customSlotMaterials_3(List_1_t908661968 * value)
	{
		___customSlotMaterials_3 = value;
		Il2CppCodeGenWriteBarrier((&___customSlotMaterials_3), value);
	}

	inline static int32_t get_offset_of_customMaterialOverrides_4() { return static_cast<int32_t>(offsetof(SkeletonRendererCustomMaterials_t2116292039, ___customMaterialOverrides_4)); }
	inline List_1_t2073729961 * get_customMaterialOverrides_4() const { return ___customMaterialOverrides_4; }
	inline List_1_t2073729961 ** get_address_of_customMaterialOverrides_4() { return &___customMaterialOverrides_4; }
	inline void set_customMaterialOverrides_4(List_1_t2073729961 * value)
	{
		___customMaterialOverrides_4 = value;
		Il2CppCodeGenWriteBarrier((&___customMaterialOverrides_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERERCUSTOMMATERIALS_T2116292039_H
#ifndef BOUNDINGBOXFOLLOWER_T836972383_H
#define BOUNDINGBOXFOLLOWER_T836972383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.BoundingBoxFollower
struct  BoundingBoxFollower_t836972383  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.SkeletonRenderer Spine.Unity.BoundingBoxFollower::skeletonRenderer
	SkeletonRenderer_t1681389628 * ___skeletonRenderer_3;
	// System.String Spine.Unity.BoundingBoxFollower::slotName
	String_t* ___slotName_4;
	// System.Boolean Spine.Unity.BoundingBoxFollower::isTrigger
	bool ___isTrigger_5;
	// System.Boolean Spine.Unity.BoundingBoxFollower::clearStateOnDisable
	bool ___clearStateOnDisable_6;
	// Spine.Slot Spine.Unity.BoundingBoxFollower::slot
	Slot_t1804116343 * ___slot_7;
	// Spine.BoundingBoxAttachment Spine.Unity.BoundingBoxFollower::currentAttachment
	BoundingBoxAttachment_t1026836171 * ___currentAttachment_8;
	// System.String Spine.Unity.BoundingBoxFollower::currentAttachmentName
	String_t* ___currentAttachmentName_9;
	// UnityEngine.PolygonCollider2D Spine.Unity.BoundingBoxFollower::currentCollider
	PolygonCollider2D_t542995269 * ___currentCollider_10;
	// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,UnityEngine.PolygonCollider2D> Spine.Unity.BoundingBoxFollower::colliderTable
	Dictionary_2_t569002753 * ___colliderTable_11;
	// System.Collections.Generic.Dictionary`2<Spine.BoundingBoxAttachment,System.String> Spine.Unity.BoundingBoxFollower::nameTable
	Dictionary_2_t1571386255 * ___nameTable_12;

public:
	inline static int32_t get_offset_of_skeletonRenderer_3() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t836972383, ___skeletonRenderer_3)); }
	inline SkeletonRenderer_t1681389628 * get_skeletonRenderer_3() const { return ___skeletonRenderer_3; }
	inline SkeletonRenderer_t1681389628 ** get_address_of_skeletonRenderer_3() { return &___skeletonRenderer_3; }
	inline void set_skeletonRenderer_3(SkeletonRenderer_t1681389628 * value)
	{
		___skeletonRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_3), value);
	}

	inline static int32_t get_offset_of_slotName_4() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t836972383, ___slotName_4)); }
	inline String_t* get_slotName_4() const { return ___slotName_4; }
	inline String_t** get_address_of_slotName_4() { return &___slotName_4; }
	inline void set_slotName_4(String_t* value)
	{
		___slotName_4 = value;
		Il2CppCodeGenWriteBarrier((&___slotName_4), value);
	}

	inline static int32_t get_offset_of_isTrigger_5() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t836972383, ___isTrigger_5)); }
	inline bool get_isTrigger_5() const { return ___isTrigger_5; }
	inline bool* get_address_of_isTrigger_5() { return &___isTrigger_5; }
	inline void set_isTrigger_5(bool value)
	{
		___isTrigger_5 = value;
	}

	inline static int32_t get_offset_of_clearStateOnDisable_6() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t836972383, ___clearStateOnDisable_6)); }
	inline bool get_clearStateOnDisable_6() const { return ___clearStateOnDisable_6; }
	inline bool* get_address_of_clearStateOnDisable_6() { return &___clearStateOnDisable_6; }
	inline void set_clearStateOnDisable_6(bool value)
	{
		___clearStateOnDisable_6 = value;
	}

	inline static int32_t get_offset_of_slot_7() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t836972383, ___slot_7)); }
	inline Slot_t1804116343 * get_slot_7() const { return ___slot_7; }
	inline Slot_t1804116343 ** get_address_of_slot_7() { return &___slot_7; }
	inline void set_slot_7(Slot_t1804116343 * value)
	{
		___slot_7 = value;
		Il2CppCodeGenWriteBarrier((&___slot_7), value);
	}

	inline static int32_t get_offset_of_currentAttachment_8() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t836972383, ___currentAttachment_8)); }
	inline BoundingBoxAttachment_t1026836171 * get_currentAttachment_8() const { return ___currentAttachment_8; }
	inline BoundingBoxAttachment_t1026836171 ** get_address_of_currentAttachment_8() { return &___currentAttachment_8; }
	inline void set_currentAttachment_8(BoundingBoxAttachment_t1026836171 * value)
	{
		___currentAttachment_8 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttachment_8), value);
	}

	inline static int32_t get_offset_of_currentAttachmentName_9() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t836972383, ___currentAttachmentName_9)); }
	inline String_t* get_currentAttachmentName_9() const { return ___currentAttachmentName_9; }
	inline String_t** get_address_of_currentAttachmentName_9() { return &___currentAttachmentName_9; }
	inline void set_currentAttachmentName_9(String_t* value)
	{
		___currentAttachmentName_9 = value;
		Il2CppCodeGenWriteBarrier((&___currentAttachmentName_9), value);
	}

	inline static int32_t get_offset_of_currentCollider_10() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t836972383, ___currentCollider_10)); }
	inline PolygonCollider2D_t542995269 * get_currentCollider_10() const { return ___currentCollider_10; }
	inline PolygonCollider2D_t542995269 ** get_address_of_currentCollider_10() { return &___currentCollider_10; }
	inline void set_currentCollider_10(PolygonCollider2D_t542995269 * value)
	{
		___currentCollider_10 = value;
		Il2CppCodeGenWriteBarrier((&___currentCollider_10), value);
	}

	inline static int32_t get_offset_of_colliderTable_11() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t836972383, ___colliderTable_11)); }
	inline Dictionary_2_t569002753 * get_colliderTable_11() const { return ___colliderTable_11; }
	inline Dictionary_2_t569002753 ** get_address_of_colliderTable_11() { return &___colliderTable_11; }
	inline void set_colliderTable_11(Dictionary_2_t569002753 * value)
	{
		___colliderTable_11 = value;
		Il2CppCodeGenWriteBarrier((&___colliderTable_11), value);
	}

	inline static int32_t get_offset_of_nameTable_12() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t836972383, ___nameTable_12)); }
	inline Dictionary_2_t1571386255 * get_nameTable_12() const { return ___nameTable_12; }
	inline Dictionary_2_t1571386255 ** get_address_of_nameTable_12() { return &___nameTable_12; }
	inline void set_nameTable_12(Dictionary_2_t1571386255 * value)
	{
		___nameTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_12), value);
	}
};

struct BoundingBoxFollower_t836972383_StaticFields
{
public:
	// System.Boolean Spine.Unity.BoundingBoxFollower::DebugMessages
	bool ___DebugMessages_2;

public:
	inline static int32_t get_offset_of_DebugMessages_2() { return static_cast<int32_t>(offsetof(BoundingBoxFollower_t836972383_StaticFields, ___DebugMessages_2)); }
	inline bool get_DebugMessages_2() const { return ___DebugMessages_2; }
	inline bool* get_address_of_DebugMessages_2() { return &___DebugMessages_2; }
	inline void set_DebugMessages_2(bool value)
	{
		___DebugMessages_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOXFOLLOWER_T836972383_H
#ifndef SKELETONRENDERER_T1681389628_H
#define SKELETONRENDERER_T1681389628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer
struct  SkeletonRenderer_t1681389628  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate Spine.Unity.SkeletonRenderer::OnRebuild
	SkeletonRendererDelegate_t878065760 * ___OnRebuild_2;
	// Spine.Unity.MeshGeneratorDelegate Spine.Unity.SkeletonRenderer::OnPostProcessVertices
	MeshGeneratorDelegate_t2067952305 * ___OnPostProcessVertices_3;
	// Spine.Unity.SkeletonDataAsset Spine.Unity.SkeletonRenderer::skeletonDataAsset
	SkeletonDataAsset_t1706375087 * ___skeletonDataAsset_4;
	// System.String Spine.Unity.SkeletonRenderer::initialSkinName
	String_t* ___initialSkinName_5;
	// System.Boolean Spine.Unity.SkeletonRenderer::initialFlipX
	bool ___initialFlipX_6;
	// System.Boolean Spine.Unity.SkeletonRenderer::initialFlipY
	bool ___initialFlipY_7;
	// System.String[] Spine.Unity.SkeletonRenderer::separatorSlotNames
	StringU5BU5D_t656593794* ___separatorSlotNames_8;
	// System.Collections.Generic.List`1<Spine.Slot> Spine.Unity.SkeletonRenderer::separatorSlots
	List_1_t3927532068 * ___separatorSlots_9;
	// System.Single Spine.Unity.SkeletonRenderer::zSpacing
	float ___zSpacing_10;
	// System.Boolean Spine.Unity.SkeletonRenderer::useClipping
	bool ___useClipping_11;
	// System.Boolean Spine.Unity.SkeletonRenderer::immutableTriangles
	bool ___immutableTriangles_12;
	// System.Boolean Spine.Unity.SkeletonRenderer::pmaVertexColors
	bool ___pmaVertexColors_13;
	// System.Boolean Spine.Unity.SkeletonRenderer::clearStateOnDisable
	bool ___clearStateOnDisable_14;
	// System.Boolean Spine.Unity.SkeletonRenderer::tintBlack
	bool ___tintBlack_15;
	// System.Boolean Spine.Unity.SkeletonRenderer::singleSubmesh
	bool ___singleSubmesh_16;
	// System.Boolean Spine.Unity.SkeletonRenderer::addNormals
	bool ___addNormals_17;
	// System.Boolean Spine.Unity.SkeletonRenderer::calculateTangents
	bool ___calculateTangents_18;
	// System.Boolean Spine.Unity.SkeletonRenderer::logErrors
	bool ___logErrors_19;
	// System.Boolean Spine.Unity.SkeletonRenderer::disableRenderingOnOverride
	bool ___disableRenderingOnOverride_20;
	// Spine.Unity.SkeletonRenderer/InstructionDelegate Spine.Unity.SkeletonRenderer::generateMeshOverride
	InstructionDelegate_t1951844748 * ___generateMeshOverride_21;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material> Spine.Unity.SkeletonRenderer::customMaterialOverride
	Dictionary_2_t2258539287 * ___customMaterialOverride_22;
	// System.Collections.Generic.Dictionary`2<Spine.Slot,UnityEngine.Material> Spine.Unity.SkeletonRenderer::customSlotMaterials
	Dictionary_2_t14780327 * ___customSlotMaterials_23;
	// UnityEngine.MeshRenderer Spine.Unity.SkeletonRenderer::meshRenderer
	MeshRenderer_t1621355309 * ___meshRenderer_24;
	// UnityEngine.MeshFilter Spine.Unity.SkeletonRenderer::meshFilter
	MeshFilter_t36322911 * ___meshFilter_25;
	// System.Boolean Spine.Unity.SkeletonRenderer::valid
	bool ___valid_26;
	// Spine.Skeleton Spine.Unity.SkeletonRenderer::skeleton
	Skeleton_t1045203634 * ___skeleton_27;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.SkeletonRenderer::currentInstructions
	SkeletonRendererInstruction_t672271390 * ___currentInstructions_28;
	// Spine.Unity.MeshGenerator Spine.Unity.SkeletonRenderer::meshGenerator
	MeshGenerator_t557654067 * ___meshGenerator_29;
	// Spine.Unity.MeshRendererBuffers Spine.Unity.SkeletonRenderer::rendererBuffers
	MeshRendererBuffers_t384051837 * ___rendererBuffers_30;

public:
	inline static int32_t get_offset_of_OnRebuild_2() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___OnRebuild_2)); }
	inline SkeletonRendererDelegate_t878065760 * get_OnRebuild_2() const { return ___OnRebuild_2; }
	inline SkeletonRendererDelegate_t878065760 ** get_address_of_OnRebuild_2() { return &___OnRebuild_2; }
	inline void set_OnRebuild_2(SkeletonRendererDelegate_t878065760 * value)
	{
		___OnRebuild_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnRebuild_2), value);
	}

	inline static int32_t get_offset_of_OnPostProcessVertices_3() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___OnPostProcessVertices_3)); }
	inline MeshGeneratorDelegate_t2067952305 * get_OnPostProcessVertices_3() const { return ___OnPostProcessVertices_3; }
	inline MeshGeneratorDelegate_t2067952305 ** get_address_of_OnPostProcessVertices_3() { return &___OnPostProcessVertices_3; }
	inline void set_OnPostProcessVertices_3(MeshGeneratorDelegate_t2067952305 * value)
	{
		___OnPostProcessVertices_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostProcessVertices_3), value);
	}

	inline static int32_t get_offset_of_skeletonDataAsset_4() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___skeletonDataAsset_4)); }
	inline SkeletonDataAsset_t1706375087 * get_skeletonDataAsset_4() const { return ___skeletonDataAsset_4; }
	inline SkeletonDataAsset_t1706375087 ** get_address_of_skeletonDataAsset_4() { return &___skeletonDataAsset_4; }
	inline void set_skeletonDataAsset_4(SkeletonDataAsset_t1706375087 * value)
	{
		___skeletonDataAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_4), value);
	}

	inline static int32_t get_offset_of_initialSkinName_5() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___initialSkinName_5)); }
	inline String_t* get_initialSkinName_5() const { return ___initialSkinName_5; }
	inline String_t** get_address_of_initialSkinName_5() { return &___initialSkinName_5; }
	inline void set_initialSkinName_5(String_t* value)
	{
		___initialSkinName_5 = value;
		Il2CppCodeGenWriteBarrier((&___initialSkinName_5), value);
	}

	inline static int32_t get_offset_of_initialFlipX_6() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___initialFlipX_6)); }
	inline bool get_initialFlipX_6() const { return ___initialFlipX_6; }
	inline bool* get_address_of_initialFlipX_6() { return &___initialFlipX_6; }
	inline void set_initialFlipX_6(bool value)
	{
		___initialFlipX_6 = value;
	}

	inline static int32_t get_offset_of_initialFlipY_7() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___initialFlipY_7)); }
	inline bool get_initialFlipY_7() const { return ___initialFlipY_7; }
	inline bool* get_address_of_initialFlipY_7() { return &___initialFlipY_7; }
	inline void set_initialFlipY_7(bool value)
	{
		___initialFlipY_7 = value;
	}

	inline static int32_t get_offset_of_separatorSlotNames_8() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___separatorSlotNames_8)); }
	inline StringU5BU5D_t656593794* get_separatorSlotNames_8() const { return ___separatorSlotNames_8; }
	inline StringU5BU5D_t656593794** get_address_of_separatorSlotNames_8() { return &___separatorSlotNames_8; }
	inline void set_separatorSlotNames_8(StringU5BU5D_t656593794* value)
	{
		___separatorSlotNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___separatorSlotNames_8), value);
	}

	inline static int32_t get_offset_of_separatorSlots_9() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___separatorSlots_9)); }
	inline List_1_t3927532068 * get_separatorSlots_9() const { return ___separatorSlots_9; }
	inline List_1_t3927532068 ** get_address_of_separatorSlots_9() { return &___separatorSlots_9; }
	inline void set_separatorSlots_9(List_1_t3927532068 * value)
	{
		___separatorSlots_9 = value;
		Il2CppCodeGenWriteBarrier((&___separatorSlots_9), value);
	}

	inline static int32_t get_offset_of_zSpacing_10() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___zSpacing_10)); }
	inline float get_zSpacing_10() const { return ___zSpacing_10; }
	inline float* get_address_of_zSpacing_10() { return &___zSpacing_10; }
	inline void set_zSpacing_10(float value)
	{
		___zSpacing_10 = value;
	}

	inline static int32_t get_offset_of_useClipping_11() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___useClipping_11)); }
	inline bool get_useClipping_11() const { return ___useClipping_11; }
	inline bool* get_address_of_useClipping_11() { return &___useClipping_11; }
	inline void set_useClipping_11(bool value)
	{
		___useClipping_11 = value;
	}

	inline static int32_t get_offset_of_immutableTriangles_12() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___immutableTriangles_12)); }
	inline bool get_immutableTriangles_12() const { return ___immutableTriangles_12; }
	inline bool* get_address_of_immutableTriangles_12() { return &___immutableTriangles_12; }
	inline void set_immutableTriangles_12(bool value)
	{
		___immutableTriangles_12 = value;
	}

	inline static int32_t get_offset_of_pmaVertexColors_13() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___pmaVertexColors_13)); }
	inline bool get_pmaVertexColors_13() const { return ___pmaVertexColors_13; }
	inline bool* get_address_of_pmaVertexColors_13() { return &___pmaVertexColors_13; }
	inline void set_pmaVertexColors_13(bool value)
	{
		___pmaVertexColors_13 = value;
	}

	inline static int32_t get_offset_of_clearStateOnDisable_14() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___clearStateOnDisable_14)); }
	inline bool get_clearStateOnDisable_14() const { return ___clearStateOnDisable_14; }
	inline bool* get_address_of_clearStateOnDisable_14() { return &___clearStateOnDisable_14; }
	inline void set_clearStateOnDisable_14(bool value)
	{
		___clearStateOnDisable_14 = value;
	}

	inline static int32_t get_offset_of_tintBlack_15() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___tintBlack_15)); }
	inline bool get_tintBlack_15() const { return ___tintBlack_15; }
	inline bool* get_address_of_tintBlack_15() { return &___tintBlack_15; }
	inline void set_tintBlack_15(bool value)
	{
		___tintBlack_15 = value;
	}

	inline static int32_t get_offset_of_singleSubmesh_16() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___singleSubmesh_16)); }
	inline bool get_singleSubmesh_16() const { return ___singleSubmesh_16; }
	inline bool* get_address_of_singleSubmesh_16() { return &___singleSubmesh_16; }
	inline void set_singleSubmesh_16(bool value)
	{
		___singleSubmesh_16 = value;
	}

	inline static int32_t get_offset_of_addNormals_17() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___addNormals_17)); }
	inline bool get_addNormals_17() const { return ___addNormals_17; }
	inline bool* get_address_of_addNormals_17() { return &___addNormals_17; }
	inline void set_addNormals_17(bool value)
	{
		___addNormals_17 = value;
	}

	inline static int32_t get_offset_of_calculateTangents_18() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___calculateTangents_18)); }
	inline bool get_calculateTangents_18() const { return ___calculateTangents_18; }
	inline bool* get_address_of_calculateTangents_18() { return &___calculateTangents_18; }
	inline void set_calculateTangents_18(bool value)
	{
		___calculateTangents_18 = value;
	}

	inline static int32_t get_offset_of_logErrors_19() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___logErrors_19)); }
	inline bool get_logErrors_19() const { return ___logErrors_19; }
	inline bool* get_address_of_logErrors_19() { return &___logErrors_19; }
	inline void set_logErrors_19(bool value)
	{
		___logErrors_19 = value;
	}

	inline static int32_t get_offset_of_disableRenderingOnOverride_20() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___disableRenderingOnOverride_20)); }
	inline bool get_disableRenderingOnOverride_20() const { return ___disableRenderingOnOverride_20; }
	inline bool* get_address_of_disableRenderingOnOverride_20() { return &___disableRenderingOnOverride_20; }
	inline void set_disableRenderingOnOverride_20(bool value)
	{
		___disableRenderingOnOverride_20 = value;
	}

	inline static int32_t get_offset_of_generateMeshOverride_21() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___generateMeshOverride_21)); }
	inline InstructionDelegate_t1951844748 * get_generateMeshOverride_21() const { return ___generateMeshOverride_21; }
	inline InstructionDelegate_t1951844748 ** get_address_of_generateMeshOverride_21() { return &___generateMeshOverride_21; }
	inline void set_generateMeshOverride_21(InstructionDelegate_t1951844748 * value)
	{
		___generateMeshOverride_21 = value;
		Il2CppCodeGenWriteBarrier((&___generateMeshOverride_21), value);
	}

	inline static int32_t get_offset_of_customMaterialOverride_22() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___customMaterialOverride_22)); }
	inline Dictionary_2_t2258539287 * get_customMaterialOverride_22() const { return ___customMaterialOverride_22; }
	inline Dictionary_2_t2258539287 ** get_address_of_customMaterialOverride_22() { return &___customMaterialOverride_22; }
	inline void set_customMaterialOverride_22(Dictionary_2_t2258539287 * value)
	{
		___customMaterialOverride_22 = value;
		Il2CppCodeGenWriteBarrier((&___customMaterialOverride_22), value);
	}

	inline static int32_t get_offset_of_customSlotMaterials_23() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___customSlotMaterials_23)); }
	inline Dictionary_2_t14780327 * get_customSlotMaterials_23() const { return ___customSlotMaterials_23; }
	inline Dictionary_2_t14780327 ** get_address_of_customSlotMaterials_23() { return &___customSlotMaterials_23; }
	inline void set_customSlotMaterials_23(Dictionary_2_t14780327 * value)
	{
		___customSlotMaterials_23 = value;
		Il2CppCodeGenWriteBarrier((&___customSlotMaterials_23), value);
	}

	inline static int32_t get_offset_of_meshRenderer_24() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___meshRenderer_24)); }
	inline MeshRenderer_t1621355309 * get_meshRenderer_24() const { return ___meshRenderer_24; }
	inline MeshRenderer_t1621355309 ** get_address_of_meshRenderer_24() { return &___meshRenderer_24; }
	inline void set_meshRenderer_24(MeshRenderer_t1621355309 * value)
	{
		___meshRenderer_24 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_24), value);
	}

	inline static int32_t get_offset_of_meshFilter_25() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___meshFilter_25)); }
	inline MeshFilter_t36322911 * get_meshFilter_25() const { return ___meshFilter_25; }
	inline MeshFilter_t36322911 ** get_address_of_meshFilter_25() { return &___meshFilter_25; }
	inline void set_meshFilter_25(MeshFilter_t36322911 * value)
	{
		___meshFilter_25 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_25), value);
	}

	inline static int32_t get_offset_of_valid_26() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___valid_26)); }
	inline bool get_valid_26() const { return ___valid_26; }
	inline bool* get_address_of_valid_26() { return &___valid_26; }
	inline void set_valid_26(bool value)
	{
		___valid_26 = value;
	}

	inline static int32_t get_offset_of_skeleton_27() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___skeleton_27)); }
	inline Skeleton_t1045203634 * get_skeleton_27() const { return ___skeleton_27; }
	inline Skeleton_t1045203634 ** get_address_of_skeleton_27() { return &___skeleton_27; }
	inline void set_skeleton_27(Skeleton_t1045203634 * value)
	{
		___skeleton_27 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_27), value);
	}

	inline static int32_t get_offset_of_currentInstructions_28() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___currentInstructions_28)); }
	inline SkeletonRendererInstruction_t672271390 * get_currentInstructions_28() const { return ___currentInstructions_28; }
	inline SkeletonRendererInstruction_t672271390 ** get_address_of_currentInstructions_28() { return &___currentInstructions_28; }
	inline void set_currentInstructions_28(SkeletonRendererInstruction_t672271390 * value)
	{
		___currentInstructions_28 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_28), value);
	}

	inline static int32_t get_offset_of_meshGenerator_29() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___meshGenerator_29)); }
	inline MeshGenerator_t557654067 * get_meshGenerator_29() const { return ___meshGenerator_29; }
	inline MeshGenerator_t557654067 ** get_address_of_meshGenerator_29() { return &___meshGenerator_29; }
	inline void set_meshGenerator_29(MeshGenerator_t557654067 * value)
	{
		___meshGenerator_29 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_29), value);
	}

	inline static int32_t get_offset_of_rendererBuffers_30() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___rendererBuffers_30)); }
	inline MeshRendererBuffers_t384051837 * get_rendererBuffers_30() const { return ___rendererBuffers_30; }
	inline MeshRendererBuffers_t384051837 ** get_address_of_rendererBuffers_30() { return &___rendererBuffers_30; }
	inline void set_rendererBuffers_30(MeshRendererBuffers_t384051837 * value)
	{
		___rendererBuffers_30 = value;
		Il2CppCodeGenWriteBarrier((&___rendererBuffers_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERER_T1681389628_H
#ifndef SKELETONUTILITY_T373332496_H
#define SKELETONUTILITY_T373332496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonUtility
struct  SkeletonUtility_t373332496  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.SkeletonUtility/SkeletonUtilityDelegate Spine.Unity.SkeletonUtility::OnReset
	SkeletonUtilityDelegate_t1611778497 * ___OnReset_2;
	// UnityEngine.Transform Spine.Unity.SkeletonUtility::boneRoot
	Transform_t2735953680 * ___boneRoot_3;
	// Spine.Unity.SkeletonRenderer Spine.Unity.SkeletonUtility::skeletonRenderer
	SkeletonRenderer_t1681389628 * ___skeletonRenderer_4;
	// Spine.Unity.ISkeletonAnimation Spine.Unity.SkeletonUtility::skeletonAnimation
	RuntimeObject* ___skeletonAnimation_5;
	// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityBone> Spine.Unity.SkeletonUtility::utilityBones
	List_1_t3614306677 * ___utilityBones_6;
	// System.Collections.Generic.List`1<Spine.Unity.SkeletonUtilityConstraint> Spine.Unity.SkeletonUtility::utilityConstraints
	List_1_t4137673178 * ___utilityConstraints_7;
	// System.Boolean Spine.Unity.SkeletonUtility::hasTransformBones
	bool ___hasTransformBones_8;
	// System.Boolean Spine.Unity.SkeletonUtility::hasUtilityConstraints
	bool ___hasUtilityConstraints_9;
	// System.Boolean Spine.Unity.SkeletonUtility::needToReprocessBones
	bool ___needToReprocessBones_10;

public:
	inline static int32_t get_offset_of_OnReset_2() { return static_cast<int32_t>(offsetof(SkeletonUtility_t373332496, ___OnReset_2)); }
	inline SkeletonUtilityDelegate_t1611778497 * get_OnReset_2() const { return ___OnReset_2; }
	inline SkeletonUtilityDelegate_t1611778497 ** get_address_of_OnReset_2() { return &___OnReset_2; }
	inline void set_OnReset_2(SkeletonUtilityDelegate_t1611778497 * value)
	{
		___OnReset_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnReset_2), value);
	}

	inline static int32_t get_offset_of_boneRoot_3() { return static_cast<int32_t>(offsetof(SkeletonUtility_t373332496, ___boneRoot_3)); }
	inline Transform_t2735953680 * get_boneRoot_3() const { return ___boneRoot_3; }
	inline Transform_t2735953680 ** get_address_of_boneRoot_3() { return &___boneRoot_3; }
	inline void set_boneRoot_3(Transform_t2735953680 * value)
	{
		___boneRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&___boneRoot_3), value);
	}

	inline static int32_t get_offset_of_skeletonRenderer_4() { return static_cast<int32_t>(offsetof(SkeletonUtility_t373332496, ___skeletonRenderer_4)); }
	inline SkeletonRenderer_t1681389628 * get_skeletonRenderer_4() const { return ___skeletonRenderer_4; }
	inline SkeletonRenderer_t1681389628 ** get_address_of_skeletonRenderer_4() { return &___skeletonRenderer_4; }
	inline void set_skeletonRenderer_4(SkeletonRenderer_t1681389628 * value)
	{
		___skeletonRenderer_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_4), value);
	}

	inline static int32_t get_offset_of_skeletonAnimation_5() { return static_cast<int32_t>(offsetof(SkeletonUtility_t373332496, ___skeletonAnimation_5)); }
	inline RuntimeObject* get_skeletonAnimation_5() const { return ___skeletonAnimation_5; }
	inline RuntimeObject** get_address_of_skeletonAnimation_5() { return &___skeletonAnimation_5; }
	inline void set_skeletonAnimation_5(RuntimeObject* value)
	{
		___skeletonAnimation_5 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonAnimation_5), value);
	}

	inline static int32_t get_offset_of_utilityBones_6() { return static_cast<int32_t>(offsetof(SkeletonUtility_t373332496, ___utilityBones_6)); }
	inline List_1_t3614306677 * get_utilityBones_6() const { return ___utilityBones_6; }
	inline List_1_t3614306677 ** get_address_of_utilityBones_6() { return &___utilityBones_6; }
	inline void set_utilityBones_6(List_1_t3614306677 * value)
	{
		___utilityBones_6 = value;
		Il2CppCodeGenWriteBarrier((&___utilityBones_6), value);
	}

	inline static int32_t get_offset_of_utilityConstraints_7() { return static_cast<int32_t>(offsetof(SkeletonUtility_t373332496, ___utilityConstraints_7)); }
	inline List_1_t4137673178 * get_utilityConstraints_7() const { return ___utilityConstraints_7; }
	inline List_1_t4137673178 ** get_address_of_utilityConstraints_7() { return &___utilityConstraints_7; }
	inline void set_utilityConstraints_7(List_1_t4137673178 * value)
	{
		___utilityConstraints_7 = value;
		Il2CppCodeGenWriteBarrier((&___utilityConstraints_7), value);
	}

	inline static int32_t get_offset_of_hasTransformBones_8() { return static_cast<int32_t>(offsetof(SkeletonUtility_t373332496, ___hasTransformBones_8)); }
	inline bool get_hasTransformBones_8() const { return ___hasTransformBones_8; }
	inline bool* get_address_of_hasTransformBones_8() { return &___hasTransformBones_8; }
	inline void set_hasTransformBones_8(bool value)
	{
		___hasTransformBones_8 = value;
	}

	inline static int32_t get_offset_of_hasUtilityConstraints_9() { return static_cast<int32_t>(offsetof(SkeletonUtility_t373332496, ___hasUtilityConstraints_9)); }
	inline bool get_hasUtilityConstraints_9() const { return ___hasUtilityConstraints_9; }
	inline bool* get_address_of_hasUtilityConstraints_9() { return &___hasUtilityConstraints_9; }
	inline void set_hasUtilityConstraints_9(bool value)
	{
		___hasUtilityConstraints_9 = value;
	}

	inline static int32_t get_offset_of_needToReprocessBones_10() { return static_cast<int32_t>(offsetof(SkeletonUtility_t373332496, ___needToReprocessBones_10)); }
	inline bool get_needToReprocessBones_10() const { return ___needToReprocessBones_10; }
	inline bool* get_address_of_needToReprocessBones_10() { return &___needToReprocessBones_10; }
	inline void set_needToReprocessBones_10(bool value)
	{
		___needToReprocessBones_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITY_T373332496_H
#ifndef SKELETONGHOST_T2198715580_H
#define SKELETONGHOST_T2198715580_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonGhost
struct  SkeletonGhost_t2198715580  : public MonoBehaviour_t3755042618
{
public:
	// System.Boolean Spine.Unity.Modules.SkeletonGhost::ghostingEnabled
	bool ___ghostingEnabled_4;
	// System.Single Spine.Unity.Modules.SkeletonGhost::spawnRate
	float ___spawnRate_5;
	// UnityEngine.Color32 Spine.Unity.Modules.SkeletonGhost::color
	Color32_t2411287715  ___color_6;
	// System.Boolean Spine.Unity.Modules.SkeletonGhost::additive
	bool ___additive_7;
	// System.Int32 Spine.Unity.Modules.SkeletonGhost::maximumGhosts
	int32_t ___maximumGhosts_8;
	// System.Single Spine.Unity.Modules.SkeletonGhost::fadeSpeed
	float ___fadeSpeed_9;
	// UnityEngine.Shader Spine.Unity.Modules.SkeletonGhost::ghostShader
	Shader_t2404284061 * ___ghostShader_10;
	// System.Single Spine.Unity.Modules.SkeletonGhost::textureFade
	float ___textureFade_11;
	// System.Boolean Spine.Unity.Modules.SkeletonGhost::sortWithDistanceOnly
	bool ___sortWithDistanceOnly_12;
	// System.Single Spine.Unity.Modules.SkeletonGhost::zOffset
	float ___zOffset_13;
	// System.Single Spine.Unity.Modules.SkeletonGhost::nextSpawnTime
	float ___nextSpawnTime_14;
	// Spine.Unity.Modules.SkeletonGhostRenderer[] Spine.Unity.Modules.SkeletonGhost::pool
	SkeletonGhostRendererU5BU5D_t2441972853* ___pool_15;
	// System.Int32 Spine.Unity.Modules.SkeletonGhost::poolIndex
	int32_t ___poolIndex_16;
	// Spine.Unity.SkeletonRenderer Spine.Unity.Modules.SkeletonGhost::skeletonRenderer
	SkeletonRenderer_t1681389628 * ___skeletonRenderer_17;
	// UnityEngine.MeshRenderer Spine.Unity.Modules.SkeletonGhost::meshRenderer
	MeshRenderer_t1621355309 * ___meshRenderer_18;
	// UnityEngine.MeshFilter Spine.Unity.Modules.SkeletonGhost::meshFilter
	MeshFilter_t36322911 * ___meshFilter_19;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material> Spine.Unity.Modules.SkeletonGhost::materialTable
	Dictionary_2_t2258539287 * ___materialTable_20;

public:
	inline static int32_t get_offset_of_ghostingEnabled_4() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___ghostingEnabled_4)); }
	inline bool get_ghostingEnabled_4() const { return ___ghostingEnabled_4; }
	inline bool* get_address_of_ghostingEnabled_4() { return &___ghostingEnabled_4; }
	inline void set_ghostingEnabled_4(bool value)
	{
		___ghostingEnabled_4 = value;
	}

	inline static int32_t get_offset_of_spawnRate_5() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___spawnRate_5)); }
	inline float get_spawnRate_5() const { return ___spawnRate_5; }
	inline float* get_address_of_spawnRate_5() { return &___spawnRate_5; }
	inline void set_spawnRate_5(float value)
	{
		___spawnRate_5 = value;
	}

	inline static int32_t get_offset_of_color_6() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___color_6)); }
	inline Color32_t2411287715  get_color_6() const { return ___color_6; }
	inline Color32_t2411287715 * get_address_of_color_6() { return &___color_6; }
	inline void set_color_6(Color32_t2411287715  value)
	{
		___color_6 = value;
	}

	inline static int32_t get_offset_of_additive_7() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___additive_7)); }
	inline bool get_additive_7() const { return ___additive_7; }
	inline bool* get_address_of_additive_7() { return &___additive_7; }
	inline void set_additive_7(bool value)
	{
		___additive_7 = value;
	}

	inline static int32_t get_offset_of_maximumGhosts_8() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___maximumGhosts_8)); }
	inline int32_t get_maximumGhosts_8() const { return ___maximumGhosts_8; }
	inline int32_t* get_address_of_maximumGhosts_8() { return &___maximumGhosts_8; }
	inline void set_maximumGhosts_8(int32_t value)
	{
		___maximumGhosts_8 = value;
	}

	inline static int32_t get_offset_of_fadeSpeed_9() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___fadeSpeed_9)); }
	inline float get_fadeSpeed_9() const { return ___fadeSpeed_9; }
	inline float* get_address_of_fadeSpeed_9() { return &___fadeSpeed_9; }
	inline void set_fadeSpeed_9(float value)
	{
		___fadeSpeed_9 = value;
	}

	inline static int32_t get_offset_of_ghostShader_10() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___ghostShader_10)); }
	inline Shader_t2404284061 * get_ghostShader_10() const { return ___ghostShader_10; }
	inline Shader_t2404284061 ** get_address_of_ghostShader_10() { return &___ghostShader_10; }
	inline void set_ghostShader_10(Shader_t2404284061 * value)
	{
		___ghostShader_10 = value;
		Il2CppCodeGenWriteBarrier((&___ghostShader_10), value);
	}

	inline static int32_t get_offset_of_textureFade_11() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___textureFade_11)); }
	inline float get_textureFade_11() const { return ___textureFade_11; }
	inline float* get_address_of_textureFade_11() { return &___textureFade_11; }
	inline void set_textureFade_11(float value)
	{
		___textureFade_11 = value;
	}

	inline static int32_t get_offset_of_sortWithDistanceOnly_12() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___sortWithDistanceOnly_12)); }
	inline bool get_sortWithDistanceOnly_12() const { return ___sortWithDistanceOnly_12; }
	inline bool* get_address_of_sortWithDistanceOnly_12() { return &___sortWithDistanceOnly_12; }
	inline void set_sortWithDistanceOnly_12(bool value)
	{
		___sortWithDistanceOnly_12 = value;
	}

	inline static int32_t get_offset_of_zOffset_13() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___zOffset_13)); }
	inline float get_zOffset_13() const { return ___zOffset_13; }
	inline float* get_address_of_zOffset_13() { return &___zOffset_13; }
	inline void set_zOffset_13(float value)
	{
		___zOffset_13 = value;
	}

	inline static int32_t get_offset_of_nextSpawnTime_14() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___nextSpawnTime_14)); }
	inline float get_nextSpawnTime_14() const { return ___nextSpawnTime_14; }
	inline float* get_address_of_nextSpawnTime_14() { return &___nextSpawnTime_14; }
	inline void set_nextSpawnTime_14(float value)
	{
		___nextSpawnTime_14 = value;
	}

	inline static int32_t get_offset_of_pool_15() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___pool_15)); }
	inline SkeletonGhostRendererU5BU5D_t2441972853* get_pool_15() const { return ___pool_15; }
	inline SkeletonGhostRendererU5BU5D_t2441972853** get_address_of_pool_15() { return &___pool_15; }
	inline void set_pool_15(SkeletonGhostRendererU5BU5D_t2441972853* value)
	{
		___pool_15 = value;
		Il2CppCodeGenWriteBarrier((&___pool_15), value);
	}

	inline static int32_t get_offset_of_poolIndex_16() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___poolIndex_16)); }
	inline int32_t get_poolIndex_16() const { return ___poolIndex_16; }
	inline int32_t* get_address_of_poolIndex_16() { return &___poolIndex_16; }
	inline void set_poolIndex_16(int32_t value)
	{
		___poolIndex_16 = value;
	}

	inline static int32_t get_offset_of_skeletonRenderer_17() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___skeletonRenderer_17)); }
	inline SkeletonRenderer_t1681389628 * get_skeletonRenderer_17() const { return ___skeletonRenderer_17; }
	inline SkeletonRenderer_t1681389628 ** get_address_of_skeletonRenderer_17() { return &___skeletonRenderer_17; }
	inline void set_skeletonRenderer_17(SkeletonRenderer_t1681389628 * value)
	{
		___skeletonRenderer_17 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonRenderer_17), value);
	}

	inline static int32_t get_offset_of_meshRenderer_18() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___meshRenderer_18)); }
	inline MeshRenderer_t1621355309 * get_meshRenderer_18() const { return ___meshRenderer_18; }
	inline MeshRenderer_t1621355309 ** get_address_of_meshRenderer_18() { return &___meshRenderer_18; }
	inline void set_meshRenderer_18(MeshRenderer_t1621355309 * value)
	{
		___meshRenderer_18 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_18), value);
	}

	inline static int32_t get_offset_of_meshFilter_19() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___meshFilter_19)); }
	inline MeshFilter_t36322911 * get_meshFilter_19() const { return ___meshFilter_19; }
	inline MeshFilter_t36322911 ** get_address_of_meshFilter_19() { return &___meshFilter_19; }
	inline void set_meshFilter_19(MeshFilter_t36322911 * value)
	{
		___meshFilter_19 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_19), value);
	}

	inline static int32_t get_offset_of_materialTable_20() { return static_cast<int32_t>(offsetof(SkeletonGhost_t2198715580, ___materialTable_20)); }
	inline Dictionary_2_t2258539287 * get_materialTable_20() const { return ___materialTable_20; }
	inline Dictionary_2_t2258539287 ** get_address_of_materialTable_20() { return &___materialTable_20; }
	inline void set_materialTable_20(Dictionary_2_t2258539287 * value)
	{
		___materialTable_20 = value;
		Il2CppCodeGenWriteBarrier((&___materialTable_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGHOST_T2198715580_H
#ifndef SKELETONUTILITYEYECONSTRAINT_T66635751_H
#define SKELETONUTILITYEYECONSTRAINT_T66635751_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityEyeConstraint
struct  SkeletonUtilityEyeConstraint_t66635751  : public SkeletonUtilityConstraint_t2014257453
{
public:
	// UnityEngine.Transform[] Spine.Unity.Modules.SkeletonUtilityEyeConstraint::eyes
	TransformU5BU5D_t2219023025* ___eyes_4;
	// System.Single Spine.Unity.Modules.SkeletonUtilityEyeConstraint::radius
	float ___radius_5;
	// UnityEngine.Transform Spine.Unity.Modules.SkeletonUtilityEyeConstraint::target
	Transform_t2735953680 * ___target_6;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityEyeConstraint::targetPosition
	Vector3_t2825674791  ___targetPosition_7;
	// System.Single Spine.Unity.Modules.SkeletonUtilityEyeConstraint::speed
	float ___speed_8;
	// UnityEngine.Vector3[] Spine.Unity.Modules.SkeletonUtilityEyeConstraint::origins
	Vector3U5BU5D_t3550801758* ___origins_9;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityEyeConstraint::centerPoint
	Vector3_t2825674791  ___centerPoint_10;

public:
	inline static int32_t get_offset_of_eyes_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t66635751, ___eyes_4)); }
	inline TransformU5BU5D_t2219023025* get_eyes_4() const { return ___eyes_4; }
	inline TransformU5BU5D_t2219023025** get_address_of_eyes_4() { return &___eyes_4; }
	inline void set_eyes_4(TransformU5BU5D_t2219023025* value)
	{
		___eyes_4 = value;
		Il2CppCodeGenWriteBarrier((&___eyes_4), value);
	}

	inline static int32_t get_offset_of_radius_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t66635751, ___radius_5)); }
	inline float get_radius_5() const { return ___radius_5; }
	inline float* get_address_of_radius_5() { return &___radius_5; }
	inline void set_radius_5(float value)
	{
		___radius_5 = value;
	}

	inline static int32_t get_offset_of_target_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t66635751, ___target_6)); }
	inline Transform_t2735953680 * get_target_6() const { return ___target_6; }
	inline Transform_t2735953680 ** get_address_of_target_6() { return &___target_6; }
	inline void set_target_6(Transform_t2735953680 * value)
	{
		___target_6 = value;
		Il2CppCodeGenWriteBarrier((&___target_6), value);
	}

	inline static int32_t get_offset_of_targetPosition_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t66635751, ___targetPosition_7)); }
	inline Vector3_t2825674791  get_targetPosition_7() const { return ___targetPosition_7; }
	inline Vector3_t2825674791 * get_address_of_targetPosition_7() { return &___targetPosition_7; }
	inline void set_targetPosition_7(Vector3_t2825674791  value)
	{
		___targetPosition_7 = value;
	}

	inline static int32_t get_offset_of_speed_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t66635751, ___speed_8)); }
	inline float get_speed_8() const { return ___speed_8; }
	inline float* get_address_of_speed_8() { return &___speed_8; }
	inline void set_speed_8(float value)
	{
		___speed_8 = value;
	}

	inline static int32_t get_offset_of_origins_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t66635751, ___origins_9)); }
	inline Vector3U5BU5D_t3550801758* get_origins_9() const { return ___origins_9; }
	inline Vector3U5BU5D_t3550801758** get_address_of_origins_9() { return &___origins_9; }
	inline void set_origins_9(Vector3U5BU5D_t3550801758* value)
	{
		___origins_9 = value;
		Il2CppCodeGenWriteBarrier((&___origins_9), value);
	}

	inline static int32_t get_offset_of_centerPoint_10() { return static_cast<int32_t>(offsetof(SkeletonUtilityEyeConstraint_t66635751, ___centerPoint_10)); }
	inline Vector3_t2825674791  get_centerPoint_10() const { return ___centerPoint_10; }
	inline Vector3_t2825674791 * get_address_of_centerPoint_10() { return &___centerPoint_10; }
	inline void set_centerPoint_10(Vector3_t2825674791  value)
	{
		___centerPoint_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYEYECONSTRAINT_T66635751_H
#ifndef SKELETONANIMATION_T2012766265_H
#define SKELETONANIMATION_T2012766265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimation
struct  SkeletonAnimation_t2012766265  : public SkeletonRenderer_t1681389628
{
public:
	// Spine.AnimationState Spine.Unity.SkeletonAnimation::state
	AnimationState_t2858486651 * ___state_31;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateLocal
	UpdateBonesDelegate_t500272946 * ____UpdateLocal_32;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateWorld
	UpdateBonesDelegate_t500272946 * ____UpdateWorld_33;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateComplete
	UpdateBonesDelegate_t500272946 * ____UpdateComplete_34;
	// System.String Spine.Unity.SkeletonAnimation::_animationName
	String_t* ____animationName_35;
	// System.Boolean Spine.Unity.SkeletonAnimation::loop
	bool ___loop_36;
	// System.Single Spine.Unity.SkeletonAnimation::timeScale
	float ___timeScale_37;

public:
	inline static int32_t get_offset_of_state_31() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ___state_31)); }
	inline AnimationState_t2858486651 * get_state_31() const { return ___state_31; }
	inline AnimationState_t2858486651 ** get_address_of_state_31() { return &___state_31; }
	inline void set_state_31(AnimationState_t2858486651 * value)
	{
		___state_31 = value;
		Il2CppCodeGenWriteBarrier((&___state_31), value);
	}

	inline static int32_t get_offset_of__UpdateLocal_32() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ____UpdateLocal_32)); }
	inline UpdateBonesDelegate_t500272946 * get__UpdateLocal_32() const { return ____UpdateLocal_32; }
	inline UpdateBonesDelegate_t500272946 ** get_address_of__UpdateLocal_32() { return &____UpdateLocal_32; }
	inline void set__UpdateLocal_32(UpdateBonesDelegate_t500272946 * value)
	{
		____UpdateLocal_32 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateLocal_32), value);
	}

	inline static int32_t get_offset_of__UpdateWorld_33() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ____UpdateWorld_33)); }
	inline UpdateBonesDelegate_t500272946 * get__UpdateWorld_33() const { return ____UpdateWorld_33; }
	inline UpdateBonesDelegate_t500272946 ** get_address_of__UpdateWorld_33() { return &____UpdateWorld_33; }
	inline void set__UpdateWorld_33(UpdateBonesDelegate_t500272946 * value)
	{
		____UpdateWorld_33 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateWorld_33), value);
	}

	inline static int32_t get_offset_of__UpdateComplete_34() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ____UpdateComplete_34)); }
	inline UpdateBonesDelegate_t500272946 * get__UpdateComplete_34() const { return ____UpdateComplete_34; }
	inline UpdateBonesDelegate_t500272946 ** get_address_of__UpdateComplete_34() { return &____UpdateComplete_34; }
	inline void set__UpdateComplete_34(UpdateBonesDelegate_t500272946 * value)
	{
		____UpdateComplete_34 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateComplete_34), value);
	}

	inline static int32_t get_offset_of__animationName_35() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ____animationName_35)); }
	inline String_t* get__animationName_35() const { return ____animationName_35; }
	inline String_t** get_address_of__animationName_35() { return &____animationName_35; }
	inline void set__animationName_35(String_t* value)
	{
		____animationName_35 = value;
		Il2CppCodeGenWriteBarrier((&____animationName_35), value);
	}

	inline static int32_t get_offset_of_loop_36() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ___loop_36)); }
	inline bool get_loop_36() const { return ___loop_36; }
	inline bool* get_address_of_loop_36() { return &___loop_36; }
	inline void set_loop_36(bool value)
	{
		___loop_36 = value;
	}

	inline static int32_t get_offset_of_timeScale_37() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ___timeScale_37)); }
	inline float get_timeScale_37() const { return ___timeScale_37; }
	inline float* get_address_of_timeScale_37() { return &___timeScale_37; }
	inline void set_timeScale_37(float value)
	{
		___timeScale_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONANIMATION_T2012766265_H
#ifndef SKELETONANIMATOR_T3794034912_H
#define SKELETONANIMATOR_T3794034912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimator
struct  SkeletonAnimator_t3794034912  : public SkeletonRenderer_t1681389628
{
public:
	// Spine.Unity.SkeletonAnimator/MecanimTranslator Spine.Unity.SkeletonAnimator::translator
	MecanimTranslator_t3470518188 * ___translator_31;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimator::_UpdateLocal
	UpdateBonesDelegate_t500272946 * ____UpdateLocal_32;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimator::_UpdateWorld
	UpdateBonesDelegate_t500272946 * ____UpdateWorld_33;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimator::_UpdateComplete
	UpdateBonesDelegate_t500272946 * ____UpdateComplete_34;

public:
	inline static int32_t get_offset_of_translator_31() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t3794034912, ___translator_31)); }
	inline MecanimTranslator_t3470518188 * get_translator_31() const { return ___translator_31; }
	inline MecanimTranslator_t3470518188 ** get_address_of_translator_31() { return &___translator_31; }
	inline void set_translator_31(MecanimTranslator_t3470518188 * value)
	{
		___translator_31 = value;
		Il2CppCodeGenWriteBarrier((&___translator_31), value);
	}

	inline static int32_t get_offset_of__UpdateLocal_32() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t3794034912, ____UpdateLocal_32)); }
	inline UpdateBonesDelegate_t500272946 * get__UpdateLocal_32() const { return ____UpdateLocal_32; }
	inline UpdateBonesDelegate_t500272946 ** get_address_of__UpdateLocal_32() { return &____UpdateLocal_32; }
	inline void set__UpdateLocal_32(UpdateBonesDelegate_t500272946 * value)
	{
		____UpdateLocal_32 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateLocal_32), value);
	}

	inline static int32_t get_offset_of__UpdateWorld_33() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t3794034912, ____UpdateWorld_33)); }
	inline UpdateBonesDelegate_t500272946 * get__UpdateWorld_33() const { return ____UpdateWorld_33; }
	inline UpdateBonesDelegate_t500272946 ** get_address_of__UpdateWorld_33() { return &____UpdateWorld_33; }
	inline void set__UpdateWorld_33(UpdateBonesDelegate_t500272946 * value)
	{
		____UpdateWorld_33 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateWorld_33), value);
	}

	inline static int32_t get_offset_of__UpdateComplete_34() { return static_cast<int32_t>(offsetof(SkeletonAnimator_t3794034912, ____UpdateComplete_34)); }
	inline UpdateBonesDelegate_t500272946 * get__UpdateComplete_34() const { return ____UpdateComplete_34; }
	inline UpdateBonesDelegate_t500272946 ** get_address_of__UpdateComplete_34() { return &____UpdateComplete_34; }
	inline void set__UpdateComplete_34(UpdateBonesDelegate_t500272946 * value)
	{
		____UpdateComplete_34 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateComplete_34), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONANIMATOR_T3794034912_H
#ifndef GRAPHIC_T366741146_H
#define GRAPHIC_T366741146_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.Graphic
struct  Graphic_t366741146  : public UIBehaviour_t3749981382
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t2681358023 * ___m_Material_4;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_t4183816058  ___m_Color_5;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_6;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t1087788885 * ___m_RectTransform_7;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRender
	CanvasRenderer_t123344510 * ___m_CanvasRender_8;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t452078485 * ___m_Canvas_9;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_10;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_11;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t4110260629 * ___m_OnDirtyLayoutCallback_12;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t4110260629 * ___m_OnDirtyVertsCallback_13;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t4110260629 * ___m_OnDirtyMaterialCallback_14;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_t223451261 * ___m_ColorTweenRunner_17;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_m_Material_4() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___m_Material_4)); }
	inline Material_t2681358023 * get_m_Material_4() const { return ___m_Material_4; }
	inline Material_t2681358023 ** get_address_of_m_Material_4() { return &___m_Material_4; }
	inline void set_m_Material_4(Material_t2681358023 * value)
	{
		___m_Material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_4), value);
	}

	inline static int32_t get_offset_of_m_Color_5() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___m_Color_5)); }
	inline Color_t4183816058  get_m_Color_5() const { return ___m_Color_5; }
	inline Color_t4183816058 * get_address_of_m_Color_5() { return &___m_Color_5; }
	inline void set_m_Color_5(Color_t4183816058  value)
	{
		___m_Color_5 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_6() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___m_RaycastTarget_6)); }
	inline bool get_m_RaycastTarget_6() const { return ___m_RaycastTarget_6; }
	inline bool* get_address_of_m_RaycastTarget_6() { return &___m_RaycastTarget_6; }
	inline void set_m_RaycastTarget_6(bool value)
	{
		___m_RaycastTarget_6 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_7() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___m_RectTransform_7)); }
	inline RectTransform_t1087788885 * get_m_RectTransform_7() const { return ___m_RectTransform_7; }
	inline RectTransform_t1087788885 ** get_address_of_m_RectTransform_7() { return &___m_RectTransform_7; }
	inline void set_m_RectTransform_7(RectTransform_t1087788885 * value)
	{
		___m_RectTransform_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_RectTransform_7), value);
	}

	inline static int32_t get_offset_of_m_CanvasRender_8() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___m_CanvasRender_8)); }
	inline CanvasRenderer_t123344510 * get_m_CanvasRender_8() const { return ___m_CanvasRender_8; }
	inline CanvasRenderer_t123344510 ** get_address_of_m_CanvasRender_8() { return &___m_CanvasRender_8; }
	inline void set_m_CanvasRender_8(CanvasRenderer_t123344510 * value)
	{
		___m_CanvasRender_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CanvasRender_8), value);
	}

	inline static int32_t get_offset_of_m_Canvas_9() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___m_Canvas_9)); }
	inline Canvas_t452078485 * get_m_Canvas_9() const { return ___m_Canvas_9; }
	inline Canvas_t452078485 ** get_address_of_m_Canvas_9() { return &___m_Canvas_9; }
	inline void set_m_Canvas_9(Canvas_t452078485 * value)
	{
		___m_Canvas_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Canvas_9), value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_10() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___m_VertsDirty_10)); }
	inline bool get_m_VertsDirty_10() const { return ___m_VertsDirty_10; }
	inline bool* get_address_of_m_VertsDirty_10() { return &___m_VertsDirty_10; }
	inline void set_m_VertsDirty_10(bool value)
	{
		___m_VertsDirty_10 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_11() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___m_MaterialDirty_11)); }
	inline bool get_m_MaterialDirty_11() const { return ___m_MaterialDirty_11; }
	inline bool* get_address_of_m_MaterialDirty_11() { return &___m_MaterialDirty_11; }
	inline void set_m_MaterialDirty_11(bool value)
	{
		___m_MaterialDirty_11 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_12() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___m_OnDirtyLayoutCallback_12)); }
	inline UnityAction_t4110260629 * get_m_OnDirtyLayoutCallback_12() const { return ___m_OnDirtyLayoutCallback_12; }
	inline UnityAction_t4110260629 ** get_address_of_m_OnDirtyLayoutCallback_12() { return &___m_OnDirtyLayoutCallback_12; }
	inline void set_m_OnDirtyLayoutCallback_12(UnityAction_t4110260629 * value)
	{
		___m_OnDirtyLayoutCallback_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyLayoutCallback_12), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_13() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___m_OnDirtyVertsCallback_13)); }
	inline UnityAction_t4110260629 * get_m_OnDirtyVertsCallback_13() const { return ___m_OnDirtyVertsCallback_13; }
	inline UnityAction_t4110260629 ** get_address_of_m_OnDirtyVertsCallback_13() { return &___m_OnDirtyVertsCallback_13; }
	inline void set_m_OnDirtyVertsCallback_13(UnityAction_t4110260629 * value)
	{
		___m_OnDirtyVertsCallback_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyVertsCallback_13), value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_14() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___m_OnDirtyMaterialCallback_14)); }
	inline UnityAction_t4110260629 * get_m_OnDirtyMaterialCallback_14() const { return ___m_OnDirtyMaterialCallback_14; }
	inline UnityAction_t4110260629 ** get_address_of_m_OnDirtyMaterialCallback_14() { return &___m_OnDirtyMaterialCallback_14; }
	inline void set_m_OnDirtyMaterialCallback_14(UnityAction_t4110260629 * value)
	{
		___m_OnDirtyMaterialCallback_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnDirtyMaterialCallback_14), value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_17() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___m_ColorTweenRunner_17)); }
	inline TweenRunner_1_t223451261 * get_m_ColorTweenRunner_17() const { return ___m_ColorTweenRunner_17; }
	inline TweenRunner_1_t223451261 ** get_address_of_m_ColorTweenRunner_17() { return &___m_ColorTweenRunner_17; }
	inline void set_m_ColorTweenRunner_17(TweenRunner_1_t223451261 * value)
	{
		___m_ColorTweenRunner_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorTweenRunner_17), value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Graphic_t366741146, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_18() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_18; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_18(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_18 = value;
	}
};

struct Graphic_t366741146_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t2681358023 * ___s_DefaultUI_2;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t1431210461 * ___s_WhiteTexture_3;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t3526862104 * ___s_Mesh_15;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_t3002916242 * ___s_VertexHelper_16;

public:
	inline static int32_t get_offset_of_s_DefaultUI_2() { return static_cast<int32_t>(offsetof(Graphic_t366741146_StaticFields, ___s_DefaultUI_2)); }
	inline Material_t2681358023 * get_s_DefaultUI_2() const { return ___s_DefaultUI_2; }
	inline Material_t2681358023 ** get_address_of_s_DefaultUI_2() { return &___s_DefaultUI_2; }
	inline void set_s_DefaultUI_2(Material_t2681358023 * value)
	{
		___s_DefaultUI_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_DefaultUI_2), value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_3() { return static_cast<int32_t>(offsetof(Graphic_t366741146_StaticFields, ___s_WhiteTexture_3)); }
	inline Texture2D_t1431210461 * get_s_WhiteTexture_3() const { return ___s_WhiteTexture_3; }
	inline Texture2D_t1431210461 ** get_address_of_s_WhiteTexture_3() { return &___s_WhiteTexture_3; }
	inline void set_s_WhiteTexture_3(Texture2D_t1431210461 * value)
	{
		___s_WhiteTexture_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_3), value);
	}

	inline static int32_t get_offset_of_s_Mesh_15() { return static_cast<int32_t>(offsetof(Graphic_t366741146_StaticFields, ___s_Mesh_15)); }
	inline Mesh_t3526862104 * get_s_Mesh_15() const { return ___s_Mesh_15; }
	inline Mesh_t3526862104 ** get_address_of_s_Mesh_15() { return &___s_Mesh_15; }
	inline void set_s_Mesh_15(Mesh_t3526862104 * value)
	{
		___s_Mesh_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_Mesh_15), value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_16() { return static_cast<int32_t>(offsetof(Graphic_t366741146_StaticFields, ___s_VertexHelper_16)); }
	inline VertexHelper_t3002916242 * get_s_VertexHelper_16() const { return ___s_VertexHelper_16; }
	inline VertexHelper_t3002916242 ** get_address_of_s_VertexHelper_16() { return &___s_VertexHelper_16; }
	inline void set_s_VertexHelper_16(VertexHelper_t3002916242 * value)
	{
		___s_VertexHelper_16 = value;
		Il2CppCodeGenWriteBarrier((&___s_VertexHelper_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHIC_T366741146_H
#ifndef SKELETONUTILITYGROUNDCONSTRAINT_T2092006017_H
#define SKELETONUTILITYGROUNDCONSTRAINT_T2092006017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.Modules.SkeletonUtilityGroundConstraint
struct  SkeletonUtilityGroundConstraint_t2092006017  : public SkeletonUtilityConstraint_t2014257453
{
public:
	// UnityEngine.LayerMask Spine.Unity.Modules.SkeletonUtilityGroundConstraint::groundMask
	LayerMask_t3306677380  ___groundMask_4;
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityGroundConstraint::use2D
	bool ___use2D_5;
	// System.Boolean Spine.Unity.Modules.SkeletonUtilityGroundConstraint::useRadius
	bool ___useRadius_6;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::castRadius
	float ___castRadius_7;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::castDistance
	float ___castDistance_8;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::castOffset
	float ___castOffset_9;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::groundOffset
	float ___groundOffset_10;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::adjustSpeed
	float ___adjustSpeed_11;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityGroundConstraint::rayOrigin
	Vector3_t2825674791  ___rayOrigin_12;
	// UnityEngine.Vector3 Spine.Unity.Modules.SkeletonUtilityGroundConstraint::rayDir
	Vector3_t2825674791  ___rayDir_13;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::hitY
	float ___hitY_14;
	// System.Single Spine.Unity.Modules.SkeletonUtilityGroundConstraint::lastHitY
	float ___lastHitY_15;

public:
	inline static int32_t get_offset_of_groundMask_4() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t2092006017, ___groundMask_4)); }
	inline LayerMask_t3306677380  get_groundMask_4() const { return ___groundMask_4; }
	inline LayerMask_t3306677380 * get_address_of_groundMask_4() { return &___groundMask_4; }
	inline void set_groundMask_4(LayerMask_t3306677380  value)
	{
		___groundMask_4 = value;
	}

	inline static int32_t get_offset_of_use2D_5() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t2092006017, ___use2D_5)); }
	inline bool get_use2D_5() const { return ___use2D_5; }
	inline bool* get_address_of_use2D_5() { return &___use2D_5; }
	inline void set_use2D_5(bool value)
	{
		___use2D_5 = value;
	}

	inline static int32_t get_offset_of_useRadius_6() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t2092006017, ___useRadius_6)); }
	inline bool get_useRadius_6() const { return ___useRadius_6; }
	inline bool* get_address_of_useRadius_6() { return &___useRadius_6; }
	inline void set_useRadius_6(bool value)
	{
		___useRadius_6 = value;
	}

	inline static int32_t get_offset_of_castRadius_7() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t2092006017, ___castRadius_7)); }
	inline float get_castRadius_7() const { return ___castRadius_7; }
	inline float* get_address_of_castRadius_7() { return &___castRadius_7; }
	inline void set_castRadius_7(float value)
	{
		___castRadius_7 = value;
	}

	inline static int32_t get_offset_of_castDistance_8() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t2092006017, ___castDistance_8)); }
	inline float get_castDistance_8() const { return ___castDistance_8; }
	inline float* get_address_of_castDistance_8() { return &___castDistance_8; }
	inline void set_castDistance_8(float value)
	{
		___castDistance_8 = value;
	}

	inline static int32_t get_offset_of_castOffset_9() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t2092006017, ___castOffset_9)); }
	inline float get_castOffset_9() const { return ___castOffset_9; }
	inline float* get_address_of_castOffset_9() { return &___castOffset_9; }
	inline void set_castOffset_9(float value)
	{
		___castOffset_9 = value;
	}

	inline static int32_t get_offset_of_groundOffset_10() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t2092006017, ___groundOffset_10)); }
	inline float get_groundOffset_10() const { return ___groundOffset_10; }
	inline float* get_address_of_groundOffset_10() { return &___groundOffset_10; }
	inline void set_groundOffset_10(float value)
	{
		___groundOffset_10 = value;
	}

	inline static int32_t get_offset_of_adjustSpeed_11() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t2092006017, ___adjustSpeed_11)); }
	inline float get_adjustSpeed_11() const { return ___adjustSpeed_11; }
	inline float* get_address_of_adjustSpeed_11() { return &___adjustSpeed_11; }
	inline void set_adjustSpeed_11(float value)
	{
		___adjustSpeed_11 = value;
	}

	inline static int32_t get_offset_of_rayOrigin_12() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t2092006017, ___rayOrigin_12)); }
	inline Vector3_t2825674791  get_rayOrigin_12() const { return ___rayOrigin_12; }
	inline Vector3_t2825674791 * get_address_of_rayOrigin_12() { return &___rayOrigin_12; }
	inline void set_rayOrigin_12(Vector3_t2825674791  value)
	{
		___rayOrigin_12 = value;
	}

	inline static int32_t get_offset_of_rayDir_13() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t2092006017, ___rayDir_13)); }
	inline Vector3_t2825674791  get_rayDir_13() const { return ___rayDir_13; }
	inline Vector3_t2825674791 * get_address_of_rayDir_13() { return &___rayDir_13; }
	inline void set_rayDir_13(Vector3_t2825674791  value)
	{
		___rayDir_13 = value;
	}

	inline static int32_t get_offset_of_hitY_14() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t2092006017, ___hitY_14)); }
	inline float get_hitY_14() const { return ___hitY_14; }
	inline float* get_address_of_hitY_14() { return &___hitY_14; }
	inline void set_hitY_14(float value)
	{
		___hitY_14 = value;
	}

	inline static int32_t get_offset_of_lastHitY_15() { return static_cast<int32_t>(offsetof(SkeletonUtilityGroundConstraint_t2092006017, ___lastHitY_15)); }
	inline float get_lastHitY_15() const { return ___lastHitY_15; }
	inline float* get_address_of_lastHitY_15() { return &___lastHitY_15; }
	inline void set_lastHitY_15(float value)
	{
		___lastHitY_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONUTILITYGROUNDCONSTRAINT_T2092006017_H
#ifndef MASKABLEGRAPHIC_T2710636079_H
#define MASKABLEGRAPHIC_T2710636079_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t2710636079  : public Graphic_t366741146
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_19;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t2681358023 * ___m_MaskMaterial_20;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_t475906393 * ___m_ParentMask_21;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_22;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_23;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t3312162645 * ___m_OnCullStateChanged_24;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_25;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_26;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t3550801758* ___m_Corners_27;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_19() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2710636079, ___m_ShouldRecalculateStencil_19)); }
	inline bool get_m_ShouldRecalculateStencil_19() const { return ___m_ShouldRecalculateStencil_19; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_19() { return &___m_ShouldRecalculateStencil_19; }
	inline void set_m_ShouldRecalculateStencil_19(bool value)
	{
		___m_ShouldRecalculateStencil_19 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_20() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2710636079, ___m_MaskMaterial_20)); }
	inline Material_t2681358023 * get_m_MaskMaterial_20() const { return ___m_MaskMaterial_20; }
	inline Material_t2681358023 ** get_address_of_m_MaskMaterial_20() { return &___m_MaskMaterial_20; }
	inline void set_m_MaskMaterial_20(Material_t2681358023 * value)
	{
		___m_MaskMaterial_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskMaterial_20), value);
	}

	inline static int32_t get_offset_of_m_ParentMask_21() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2710636079, ___m_ParentMask_21)); }
	inline RectMask2D_t475906393 * get_m_ParentMask_21() const { return ___m_ParentMask_21; }
	inline RectMask2D_t475906393 ** get_address_of_m_ParentMask_21() { return &___m_ParentMask_21; }
	inline void set_m_ParentMask_21(RectMask2D_t475906393 * value)
	{
		___m_ParentMask_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_ParentMask_21), value);
	}

	inline static int32_t get_offset_of_m_Maskable_22() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2710636079, ___m_Maskable_22)); }
	inline bool get_m_Maskable_22() const { return ___m_Maskable_22; }
	inline bool* get_address_of_m_Maskable_22() { return &___m_Maskable_22; }
	inline void set_m_Maskable_22(bool value)
	{
		___m_Maskable_22 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_23() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2710636079, ___m_IncludeForMasking_23)); }
	inline bool get_m_IncludeForMasking_23() const { return ___m_IncludeForMasking_23; }
	inline bool* get_address_of_m_IncludeForMasking_23() { return &___m_IncludeForMasking_23; }
	inline void set_m_IncludeForMasking_23(bool value)
	{
		___m_IncludeForMasking_23 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_24() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2710636079, ___m_OnCullStateChanged_24)); }
	inline CullStateChangedEvent_t3312162645 * get_m_OnCullStateChanged_24() const { return ___m_OnCullStateChanged_24; }
	inline CullStateChangedEvent_t3312162645 ** get_address_of_m_OnCullStateChanged_24() { return &___m_OnCullStateChanged_24; }
	inline void set_m_OnCullStateChanged_24(CullStateChangedEvent_t3312162645 * value)
	{
		___m_OnCullStateChanged_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnCullStateChanged_24), value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_25() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2710636079, ___m_ShouldRecalculate_25)); }
	inline bool get_m_ShouldRecalculate_25() const { return ___m_ShouldRecalculate_25; }
	inline bool* get_address_of_m_ShouldRecalculate_25() { return &___m_ShouldRecalculate_25; }
	inline void set_m_ShouldRecalculate_25(bool value)
	{
		___m_ShouldRecalculate_25 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2710636079, ___m_StencilValue_26)); }
	inline int32_t get_m_StencilValue_26() const { return ___m_StencilValue_26; }
	inline int32_t* get_address_of_m_StencilValue_26() { return &___m_StencilValue_26; }
	inline void set_m_StencilValue_26(int32_t value)
	{
		___m_StencilValue_26 = value;
	}

	inline static int32_t get_offset_of_m_Corners_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t2710636079, ___m_Corners_27)); }
	inline Vector3U5BU5D_t3550801758* get_m_Corners_27() const { return ___m_Corners_27; }
	inline Vector3U5BU5D_t3550801758** get_address_of_m_Corners_27() { return &___m_Corners_27; }
	inline void set_m_Corners_27(Vector3U5BU5D_t3550801758* value)
	{
		___m_Corners_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Corners_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKABLEGRAPHIC_T2710636079_H
#ifndef SKELETONGRAPHIC_T1867541356_H
#define SKELETONGRAPHIC_T1867541356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonGraphic
struct  SkeletonGraphic_t1867541356  : public MaskableGraphic_t2710636079
{
public:
	// Spine.Unity.SkeletonDataAsset Spine.Unity.SkeletonGraphic::skeletonDataAsset
	SkeletonDataAsset_t1706375087 * ___skeletonDataAsset_28;
	// System.String Spine.Unity.SkeletonGraphic::initialSkinName
	String_t* ___initialSkinName_29;
	// System.Boolean Spine.Unity.SkeletonGraphic::initialFlipX
	bool ___initialFlipX_30;
	// System.Boolean Spine.Unity.SkeletonGraphic::initialFlipY
	bool ___initialFlipY_31;
	// System.String Spine.Unity.SkeletonGraphic::startingAnimation
	String_t* ___startingAnimation_32;
	// System.Boolean Spine.Unity.SkeletonGraphic::startingLoop
	bool ___startingLoop_33;
	// System.Single Spine.Unity.SkeletonGraphic::timeScale
	float ___timeScale_34;
	// System.Boolean Spine.Unity.SkeletonGraphic::freeze
	bool ___freeze_35;
	// System.Boolean Spine.Unity.SkeletonGraphic::unscaledTime
	bool ___unscaledTime_36;
	// UnityEngine.Texture Spine.Unity.SkeletonGraphic::overrideTexture
	Texture_t4046637001 * ___overrideTexture_37;
	// Spine.Skeleton Spine.Unity.SkeletonGraphic::skeleton
	Skeleton_t1045203634 * ___skeleton_38;
	// Spine.AnimationState Spine.Unity.SkeletonGraphic::state
	AnimationState_t2858486651 * ___state_39;
	// Spine.Unity.MeshGenerator Spine.Unity.SkeletonGraphic::meshGenerator
	MeshGenerator_t557654067 * ___meshGenerator_40;
	// Spine.Unity.DoubleBuffered`1<Spine.Unity.MeshRendererBuffers/SmartMesh> Spine.Unity.SkeletonGraphic::meshBuffers
	DoubleBuffered_1_t310388224 * ___meshBuffers_41;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.SkeletonGraphic::currentInstructions
	SkeletonRendererInstruction_t672271390 * ___currentInstructions_42;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateLocal
	UpdateBonesDelegate_t500272946 * ___UpdateLocal_43;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateWorld
	UpdateBonesDelegate_t500272946 * ___UpdateWorld_44;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonGraphic::UpdateComplete
	UpdateBonesDelegate_t500272946 * ___UpdateComplete_45;
	// Spine.Unity.MeshGeneratorDelegate Spine.Unity.SkeletonGraphic::OnPostProcessVertices
	MeshGeneratorDelegate_t2067952305 * ___OnPostProcessVertices_46;

public:
	inline static int32_t get_offset_of_skeletonDataAsset_28() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___skeletonDataAsset_28)); }
	inline SkeletonDataAsset_t1706375087 * get_skeletonDataAsset_28() const { return ___skeletonDataAsset_28; }
	inline SkeletonDataAsset_t1706375087 ** get_address_of_skeletonDataAsset_28() { return &___skeletonDataAsset_28; }
	inline void set_skeletonDataAsset_28(SkeletonDataAsset_t1706375087 * value)
	{
		___skeletonDataAsset_28 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_28), value);
	}

	inline static int32_t get_offset_of_initialSkinName_29() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___initialSkinName_29)); }
	inline String_t* get_initialSkinName_29() const { return ___initialSkinName_29; }
	inline String_t** get_address_of_initialSkinName_29() { return &___initialSkinName_29; }
	inline void set_initialSkinName_29(String_t* value)
	{
		___initialSkinName_29 = value;
		Il2CppCodeGenWriteBarrier((&___initialSkinName_29), value);
	}

	inline static int32_t get_offset_of_initialFlipX_30() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___initialFlipX_30)); }
	inline bool get_initialFlipX_30() const { return ___initialFlipX_30; }
	inline bool* get_address_of_initialFlipX_30() { return &___initialFlipX_30; }
	inline void set_initialFlipX_30(bool value)
	{
		___initialFlipX_30 = value;
	}

	inline static int32_t get_offset_of_initialFlipY_31() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___initialFlipY_31)); }
	inline bool get_initialFlipY_31() const { return ___initialFlipY_31; }
	inline bool* get_address_of_initialFlipY_31() { return &___initialFlipY_31; }
	inline void set_initialFlipY_31(bool value)
	{
		___initialFlipY_31 = value;
	}

	inline static int32_t get_offset_of_startingAnimation_32() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___startingAnimation_32)); }
	inline String_t* get_startingAnimation_32() const { return ___startingAnimation_32; }
	inline String_t** get_address_of_startingAnimation_32() { return &___startingAnimation_32; }
	inline void set_startingAnimation_32(String_t* value)
	{
		___startingAnimation_32 = value;
		Il2CppCodeGenWriteBarrier((&___startingAnimation_32), value);
	}

	inline static int32_t get_offset_of_startingLoop_33() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___startingLoop_33)); }
	inline bool get_startingLoop_33() const { return ___startingLoop_33; }
	inline bool* get_address_of_startingLoop_33() { return &___startingLoop_33; }
	inline void set_startingLoop_33(bool value)
	{
		___startingLoop_33 = value;
	}

	inline static int32_t get_offset_of_timeScale_34() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___timeScale_34)); }
	inline float get_timeScale_34() const { return ___timeScale_34; }
	inline float* get_address_of_timeScale_34() { return &___timeScale_34; }
	inline void set_timeScale_34(float value)
	{
		___timeScale_34 = value;
	}

	inline static int32_t get_offset_of_freeze_35() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___freeze_35)); }
	inline bool get_freeze_35() const { return ___freeze_35; }
	inline bool* get_address_of_freeze_35() { return &___freeze_35; }
	inline void set_freeze_35(bool value)
	{
		___freeze_35 = value;
	}

	inline static int32_t get_offset_of_unscaledTime_36() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___unscaledTime_36)); }
	inline bool get_unscaledTime_36() const { return ___unscaledTime_36; }
	inline bool* get_address_of_unscaledTime_36() { return &___unscaledTime_36; }
	inline void set_unscaledTime_36(bool value)
	{
		___unscaledTime_36 = value;
	}

	inline static int32_t get_offset_of_overrideTexture_37() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___overrideTexture_37)); }
	inline Texture_t4046637001 * get_overrideTexture_37() const { return ___overrideTexture_37; }
	inline Texture_t4046637001 ** get_address_of_overrideTexture_37() { return &___overrideTexture_37; }
	inline void set_overrideTexture_37(Texture_t4046637001 * value)
	{
		___overrideTexture_37 = value;
		Il2CppCodeGenWriteBarrier((&___overrideTexture_37), value);
	}

	inline static int32_t get_offset_of_skeleton_38() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___skeleton_38)); }
	inline Skeleton_t1045203634 * get_skeleton_38() const { return ___skeleton_38; }
	inline Skeleton_t1045203634 ** get_address_of_skeleton_38() { return &___skeleton_38; }
	inline void set_skeleton_38(Skeleton_t1045203634 * value)
	{
		___skeleton_38 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_38), value);
	}

	inline static int32_t get_offset_of_state_39() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___state_39)); }
	inline AnimationState_t2858486651 * get_state_39() const { return ___state_39; }
	inline AnimationState_t2858486651 ** get_address_of_state_39() { return &___state_39; }
	inline void set_state_39(AnimationState_t2858486651 * value)
	{
		___state_39 = value;
		Il2CppCodeGenWriteBarrier((&___state_39), value);
	}

	inline static int32_t get_offset_of_meshGenerator_40() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___meshGenerator_40)); }
	inline MeshGenerator_t557654067 * get_meshGenerator_40() const { return ___meshGenerator_40; }
	inline MeshGenerator_t557654067 ** get_address_of_meshGenerator_40() { return &___meshGenerator_40; }
	inline void set_meshGenerator_40(MeshGenerator_t557654067 * value)
	{
		___meshGenerator_40 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_40), value);
	}

	inline static int32_t get_offset_of_meshBuffers_41() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___meshBuffers_41)); }
	inline DoubleBuffered_1_t310388224 * get_meshBuffers_41() const { return ___meshBuffers_41; }
	inline DoubleBuffered_1_t310388224 ** get_address_of_meshBuffers_41() { return &___meshBuffers_41; }
	inline void set_meshBuffers_41(DoubleBuffered_1_t310388224 * value)
	{
		___meshBuffers_41 = value;
		Il2CppCodeGenWriteBarrier((&___meshBuffers_41), value);
	}

	inline static int32_t get_offset_of_currentInstructions_42() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___currentInstructions_42)); }
	inline SkeletonRendererInstruction_t672271390 * get_currentInstructions_42() const { return ___currentInstructions_42; }
	inline SkeletonRendererInstruction_t672271390 ** get_address_of_currentInstructions_42() { return &___currentInstructions_42; }
	inline void set_currentInstructions_42(SkeletonRendererInstruction_t672271390 * value)
	{
		___currentInstructions_42 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_42), value);
	}

	inline static int32_t get_offset_of_UpdateLocal_43() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___UpdateLocal_43)); }
	inline UpdateBonesDelegate_t500272946 * get_UpdateLocal_43() const { return ___UpdateLocal_43; }
	inline UpdateBonesDelegate_t500272946 ** get_address_of_UpdateLocal_43() { return &___UpdateLocal_43; }
	inline void set_UpdateLocal_43(UpdateBonesDelegate_t500272946 * value)
	{
		___UpdateLocal_43 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateLocal_43), value);
	}

	inline static int32_t get_offset_of_UpdateWorld_44() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___UpdateWorld_44)); }
	inline UpdateBonesDelegate_t500272946 * get_UpdateWorld_44() const { return ___UpdateWorld_44; }
	inline UpdateBonesDelegate_t500272946 ** get_address_of_UpdateWorld_44() { return &___UpdateWorld_44; }
	inline void set_UpdateWorld_44(UpdateBonesDelegate_t500272946 * value)
	{
		___UpdateWorld_44 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateWorld_44), value);
	}

	inline static int32_t get_offset_of_UpdateComplete_45() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___UpdateComplete_45)); }
	inline UpdateBonesDelegate_t500272946 * get_UpdateComplete_45() const { return ___UpdateComplete_45; }
	inline UpdateBonesDelegate_t500272946 ** get_address_of_UpdateComplete_45() { return &___UpdateComplete_45; }
	inline void set_UpdateComplete_45(UpdateBonesDelegate_t500272946 * value)
	{
		___UpdateComplete_45 = value;
		Il2CppCodeGenWriteBarrier((&___UpdateComplete_45), value);
	}

	inline static int32_t get_offset_of_OnPostProcessVertices_46() { return static_cast<int32_t>(offsetof(SkeletonGraphic_t1867541356, ___OnPostProcessVertices_46)); }
	inline MeshGeneratorDelegate_t2067952305 * get_OnPostProcessVertices_46() const { return ___OnPostProcessVertices_46; }
	inline MeshGeneratorDelegate_t2067952305 ** get_address_of_OnPostProcessVertices_46() { return &___OnPostProcessVertices_46; }
	inline void set_OnPostProcessVertices_46(MeshGeneratorDelegate_t2067952305 * value)
	{
		___OnPostProcessVertices_46 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostProcessVertices_46), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONGRAPHIC_T1867541356_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (MathUtils_t2185402769), -1, sizeof(MathUtils_t2185402769_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1900[12] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	MathUtils_t2185402769_StaticFields::get_offset_of_sin_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (PathConstraint_t1854663405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1901[17] = 
{
	0,
	0,
	0,
	0,
	PathConstraint_t1854663405::get_offset_of_data_4(),
	PathConstraint_t1854663405::get_offset_of_bones_5(),
	PathConstraint_t1854663405::get_offset_of_target_6(),
	PathConstraint_t1854663405::get_offset_of_position_7(),
	PathConstraint_t1854663405::get_offset_of_spacing_8(),
	PathConstraint_t1854663405::get_offset_of_rotateMix_9(),
	PathConstraint_t1854663405::get_offset_of_translateMix_10(),
	PathConstraint_t1854663405::get_offset_of_spaces_11(),
	PathConstraint_t1854663405::get_offset_of_positions_12(),
	PathConstraint_t1854663405::get_offset_of_world_13(),
	PathConstraint_t1854663405::get_offset_of_curves_14(),
	PathConstraint_t1854663405::get_offset_of_lengths_15(),
	PathConstraint_t1854663405::get_offset_of_segments_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (PathConstraintData_t3185350348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[12] = 
{
	PathConstraintData_t3185350348::get_offset_of_name_0(),
	PathConstraintData_t3185350348::get_offset_of_order_1(),
	PathConstraintData_t3185350348::get_offset_of_bones_2(),
	PathConstraintData_t3185350348::get_offset_of_target_3(),
	PathConstraintData_t3185350348::get_offset_of_positionMode_4(),
	PathConstraintData_t3185350348::get_offset_of_spacingMode_5(),
	PathConstraintData_t3185350348::get_offset_of_rotateMode_6(),
	PathConstraintData_t3185350348::get_offset_of_offsetRotation_7(),
	PathConstraintData_t3185350348::get_offset_of_position_8(),
	PathConstraintData_t3185350348::get_offset_of_spacing_9(),
	PathConstraintData_t3185350348::get_offset_of_rotateMix_10(),
	PathConstraintData_t3185350348::get_offset_of_translateMix_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (PositionMode_t2631516897)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1903[3] = 
{
	PositionMode_t2631516897::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (SpacingMode_t2585861093)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1904[4] = 
{
	SpacingMode_t2585861093::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (RotateMode_t1288973915)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1905[4] = 
{
	RotateMode_t1288973915::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (Skeleton_t1045203634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[19] = 
{
	Skeleton_t1045203634::get_offset_of_data_0(),
	Skeleton_t1045203634::get_offset_of_bones_1(),
	Skeleton_t1045203634::get_offset_of_slots_2(),
	Skeleton_t1045203634::get_offset_of_drawOrder_3(),
	Skeleton_t1045203634::get_offset_of_ikConstraints_4(),
	Skeleton_t1045203634::get_offset_of_transformConstraints_5(),
	Skeleton_t1045203634::get_offset_of_pathConstraints_6(),
	Skeleton_t1045203634::get_offset_of_updateCache_7(),
	Skeleton_t1045203634::get_offset_of_updateCacheReset_8(),
	Skeleton_t1045203634::get_offset_of_skin_9(),
	Skeleton_t1045203634::get_offset_of_r_10(),
	Skeleton_t1045203634::get_offset_of_g_11(),
	Skeleton_t1045203634::get_offset_of_b_12(),
	Skeleton_t1045203634::get_offset_of_a_13(),
	Skeleton_t1045203634::get_offset_of_time_14(),
	Skeleton_t1045203634::get_offset_of_flipX_15(),
	Skeleton_t1045203634::get_offset_of_flipY_16(),
	Skeleton_t1045203634::get_offset_of_x_17(),
	Skeleton_t1045203634::get_offset_of_y_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (SkeletonBinary_t698135267), -1, sizeof(SkeletonBinary_t698135267_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1907[18] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	SkeletonBinary_t698135267::get_offset_of_U3CScaleU3Ek__BackingField_13(),
	SkeletonBinary_t698135267::get_offset_of_attachmentLoader_14(),
	SkeletonBinary_t698135267::get_offset_of_buffer_15(),
	SkeletonBinary_t698135267::get_offset_of_linkedMeshes_16(),
	SkeletonBinary_t698135267_StaticFields::get_offset_of_TransformModeValues_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (Vertices_t2244423585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[2] = 
{
	Vertices_t2244423585::get_offset_of_bones_0(),
	Vertices_t2244423585::get_offset_of_vertices_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (SkeletonBounds_t948042636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[7] = 
{
	SkeletonBounds_t948042636::get_offset_of_polygonPool_0(),
	SkeletonBounds_t948042636::get_offset_of_minX_1(),
	SkeletonBounds_t948042636::get_offset_of_minY_2(),
	SkeletonBounds_t948042636::get_offset_of_maxX_3(),
	SkeletonBounds_t948042636::get_offset_of_maxY_4(),
	SkeletonBounds_t948042636::get_offset_of_U3CBoundingBoxesU3Ek__BackingField_5(),
	SkeletonBounds_t948042636::get_offset_of_U3CPolygonsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (Polygon_t3410609907), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1910[2] = 
{
	Polygon_t3410609907::get_offset_of_U3CVerticesU3Ek__BackingField_0(),
	Polygon_t3410609907::get_offset_of_U3CCountU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (SkeletonClipping_t2504359873), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1911[9] = 
{
	SkeletonClipping_t2504359873::get_offset_of_triangulator_0(),
	SkeletonClipping_t2504359873::get_offset_of_clippingPolygon_1(),
	SkeletonClipping_t2504359873::get_offset_of_clipOutput_2(),
	SkeletonClipping_t2504359873::get_offset_of_clippedVertices_3(),
	SkeletonClipping_t2504359873::get_offset_of_clippedTriangles_4(),
	SkeletonClipping_t2504359873::get_offset_of_clippedUVs_5(),
	SkeletonClipping_t2504359873::get_offset_of_scratch_6(),
	SkeletonClipping_t2504359873::get_offset_of_clipAttachment_7(),
	SkeletonClipping_t2504359873::get_offset_of_clippingPolygons_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (SkeletonData_t2665604974), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[16] = 
{
	SkeletonData_t2665604974::get_offset_of_name_0(),
	SkeletonData_t2665604974::get_offset_of_bones_1(),
	SkeletonData_t2665604974::get_offset_of_slots_2(),
	SkeletonData_t2665604974::get_offset_of_skins_3(),
	SkeletonData_t2665604974::get_offset_of_defaultSkin_4(),
	SkeletonData_t2665604974::get_offset_of_events_5(),
	SkeletonData_t2665604974::get_offset_of_animations_6(),
	SkeletonData_t2665604974::get_offset_of_ikConstraints_7(),
	SkeletonData_t2665604974::get_offset_of_transformConstraints_8(),
	SkeletonData_t2665604974::get_offset_of_pathConstraints_9(),
	SkeletonData_t2665604974::get_offset_of_width_10(),
	SkeletonData_t2665604974::get_offset_of_height_11(),
	SkeletonData_t2665604974::get_offset_of_version_12(),
	SkeletonData_t2665604974::get_offset_of_hash_13(),
	SkeletonData_t2665604974::get_offset_of_fps_14(),
	SkeletonData_t2665604974::get_offset_of_imagesPath_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (SkeletonJson_t3645594475), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[3] = 
{
	SkeletonJson_t3645594475::get_offset_of_U3CScaleU3Ek__BackingField_0(),
	SkeletonJson_t3645594475::get_offset_of_attachmentLoader_1(),
	SkeletonJson_t3645594475::get_offset_of_linkedMeshes_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (LinkedMesh_t3424637972), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1914[4] = 
{
	LinkedMesh_t3424637972::get_offset_of_parent_0(),
	LinkedMesh_t3424637972::get_offset_of_skin_1(),
	LinkedMesh_t3424637972::get_offset_of_slotIndex_2(),
	LinkedMesh_t3424637972::get_offset_of_mesh_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (Skin_t1695237131), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[2] = 
{
	Skin_t1695237131::get_offset_of_name_0(),
	Skin_t1695237131::get_offset_of_attachments_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (AttachmentKeyTuple_t2283295499)+ sizeof (RuntimeObject), sizeof(AttachmentKeyTuple_t2283295499_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1916[3] = 
{
	AttachmentKeyTuple_t2283295499::get_offset_of_slotIndex_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttachmentKeyTuple_t2283295499::get_offset_of_name_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttachmentKeyTuple_t2283295499::get_offset_of_nameHashCode_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (AttachmentKeyTupleComparer_t2248732946), -1, sizeof(AttachmentKeyTupleComparer_t2248732946_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1917[1] = 
{
	AttachmentKeyTupleComparer_t2248732946_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (Slot_t1804116343), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[13] = 
{
	Slot_t1804116343::get_offset_of_data_0(),
	Slot_t1804116343::get_offset_of_bone_1(),
	Slot_t1804116343::get_offset_of_r_2(),
	Slot_t1804116343::get_offset_of_g_3(),
	Slot_t1804116343::get_offset_of_b_4(),
	Slot_t1804116343::get_offset_of_a_5(),
	Slot_t1804116343::get_offset_of_r2_6(),
	Slot_t1804116343::get_offset_of_g2_7(),
	Slot_t1804116343::get_offset_of_b2_8(),
	Slot_t1804116343::get_offset_of_hasSecondColor_9(),
	Slot_t1804116343::get_offset_of_attachment_10(),
	Slot_t1804116343::get_offset_of_attachmentTime_11(),
	Slot_t1804116343::get_offset_of_attachmentVertices_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (SlotData_t587781850), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1919[13] = 
{
	SlotData_t587781850::get_offset_of_index_0(),
	SlotData_t587781850::get_offset_of_name_1(),
	SlotData_t587781850::get_offset_of_boneData_2(),
	SlotData_t587781850::get_offset_of_r_3(),
	SlotData_t587781850::get_offset_of_g_4(),
	SlotData_t587781850::get_offset_of_b_5(),
	SlotData_t587781850::get_offset_of_a_6(),
	SlotData_t587781850::get_offset_of_r2_7(),
	SlotData_t587781850::get_offset_of_g2_8(),
	SlotData_t587781850::get_offset_of_b2_9(),
	SlotData_t587781850::get_offset_of_hasSecondColor_10(),
	SlotData_t587781850::get_offset_of_attachmentName_11(),
	SlotData_t587781850::get_offset_of_blendMode_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (TransformConstraint_t509539004), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[7] = 
{
	TransformConstraint_t509539004::get_offset_of_data_0(),
	TransformConstraint_t509539004::get_offset_of_bones_1(),
	TransformConstraint_t509539004::get_offset_of_target_2(),
	TransformConstraint_t509539004::get_offset_of_rotateMix_3(),
	TransformConstraint_t509539004::get_offset_of_translateMix_4(),
	TransformConstraint_t509539004::get_offset_of_scaleMix_5(),
	TransformConstraint_t509539004::get_offset_of_shearMix_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (TransformConstraintData_t1381040281), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[16] = 
{
	TransformConstraintData_t1381040281::get_offset_of_name_0(),
	TransformConstraintData_t1381040281::get_offset_of_order_1(),
	TransformConstraintData_t1381040281::get_offset_of_bones_2(),
	TransformConstraintData_t1381040281::get_offset_of_target_3(),
	TransformConstraintData_t1381040281::get_offset_of_rotateMix_4(),
	TransformConstraintData_t1381040281::get_offset_of_translateMix_5(),
	TransformConstraintData_t1381040281::get_offset_of_scaleMix_6(),
	TransformConstraintData_t1381040281::get_offset_of_shearMix_7(),
	TransformConstraintData_t1381040281::get_offset_of_offsetRotation_8(),
	TransformConstraintData_t1381040281::get_offset_of_offsetX_9(),
	TransformConstraintData_t1381040281::get_offset_of_offsetY_10(),
	TransformConstraintData_t1381040281::get_offset_of_offsetScaleX_11(),
	TransformConstraintData_t1381040281::get_offset_of_offsetScaleY_12(),
	TransformConstraintData_t1381040281::get_offset_of_offsetShearY_13(),
	TransformConstraintData_t1381040281::get_offset_of_relative_14(),
	TransformConstraintData_t1381040281::get_offset_of_local_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (Triangulator_t1509316840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1922[7] = 
{
	Triangulator_t1509316840::get_offset_of_convexPolygons_0(),
	Triangulator_t1509316840::get_offset_of_convexPolygonsIndices_1(),
	Triangulator_t1509316840::get_offset_of_indicesArray_2(),
	Triangulator_t1509316840::get_offset_of_isConcaveArray_3(),
	Triangulator_t1509316840::get_offset_of_triangles_4(),
	Triangulator_t1509316840::get_offset_of_polygonPool_5(),
	Triangulator_t1509316840::get_offset_of_polygonIndicesPool_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (AtlasAsset_t2525633250), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1923[3] = 
{
	AtlasAsset_t2525633250::get_offset_of_atlasFile_2(),
	AtlasAsset_t2525633250::get_offset_of_materials_3(),
	AtlasAsset_t2525633250::get_offset_of_atlas_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (MaterialsTextureLoader_t3752126477), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1924[1] = 
{
	MaterialsTextureLoader_t3752126477::get_offset_of_atlasAsset_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (SkeletonDataAsset_t1706375087), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1925[10] = 
{
	SkeletonDataAsset_t1706375087::get_offset_of_atlasAssets_2(),
	SkeletonDataAsset_t1706375087::get_offset_of_scale_3(),
	SkeletonDataAsset_t1706375087::get_offset_of_skeletonJSON_4(),
	SkeletonDataAsset_t1706375087::get_offset_of_fromAnimation_5(),
	SkeletonDataAsset_t1706375087::get_offset_of_toAnimation_6(),
	SkeletonDataAsset_t1706375087::get_offset_of_duration_7(),
	SkeletonDataAsset_t1706375087::get_offset_of_defaultMix_8(),
	SkeletonDataAsset_t1706375087::get_offset_of_controller_9(),
	SkeletonDataAsset_t1706375087::get_offset_of_skeletonData_10(),
	SkeletonDataAsset_t1706375087::get_offset_of_stateData_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (BoneFollower_t3311442535), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1926[11] = 
{
	BoneFollower_t3311442535::get_offset_of_skeletonRenderer_2(),
	BoneFollower_t3311442535::get_offset_of_boneName_3(),
	BoneFollower_t3311442535::get_offset_of_followZPosition_4(),
	BoneFollower_t3311442535::get_offset_of_followBoneRotation_5(),
	BoneFollower_t3311442535::get_offset_of_followSkeletonFlip_6(),
	BoneFollower_t3311442535::get_offset_of_followLocalScale_7(),
	BoneFollower_t3311442535::get_offset_of_initializeOnAwake_8(),
	BoneFollower_t3311442535::get_offset_of_valid_9(),
	BoneFollower_t3311442535::get_offset_of_bone_10(),
	BoneFollower_t3311442535::get_offset_of_skeletonTransform_11(),
	BoneFollower_t3311442535::get_offset_of_skeletonTransformIsParent_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (UpdateBonesDelegate_t500272946), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (SpineMesh_t4270445991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (SubmeshInstruction_t3765917328)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[10] = 
{
	SubmeshInstruction_t3765917328::get_offset_of_skeleton_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t3765917328::get_offset_of_startSlot_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t3765917328::get_offset_of_endSlot_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t3765917328::get_offset_of_material_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t3765917328::get_offset_of_forceSeparate_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t3765917328::get_offset_of_preActiveClippingSlotSource_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t3765917328::get_offset_of_rawTriangleCount_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t3765917328::get_offset_of_rawVertexCount_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t3765917328::get_offset_of_rawFirstVertexIndex_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SubmeshInstruction_t3765917328::get_offset_of_hasClipping_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { sizeof (MeshGeneratorDelegate_t2067952305), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { sizeof (MeshGeneratorBuffers_t1203282268)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1936[5] = 
{
	MeshGeneratorBuffers_t1203282268::get_offset_of_vertexCount_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_t1203282268::get_offset_of_vertexBuffer_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_t1203282268::get_offset_of_uvBuffer_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_t1203282268::get_offset_of_colorBuffer_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MeshGeneratorBuffers_t1203282268::get_offset_of_meshGenerator_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { sizeof (MeshGenerator_t557654067), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1937[19] = 
{
	MeshGenerator_t557654067::get_offset_of_settings_0(),
	0,
	0,
	MeshGenerator_t557654067::get_offset_of_vertexBuffer_3(),
	MeshGenerator_t557654067::get_offset_of_uvBuffer_4(),
	MeshGenerator_t557654067::get_offset_of_colorBuffer_5(),
	MeshGenerator_t557654067::get_offset_of_submeshes_6(),
	MeshGenerator_t557654067::get_offset_of_meshBoundsMin_7(),
	MeshGenerator_t557654067::get_offset_of_meshBoundsMax_8(),
	MeshGenerator_t557654067::get_offset_of_meshBoundsThickness_9(),
	MeshGenerator_t557654067::get_offset_of_submeshIndex_10(),
	MeshGenerator_t557654067::get_offset_of_clipper_11(),
	MeshGenerator_t557654067::get_offset_of_tempVerts_12(),
	MeshGenerator_t557654067::get_offset_of_regionTriangles_13(),
	MeshGenerator_t557654067::get_offset_of_normals_14(),
	MeshGenerator_t557654067::get_offset_of_tangents_15(),
	MeshGenerator_t557654067::get_offset_of_tempTanBuffer_16(),
	MeshGenerator_t557654067::get_offset_of_uv2_17(),
	MeshGenerator_t557654067::get_offset_of_uv3_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (Settings_t1339330787)+ sizeof (RuntimeObject), sizeof(Settings_t1339330787_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1938[7] = 
{
	Settings_t1339330787::get_offset_of_useClipping_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1339330787::get_offset_of_zSpacing_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1339330787::get_offset_of_pmaVertexColors_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1339330787::get_offset_of_tintBlack_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1339330787::get_offset_of_calculateTangents_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1339330787::get_offset_of_addNormals_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1339330787::get_offset_of_immutableTriangles_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (MeshRendererBuffers_t384051837), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[3] = 
{
	MeshRendererBuffers_t384051837::get_offset_of_doubleBufferedMesh_0(),
	MeshRendererBuffers_t384051837::get_offset_of_submeshMaterials_1(),
	MeshRendererBuffers_t384051837::get_offset_of_sharedMaterials_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (SmartMesh_t2574154657), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[2] = 
{
	SmartMesh_t2574154657::get_offset_of_mesh_0(),
	SmartMesh_t2574154657::get_offset_of_instructionUsed_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (SkeletonRendererInstruction_t672271390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[5] = 
{
	SkeletonRendererInstruction_t672271390::get_offset_of_immutableTriangles_0(),
	SkeletonRendererInstruction_t672271390::get_offset_of_submeshInstructions_1(),
	SkeletonRendererInstruction_t672271390::get_offset_of_hasActiveClipping_2(),
	SkeletonRendererInstruction_t672271390::get_offset_of_rawVertexCount_3(),
	SkeletonRendererInstruction_t672271390::get_offset_of_attachments_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (AttachmentRegionExtensions_t3872463489), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (AtlasUtilities_t3407187445), -1, sizeof(AtlasUtilities_t3407187445_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1943[6] = 
{
	0,
	0,
	0,
	0,
	AtlasUtilities_t3407187445_StaticFields::get_offset_of_CachedRegionTextures_4(),
	AtlasUtilities_t3407187445_StaticFields::get_offset_of_CachedRegionTexturesList_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (SkinUtilities_t4185041917), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (AttachmentCloneExtensions_t2643942632), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (BoundingBoxFollower_t836972383), -1, sizeof(BoundingBoxFollower_t836972383_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1946[11] = 
{
	BoundingBoxFollower_t836972383_StaticFields::get_offset_of_DebugMessages_2(),
	BoundingBoxFollower_t836972383::get_offset_of_skeletonRenderer_3(),
	BoundingBoxFollower_t836972383::get_offset_of_slotName_4(),
	BoundingBoxFollower_t836972383::get_offset_of_isTrigger_5(),
	BoundingBoxFollower_t836972383::get_offset_of_clearStateOnDisable_6(),
	BoundingBoxFollower_t836972383::get_offset_of_slot_7(),
	BoundingBoxFollower_t836972383::get_offset_of_currentAttachment_8(),
	BoundingBoxFollower_t836972383::get_offset_of_currentAttachmentName_9(),
	BoundingBoxFollower_t836972383::get_offset_of_currentCollider_10(),
	BoundingBoxFollower_t836972383::get_offset_of_colliderTable_11(),
	BoundingBoxFollower_t836972383::get_offset_of_nameTable_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (SkeletonRendererCustomMaterials_t2116292039), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1947[3] = 
{
	SkeletonRendererCustomMaterials_t2116292039::get_offset_of_skeletonRenderer_2(),
	SkeletonRendererCustomMaterials_t2116292039::get_offset_of_customSlotMaterials_3(),
	SkeletonRendererCustomMaterials_t2116292039::get_offset_of_customMaterialOverrides_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (SlotMaterialOverride_t3080213539)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1948[3] = 
{
	SlotMaterialOverride_t3080213539::get_offset_of_overrideDisabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SlotMaterialOverride_t3080213539::get_offset_of_slotName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	SlotMaterialOverride_t3080213539::get_offset_of_material_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (AtlasMaterialOverride_t4245281532)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1949[3] = 
{
	AtlasMaterialOverride_t4245281532::get_offset_of_overrideDisabled_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AtlasMaterialOverride_t4245281532::get_offset_of_originalMaterial_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AtlasMaterialOverride_t4245281532::get_offset_of_replacementMaterial_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1950 = { sizeof (SkeletonGhost_t2198715580), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1950[19] = 
{
	0,
	0,
	SkeletonGhost_t2198715580::get_offset_of_ghostingEnabled_4(),
	SkeletonGhost_t2198715580::get_offset_of_spawnRate_5(),
	SkeletonGhost_t2198715580::get_offset_of_color_6(),
	SkeletonGhost_t2198715580::get_offset_of_additive_7(),
	SkeletonGhost_t2198715580::get_offset_of_maximumGhosts_8(),
	SkeletonGhost_t2198715580::get_offset_of_fadeSpeed_9(),
	SkeletonGhost_t2198715580::get_offset_of_ghostShader_10(),
	SkeletonGhost_t2198715580::get_offset_of_textureFade_11(),
	SkeletonGhost_t2198715580::get_offset_of_sortWithDistanceOnly_12(),
	SkeletonGhost_t2198715580::get_offset_of_zOffset_13(),
	SkeletonGhost_t2198715580::get_offset_of_nextSpawnTime_14(),
	SkeletonGhost_t2198715580::get_offset_of_pool_15(),
	SkeletonGhost_t2198715580::get_offset_of_poolIndex_16(),
	SkeletonGhost_t2198715580::get_offset_of_skeletonRenderer_17(),
	SkeletonGhost_t2198715580::get_offset_of_meshRenderer_18(),
	SkeletonGhost_t2198715580::get_offset_of_meshFilter_19(),
	SkeletonGhost_t2198715580::get_offset_of_materialTable_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1951 = { sizeof (SkeletonGhostRenderer_t108436892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1951[5] = 
{
	SkeletonGhostRenderer_t108436892::get_offset_of_fadeSpeed_2(),
	SkeletonGhostRenderer_t108436892::get_offset_of_colors_3(),
	SkeletonGhostRenderer_t108436892::get_offset_of_black_4(),
	SkeletonGhostRenderer_t108436892::get_offset_of_meshFilter_5(),
	SkeletonGhostRenderer_t108436892::get_offset_of_meshRenderer_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1952 = { sizeof (U3CFadeU3Ec__Iterator0_t2035475871), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1952[7] = 
{
	U3CFadeU3Ec__Iterator0_t2035475871::get_offset_of_U3CtU3E__1_0(),
	U3CFadeU3Ec__Iterator0_t2035475871::get_offset_of_U3CbreakoutU3E__2_1(),
	U3CFadeU3Ec__Iterator0_t2035475871::get_offset_of_U3CcU3E__3_2(),
	U3CFadeU3Ec__Iterator0_t2035475871::get_offset_of_U24this_3(),
	U3CFadeU3Ec__Iterator0_t2035475871::get_offset_of_U24current_4(),
	U3CFadeU3Ec__Iterator0_t2035475871::get_offset_of_U24disposing_5(),
	U3CFadeU3Ec__Iterator0_t2035475871::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1953 = { sizeof (U3CFadeAdditiveU3Ec__Iterator1_t1481999734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1953[8] = 
{
	U3CFadeAdditiveU3Ec__Iterator1_t1481999734::get_offset_of_U3CblackU3E__0_0(),
	U3CFadeAdditiveU3Ec__Iterator1_t1481999734::get_offset_of_U3CtU3E__1_1(),
	U3CFadeAdditiveU3Ec__Iterator1_t1481999734::get_offset_of_U3CbreakoutU3E__2_2(),
	U3CFadeAdditiveU3Ec__Iterator1_t1481999734::get_offset_of_U3CcU3E__3_3(),
	U3CFadeAdditiveU3Ec__Iterator1_t1481999734::get_offset_of_U24this_4(),
	U3CFadeAdditiveU3Ec__Iterator1_t1481999734::get_offset_of_U24current_5(),
	U3CFadeAdditiveU3Ec__Iterator1_t1481999734::get_offset_of_U24disposing_6(),
	U3CFadeAdditiveU3Ec__Iterator1_t1481999734::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1954 = { sizeof (SkeletonRagdoll_t1253535559), -1, sizeof(SkeletonRagdoll_t1253535559_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1954[23] = 
{
	SkeletonRagdoll_t1253535559_StaticFields::get_offset_of_parentSpaceHelper_2(),
	SkeletonRagdoll_t1253535559::get_offset_of_startingBoneName_3(),
	SkeletonRagdoll_t1253535559::get_offset_of_stopBoneNames_4(),
	SkeletonRagdoll_t1253535559::get_offset_of_applyOnStart_5(),
	SkeletonRagdoll_t1253535559::get_offset_of_disableIK_6(),
	SkeletonRagdoll_t1253535559::get_offset_of_disableOtherConstraints_7(),
	SkeletonRagdoll_t1253535559::get_offset_of_pinStartBone_8(),
	SkeletonRagdoll_t1253535559::get_offset_of_enableJointCollision_9(),
	SkeletonRagdoll_t1253535559::get_offset_of_useGravity_10(),
	SkeletonRagdoll_t1253535559::get_offset_of_thickness_11(),
	SkeletonRagdoll_t1253535559::get_offset_of_rotationLimit_12(),
	SkeletonRagdoll_t1253535559::get_offset_of_rootMass_13(),
	SkeletonRagdoll_t1253535559::get_offset_of_massFalloffFactor_14(),
	SkeletonRagdoll_t1253535559::get_offset_of_colliderLayer_15(),
	SkeletonRagdoll_t1253535559::get_offset_of_mix_16(),
	SkeletonRagdoll_t1253535559::get_offset_of_targetSkeletonComponent_17(),
	SkeletonRagdoll_t1253535559::get_offset_of_skeleton_18(),
	SkeletonRagdoll_t1253535559::get_offset_of_boneTable_19(),
	SkeletonRagdoll_t1253535559::get_offset_of_ragdollRoot_20(),
	SkeletonRagdoll_t1253535559::get_offset_of_U3CRootRigidbodyU3Ek__BackingField_21(),
	SkeletonRagdoll_t1253535559::get_offset_of_U3CStartingBoneU3Ek__BackingField_22(),
	SkeletonRagdoll_t1253535559::get_offset_of_rootOffset_23(),
	SkeletonRagdoll_t1253535559::get_offset_of_isActive_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1955 = { sizeof (LayerFieldAttribute_t2281748996), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1956 = { sizeof (U3CStartU3Ec__Iterator0_t2092556417), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1956[4] = 
{
	U3CStartU3Ec__Iterator0_t2092556417::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t2092556417::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t2092556417::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t2092556417::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1957 = { sizeof (U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1957[8] = 
{
	U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827::get_offset_of_U3CstartTimeU3E__0_0(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827::get_offset_of_U3CstartMixU3E__0_1(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827::get_offset_of_target_2(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827::get_offset_of_duration_3(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827::get_offset_of_U24this_4(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827::get_offset_of_U24current_5(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827::get_offset_of_U24disposing_6(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t447401827::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1958 = { sizeof (SkeletonRagdoll2D_t3940222091), -1, sizeof(SkeletonRagdoll2D_t3940222091_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1958[22] = 
{
	SkeletonRagdoll2D_t3940222091_StaticFields::get_offset_of_parentSpaceHelper_2(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_startingBoneName_3(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_stopBoneNames_4(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_applyOnStart_5(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_disableIK_6(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_disableOtherConstraints_7(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_pinStartBone_8(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_gravityScale_9(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_thickness_10(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_rotationLimit_11(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_rootMass_12(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_massFalloffFactor_13(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_colliderLayer_14(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_mix_15(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_targetSkeletonComponent_16(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_skeleton_17(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_boneTable_18(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_ragdollRoot_19(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_U3CRootRigidbodyU3Ek__BackingField_20(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_U3CStartingBoneU3Ek__BackingField_21(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_rootOffset_22(),
	SkeletonRagdoll2D_t3940222091::get_offset_of_isActive_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1959 = { sizeof (U3CStartU3Ec__Iterator0_t534358340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1959[4] = 
{
	U3CStartU3Ec__Iterator0_t534358340::get_offset_of_U24this_0(),
	U3CStartU3Ec__Iterator0_t534358340::get_offset_of_U24current_1(),
	U3CStartU3Ec__Iterator0_t534358340::get_offset_of_U24disposing_2(),
	U3CStartU3Ec__Iterator0_t534358340::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1960 = { sizeof (U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1960[8] = 
{
	U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462::get_offset_of_U3CstartTimeU3E__0_0(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462::get_offset_of_U3CstartMixU3E__0_1(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462::get_offset_of_target_2(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462::get_offset_of_duration_3(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462::get_offset_of_U24this_4(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462::get_offset_of_U24current_5(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462::get_offset_of_U24disposing_6(),
	U3CSmoothMixCoroutineU3Ec__Iterator1_t4243939462::get_offset_of_U24PC_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1961 = { sizeof (BoneFollowerGraphic_t865397923), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1961[11] = 
{
	BoneFollowerGraphic_t865397923::get_offset_of_skeletonGraphic_2(),
	BoneFollowerGraphic_t865397923::get_offset_of_initializeOnAwake_3(),
	BoneFollowerGraphic_t865397923::get_offset_of_boneName_4(),
	BoneFollowerGraphic_t865397923::get_offset_of_followBoneRotation_5(),
	BoneFollowerGraphic_t865397923::get_offset_of_followSkeletonFlip_6(),
	BoneFollowerGraphic_t865397923::get_offset_of_followLocalScale_7(),
	BoneFollowerGraphic_t865397923::get_offset_of_followZPosition_8(),
	BoneFollowerGraphic_t865397923::get_offset_of_bone_9(),
	BoneFollowerGraphic_t865397923::get_offset_of_skeletonTransform_10(),
	BoneFollowerGraphic_t865397923::get_offset_of_skeletonTransformIsParent_11(),
	BoneFollowerGraphic_t865397923::get_offset_of_valid_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1962 = { sizeof (SkeletonGraphic_t1867541356), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1962[19] = 
{
	SkeletonGraphic_t1867541356::get_offset_of_skeletonDataAsset_28(),
	SkeletonGraphic_t1867541356::get_offset_of_initialSkinName_29(),
	SkeletonGraphic_t1867541356::get_offset_of_initialFlipX_30(),
	SkeletonGraphic_t1867541356::get_offset_of_initialFlipY_31(),
	SkeletonGraphic_t1867541356::get_offset_of_startingAnimation_32(),
	SkeletonGraphic_t1867541356::get_offset_of_startingLoop_33(),
	SkeletonGraphic_t1867541356::get_offset_of_timeScale_34(),
	SkeletonGraphic_t1867541356::get_offset_of_freeze_35(),
	SkeletonGraphic_t1867541356::get_offset_of_unscaledTime_36(),
	SkeletonGraphic_t1867541356::get_offset_of_overrideTexture_37(),
	SkeletonGraphic_t1867541356::get_offset_of_skeleton_38(),
	SkeletonGraphic_t1867541356::get_offset_of_state_39(),
	SkeletonGraphic_t1867541356::get_offset_of_meshGenerator_40(),
	SkeletonGraphic_t1867541356::get_offset_of_meshBuffers_41(),
	SkeletonGraphic_t1867541356::get_offset_of_currentInstructions_42(),
	SkeletonGraphic_t1867541356::get_offset_of_UpdateLocal_43(),
	SkeletonGraphic_t1867541356::get_offset_of_UpdateWorld_44(),
	SkeletonGraphic_t1867541356::get_offset_of_UpdateComplete_45(),
	SkeletonGraphic_t1867541356::get_offset_of_OnPostProcessVertices_46(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1963 = { sizeof (SkeletonPartsRenderer_t3336947722), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1963[5] = 
{
	SkeletonPartsRenderer_t3336947722::get_offset_of_meshGenerator_2(),
	SkeletonPartsRenderer_t3336947722::get_offset_of_meshRenderer_3(),
	SkeletonPartsRenderer_t3336947722::get_offset_of_meshFilter_4(),
	SkeletonPartsRenderer_t3336947722::get_offset_of_buffers_5(),
	SkeletonPartsRenderer_t3336947722::get_offset_of_currentInstructions_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1964 = { sizeof (SkeletonRenderSeparator_t3391021243), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1964[7] = 
{
	0,
	SkeletonRenderSeparator_t3391021243::get_offset_of_skeletonRenderer_3(),
	SkeletonRenderSeparator_t3391021243::get_offset_of_mainMeshRenderer_4(),
	SkeletonRenderSeparator_t3391021243::get_offset_of_copyPropertyBlock_5(),
	SkeletonRenderSeparator_t3391021243::get_offset_of_copyMeshRendererFlags_6(),
	SkeletonRenderSeparator_t3391021243::get_offset_of_partsRenderers_7(),
	SkeletonRenderSeparator_t3391021243::get_offset_of_copiedBlock_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1965 = { sizeof (SkeletonUtilityEyeConstraint_t66635751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1965[7] = 
{
	SkeletonUtilityEyeConstraint_t66635751::get_offset_of_eyes_4(),
	SkeletonUtilityEyeConstraint_t66635751::get_offset_of_radius_5(),
	SkeletonUtilityEyeConstraint_t66635751::get_offset_of_target_6(),
	SkeletonUtilityEyeConstraint_t66635751::get_offset_of_targetPosition_7(),
	SkeletonUtilityEyeConstraint_t66635751::get_offset_of_speed_8(),
	SkeletonUtilityEyeConstraint_t66635751::get_offset_of_origins_9(),
	SkeletonUtilityEyeConstraint_t66635751::get_offset_of_centerPoint_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1966 = { sizeof (SkeletonUtilityGroundConstraint_t2092006017), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1966[12] = 
{
	SkeletonUtilityGroundConstraint_t2092006017::get_offset_of_groundMask_4(),
	SkeletonUtilityGroundConstraint_t2092006017::get_offset_of_use2D_5(),
	SkeletonUtilityGroundConstraint_t2092006017::get_offset_of_useRadius_6(),
	SkeletonUtilityGroundConstraint_t2092006017::get_offset_of_castRadius_7(),
	SkeletonUtilityGroundConstraint_t2092006017::get_offset_of_castDistance_8(),
	SkeletonUtilityGroundConstraint_t2092006017::get_offset_of_castOffset_9(),
	SkeletonUtilityGroundConstraint_t2092006017::get_offset_of_groundOffset_10(),
	SkeletonUtilityGroundConstraint_t2092006017::get_offset_of_adjustSpeed_11(),
	SkeletonUtilityGroundConstraint_t2092006017::get_offset_of_rayOrigin_12(),
	SkeletonUtilityGroundConstraint_t2092006017::get_offset_of_rayDir_13(),
	SkeletonUtilityGroundConstraint_t2092006017::get_offset_of_hitY_14(),
	SkeletonUtilityGroundConstraint_t2092006017::get_offset_of_lastHitY_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1967 = { sizeof (SkeletonUtilityKinematicShadow_t2751461930), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1967[5] = 
{
	SkeletonUtilityKinematicShadow_t2751461930::get_offset_of_detachedShadow_2(),
	SkeletonUtilityKinematicShadow_t2751461930::get_offset_of_parent_3(),
	SkeletonUtilityKinematicShadow_t2751461930::get_offset_of_hideShadow_4(),
	SkeletonUtilityKinematicShadow_t2751461930::get_offset_of_shadowRoot_5(),
	SkeletonUtilityKinematicShadow_t2751461930::get_offset_of_shadowTable_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1968 = { sizeof (TransformPair_t1730302243)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1968[2] = 
{
	TransformPair_t1730302243::get_offset_of_dest_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TransformPair_t1730302243::get_offset_of_src_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1969 = { sizeof (SlotBlendModes_t3822096134), -1, sizeof(SlotBlendModes_t3822096134_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1969[5] = 
{
	SlotBlendModes_t3822096134_StaticFields::get_offset_of_materialTable_2(),
	SlotBlendModes_t3822096134::get_offset_of_multiplyMaterialSource_3(),
	SlotBlendModes_t3822096134::get_offset_of_screenMaterialSource_4(),
	SlotBlendModes_t3822096134::get_offset_of_texture_5(),
	SlotBlendModes_t3822096134::get_offset_of_U3CAppliedU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1970 = { sizeof (MaterialTexturePair_t2729914733)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1970[2] = 
{
	MaterialTexturePair_t2729914733::get_offset_of_texture2D_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	MaterialTexturePair_t2729914733::get_offset_of_material_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1971 = { sizeof (WaitForSpineAnimationComplete_t694865537), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1971[1] = 
{
	WaitForSpineAnimationComplete_t694865537::get_offset_of_m_WasFired_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1972 = { sizeof (WaitForSpineEvent_t3387552986), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1972[5] = 
{
	WaitForSpineEvent_t3387552986::get_offset_of_m_TargetEvent_0(),
	WaitForSpineEvent_t3387552986::get_offset_of_m_EventName_1(),
	WaitForSpineEvent_t3387552986::get_offset_of_m_AnimationState_2(),
	WaitForSpineEvent_t3387552986::get_offset_of_m_WasFired_3(),
	WaitForSpineEvent_t3387552986::get_offset_of_m_unsubscribeAfterFiring_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1973 = { sizeof (WaitForSpineTrackEntryEnd_t2837745715), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1973[1] = 
{
	WaitForSpineTrackEntryEnd_t2837745715::get_offset_of_m_WasFired_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1974 = { sizeof (SkeletonAnimation_t2012766265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1974[7] = 
{
	SkeletonAnimation_t2012766265::get_offset_of_state_31(),
	SkeletonAnimation_t2012766265::get_offset_of__UpdateLocal_32(),
	SkeletonAnimation_t2012766265::get_offset_of__UpdateWorld_33(),
	SkeletonAnimation_t2012766265::get_offset_of__UpdateComplete_34(),
	SkeletonAnimation_t2012766265::get_offset_of__animationName_35(),
	SkeletonAnimation_t2012766265::get_offset_of_loop_36(),
	SkeletonAnimation_t2012766265::get_offset_of_timeScale_37(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1975 = { sizeof (SkeletonAnimator_t3794034912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1975[4] = 
{
	SkeletonAnimator_t3794034912::get_offset_of_translator_31(),
	SkeletonAnimator_t3794034912::get_offset_of__UpdateLocal_32(),
	SkeletonAnimator_t3794034912::get_offset_of__UpdateWorld_33(),
	SkeletonAnimator_t3794034912::get_offset_of__UpdateComplete_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1976 = { sizeof (MecanimTranslator_t3470518188), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1976[8] = 
{
	MecanimTranslator_t3470518188::get_offset_of_autoReset_0(),
	MecanimTranslator_t3470518188::get_offset_of_layerMixModes_1(),
	MecanimTranslator_t3470518188::get_offset_of_animationTable_2(),
	MecanimTranslator_t3470518188::get_offset_of_clipNameHashCodeTable_3(),
	MecanimTranslator_t3470518188::get_offset_of_previousAnimations_4(),
	MecanimTranslator_t3470518188::get_offset_of_clipInfoCache_5(),
	MecanimTranslator_t3470518188::get_offset_of_nextClipInfoCache_6(),
	MecanimTranslator_t3470518188::get_offset_of_animator_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1977 = { sizeof (MixMode_t2985542706)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1977[4] = 
{
	MixMode_t2985542706::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1978 = { sizeof (AnimationClipEqualityComparer_t1265784511), -1, sizeof(AnimationClipEqualityComparer_t1265784511_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1978[1] = 
{
	AnimationClipEqualityComparer_t1265784511_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1979 = { sizeof (IntEqualityComparer_t1300331196), -1, sizeof(IntEqualityComparer_t1300331196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1979[1] = 
{
	IntEqualityComparer_t1300331196_StaticFields::get_offset_of_Instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1980 = { sizeof (SkeletonExtensions_t1411540216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1980[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1981 = { sizeof (SkeletonExtensions_t549969845), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1982 = { sizeof (SkeletonRenderer_t1681389628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1982[29] = 
{
	SkeletonRenderer_t1681389628::get_offset_of_OnRebuild_2(),
	SkeletonRenderer_t1681389628::get_offset_of_OnPostProcessVertices_3(),
	SkeletonRenderer_t1681389628::get_offset_of_skeletonDataAsset_4(),
	SkeletonRenderer_t1681389628::get_offset_of_initialSkinName_5(),
	SkeletonRenderer_t1681389628::get_offset_of_initialFlipX_6(),
	SkeletonRenderer_t1681389628::get_offset_of_initialFlipY_7(),
	SkeletonRenderer_t1681389628::get_offset_of_separatorSlotNames_8(),
	SkeletonRenderer_t1681389628::get_offset_of_separatorSlots_9(),
	SkeletonRenderer_t1681389628::get_offset_of_zSpacing_10(),
	SkeletonRenderer_t1681389628::get_offset_of_useClipping_11(),
	SkeletonRenderer_t1681389628::get_offset_of_immutableTriangles_12(),
	SkeletonRenderer_t1681389628::get_offset_of_pmaVertexColors_13(),
	SkeletonRenderer_t1681389628::get_offset_of_clearStateOnDisable_14(),
	SkeletonRenderer_t1681389628::get_offset_of_tintBlack_15(),
	SkeletonRenderer_t1681389628::get_offset_of_singleSubmesh_16(),
	SkeletonRenderer_t1681389628::get_offset_of_addNormals_17(),
	SkeletonRenderer_t1681389628::get_offset_of_calculateTangents_18(),
	SkeletonRenderer_t1681389628::get_offset_of_logErrors_19(),
	SkeletonRenderer_t1681389628::get_offset_of_disableRenderingOnOverride_20(),
	SkeletonRenderer_t1681389628::get_offset_of_generateMeshOverride_21(),
	SkeletonRenderer_t1681389628::get_offset_of_customMaterialOverride_22(),
	SkeletonRenderer_t1681389628::get_offset_of_customSlotMaterials_23(),
	SkeletonRenderer_t1681389628::get_offset_of_meshRenderer_24(),
	SkeletonRenderer_t1681389628::get_offset_of_meshFilter_25(),
	SkeletonRenderer_t1681389628::get_offset_of_valid_26(),
	SkeletonRenderer_t1681389628::get_offset_of_skeleton_27(),
	SkeletonRenderer_t1681389628::get_offset_of_currentInstructions_28(),
	SkeletonRenderer_t1681389628::get_offset_of_meshGenerator_29(),
	SkeletonRenderer_t1681389628::get_offset_of_rendererBuffers_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1983 = { sizeof (SkeletonRendererDelegate_t878065760), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1984 = { sizeof (InstructionDelegate_t1951844748), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1985 = { sizeof (SkeletonUtility_t373332496), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1985[9] = 
{
	SkeletonUtility_t373332496::get_offset_of_OnReset_2(),
	SkeletonUtility_t373332496::get_offset_of_boneRoot_3(),
	SkeletonUtility_t373332496::get_offset_of_skeletonRenderer_4(),
	SkeletonUtility_t373332496::get_offset_of_skeletonAnimation_5(),
	SkeletonUtility_t373332496::get_offset_of_utilityBones_6(),
	SkeletonUtility_t373332496::get_offset_of_utilityConstraints_7(),
	SkeletonUtility_t373332496::get_offset_of_hasTransformBones_8(),
	SkeletonUtility_t373332496::get_offset_of_hasUtilityConstraints_9(),
	SkeletonUtility_t373332496::get_offset_of_needToReprocessBones_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1986 = { sizeof (SkeletonUtilityDelegate_t1611778497), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1987 = { sizeof (SkeletonUtilityBone_t1490890952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1987[15] = 
{
	SkeletonUtilityBone_t1490890952::get_offset_of_boneName_2(),
	SkeletonUtilityBone_t1490890952::get_offset_of_parentReference_3(),
	SkeletonUtilityBone_t1490890952::get_offset_of_mode_4(),
	SkeletonUtilityBone_t1490890952::get_offset_of_position_5(),
	SkeletonUtilityBone_t1490890952::get_offset_of_rotation_6(),
	SkeletonUtilityBone_t1490890952::get_offset_of_scale_7(),
	SkeletonUtilityBone_t1490890952::get_offset_of_zPosition_8(),
	SkeletonUtilityBone_t1490890952::get_offset_of_overrideAlpha_9(),
	SkeletonUtilityBone_t1490890952::get_offset_of_skeletonUtility_10(),
	SkeletonUtilityBone_t1490890952::get_offset_of_bone_11(),
	SkeletonUtilityBone_t1490890952::get_offset_of_transformLerpComplete_12(),
	SkeletonUtilityBone_t1490890952::get_offset_of_valid_13(),
	SkeletonUtilityBone_t1490890952::get_offset_of_cachedTransform_14(),
	SkeletonUtilityBone_t1490890952::get_offset_of_skeletonTransform_15(),
	SkeletonUtilityBone_t1490890952::get_offset_of_incompatibleTransformMode_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1988 = { sizeof (Mode_t3269804131)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1988[3] = 
{
	Mode_t3269804131::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1989 = { sizeof (SkeletonUtilityConstraint_t2014257453), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1989[2] = 
{
	SkeletonUtilityConstraint_t2014257453::get_offset_of_utilBone_2(),
	SkeletonUtilityConstraint_t2014257453::get_offset_of_skeletonUtility_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1990 = { sizeof (SpineAttributeBase_t3125339574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1990[4] = 
{
	SpineAttributeBase_t3125339574::get_offset_of_dataField_0(),
	SpineAttributeBase_t3125339574::get_offset_of_startsWith_1(),
	SpineAttributeBase_t3125339574::get_offset_of_includeNone_2(),
	SpineAttributeBase_t3125339574::get_offset_of_fallbackToTextField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1991 = { sizeof (SpineSlot_t3243618918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1991[1] = 
{
	SpineSlot_t3243618918::get_offset_of_containsBoundingBoxes_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1992 = { sizeof (SpineEvent_t1674004351), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1993 = { sizeof (SpineIkConstraint_t203333819), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1994 = { sizeof (SpinePathConstraint_t1221980072), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1995 = { sizeof (SpineTransformConstraint_t2634477230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1996 = { sizeof (SpineSkin_t1537368333), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1997 = { sizeof (SpineAnimation_t529128464), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1998 = { sizeof (SpineAttachment_t4141084984), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1998[5] = 
{
	SpineAttachment_t4141084984::get_offset_of_returnAttachmentPath_4(),
	SpineAttachment_t4141084984::get_offset_of_currentSkinOnly_5(),
	SpineAttachment_t4141084984::get_offset_of_placeholdersOnly_6(),
	SpineAttachment_t4141084984::get_offset_of_skinField_7(),
	SpineAttachment_t4141084984::get_offset_of_slotField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1999 = { sizeof (Hierarchy_t2251599244)+ sizeof (RuntimeObject), sizeof(Hierarchy_t2251599244_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1999[3] = 
{
	Hierarchy_t2251599244::get_offset_of_skin_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Hierarchy_t2251599244::get_offset_of_slot_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Hierarchy_t2251599244::get_offset_of_name_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
