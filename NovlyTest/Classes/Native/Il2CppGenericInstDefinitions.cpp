﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"




extern const Il2CppType RuntimeObject_0_0_0;
extern const Il2CppType Int32_t4116043316_0_0_0;
extern const Il2CppType Char_t3956936558_0_0_0;
extern const Il2CppType Int64_t2552900376_0_0_0;
extern const Il2CppType UInt32_t1552934845_0_0_0;
extern const Il2CppType UInt64_t82101530_0_0_0;
extern const Il2CppType Byte_t1072372818_0_0_0;
extern const Il2CppType SByte_t4061040188_0_0_0;
extern const Il2CppType Int16_t141815767_0_0_0;
extern const Il2CppType UInt16_t243830035_0_0_0;
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType IConvertible_t2924781617_0_0_0;
extern const Il2CppType IComparable_t4196824924_0_0_0;
extern const Il2CppType IEnumerable_t2786877510_0_0_0;
extern const Il2CppType ICloneable_t3071307377_0_0_0;
extern const Il2CppType IComparable_1_t1059852791_0_0_0;
extern const Il2CppType IEquatable_1_t2051519270_0_0_0;
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType IReflect_t3763132637_0_0_0;
extern const Il2CppType _Type_t2684199017_0_0_0;
extern const Il2CppType MemberInfo_t_0_0_0;
extern const Il2CppType ICustomAttributeProvider_t2951324458_0_0_0;
extern const Il2CppType _MemberInfo_t2248025095_0_0_0;
extern const Il2CppType Double_t385876152_0_0_0;
extern const Il2CppType Single_t1534300504_0_0_0;
extern const Il2CppType Decimal_t162531659_0_0_0;
extern const Il2CppType Boolean_t1039239260_0_0_0;
extern const Il2CppType Delegate_t1310841479_0_0_0;
extern const Il2CppType ISerializable_t1907411279_0_0_0;
extern const Il2CppType ParameterInfo_t4161013588_0_0_0;
extern const Il2CppType _ParameterInfo_t1333255694_0_0_0;
extern const Il2CppType ParameterModifier_t1293521645_0_0_0;
extern const Il2CppType FieldInfo_t_0_0_0;
extern const Il2CppType _FieldInfo_t4031414373_0_0_0;
extern const Il2CppType MethodInfo_t_0_0_0;
extern const Il2CppType _MethodInfo_t1133384202_0_0_0;
extern const Il2CppType MethodBase_t4283069302_0_0_0;
extern const Il2CppType _MethodBase_t4233423649_0_0_0;
extern const Il2CppType ConstructorInfo_t4238936464_0_0_0;
extern const Il2CppType _ConstructorInfo_t1306361156_0_0_0;
extern const Il2CppType IntPtr_t_0_0_0;
extern const Il2CppType TableRange_t2678548861_0_0_0;
extern const Il2CppType TailoringInfo_t1112703235_0_0_0;
extern const Il2CppType KeyValuePair_2_t372023137_0_0_0;
extern const Il2CppType Link_t2829283872_0_0_0;
extern const Il2CppType DictionaryEntry_t2732573845_0_0_0;
extern const Il2CppType KeyValuePair_2_t4223200175_0_0_0;
extern const Il2CppType Contraction_t1226415401_0_0_0;
extern const Il2CppType Level2Map_t2111576706_0_0_0;
extern const Il2CppType BigInteger_t999372291_0_0_0;
extern const Il2CppType KeySizes_t1153709797_0_0_0;
extern const Il2CppType KeyValuePair_2_t3292850614_0_0_0;
extern const Il2CppType Slot_t2420837462_0_0_0;
extern const Il2CppType Slot_t1498351365_0_0_0;
extern const Il2CppType StackFrame_t1665646767_0_0_0;
extern const Il2CppType Calendar_t3315179817_0_0_0;
extern const Il2CppType ModuleBuilder_t1649085347_0_0_0;
extern const Il2CppType _ModuleBuilder_t3557728172_0_0_0;
extern const Il2CppType Module_t1529303715_0_0_0;
extern const Il2CppType _Module_t3000549352_0_0_0;
extern const Il2CppType ParameterBuilder_t1717477986_0_0_0;
extern const Il2CppType _ParameterBuilder_t309653636_0_0_0;
extern const Il2CppType TypeU5BU5D_t2532561753_0_0_0;
extern const Il2CppType RuntimeArray_0_0_0;
extern const Il2CppType ICollection_t4150824962_0_0_0;
extern const Il2CppType IList_t682362525_0_0_0;
extern const Il2CppType IList_1_t2748475375_0_0_0;
extern const Il2CppType ICollection_1_t516081447_0_0_0;
extern const Il2CppType IEnumerable_1_t1681982789_0_0_0;
extern const Il2CppType IList_1_t2168955716_0_0_0;
extern const Il2CppType ICollection_1_t4231529084_0_0_0;
extern const Il2CppType IEnumerable_1_t1102463130_0_0_0;
extern const Il2CppType IList_1_t1090022096_0_0_0;
extern const Il2CppType ICollection_1_t3152595464_0_0_0;
extern const Il2CppType IEnumerable_1_t23529510_0_0_0;
extern const Il2CppType IList_1_t2753805949_0_0_0;
extern const Il2CppType ICollection_1_t521412021_0_0_0;
extern const Il2CppType IEnumerable_1_t1687313363_0_0_0;
extern const Il2CppType IList_1_t1357147537_0_0_0;
extern const Il2CppType ICollection_1_t3419720905_0_0_0;
extern const Il2CppType IEnumerable_1_t290654951_0_0_0;
extern const Il2CppType IList_1_t653848174_0_0_0;
extern const Il2CppType ICollection_1_t2716421542_0_0_0;
extern const Il2CppType IEnumerable_1_t3882322884_0_0_0;
extern const Il2CppType IList_1_t1147726576_0_0_0;
extern const Il2CppType ICollection_1_t3210299944_0_0_0;
extern const Il2CppType IEnumerable_1_t81233990_0_0_0;
extern const Il2CppType ILTokenInfo_t511186550_0_0_0;
extern const Il2CppType LabelData_t1722697480_0_0_0;
extern const Il2CppType LabelFixup_t742834559_0_0_0;
extern const Il2CppType GenericTypeParameterBuilder_t1146986652_0_0_0;
extern const Il2CppType TypeBuilder_t2259672225_0_0_0;
extern const Il2CppType _TypeBuilder_t1767590369_0_0_0;
extern const Il2CppType MethodBuilder_t3780982296_0_0_0;
extern const Il2CppType _MethodBuilder_t3671189628_0_0_0;
extern const Il2CppType ConstructorBuilder_t3893115018_0_0_0;
extern const Il2CppType _ConstructorBuilder_t865107083_0_0_0;
extern const Il2CppType FieldBuilder_t3558139324_0_0_0;
extern const Il2CppType _FieldBuilder_t4178988306_0_0_0;
extern const Il2CppType PropertyInfo_t_0_0_0;
extern const Il2CppType _PropertyInfo_t3692780915_0_0_0;
extern const Il2CppType CustomAttributeTypedArgument_t1279663203_0_0_0;
extern const Il2CppType CustomAttributeNamedArgument_t2964753189_0_0_0;
extern const Il2CppType CustomAttributeData_t3364937814_0_0_0;
extern const Il2CppType ResourceInfo_t2441261143_0_0_0;
extern const Il2CppType ResourceCacheItem_t484540818_0_0_0;
extern const Il2CppType IContextProperty_t4041766982_0_0_0;
extern const Il2CppType Header_t3237302409_0_0_0;
extern const Il2CppType ITrackingHandler_t817503570_0_0_0;
extern const Il2CppType IContextAttribute_t142910004_0_0_0;
extern const Il2CppType DateTime_t1078222776_0_0_0;
extern const Il2CppType TimeSpan_t1041562996_0_0_0;
extern const Il2CppType TypeTag_t1928723012_0_0_0;
extern const Il2CppType MonoType_t_0_0_0;
extern const Il2CppType StrongName_t1017363585_0_0_0;
extern const Il2CppType IBuiltInEvidence_t3051585724_0_0_0;
extern const Il2CppType IIdentityPermissionFactory_t419319585_0_0_0;
extern const Il2CppType DateTimeOffset_t774991079_0_0_0;
extern const Il2CppType Guid_t_0_0_0;
extern const Il2CppType Version_t2450729307_0_0_0;
extern const Il2CppType KeyValuePair_2_t1590186377_0_0_0;
extern const Il2CppType KeyValuePair_2_t1146396119_0_0_0;
extern const Il2CppType X509Certificate_t896677858_0_0_0;
extern const Il2CppType IDeserializationCallback_t1468984353_0_0_0;
extern const Il2CppType X509ChainStatus_t1809129440_0_0_0;
extern const Il2CppType Capture_t3038483990_0_0_0;
extern const Il2CppType Group_t2769576737_0_0_0;
extern const Il2CppType Mark_t3997027598_0_0_0;
extern const Il2CppType UriScheme_t2335814732_0_0_0;
extern const Il2CppType BigInteger_t999372292_0_0_0;
extern const Il2CppType ByteU5BU5D_t371269159_0_0_0;
extern const Il2CppType IList_1_t3773163193_0_0_0;
extern const Il2CppType ICollection_1_t1540769265_0_0_0;
extern const Il2CppType IEnumerable_1_t2706670607_0_0_0;
extern const Il2CppType ClientCertificateType_t3184407727_0_0_0;
extern const Il2CppType Link_t1352151537_0_0_0;
extern const Il2CppType Object_t350248726_0_0_0;
extern const Il2CppType Camera_t620664847_0_0_0;
extern const Il2CppType Behaviour_t2905267502_0_0_0;
extern const Il2CppType Component_t1852985663_0_0_0;
extern const Il2CppType Display_t3403130684_0_0_0;
extern const Il2CppType Material_t2681358023_0_0_0;
extern const Il2CppType Keyframe_t1429249175_0_0_0;
extern const Il2CppType Vector3_t2825674791_0_0_0;
extern const Il2CppType Vector4_t2651204353_0_0_0;
extern const Il2CppType Vector2_t1065050092_0_0_0;
extern const Il2CppType Color_t4183816058_0_0_0;
extern const Il2CppType Color32_t2411287715_0_0_0;
extern const Il2CppType Rect_t82402607_0_0_0;
extern const Il2CppType Texture2D_t1431210461_0_0_0;
extern const Il2CppType Texture_t4046637001_0_0_0;
extern const Il2CppType Playable_t3322485176_0_0_0;
extern const Il2CppType PlayableOutput_t1562817808_0_0_0;
extern const Il2CppType Scene_t937589653_0_0_0;
extern const Il2CppType LoadSceneMode_t56641604_0_0_0;
extern const Il2CppType SpriteAtlas_t2050322510_0_0_0;
extern const Il2CppType ContactPoint_t1469515158_0_0_0;
extern const Il2CppType RaycastHit_t2126573776_0_0_0;
extern const Il2CppType Rigidbody2D_t4047763079_0_0_0;
extern const Il2CppType RaycastHit2D_t3956163020_0_0_0;
extern const Il2CppType ContactPoint2D_t2112099150_0_0_0;
extern const Il2CppType AudioClipPlayable_t3721052735_0_0_0;
extern const Il2CppType AudioMixerPlayable_t1728225682_0_0_0;
extern const Il2CppType AnimationClipPlayable_t2781760252_0_0_0;
extern const Il2CppType AnimationLayerMixerPlayable_t4269461007_0_0_0;
extern const Il2CppType AnimationMixerPlayable_t2857389361_0_0_0;
extern const Il2CppType AnimationOffsetPlayable_t3129069933_0_0_0;
extern const Il2CppType AnimatorControllerPlayable_t4108006533_0_0_0;
extern const Il2CppType AnimatorClipInfo_t3943754594_0_0_0;
extern const Il2CppType AnimatorControllerParameter_t2432615499_0_0_0;
extern const Il2CppType UIVertex_t475044788_0_0_0;
extern const Il2CppType UICharInfo_t3450488657_0_0_0;
extern const Il2CppType UILineInfo_t4132474705_0_0_0;
extern const Il2CppType Font_t1312336880_0_0_0;
extern const Il2CppType GUILayoutOption_t3257264162_0_0_0;
extern const Il2CppType GUILayoutEntry_t2519065403_0_0_0;
extern const Il2CppType LayoutCache_t851475779_0_0_0;
extern const Il2CppType KeyValuePair_2_t2473691391_0_0_0;
extern const Il2CppType KeyValuePair_2_t583263673_0_0_0;
extern const Il2CppType GUIStyle_t2201526215_0_0_0;
extern const Il2CppType KeyValuePair_2_t2308683074_0_0_0;
extern const Il2CppType Exception_t2572292308_0_0_0;
extern const Il2CppType UserProfile_t1495896542_0_0_0;
extern const Il2CppType IUserProfile_t1419151331_0_0_0;
extern const Il2CppType AchievementDescription_t3623532732_0_0_0;
extern const Il2CppType IAchievementDescription_t2263541353_0_0_0;
extern const Il2CppType GcLeaderboard_t582328223_0_0_0;
extern const Il2CppType IAchievementDescriptionU5BU5D_t2404093396_0_0_0;
extern const Il2CppType IAchievementU5BU5D_t3105227985_0_0_0;
extern const Il2CppType IAchievement_t2790711152_0_0_0;
extern const Il2CppType GcAchievementData_t3971162273_0_0_0;
extern const Il2CppType Achievement_t1941630733_0_0_0;
extern const Il2CppType IScoreU5BU5D_t3128097251_0_0_0;
extern const Il2CppType IScore_t1577383494_0_0_0;
extern const Il2CppType GcScoreData_t3049994916_0_0_0;
extern const Il2CppType Score_t246219269_0_0_0;
extern const Il2CppType IUserProfileU5BU5D_t644998450_0_0_0;
extern const Il2CppType DisallowMultipleComponent_t2944104030_0_0_0;
extern const Il2CppType Attribute_t2169343654_0_0_0;
extern const Il2CppType _Attribute_t156294154_0_0_0;
extern const Il2CppType ExecuteInEditMode_t767168347_0_0_0;
extern const Il2CppType RequireComponent_t1008017238_0_0_0;
extern const Il2CppType HitInfo_t2706273657_0_0_0;
extern const Il2CppType PersistentCall_t2119335335_0_0_0;
extern const Il2CppType BaseInvokableCall_t2985877092_0_0_0;
extern const Il2CppType WorkRequest_t2264216812_0_0_0;
extern const Il2CppType PlayableBinding_t150998691_0_0_0;
extern const Il2CppType MessageTypeSubscribers_t2381605113_0_0_0;
extern const Il2CppType MessageEventArgs_t3322264806_0_0_0;
extern const Il2CppType WeakReference_t449294333_0_0_0;
extern const Il2CppType KeyValuePair_2_t3050155971_0_0_0;
extern const Il2CppType KeyValuePair_2_t757546807_0_0_0;
extern const Il2CppType BaseInputModule_t4202312920_0_0_0;
extern const Il2CppType UIBehaviour_t3749981382_0_0_0;
extern const Il2CppType MonoBehaviour_t3755042618_0_0_0;
extern const Il2CppType RaycastResult_t2363482100_0_0_0;
extern const Il2CppType IDeselectHandler_t2317430855_0_0_0;
extern const Il2CppType IEventSystemHandler_t989066233_0_0_0;
extern const Il2CppType List_1_t3112481958_0_0_0;
extern const Il2CppType List_1_t570351926_0_0_0;
extern const Il2CppType List_1_t3976401388_0_0_0;
extern const Il2CppType ISelectHandler_t4128318009_0_0_0;
extern const Il2CppType BaseRaycaster_t3149871384_0_0_0;
extern const Il2CppType Entry_t1923502037_0_0_0;
extern const Il2CppType BaseEventData_t2200158663_0_0_0;
extern const Il2CppType IPointerEnterHandler_t1206321535_0_0_0;
extern const Il2CppType IPointerExitHandler_t563170456_0_0_0;
extern const Il2CppType IPointerDownHandler_t788657900_0_0_0;
extern const Il2CppType IPointerUpHandler_t3907224541_0_0_0;
extern const Il2CppType IPointerClickHandler_t4166377497_0_0_0;
extern const Il2CppType IInitializePotentialDragHandler_t3159303760_0_0_0;
extern const Il2CppType IBeginDragHandler_t4197993221_0_0_0;
extern const Il2CppType IDragHandler_t4019865541_0_0_0;
extern const Il2CppType IEndDragHandler_t760648009_0_0_0;
extern const Il2CppType IDropHandler_t3618352481_0_0_0;
extern const Il2CppType IScrollHandler_t3841850943_0_0_0;
extern const Il2CppType IUpdateSelectedHandler_t3267774463_0_0_0;
extern const Il2CppType IMoveHandler_t2215200409_0_0_0;
extern const Il2CppType ISubmitHandler_t1318372016_0_0_0;
extern const Il2CppType ICancelHandler_t2259736822_0_0_0;
extern const Il2CppType Transform_t2735953680_0_0_0;
extern const Il2CppType GameObject_t3093992626_0_0_0;
extern const Il2CppType BaseInput_t1368120269_0_0_0;
extern const Il2CppType PointerEventData_t1843710511_0_0_0;
extern const Il2CppType AbstractEventData_t3634143719_0_0_0;
extern const Il2CppType KeyValuePair_2_t1575498405_0_0_0;
extern const Il2CppType ButtonState_t1423469858_0_0_0;
extern const Il2CppType ICanvasElement_t1064023118_0_0_0;
extern const Il2CppType ColorBlock_t2548562872_0_0_0;
extern const Il2CppType OptionData_t2269286103_0_0_0;
extern const Il2CppType DropdownItem_t706906876_0_0_0;
extern const Il2CppType FloatTween_t1018971280_0_0_0;
extern const Il2CppType Sprite_t360607379_0_0_0;
extern const Il2CppType Canvas_t452078485_0_0_0;
extern const Il2CppType List_1_t2575494210_0_0_0;
extern const Il2CppType HashSet_1_t64826019_0_0_0;
extern const Il2CppType Text_t1469493361_0_0_0;
extern const Il2CppType Link_t79741401_0_0_0;
extern const Il2CppType ILayoutElement_t3747613007_0_0_0;
extern const Il2CppType MaskableGraphic_t2710636079_0_0_0;
extern const Il2CppType IClippable_t617981727_0_0_0;
extern const Il2CppType IMaskable_t865684376_0_0_0;
extern const Il2CppType IMaterialModifier_t3524121170_0_0_0;
extern const Il2CppType Graphic_t366741146_0_0_0;
extern const Il2CppType KeyValuePair_2_t3536216813_0_0_0;
extern const Il2CppType ColorTween_t3858592911_0_0_0;
extern const Il2CppType IndexedSet_1_t3523870668_0_0_0;
extern const Il2CppType KeyValuePair_2_t3649819709_0_0_0;
extern const Il2CppType KeyValuePair_2_t241250796_0_0_0;
extern const Il2CppType KeyValuePair_2_t3657347816_0_0_0;
extern const Il2CppType Type_t3593005433_0_0_0;
extern const Il2CppType FillMethod_t2569813474_0_0_0;
extern const Il2CppType ContentType_t2659363712_0_0_0;
extern const Il2CppType LineType_t1397773182_0_0_0;
extern const Il2CppType InputType_t1682781151_0_0_0;
extern const Il2CppType TouchScreenKeyboardType_t3909346333_0_0_0;
extern const Il2CppType CharacterValidation_t2407825395_0_0_0;
extern const Il2CppType Mask_t1104295890_0_0_0;
extern const Il2CppType ICanvasRaycastFilter_t640459987_0_0_0;
extern const Il2CppType List_1_t3227711615_0_0_0;
extern const Il2CppType RectMask2D_t475906393_0_0_0;
extern const Il2CppType IClipper_t4180951425_0_0_0;
extern const Il2CppType List_1_t2599322118_0_0_0;
extern const Il2CppType Navigation_t2074156770_0_0_0;
extern const Il2CppType Link_t3523197063_0_0_0;
extern const Il2CppType Direction_t2341802389_0_0_0;
extern const Il2CppType Selectable_t301191868_0_0_0;
extern const Il2CppType Transition_t2430538793_0_0_0;
extern const Il2CppType SpriteState_t4104548053_0_0_0;
extern const Il2CppType CanvasGroup_t514076778_0_0_0;
extern const Il2CppType Direction_t3623476438_0_0_0;
extern const Il2CppType MatEntry_t961266217_0_0_0;
extern const Il2CppType Toggle_t1097858442_0_0_0;
extern const Il2CppType KeyValuePair_2_t699909001_0_0_0;
extern const Il2CppType AspectMode_t757271725_0_0_0;
extern const Il2CppType FitMode_t1874677476_0_0_0;
extern const Il2CppType RectTransform_t1087788885_0_0_0;
extern const Il2CppType LayoutRebuilder_t3161446486_0_0_0;
extern const Il2CppType List_1_t654123220_0_0_0;
extern const Il2CppType List_1_t239736144_0_0_0;
extern const Il2CppType List_1_t3188465817_0_0_0;
extern const Il2CppType List_1_t479652782_0_0_0;
extern const Il2CppType List_1_t1944491745_0_0_0;
extern const Il2CppType List_1_t2598460513_0_0_0;
extern const Il2CppType AtlasAsset_t2525633250_0_0_0;
extern const Il2CppType ScriptableObject_t229319923_0_0_0;
extern const Il2CppType SlotRegionPair_t2265310044_0_0_0;
extern const Il2CppType AtlasPage_t3729449121_0_0_0;
extern const Il2CppType KeyValuePair_2_t1286815118_0_0_0;
extern const Il2CppType Animation_t2067143511_0_0_0;
extern const Il2CppType SkeletonAnimation_t2012766265_0_0_0;
extern const Il2CppType ISkeletonAnimation_t2072256521_0_0_0;
extern const Il2CppType IAnimationStateComponent_t2249916977_0_0_0;
extern const Il2CppType SkeletonRenderer_t1681389628_0_0_0;
extern const Il2CppType ISkeletonComponent_t3641217750_0_0_0;
extern const Il2CppType ISkeletonDataAssetComponent_t3477846014_0_0_0;
extern const Il2CppType KeyValuePair_2_t851776416_0_0_0;
extern const Il2CppType KeyValuePair_2_t2174300370_0_0_0;
extern const Il2CppType SkeletonDataAsset_t1706375087_0_0_0;
extern const Il2CppType SlotSettings_t3151105826_0_0_0;
extern const Il2CppType EventPair_t1359900303_0_0_0;
extern const Il2CppType Event_t27715519_0_0_0;
extern const Il2CppType Timeline_t3562138029_0_0_0;
extern const Il2CppType Bone_t1763862836_0_0_0;
extern const Il2CppType IUpdatable_t125710159_0_0_0;
extern const Il2CppType Slot_t1804116343_0_0_0;
extern const Il2CppType SingleU5BU5D_t3989243465_0_0_0;
extern const Il2CppType IList_1_t4235090879_0_0_0;
extern const Il2CppType ICollection_1_t2002696951_0_0_0;
extern const Il2CppType IEnumerable_1_t3168598293_0_0_0;
extern const Il2CppType Int32U5BU5D_t281224829_0_0_0;
extern const Il2CppType IList_1_t2521866395_0_0_0;
extern const Il2CppType ICollection_1_t289472467_0_0_0;
extern const Il2CppType IEnumerable_1_t1455373809_0_0_0;
extern const Il2CppType IkConstraint_t2514691977_0_0_0;
extern const Il2CppType IConstraint_t900275133_0_0_0;
extern const Il2CppType TransformConstraint_t509539004_0_0_0;
extern const Il2CppType PathConstraint_t1854663405_0_0_0;
extern const Il2CppType TrackEntry_t4062297782_0_0_0;
extern const Il2CppType IPoolable_t612200214_0_0_0;
extern const Il2CppType Link_t2726291356_0_0_0;
extern const Il2CppType EventQueueEntry_t3756679084_0_0_0;
extern const Il2CppType AnimationPair_t2435898854_0_0_0;
extern const Il2CppType KeyValuePair_2_t914961172_0_0_0;
extern const Il2CppType AtlasRegion_t2476064513_0_0_0;
extern const Il2CppType Atlas_t604244430_0_0_0;
extern const Il2CppType BoneData_t1194659452_0_0_0;
extern const Il2CppType KeyValuePair_2_t2849060356_0_0_0;
extern const Il2CppType SlotData_t587781850_0_0_0;
extern const Il2CppType IkConstraintData_t3215615709_0_0_0;
extern const Il2CppType TransformConstraintData_t1381040281_0_0_0;
extern const Il2CppType PathConstraintData_t3185350348_0_0_0;
extern const Il2CppType Skin_t1695237131_0_0_0;
extern const Il2CppType AttachmentKeyTuple_t2283295499_0_0_0;
extern const Il2CppType Attachment_t397756874_0_0_0;
extern const Il2CppType KeyValuePair_2_t2697487084_0_0_0;
extern const Il2CppType KeyValuePair_2_t353340461_0_0_0;
extern const Il2CppType LinkedMesh_t3424637972_0_0_0;
extern const Il2CppType TransformMode_t1529364024_0_0_0;
extern const Il2CppType EventData_t295981108_0_0_0;
extern const Il2CppType BoundingBoxAttachment_t1026836171_0_0_0;
extern const Il2CppType VertexAttachment_t1776545046_0_0_0;
extern const Il2CppType Polygon_t3410609907_0_0_0;
extern const Il2CppType ExposedList_1_t3384296343_0_0_0;
extern const Il2CppType ExposedList_1_t1671071859_0_0_0;
extern const Il2CppType SubmeshInstruction_t3765917328_0_0_0;
extern const Il2CppType KeyValuePair_2_t2774005646_0_0_0;
extern const Il2CppType KeyValuePair_2_t722797310_0_0_0;
extern const Il2CppType SmartMesh_t2574154657_0_0_0;
extern const Il2CppType KeyValuePair_2_t3682718642_0_0_0;
extern const Il2CppType KeyValuePair_2_t2072584201_0_0_0;
extern const Il2CppType PolygonCollider2D_t542995269_0_0_0;
extern const Il2CppType Collider2D_t179611260_0_0_0;
extern const Il2CppType KeyValuePair_2_t3328228072_0_0_0;
extern const Il2CppType KeyValuePair_2_t35644278_0_0_0;
extern const Il2CppType SlotMaterialOverride_t3080213539_0_0_0;
extern const Il2CppType AtlasMaterialOverride_t4245281532_0_0_0;
extern const Il2CppType SkeletonGhostRenderer_t108436892_0_0_0;
extern const Il2CppType Rigidbody_t2418109227_0_0_0;
extern const Il2CppType KeyValuePair_2_t2787016838_0_0_0;
extern const Il2CppType Collider_t4062789335_0_0_0;
extern const Il2CppType SkeletonUtilityBone_t1490890952_0_0_0;
extern const Il2CppType SkeletonPartsRenderer_t3336947722_0_0_0;
extern const Il2CppType TransformPair_t1730302243_0_0_0;
extern const Il2CppType Joint_t59536310_0_0_0;
extern const Il2CppType MaterialTexturePair_t2729914733_0_0_0;
extern const Il2CppType KeyValuePair_2_t3542000834_0_0_0;
extern const Il2CppType KeyValuePair_2_t3481455360_0_0_0;
extern const Il2CppType MixMode_t2985542706_0_0_0;
extern const Il2CppType KeyValuePair_2_t1798931405_0_0_0;
extern const Il2CppType AnimationClip_t3806409204_0_0_0;
extern const Il2CppType Motion_t2361590416_0_0_0;
extern const Il2CppType KeyValuePair_2_t3844094954_0_0_0;
extern const Il2CppType SkeletonUtilityConstraint_t2014257453_0_0_0;
extern const Il2CppType IEnumerable_1_t3156408824_gp_0_0_0_0;
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m3085334648_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2713204366_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m482111105_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m482111105_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m2832668269_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1728532693_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m1728532693_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m3755495371_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m748731730_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m748731730_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m2310720347_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2925804184_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2925804184_gp_1_0_0_0;
extern const Il2CppType Array_Sort_m3742156574_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m2614704753_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m4171624513_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m4171624513_gp_1_0_0_0;
extern const Il2CppType Array_compare_m4383310_gp_0_0_0_0;
extern const Il2CppType Array_qsort_m3103677270_gp_0_0_0_0;
extern const Il2CppType Array_Resize_m277214561_gp_0_0_0_0;
extern const Il2CppType Array_TrueForAll_m513891374_gp_0_0_0_0;
extern const Il2CppType Array_ForEach_m466051874_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1422568442_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m1422568442_gp_1_0_0_0;
extern const Il2CppType Array_FindLastIndex_m2989453766_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m1230572012_gp_0_0_0_0;
extern const Il2CppType Array_FindLastIndex_m411634194_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m3912172757_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m2906578126_gp_0_0_0_0;
extern const Il2CppType Array_FindIndex_m1157017374_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m3771917272_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m3734383632_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m2735412359_gp_0_0_0_0;
extern const Il2CppType Array_BinarySearch_m977584100_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m4293893642_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m3131945657_gp_0_0_0_0;
extern const Il2CppType Array_IndexOf_m523691678_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m3813191867_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m148270828_gp_0_0_0_0;
extern const Il2CppType Array_LastIndexOf_m477455076_gp_0_0_0_0;
extern const Il2CppType Array_FindAll_m310413631_gp_0_0_0_0;
extern const Il2CppType Array_Exists_m865355114_gp_0_0_0_0;
extern const Il2CppType Array_AsReadOnly_m290138908_gp_0_0_0_0;
extern const Il2CppType Array_Find_m2601461597_gp_0_0_0_0;
extern const Il2CppType Array_FindLast_m1619294651_gp_0_0_0_0;
extern const Il2CppType InternalEnumerator_1_t481779380_gp_0_0_0_0;
extern const Il2CppType ArrayReadOnlyList_1_t1645599289_gp_0_0_0_0;
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t1910536622_gp_0_0_0_0;
extern const Il2CppType IList_1_t3190236850_gp_0_0_0_0;
extern const Il2CppType ICollection_1_t1858752682_gp_0_0_0_0;
extern const Il2CppType Nullable_1_t3509863868_gp_0_0_0_0;
extern const Il2CppType Comparer_1_t1586122693_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t3317074146_gp_0_0_0_0;
extern const Il2CppType GenericComparer_1_t612734070_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t3826160452_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_t3826160452_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t1051722143_0_0_0;
extern const Il2CppType Dictionary_2_Do_CopyTo_m1678136693_gp_0_0_0_0;
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m3850636111_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t1628784215_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t1628784215_gp_1_0_0_0;
extern const Il2CppType Enumerator_t1833107221_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1833107221_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t2047929147_0_0_0;
extern const Il2CppType KeyCollection_t3967759248_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t3967759248_gp_1_0_0_0;
extern const Il2CppType Enumerator_t457868420_gp_0_0_0_0;
extern const Il2CppType Enumerator_t457868420_gp_1_0_0_0;
extern const Il2CppType ValueCollection_t1986136921_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t1986136921_gp_1_0_0_0;
extern const Il2CppType Enumerator_t1246502289_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1246502289_gp_1_0_0_0;
extern const Il2CppType EqualityComparer_1_t376817520_gp_0_0_0_0;
extern const Il2CppType DefaultComparer_t371954493_gp_0_0_0_0;
extern const Il2CppType GenericEqualityComparer_1_t510648398_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t3857675627_0_0_0;
extern const Il2CppType IDictionary_2_t2146790633_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t2146790633_gp_1_0_0_0;
extern const Il2CppType KeyValuePair_2_t2275830193_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t2275830193_gp_1_0_0_0;
extern const Il2CppType List_1_t1241771112_gp_0_0_0_0;
extern const Il2CppType Enumerator_t930421405_gp_0_0_0_0;
extern const Il2CppType Collection_1_t3355046425_gp_0_0_0_0;
extern const Il2CppType ReadOnlyCollection_1_t2677599621_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m3692694854_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m3692694854_gp_1_0_0_0;
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m2072945890_gp_0_0_0_0;
extern const Il2CppType Queue_1_t167895599_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1878021519_gp_0_0_0_0;
extern const Il2CppType Stack_1_t2037534615_gp_0_0_0_0;
extern const Il2CppType Enumerator_t3227029231_gp_0_0_0_0;
extern const Il2CppType HashSet_1_t3110041205_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2912145968_gp_0_0_0_0;
extern const Il2CppType PrimeHelper_t363312612_gp_0_0_0_0;
extern const Il2CppType Enumerable_Any_m795394156_gp_0_0_0_0;
extern const Il2CppType Enumerable_Where_m2840543242_gp_0_0_0_0;
extern const Il2CppType Enumerable_CreateWhereIterator_m2042176342_gp_0_0_0_0;
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2323895991_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentInChildren_m476264693_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m896983223_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m3176757661_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m2940377740_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInChildren_m1572369961_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m3185394231_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m2454520785_gp_0_0_0_0;
extern const Il2CppType Component_GetComponentsInParent_m2474810776_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m945396275_gp_0_0_0_0;
extern const Il2CppType Component_GetComponents_m4225586958_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentInChildren_m3366407555_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponents_m3404916630_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInChildren_m58698754_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInChildren_m2855193608_gp_0_0_0_0;
extern const Il2CppType GameObject_GetComponentsInParent_m3595539389_gp_0_0_0_0;
extern const Il2CppType Mesh_GetAllocArrayFromChannel_m3442725572_gp_0_0_0_0;
extern const Il2CppType Mesh_SafeLength_m4026736427_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m2097026643_gp_0_0_0_0;
extern const Il2CppType Mesh_SetListForChannel_m4081195211_gp_0_0_0_0;
extern const Il2CppType Mesh_SetUvsImpl_m2170684948_gp_0_0_0_0;
extern const Il2CppType Object_Instantiate_m3989777074_gp_0_0_0_0;
extern const Il2CppType Object_FindObjectsOfType_m3148200899_gp_0_0_0_0;
extern const Il2CppType Playable_IsPlayableOfType_m258057198_gp_0_0_0_0;
extern const Il2CppType PlayableOutput_IsPlayableOutputOfType_m4020851067_gp_0_0_0_0;
extern const Il2CppType InvokableCall_1_t3974913308_gp_0_0_0_0;
extern const Il2CppType UnityAction_1_t1195038063_0_0_0;
extern const Il2CppType InvokableCall_2_t2355787272_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2355787272_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t1710801379_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t1710801379_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t1710801379_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t827458010_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t827458010_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t827458010_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t827458010_gp_3_0_0_0;
extern const Il2CppType CachedInvokableCall_1_t1469715872_gp_0_0_0_0;
extern const Il2CppType UnityEvent_1_t3652835655_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t329765484_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t329765484_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t461102170_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t461102170_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t461102170_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t2903829384_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t2903829384_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t2903829384_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t2903829384_gp_3_0_0_0;
extern const Il2CppType ExecuteEvents_Execute_m3479605520_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m3277601682_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventList_m3888996684_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_CanHandleEvent_m797943676_gp_0_0_0_0;
extern const Il2CppType ExecuteEvents_GetEventHandler_m4096618946_gp_0_0_0_0;
extern const Il2CppType TweenRunner_1_t433795729_gp_0_0_0_0;
extern const Il2CppType Dropdown_GetOrAddComponent_m2235731956_gp_0_0_0_0;
extern const Il2CppType SetPropertyUtility_SetStruct_m3364939634_gp_0_0_0_0;
extern const Il2CppType IndexedSet_1_t86117749_gp_0_0_0_0;
extern const Il2CppType ListPool_1_t4073877284_gp_0_0_0_0;
extern const Il2CppType List_1_t2549958162_0_0_0;
extern const Il2CppType ObjectPool_1_t2076978748_gp_0_0_0_0;
extern const Il2CppType Pool_1_t337449648_gp_0_0_0_0;
extern const Il2CppType ExposedList_1_t1803397706_gp_0_0_0_0;
extern const Il2CppType ExposedList_1_ConvertAll_m4061944730_gp_0_0_0_0;
extern const Il2CppType Enumerator_t1863178257_gp_0_0_0_0;
extern const Il2CppType DoubleBuffered_1_t1464434538_gp_0_0_0_0;
extern const Il2CppType SkeletonRenderer_NewSpineGameObject_m853295412_gp_0_0_0_0;
extern const Il2CppType SkeletonRenderer_AddSpineComponent_m4245276095_gp_0_0_0_0;
extern const Il2CppType AnimationPlayableOutput_t2354988367_0_0_0;
extern const Il2CppType DefaultExecutionOrder_t135071803_0_0_0;
extern const Il2CppType AudioPlayableOutput_t108233817_0_0_0;
extern const Il2CppType PlayerConnection_t939829292_0_0_0;
extern const Il2CppType ScriptPlayableOutput_t3200927392_0_0_0;
extern const Il2CppType GUILayer_t3566394258_0_0_0;
extern const Il2CppType EventSystem_t1402709230_0_0_0;
extern const Il2CppType AxisEventData_t1890216861_0_0_0;
extern const Il2CppType SpriteRenderer_t1526302145_0_0_0;
extern const Il2CppType Image_t2836066662_0_0_0;
extern const Il2CppType Button_t2390191360_0_0_0;
extern const Il2CppType RawImage_t2407390601_0_0_0;
extern const Il2CppType Slider_t3490919315_0_0_0;
extern const Il2CppType Scrollbar_t2916731442_0_0_0;
extern const Il2CppType InputField_t2184098916_0_0_0;
extern const Il2CppType ScrollRect_t3671999215_0_0_0;
extern const Il2CppType Dropdown_t520728739_0_0_0;
extern const Il2CppType GraphicRaycaster_t4012288718_0_0_0;
extern const Il2CppType CanvasRenderer_t123344510_0_0_0;
extern const Il2CppType Corner_t1996352389_0_0_0;
extern const Il2CppType Axis_t2661399177_0_0_0;
extern const Il2CppType Constraint_t2532561711_0_0_0;
extern const Il2CppType SubmitEvent_t1927180329_0_0_0;
extern const Il2CppType OnChangeEvent_t1963771712_0_0_0;
extern const Il2CppType OnValidateInput_t1653235578_0_0_0;
extern const Il2CppType LayoutElement_t63293772_0_0_0;
extern const Il2CppType RectOffset_t2305860064_0_0_0;
extern const Il2CppType TextAnchor_t1106282459_0_0_0;
extern const Il2CppType AnimationTriggers_t1030489365_0_0_0;
extern const Il2CppType Animator_t126418246_0_0_0;
extern const Il2CppType MeshRenderer_t1621355309_0_0_0;
extern const Il2CppType CharacterController_t3006247655_0_0_0;
extern const Il2CppType SkeletonGraphic_t1867541356_0_0_0;
extern const Il2CppType SkeletonRagdoll2D_t3940222091_0_0_0;
extern const Il2CppType SpineboyBeginnerModel_t865536388_0_0_0;
extern const Il2CppType MeshFilter_t36322911_0_0_0;
extern const Il2CppType Mesh_t3526862104_0_0_0;
extern const Il2CppType HingeJoint_t2932727570_0_0_0;
extern const Il2CppType SphereCollider_t3968896157_0_0_0;
extern const Il2CppType BoxCollider_t779584950_0_0_0;
extern const Il2CppType HingeJoint2D_t2351416406_0_0_0;
extern const Il2CppType CircleCollider2D_t3428612651_0_0_0;
extern const Il2CppType BoxCollider2D_t3639716847_0_0_0;
extern const Il2CppType SkeletonRenderSeparator_t3391021243_0_0_0;
extern const Il2CppType SkeletonUtilityKinematicShadow_t2751461930_0_0_0;
extern const Il2CppType SkeletonAnimator_t3794034912_0_0_0;
extern const Il2CppType SkeletonUtility_t373332496_0_0_0;




static const RuntimeType* GenInst_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0 = { 1, GenInst_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0 = { 1, GenInst_Int32_t4116043316_0_0_0_Types };
static const RuntimeType* GenInst_Char_t3956936558_0_0_0_Types[] = { (&Char_t3956936558_0_0_0) };
extern const Il2CppGenericInst GenInst_Char_t3956936558_0_0_0 = { 1, GenInst_Char_t3956936558_0_0_0_Types };
static const RuntimeType* GenInst_Int64_t2552900376_0_0_0_Types[] = { (&Int64_t2552900376_0_0_0) };
extern const Il2CppGenericInst GenInst_Int64_t2552900376_0_0_0 = { 1, GenInst_Int64_t2552900376_0_0_0_Types };
static const RuntimeType* GenInst_UInt32_t1552934845_0_0_0_Types[] = { (&UInt32_t1552934845_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt32_t1552934845_0_0_0 = { 1, GenInst_UInt32_t1552934845_0_0_0_Types };
static const RuntimeType* GenInst_UInt64_t82101530_0_0_0_Types[] = { (&UInt64_t82101530_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt64_t82101530_0_0_0 = { 1, GenInst_UInt64_t82101530_0_0_0_Types };
static const RuntimeType* GenInst_Byte_t1072372818_0_0_0_Types[] = { (&Byte_t1072372818_0_0_0) };
extern const Il2CppGenericInst GenInst_Byte_t1072372818_0_0_0 = { 1, GenInst_Byte_t1072372818_0_0_0_Types };
static const RuntimeType* GenInst_SByte_t4061040188_0_0_0_Types[] = { (&SByte_t4061040188_0_0_0) };
extern const Il2CppGenericInst GenInst_SByte_t4061040188_0_0_0 = { 1, GenInst_SByte_t4061040188_0_0_0_Types };
static const RuntimeType* GenInst_Int16_t141815767_0_0_0_Types[] = { (&Int16_t141815767_0_0_0) };
extern const Il2CppGenericInst GenInst_Int16_t141815767_0_0_0 = { 1, GenInst_Int16_t141815767_0_0_0_Types };
static const RuntimeType* GenInst_UInt16_t243830035_0_0_0_Types[] = { (&UInt16_t243830035_0_0_0) };
extern const Il2CppGenericInst GenInst_UInt16_t243830035_0_0_0 = { 1, GenInst_UInt16_t243830035_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Types[] = { (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
static const RuntimeType* GenInst_IConvertible_t2924781617_0_0_0_Types[] = { (&IConvertible_t2924781617_0_0_0) };
extern const Il2CppGenericInst GenInst_IConvertible_t2924781617_0_0_0 = { 1, GenInst_IConvertible_t2924781617_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_t4196824924_0_0_0_Types[] = { (&IComparable_t4196824924_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_t4196824924_0_0_0 = { 1, GenInst_IComparable_t4196824924_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_t2786877510_0_0_0_Types[] = { (&IEnumerable_t2786877510_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_t2786877510_0_0_0 = { 1, GenInst_IEnumerable_t2786877510_0_0_0_Types };
static const RuntimeType* GenInst_ICloneable_t3071307377_0_0_0_Types[] = { (&ICloneable_t3071307377_0_0_0) };
extern const Il2CppGenericInst GenInst_ICloneable_t3071307377_0_0_0 = { 1, GenInst_ICloneable_t3071307377_0_0_0_Types };
static const RuntimeType* GenInst_IComparable_1_t1059852791_0_0_0_Types[] = { (&IComparable_1_t1059852791_0_0_0) };
extern const Il2CppGenericInst GenInst_IComparable_1_t1059852791_0_0_0 = { 1, GenInst_IComparable_1_t1059852791_0_0_0_Types };
static const RuntimeType* GenInst_IEquatable_1_t2051519270_0_0_0_Types[] = { (&IEquatable_1_t2051519270_0_0_0) };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2051519270_0_0_0 = { 1, GenInst_IEquatable_1_t2051519270_0_0_0_Types };
static const RuntimeType* GenInst_Type_t_0_0_0_Types[] = { (&Type_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
static const RuntimeType* GenInst_IReflect_t3763132637_0_0_0_Types[] = { (&IReflect_t3763132637_0_0_0) };
extern const Il2CppGenericInst GenInst_IReflect_t3763132637_0_0_0 = { 1, GenInst_IReflect_t3763132637_0_0_0_Types };
static const RuntimeType* GenInst__Type_t2684199017_0_0_0_Types[] = { (&_Type_t2684199017_0_0_0) };
extern const Il2CppGenericInst GenInst__Type_t2684199017_0_0_0 = { 1, GenInst__Type_t2684199017_0_0_0_Types };
static const RuntimeType* GenInst_MemberInfo_t_0_0_0_Types[] = { (&MemberInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
static const RuntimeType* GenInst_ICustomAttributeProvider_t2951324458_0_0_0_Types[] = { (&ICustomAttributeProvider_t2951324458_0_0_0) };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t2951324458_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t2951324458_0_0_0_Types };
static const RuntimeType* GenInst__MemberInfo_t2248025095_0_0_0_Types[] = { (&_MemberInfo_t2248025095_0_0_0) };
extern const Il2CppGenericInst GenInst__MemberInfo_t2248025095_0_0_0 = { 1, GenInst__MemberInfo_t2248025095_0_0_0_Types };
static const RuntimeType* GenInst_Double_t385876152_0_0_0_Types[] = { (&Double_t385876152_0_0_0) };
extern const Il2CppGenericInst GenInst_Double_t385876152_0_0_0 = { 1, GenInst_Double_t385876152_0_0_0_Types };
static const RuntimeType* GenInst_Single_t1534300504_0_0_0_Types[] = { (&Single_t1534300504_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t1534300504_0_0_0 = { 1, GenInst_Single_t1534300504_0_0_0_Types };
static const RuntimeType* GenInst_Decimal_t162531659_0_0_0_Types[] = { (&Decimal_t162531659_0_0_0) };
extern const Il2CppGenericInst GenInst_Decimal_t162531659_0_0_0 = { 1, GenInst_Decimal_t162531659_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t1039239260_0_0_0_Types[] = { (&Boolean_t1039239260_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t1039239260_0_0_0 = { 1, GenInst_Boolean_t1039239260_0_0_0_Types };
static const RuntimeType* GenInst_Delegate_t1310841479_0_0_0_Types[] = { (&Delegate_t1310841479_0_0_0) };
extern const Il2CppGenericInst GenInst_Delegate_t1310841479_0_0_0 = { 1, GenInst_Delegate_t1310841479_0_0_0_Types };
static const RuntimeType* GenInst_ISerializable_t1907411279_0_0_0_Types[] = { (&ISerializable_t1907411279_0_0_0) };
extern const Il2CppGenericInst GenInst_ISerializable_t1907411279_0_0_0 = { 1, GenInst_ISerializable_t1907411279_0_0_0_Types };
static const RuntimeType* GenInst_ParameterInfo_t4161013588_0_0_0_Types[] = { (&ParameterInfo_t4161013588_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterInfo_t4161013588_0_0_0 = { 1, GenInst_ParameterInfo_t4161013588_0_0_0_Types };
static const RuntimeType* GenInst__ParameterInfo_t1333255694_0_0_0_Types[] = { (&_ParameterInfo_t1333255694_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterInfo_t1333255694_0_0_0 = { 1, GenInst__ParameterInfo_t1333255694_0_0_0_Types };
static const RuntimeType* GenInst_ParameterModifier_t1293521645_0_0_0_Types[] = { (&ParameterModifier_t1293521645_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1293521645_0_0_0 = { 1, GenInst_ParameterModifier_t1293521645_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_FieldInfo_t_0_0_0_Types[] = { (&FieldInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__FieldInfo_t4031414373_0_0_0_Types[] = { (&_FieldInfo_t4031414373_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldInfo_t4031414373_0_0_0 = { 1, GenInst__FieldInfo_t4031414373_0_0_0_Types };
static const RuntimeType* GenInst_MethodInfo_t_0_0_0_Types[] = { (&MethodInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__MethodInfo_t1133384202_0_0_0_Types[] = { (&_MethodInfo_t1133384202_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodInfo_t1133384202_0_0_0 = { 1, GenInst__MethodInfo_t1133384202_0_0_0_Types };
static const RuntimeType* GenInst_MethodBase_t4283069302_0_0_0_Types[] = { (&MethodBase_t4283069302_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBase_t4283069302_0_0_0 = { 1, GenInst_MethodBase_t4283069302_0_0_0_Types };
static const RuntimeType* GenInst__MethodBase_t4233423649_0_0_0_Types[] = { (&_MethodBase_t4233423649_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBase_t4233423649_0_0_0 = { 1, GenInst__MethodBase_t4233423649_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorInfo_t4238936464_0_0_0_Types[] = { (&ConstructorInfo_t4238936464_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t4238936464_0_0_0 = { 1, GenInst_ConstructorInfo_t4238936464_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorInfo_t1306361156_0_0_0_Types[] = { (&_ConstructorInfo_t1306361156_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t1306361156_0_0_0 = { 1, GenInst__ConstructorInfo_t1306361156_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_TableRange_t2678548861_0_0_0_Types[] = { (&TableRange_t2678548861_0_0_0) };
extern const Il2CppGenericInst GenInst_TableRange_t2678548861_0_0_0 = { 1, GenInst_TableRange_t2678548861_0_0_0_Types };
static const RuntimeType* GenInst_TailoringInfo_t1112703235_0_0_0_Types[] = { (&TailoringInfo_t1112703235_0_0_0) };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1112703235_0_0_0 = { 1, GenInst_TailoringInfo_t1112703235_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t4116043316_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t4116043316_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t4116043316_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t4116043316_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t4116043316_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t372023137_0_0_0_Types[] = { (&KeyValuePair_2_t372023137_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t372023137_0_0_0 = { 1, GenInst_KeyValuePair_2_t372023137_0_0_0_Types };
static const RuntimeType* GenInst_Link_t2829283872_0_0_0_Types[] = { (&Link_t2829283872_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t2829283872_0_0_0 = { 1, GenInst_Link_t2829283872_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t4116043316_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_Int32_t4116043316_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t4116043316_0_0_0), (&Int32_t4116043316_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_Int32_t4116043316_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_Int32_t4116043316_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t4116043316_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t2732573845_0_0_0 = { 1, GenInst_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t372023137_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Int32_t4116043316_0_0_0), (&KeyValuePair_2_t372023137_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t372023137_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t372023137_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t4116043316_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t4223200175_0_0_0_Types[] = { (&KeyValuePair_2_t4223200175_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t4223200175_0_0_0 = { 1, GenInst_KeyValuePair_2_t4223200175_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t4223200175_0_0_0_Types[] = { (&String_t_0_0_0), (&Int32_t4116043316_0_0_0), (&KeyValuePair_2_t4223200175_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t4223200175_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t4223200175_0_0_0_Types };
static const RuntimeType* GenInst_Contraction_t1226415401_0_0_0_Types[] = { (&Contraction_t1226415401_0_0_0) };
extern const Il2CppGenericInst GenInst_Contraction_t1226415401_0_0_0 = { 1, GenInst_Contraction_t1226415401_0_0_0_Types };
static const RuntimeType* GenInst_Level2Map_t2111576706_0_0_0_Types[] = { (&Level2Map_t2111576706_0_0_0) };
extern const Il2CppGenericInst GenInst_Level2Map_t2111576706_0_0_0 = { 1, GenInst_Level2Map_t2111576706_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t999372291_0_0_0_Types[] = { (&BigInteger_t999372291_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t999372291_0_0_0 = { 1, GenInst_BigInteger_t999372291_0_0_0_Types };
static const RuntimeType* GenInst_KeySizes_t1153709797_0_0_0_Types[] = { (&KeySizes_t1153709797_0_0_0) };
extern const Il2CppGenericInst GenInst_KeySizes_t1153709797_0_0_0 = { 1, GenInst_KeySizes_t1153709797_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3292850614_0_0_0_Types[] = { (&KeyValuePair_2_t3292850614_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3292850614_0_0_0 = { 1, GenInst_KeyValuePair_2_t3292850614_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3292850614_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t3292850614_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3292850614_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3292850614_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t2420837462_0_0_0_Types[] = { (&Slot_t2420837462_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t2420837462_0_0_0 = { 1, GenInst_Slot_t2420837462_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t1498351365_0_0_0_Types[] = { (&Slot_t1498351365_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t1498351365_0_0_0 = { 1, GenInst_Slot_t1498351365_0_0_0_Types };
static const RuntimeType* GenInst_StackFrame_t1665646767_0_0_0_Types[] = { (&StackFrame_t1665646767_0_0_0) };
extern const Il2CppGenericInst GenInst_StackFrame_t1665646767_0_0_0 = { 1, GenInst_StackFrame_t1665646767_0_0_0_Types };
static const RuntimeType* GenInst_Calendar_t3315179817_0_0_0_Types[] = { (&Calendar_t3315179817_0_0_0) };
extern const Il2CppGenericInst GenInst_Calendar_t3315179817_0_0_0 = { 1, GenInst_Calendar_t3315179817_0_0_0_Types };
static const RuntimeType* GenInst_ModuleBuilder_t1649085347_0_0_0_Types[] = { (&ModuleBuilder_t1649085347_0_0_0) };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t1649085347_0_0_0 = { 1, GenInst_ModuleBuilder_t1649085347_0_0_0_Types };
static const RuntimeType* GenInst__ModuleBuilder_t3557728172_0_0_0_Types[] = { (&_ModuleBuilder_t3557728172_0_0_0) };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t3557728172_0_0_0 = { 1, GenInst__ModuleBuilder_t3557728172_0_0_0_Types };
static const RuntimeType* GenInst_Module_t1529303715_0_0_0_Types[] = { (&Module_t1529303715_0_0_0) };
extern const Il2CppGenericInst GenInst_Module_t1529303715_0_0_0 = { 1, GenInst_Module_t1529303715_0_0_0_Types };
static const RuntimeType* GenInst__Module_t3000549352_0_0_0_Types[] = { (&_Module_t3000549352_0_0_0) };
extern const Il2CppGenericInst GenInst__Module_t3000549352_0_0_0 = { 1, GenInst__Module_t3000549352_0_0_0_Types };
static const RuntimeType* GenInst_ParameterBuilder_t1717477986_0_0_0_Types[] = { (&ParameterBuilder_t1717477986_0_0_0) };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1717477986_0_0_0 = { 1, GenInst_ParameterBuilder_t1717477986_0_0_0_Types };
static const RuntimeType* GenInst__ParameterBuilder_t309653636_0_0_0_Types[] = { (&_ParameterBuilder_t309653636_0_0_0) };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t309653636_0_0_0 = { 1, GenInst__ParameterBuilder_t309653636_0_0_0_Types };
static const RuntimeType* GenInst_TypeU5BU5D_t2532561753_0_0_0_Types[] = { (&TypeU5BU5D_t2532561753_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t2532561753_0_0_0 = { 1, GenInst_TypeU5BU5D_t2532561753_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeArray_0_0_0_Types[] = { (&RuntimeArray_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeArray_0_0_0 = { 1, GenInst_RuntimeArray_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_t4150824962_0_0_0_Types[] = { (&ICollection_t4150824962_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_t4150824962_0_0_0 = { 1, GenInst_ICollection_t4150824962_0_0_0_Types };
static const RuntimeType* GenInst_IList_t682362525_0_0_0_Types[] = { (&IList_t682362525_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_t682362525_0_0_0 = { 1, GenInst_IList_t682362525_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2748475375_0_0_0_Types[] = { (&IList_1_t2748475375_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2748475375_0_0_0 = { 1, GenInst_IList_1_t2748475375_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t516081447_0_0_0_Types[] = { (&ICollection_1_t516081447_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t516081447_0_0_0 = { 1, GenInst_ICollection_1_t516081447_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1681982789_0_0_0_Types[] = { (&IEnumerable_1_t1681982789_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1681982789_0_0_0 = { 1, GenInst_IEnumerable_1_t1681982789_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2168955716_0_0_0_Types[] = { (&IList_1_t2168955716_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2168955716_0_0_0 = { 1, GenInst_IList_1_t2168955716_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t4231529084_0_0_0_Types[] = { (&ICollection_1_t4231529084_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t4231529084_0_0_0 = { 1, GenInst_ICollection_1_t4231529084_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1102463130_0_0_0_Types[] = { (&IEnumerable_1_t1102463130_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1102463130_0_0_0 = { 1, GenInst_IEnumerable_1_t1102463130_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1090022096_0_0_0_Types[] = { (&IList_1_t1090022096_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1090022096_0_0_0 = { 1, GenInst_IList_1_t1090022096_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3152595464_0_0_0_Types[] = { (&ICollection_1_t3152595464_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3152595464_0_0_0 = { 1, GenInst_ICollection_1_t3152595464_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t23529510_0_0_0_Types[] = { (&IEnumerable_1_t23529510_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t23529510_0_0_0 = { 1, GenInst_IEnumerable_1_t23529510_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2753805949_0_0_0_Types[] = { (&IList_1_t2753805949_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2753805949_0_0_0 = { 1, GenInst_IList_1_t2753805949_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t521412021_0_0_0_Types[] = { (&ICollection_1_t521412021_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t521412021_0_0_0 = { 1, GenInst_ICollection_1_t521412021_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1687313363_0_0_0_Types[] = { (&IEnumerable_1_t1687313363_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1687313363_0_0_0 = { 1, GenInst_IEnumerable_1_t1687313363_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1357147537_0_0_0_Types[] = { (&IList_1_t1357147537_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1357147537_0_0_0 = { 1, GenInst_IList_1_t1357147537_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3419720905_0_0_0_Types[] = { (&ICollection_1_t3419720905_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3419720905_0_0_0 = { 1, GenInst_ICollection_1_t3419720905_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t290654951_0_0_0_Types[] = { (&IEnumerable_1_t290654951_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t290654951_0_0_0 = { 1, GenInst_IEnumerable_1_t290654951_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t653848174_0_0_0_Types[] = { (&IList_1_t653848174_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t653848174_0_0_0 = { 1, GenInst_IList_1_t653848174_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2716421542_0_0_0_Types[] = { (&ICollection_1_t2716421542_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2716421542_0_0_0 = { 1, GenInst_ICollection_1_t2716421542_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3882322884_0_0_0_Types[] = { (&IEnumerable_1_t3882322884_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3882322884_0_0_0 = { 1, GenInst_IEnumerable_1_t3882322884_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t1147726576_0_0_0_Types[] = { (&IList_1_t1147726576_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t1147726576_0_0_0 = { 1, GenInst_IList_1_t1147726576_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t3210299944_0_0_0_Types[] = { (&ICollection_1_t3210299944_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t3210299944_0_0_0 = { 1, GenInst_ICollection_1_t3210299944_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t81233990_0_0_0_Types[] = { (&IEnumerable_1_t81233990_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t81233990_0_0_0 = { 1, GenInst_IEnumerable_1_t81233990_0_0_0_Types };
static const RuntimeType* GenInst_ILTokenInfo_t511186550_0_0_0_Types[] = { (&ILTokenInfo_t511186550_0_0_0) };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t511186550_0_0_0 = { 1, GenInst_ILTokenInfo_t511186550_0_0_0_Types };
static const RuntimeType* GenInst_LabelData_t1722697480_0_0_0_Types[] = { (&LabelData_t1722697480_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelData_t1722697480_0_0_0 = { 1, GenInst_LabelData_t1722697480_0_0_0_Types };
static const RuntimeType* GenInst_LabelFixup_t742834559_0_0_0_Types[] = { (&LabelFixup_t742834559_0_0_0) };
extern const Il2CppGenericInst GenInst_LabelFixup_t742834559_0_0_0 = { 1, GenInst_LabelFixup_t742834559_0_0_0_Types };
static const RuntimeType* GenInst_GenericTypeParameterBuilder_t1146986652_0_0_0_Types[] = { (&GenericTypeParameterBuilder_t1146986652_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1146986652_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1146986652_0_0_0_Types };
static const RuntimeType* GenInst_TypeBuilder_t2259672225_0_0_0_Types[] = { (&TypeBuilder_t2259672225_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeBuilder_t2259672225_0_0_0 = { 1, GenInst_TypeBuilder_t2259672225_0_0_0_Types };
static const RuntimeType* GenInst__TypeBuilder_t1767590369_0_0_0_Types[] = { (&_TypeBuilder_t1767590369_0_0_0) };
extern const Il2CppGenericInst GenInst__TypeBuilder_t1767590369_0_0_0 = { 1, GenInst__TypeBuilder_t1767590369_0_0_0_Types };
static const RuntimeType* GenInst_MethodBuilder_t3780982296_0_0_0_Types[] = { (&MethodBuilder_t3780982296_0_0_0) };
extern const Il2CppGenericInst GenInst_MethodBuilder_t3780982296_0_0_0 = { 1, GenInst_MethodBuilder_t3780982296_0_0_0_Types };
static const RuntimeType* GenInst__MethodBuilder_t3671189628_0_0_0_Types[] = { (&_MethodBuilder_t3671189628_0_0_0) };
extern const Il2CppGenericInst GenInst__MethodBuilder_t3671189628_0_0_0 = { 1, GenInst__MethodBuilder_t3671189628_0_0_0_Types };
static const RuntimeType* GenInst_ConstructorBuilder_t3893115018_0_0_0_Types[] = { (&ConstructorBuilder_t3893115018_0_0_0) };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t3893115018_0_0_0 = { 1, GenInst_ConstructorBuilder_t3893115018_0_0_0_Types };
static const RuntimeType* GenInst__ConstructorBuilder_t865107083_0_0_0_Types[] = { (&_ConstructorBuilder_t865107083_0_0_0) };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t865107083_0_0_0 = { 1, GenInst__ConstructorBuilder_t865107083_0_0_0_Types };
static const RuntimeType* GenInst_FieldBuilder_t3558139324_0_0_0_Types[] = { (&FieldBuilder_t3558139324_0_0_0) };
extern const Il2CppGenericInst GenInst_FieldBuilder_t3558139324_0_0_0 = { 1, GenInst_FieldBuilder_t3558139324_0_0_0_Types };
static const RuntimeType* GenInst__FieldBuilder_t4178988306_0_0_0_Types[] = { (&_FieldBuilder_t4178988306_0_0_0) };
extern const Il2CppGenericInst GenInst__FieldBuilder_t4178988306_0_0_0 = { 1, GenInst__FieldBuilder_t4178988306_0_0_0_Types };
static const RuntimeType* GenInst_PropertyInfo_t_0_0_0_Types[] = { (&PropertyInfo_t_0_0_0) };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
static const RuntimeType* GenInst__PropertyInfo_t3692780915_0_0_0_Types[] = { (&_PropertyInfo_t3692780915_0_0_0) };
extern const Il2CppGenericInst GenInst__PropertyInfo_t3692780915_0_0_0 = { 1, GenInst__PropertyInfo_t3692780915_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t1279663203_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t1279663203_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1279663203_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1279663203_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t2964753189_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t2964753189_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t2964753189_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t2964753189_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeData_t3364937814_0_0_0_Types[] = { (&CustomAttributeData_t3364937814_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t3364937814_0_0_0 = { 1, GenInst_CustomAttributeData_t3364937814_0_0_0_Types };
static const RuntimeType* GenInst_ResourceInfo_t2441261143_0_0_0_Types[] = { (&ResourceInfo_t2441261143_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceInfo_t2441261143_0_0_0 = { 1, GenInst_ResourceInfo_t2441261143_0_0_0_Types };
static const RuntimeType* GenInst_ResourceCacheItem_t484540818_0_0_0_Types[] = { (&ResourceCacheItem_t484540818_0_0_0) };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t484540818_0_0_0 = { 1, GenInst_ResourceCacheItem_t484540818_0_0_0_Types };
static const RuntimeType* GenInst_IContextProperty_t4041766982_0_0_0_Types[] = { (&IContextProperty_t4041766982_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextProperty_t4041766982_0_0_0 = { 1, GenInst_IContextProperty_t4041766982_0_0_0_Types };
static const RuntimeType* GenInst_Header_t3237302409_0_0_0_Types[] = { (&Header_t3237302409_0_0_0) };
extern const Il2CppGenericInst GenInst_Header_t3237302409_0_0_0 = { 1, GenInst_Header_t3237302409_0_0_0_Types };
static const RuntimeType* GenInst_ITrackingHandler_t817503570_0_0_0_Types[] = { (&ITrackingHandler_t817503570_0_0_0) };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t817503570_0_0_0 = { 1, GenInst_ITrackingHandler_t817503570_0_0_0_Types };
static const RuntimeType* GenInst_IContextAttribute_t142910004_0_0_0_Types[] = { (&IContextAttribute_t142910004_0_0_0) };
extern const Il2CppGenericInst GenInst_IContextAttribute_t142910004_0_0_0 = { 1, GenInst_IContextAttribute_t142910004_0_0_0_Types };
static const RuntimeType* GenInst_DateTime_t1078222776_0_0_0_Types[] = { (&DateTime_t1078222776_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTime_t1078222776_0_0_0 = { 1, GenInst_DateTime_t1078222776_0_0_0_Types };
static const RuntimeType* GenInst_TimeSpan_t1041562996_0_0_0_Types[] = { (&TimeSpan_t1041562996_0_0_0) };
extern const Il2CppGenericInst GenInst_TimeSpan_t1041562996_0_0_0 = { 1, GenInst_TimeSpan_t1041562996_0_0_0_Types };
static const RuntimeType* GenInst_TypeTag_t1928723012_0_0_0_Types[] = { (&TypeTag_t1928723012_0_0_0) };
extern const Il2CppGenericInst GenInst_TypeTag_t1928723012_0_0_0 = { 1, GenInst_TypeTag_t1928723012_0_0_0_Types };
static const RuntimeType* GenInst_MonoType_t_0_0_0_Types[] = { (&MonoType_t_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
static const RuntimeType* GenInst_StrongName_t1017363585_0_0_0_Types[] = { (&StrongName_t1017363585_0_0_0) };
extern const Il2CppGenericInst GenInst_StrongName_t1017363585_0_0_0 = { 1, GenInst_StrongName_t1017363585_0_0_0_Types };
static const RuntimeType* GenInst_IBuiltInEvidence_t3051585724_0_0_0_Types[] = { (&IBuiltInEvidence_t3051585724_0_0_0) };
extern const Il2CppGenericInst GenInst_IBuiltInEvidence_t3051585724_0_0_0 = { 1, GenInst_IBuiltInEvidence_t3051585724_0_0_0_Types };
static const RuntimeType* GenInst_IIdentityPermissionFactory_t419319585_0_0_0_Types[] = { (&IIdentityPermissionFactory_t419319585_0_0_0) };
extern const Il2CppGenericInst GenInst_IIdentityPermissionFactory_t419319585_0_0_0 = { 1, GenInst_IIdentityPermissionFactory_t419319585_0_0_0_Types };
static const RuntimeType* GenInst_DateTimeOffset_t774991079_0_0_0_Types[] = { (&DateTimeOffset_t774991079_0_0_0) };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t774991079_0_0_0 = { 1, GenInst_DateTimeOffset_t774991079_0_0_0_Types };
static const RuntimeType* GenInst_Guid_t_0_0_0_Types[] = { (&Guid_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Guid_t_0_0_0 = { 1, GenInst_Guid_t_0_0_0_Types };
static const RuntimeType* GenInst_Version_t2450729307_0_0_0_Types[] = { (&Version_t2450729307_0_0_0) };
extern const Il2CppGenericInst GenInst_Version_t2450729307_0_0_0 = { 1, GenInst_Version_t2450729307_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t1039239260_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t1039239260_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t1039239260_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t1039239260_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t1039239260_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1590186377_0_0_0_Types[] = { (&KeyValuePair_2_t1590186377_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1590186377_0_0_0 = { 1, GenInst_KeyValuePair_2_t1590186377_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t1039239260_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_Boolean_t1039239260_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t1039239260_0_0_0), (&Boolean_t1039239260_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_Boolean_t1039239260_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_Boolean_t1039239260_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t1039239260_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_KeyValuePair_2_t1590186377_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Boolean_t1039239260_0_0_0), (&KeyValuePair_2_t1590186377_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_KeyValuePair_2_t1590186377_0_0_0 = { 3, GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_KeyValuePair_2_t1590186377_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t1039239260_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t1039239260_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t1039239260_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t1039239260_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1146396119_0_0_0_Types[] = { (&KeyValuePair_2_t1146396119_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1146396119_0_0_0 = { 1, GenInst_KeyValuePair_2_t1146396119_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Boolean_t1039239260_0_0_0_KeyValuePair_2_t1146396119_0_0_0_Types[] = { (&String_t_0_0_0), (&Boolean_t1039239260_0_0_0), (&KeyValuePair_2_t1146396119_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t1039239260_0_0_0_KeyValuePair_2_t1146396119_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t1039239260_0_0_0_KeyValuePair_2_t1146396119_0_0_0_Types };
static const RuntimeType* GenInst_X509Certificate_t896677858_0_0_0_Types[] = { (&X509Certificate_t896677858_0_0_0) };
extern const Il2CppGenericInst GenInst_X509Certificate_t896677858_0_0_0 = { 1, GenInst_X509Certificate_t896677858_0_0_0_Types };
static const RuntimeType* GenInst_IDeserializationCallback_t1468984353_0_0_0_Types[] = { (&IDeserializationCallback_t1468984353_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1468984353_0_0_0 = { 1, GenInst_IDeserializationCallback_t1468984353_0_0_0_Types };
static const RuntimeType* GenInst_X509ChainStatus_t1809129440_0_0_0_Types[] = { (&X509ChainStatus_t1809129440_0_0_0) };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t1809129440_0_0_0 = { 1, GenInst_X509ChainStatus_t1809129440_0_0_0_Types };
static const RuntimeType* GenInst_Capture_t3038483990_0_0_0_Types[] = { (&Capture_t3038483990_0_0_0) };
extern const Il2CppGenericInst GenInst_Capture_t3038483990_0_0_0 = { 1, GenInst_Capture_t3038483990_0_0_0_Types };
static const RuntimeType* GenInst_Group_t2769576737_0_0_0_Types[] = { (&Group_t2769576737_0_0_0) };
extern const Il2CppGenericInst GenInst_Group_t2769576737_0_0_0 = { 1, GenInst_Group_t2769576737_0_0_0_Types };
static const RuntimeType* GenInst_Mark_t3997027598_0_0_0_Types[] = { (&Mark_t3997027598_0_0_0) };
extern const Il2CppGenericInst GenInst_Mark_t3997027598_0_0_0 = { 1, GenInst_Mark_t3997027598_0_0_0_Types };
static const RuntimeType* GenInst_UriScheme_t2335814732_0_0_0_Types[] = { (&UriScheme_t2335814732_0_0_0) };
extern const Il2CppGenericInst GenInst_UriScheme_t2335814732_0_0_0 = { 1, GenInst_UriScheme_t2335814732_0_0_0_Types };
static const RuntimeType* GenInst_BigInteger_t999372292_0_0_0_Types[] = { (&BigInteger_t999372292_0_0_0) };
extern const Il2CppGenericInst GenInst_BigInteger_t999372292_0_0_0 = { 1, GenInst_BigInteger_t999372292_0_0_0_Types };
static const RuntimeType* GenInst_ByteU5BU5D_t371269159_0_0_0_Types[] = { (&ByteU5BU5D_t371269159_0_0_0) };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t371269159_0_0_0 = { 1, GenInst_ByteU5BU5D_t371269159_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3773163193_0_0_0_Types[] = { (&IList_1_t3773163193_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3773163193_0_0_0 = { 1, GenInst_IList_1_t3773163193_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1540769265_0_0_0_Types[] = { (&ICollection_1_t1540769265_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1540769265_0_0_0 = { 1, GenInst_ICollection_1_t1540769265_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t2706670607_0_0_0_Types[] = { (&IEnumerable_1_t2706670607_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2706670607_0_0_0 = { 1, GenInst_IEnumerable_1_t2706670607_0_0_0_Types };
static const RuntimeType* GenInst_ClientCertificateType_t3184407727_0_0_0_Types[] = { (&ClientCertificateType_t3184407727_0_0_0) };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t3184407727_0_0_0 = { 1, GenInst_ClientCertificateType_t3184407727_0_0_0_Types };
static const RuntimeType* GenInst_Link_t1352151537_0_0_0_Types[] = { (&Link_t1352151537_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t1352151537_0_0_0 = { 1, GenInst_Link_t1352151537_0_0_0_Types };
static const RuntimeType* GenInst_Object_t350248726_0_0_0_Types[] = { (&Object_t350248726_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_t350248726_0_0_0 = { 1, GenInst_Object_t350248726_0_0_0_Types };
static const RuntimeType* GenInst_Camera_t620664847_0_0_0_Types[] = { (&Camera_t620664847_0_0_0) };
extern const Il2CppGenericInst GenInst_Camera_t620664847_0_0_0 = { 1, GenInst_Camera_t620664847_0_0_0_Types };
static const RuntimeType* GenInst_Behaviour_t2905267502_0_0_0_Types[] = { (&Behaviour_t2905267502_0_0_0) };
extern const Il2CppGenericInst GenInst_Behaviour_t2905267502_0_0_0 = { 1, GenInst_Behaviour_t2905267502_0_0_0_Types };
static const RuntimeType* GenInst_Component_t1852985663_0_0_0_Types[] = { (&Component_t1852985663_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_t1852985663_0_0_0 = { 1, GenInst_Component_t1852985663_0_0_0_Types };
static const RuntimeType* GenInst_Display_t3403130684_0_0_0_Types[] = { (&Display_t3403130684_0_0_0) };
extern const Il2CppGenericInst GenInst_Display_t3403130684_0_0_0 = { 1, GenInst_Display_t3403130684_0_0_0_Types };
static const RuntimeType* GenInst_Material_t2681358023_0_0_0_Types[] = { (&Material_t2681358023_0_0_0) };
extern const Il2CppGenericInst GenInst_Material_t2681358023_0_0_0 = { 1, GenInst_Material_t2681358023_0_0_0_Types };
static const RuntimeType* GenInst_Keyframe_t1429249175_0_0_0_Types[] = { (&Keyframe_t1429249175_0_0_0) };
extern const Il2CppGenericInst GenInst_Keyframe_t1429249175_0_0_0 = { 1, GenInst_Keyframe_t1429249175_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t2825674791_0_0_0_Types[] = { (&Vector3_t2825674791_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t2825674791_0_0_0 = { 1, GenInst_Vector3_t2825674791_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t2651204353_0_0_0_Types[] = { (&Vector4_t2651204353_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t2651204353_0_0_0 = { 1, GenInst_Vector4_t2651204353_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t1065050092_0_0_0_Types[] = { (&Vector2_t1065050092_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t1065050092_0_0_0 = { 1, GenInst_Vector2_t1065050092_0_0_0_Types };
static const RuntimeType* GenInst_Color_t4183816058_0_0_0_Types[] = { (&Color_t4183816058_0_0_0) };
extern const Il2CppGenericInst GenInst_Color_t4183816058_0_0_0 = { 1, GenInst_Color_t4183816058_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t2411287715_0_0_0_Types[] = { (&Color32_t2411287715_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t2411287715_0_0_0 = { 1, GenInst_Color32_t2411287715_0_0_0_Types };
static const RuntimeType* GenInst_Rect_t82402607_0_0_0_Types[] = { (&Rect_t82402607_0_0_0) };
extern const Il2CppGenericInst GenInst_Rect_t82402607_0_0_0 = { 1, GenInst_Rect_t82402607_0_0_0_Types };
static const RuntimeType* GenInst_Texture2D_t1431210461_0_0_0_Types[] = { (&Texture2D_t1431210461_0_0_0) };
extern const Il2CppGenericInst GenInst_Texture2D_t1431210461_0_0_0 = { 1, GenInst_Texture2D_t1431210461_0_0_0_Types };
static const RuntimeType* GenInst_Texture_t4046637001_0_0_0_Types[] = { (&Texture_t4046637001_0_0_0) };
extern const Il2CppGenericInst GenInst_Texture_t4046637001_0_0_0 = { 1, GenInst_Texture_t4046637001_0_0_0_Types };
static const RuntimeType* GenInst_Playable_t3322485176_0_0_0_Types[] = { (&Playable_t3322485176_0_0_0) };
extern const Il2CppGenericInst GenInst_Playable_t3322485176_0_0_0 = { 1, GenInst_Playable_t3322485176_0_0_0_Types };
static const RuntimeType* GenInst_PlayableOutput_t1562817808_0_0_0_Types[] = { (&PlayableOutput_t1562817808_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableOutput_t1562817808_0_0_0 = { 1, GenInst_PlayableOutput_t1562817808_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t937589653_0_0_0_LoadSceneMode_t56641604_0_0_0_Types[] = { (&Scene_t937589653_0_0_0), (&LoadSceneMode_t56641604_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t937589653_0_0_0_LoadSceneMode_t56641604_0_0_0 = { 2, GenInst_Scene_t937589653_0_0_0_LoadSceneMode_t56641604_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t937589653_0_0_0_Types[] = { (&Scene_t937589653_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t937589653_0_0_0 = { 1, GenInst_Scene_t937589653_0_0_0_Types };
static const RuntimeType* GenInst_Scene_t937589653_0_0_0_Scene_t937589653_0_0_0_Types[] = { (&Scene_t937589653_0_0_0), (&Scene_t937589653_0_0_0) };
extern const Il2CppGenericInst GenInst_Scene_t937589653_0_0_0_Scene_t937589653_0_0_0 = { 2, GenInst_Scene_t937589653_0_0_0_Scene_t937589653_0_0_0_Types };
static const RuntimeType* GenInst_SpriteAtlas_t2050322510_0_0_0_Types[] = { (&SpriteAtlas_t2050322510_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteAtlas_t2050322510_0_0_0 = { 1, GenInst_SpriteAtlas_t2050322510_0_0_0_Types };
static const RuntimeType* GenInst_ContactPoint_t1469515158_0_0_0_Types[] = { (&ContactPoint_t1469515158_0_0_0) };
extern const Il2CppGenericInst GenInst_ContactPoint_t1469515158_0_0_0 = { 1, GenInst_ContactPoint_t1469515158_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit_t2126573776_0_0_0_Types[] = { (&RaycastHit_t2126573776_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit_t2126573776_0_0_0 = { 1, GenInst_RaycastHit_t2126573776_0_0_0_Types };
static const RuntimeType* GenInst_Rigidbody2D_t4047763079_0_0_0_Types[] = { (&Rigidbody2D_t4047763079_0_0_0) };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t4047763079_0_0_0 = { 1, GenInst_Rigidbody2D_t4047763079_0_0_0_Types };
static const RuntimeType* GenInst_RaycastHit2D_t3956163020_0_0_0_Types[] = { (&RaycastHit2D_t3956163020_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t3956163020_0_0_0 = { 1, GenInst_RaycastHit2D_t3956163020_0_0_0_Types };
static const RuntimeType* GenInst_ContactPoint2D_t2112099150_0_0_0_Types[] = { (&ContactPoint2D_t2112099150_0_0_0) };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t2112099150_0_0_0 = { 1, GenInst_ContactPoint2D_t2112099150_0_0_0_Types };
static const RuntimeType* GenInst_AudioClipPlayable_t3721052735_0_0_0_Types[] = { (&AudioClipPlayable_t3721052735_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioClipPlayable_t3721052735_0_0_0 = { 1, GenInst_AudioClipPlayable_t3721052735_0_0_0_Types };
static const RuntimeType* GenInst_AudioMixerPlayable_t1728225682_0_0_0_Types[] = { (&AudioMixerPlayable_t1728225682_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioMixerPlayable_t1728225682_0_0_0 = { 1, GenInst_AudioMixerPlayable_t1728225682_0_0_0_Types };
static const RuntimeType* GenInst_AnimationClipPlayable_t2781760252_0_0_0_Types[] = { (&AnimationClipPlayable_t2781760252_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationClipPlayable_t2781760252_0_0_0 = { 1, GenInst_AnimationClipPlayable_t2781760252_0_0_0_Types };
static const RuntimeType* GenInst_AnimationLayerMixerPlayable_t4269461007_0_0_0_Types[] = { (&AnimationLayerMixerPlayable_t4269461007_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationLayerMixerPlayable_t4269461007_0_0_0 = { 1, GenInst_AnimationLayerMixerPlayable_t4269461007_0_0_0_Types };
static const RuntimeType* GenInst_AnimationMixerPlayable_t2857389361_0_0_0_Types[] = { (&AnimationMixerPlayable_t2857389361_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationMixerPlayable_t2857389361_0_0_0 = { 1, GenInst_AnimationMixerPlayable_t2857389361_0_0_0_Types };
static const RuntimeType* GenInst_AnimationOffsetPlayable_t3129069933_0_0_0_Types[] = { (&AnimationOffsetPlayable_t3129069933_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationOffsetPlayable_t3129069933_0_0_0 = { 1, GenInst_AnimationOffsetPlayable_t3129069933_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorControllerPlayable_t4108006533_0_0_0_Types[] = { (&AnimatorControllerPlayable_t4108006533_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorControllerPlayable_t4108006533_0_0_0 = { 1, GenInst_AnimatorControllerPlayable_t4108006533_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorClipInfo_t3943754594_0_0_0_Types[] = { (&AnimatorClipInfo_t3943754594_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3943754594_0_0_0 = { 1, GenInst_AnimatorClipInfo_t3943754594_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorControllerParameter_t2432615499_0_0_0_Types[] = { (&AnimatorControllerParameter_t2432615499_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorControllerParameter_t2432615499_0_0_0 = { 1, GenInst_AnimatorControllerParameter_t2432615499_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t475044788_0_0_0_Types[] = { (&UIVertex_t475044788_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t475044788_0_0_0 = { 1, GenInst_UIVertex_t475044788_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t3450488657_0_0_0_Types[] = { (&UICharInfo_t3450488657_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t3450488657_0_0_0 = { 1, GenInst_UICharInfo_t3450488657_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t4132474705_0_0_0_Types[] = { (&UILineInfo_t4132474705_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t4132474705_0_0_0 = { 1, GenInst_UILineInfo_t4132474705_0_0_0_Types };
static const RuntimeType* GenInst_Font_t1312336880_0_0_0_Types[] = { (&Font_t1312336880_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t1312336880_0_0_0 = { 1, GenInst_Font_t1312336880_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutOption_t3257264162_0_0_0_Types[] = { (&GUILayoutOption_t3257264162_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t3257264162_0_0_0 = { 1, GenInst_GUILayoutOption_t3257264162_0_0_0_Types };
static const RuntimeType* GenInst_GUILayoutEntry_t2519065403_0_0_0_Types[] = { (&GUILayoutEntry_t2519065403_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t2519065403_0_0_0 = { 1, GenInst_GUILayoutEntry_t2519065403_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_LayoutCache_t851475779_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&LayoutCache_t851475779_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_LayoutCache_t851475779_0_0_0 = { 2, GenInst_Int32_t4116043316_0_0_0_LayoutCache_t851475779_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2473691391_0_0_0_Types[] = { (&KeyValuePair_2_t2473691391_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2473691391_0_0_0 = { 1, GenInst_KeyValuePair_2_t2473691391_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&RuntimeObject_0_0_0), (&Int32_t4116043316_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0 = { 3, GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2473691391_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t2473691391_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2473691391_0_0_0 = { 3, GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2473691391_0_0_0_Types };
static const RuntimeType* GenInst_LayoutCache_t851475779_0_0_0_Types[] = { (&LayoutCache_t851475779_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutCache_t851475779_0_0_0 = { 1, GenInst_LayoutCache_t851475779_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_LayoutCache_t851475779_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&LayoutCache_t851475779_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_LayoutCache_t851475779_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Int32_t4116043316_0_0_0_LayoutCache_t851475779_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t583263673_0_0_0_Types[] = { (&KeyValuePair_2_t583263673_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t583263673_0_0_0 = { 1, GenInst_KeyValuePair_2_t583263673_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_LayoutCache_t851475779_0_0_0_KeyValuePair_2_t583263673_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&LayoutCache_t851475779_0_0_0), (&KeyValuePair_2_t583263673_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_LayoutCache_t851475779_0_0_0_KeyValuePair_2_t583263673_0_0_0 = { 3, GenInst_Int32_t4116043316_0_0_0_LayoutCache_t851475779_0_0_0_KeyValuePair_2_t583263673_0_0_0_Types };
static const RuntimeType* GenInst_GUIStyle_t2201526215_0_0_0_Types[] = { (&GUIStyle_t2201526215_0_0_0) };
extern const Il2CppGenericInst GenInst_GUIStyle_t2201526215_0_0_0 = { 1, GenInst_GUIStyle_t2201526215_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t2201526215_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t2201526215_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2308683074_0_0_0_Types[] = { (&KeyValuePair_2_t2308683074_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2308683074_0_0_0 = { 1, GenInst_KeyValuePair_2_t2308683074_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_KeyValuePair_2_t2308683074_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t2201526215_0_0_0), (&KeyValuePair_2_t2308683074_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_KeyValuePair_2_t2308683074_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_KeyValuePair_2_t2308683074_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_GUIStyle_t2201526215_0_0_0_Types[] = { (&String_t_0_0_0), (&GUIStyle_t2201526215_0_0_0), (&GUIStyle_t2201526215_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_GUIStyle_t2201526215_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_GUIStyle_t2201526215_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_IntPtr_t_0_0_0_Boolean_t1039239260_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&IntPtr_t_0_0_0), (&Boolean_t1039239260_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_IntPtr_t_0_0_0_Boolean_t1039239260_0_0_0 = { 3, GenInst_Int32_t4116043316_0_0_0_IntPtr_t_0_0_0_Boolean_t1039239260_0_0_0_Types };
static const RuntimeType* GenInst_Exception_t2572292308_0_0_0_Boolean_t1039239260_0_0_0_Types[] = { (&Exception_t2572292308_0_0_0), (&Boolean_t1039239260_0_0_0) };
extern const Il2CppGenericInst GenInst_Exception_t2572292308_0_0_0_Boolean_t1039239260_0_0_0 = { 2, GenInst_Exception_t2572292308_0_0_0_Boolean_t1039239260_0_0_0_Types };
static const RuntimeType* GenInst_UserProfile_t1495896542_0_0_0_Types[] = { (&UserProfile_t1495896542_0_0_0) };
extern const Il2CppGenericInst GenInst_UserProfile_t1495896542_0_0_0 = { 1, GenInst_UserProfile_t1495896542_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfile_t1419151331_0_0_0_Types[] = { (&IUserProfile_t1419151331_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfile_t1419151331_0_0_0 = { 1, GenInst_IUserProfile_t1419151331_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t1039239260_0_0_0_String_t_0_0_0_Types[] = { (&Boolean_t1039239260_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t1039239260_0_0_0_String_t_0_0_0 = { 2, GenInst_Boolean_t1039239260_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t1039239260_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Boolean_t1039239260_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t1039239260_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Boolean_t1039239260_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AchievementDescription_t3623532732_0_0_0_Types[] = { (&AchievementDescription_t3623532732_0_0_0) };
extern const Il2CppGenericInst GenInst_AchievementDescription_t3623532732_0_0_0 = { 1, GenInst_AchievementDescription_t3623532732_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescription_t2263541353_0_0_0_Types[] = { (&IAchievementDescription_t2263541353_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t2263541353_0_0_0 = { 1, GenInst_IAchievementDescription_t2263541353_0_0_0_Types };
static const RuntimeType* GenInst_GcLeaderboard_t582328223_0_0_0_Types[] = { (&GcLeaderboard_t582328223_0_0_0) };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t582328223_0_0_0 = { 1, GenInst_GcLeaderboard_t582328223_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementDescriptionU5BU5D_t2404093396_0_0_0_Types[] = { (&IAchievementDescriptionU5BU5D_t2404093396_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t2404093396_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t2404093396_0_0_0_Types };
static const RuntimeType* GenInst_IAchievementU5BU5D_t3105227985_0_0_0_Types[] = { (&IAchievementU5BU5D_t3105227985_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t3105227985_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t3105227985_0_0_0_Types };
static const RuntimeType* GenInst_IAchievement_t2790711152_0_0_0_Types[] = { (&IAchievement_t2790711152_0_0_0) };
extern const Il2CppGenericInst GenInst_IAchievement_t2790711152_0_0_0 = { 1, GenInst_IAchievement_t2790711152_0_0_0_Types };
static const RuntimeType* GenInst_GcAchievementData_t3971162273_0_0_0_Types[] = { (&GcAchievementData_t3971162273_0_0_0) };
extern const Il2CppGenericInst GenInst_GcAchievementData_t3971162273_0_0_0 = { 1, GenInst_GcAchievementData_t3971162273_0_0_0_Types };
static const RuntimeType* GenInst_Achievement_t1941630733_0_0_0_Types[] = { (&Achievement_t1941630733_0_0_0) };
extern const Il2CppGenericInst GenInst_Achievement_t1941630733_0_0_0 = { 1, GenInst_Achievement_t1941630733_0_0_0_Types };
static const RuntimeType* GenInst_IScoreU5BU5D_t3128097251_0_0_0_Types[] = { (&IScoreU5BU5D_t3128097251_0_0_0) };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t3128097251_0_0_0 = { 1, GenInst_IScoreU5BU5D_t3128097251_0_0_0_Types };
static const RuntimeType* GenInst_IScore_t1577383494_0_0_0_Types[] = { (&IScore_t1577383494_0_0_0) };
extern const Il2CppGenericInst GenInst_IScore_t1577383494_0_0_0 = { 1, GenInst_IScore_t1577383494_0_0_0_Types };
static const RuntimeType* GenInst_GcScoreData_t3049994916_0_0_0_Types[] = { (&GcScoreData_t3049994916_0_0_0) };
extern const Il2CppGenericInst GenInst_GcScoreData_t3049994916_0_0_0 = { 1, GenInst_GcScoreData_t3049994916_0_0_0_Types };
static const RuntimeType* GenInst_Score_t246219269_0_0_0_Types[] = { (&Score_t246219269_0_0_0) };
extern const Il2CppGenericInst GenInst_Score_t246219269_0_0_0 = { 1, GenInst_Score_t246219269_0_0_0_Types };
static const RuntimeType* GenInst_IUserProfileU5BU5D_t644998450_0_0_0_Types[] = { (&IUserProfileU5BU5D_t644998450_0_0_0) };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t644998450_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t644998450_0_0_0_Types };
static const RuntimeType* GenInst_DisallowMultipleComponent_t2944104030_0_0_0_Types[] = { (&DisallowMultipleComponent_t2944104030_0_0_0) };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t2944104030_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t2944104030_0_0_0_Types };
static const RuntimeType* GenInst_Attribute_t2169343654_0_0_0_Types[] = { (&Attribute_t2169343654_0_0_0) };
extern const Il2CppGenericInst GenInst_Attribute_t2169343654_0_0_0 = { 1, GenInst_Attribute_t2169343654_0_0_0_Types };
static const RuntimeType* GenInst__Attribute_t156294154_0_0_0_Types[] = { (&_Attribute_t156294154_0_0_0) };
extern const Il2CppGenericInst GenInst__Attribute_t156294154_0_0_0 = { 1, GenInst__Attribute_t156294154_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteInEditMode_t767168347_0_0_0_Types[] = { (&ExecuteInEditMode_t767168347_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t767168347_0_0_0 = { 1, GenInst_ExecuteInEditMode_t767168347_0_0_0_Types };
static const RuntimeType* GenInst_RequireComponent_t1008017238_0_0_0_Types[] = { (&RequireComponent_t1008017238_0_0_0) };
extern const Il2CppGenericInst GenInst_RequireComponent_t1008017238_0_0_0 = { 1, GenInst_RequireComponent_t1008017238_0_0_0_Types };
static const RuntimeType* GenInst_HitInfo_t2706273657_0_0_0_Types[] = { (&HitInfo_t2706273657_0_0_0) };
extern const Il2CppGenericInst GenInst_HitInfo_t2706273657_0_0_0 = { 1, GenInst_HitInfo_t2706273657_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 4, GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_PersistentCall_t2119335335_0_0_0_Types[] = { (&PersistentCall_t2119335335_0_0_0) };
extern const Il2CppGenericInst GenInst_PersistentCall_t2119335335_0_0_0 = { 1, GenInst_PersistentCall_t2119335335_0_0_0_Types };
static const RuntimeType* GenInst_BaseInvokableCall_t2985877092_0_0_0_Types[] = { (&BaseInvokableCall_t2985877092_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t2985877092_0_0_0 = { 1, GenInst_BaseInvokableCall_t2985877092_0_0_0_Types };
static const RuntimeType* GenInst_WorkRequest_t2264216812_0_0_0_Types[] = { (&WorkRequest_t2264216812_0_0_0) };
extern const Il2CppGenericInst GenInst_WorkRequest_t2264216812_0_0_0 = { 1, GenInst_WorkRequest_t2264216812_0_0_0_Types };
static const RuntimeType* GenInst_PlayableBinding_t150998691_0_0_0_Types[] = { (&PlayableBinding_t150998691_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableBinding_t150998691_0_0_0 = { 1, GenInst_PlayableBinding_t150998691_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t2381605113_0_0_0_Types[] = { (&MessageTypeSubscribers_t2381605113_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2381605113_0_0_0 = { 1, GenInst_MessageTypeSubscribers_t2381605113_0_0_0_Types };
static const RuntimeType* GenInst_MessageTypeSubscribers_t2381605113_0_0_0_Boolean_t1039239260_0_0_0_Types[] = { (&MessageTypeSubscribers_t2381605113_0_0_0), (&Boolean_t1039239260_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageTypeSubscribers_t2381605113_0_0_0_Boolean_t1039239260_0_0_0 = { 2, GenInst_MessageTypeSubscribers_t2381605113_0_0_0_Boolean_t1039239260_0_0_0_Types };
static const RuntimeType* GenInst_MessageEventArgs_t3322264806_0_0_0_Types[] = { (&MessageEventArgs_t3322264806_0_0_0) };
extern const Il2CppGenericInst GenInst_MessageEventArgs_t3322264806_0_0_0 = { 1, GenInst_MessageEventArgs_t3322264806_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t449294333_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t449294333_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t449294333_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_WeakReference_t449294333_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3050155971_0_0_0_Types[] = { (&KeyValuePair_2_t3050155971_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3050155971_0_0_0 = { 1, GenInst_KeyValuePair_2_t3050155971_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_IntPtr_t_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3050155971_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t3050155971_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3050155971_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3050155971_0_0_0_Types };
static const RuntimeType* GenInst_WeakReference_t449294333_0_0_0_Types[] = { (&WeakReference_t449294333_0_0_0) };
extern const Il2CppGenericInst GenInst_WeakReference_t449294333_0_0_0 = { 1, GenInst_WeakReference_t449294333_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t449294333_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t449294333_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t449294333_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t449294333_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t757546807_0_0_0_Types[] = { (&KeyValuePair_2_t757546807_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t757546807_0_0_0 = { 1, GenInst_KeyValuePair_2_t757546807_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_WeakReference_t449294333_0_0_0_KeyValuePair_2_t757546807_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&WeakReference_t449294333_0_0_0), (&KeyValuePair_2_t757546807_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_WeakReference_t449294333_0_0_0_KeyValuePair_2_t757546807_0_0_0 = { 3, GenInst_IntPtr_t_0_0_0_WeakReference_t449294333_0_0_0_KeyValuePair_2_t757546807_0_0_0_Types };
static const RuntimeType* GenInst_BaseInputModule_t4202312920_0_0_0_Types[] = { (&BaseInputModule_t4202312920_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInputModule_t4202312920_0_0_0 = { 1, GenInst_BaseInputModule_t4202312920_0_0_0_Types };
static const RuntimeType* GenInst_UIBehaviour_t3749981382_0_0_0_Types[] = { (&UIBehaviour_t3749981382_0_0_0) };
extern const Il2CppGenericInst GenInst_UIBehaviour_t3749981382_0_0_0 = { 1, GenInst_UIBehaviour_t3749981382_0_0_0_Types };
static const RuntimeType* GenInst_MonoBehaviour_t3755042618_0_0_0_Types[] = { (&MonoBehaviour_t3755042618_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t3755042618_0_0_0 = { 1, GenInst_MonoBehaviour_t3755042618_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t2363482100_0_0_0_Types[] = { (&RaycastResult_t2363482100_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t2363482100_0_0_0 = { 1, GenInst_RaycastResult_t2363482100_0_0_0_Types };
static const RuntimeType* GenInst_IDeselectHandler_t2317430855_0_0_0_Types[] = { (&IDeselectHandler_t2317430855_0_0_0) };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t2317430855_0_0_0 = { 1, GenInst_IDeselectHandler_t2317430855_0_0_0_Types };
static const RuntimeType* GenInst_IEventSystemHandler_t989066233_0_0_0_Types[] = { (&IEventSystemHandler_t989066233_0_0_0) };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t989066233_0_0_0 = { 1, GenInst_IEventSystemHandler_t989066233_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3112481958_0_0_0_Types[] = { (&List_1_t3112481958_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3112481958_0_0_0 = { 1, GenInst_List_1_t3112481958_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t570351926_0_0_0_Types[] = { (&List_1_t570351926_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t570351926_0_0_0 = { 1, GenInst_List_1_t570351926_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3976401388_0_0_0_Types[] = { (&List_1_t3976401388_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3976401388_0_0_0 = { 1, GenInst_List_1_t3976401388_0_0_0_Types };
static const RuntimeType* GenInst_ISelectHandler_t4128318009_0_0_0_Types[] = { (&ISelectHandler_t4128318009_0_0_0) };
extern const Il2CppGenericInst GenInst_ISelectHandler_t4128318009_0_0_0 = { 1, GenInst_ISelectHandler_t4128318009_0_0_0_Types };
static const RuntimeType* GenInst_BaseRaycaster_t3149871384_0_0_0_Types[] = { (&BaseRaycaster_t3149871384_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t3149871384_0_0_0 = { 1, GenInst_BaseRaycaster_t3149871384_0_0_0_Types };
static const RuntimeType* GenInst_Entry_t1923502037_0_0_0_Types[] = { (&Entry_t1923502037_0_0_0) };
extern const Il2CppGenericInst GenInst_Entry_t1923502037_0_0_0 = { 1, GenInst_Entry_t1923502037_0_0_0_Types };
static const RuntimeType* GenInst_BaseEventData_t2200158663_0_0_0_Types[] = { (&BaseEventData_t2200158663_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseEventData_t2200158663_0_0_0 = { 1, GenInst_BaseEventData_t2200158663_0_0_0_Types };
static const RuntimeType* GenInst_IPointerEnterHandler_t1206321535_0_0_0_Types[] = { (&IPointerEnterHandler_t1206321535_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t1206321535_0_0_0 = { 1, GenInst_IPointerEnterHandler_t1206321535_0_0_0_Types };
static const RuntimeType* GenInst_IPointerExitHandler_t563170456_0_0_0_Types[] = { (&IPointerExitHandler_t563170456_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t563170456_0_0_0 = { 1, GenInst_IPointerExitHandler_t563170456_0_0_0_Types };
static const RuntimeType* GenInst_IPointerDownHandler_t788657900_0_0_0_Types[] = { (&IPointerDownHandler_t788657900_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t788657900_0_0_0 = { 1, GenInst_IPointerDownHandler_t788657900_0_0_0_Types };
static const RuntimeType* GenInst_IPointerUpHandler_t3907224541_0_0_0_Types[] = { (&IPointerUpHandler_t3907224541_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t3907224541_0_0_0 = { 1, GenInst_IPointerUpHandler_t3907224541_0_0_0_Types };
static const RuntimeType* GenInst_IPointerClickHandler_t4166377497_0_0_0_Types[] = { (&IPointerClickHandler_t4166377497_0_0_0) };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t4166377497_0_0_0 = { 1, GenInst_IPointerClickHandler_t4166377497_0_0_0_Types };
static const RuntimeType* GenInst_IInitializePotentialDragHandler_t3159303760_0_0_0_Types[] = { (&IInitializePotentialDragHandler_t3159303760_0_0_0) };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t3159303760_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t3159303760_0_0_0_Types };
static const RuntimeType* GenInst_IBeginDragHandler_t4197993221_0_0_0_Types[] = { (&IBeginDragHandler_t4197993221_0_0_0) };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t4197993221_0_0_0 = { 1, GenInst_IBeginDragHandler_t4197993221_0_0_0_Types };
static const RuntimeType* GenInst_IDragHandler_t4019865541_0_0_0_Types[] = { (&IDragHandler_t4019865541_0_0_0) };
extern const Il2CppGenericInst GenInst_IDragHandler_t4019865541_0_0_0 = { 1, GenInst_IDragHandler_t4019865541_0_0_0_Types };
static const RuntimeType* GenInst_IEndDragHandler_t760648009_0_0_0_Types[] = { (&IEndDragHandler_t760648009_0_0_0) };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t760648009_0_0_0 = { 1, GenInst_IEndDragHandler_t760648009_0_0_0_Types };
static const RuntimeType* GenInst_IDropHandler_t3618352481_0_0_0_Types[] = { (&IDropHandler_t3618352481_0_0_0) };
extern const Il2CppGenericInst GenInst_IDropHandler_t3618352481_0_0_0 = { 1, GenInst_IDropHandler_t3618352481_0_0_0_Types };
static const RuntimeType* GenInst_IScrollHandler_t3841850943_0_0_0_Types[] = { (&IScrollHandler_t3841850943_0_0_0) };
extern const Il2CppGenericInst GenInst_IScrollHandler_t3841850943_0_0_0 = { 1, GenInst_IScrollHandler_t3841850943_0_0_0_Types };
static const RuntimeType* GenInst_IUpdateSelectedHandler_t3267774463_0_0_0_Types[] = { (&IUpdateSelectedHandler_t3267774463_0_0_0) };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t3267774463_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t3267774463_0_0_0_Types };
static const RuntimeType* GenInst_IMoveHandler_t2215200409_0_0_0_Types[] = { (&IMoveHandler_t2215200409_0_0_0) };
extern const Il2CppGenericInst GenInst_IMoveHandler_t2215200409_0_0_0 = { 1, GenInst_IMoveHandler_t2215200409_0_0_0_Types };
static const RuntimeType* GenInst_ISubmitHandler_t1318372016_0_0_0_Types[] = { (&ISubmitHandler_t1318372016_0_0_0) };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t1318372016_0_0_0 = { 1, GenInst_ISubmitHandler_t1318372016_0_0_0_Types };
static const RuntimeType* GenInst_ICancelHandler_t2259736822_0_0_0_Types[] = { (&ICancelHandler_t2259736822_0_0_0) };
extern const Il2CppGenericInst GenInst_ICancelHandler_t2259736822_0_0_0 = { 1, GenInst_ICancelHandler_t2259736822_0_0_0_Types };
static const RuntimeType* GenInst_Transform_t2735953680_0_0_0_Types[] = { (&Transform_t2735953680_0_0_0) };
extern const Il2CppGenericInst GenInst_Transform_t2735953680_0_0_0 = { 1, GenInst_Transform_t2735953680_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_t3093992626_0_0_0_Types[] = { (&GameObject_t3093992626_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_t3093992626_0_0_0 = { 1, GenInst_GameObject_t3093992626_0_0_0_Types };
static const RuntimeType* GenInst_BaseInput_t1368120269_0_0_0_Types[] = { (&BaseInput_t1368120269_0_0_0) };
extern const Il2CppGenericInst GenInst_BaseInput_t1368120269_0_0_0 = { 1, GenInst_BaseInput_t1368120269_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&PointerEventData_t1843710511_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0 = { 2, GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_Types };
static const RuntimeType* GenInst_PointerEventData_t1843710511_0_0_0_Types[] = { (&PointerEventData_t1843710511_0_0_0) };
extern const Il2CppGenericInst GenInst_PointerEventData_t1843710511_0_0_0 = { 1, GenInst_PointerEventData_t1843710511_0_0_0_Types };
static const RuntimeType* GenInst_AbstractEventData_t3634143719_0_0_0_Types[] = { (&AbstractEventData_t3634143719_0_0_0) };
extern const Il2CppGenericInst GenInst_AbstractEventData_t3634143719_0_0_0 = { 1, GenInst_AbstractEventData_t3634143719_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&PointerEventData_t1843710511_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1575498405_0_0_0_Types[] = { (&KeyValuePair_2_t1575498405_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1575498405_0_0_0 = { 1, GenInst_KeyValuePair_2_t1575498405_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_KeyValuePair_2_t1575498405_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&PointerEventData_t1843710511_0_0_0), (&KeyValuePair_2_t1575498405_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_KeyValuePair_2_t1575498405_0_0_0 = { 3, GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_KeyValuePair_2_t1575498405_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_PointerEventData_t1843710511_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&PointerEventData_t1843710511_0_0_0), (&PointerEventData_t1843710511_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_PointerEventData_t1843710511_0_0_0 = { 3, GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_PointerEventData_t1843710511_0_0_0_Types };
static const RuntimeType* GenInst_ButtonState_t1423469858_0_0_0_Types[] = { (&ButtonState_t1423469858_0_0_0) };
extern const Il2CppGenericInst GenInst_ButtonState_t1423469858_0_0_0 = { 1, GenInst_ButtonState_t1423469858_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t1064023118_0_0_0_Types[] = { (&ICanvasElement_t1064023118_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t1064023118_0_0_0 = { 1, GenInst_ICanvasElement_t1064023118_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t1064023118_0_0_0_Int32_t4116043316_0_0_0_Types[] = { (&ICanvasElement_t1064023118_0_0_0), (&Int32_t4116043316_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t1064023118_0_0_0_Int32_t4116043316_0_0_0 = { 2, GenInst_ICanvasElement_t1064023118_0_0_0_Int32_t4116043316_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t1064023118_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&ICanvasElement_t1064023118_0_0_0), (&Int32_t4116043316_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t1064023118_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_ICanvasElement_t1064023118_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_ColorBlock_t2548562872_0_0_0_Types[] = { (&ColorBlock_t2548562872_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorBlock_t2548562872_0_0_0 = { 1, GenInst_ColorBlock_t2548562872_0_0_0_Types };
static const RuntimeType* GenInst_OptionData_t2269286103_0_0_0_Types[] = { (&OptionData_t2269286103_0_0_0) };
extern const Il2CppGenericInst GenInst_OptionData_t2269286103_0_0_0 = { 1, GenInst_OptionData_t2269286103_0_0_0_Types };
static const RuntimeType* GenInst_DropdownItem_t706906876_0_0_0_Types[] = { (&DropdownItem_t706906876_0_0_0) };
extern const Il2CppGenericInst GenInst_DropdownItem_t706906876_0_0_0 = { 1, GenInst_DropdownItem_t706906876_0_0_0_Types };
static const RuntimeType* GenInst_FloatTween_t1018971280_0_0_0_Types[] = { (&FloatTween_t1018971280_0_0_0) };
extern const Il2CppGenericInst GenInst_FloatTween_t1018971280_0_0_0 = { 1, GenInst_FloatTween_t1018971280_0_0_0_Types };
static const RuntimeType* GenInst_Sprite_t360607379_0_0_0_Types[] = { (&Sprite_t360607379_0_0_0) };
extern const Il2CppGenericInst GenInst_Sprite_t360607379_0_0_0 = { 1, GenInst_Sprite_t360607379_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t452078485_0_0_0_Types[] = { (&Canvas_t452078485_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t452078485_0_0_0 = { 1, GenInst_Canvas_t452078485_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2575494210_0_0_0_Types[] = { (&List_1_t2575494210_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2575494210_0_0_0 = { 1, GenInst_List_1_t2575494210_0_0_0_Types };
static const RuntimeType* GenInst_Font_t1312336880_0_0_0_HashSet_1_t64826019_0_0_0_Types[] = { (&Font_t1312336880_0_0_0), (&HashSet_1_t64826019_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t1312336880_0_0_0_HashSet_1_t64826019_0_0_0 = { 2, GenInst_Font_t1312336880_0_0_0_HashSet_1_t64826019_0_0_0_Types };
static const RuntimeType* GenInst_Text_t1469493361_0_0_0_Types[] = { (&Text_t1469493361_0_0_0) };
extern const Il2CppGenericInst GenInst_Text_t1469493361_0_0_0 = { 1, GenInst_Text_t1469493361_0_0_0_Types };
static const RuntimeType* GenInst_Link_t79741401_0_0_0_Types[] = { (&Link_t79741401_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t79741401_0_0_0 = { 1, GenInst_Link_t79741401_0_0_0_Types };
static const RuntimeType* GenInst_ILayoutElement_t3747613007_0_0_0_Types[] = { (&ILayoutElement_t3747613007_0_0_0) };
extern const Il2CppGenericInst GenInst_ILayoutElement_t3747613007_0_0_0 = { 1, GenInst_ILayoutElement_t3747613007_0_0_0_Types };
static const RuntimeType* GenInst_MaskableGraphic_t2710636079_0_0_0_Types[] = { (&MaskableGraphic_t2710636079_0_0_0) };
extern const Il2CppGenericInst GenInst_MaskableGraphic_t2710636079_0_0_0 = { 1, GenInst_MaskableGraphic_t2710636079_0_0_0_Types };
static const RuntimeType* GenInst_IClippable_t617981727_0_0_0_Types[] = { (&IClippable_t617981727_0_0_0) };
extern const Il2CppGenericInst GenInst_IClippable_t617981727_0_0_0 = { 1, GenInst_IClippable_t617981727_0_0_0_Types };
static const RuntimeType* GenInst_IMaskable_t865684376_0_0_0_Types[] = { (&IMaskable_t865684376_0_0_0) };
extern const Il2CppGenericInst GenInst_IMaskable_t865684376_0_0_0 = { 1, GenInst_IMaskable_t865684376_0_0_0_Types };
static const RuntimeType* GenInst_IMaterialModifier_t3524121170_0_0_0_Types[] = { (&IMaterialModifier_t3524121170_0_0_0) };
extern const Il2CppGenericInst GenInst_IMaterialModifier_t3524121170_0_0_0 = { 1, GenInst_IMaterialModifier_t3524121170_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t366741146_0_0_0_Types[] = { (&Graphic_t366741146_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t366741146_0_0_0 = { 1, GenInst_Graphic_t366741146_0_0_0_Types };
static const RuntimeType* GenInst_HashSet_1_t64826019_0_0_0_Types[] = { (&HashSet_1_t64826019_0_0_0) };
extern const Il2CppGenericInst GenInst_HashSet_1_t64826019_0_0_0 = { 1, GenInst_HashSet_1_t64826019_0_0_0_Types };
static const RuntimeType* GenInst_Font_t1312336880_0_0_0_HashSet_1_t64826019_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Font_t1312336880_0_0_0), (&HashSet_1_t64826019_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t1312336880_0_0_0_HashSet_1_t64826019_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Font_t1312336880_0_0_0_HashSet_1_t64826019_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3536216813_0_0_0_Types[] = { (&KeyValuePair_2_t3536216813_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3536216813_0_0_0 = { 1, GenInst_KeyValuePair_2_t3536216813_0_0_0_Types };
static const RuntimeType* GenInst_Font_t1312336880_0_0_0_HashSet_1_t64826019_0_0_0_KeyValuePair_2_t3536216813_0_0_0_Types[] = { (&Font_t1312336880_0_0_0), (&HashSet_1_t64826019_0_0_0), (&KeyValuePair_2_t3536216813_0_0_0) };
extern const Il2CppGenericInst GenInst_Font_t1312336880_0_0_0_HashSet_1_t64826019_0_0_0_KeyValuePair_2_t3536216813_0_0_0 = { 3, GenInst_Font_t1312336880_0_0_0_HashSet_1_t64826019_0_0_0_KeyValuePair_2_t3536216813_0_0_0_Types };
static const RuntimeType* GenInst_ColorTween_t3858592911_0_0_0_Types[] = { (&ColorTween_t3858592911_0_0_0) };
extern const Il2CppGenericInst GenInst_ColorTween_t3858592911_0_0_0 = { 1, GenInst_ColorTween_t3858592911_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t452078485_0_0_0_IndexedSet_1_t3523870668_0_0_0_Types[] = { (&Canvas_t452078485_0_0_0), (&IndexedSet_1_t3523870668_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t452078485_0_0_0_IndexedSet_1_t3523870668_0_0_0 = { 2, GenInst_Canvas_t452078485_0_0_0_IndexedSet_1_t3523870668_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t366741146_0_0_0_Int32_t4116043316_0_0_0_Types[] = { (&Graphic_t366741146_0_0_0), (&Int32_t4116043316_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t366741146_0_0_0_Int32_t4116043316_0_0_0 = { 2, GenInst_Graphic_t366741146_0_0_0_Int32_t4116043316_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t366741146_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Graphic_t366741146_0_0_0), (&Int32_t4116043316_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t366741146_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Graphic_t366741146_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t3523870668_0_0_0_Types[] = { (&IndexedSet_1_t3523870668_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t3523870668_0_0_0 = { 1, GenInst_IndexedSet_1_t3523870668_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t452078485_0_0_0_IndexedSet_1_t3523870668_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Canvas_t452078485_0_0_0), (&IndexedSet_1_t3523870668_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t452078485_0_0_0_IndexedSet_1_t3523870668_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Canvas_t452078485_0_0_0_IndexedSet_1_t3523870668_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3649819709_0_0_0_Types[] = { (&KeyValuePair_2_t3649819709_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3649819709_0_0_0 = { 1, GenInst_KeyValuePair_2_t3649819709_0_0_0_Types };
static const RuntimeType* GenInst_Canvas_t452078485_0_0_0_IndexedSet_1_t3523870668_0_0_0_KeyValuePair_2_t3649819709_0_0_0_Types[] = { (&Canvas_t452078485_0_0_0), (&IndexedSet_1_t3523870668_0_0_0), (&KeyValuePair_2_t3649819709_0_0_0) };
extern const Il2CppGenericInst GenInst_Canvas_t452078485_0_0_0_IndexedSet_1_t3523870668_0_0_0_KeyValuePair_2_t3649819709_0_0_0 = { 3, GenInst_Canvas_t452078485_0_0_0_IndexedSet_1_t3523870668_0_0_0_KeyValuePair_2_t3649819709_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t241250796_0_0_0_Types[] = { (&KeyValuePair_2_t241250796_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t241250796_0_0_0 = { 1, GenInst_KeyValuePair_2_t241250796_0_0_0_Types };
static const RuntimeType* GenInst_Graphic_t366741146_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t241250796_0_0_0_Types[] = { (&Graphic_t366741146_0_0_0), (&Int32_t4116043316_0_0_0), (&KeyValuePair_2_t241250796_0_0_0) };
extern const Il2CppGenericInst GenInst_Graphic_t366741146_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t241250796_0_0_0 = { 3, GenInst_Graphic_t366741146_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t241250796_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3657347816_0_0_0_Types[] = { (&KeyValuePair_2_t3657347816_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3657347816_0_0_0 = { 1, GenInst_KeyValuePair_2_t3657347816_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasElement_t1064023118_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t3657347816_0_0_0_Types[] = { (&ICanvasElement_t1064023118_0_0_0), (&Int32_t4116043316_0_0_0), (&KeyValuePair_2_t3657347816_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasElement_t1064023118_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t3657347816_0_0_0 = { 3, GenInst_ICanvasElement_t1064023118_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t3657347816_0_0_0_Types };
static const RuntimeType* GenInst_Type_t3593005433_0_0_0_Types[] = { (&Type_t3593005433_0_0_0) };
extern const Il2CppGenericInst GenInst_Type_t3593005433_0_0_0 = { 1, GenInst_Type_t3593005433_0_0_0_Types };
static const RuntimeType* GenInst_FillMethod_t2569813474_0_0_0_Types[] = { (&FillMethod_t2569813474_0_0_0) };
extern const Il2CppGenericInst GenInst_FillMethod_t2569813474_0_0_0 = { 1, GenInst_FillMethod_t2569813474_0_0_0_Types };
static const RuntimeType* GenInst_ContentType_t2659363712_0_0_0_Types[] = { (&ContentType_t2659363712_0_0_0) };
extern const Il2CppGenericInst GenInst_ContentType_t2659363712_0_0_0 = { 1, GenInst_ContentType_t2659363712_0_0_0_Types };
static const RuntimeType* GenInst_LineType_t1397773182_0_0_0_Types[] = { (&LineType_t1397773182_0_0_0) };
extern const Il2CppGenericInst GenInst_LineType_t1397773182_0_0_0 = { 1, GenInst_LineType_t1397773182_0_0_0_Types };
static const RuntimeType* GenInst_InputType_t1682781151_0_0_0_Types[] = { (&InputType_t1682781151_0_0_0) };
extern const Il2CppGenericInst GenInst_InputType_t1682781151_0_0_0 = { 1, GenInst_InputType_t1682781151_0_0_0_Types };
static const RuntimeType* GenInst_TouchScreenKeyboardType_t3909346333_0_0_0_Types[] = { (&TouchScreenKeyboardType_t3909346333_0_0_0) };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t3909346333_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t3909346333_0_0_0_Types };
static const RuntimeType* GenInst_CharacterValidation_t2407825395_0_0_0_Types[] = { (&CharacterValidation_t2407825395_0_0_0) };
extern const Il2CppGenericInst GenInst_CharacterValidation_t2407825395_0_0_0 = { 1, GenInst_CharacterValidation_t2407825395_0_0_0_Types };
static const RuntimeType* GenInst_Mask_t1104295890_0_0_0_Types[] = { (&Mask_t1104295890_0_0_0) };
extern const Il2CppGenericInst GenInst_Mask_t1104295890_0_0_0 = { 1, GenInst_Mask_t1104295890_0_0_0_Types };
static const RuntimeType* GenInst_ICanvasRaycastFilter_t640459987_0_0_0_Types[] = { (&ICanvasRaycastFilter_t640459987_0_0_0) };
extern const Il2CppGenericInst GenInst_ICanvasRaycastFilter_t640459987_0_0_0 = { 1, GenInst_ICanvasRaycastFilter_t640459987_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3227711615_0_0_0_Types[] = { (&List_1_t3227711615_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3227711615_0_0_0 = { 1, GenInst_List_1_t3227711615_0_0_0_Types };
static const RuntimeType* GenInst_RectMask2D_t475906393_0_0_0_Types[] = { (&RectMask2D_t475906393_0_0_0) };
extern const Il2CppGenericInst GenInst_RectMask2D_t475906393_0_0_0 = { 1, GenInst_RectMask2D_t475906393_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t4180951425_0_0_0_Types[] = { (&IClipper_t4180951425_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t4180951425_0_0_0 = { 1, GenInst_IClipper_t4180951425_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2599322118_0_0_0_Types[] = { (&List_1_t2599322118_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2599322118_0_0_0 = { 1, GenInst_List_1_t2599322118_0_0_0_Types };
static const RuntimeType* GenInst_Navigation_t2074156770_0_0_0_Types[] = { (&Navigation_t2074156770_0_0_0) };
extern const Il2CppGenericInst GenInst_Navigation_t2074156770_0_0_0 = { 1, GenInst_Navigation_t2074156770_0_0_0_Types };
static const RuntimeType* GenInst_Link_t3523197063_0_0_0_Types[] = { (&Link_t3523197063_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t3523197063_0_0_0 = { 1, GenInst_Link_t3523197063_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t2341802389_0_0_0_Types[] = { (&Direction_t2341802389_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t2341802389_0_0_0 = { 1, GenInst_Direction_t2341802389_0_0_0_Types };
static const RuntimeType* GenInst_Selectable_t301191868_0_0_0_Types[] = { (&Selectable_t301191868_0_0_0) };
extern const Il2CppGenericInst GenInst_Selectable_t301191868_0_0_0 = { 1, GenInst_Selectable_t301191868_0_0_0_Types };
static const RuntimeType* GenInst_Transition_t2430538793_0_0_0_Types[] = { (&Transition_t2430538793_0_0_0) };
extern const Il2CppGenericInst GenInst_Transition_t2430538793_0_0_0 = { 1, GenInst_Transition_t2430538793_0_0_0_Types };
static const RuntimeType* GenInst_SpriteState_t4104548053_0_0_0_Types[] = { (&SpriteState_t4104548053_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteState_t4104548053_0_0_0 = { 1, GenInst_SpriteState_t4104548053_0_0_0_Types };
static const RuntimeType* GenInst_CanvasGroup_t514076778_0_0_0_Types[] = { (&CanvasGroup_t514076778_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasGroup_t514076778_0_0_0 = { 1, GenInst_CanvasGroup_t514076778_0_0_0_Types };
static const RuntimeType* GenInst_Direction_t3623476438_0_0_0_Types[] = { (&Direction_t3623476438_0_0_0) };
extern const Il2CppGenericInst GenInst_Direction_t3623476438_0_0_0 = { 1, GenInst_Direction_t3623476438_0_0_0_Types };
static const RuntimeType* GenInst_MatEntry_t961266217_0_0_0_Types[] = { (&MatEntry_t961266217_0_0_0) };
extern const Il2CppGenericInst GenInst_MatEntry_t961266217_0_0_0 = { 1, GenInst_MatEntry_t961266217_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t1097858442_0_0_0_Types[] = { (&Toggle_t1097858442_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t1097858442_0_0_0 = { 1, GenInst_Toggle_t1097858442_0_0_0_Types };
static const RuntimeType* GenInst_Toggle_t1097858442_0_0_0_Boolean_t1039239260_0_0_0_Types[] = { (&Toggle_t1097858442_0_0_0), (&Boolean_t1039239260_0_0_0) };
extern const Il2CppGenericInst GenInst_Toggle_t1097858442_0_0_0_Boolean_t1039239260_0_0_0 = { 2, GenInst_Toggle_t1097858442_0_0_0_Boolean_t1039239260_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t4180951425_0_0_0_Int32_t4116043316_0_0_0_Types[] = { (&IClipper_t4180951425_0_0_0), (&Int32_t4116043316_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t4180951425_0_0_0_Int32_t4116043316_0_0_0 = { 2, GenInst_IClipper_t4180951425_0_0_0_Int32_t4116043316_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t4180951425_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&IClipper_t4180951425_0_0_0), (&Int32_t4116043316_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t4180951425_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_IClipper_t4180951425_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t699909001_0_0_0_Types[] = { (&KeyValuePair_2_t699909001_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t699909001_0_0_0 = { 1, GenInst_KeyValuePair_2_t699909001_0_0_0_Types };
static const RuntimeType* GenInst_IClipper_t4180951425_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t699909001_0_0_0_Types[] = { (&IClipper_t4180951425_0_0_0), (&Int32_t4116043316_0_0_0), (&KeyValuePair_2_t699909001_0_0_0) };
extern const Il2CppGenericInst GenInst_IClipper_t4180951425_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t699909001_0_0_0 = { 3, GenInst_IClipper_t4180951425_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t699909001_0_0_0_Types };
static const RuntimeType* GenInst_AspectMode_t757271725_0_0_0_Types[] = { (&AspectMode_t757271725_0_0_0) };
extern const Il2CppGenericInst GenInst_AspectMode_t757271725_0_0_0 = { 1, GenInst_AspectMode_t757271725_0_0_0_Types };
static const RuntimeType* GenInst_FitMode_t1874677476_0_0_0_Types[] = { (&FitMode_t1874677476_0_0_0) };
extern const Il2CppGenericInst GenInst_FitMode_t1874677476_0_0_0 = { 1, GenInst_FitMode_t1874677476_0_0_0_Types };
static const RuntimeType* GenInst_RectTransform_t1087788885_0_0_0_Types[] = { (&RectTransform_t1087788885_0_0_0) };
extern const Il2CppGenericInst GenInst_RectTransform_t1087788885_0_0_0 = { 1, GenInst_RectTransform_t1087788885_0_0_0_Types };
static const RuntimeType* GenInst_LayoutRebuilder_t3161446486_0_0_0_Types[] = { (&LayoutRebuilder_t3161446486_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t3161446486_0_0_0 = { 1, GenInst_LayoutRebuilder_t3161446486_0_0_0_Types };
static const RuntimeType* GenInst_ILayoutElement_t3747613007_0_0_0_Single_t1534300504_0_0_0_Types[] = { (&ILayoutElement_t3747613007_0_0_0), (&Single_t1534300504_0_0_0) };
extern const Il2CppGenericInst GenInst_ILayoutElement_t3747613007_0_0_0_Single_t1534300504_0_0_0 = { 2, GenInst_ILayoutElement_t3747613007_0_0_0_Single_t1534300504_0_0_0_Types };
static const RuntimeType* GenInst_RuntimeObject_0_0_0_Single_t1534300504_0_0_0_Types[] = { (&RuntimeObject_0_0_0), (&Single_t1534300504_0_0_0) };
extern const Il2CppGenericInst GenInst_RuntimeObject_0_0_0_Single_t1534300504_0_0_0 = { 2, GenInst_RuntimeObject_0_0_0_Single_t1534300504_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t654123220_0_0_0_Types[] = { (&List_1_t654123220_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t654123220_0_0_0 = { 1, GenInst_List_1_t654123220_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t239736144_0_0_0_Types[] = { (&List_1_t239736144_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t239736144_0_0_0 = { 1, GenInst_List_1_t239736144_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t3188465817_0_0_0_Types[] = { (&List_1_t3188465817_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t3188465817_0_0_0 = { 1, GenInst_List_1_t3188465817_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t479652782_0_0_0_Types[] = { (&List_1_t479652782_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t479652782_0_0_0 = { 1, GenInst_List_1_t479652782_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1944491745_0_0_0_Types[] = { (&List_1_t1944491745_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1944491745_0_0_0 = { 1, GenInst_List_1_t1944491745_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2598460513_0_0_0_Types[] = { (&List_1_t2598460513_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2598460513_0_0_0 = { 1, GenInst_List_1_t2598460513_0_0_0_Types };
static const RuntimeType* GenInst_AtlasAsset_t2525633250_0_0_0_Types[] = { (&AtlasAsset_t2525633250_0_0_0) };
extern const Il2CppGenericInst GenInst_AtlasAsset_t2525633250_0_0_0 = { 1, GenInst_AtlasAsset_t2525633250_0_0_0_Types };
static const RuntimeType* GenInst_ScriptableObject_t229319923_0_0_0_Types[] = { (&ScriptableObject_t229319923_0_0_0) };
extern const Il2CppGenericInst GenInst_ScriptableObject_t229319923_0_0_0 = { 1, GenInst_ScriptableObject_t229319923_0_0_0_Types };
static const RuntimeType* GenInst_SlotRegionPair_t2265310044_0_0_0_Types[] = { (&SlotRegionPair_t2265310044_0_0_0) };
extern const Il2CppGenericInst GenInst_SlotRegionPair_t2265310044_0_0_0 = { 1, GenInst_SlotRegionPair_t2265310044_0_0_0_Types };
static const RuntimeType* GenInst_Texture_t4046637001_0_0_0_AtlasPage_t3729449121_0_0_0_Types[] = { (&Texture_t4046637001_0_0_0), (&AtlasPage_t3729449121_0_0_0) };
extern const Il2CppGenericInst GenInst_Texture_t4046637001_0_0_0_AtlasPage_t3729449121_0_0_0 = { 2, GenInst_Texture_t4046637001_0_0_0_AtlasPage_t3729449121_0_0_0_Types };
static const RuntimeType* GenInst_AtlasPage_t3729449121_0_0_0_Types[] = { (&AtlasPage_t3729449121_0_0_0) };
extern const Il2CppGenericInst GenInst_AtlasPage_t3729449121_0_0_0 = { 1, GenInst_AtlasPage_t3729449121_0_0_0_Types };
static const RuntimeType* GenInst_Texture_t4046637001_0_0_0_AtlasPage_t3729449121_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Texture_t4046637001_0_0_0), (&AtlasPage_t3729449121_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Texture_t4046637001_0_0_0_AtlasPage_t3729449121_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Texture_t4046637001_0_0_0_AtlasPage_t3729449121_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1286815118_0_0_0_Types[] = { (&KeyValuePair_2_t1286815118_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1286815118_0_0_0 = { 1, GenInst_KeyValuePair_2_t1286815118_0_0_0_Types };
static const RuntimeType* GenInst_Texture_t4046637001_0_0_0_AtlasPage_t3729449121_0_0_0_KeyValuePair_2_t1286815118_0_0_0_Types[] = { (&Texture_t4046637001_0_0_0), (&AtlasPage_t3729449121_0_0_0), (&KeyValuePair_2_t1286815118_0_0_0) };
extern const Il2CppGenericInst GenInst_Texture_t4046637001_0_0_0_AtlasPage_t3729449121_0_0_0_KeyValuePair_2_t1286815118_0_0_0 = { 3, GenInst_Texture_t4046637001_0_0_0_AtlasPage_t3729449121_0_0_0_KeyValuePair_2_t1286815118_0_0_0_Types };
static const RuntimeType* GenInst_Animation_t2067143511_0_0_0_SkeletonAnimation_t2012766265_0_0_0_Types[] = { (&Animation_t2067143511_0_0_0), (&SkeletonAnimation_t2012766265_0_0_0) };
extern const Il2CppGenericInst GenInst_Animation_t2067143511_0_0_0_SkeletonAnimation_t2012766265_0_0_0 = { 2, GenInst_Animation_t2067143511_0_0_0_SkeletonAnimation_t2012766265_0_0_0_Types };
static const RuntimeType* GenInst_Animation_t2067143511_0_0_0_Types[] = { (&Animation_t2067143511_0_0_0) };
extern const Il2CppGenericInst GenInst_Animation_t2067143511_0_0_0 = { 1, GenInst_Animation_t2067143511_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonAnimation_t2012766265_0_0_0_Types[] = { (&SkeletonAnimation_t2012766265_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonAnimation_t2012766265_0_0_0 = { 1, GenInst_SkeletonAnimation_t2012766265_0_0_0_Types };
static const RuntimeType* GenInst_ISkeletonAnimation_t2072256521_0_0_0_Types[] = { (&ISkeletonAnimation_t2072256521_0_0_0) };
extern const Il2CppGenericInst GenInst_ISkeletonAnimation_t2072256521_0_0_0 = { 1, GenInst_ISkeletonAnimation_t2072256521_0_0_0_Types };
static const RuntimeType* GenInst_IAnimationStateComponent_t2249916977_0_0_0_Types[] = { (&IAnimationStateComponent_t2249916977_0_0_0) };
extern const Il2CppGenericInst GenInst_IAnimationStateComponent_t2249916977_0_0_0 = { 1, GenInst_IAnimationStateComponent_t2249916977_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonRenderer_t1681389628_0_0_0_Types[] = { (&SkeletonRenderer_t1681389628_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonRenderer_t1681389628_0_0_0 = { 1, GenInst_SkeletonRenderer_t1681389628_0_0_0_Types };
static const RuntimeType* GenInst_ISkeletonComponent_t3641217750_0_0_0_Types[] = { (&ISkeletonComponent_t3641217750_0_0_0) };
extern const Il2CppGenericInst GenInst_ISkeletonComponent_t3641217750_0_0_0 = { 1, GenInst_ISkeletonComponent_t3641217750_0_0_0_Types };
static const RuntimeType* GenInst_ISkeletonDataAssetComponent_t3477846014_0_0_0_Types[] = { (&ISkeletonDataAssetComponent_t3477846014_0_0_0) };
extern const Il2CppGenericInst GenInst_ISkeletonDataAssetComponent_t3477846014_0_0_0 = { 1, GenInst_ISkeletonDataAssetComponent_t3477846014_0_0_0_Types };
static const RuntimeType* GenInst_Animation_t2067143511_0_0_0_SkeletonAnimation_t2012766265_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Animation_t2067143511_0_0_0), (&SkeletonAnimation_t2012766265_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Animation_t2067143511_0_0_0_SkeletonAnimation_t2012766265_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Animation_t2067143511_0_0_0_SkeletonAnimation_t2012766265_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t851776416_0_0_0_Types[] = { (&KeyValuePair_2_t851776416_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t851776416_0_0_0 = { 1, GenInst_KeyValuePair_2_t851776416_0_0_0_Types };
static const RuntimeType* GenInst_Animation_t2067143511_0_0_0_SkeletonAnimation_t2012766265_0_0_0_KeyValuePair_2_t851776416_0_0_0_Types[] = { (&Animation_t2067143511_0_0_0), (&SkeletonAnimation_t2012766265_0_0_0), (&KeyValuePair_2_t851776416_0_0_0) };
extern const Il2CppGenericInst GenInst_Animation_t2067143511_0_0_0_SkeletonAnimation_t2012766265_0_0_0_KeyValuePair_2_t851776416_0_0_0 = { 3, GenInst_Animation_t2067143511_0_0_0_SkeletonAnimation_t2012766265_0_0_0_KeyValuePair_2_t851776416_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Animation_t2067143511_0_0_0_Types[] = { (&String_t_0_0_0), (&Animation_t2067143511_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Animation_t2067143511_0_0_0 = { 2, GenInst_String_t_0_0_0_Animation_t2067143511_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Animation_t2067143511_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&String_t_0_0_0), (&Animation_t2067143511_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Animation_t2067143511_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_String_t_0_0_0_Animation_t2067143511_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2174300370_0_0_0_Types[] = { (&KeyValuePair_2_t2174300370_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2174300370_0_0_0 = { 1, GenInst_KeyValuePair_2_t2174300370_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_Animation_t2067143511_0_0_0_KeyValuePair_2_t2174300370_0_0_0_Types[] = { (&String_t_0_0_0), (&Animation_t2067143511_0_0_0), (&KeyValuePair_2_t2174300370_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Animation_t2067143511_0_0_0_KeyValuePair_2_t2174300370_0_0_0 = { 3, GenInst_String_t_0_0_0_Animation_t2067143511_0_0_0_KeyValuePair_2_t2174300370_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonDataAsset_t1706375087_0_0_0_Types[] = { (&SkeletonDataAsset_t1706375087_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonDataAsset_t1706375087_0_0_0 = { 1, GenInst_SkeletonDataAsset_t1706375087_0_0_0_Types };
static const RuntimeType* GenInst_SlotSettings_t3151105826_0_0_0_Types[] = { (&SlotSettings_t3151105826_0_0_0) };
extern const Il2CppGenericInst GenInst_SlotSettings_t3151105826_0_0_0 = { 1, GenInst_SlotSettings_t3151105826_0_0_0_Types };
static const RuntimeType* GenInst_EventPair_t1359900303_0_0_0_Types[] = { (&EventPair_t1359900303_0_0_0) };
extern const Il2CppGenericInst GenInst_EventPair_t1359900303_0_0_0 = { 1, GenInst_EventPair_t1359900303_0_0_0_Types };
static const RuntimeType* GenInst_Event_t27715519_0_0_0_Types[] = { (&Event_t27715519_0_0_0) };
extern const Il2CppGenericInst GenInst_Event_t27715519_0_0_0 = { 1, GenInst_Event_t27715519_0_0_0_Types };
static const RuntimeType* GenInst_Timeline_t3562138029_0_0_0_Types[] = { (&Timeline_t3562138029_0_0_0) };
extern const Il2CppGenericInst GenInst_Timeline_t3562138029_0_0_0 = { 1, GenInst_Timeline_t3562138029_0_0_0_Types };
static const RuntimeType* GenInst_Bone_t1763862836_0_0_0_Types[] = { (&Bone_t1763862836_0_0_0) };
extern const Il2CppGenericInst GenInst_Bone_t1763862836_0_0_0 = { 1, GenInst_Bone_t1763862836_0_0_0_Types };
static const RuntimeType* GenInst_IUpdatable_t125710159_0_0_0_Types[] = { (&IUpdatable_t125710159_0_0_0) };
extern const Il2CppGenericInst GenInst_IUpdatable_t125710159_0_0_0 = { 1, GenInst_IUpdatable_t125710159_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t1804116343_0_0_0_Types[] = { (&Slot_t1804116343_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t1804116343_0_0_0 = { 1, GenInst_Slot_t1804116343_0_0_0_Types };
static const RuntimeType* GenInst_SingleU5BU5D_t3989243465_0_0_0_Types[] = { (&SingleU5BU5D_t3989243465_0_0_0) };
extern const Il2CppGenericInst GenInst_SingleU5BU5D_t3989243465_0_0_0 = { 1, GenInst_SingleU5BU5D_t3989243465_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t4235090879_0_0_0_Types[] = { (&IList_1_t4235090879_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t4235090879_0_0_0 = { 1, GenInst_IList_1_t4235090879_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t2002696951_0_0_0_Types[] = { (&ICollection_1_t2002696951_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t2002696951_0_0_0 = { 1, GenInst_ICollection_1_t2002696951_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3168598293_0_0_0_Types[] = { (&IEnumerable_1_t3168598293_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3168598293_0_0_0 = { 1, GenInst_IEnumerable_1_t3168598293_0_0_0_Types };
static const RuntimeType* GenInst_Int32U5BU5D_t281224829_0_0_0_Types[] = { (&Int32U5BU5D_t281224829_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32U5BU5D_t281224829_0_0_0 = { 1, GenInst_Int32U5BU5D_t281224829_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t2521866395_0_0_0_Types[] = { (&IList_1_t2521866395_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t2521866395_0_0_0 = { 1, GenInst_IList_1_t2521866395_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t289472467_0_0_0_Types[] = { (&ICollection_1_t289472467_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t289472467_0_0_0 = { 1, GenInst_ICollection_1_t289472467_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t1455373809_0_0_0_Types[] = { (&IEnumerable_1_t1455373809_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t1455373809_0_0_0 = { 1, GenInst_IEnumerable_1_t1455373809_0_0_0_Types };
static const RuntimeType* GenInst_IkConstraint_t2514691977_0_0_0_Types[] = { (&IkConstraint_t2514691977_0_0_0) };
extern const Il2CppGenericInst GenInst_IkConstraint_t2514691977_0_0_0 = { 1, GenInst_IkConstraint_t2514691977_0_0_0_Types };
static const RuntimeType* GenInst_IConstraint_t900275133_0_0_0_Types[] = { (&IConstraint_t900275133_0_0_0) };
extern const Il2CppGenericInst GenInst_IConstraint_t900275133_0_0_0 = { 1, GenInst_IConstraint_t900275133_0_0_0_Types };
static const RuntimeType* GenInst_TransformConstraint_t509539004_0_0_0_Types[] = { (&TransformConstraint_t509539004_0_0_0) };
extern const Il2CppGenericInst GenInst_TransformConstraint_t509539004_0_0_0 = { 1, GenInst_TransformConstraint_t509539004_0_0_0_Types };
static const RuntimeType* GenInst_PathConstraint_t1854663405_0_0_0_Types[] = { (&PathConstraint_t1854663405_0_0_0) };
extern const Il2CppGenericInst GenInst_PathConstraint_t1854663405_0_0_0 = { 1, GenInst_PathConstraint_t1854663405_0_0_0_Types };
static const RuntimeType* GenInst_TrackEntry_t4062297782_0_0_0_Types[] = { (&TrackEntry_t4062297782_0_0_0) };
extern const Il2CppGenericInst GenInst_TrackEntry_t4062297782_0_0_0 = { 1, GenInst_TrackEntry_t4062297782_0_0_0_Types };
static const RuntimeType* GenInst_IPoolable_t612200214_0_0_0_Types[] = { (&IPoolable_t612200214_0_0_0) };
extern const Il2CppGenericInst GenInst_IPoolable_t612200214_0_0_0 = { 1, GenInst_IPoolable_t612200214_0_0_0_Types };
static const RuntimeType* GenInst_Link_t2726291356_0_0_0_Types[] = { (&Link_t2726291356_0_0_0) };
extern const Il2CppGenericInst GenInst_Link_t2726291356_0_0_0 = { 1, GenInst_Link_t2726291356_0_0_0_Types };
static const RuntimeType* GenInst_EventQueueEntry_t3756679084_0_0_0_Types[] = { (&EventQueueEntry_t3756679084_0_0_0) };
extern const Il2CppGenericInst GenInst_EventQueueEntry_t3756679084_0_0_0 = { 1, GenInst_EventQueueEntry_t3756679084_0_0_0_Types };
static const RuntimeType* GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_Types[] = { (&AnimationPair_t2435898854_0_0_0), (&Single_t1534300504_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0 = { 2, GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t914961172_0_0_0_Types[] = { (&KeyValuePair_2_t914961172_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t914961172_0_0_0 = { 1, GenInst_KeyValuePair_2_t914961172_0_0_0_Types };
static const RuntimeType* GenInst_AnimationPair_t2435898854_0_0_0_Types[] = { (&AnimationPair_t2435898854_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationPair_t2435898854_0_0_0 = { 1, GenInst_AnimationPair_t2435898854_0_0_0_Types };
static const RuntimeType* GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_AnimationPair_t2435898854_0_0_0_Types[] = { (&AnimationPair_t2435898854_0_0_0), (&Single_t1534300504_0_0_0), (&AnimationPair_t2435898854_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_AnimationPair_t2435898854_0_0_0 = { 3, GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_AnimationPair_t2435898854_0_0_0_Types };
static const RuntimeType* GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_Single_t1534300504_0_0_0_Types[] = { (&AnimationPair_t2435898854_0_0_0), (&Single_t1534300504_0_0_0), (&Single_t1534300504_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_Single_t1534300504_0_0_0 = { 3, GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_Single_t1534300504_0_0_0_Types };
static const RuntimeType* GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&AnimationPair_t2435898854_0_0_0), (&Single_t1534300504_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_KeyValuePair_2_t914961172_0_0_0_Types[] = { (&AnimationPair_t2435898854_0_0_0), (&Single_t1534300504_0_0_0), (&KeyValuePair_2_t914961172_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_KeyValuePair_2_t914961172_0_0_0 = { 3, GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_KeyValuePair_2_t914961172_0_0_0_Types };
static const RuntimeType* GenInst_AtlasRegion_t2476064513_0_0_0_Types[] = { (&AtlasRegion_t2476064513_0_0_0) };
extern const Il2CppGenericInst GenInst_AtlasRegion_t2476064513_0_0_0 = { 1, GenInst_AtlasRegion_t2476064513_0_0_0_Types };
static const RuntimeType* GenInst_Atlas_t604244430_0_0_0_Types[] = { (&Atlas_t604244430_0_0_0) };
extern const Il2CppGenericInst GenInst_Atlas_t604244430_0_0_0 = { 1, GenInst_Atlas_t604244430_0_0_0_Types };
static const RuntimeType* GenInst_BoneData_t1194659452_0_0_0_Types[] = { (&BoneData_t1194659452_0_0_0) };
extern const Il2CppGenericInst GenInst_BoneData_t1194659452_0_0_0 = { 1, GenInst_BoneData_t1194659452_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RuntimeObject_0_0_0_Types[] = { (&String_t_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_String_t_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&String_t_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2849060356_0_0_0_Types[] = { (&KeyValuePair_2_t2849060356_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2849060356_0_0_0 = { 1, GenInst_KeyValuePair_2_t2849060356_0_0_0_Types };
static const RuntimeType* GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2849060356_0_0_0_Types[] = { (&String_t_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t2849060356_0_0_0) };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2849060356_0_0_0 = { 3, GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2849060356_0_0_0_Types };
static const RuntimeType* GenInst_SlotData_t587781850_0_0_0_Types[] = { (&SlotData_t587781850_0_0_0) };
extern const Il2CppGenericInst GenInst_SlotData_t587781850_0_0_0 = { 1, GenInst_SlotData_t587781850_0_0_0_Types };
static const RuntimeType* GenInst_IkConstraintData_t3215615709_0_0_0_Types[] = { (&IkConstraintData_t3215615709_0_0_0) };
extern const Il2CppGenericInst GenInst_IkConstraintData_t3215615709_0_0_0 = { 1, GenInst_IkConstraintData_t3215615709_0_0_0_Types };
static const RuntimeType* GenInst_TransformConstraintData_t1381040281_0_0_0_Types[] = { (&TransformConstraintData_t1381040281_0_0_0) };
extern const Il2CppGenericInst GenInst_TransformConstraintData_t1381040281_0_0_0 = { 1, GenInst_TransformConstraintData_t1381040281_0_0_0_Types };
static const RuntimeType* GenInst_PathConstraintData_t3185350348_0_0_0_Types[] = { (&PathConstraintData_t3185350348_0_0_0) };
extern const Il2CppGenericInst GenInst_PathConstraintData_t3185350348_0_0_0 = { 1, GenInst_PathConstraintData_t3185350348_0_0_0_Types };
static const RuntimeType* GenInst_Skin_t1695237131_0_0_0_Types[] = { (&Skin_t1695237131_0_0_0) };
extern const Il2CppGenericInst GenInst_Skin_t1695237131_0_0_0 = { 1, GenInst_Skin_t1695237131_0_0_0_Types };
static const RuntimeType* GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_Types[] = { (&AttachmentKeyTuple_t2283295499_0_0_0), (&Attachment_t397756874_0_0_0) };
extern const Il2CppGenericInst GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0 = { 2, GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_Types };
static const RuntimeType* GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_Types[] = { (&AttachmentKeyTuple_t2283295499_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2697487084_0_0_0_Types[] = { (&KeyValuePair_2_t2697487084_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2697487084_0_0_0 = { 1, GenInst_KeyValuePair_2_t2697487084_0_0_0_Types };
static const RuntimeType* GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Types[] = { (&AttachmentKeyTuple_t2283295499_0_0_0) };
extern const Il2CppGenericInst GenInst_AttachmentKeyTuple_t2283295499_0_0_0 = { 1, GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Types };
static const RuntimeType* GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_AttachmentKeyTuple_t2283295499_0_0_0_Types[] = { (&AttachmentKeyTuple_t2283295499_0_0_0), (&RuntimeObject_0_0_0), (&AttachmentKeyTuple_t2283295499_0_0_0) };
extern const Il2CppGenericInst GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_AttachmentKeyTuple_t2283295499_0_0_0 = { 3, GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_AttachmentKeyTuple_t2283295499_0_0_0_Types };
static const RuntimeType* GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&AttachmentKeyTuple_t2283295499_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&AttachmentKeyTuple_t2283295499_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2697487084_0_0_0_Types[] = { (&AttachmentKeyTuple_t2283295499_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t2697487084_0_0_0) };
extern const Il2CppGenericInst GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2697487084_0_0_0 = { 3, GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2697487084_0_0_0_Types };
static const RuntimeType* GenInst_Attachment_t397756874_0_0_0_Types[] = { (&Attachment_t397756874_0_0_0) };
extern const Il2CppGenericInst GenInst_Attachment_t397756874_0_0_0 = { 1, GenInst_Attachment_t397756874_0_0_0_Types };
static const RuntimeType* GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&AttachmentKeyTuple_t2283295499_0_0_0), (&Attachment_t397756874_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t353340461_0_0_0_Types[] = { (&KeyValuePair_2_t353340461_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t353340461_0_0_0 = { 1, GenInst_KeyValuePair_2_t353340461_0_0_0_Types };
static const RuntimeType* GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_KeyValuePair_2_t353340461_0_0_0_Types[] = { (&AttachmentKeyTuple_t2283295499_0_0_0), (&Attachment_t397756874_0_0_0), (&KeyValuePair_2_t353340461_0_0_0) };
extern const Il2CppGenericInst GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_KeyValuePair_2_t353340461_0_0_0 = { 3, GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_KeyValuePair_2_t353340461_0_0_0_Types };
static const RuntimeType* GenInst_LinkedMesh_t3424637972_0_0_0_Types[] = { (&LinkedMesh_t3424637972_0_0_0) };
extern const Il2CppGenericInst GenInst_LinkedMesh_t3424637972_0_0_0 = { 1, GenInst_LinkedMesh_t3424637972_0_0_0_Types };
static const RuntimeType* GenInst_TransformMode_t1529364024_0_0_0_Types[] = { (&TransformMode_t1529364024_0_0_0) };
extern const Il2CppGenericInst GenInst_TransformMode_t1529364024_0_0_0 = { 1, GenInst_TransformMode_t1529364024_0_0_0_Types };
static const RuntimeType* GenInst_EventData_t295981108_0_0_0_Types[] = { (&EventData_t295981108_0_0_0) };
extern const Il2CppGenericInst GenInst_EventData_t295981108_0_0_0 = { 1, GenInst_EventData_t295981108_0_0_0_Types };
static const RuntimeType* GenInst_BoundingBoxAttachment_t1026836171_0_0_0_Types[] = { (&BoundingBoxAttachment_t1026836171_0_0_0) };
extern const Il2CppGenericInst GenInst_BoundingBoxAttachment_t1026836171_0_0_0 = { 1, GenInst_BoundingBoxAttachment_t1026836171_0_0_0_Types };
static const RuntimeType* GenInst_VertexAttachment_t1776545046_0_0_0_Types[] = { (&VertexAttachment_t1776545046_0_0_0) };
extern const Il2CppGenericInst GenInst_VertexAttachment_t1776545046_0_0_0 = { 1, GenInst_VertexAttachment_t1776545046_0_0_0_Types };
static const RuntimeType* GenInst_Polygon_t3410609907_0_0_0_Types[] = { (&Polygon_t3410609907_0_0_0) };
extern const Il2CppGenericInst GenInst_Polygon_t3410609907_0_0_0 = { 1, GenInst_Polygon_t3410609907_0_0_0_Types };
static const RuntimeType* GenInst_ExposedList_1_t3384296343_0_0_0_Types[] = { (&ExposedList_1_t3384296343_0_0_0) };
extern const Il2CppGenericInst GenInst_ExposedList_1_t3384296343_0_0_0 = { 1, GenInst_ExposedList_1_t3384296343_0_0_0_Types };
static const RuntimeType* GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_AttachmentKeyTuple_t2283295499_0_0_0_Types[] = { (&AttachmentKeyTuple_t2283295499_0_0_0), (&Attachment_t397756874_0_0_0), (&AttachmentKeyTuple_t2283295499_0_0_0) };
extern const Il2CppGenericInst GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_AttachmentKeyTuple_t2283295499_0_0_0 = { 3, GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_AttachmentKeyTuple_t2283295499_0_0_0_Types };
static const RuntimeType* GenInst_ExposedList_1_t1671071859_0_0_0_Types[] = { (&ExposedList_1_t1671071859_0_0_0) };
extern const Il2CppGenericInst GenInst_ExposedList_1_t1671071859_0_0_0 = { 1, GenInst_ExposedList_1_t1671071859_0_0_0_Types };
static const RuntimeType* GenInst_SubmeshInstruction_t3765917328_0_0_0_Types[] = { (&SubmeshInstruction_t3765917328_0_0_0) };
extern const Il2CppGenericInst GenInst_SubmeshInstruction_t3765917328_0_0_0 = { 1, GenInst_SubmeshInstruction_t3765917328_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t1804116343_0_0_0_Material_t2681358023_0_0_0_Types[] = { (&Slot_t1804116343_0_0_0), (&Material_t2681358023_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t1804116343_0_0_0_Material_t2681358023_0_0_0 = { 2, GenInst_Slot_t1804116343_0_0_0_Material_t2681358023_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t1804116343_0_0_0_Material_t2681358023_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Slot_t1804116343_0_0_0), (&Material_t2681358023_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t1804116343_0_0_0_Material_t2681358023_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Slot_t1804116343_0_0_0_Material_t2681358023_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2774005646_0_0_0_Types[] = { (&KeyValuePair_2_t2774005646_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2774005646_0_0_0 = { 1, GenInst_KeyValuePair_2_t2774005646_0_0_0_Types };
static const RuntimeType* GenInst_Slot_t1804116343_0_0_0_Material_t2681358023_0_0_0_KeyValuePair_2_t2774005646_0_0_0_Types[] = { (&Slot_t1804116343_0_0_0), (&Material_t2681358023_0_0_0), (&KeyValuePair_2_t2774005646_0_0_0) };
extern const Il2CppGenericInst GenInst_Slot_t1804116343_0_0_0_Material_t2681358023_0_0_0_KeyValuePair_2_t2774005646_0_0_0 = { 3, GenInst_Slot_t1804116343_0_0_0_Material_t2681358023_0_0_0_KeyValuePair_2_t2774005646_0_0_0_Types };
static const RuntimeType* GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_Types[] = { (&Material_t2681358023_0_0_0), (&Material_t2681358023_0_0_0) };
extern const Il2CppGenericInst GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0 = { 2, GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_Types };
static const RuntimeType* GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Material_t2681358023_0_0_0), (&Material_t2681358023_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t722797310_0_0_0_Types[] = { (&KeyValuePair_2_t722797310_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t722797310_0_0_0 = { 1, GenInst_KeyValuePair_2_t722797310_0_0_0_Types };
static const RuntimeType* GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_KeyValuePair_2_t722797310_0_0_0_Types[] = { (&Material_t2681358023_0_0_0), (&Material_t2681358023_0_0_0), (&KeyValuePair_2_t722797310_0_0_0) };
extern const Il2CppGenericInst GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_KeyValuePair_2_t722797310_0_0_0 = { 3, GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_KeyValuePair_2_t722797310_0_0_0_Types };
static const RuntimeType* GenInst_SmartMesh_t2574154657_0_0_0_Types[] = { (&SmartMesh_t2574154657_0_0_0) };
extern const Il2CppGenericInst GenInst_SmartMesh_t2574154657_0_0_0 = { 1, GenInst_SmartMesh_t2574154657_0_0_0_Types };
static const RuntimeType* GenInst_AtlasRegion_t2476064513_0_0_0_Texture2D_t1431210461_0_0_0_Types[] = { (&AtlasRegion_t2476064513_0_0_0), (&Texture2D_t1431210461_0_0_0) };
extern const Il2CppGenericInst GenInst_AtlasRegion_t2476064513_0_0_0_Texture2D_t1431210461_0_0_0 = { 2, GenInst_AtlasRegion_t2476064513_0_0_0_Texture2D_t1431210461_0_0_0_Types };
static const RuntimeType* GenInst_AtlasRegion_t2476064513_0_0_0_Texture2D_t1431210461_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&AtlasRegion_t2476064513_0_0_0), (&Texture2D_t1431210461_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_AtlasRegion_t2476064513_0_0_0_Texture2D_t1431210461_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_AtlasRegion_t2476064513_0_0_0_Texture2D_t1431210461_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3682718642_0_0_0_Types[] = { (&KeyValuePair_2_t3682718642_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3682718642_0_0_0 = { 1, GenInst_KeyValuePair_2_t3682718642_0_0_0_Types };
static const RuntimeType* GenInst_AtlasRegion_t2476064513_0_0_0_Texture2D_t1431210461_0_0_0_KeyValuePair_2_t3682718642_0_0_0_Types[] = { (&AtlasRegion_t2476064513_0_0_0), (&Texture2D_t1431210461_0_0_0), (&KeyValuePair_2_t3682718642_0_0_0) };
extern const Il2CppGenericInst GenInst_AtlasRegion_t2476064513_0_0_0_Texture2D_t1431210461_0_0_0_KeyValuePair_2_t3682718642_0_0_0 = { 3, GenInst_AtlasRegion_t2476064513_0_0_0_Texture2D_t1431210461_0_0_0_KeyValuePair_2_t3682718642_0_0_0_Types };
static const RuntimeType* GenInst_AtlasRegion_t2476064513_0_0_0_Int32_t4116043316_0_0_0_Types[] = { (&AtlasRegion_t2476064513_0_0_0), (&Int32_t4116043316_0_0_0) };
extern const Il2CppGenericInst GenInst_AtlasRegion_t2476064513_0_0_0_Int32_t4116043316_0_0_0 = { 2, GenInst_AtlasRegion_t2476064513_0_0_0_Int32_t4116043316_0_0_0_Types };
static const RuntimeType* GenInst_AtlasRegion_t2476064513_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&AtlasRegion_t2476064513_0_0_0), (&Int32_t4116043316_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_AtlasRegion_t2476064513_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_AtlasRegion_t2476064513_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2072584201_0_0_0_Types[] = { (&KeyValuePair_2_t2072584201_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2072584201_0_0_0 = { 1, GenInst_KeyValuePair_2_t2072584201_0_0_0_Types };
static const RuntimeType* GenInst_AtlasRegion_t2476064513_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t2072584201_0_0_0_Types[] = { (&AtlasRegion_t2476064513_0_0_0), (&Int32_t4116043316_0_0_0), (&KeyValuePair_2_t2072584201_0_0_0) };
extern const Il2CppGenericInst GenInst_AtlasRegion_t2476064513_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t2072584201_0_0_0 = { 3, GenInst_AtlasRegion_t2476064513_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t2072584201_0_0_0_Types };
static const RuntimeType* GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_Types[] = { (&BoundingBoxAttachment_t1026836171_0_0_0), (&PolygonCollider2D_t542995269_0_0_0) };
extern const Il2CppGenericInst GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0 = { 2, GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_Types };
static const RuntimeType* GenInst_PolygonCollider2D_t542995269_0_0_0_Types[] = { (&PolygonCollider2D_t542995269_0_0_0) };
extern const Il2CppGenericInst GenInst_PolygonCollider2D_t542995269_0_0_0 = { 1, GenInst_PolygonCollider2D_t542995269_0_0_0_Types };
static const RuntimeType* GenInst_Collider2D_t179611260_0_0_0_Types[] = { (&Collider2D_t179611260_0_0_0) };
extern const Il2CppGenericInst GenInst_Collider2D_t179611260_0_0_0 = { 1, GenInst_Collider2D_t179611260_0_0_0_Types };
static const RuntimeType* GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&BoundingBoxAttachment_t1026836171_0_0_0), (&PolygonCollider2D_t542995269_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3328228072_0_0_0_Types[] = { (&KeyValuePair_2_t3328228072_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3328228072_0_0_0 = { 1, GenInst_KeyValuePair_2_t3328228072_0_0_0_Types };
static const RuntimeType* GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_KeyValuePair_2_t3328228072_0_0_0_Types[] = { (&BoundingBoxAttachment_t1026836171_0_0_0), (&PolygonCollider2D_t542995269_0_0_0), (&KeyValuePair_2_t3328228072_0_0_0) };
extern const Il2CppGenericInst GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_KeyValuePair_2_t3328228072_0_0_0 = { 3, GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_KeyValuePair_2_t3328228072_0_0_0_Types };
static const RuntimeType* GenInst_BoundingBoxAttachment_t1026836171_0_0_0_String_t_0_0_0_Types[] = { (&BoundingBoxAttachment_t1026836171_0_0_0), (&String_t_0_0_0) };
extern const Il2CppGenericInst GenInst_BoundingBoxAttachment_t1026836171_0_0_0_String_t_0_0_0 = { 2, GenInst_BoundingBoxAttachment_t1026836171_0_0_0_String_t_0_0_0_Types };
static const RuntimeType* GenInst_BoundingBoxAttachment_t1026836171_0_0_0_String_t_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&BoundingBoxAttachment_t1026836171_0_0_0), (&String_t_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_BoundingBoxAttachment_t1026836171_0_0_0_String_t_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_BoundingBoxAttachment_t1026836171_0_0_0_String_t_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t35644278_0_0_0_Types[] = { (&KeyValuePair_2_t35644278_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t35644278_0_0_0 = { 1, GenInst_KeyValuePair_2_t35644278_0_0_0_Types };
static const RuntimeType* GenInst_BoundingBoxAttachment_t1026836171_0_0_0_String_t_0_0_0_KeyValuePair_2_t35644278_0_0_0_Types[] = { (&BoundingBoxAttachment_t1026836171_0_0_0), (&String_t_0_0_0), (&KeyValuePair_2_t35644278_0_0_0) };
extern const Il2CppGenericInst GenInst_BoundingBoxAttachment_t1026836171_0_0_0_String_t_0_0_0_KeyValuePair_2_t35644278_0_0_0 = { 3, GenInst_BoundingBoxAttachment_t1026836171_0_0_0_String_t_0_0_0_KeyValuePair_2_t35644278_0_0_0_Types };
static const RuntimeType* GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_PolygonCollider2D_t542995269_0_0_0_Types[] = { (&BoundingBoxAttachment_t1026836171_0_0_0), (&PolygonCollider2D_t542995269_0_0_0), (&PolygonCollider2D_t542995269_0_0_0) };
extern const Il2CppGenericInst GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_PolygonCollider2D_t542995269_0_0_0 = { 3, GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_PolygonCollider2D_t542995269_0_0_0_Types };
static const RuntimeType* GenInst_SlotMaterialOverride_t3080213539_0_0_0_Types[] = { (&SlotMaterialOverride_t3080213539_0_0_0) };
extern const Il2CppGenericInst GenInst_SlotMaterialOverride_t3080213539_0_0_0 = { 1, GenInst_SlotMaterialOverride_t3080213539_0_0_0_Types };
static const RuntimeType* GenInst_AtlasMaterialOverride_t4245281532_0_0_0_Types[] = { (&AtlasMaterialOverride_t4245281532_0_0_0) };
extern const Il2CppGenericInst GenInst_AtlasMaterialOverride_t4245281532_0_0_0 = { 1, GenInst_AtlasMaterialOverride_t4245281532_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonGhostRenderer_t108436892_0_0_0_Types[] = { (&SkeletonGhostRenderer_t108436892_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonGhostRenderer_t108436892_0_0_0 = { 1, GenInst_SkeletonGhostRenderer_t108436892_0_0_0_Types };
static const RuntimeType* GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_Types[] = { (&Material_t2681358023_0_0_0), (&Material_t2681358023_0_0_0), (&Material_t2681358023_0_0_0) };
extern const Il2CppGenericInst GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0 = { 3, GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_Types };
static const RuntimeType* GenInst_Rigidbody_t2418109227_0_0_0_Types[] = { (&Rigidbody_t2418109227_0_0_0) };
extern const Il2CppGenericInst GenInst_Rigidbody_t2418109227_0_0_0 = { 1, GenInst_Rigidbody_t2418109227_0_0_0_Types };
static const RuntimeType* GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_Types[] = { (&Bone_t1763862836_0_0_0), (&Transform_t2735953680_0_0_0) };
extern const Il2CppGenericInst GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0 = { 2, GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_Types };
static const RuntimeType* GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Bone_t1763862836_0_0_0), (&Transform_t2735953680_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2787016838_0_0_0_Types[] = { (&KeyValuePair_2_t2787016838_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2787016838_0_0_0 = { 1, GenInst_KeyValuePair_2_t2787016838_0_0_0_Types };
static const RuntimeType* GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_KeyValuePair_2_t2787016838_0_0_0_Types[] = { (&Bone_t1763862836_0_0_0), (&Transform_t2735953680_0_0_0), (&KeyValuePair_2_t2787016838_0_0_0) };
extern const Il2CppGenericInst GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_KeyValuePair_2_t2787016838_0_0_0 = { 3, GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_KeyValuePair_2_t2787016838_0_0_0_Types };
static const RuntimeType* GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_Transform_t2735953680_0_0_0_Types[] = { (&Bone_t1763862836_0_0_0), (&Transform_t2735953680_0_0_0), (&Transform_t2735953680_0_0_0) };
extern const Il2CppGenericInst GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_Transform_t2735953680_0_0_0 = { 3, GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_Transform_t2735953680_0_0_0_Types };
static const RuntimeType* GenInst_Collider_t4062789335_0_0_0_Types[] = { (&Collider_t4062789335_0_0_0) };
extern const Il2CppGenericInst GenInst_Collider_t4062789335_0_0_0 = { 1, GenInst_Collider_t4062789335_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonUtilityBone_t1490890952_0_0_0_Types[] = { (&SkeletonUtilityBone_t1490890952_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonUtilityBone_t1490890952_0_0_0 = { 1, GenInst_SkeletonUtilityBone_t1490890952_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonPartsRenderer_t3336947722_0_0_0_Types[] = { (&SkeletonPartsRenderer_t3336947722_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonPartsRenderer_t3336947722_0_0_0 = { 1, GenInst_SkeletonPartsRenderer_t3336947722_0_0_0_Types };
static const RuntimeType* GenInst_TransformPair_t1730302243_0_0_0_Types[] = { (&TransformPair_t1730302243_0_0_0) };
extern const Il2CppGenericInst GenInst_TransformPair_t1730302243_0_0_0 = { 1, GenInst_TransformPair_t1730302243_0_0_0_Types };
static const RuntimeType* GenInst_Joint_t59536310_0_0_0_Types[] = { (&Joint_t59536310_0_0_0) };
extern const Il2CppGenericInst GenInst_Joint_t59536310_0_0_0 = { 1, GenInst_Joint_t59536310_0_0_0_Types };
static const RuntimeType* GenInst_MaterialTexturePair_t2729914733_0_0_0_Material_t2681358023_0_0_0_Types[] = { (&MaterialTexturePair_t2729914733_0_0_0), (&Material_t2681358023_0_0_0) };
extern const Il2CppGenericInst GenInst_MaterialTexturePair_t2729914733_0_0_0_Material_t2681358023_0_0_0 = { 2, GenInst_MaterialTexturePair_t2729914733_0_0_0_Material_t2681358023_0_0_0_Types };
static const RuntimeType* GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_Types[] = { (&MaterialTexturePair_t2729914733_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3542000834_0_0_0_Types[] = { (&KeyValuePair_2_t3542000834_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3542000834_0_0_0 = { 1, GenInst_KeyValuePair_2_t3542000834_0_0_0_Types };
static const RuntimeType* GenInst_MaterialTexturePair_t2729914733_0_0_0_Types[] = { (&MaterialTexturePair_t2729914733_0_0_0) };
extern const Il2CppGenericInst GenInst_MaterialTexturePair_t2729914733_0_0_0 = { 1, GenInst_MaterialTexturePair_t2729914733_0_0_0_Types };
static const RuntimeType* GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_MaterialTexturePair_t2729914733_0_0_0_Types[] = { (&MaterialTexturePair_t2729914733_0_0_0), (&RuntimeObject_0_0_0), (&MaterialTexturePair_t2729914733_0_0_0) };
extern const Il2CppGenericInst GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_MaterialTexturePair_t2729914733_0_0_0 = { 3, GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_MaterialTexturePair_t2729914733_0_0_0_Types };
static const RuntimeType* GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types[] = { (&MaterialTexturePair_t2729914733_0_0_0), (&RuntimeObject_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0 = { 3, GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&MaterialTexturePair_t2729914733_0_0_0), (&RuntimeObject_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3542000834_0_0_0_Types[] = { (&MaterialTexturePair_t2729914733_0_0_0), (&RuntimeObject_0_0_0), (&KeyValuePair_2_t3542000834_0_0_0) };
extern const Il2CppGenericInst GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3542000834_0_0_0 = { 3, GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3542000834_0_0_0_Types };
static const RuntimeType* GenInst_MaterialTexturePair_t2729914733_0_0_0_Material_t2681358023_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&MaterialTexturePair_t2729914733_0_0_0), (&Material_t2681358023_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_MaterialTexturePair_t2729914733_0_0_0_Material_t2681358023_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_MaterialTexturePair_t2729914733_0_0_0_Material_t2681358023_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3481455360_0_0_0_Types[] = { (&KeyValuePair_2_t3481455360_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3481455360_0_0_0 = { 1, GenInst_KeyValuePair_2_t3481455360_0_0_0_Types };
static const RuntimeType* GenInst_MaterialTexturePair_t2729914733_0_0_0_Material_t2681358023_0_0_0_KeyValuePair_2_t3481455360_0_0_0_Types[] = { (&MaterialTexturePair_t2729914733_0_0_0), (&Material_t2681358023_0_0_0), (&KeyValuePair_2_t3481455360_0_0_0) };
extern const Il2CppGenericInst GenInst_MaterialTexturePair_t2729914733_0_0_0_Material_t2681358023_0_0_0_KeyValuePair_2_t3481455360_0_0_0 = { 3, GenInst_MaterialTexturePair_t2729914733_0_0_0_Material_t2681358023_0_0_0_KeyValuePair_2_t3481455360_0_0_0_Types };
static const RuntimeType* GenInst_MixMode_t2985542706_0_0_0_Types[] = { (&MixMode_t2985542706_0_0_0) };
extern const Il2CppGenericInst GenInst_MixMode_t2985542706_0_0_0 = { 1, GenInst_MixMode_t2985542706_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_Animation_t2067143511_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&Animation_t2067143511_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_Animation_t2067143511_0_0_0 = { 2, GenInst_Int32_t4116043316_0_0_0_Animation_t2067143511_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_Animation_t2067143511_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&Animation_t2067143511_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_Animation_t2067143511_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Int32_t4116043316_0_0_0_Animation_t2067143511_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1798931405_0_0_0_Types[] = { (&KeyValuePair_2_t1798931405_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1798931405_0_0_0 = { 1, GenInst_KeyValuePair_2_t1798931405_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_Animation_t2067143511_0_0_0_KeyValuePair_2_t1798931405_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&Animation_t2067143511_0_0_0), (&KeyValuePair_2_t1798931405_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_Animation_t2067143511_0_0_0_KeyValuePair_2_t1798931405_0_0_0 = { 3, GenInst_Int32_t4116043316_0_0_0_Animation_t2067143511_0_0_0_KeyValuePair_2_t1798931405_0_0_0_Types };
static const RuntimeType* GenInst_AnimationClip_t3806409204_0_0_0_Int32_t4116043316_0_0_0_Types[] = { (&AnimationClip_t3806409204_0_0_0), (&Int32_t4116043316_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationClip_t3806409204_0_0_0_Int32_t4116043316_0_0_0 = { 2, GenInst_AnimationClip_t3806409204_0_0_0_Int32_t4116043316_0_0_0_Types };
static const RuntimeType* GenInst_AnimationClip_t3806409204_0_0_0_Types[] = { (&AnimationClip_t3806409204_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationClip_t3806409204_0_0_0 = { 1, GenInst_AnimationClip_t3806409204_0_0_0_Types };
static const RuntimeType* GenInst_Motion_t2361590416_0_0_0_Types[] = { (&Motion_t2361590416_0_0_0) };
extern const Il2CppGenericInst GenInst_Motion_t2361590416_0_0_0 = { 1, GenInst_Motion_t2361590416_0_0_0_Types };
static const RuntimeType* GenInst_AnimationClip_t3806409204_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&AnimationClip_t3806409204_0_0_0), (&Int32_t4116043316_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationClip_t3806409204_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_AnimationClip_t3806409204_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3844094954_0_0_0_Types[] = { (&KeyValuePair_2_t3844094954_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3844094954_0_0_0 = { 1, GenInst_KeyValuePair_2_t3844094954_0_0_0_Types };
static const RuntimeType* GenInst_AnimationClip_t3806409204_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t3844094954_0_0_0_Types[] = { (&AnimationClip_t3806409204_0_0_0), (&Int32_t4116043316_0_0_0), (&KeyValuePair_2_t3844094954_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationClip_t3806409204_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t3844094954_0_0_0 = { 3, GenInst_AnimationClip_t3806409204_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t3844094954_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonUtilityConstraint_t2014257453_0_0_0_Types[] = { (&SkeletonUtilityConstraint_t2014257453_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonUtilityConstraint_t2014257453_0_0_0 = { 1, GenInst_SkeletonUtilityConstraint_t2014257453_0_0_0_Types };
static const RuntimeType* GenInst_IEnumerable_1_t3156408824_gp_0_0_0_0_Types[] = { (&IEnumerable_1_t3156408824_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t3156408824_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t3156408824_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m3085334648_gp_0_0_0_0_Types[] = { (&Array_InternalArray__IEnumerable_GetEnumerator_m3085334648_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m3085334648_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m3085334648_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2713204366_gp_0_0_0_0_Array_Sort_m2713204366_gp_0_0_0_0_Types[] = { (&Array_Sort_m2713204366_gp_0_0_0_0), (&Array_Sort_m2713204366_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2713204366_gp_0_0_0_0_Array_Sort_m2713204366_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2713204366_gp_0_0_0_0_Array_Sort_m2713204366_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m482111105_gp_0_0_0_0_Array_Sort_m482111105_gp_1_0_0_0_Types[] = { (&Array_Sort_m482111105_gp_0_0_0_0), (&Array_Sort_m482111105_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m482111105_gp_0_0_0_0_Array_Sort_m482111105_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m482111105_gp_0_0_0_0_Array_Sort_m482111105_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2832668269_gp_0_0_0_0_Types[] = { (&Array_Sort_m2832668269_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2832668269_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2832668269_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2832668269_gp_0_0_0_0_Array_Sort_m2832668269_gp_0_0_0_0_Types[] = { (&Array_Sort_m2832668269_gp_0_0_0_0), (&Array_Sort_m2832668269_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2832668269_gp_0_0_0_0_Array_Sort_m2832668269_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2832668269_gp_0_0_0_0_Array_Sort_m2832668269_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1728532693_gp_0_0_0_0_Types[] = { (&Array_Sort_m1728532693_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1728532693_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m1728532693_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m1728532693_gp_0_0_0_0_Array_Sort_m1728532693_gp_1_0_0_0_Types[] = { (&Array_Sort_m1728532693_gp_0_0_0_0), (&Array_Sort_m1728532693_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m1728532693_gp_0_0_0_0_Array_Sort_m1728532693_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m1728532693_gp_0_0_0_0_Array_Sort_m1728532693_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3755495371_gp_0_0_0_0_Array_Sort_m3755495371_gp_0_0_0_0_Types[] = { (&Array_Sort_m3755495371_gp_0_0_0_0), (&Array_Sort_m3755495371_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3755495371_gp_0_0_0_0_Array_Sort_m3755495371_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m3755495371_gp_0_0_0_0_Array_Sort_m3755495371_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m748731730_gp_0_0_0_0_Array_Sort_m748731730_gp_1_0_0_0_Types[] = { (&Array_Sort_m748731730_gp_0_0_0_0), (&Array_Sort_m748731730_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m748731730_gp_0_0_0_0_Array_Sort_m748731730_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m748731730_gp_0_0_0_0_Array_Sort_m748731730_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2310720347_gp_0_0_0_0_Types[] = { (&Array_Sort_m2310720347_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2310720347_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2310720347_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2310720347_gp_0_0_0_0_Array_Sort_m2310720347_gp_0_0_0_0_Types[] = { (&Array_Sort_m2310720347_gp_0_0_0_0), (&Array_Sort_m2310720347_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2310720347_gp_0_0_0_0_Array_Sort_m2310720347_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m2310720347_gp_0_0_0_0_Array_Sort_m2310720347_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2925804184_gp_0_0_0_0_Types[] = { (&Array_Sort_m2925804184_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2925804184_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2925804184_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2925804184_gp_1_0_0_0_Types[] = { (&Array_Sort_m2925804184_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2925804184_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m2925804184_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2925804184_gp_0_0_0_0_Array_Sort_m2925804184_gp_1_0_0_0_Types[] = { (&Array_Sort_m2925804184_gp_0_0_0_0), (&Array_Sort_m2925804184_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2925804184_gp_0_0_0_0_Array_Sort_m2925804184_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m2925804184_gp_0_0_0_0_Array_Sort_m2925804184_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m3742156574_gp_0_0_0_0_Types[] = { (&Array_Sort_m3742156574_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m3742156574_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m3742156574_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Sort_m2614704753_gp_0_0_0_0_Types[] = { (&Array_Sort_m2614704753_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Sort_m2614704753_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m2614704753_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m4171624513_gp_0_0_0_0_Types[] = { (&Array_qsort_m4171624513_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m4171624513_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m4171624513_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m4171624513_gp_0_0_0_0_Array_qsort_m4171624513_gp_1_0_0_0_Types[] = { (&Array_qsort_m4171624513_gp_0_0_0_0), (&Array_qsort_m4171624513_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m4171624513_gp_0_0_0_0_Array_qsort_m4171624513_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m4171624513_gp_0_0_0_0_Array_qsort_m4171624513_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_compare_m4383310_gp_0_0_0_0_Types[] = { (&Array_compare_m4383310_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_compare_m4383310_gp_0_0_0_0 = { 1, GenInst_Array_compare_m4383310_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_qsort_m3103677270_gp_0_0_0_0_Types[] = { (&Array_qsort_m3103677270_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_qsort_m3103677270_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m3103677270_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Resize_m277214561_gp_0_0_0_0_Types[] = { (&Array_Resize_m277214561_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Resize_m277214561_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m277214561_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_TrueForAll_m513891374_gp_0_0_0_0_Types[] = { (&Array_TrueForAll_m513891374_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m513891374_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m513891374_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ForEach_m466051874_gp_0_0_0_0_Types[] = { (&Array_ForEach_m466051874_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ForEach_m466051874_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m466051874_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_ConvertAll_m1422568442_gp_0_0_0_0_Array_ConvertAll_m1422568442_gp_1_0_0_0_Types[] = { (&Array_ConvertAll_m1422568442_gp_0_0_0_0), (&Array_ConvertAll_m1422568442_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m1422568442_gp_0_0_0_0_Array_ConvertAll_m1422568442_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m1422568442_gp_0_0_0_0_Array_ConvertAll_m1422568442_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m2989453766_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m2989453766_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m2989453766_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m2989453766_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m1230572012_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m1230572012_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m1230572012_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m1230572012_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLastIndex_m411634194_gp_0_0_0_0_Types[] = { (&Array_FindLastIndex_m411634194_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m411634194_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m411634194_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m3912172757_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m3912172757_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m3912172757_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m3912172757_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m2906578126_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m2906578126_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m2906578126_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m2906578126_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindIndex_m1157017374_gp_0_0_0_0_Types[] = { (&Array_FindIndex_m1157017374_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m1157017374_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m1157017374_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m3771917272_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m3771917272_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3771917272_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3771917272_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m3734383632_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m3734383632_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m3734383632_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m3734383632_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m2735412359_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m2735412359_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m2735412359_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m2735412359_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_BinarySearch_m977584100_gp_0_0_0_0_Types[] = { (&Array_BinarySearch_m977584100_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m977584100_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m977584100_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m4293893642_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m4293893642_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m4293893642_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m4293893642_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m3131945657_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m3131945657_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m3131945657_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m3131945657_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_IndexOf_m523691678_gp_0_0_0_0_Types[] = { (&Array_IndexOf_m523691678_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m523691678_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m523691678_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m3813191867_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m3813191867_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m3813191867_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m3813191867_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m148270828_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m148270828_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m148270828_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m148270828_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_LastIndexOf_m477455076_gp_0_0_0_0_Types[] = { (&Array_LastIndexOf_m477455076_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m477455076_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m477455076_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindAll_m310413631_gp_0_0_0_0_Types[] = { (&Array_FindAll_m310413631_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindAll_m310413631_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m310413631_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Exists_m865355114_gp_0_0_0_0_Types[] = { (&Array_Exists_m865355114_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Exists_m865355114_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m865355114_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_AsReadOnly_m290138908_gp_0_0_0_0_Types[] = { (&Array_AsReadOnly_m290138908_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m290138908_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m290138908_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_Find_m2601461597_gp_0_0_0_0_Types[] = { (&Array_Find_m2601461597_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_Find_m2601461597_gp_0_0_0_0 = { 1, GenInst_Array_Find_m2601461597_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Array_FindLast_m1619294651_gp_0_0_0_0_Types[] = { (&Array_FindLast_m1619294651_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Array_FindLast_m1619294651_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m1619294651_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InternalEnumerator_1_t481779380_gp_0_0_0_0_Types[] = { (&InternalEnumerator_1_t481779380_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t481779380_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t481779380_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ArrayReadOnlyList_1_t1645599289_gp_0_0_0_0_Types[] = { (&ArrayReadOnlyList_1_t1645599289_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t1645599289_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t1645599289_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1910536622_gp_0_0_0_0_Types[] = { (&U3CGetEnumeratorU3Ec__Iterator0_t1910536622_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1910536622_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1910536622_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IList_1_t3190236850_gp_0_0_0_0_Types[] = { (&IList_1_t3190236850_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IList_1_t3190236850_gp_0_0_0_0 = { 1, GenInst_IList_1_t3190236850_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ICollection_1_t1858752682_gp_0_0_0_0_Types[] = { (&ICollection_1_t1858752682_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ICollection_1_t1858752682_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t1858752682_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Nullable_1_t3509863868_gp_0_0_0_0_Types[] = { (&Nullable_1_t3509863868_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Nullable_1_t3509863868_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t3509863868_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Comparer_1_t1586122693_gp_0_0_0_0_Types[] = { (&Comparer_1_t1586122693_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Comparer_1_t1586122693_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t1586122693_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t3317074146_gp_0_0_0_0_Types[] = { (&DefaultComparer_t3317074146_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t3317074146_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t3317074146_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericComparer_1_t612734070_gp_0_0_0_0_Types[] = { (&GenericComparer_1_t612734070_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t612734070_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t612734070_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Types[] = { (&Dictionary_2_t3826160452_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3826160452_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_Types[] = { (&Dictionary_2_t3826160452_gp_0_0_0_0), (&Dictionary_2_t3826160452_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1051722143_0_0_0_Types[] = { (&KeyValuePair_2_t1051722143_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1051722143_0_0_0 = { 1, GenInst_KeyValuePair_2_t1051722143_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1678136693_gp_0_0_0_0_Types[] = { (&Dictionary_2_t3826160452_gp_0_0_0_0), (&Dictionary_2_t3826160452_gp_1_0_0_0), (&Dictionary_2_Do_CopyTo_m1678136693_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1678136693_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1678136693_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3850636111_gp_0_0_0_0_Types[] = { (&Dictionary_2_t3826160452_gp_0_0_0_0), (&Dictionary_2_t3826160452_gp_1_0_0_0), (&Dictionary_2_Do_ICollectionCopyTo_m3850636111_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3850636111_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3850636111_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m3850636111_gp_0_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Dictionary_2_Do_ICollectionCopyTo_m3850636111_gp_0_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m3850636111_gp_0_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m3850636111_gp_0_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&Dictionary_2_t3826160452_gp_0_0_0_0), (&Dictionary_2_t3826160452_gp_1_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 3, GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_ShimEnumerator_t1628784215_gp_0_0_0_0_ShimEnumerator_t1628784215_gp_1_0_0_0_Types[] = { (&ShimEnumerator_t1628784215_gp_0_0_0_0), (&ShimEnumerator_t1628784215_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t1628784215_gp_0_0_0_0_ShimEnumerator_t1628784215_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t1628784215_gp_0_0_0_0_ShimEnumerator_t1628784215_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1833107221_gp_0_0_0_0_Enumerator_t1833107221_gp_1_0_0_0_Types[] = { (&Enumerator_t1833107221_gp_0_0_0_0), (&Enumerator_t1833107221_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1833107221_gp_0_0_0_0_Enumerator_t1833107221_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1833107221_gp_0_0_0_0_Enumerator_t1833107221_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2047929147_0_0_0_Types[] = { (&KeyValuePair_2_t2047929147_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2047929147_0_0_0 = { 1, GenInst_KeyValuePair_2_t2047929147_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t3967759248_gp_0_0_0_0_KeyCollection_t3967759248_gp_1_0_0_0_Types[] = { (&KeyCollection_t3967759248_gp_0_0_0_0), (&KeyCollection_t3967759248_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t3967759248_gp_0_0_0_0_KeyCollection_t3967759248_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t3967759248_gp_0_0_0_0_KeyCollection_t3967759248_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t3967759248_gp_0_0_0_0_Types[] = { (&KeyCollection_t3967759248_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t3967759248_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t3967759248_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t457868420_gp_0_0_0_0_Enumerator_t457868420_gp_1_0_0_0_Types[] = { (&Enumerator_t457868420_gp_0_0_0_0), (&Enumerator_t457868420_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t457868420_gp_0_0_0_0_Enumerator_t457868420_gp_1_0_0_0 = { 2, GenInst_Enumerator_t457868420_gp_0_0_0_0_Enumerator_t457868420_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t457868420_gp_0_0_0_0_Types[] = { (&Enumerator_t457868420_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t457868420_gp_0_0_0_0 = { 1, GenInst_Enumerator_t457868420_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t3967759248_gp_0_0_0_0_KeyCollection_t3967759248_gp_1_0_0_0_KeyCollection_t3967759248_gp_0_0_0_0_Types[] = { (&KeyCollection_t3967759248_gp_0_0_0_0), (&KeyCollection_t3967759248_gp_1_0_0_0), (&KeyCollection_t3967759248_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t3967759248_gp_0_0_0_0_KeyCollection_t3967759248_gp_1_0_0_0_KeyCollection_t3967759248_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t3967759248_gp_0_0_0_0_KeyCollection_t3967759248_gp_1_0_0_0_KeyCollection_t3967759248_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyCollection_t3967759248_gp_0_0_0_0_KeyCollection_t3967759248_gp_0_0_0_0_Types[] = { (&KeyCollection_t3967759248_gp_0_0_0_0), (&KeyCollection_t3967759248_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyCollection_t3967759248_gp_0_0_0_0_KeyCollection_t3967759248_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t3967759248_gp_0_0_0_0_KeyCollection_t3967759248_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t1986136921_gp_0_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0_Types[] = { (&ValueCollection_t1986136921_gp_0_0_0_0), (&ValueCollection_t1986136921_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t1986136921_gp_0_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t1986136921_gp_0_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t1986136921_gp_1_0_0_0_Types[] = { (&ValueCollection_t1986136921_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t1986136921_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t1986136921_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1246502289_gp_0_0_0_0_Enumerator_t1246502289_gp_1_0_0_0_Types[] = { (&Enumerator_t1246502289_gp_0_0_0_0), (&Enumerator_t1246502289_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1246502289_gp_0_0_0_0_Enumerator_t1246502289_gp_1_0_0_0 = { 2, GenInst_Enumerator_t1246502289_gp_0_0_0_0_Enumerator_t1246502289_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1246502289_gp_1_0_0_0_Types[] = { (&Enumerator_t1246502289_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1246502289_gp_1_0_0_0 = { 1, GenInst_Enumerator_t1246502289_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t1986136921_gp_0_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0_Types[] = { (&ValueCollection_t1986136921_gp_0_0_0_0), (&ValueCollection_t1986136921_gp_1_0_0_0), (&ValueCollection_t1986136921_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t1986136921_gp_0_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t1986136921_gp_0_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_ValueCollection_t1986136921_gp_1_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0_Types[] = { (&ValueCollection_t1986136921_gp_1_0_0_0), (&ValueCollection_t1986136921_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_ValueCollection_t1986136921_gp_1_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t1986136921_gp_1_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_DictionaryEntry_t2732573845_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types[] = { (&DictionaryEntry_t2732573845_0_0_0), (&DictionaryEntry_t2732573845_0_0_0) };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t2732573845_0_0_0_DictionaryEntry_t2732573845_0_0_0 = { 2, GenInst_DictionaryEntry_t2732573845_0_0_0_DictionaryEntry_t2732573845_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_KeyValuePair_2_t1051722143_0_0_0_Types[] = { (&Dictionary_2_t3826160452_gp_0_0_0_0), (&Dictionary_2_t3826160452_gp_1_0_0_0), (&KeyValuePair_2_t1051722143_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_KeyValuePair_2_t1051722143_0_0_0 = { 3, GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_KeyValuePair_2_t1051722143_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1051722143_0_0_0_KeyValuePair_2_t1051722143_0_0_0_Types[] = { (&KeyValuePair_2_t1051722143_0_0_0), (&KeyValuePair_2_t1051722143_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1051722143_0_0_0_KeyValuePair_2_t1051722143_0_0_0 = { 2, GenInst_KeyValuePair_2_t1051722143_0_0_0_KeyValuePair_2_t1051722143_0_0_0_Types };
static const RuntimeType* GenInst_Dictionary_2_t3826160452_gp_1_0_0_0_Types[] = { (&Dictionary_2_t3826160452_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_Dictionary_2_t3826160452_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t3826160452_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_EqualityComparer_1_t376817520_gp_0_0_0_0_Types[] = { (&EqualityComparer_1_t376817520_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t376817520_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t376817520_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DefaultComparer_t371954493_gp_0_0_0_0_Types[] = { (&DefaultComparer_t371954493_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultComparer_t371954493_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t371954493_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GenericEqualityComparer_1_t510648398_gp_0_0_0_0_Types[] = { (&GenericEqualityComparer_1_t510648398_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t510648398_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t510648398_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3857675627_0_0_0_Types[] = { (&KeyValuePair_2_t3857675627_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3857675627_0_0_0 = { 1, GenInst_KeyValuePair_2_t3857675627_0_0_0_Types };
static const RuntimeType* GenInst_IDictionary_2_t2146790633_gp_0_0_0_0_IDictionary_2_t2146790633_gp_1_0_0_0_Types[] = { (&IDictionary_2_t2146790633_gp_0_0_0_0), (&IDictionary_2_t2146790633_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2146790633_gp_0_0_0_0_IDictionary_2_t2146790633_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t2146790633_gp_0_0_0_0_IDictionary_2_t2146790633_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2275830193_gp_0_0_0_0_KeyValuePair_2_t2275830193_gp_1_0_0_0_Types[] = { (&KeyValuePair_2_t2275830193_gp_0_0_0_0), (&KeyValuePair_2_t2275830193_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2275830193_gp_0_0_0_0_KeyValuePair_2_t2275830193_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t2275830193_gp_0_0_0_0_KeyValuePair_2_t2275830193_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t1241771112_gp_0_0_0_0_Types[] = { (&List_1_t1241771112_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t1241771112_gp_0_0_0_0 = { 1, GenInst_List_1_t1241771112_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t930421405_gp_0_0_0_0_Types[] = { (&Enumerator_t930421405_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t930421405_gp_0_0_0_0 = { 1, GenInst_Enumerator_t930421405_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Collection_1_t3355046425_gp_0_0_0_0_Types[] = { (&Collection_1_t3355046425_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Collection_1_t3355046425_gp_0_0_0_0 = { 1, GenInst_Collection_1_t3355046425_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ReadOnlyCollection_1_t2677599621_gp_0_0_0_0_Types[] = { (&ReadOnlyCollection_1_t2677599621_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2677599621_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t2677599621_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_GetterAdapterFrame_m3692694854_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m3692694854_gp_1_0_0_0_Types[] = { (&MonoProperty_GetterAdapterFrame_m3692694854_gp_0_0_0_0), (&MonoProperty_GetterAdapterFrame_m3692694854_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m3692694854_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m3692694854_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m3692694854_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m3692694854_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_MonoProperty_StaticGetterAdapterFrame_m2072945890_gp_0_0_0_0_Types[] = { (&MonoProperty_StaticGetterAdapterFrame_m2072945890_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m2072945890_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m2072945890_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Queue_1_t167895599_gp_0_0_0_0_Types[] = { (&Queue_1_t167895599_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Queue_1_t167895599_gp_0_0_0_0 = { 1, GenInst_Queue_1_t167895599_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1878021519_gp_0_0_0_0_Types[] = { (&Enumerator_t1878021519_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1878021519_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1878021519_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Stack_1_t2037534615_gp_0_0_0_0_Types[] = { (&Stack_1_t2037534615_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Stack_1_t2037534615_gp_0_0_0_0 = { 1, GenInst_Stack_1_t2037534615_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t3227029231_gp_0_0_0_0_Types[] = { (&Enumerator_t3227029231_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t3227029231_gp_0_0_0_0 = { 1, GenInst_Enumerator_t3227029231_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_HashSet_1_t3110041205_gp_0_0_0_0_Types[] = { (&HashSet_1_t3110041205_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_HashSet_1_t3110041205_gp_0_0_0_0 = { 1, GenInst_HashSet_1_t3110041205_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t2912145968_gp_0_0_0_0_Types[] = { (&Enumerator_t2912145968_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t2912145968_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2912145968_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_PrimeHelper_t363312612_gp_0_0_0_0_Types[] = { (&PrimeHelper_t363312612_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_PrimeHelper_t363312612_gp_0_0_0_0 = { 1, GenInst_PrimeHelper_t363312612_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Any_m795394156_gp_0_0_0_0_Types[] = { (&Enumerable_Any_m795394156_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Any_m795394156_gp_0_0_0_0 = { 1, GenInst_Enumerable_Any_m795394156_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m2840543242_gp_0_0_0_0_Types[] = { (&Enumerable_Where_m2840543242_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2840543242_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m2840543242_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_Where_m2840543242_gp_0_0_0_0_Boolean_t1039239260_0_0_0_Types[] = { (&Enumerable_Where_m2840543242_gp_0_0_0_0), (&Boolean_t1039239260_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m2840543242_gp_0_0_0_0_Boolean_t1039239260_0_0_0 = { 2, GenInst_Enumerable_Where_m2840543242_gp_0_0_0_0_Boolean_t1039239260_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m2042176342_gp_0_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m2042176342_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2042176342_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m2042176342_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerable_CreateWhereIterator_m2042176342_gp_0_0_0_0_Boolean_t1039239260_0_0_0_Types[] = { (&Enumerable_CreateWhereIterator_m2042176342_gp_0_0_0_0), (&Boolean_t1039239260_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m2042176342_gp_0_0_0_0_Boolean_t1039239260_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m2042176342_gp_0_0_0_0_Boolean_t1039239260_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2323895991_gp_0_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2323895991_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2323895991_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2323895991_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2323895991_gp_0_0_0_0_Boolean_t1039239260_0_0_0_Types[] = { (&U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2323895991_gp_0_0_0_0), (&Boolean_t1039239260_0_0_0) };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2323895991_gp_0_0_0_0_Boolean_t1039239260_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2323895991_gp_0_0_0_0_Boolean_t1039239260_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentInChildren_m476264693_gp_0_0_0_0_Types[] = { (&Component_GetComponentInChildren_m476264693_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentInChildren_m476264693_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentInChildren_m476264693_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m896983223_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m896983223_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m896983223_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m896983223_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m3176757661_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m3176757661_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m3176757661_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m3176757661_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m2940377740_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m2940377740_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m2940377740_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m2940377740_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInChildren_m1572369961_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInChildren_m1572369961_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m1572369961_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m1572369961_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m3185394231_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m3185394231_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m3185394231_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m3185394231_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m2454520785_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m2454520785_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2454520785_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2454520785_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponentsInParent_m2474810776_gp_0_0_0_0_Types[] = { (&Component_GetComponentsInParent_m2474810776_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInParent_m2474810776_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInParent_m2474810776_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m945396275_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m945396275_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m945396275_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m945396275_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Component_GetComponents_m4225586958_gp_0_0_0_0_Types[] = { (&Component_GetComponents_m4225586958_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m4225586958_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m4225586958_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentInChildren_m3366407555_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentInChildren_m3366407555_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentInChildren_m3366407555_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentInChildren_m3366407555_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponents_m3404916630_gp_0_0_0_0_Types[] = { (&GameObject_GetComponents_m3404916630_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m3404916630_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m3404916630_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInChildren_m58698754_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInChildren_m58698754_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m58698754_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m58698754_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInChildren_m2855193608_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInChildren_m2855193608_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m2855193608_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m2855193608_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_GameObject_GetComponentsInParent_m3595539389_gp_0_0_0_0_Types[] = { (&GameObject_GetComponentsInParent_m3595539389_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m3595539389_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m3595539389_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_GetAllocArrayFromChannel_m3442725572_gp_0_0_0_0_Types[] = { (&Mesh_GetAllocArrayFromChannel_m3442725572_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_GetAllocArrayFromChannel_m3442725572_gp_0_0_0_0 = { 1, GenInst_Mesh_GetAllocArrayFromChannel_m3442725572_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SafeLength_m4026736427_gp_0_0_0_0_Types[] = { (&Mesh_SafeLength_m4026736427_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SafeLength_m4026736427_gp_0_0_0_0 = { 1, GenInst_Mesh_SafeLength_m4026736427_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m2097026643_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m2097026643_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m2097026643_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m2097026643_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetListForChannel_m4081195211_gp_0_0_0_0_Types[] = { (&Mesh_SetListForChannel_m4081195211_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetListForChannel_m4081195211_gp_0_0_0_0 = { 1, GenInst_Mesh_SetListForChannel_m4081195211_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_SetUvsImpl_m2170684948_gp_0_0_0_0_Types[] = { (&Mesh_SetUvsImpl_m2170684948_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_SetUvsImpl_m2170684948_gp_0_0_0_0 = { 1, GenInst_Mesh_SetUvsImpl_m2170684948_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Object_Instantiate_m3989777074_gp_0_0_0_0_Types[] = { (&Object_Instantiate_m3989777074_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_Instantiate_m3989777074_gp_0_0_0_0 = { 1, GenInst_Object_Instantiate_m3989777074_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Object_FindObjectsOfType_m3148200899_gp_0_0_0_0_Types[] = { (&Object_FindObjectsOfType_m3148200899_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Object_FindObjectsOfType_m3148200899_gp_0_0_0_0 = { 1, GenInst_Object_FindObjectsOfType_m3148200899_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Playable_IsPlayableOfType_m258057198_gp_0_0_0_0_Types[] = { (&Playable_IsPlayableOfType_m258057198_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Playable_IsPlayableOfType_m258057198_gp_0_0_0_0 = { 1, GenInst_Playable_IsPlayableOfType_m258057198_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_PlayableOutput_IsPlayableOutputOfType_m4020851067_gp_0_0_0_0_Types[] = { (&PlayableOutput_IsPlayableOutputOfType_m4020851067_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayableOutput_IsPlayableOutputOfType_m4020851067_gp_0_0_0_0 = { 1, GenInst_PlayableOutput_IsPlayableOutputOfType_m4020851067_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_1_t3974913308_gp_0_0_0_0_Types[] = { (&InvokableCall_1_t3974913308_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t3974913308_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t3974913308_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityAction_1_t1195038063_0_0_0_Types[] = { (&UnityAction_1_t1195038063_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityAction_1_t1195038063_0_0_0 = { 1, GenInst_UnityAction_1_t1195038063_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t2355787272_gp_0_0_0_0_InvokableCall_2_t2355787272_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t2355787272_gp_0_0_0_0), (&InvokableCall_2_t2355787272_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2355787272_gp_0_0_0_0_InvokableCall_2_t2355787272_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2355787272_gp_0_0_0_0_InvokableCall_2_t2355787272_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t2355787272_gp_0_0_0_0_Types[] = { (&InvokableCall_2_t2355787272_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2355787272_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2355787272_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_2_t2355787272_gp_1_0_0_0_Types[] = { (&InvokableCall_2_t2355787272_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2355787272_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2355787272_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t1710801379_gp_0_0_0_0_InvokableCall_3_t1710801379_gp_1_0_0_0_InvokableCall_3_t1710801379_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t1710801379_gp_0_0_0_0), (&InvokableCall_3_t1710801379_gp_1_0_0_0), (&InvokableCall_3_t1710801379_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1710801379_gp_0_0_0_0_InvokableCall_3_t1710801379_gp_1_0_0_0_InvokableCall_3_t1710801379_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t1710801379_gp_0_0_0_0_InvokableCall_3_t1710801379_gp_1_0_0_0_InvokableCall_3_t1710801379_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t1710801379_gp_0_0_0_0_Types[] = { (&InvokableCall_3_t1710801379_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1710801379_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t1710801379_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t1710801379_gp_1_0_0_0_Types[] = { (&InvokableCall_3_t1710801379_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1710801379_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t1710801379_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_3_t1710801379_gp_2_0_0_0_Types[] = { (&InvokableCall_3_t1710801379_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t1710801379_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t1710801379_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t827458010_gp_0_0_0_0_InvokableCall_4_t827458010_gp_1_0_0_0_InvokableCall_4_t827458010_gp_2_0_0_0_InvokableCall_4_t827458010_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t827458010_gp_0_0_0_0), (&InvokableCall_4_t827458010_gp_1_0_0_0), (&InvokableCall_4_t827458010_gp_2_0_0_0), (&InvokableCall_4_t827458010_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t827458010_gp_0_0_0_0_InvokableCall_4_t827458010_gp_1_0_0_0_InvokableCall_4_t827458010_gp_2_0_0_0_InvokableCall_4_t827458010_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t827458010_gp_0_0_0_0_InvokableCall_4_t827458010_gp_1_0_0_0_InvokableCall_4_t827458010_gp_2_0_0_0_InvokableCall_4_t827458010_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t827458010_gp_0_0_0_0_Types[] = { (&InvokableCall_4_t827458010_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t827458010_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t827458010_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t827458010_gp_1_0_0_0_Types[] = { (&InvokableCall_4_t827458010_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t827458010_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t827458010_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t827458010_gp_2_0_0_0_Types[] = { (&InvokableCall_4_t827458010_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t827458010_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t827458010_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_InvokableCall_4_t827458010_gp_3_0_0_0_Types[] = { (&InvokableCall_4_t827458010_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t827458010_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t827458010_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_CachedInvokableCall_1_t1469715872_gp_0_0_0_0_Types[] = { (&CachedInvokableCall_1_t1469715872_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t1469715872_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t1469715872_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_1_t3652835655_gp_0_0_0_0_Types[] = { (&UnityEvent_1_t3652835655_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t3652835655_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t3652835655_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_2_t329765484_gp_0_0_0_0_UnityEvent_2_t329765484_gp_1_0_0_0_Types[] = { (&UnityEvent_2_t329765484_gp_0_0_0_0), (&UnityEvent_2_t329765484_gp_1_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t329765484_gp_0_0_0_0_UnityEvent_2_t329765484_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t329765484_gp_0_0_0_0_UnityEvent_2_t329765484_gp_1_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_3_t461102170_gp_0_0_0_0_UnityEvent_3_t461102170_gp_1_0_0_0_UnityEvent_3_t461102170_gp_2_0_0_0_Types[] = { (&UnityEvent_3_t461102170_gp_0_0_0_0), (&UnityEvent_3_t461102170_gp_1_0_0_0), (&UnityEvent_3_t461102170_gp_2_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t461102170_gp_0_0_0_0_UnityEvent_3_t461102170_gp_1_0_0_0_UnityEvent_3_t461102170_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t461102170_gp_0_0_0_0_UnityEvent_3_t461102170_gp_1_0_0_0_UnityEvent_3_t461102170_gp_2_0_0_0_Types };
static const RuntimeType* GenInst_UnityEvent_4_t2903829384_gp_0_0_0_0_UnityEvent_4_t2903829384_gp_1_0_0_0_UnityEvent_4_t2903829384_gp_2_0_0_0_UnityEvent_4_t2903829384_gp_3_0_0_0_Types[] = { (&UnityEvent_4_t2903829384_gp_0_0_0_0), (&UnityEvent_4_t2903829384_gp_1_0_0_0), (&UnityEvent_4_t2903829384_gp_2_0_0_0), (&UnityEvent_4_t2903829384_gp_3_0_0_0) };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t2903829384_gp_0_0_0_0_UnityEvent_4_t2903829384_gp_1_0_0_0_UnityEvent_4_t2903829384_gp_2_0_0_0_UnityEvent_4_t2903829384_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t2903829384_gp_0_0_0_0_UnityEvent_4_t2903829384_gp_1_0_0_0_UnityEvent_4_t2903829384_gp_2_0_0_0_UnityEvent_4_t2903829384_gp_3_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_Execute_m3479605520_gp_0_0_0_0_Types[] = { (&ExecuteEvents_Execute_m3479605520_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m3479605520_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m3479605520_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_ExecuteHierarchy_m3277601682_gp_0_0_0_0_Types[] = { (&ExecuteEvents_ExecuteHierarchy_m3277601682_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m3277601682_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m3277601682_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventList_m3888996684_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventList_m3888996684_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m3888996684_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m3888996684_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_CanHandleEvent_m797943676_gp_0_0_0_0_Types[] = { (&ExecuteEvents_CanHandleEvent_m797943676_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m797943676_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m797943676_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExecuteEvents_GetEventHandler_m4096618946_gp_0_0_0_0_Types[] = { (&ExecuteEvents_GetEventHandler_m4096618946_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m4096618946_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m4096618946_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_TweenRunner_1_t433795729_gp_0_0_0_0_Types[] = { (&TweenRunner_1_t433795729_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t433795729_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t433795729_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_GetOrAddComponent_m2235731956_gp_0_0_0_0_Types[] = { (&Dropdown_GetOrAddComponent_m2235731956_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m2235731956_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m2235731956_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SetPropertyUtility_SetStruct_m3364939634_gp_0_0_0_0_Types[] = { (&SetPropertyUtility_SetStruct_m3364939634_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SetPropertyUtility_SetStruct_m3364939634_gp_0_0_0_0 = { 1, GenInst_SetPropertyUtility_SetStruct_m3364939634_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t86117749_gp_0_0_0_0_Types[] = { (&IndexedSet_1_t86117749_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t86117749_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t86117749_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_IndexedSet_1_t86117749_gp_0_0_0_0_Int32_t4116043316_0_0_0_Types[] = { (&IndexedSet_1_t86117749_gp_0_0_0_0), (&Int32_t4116043316_0_0_0) };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t86117749_gp_0_0_0_0_Int32_t4116043316_0_0_0 = { 2, GenInst_IndexedSet_1_t86117749_gp_0_0_0_0_Int32_t4116043316_0_0_0_Types };
static const RuntimeType* GenInst_ListPool_1_t4073877284_gp_0_0_0_0_Types[] = { (&ListPool_1_t4073877284_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ListPool_1_t4073877284_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t4073877284_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_List_1_t2549958162_0_0_0_Types[] = { (&List_1_t2549958162_0_0_0) };
extern const Il2CppGenericInst GenInst_List_1_t2549958162_0_0_0 = { 1, GenInst_List_1_t2549958162_0_0_0_Types };
static const RuntimeType* GenInst_ObjectPool_1_t2076978748_gp_0_0_0_0_Types[] = { (&ObjectPool_1_t2076978748_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t2076978748_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t2076978748_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Pool_1_t337449648_gp_0_0_0_0_Types[] = { (&Pool_1_t337449648_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Pool_1_t337449648_gp_0_0_0_0 = { 1, GenInst_Pool_1_t337449648_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExposedList_1_t1803397706_gp_0_0_0_0_Types[] = { (&ExposedList_1_t1803397706_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExposedList_1_t1803397706_gp_0_0_0_0 = { 1, GenInst_ExposedList_1_t1803397706_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExposedList_1_t1803397706_gp_0_0_0_0_ExposedList_1_ConvertAll_m4061944730_gp_0_0_0_0_Types[] = { (&ExposedList_1_t1803397706_gp_0_0_0_0), (&ExposedList_1_ConvertAll_m4061944730_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExposedList_1_t1803397706_gp_0_0_0_0_ExposedList_1_ConvertAll_m4061944730_gp_0_0_0_0 = { 2, GenInst_ExposedList_1_t1803397706_gp_0_0_0_0_ExposedList_1_ConvertAll_m4061944730_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_ExposedList_1_ConvertAll_m4061944730_gp_0_0_0_0_Types[] = { (&ExposedList_1_ConvertAll_m4061944730_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_ExposedList_1_ConvertAll_m4061944730_gp_0_0_0_0 = { 1, GenInst_ExposedList_1_ConvertAll_m4061944730_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_Enumerator_t1863178257_gp_0_0_0_0_Types[] = { (&Enumerator_t1863178257_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_Enumerator_t1863178257_gp_0_0_0_0 = { 1, GenInst_Enumerator_t1863178257_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_DoubleBuffered_1_t1464434538_gp_0_0_0_0_Types[] = { (&DoubleBuffered_1_t1464434538_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_DoubleBuffered_1_t1464434538_gp_0_0_0_0 = { 1, GenInst_DoubleBuffered_1_t1464434538_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonRenderer_NewSpineGameObject_m853295412_gp_0_0_0_0_Types[] = { (&SkeletonRenderer_NewSpineGameObject_m853295412_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonRenderer_NewSpineGameObject_m853295412_gp_0_0_0_0 = { 1, GenInst_SkeletonRenderer_NewSpineGameObject_m853295412_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonRenderer_AddSpineComponent_m4245276095_gp_0_0_0_0_Types[] = { (&SkeletonRenderer_AddSpineComponent_m4245276095_gp_0_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonRenderer_AddSpineComponent_m4245276095_gp_0_0_0_0 = { 1, GenInst_SkeletonRenderer_AddSpineComponent_m4245276095_gp_0_0_0_0_Types };
static const RuntimeType* GenInst_AnimationPlayableOutput_t2354988367_0_0_0_Types[] = { (&AnimationPlayableOutput_t2354988367_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationPlayableOutput_t2354988367_0_0_0 = { 1, GenInst_AnimationPlayableOutput_t2354988367_0_0_0_Types };
static const RuntimeType* GenInst_DefaultExecutionOrder_t135071803_0_0_0_Types[] = { (&DefaultExecutionOrder_t135071803_0_0_0) };
extern const Il2CppGenericInst GenInst_DefaultExecutionOrder_t135071803_0_0_0 = { 1, GenInst_DefaultExecutionOrder_t135071803_0_0_0_Types };
static const RuntimeType* GenInst_AudioPlayableOutput_t108233817_0_0_0_Types[] = { (&AudioPlayableOutput_t108233817_0_0_0) };
extern const Il2CppGenericInst GenInst_AudioPlayableOutput_t108233817_0_0_0 = { 1, GenInst_AudioPlayableOutput_t108233817_0_0_0_Types };
static const RuntimeType* GenInst_PlayerConnection_t939829292_0_0_0_Types[] = { (&PlayerConnection_t939829292_0_0_0) };
extern const Il2CppGenericInst GenInst_PlayerConnection_t939829292_0_0_0 = { 1, GenInst_PlayerConnection_t939829292_0_0_0_Types };
static const RuntimeType* GenInst_ScriptPlayableOutput_t3200927392_0_0_0_Types[] = { (&ScriptPlayableOutput_t3200927392_0_0_0) };
extern const Il2CppGenericInst GenInst_ScriptPlayableOutput_t3200927392_0_0_0 = { 1, GenInst_ScriptPlayableOutput_t3200927392_0_0_0_Types };
static const RuntimeType* GenInst_GUILayer_t3566394258_0_0_0_Types[] = { (&GUILayer_t3566394258_0_0_0) };
extern const Il2CppGenericInst GenInst_GUILayer_t3566394258_0_0_0 = { 1, GenInst_GUILayer_t3566394258_0_0_0_Types };
static const RuntimeType* GenInst_EventSystem_t1402709230_0_0_0_Types[] = { (&EventSystem_t1402709230_0_0_0) };
extern const Il2CppGenericInst GenInst_EventSystem_t1402709230_0_0_0 = { 1, GenInst_EventSystem_t1402709230_0_0_0_Types };
static const RuntimeType* GenInst_AxisEventData_t1890216861_0_0_0_Types[] = { (&AxisEventData_t1890216861_0_0_0) };
extern const Il2CppGenericInst GenInst_AxisEventData_t1890216861_0_0_0 = { 1, GenInst_AxisEventData_t1890216861_0_0_0_Types };
static const RuntimeType* GenInst_SpriteRenderer_t1526302145_0_0_0_Types[] = { (&SpriteRenderer_t1526302145_0_0_0) };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t1526302145_0_0_0 = { 1, GenInst_SpriteRenderer_t1526302145_0_0_0_Types };
static const RuntimeType* GenInst_Image_t2836066662_0_0_0_Types[] = { (&Image_t2836066662_0_0_0) };
extern const Il2CppGenericInst GenInst_Image_t2836066662_0_0_0 = { 1, GenInst_Image_t2836066662_0_0_0_Types };
static const RuntimeType* GenInst_Button_t2390191360_0_0_0_Types[] = { (&Button_t2390191360_0_0_0) };
extern const Il2CppGenericInst GenInst_Button_t2390191360_0_0_0 = { 1, GenInst_Button_t2390191360_0_0_0_Types };
static const RuntimeType* GenInst_RawImage_t2407390601_0_0_0_Types[] = { (&RawImage_t2407390601_0_0_0) };
extern const Il2CppGenericInst GenInst_RawImage_t2407390601_0_0_0 = { 1, GenInst_RawImage_t2407390601_0_0_0_Types };
static const RuntimeType* GenInst_Slider_t3490919315_0_0_0_Types[] = { (&Slider_t3490919315_0_0_0) };
extern const Il2CppGenericInst GenInst_Slider_t3490919315_0_0_0 = { 1, GenInst_Slider_t3490919315_0_0_0_Types };
static const RuntimeType* GenInst_Scrollbar_t2916731442_0_0_0_Types[] = { (&Scrollbar_t2916731442_0_0_0) };
extern const Il2CppGenericInst GenInst_Scrollbar_t2916731442_0_0_0 = { 1, GenInst_Scrollbar_t2916731442_0_0_0_Types };
static const RuntimeType* GenInst_InputField_t2184098916_0_0_0_Types[] = { (&InputField_t2184098916_0_0_0) };
extern const Il2CppGenericInst GenInst_InputField_t2184098916_0_0_0 = { 1, GenInst_InputField_t2184098916_0_0_0_Types };
static const RuntimeType* GenInst_ScrollRect_t3671999215_0_0_0_Types[] = { (&ScrollRect_t3671999215_0_0_0) };
extern const Il2CppGenericInst GenInst_ScrollRect_t3671999215_0_0_0 = { 1, GenInst_ScrollRect_t3671999215_0_0_0_Types };
static const RuntimeType* GenInst_Dropdown_t520728739_0_0_0_Types[] = { (&Dropdown_t520728739_0_0_0) };
extern const Il2CppGenericInst GenInst_Dropdown_t520728739_0_0_0 = { 1, GenInst_Dropdown_t520728739_0_0_0_Types };
static const RuntimeType* GenInst_GraphicRaycaster_t4012288718_0_0_0_Types[] = { (&GraphicRaycaster_t4012288718_0_0_0) };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t4012288718_0_0_0 = { 1, GenInst_GraphicRaycaster_t4012288718_0_0_0_Types };
static const RuntimeType* GenInst_CanvasRenderer_t123344510_0_0_0_Types[] = { (&CanvasRenderer_t123344510_0_0_0) };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t123344510_0_0_0 = { 1, GenInst_CanvasRenderer_t123344510_0_0_0_Types };
static const RuntimeType* GenInst_Corner_t1996352389_0_0_0_Types[] = { (&Corner_t1996352389_0_0_0) };
extern const Il2CppGenericInst GenInst_Corner_t1996352389_0_0_0 = { 1, GenInst_Corner_t1996352389_0_0_0_Types };
static const RuntimeType* GenInst_Axis_t2661399177_0_0_0_Types[] = { (&Axis_t2661399177_0_0_0) };
extern const Il2CppGenericInst GenInst_Axis_t2661399177_0_0_0 = { 1, GenInst_Axis_t2661399177_0_0_0_Types };
static const RuntimeType* GenInst_Constraint_t2532561711_0_0_0_Types[] = { (&Constraint_t2532561711_0_0_0) };
extern const Il2CppGenericInst GenInst_Constraint_t2532561711_0_0_0 = { 1, GenInst_Constraint_t2532561711_0_0_0_Types };
static const RuntimeType* GenInst_SubmitEvent_t1927180329_0_0_0_Types[] = { (&SubmitEvent_t1927180329_0_0_0) };
extern const Il2CppGenericInst GenInst_SubmitEvent_t1927180329_0_0_0 = { 1, GenInst_SubmitEvent_t1927180329_0_0_0_Types };
static const RuntimeType* GenInst_OnChangeEvent_t1963771712_0_0_0_Types[] = { (&OnChangeEvent_t1963771712_0_0_0) };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t1963771712_0_0_0 = { 1, GenInst_OnChangeEvent_t1963771712_0_0_0_Types };
static const RuntimeType* GenInst_OnValidateInput_t1653235578_0_0_0_Types[] = { (&OnValidateInput_t1653235578_0_0_0) };
extern const Il2CppGenericInst GenInst_OnValidateInput_t1653235578_0_0_0 = { 1, GenInst_OnValidateInput_t1653235578_0_0_0_Types };
static const RuntimeType* GenInst_LayoutElement_t63293772_0_0_0_Types[] = { (&LayoutElement_t63293772_0_0_0) };
extern const Il2CppGenericInst GenInst_LayoutElement_t63293772_0_0_0 = { 1, GenInst_LayoutElement_t63293772_0_0_0_Types };
static const RuntimeType* GenInst_RectOffset_t2305860064_0_0_0_Types[] = { (&RectOffset_t2305860064_0_0_0) };
extern const Il2CppGenericInst GenInst_RectOffset_t2305860064_0_0_0 = { 1, GenInst_RectOffset_t2305860064_0_0_0_Types };
static const RuntimeType* GenInst_TextAnchor_t1106282459_0_0_0_Types[] = { (&TextAnchor_t1106282459_0_0_0) };
extern const Il2CppGenericInst GenInst_TextAnchor_t1106282459_0_0_0 = { 1, GenInst_TextAnchor_t1106282459_0_0_0_Types };
static const RuntimeType* GenInst_AnimationTriggers_t1030489365_0_0_0_Types[] = { (&AnimationTriggers_t1030489365_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t1030489365_0_0_0 = { 1, GenInst_AnimationTriggers_t1030489365_0_0_0_Types };
static const RuntimeType* GenInst_Animator_t126418246_0_0_0_Types[] = { (&Animator_t126418246_0_0_0) };
extern const Il2CppGenericInst GenInst_Animator_t126418246_0_0_0 = { 1, GenInst_Animator_t126418246_0_0_0_Types };
static const RuntimeType* GenInst_MeshRenderer_t1621355309_0_0_0_Types[] = { (&MeshRenderer_t1621355309_0_0_0) };
extern const Il2CppGenericInst GenInst_MeshRenderer_t1621355309_0_0_0 = { 1, GenInst_MeshRenderer_t1621355309_0_0_0_Types };
static const RuntimeType* GenInst_CharacterController_t3006247655_0_0_0_Types[] = { (&CharacterController_t3006247655_0_0_0) };
extern const Il2CppGenericInst GenInst_CharacterController_t3006247655_0_0_0 = { 1, GenInst_CharacterController_t3006247655_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonGraphic_t1867541356_0_0_0_Types[] = { (&SkeletonGraphic_t1867541356_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonGraphic_t1867541356_0_0_0 = { 1, GenInst_SkeletonGraphic_t1867541356_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonRagdoll2D_t3940222091_0_0_0_Types[] = { (&SkeletonRagdoll2D_t3940222091_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonRagdoll2D_t3940222091_0_0_0 = { 1, GenInst_SkeletonRagdoll2D_t3940222091_0_0_0_Types };
static const RuntimeType* GenInst_SpineboyBeginnerModel_t865536388_0_0_0_Types[] = { (&SpineboyBeginnerModel_t865536388_0_0_0) };
extern const Il2CppGenericInst GenInst_SpineboyBeginnerModel_t865536388_0_0_0 = { 1, GenInst_SpineboyBeginnerModel_t865536388_0_0_0_Types };
static const RuntimeType* GenInst_MeshFilter_t36322911_0_0_0_Types[] = { (&MeshFilter_t36322911_0_0_0) };
extern const Il2CppGenericInst GenInst_MeshFilter_t36322911_0_0_0 = { 1, GenInst_MeshFilter_t36322911_0_0_0_Types };
static const RuntimeType* GenInst_Mesh_t3526862104_0_0_0_Types[] = { (&Mesh_t3526862104_0_0_0) };
extern const Il2CppGenericInst GenInst_Mesh_t3526862104_0_0_0 = { 1, GenInst_Mesh_t3526862104_0_0_0_Types };
static const RuntimeType* GenInst_HingeJoint_t2932727570_0_0_0_Types[] = { (&HingeJoint_t2932727570_0_0_0) };
extern const Il2CppGenericInst GenInst_HingeJoint_t2932727570_0_0_0 = { 1, GenInst_HingeJoint_t2932727570_0_0_0_Types };
static const RuntimeType* GenInst_SphereCollider_t3968896157_0_0_0_Types[] = { (&SphereCollider_t3968896157_0_0_0) };
extern const Il2CppGenericInst GenInst_SphereCollider_t3968896157_0_0_0 = { 1, GenInst_SphereCollider_t3968896157_0_0_0_Types };
static const RuntimeType* GenInst_BoxCollider_t779584950_0_0_0_Types[] = { (&BoxCollider_t779584950_0_0_0) };
extern const Il2CppGenericInst GenInst_BoxCollider_t779584950_0_0_0 = { 1, GenInst_BoxCollider_t779584950_0_0_0_Types };
static const RuntimeType* GenInst_HingeJoint2D_t2351416406_0_0_0_Types[] = { (&HingeJoint2D_t2351416406_0_0_0) };
extern const Il2CppGenericInst GenInst_HingeJoint2D_t2351416406_0_0_0 = { 1, GenInst_HingeJoint2D_t2351416406_0_0_0_Types };
static const RuntimeType* GenInst_CircleCollider2D_t3428612651_0_0_0_Types[] = { (&CircleCollider2D_t3428612651_0_0_0) };
extern const Il2CppGenericInst GenInst_CircleCollider2D_t3428612651_0_0_0 = { 1, GenInst_CircleCollider2D_t3428612651_0_0_0_Types };
static const RuntimeType* GenInst_BoxCollider2D_t3639716847_0_0_0_Types[] = { (&BoxCollider2D_t3639716847_0_0_0) };
extern const Il2CppGenericInst GenInst_BoxCollider2D_t3639716847_0_0_0 = { 1, GenInst_BoxCollider2D_t3639716847_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonRenderSeparator_t3391021243_0_0_0_Types[] = { (&SkeletonRenderSeparator_t3391021243_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonRenderSeparator_t3391021243_0_0_0 = { 1, GenInst_SkeletonRenderSeparator_t3391021243_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonUtilityKinematicShadow_t2751461930_0_0_0_Types[] = { (&SkeletonUtilityKinematicShadow_t2751461930_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonUtilityKinematicShadow_t2751461930_0_0_0 = { 1, GenInst_SkeletonUtilityKinematicShadow_t2751461930_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonAnimator_t3794034912_0_0_0_Types[] = { (&SkeletonAnimator_t3794034912_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonAnimator_t3794034912_0_0_0 = { 1, GenInst_SkeletonAnimator_t3794034912_0_0_0_Types };
static const RuntimeType* GenInst_SkeletonUtility_t373332496_0_0_0_Types[] = { (&SkeletonUtility_t373332496_0_0_0) };
extern const Il2CppGenericInst GenInst_SkeletonUtility_t373332496_0_0_0 = { 1, GenInst_SkeletonUtility_t373332496_0_0_0_Types };
static const RuntimeType* GenInst_EventQueueEntry_t3756679084_0_0_0_EventQueueEntry_t3756679084_0_0_0_Types[] = { (&EventQueueEntry_t3756679084_0_0_0), (&EventQueueEntry_t3756679084_0_0_0) };
extern const Il2CppGenericInst GenInst_EventQueueEntry_t3756679084_0_0_0_EventQueueEntry_t3756679084_0_0_0 = { 2, GenInst_EventQueueEntry_t3756679084_0_0_0_EventQueueEntry_t3756679084_0_0_0_Types };
static const RuntimeType* GenInst_AtlasMaterialOverride_t4245281532_0_0_0_AtlasMaterialOverride_t4245281532_0_0_0_Types[] = { (&AtlasMaterialOverride_t4245281532_0_0_0), (&AtlasMaterialOverride_t4245281532_0_0_0) };
extern const Il2CppGenericInst GenInst_AtlasMaterialOverride_t4245281532_0_0_0_AtlasMaterialOverride_t4245281532_0_0_0 = { 2, GenInst_AtlasMaterialOverride_t4245281532_0_0_0_AtlasMaterialOverride_t4245281532_0_0_0_Types };
static const RuntimeType* GenInst_SlotMaterialOverride_t3080213539_0_0_0_SlotMaterialOverride_t3080213539_0_0_0_Types[] = { (&SlotMaterialOverride_t3080213539_0_0_0), (&SlotMaterialOverride_t3080213539_0_0_0) };
extern const Il2CppGenericInst GenInst_SlotMaterialOverride_t3080213539_0_0_0_SlotMaterialOverride_t3080213539_0_0_0 = { 2, GenInst_SlotMaterialOverride_t3080213539_0_0_0_SlotMaterialOverride_t3080213539_0_0_0_Types };
static const RuntimeType* GenInst_TransformPair_t1730302243_0_0_0_TransformPair_t1730302243_0_0_0_Types[] = { (&TransformPair_t1730302243_0_0_0), (&TransformPair_t1730302243_0_0_0) };
extern const Il2CppGenericInst GenInst_TransformPair_t1730302243_0_0_0_TransformPair_t1730302243_0_0_0 = { 2, GenInst_TransformPair_t1730302243_0_0_0_TransformPair_t1730302243_0_0_0_Types };
static const RuntimeType* GenInst_SubmeshInstruction_t3765917328_0_0_0_SubmeshInstruction_t3765917328_0_0_0_Types[] = { (&SubmeshInstruction_t3765917328_0_0_0), (&SubmeshInstruction_t3765917328_0_0_0) };
extern const Il2CppGenericInst GenInst_SubmeshInstruction_t3765917328_0_0_0_SubmeshInstruction_t3765917328_0_0_0 = { 2, GenInst_SubmeshInstruction_t3765917328_0_0_0_SubmeshInstruction_t3765917328_0_0_0_Types };
static const RuntimeType* GenInst_Boolean_t1039239260_0_0_0_Boolean_t1039239260_0_0_0_Types[] = { (&Boolean_t1039239260_0_0_0), (&Boolean_t1039239260_0_0_0) };
extern const Il2CppGenericInst GenInst_Boolean_t1039239260_0_0_0_Boolean_t1039239260_0_0_0 = { 2, GenInst_Boolean_t1039239260_0_0_0_Boolean_t1039239260_0_0_0_Types };
static const RuntimeType* GenInst_Int32_t4116043316_0_0_0_Int32_t4116043316_0_0_0_Types[] = { (&Int32_t4116043316_0_0_0), (&Int32_t4116043316_0_0_0) };
extern const Il2CppGenericInst GenInst_Int32_t4116043316_0_0_0_Int32_t4116043316_0_0_0 = { 2, GenInst_Int32_t4116043316_0_0_0_Int32_t4116043316_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeNamedArgument_t2964753189_0_0_0_CustomAttributeNamedArgument_t2964753189_0_0_0_Types[] = { (&CustomAttributeNamedArgument_t2964753189_0_0_0), (&CustomAttributeNamedArgument_t2964753189_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t2964753189_0_0_0_CustomAttributeNamedArgument_t2964753189_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t2964753189_0_0_0_CustomAttributeNamedArgument_t2964753189_0_0_0_Types };
static const RuntimeType* GenInst_CustomAttributeTypedArgument_t1279663203_0_0_0_CustomAttributeTypedArgument_t1279663203_0_0_0_Types[] = { (&CustomAttributeTypedArgument_t1279663203_0_0_0), (&CustomAttributeTypedArgument_t1279663203_0_0_0) };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1279663203_0_0_0_CustomAttributeTypedArgument_t1279663203_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1279663203_0_0_0_CustomAttributeTypedArgument_t1279663203_0_0_0_Types };
static const RuntimeType* GenInst_Single_t1534300504_0_0_0_Single_t1534300504_0_0_0_Types[] = { (&Single_t1534300504_0_0_0), (&Single_t1534300504_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t1534300504_0_0_0_Single_t1534300504_0_0_0 = { 2, GenInst_Single_t1534300504_0_0_0_Single_t1534300504_0_0_0_Types };
static const RuntimeType* GenInst_AnimatorClipInfo_t3943754594_0_0_0_AnimatorClipInfo_t3943754594_0_0_0_Types[] = { (&AnimatorClipInfo_t3943754594_0_0_0), (&AnimatorClipInfo_t3943754594_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimatorClipInfo_t3943754594_0_0_0_AnimatorClipInfo_t3943754594_0_0_0 = { 2, GenInst_AnimatorClipInfo_t3943754594_0_0_0_AnimatorClipInfo_t3943754594_0_0_0_Types };
static const RuntimeType* GenInst_Color32_t2411287715_0_0_0_Color32_t2411287715_0_0_0_Types[] = { (&Color32_t2411287715_0_0_0), (&Color32_t2411287715_0_0_0) };
extern const Il2CppGenericInst GenInst_Color32_t2411287715_0_0_0_Color32_t2411287715_0_0_0 = { 2, GenInst_Color32_t2411287715_0_0_0_Color32_t2411287715_0_0_0_Types };
static const RuntimeType* GenInst_RaycastResult_t2363482100_0_0_0_RaycastResult_t2363482100_0_0_0_Types[] = { (&RaycastResult_t2363482100_0_0_0), (&RaycastResult_t2363482100_0_0_0) };
extern const Il2CppGenericInst GenInst_RaycastResult_t2363482100_0_0_0_RaycastResult_t2363482100_0_0_0 = { 2, GenInst_RaycastResult_t2363482100_0_0_0_RaycastResult_t2363482100_0_0_0_Types };
static const RuntimeType* GenInst_UICharInfo_t3450488657_0_0_0_UICharInfo_t3450488657_0_0_0_Types[] = { (&UICharInfo_t3450488657_0_0_0), (&UICharInfo_t3450488657_0_0_0) };
extern const Il2CppGenericInst GenInst_UICharInfo_t3450488657_0_0_0_UICharInfo_t3450488657_0_0_0 = { 2, GenInst_UICharInfo_t3450488657_0_0_0_UICharInfo_t3450488657_0_0_0_Types };
static const RuntimeType* GenInst_UILineInfo_t4132474705_0_0_0_UILineInfo_t4132474705_0_0_0_Types[] = { (&UILineInfo_t4132474705_0_0_0), (&UILineInfo_t4132474705_0_0_0) };
extern const Il2CppGenericInst GenInst_UILineInfo_t4132474705_0_0_0_UILineInfo_t4132474705_0_0_0 = { 2, GenInst_UILineInfo_t4132474705_0_0_0_UILineInfo_t4132474705_0_0_0_Types };
static const RuntimeType* GenInst_UIVertex_t475044788_0_0_0_UIVertex_t475044788_0_0_0_Types[] = { (&UIVertex_t475044788_0_0_0), (&UIVertex_t475044788_0_0_0) };
extern const Il2CppGenericInst GenInst_UIVertex_t475044788_0_0_0_UIVertex_t475044788_0_0_0 = { 2, GenInst_UIVertex_t475044788_0_0_0_UIVertex_t475044788_0_0_0_Types };
static const RuntimeType* GenInst_Vector2_t1065050092_0_0_0_Vector2_t1065050092_0_0_0_Types[] = { (&Vector2_t1065050092_0_0_0), (&Vector2_t1065050092_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector2_t1065050092_0_0_0_Vector2_t1065050092_0_0_0 = { 2, GenInst_Vector2_t1065050092_0_0_0_Vector2_t1065050092_0_0_0_Types };
static const RuntimeType* GenInst_Vector3_t2825674791_0_0_0_Vector3_t2825674791_0_0_0_Types[] = { (&Vector3_t2825674791_0_0_0), (&Vector3_t2825674791_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector3_t2825674791_0_0_0_Vector3_t2825674791_0_0_0 = { 2, GenInst_Vector3_t2825674791_0_0_0_Vector3_t2825674791_0_0_0_Types };
static const RuntimeType* GenInst_Vector4_t2651204353_0_0_0_Vector4_t2651204353_0_0_0_Types[] = { (&Vector4_t2651204353_0_0_0), (&Vector4_t2651204353_0_0_0) };
extern const Il2CppGenericInst GenInst_Vector4_t2651204353_0_0_0_Vector4_t2651204353_0_0_0 = { 2, GenInst_Vector4_t2651204353_0_0_0_Vector4_t2651204353_0_0_0_Types };
static const RuntimeType* GenInst_AnimationPair_t2435898854_0_0_0_AnimationPair_t2435898854_0_0_0_Types[] = { (&AnimationPair_t2435898854_0_0_0), (&AnimationPair_t2435898854_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationPair_t2435898854_0_0_0_AnimationPair_t2435898854_0_0_0 = { 2, GenInst_AnimationPair_t2435898854_0_0_0_AnimationPair_t2435898854_0_0_0_Types };
static const RuntimeType* GenInst_AnimationPair_t2435898854_0_0_0_RuntimeObject_0_0_0_Types[] = { (&AnimationPair_t2435898854_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_AnimationPair_t2435898854_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_AnimationPair_t2435898854_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t914961172_0_0_0_KeyValuePair_2_t914961172_0_0_0_Types[] = { (&KeyValuePair_2_t914961172_0_0_0), (&KeyValuePair_2_t914961172_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t914961172_0_0_0_KeyValuePair_2_t914961172_0_0_0 = { 2, GenInst_KeyValuePair_2_t914961172_0_0_0_KeyValuePair_2_t914961172_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t914961172_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t914961172_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t914961172_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t914961172_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_Single_t1534300504_0_0_0_RuntimeObject_0_0_0_Types[] = { (&Single_t1534300504_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_Single_t1534300504_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_Single_t1534300504_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_AttachmentKeyTuple_t2283295499_0_0_0_AttachmentKeyTuple_t2283295499_0_0_0_Types[] = { (&AttachmentKeyTuple_t2283295499_0_0_0), (&AttachmentKeyTuple_t2283295499_0_0_0) };
extern const Il2CppGenericInst GenInst_AttachmentKeyTuple_t2283295499_0_0_0_AttachmentKeyTuple_t2283295499_0_0_0 = { 2, GenInst_AttachmentKeyTuple_t2283295499_0_0_0_AttachmentKeyTuple_t2283295499_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2697487084_0_0_0_KeyValuePair_2_t2697487084_0_0_0_Types[] = { (&KeyValuePair_2_t2697487084_0_0_0), (&KeyValuePair_2_t2697487084_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2697487084_0_0_0_KeyValuePair_2_t2697487084_0_0_0 = { 2, GenInst_KeyValuePair_2_t2697487084_0_0_0_KeyValuePair_2_t2697487084_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2697487084_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t2697487084_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2697487084_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2697487084_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_MaterialTexturePair_t2729914733_0_0_0_MaterialTexturePair_t2729914733_0_0_0_Types[] = { (&MaterialTexturePair_t2729914733_0_0_0), (&MaterialTexturePair_t2729914733_0_0_0) };
extern const Il2CppGenericInst GenInst_MaterialTexturePair_t2729914733_0_0_0_MaterialTexturePair_t2729914733_0_0_0 = { 2, GenInst_MaterialTexturePair_t2729914733_0_0_0_MaterialTexturePair_t2729914733_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3542000834_0_0_0_KeyValuePair_2_t3542000834_0_0_0_Types[] = { (&KeyValuePair_2_t3542000834_0_0_0), (&KeyValuePair_2_t3542000834_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3542000834_0_0_0_KeyValuePair_2_t3542000834_0_0_0 = { 2, GenInst_KeyValuePair_2_t3542000834_0_0_0_KeyValuePair_2_t3542000834_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3542000834_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3542000834_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3542000834_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3542000834_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2473691391_0_0_0_KeyValuePair_2_t2473691391_0_0_0_Types[] = { (&KeyValuePair_2_t2473691391_0_0_0), (&KeyValuePair_2_t2473691391_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2473691391_0_0_0_KeyValuePair_2_t2473691391_0_0_0 = { 2, GenInst_KeyValuePair_2_t2473691391_0_0_0_KeyValuePair_2_t2473691391_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t2473691391_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t2473691391_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2473691391_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t2473691391_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3050155971_0_0_0_KeyValuePair_2_t3050155971_0_0_0_Types[] = { (&KeyValuePair_2_t3050155971_0_0_0), (&KeyValuePair_2_t3050155971_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3050155971_0_0_0_KeyValuePair_2_t3050155971_0_0_0 = { 2, GenInst_KeyValuePair_2_t3050155971_0_0_0_KeyValuePair_2_t3050155971_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3050155971_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3050155971_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3050155971_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3050155971_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types[] = { (&IntPtr_t_0_0_0), (&IntPtr_t_0_0_0) };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0 = { 2, GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1590186377_0_0_0_KeyValuePair_2_t1590186377_0_0_0_Types[] = { (&KeyValuePair_2_t1590186377_0_0_0), (&KeyValuePair_2_t1590186377_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1590186377_0_0_0_KeyValuePair_2_t1590186377_0_0_0 = { 2, GenInst_KeyValuePair_2_t1590186377_0_0_0_KeyValuePair_2_t1590186377_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t1590186377_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t1590186377_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1590186377_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t1590186377_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t372023137_0_0_0_KeyValuePair_2_t372023137_0_0_0_Types[] = { (&KeyValuePair_2_t372023137_0_0_0), (&KeyValuePair_2_t372023137_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t372023137_0_0_0_KeyValuePair_2_t372023137_0_0_0 = { 2, GenInst_KeyValuePair_2_t372023137_0_0_0_KeyValuePair_2_t372023137_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t372023137_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t372023137_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t372023137_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t372023137_0_0_0_RuntimeObject_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3292850614_0_0_0_KeyValuePair_2_t3292850614_0_0_0_Types[] = { (&KeyValuePair_2_t3292850614_0_0_0), (&KeyValuePair_2_t3292850614_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3292850614_0_0_0_KeyValuePair_2_t3292850614_0_0_0 = { 2, GenInst_KeyValuePair_2_t3292850614_0_0_0_KeyValuePair_2_t3292850614_0_0_0_Types };
static const RuntimeType* GenInst_KeyValuePair_2_t3292850614_0_0_0_RuntimeObject_0_0_0_Types[] = { (&KeyValuePair_2_t3292850614_0_0_0), (&RuntimeObject_0_0_0) };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3292850614_0_0_0_RuntimeObject_0_0_0 = { 2, GenInst_KeyValuePair_2_t3292850614_0_0_0_RuntimeObject_0_0_0_Types };
extern const Il2CppGenericInst* const g_Il2CppGenericInstTable[785] = 
{
	&GenInst_RuntimeObject_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0,
	&GenInst_Char_t3956936558_0_0_0,
	&GenInst_Int64_t2552900376_0_0_0,
	&GenInst_UInt32_t1552934845_0_0_0,
	&GenInst_UInt64_t82101530_0_0_0,
	&GenInst_Byte_t1072372818_0_0_0,
	&GenInst_SByte_t4061040188_0_0_0,
	&GenInst_Int16_t141815767_0_0_0,
	&GenInst_UInt16_t243830035_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_IConvertible_t2924781617_0_0_0,
	&GenInst_IComparable_t4196824924_0_0_0,
	&GenInst_IEnumerable_t2786877510_0_0_0,
	&GenInst_ICloneable_t3071307377_0_0_0,
	&GenInst_IComparable_1_t1059852791_0_0_0,
	&GenInst_IEquatable_1_t2051519270_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_IReflect_t3763132637_0_0_0,
	&GenInst__Type_t2684199017_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t2951324458_0_0_0,
	&GenInst__MemberInfo_t2248025095_0_0_0,
	&GenInst_Double_t385876152_0_0_0,
	&GenInst_Single_t1534300504_0_0_0,
	&GenInst_Decimal_t162531659_0_0_0,
	&GenInst_Boolean_t1039239260_0_0_0,
	&GenInst_Delegate_t1310841479_0_0_0,
	&GenInst_ISerializable_t1907411279_0_0_0,
	&GenInst_ParameterInfo_t4161013588_0_0_0,
	&GenInst__ParameterInfo_t1333255694_0_0_0,
	&GenInst_ParameterModifier_t1293521645_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t4031414373_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t1133384202_0_0_0,
	&GenInst_MethodBase_t4283069302_0_0_0,
	&GenInst__MethodBase_t4233423649_0_0_0,
	&GenInst_ConstructorInfo_t4238936464_0_0_0,
	&GenInst__ConstructorInfo_t1306361156_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_TableRange_t2678548861_0_0_0,
	&GenInst_TailoringInfo_t1112703235_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t4116043316_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0,
	&GenInst_KeyValuePair_2_t372023137_0_0_0,
	&GenInst_Link_t2829283872_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_Int32_t4116043316_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t372023137_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t4223200175_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t4223200175_0_0_0,
	&GenInst_Contraction_t1226415401_0_0_0,
	&GenInst_Level2Map_t2111576706_0_0_0,
	&GenInst_BigInteger_t999372291_0_0_0,
	&GenInst_KeySizes_t1153709797_0_0_0,
	&GenInst_KeyValuePair_2_t3292850614_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3292850614_0_0_0,
	&GenInst_Slot_t2420837462_0_0_0,
	&GenInst_Slot_t1498351365_0_0_0,
	&GenInst_StackFrame_t1665646767_0_0_0,
	&GenInst_Calendar_t3315179817_0_0_0,
	&GenInst_ModuleBuilder_t1649085347_0_0_0,
	&GenInst__ModuleBuilder_t3557728172_0_0_0,
	&GenInst_Module_t1529303715_0_0_0,
	&GenInst__Module_t3000549352_0_0_0,
	&GenInst_ParameterBuilder_t1717477986_0_0_0,
	&GenInst__ParameterBuilder_t309653636_0_0_0,
	&GenInst_TypeU5BU5D_t2532561753_0_0_0,
	&GenInst_RuntimeArray_0_0_0,
	&GenInst_ICollection_t4150824962_0_0_0,
	&GenInst_IList_t682362525_0_0_0,
	&GenInst_IList_1_t2748475375_0_0_0,
	&GenInst_ICollection_1_t516081447_0_0_0,
	&GenInst_IEnumerable_1_t1681982789_0_0_0,
	&GenInst_IList_1_t2168955716_0_0_0,
	&GenInst_ICollection_1_t4231529084_0_0_0,
	&GenInst_IEnumerable_1_t1102463130_0_0_0,
	&GenInst_IList_1_t1090022096_0_0_0,
	&GenInst_ICollection_1_t3152595464_0_0_0,
	&GenInst_IEnumerable_1_t23529510_0_0_0,
	&GenInst_IList_1_t2753805949_0_0_0,
	&GenInst_ICollection_1_t521412021_0_0_0,
	&GenInst_IEnumerable_1_t1687313363_0_0_0,
	&GenInst_IList_1_t1357147537_0_0_0,
	&GenInst_ICollection_1_t3419720905_0_0_0,
	&GenInst_IEnumerable_1_t290654951_0_0_0,
	&GenInst_IList_1_t653848174_0_0_0,
	&GenInst_ICollection_1_t2716421542_0_0_0,
	&GenInst_IEnumerable_1_t3882322884_0_0_0,
	&GenInst_IList_1_t1147726576_0_0_0,
	&GenInst_ICollection_1_t3210299944_0_0_0,
	&GenInst_IEnumerable_1_t81233990_0_0_0,
	&GenInst_ILTokenInfo_t511186550_0_0_0,
	&GenInst_LabelData_t1722697480_0_0_0,
	&GenInst_LabelFixup_t742834559_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1146986652_0_0_0,
	&GenInst_TypeBuilder_t2259672225_0_0_0,
	&GenInst__TypeBuilder_t1767590369_0_0_0,
	&GenInst_MethodBuilder_t3780982296_0_0_0,
	&GenInst__MethodBuilder_t3671189628_0_0_0,
	&GenInst_ConstructorBuilder_t3893115018_0_0_0,
	&GenInst__ConstructorBuilder_t865107083_0_0_0,
	&GenInst_FieldBuilder_t3558139324_0_0_0,
	&GenInst__FieldBuilder_t4178988306_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t3692780915_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1279663203_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t2964753189_0_0_0,
	&GenInst_CustomAttributeData_t3364937814_0_0_0,
	&GenInst_ResourceInfo_t2441261143_0_0_0,
	&GenInst_ResourceCacheItem_t484540818_0_0_0,
	&GenInst_IContextProperty_t4041766982_0_0_0,
	&GenInst_Header_t3237302409_0_0_0,
	&GenInst_ITrackingHandler_t817503570_0_0_0,
	&GenInst_IContextAttribute_t142910004_0_0_0,
	&GenInst_DateTime_t1078222776_0_0_0,
	&GenInst_TimeSpan_t1041562996_0_0_0,
	&GenInst_TypeTag_t1928723012_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_StrongName_t1017363585_0_0_0,
	&GenInst_IBuiltInEvidence_t3051585724_0_0_0,
	&GenInst_IIdentityPermissionFactory_t419319585_0_0_0,
	&GenInst_DateTimeOffset_t774991079_0_0_0,
	&GenInst_Guid_t_0_0_0,
	&GenInst_Version_t2450729307_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t1039239260_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0,
	&GenInst_KeyValuePair_2_t1590186377_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_RuntimeObject_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_Boolean_t1039239260_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Boolean_t1039239260_0_0_0_KeyValuePair_2_t1590186377_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t1039239260_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t1146396119_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t1039239260_0_0_0_KeyValuePair_2_t1146396119_0_0_0,
	&GenInst_X509Certificate_t896677858_0_0_0,
	&GenInst_IDeserializationCallback_t1468984353_0_0_0,
	&GenInst_X509ChainStatus_t1809129440_0_0_0,
	&GenInst_Capture_t3038483990_0_0_0,
	&GenInst_Group_t2769576737_0_0_0,
	&GenInst_Mark_t3997027598_0_0_0,
	&GenInst_UriScheme_t2335814732_0_0_0,
	&GenInst_BigInteger_t999372292_0_0_0,
	&GenInst_ByteU5BU5D_t371269159_0_0_0,
	&GenInst_IList_1_t3773163193_0_0_0,
	&GenInst_ICollection_1_t1540769265_0_0_0,
	&GenInst_IEnumerable_1_t2706670607_0_0_0,
	&GenInst_ClientCertificateType_t3184407727_0_0_0,
	&GenInst_Link_t1352151537_0_0_0,
	&GenInst_Object_t350248726_0_0_0,
	&GenInst_Camera_t620664847_0_0_0,
	&GenInst_Behaviour_t2905267502_0_0_0,
	&GenInst_Component_t1852985663_0_0_0,
	&GenInst_Display_t3403130684_0_0_0,
	&GenInst_Material_t2681358023_0_0_0,
	&GenInst_Keyframe_t1429249175_0_0_0,
	&GenInst_Vector3_t2825674791_0_0_0,
	&GenInst_Vector4_t2651204353_0_0_0,
	&GenInst_Vector2_t1065050092_0_0_0,
	&GenInst_Color_t4183816058_0_0_0,
	&GenInst_Color32_t2411287715_0_0_0,
	&GenInst_Rect_t82402607_0_0_0,
	&GenInst_Texture2D_t1431210461_0_0_0,
	&GenInst_Texture_t4046637001_0_0_0,
	&GenInst_Playable_t3322485176_0_0_0,
	&GenInst_PlayableOutput_t1562817808_0_0_0,
	&GenInst_Scene_t937589653_0_0_0_LoadSceneMode_t56641604_0_0_0,
	&GenInst_Scene_t937589653_0_0_0,
	&GenInst_Scene_t937589653_0_0_0_Scene_t937589653_0_0_0,
	&GenInst_SpriteAtlas_t2050322510_0_0_0,
	&GenInst_ContactPoint_t1469515158_0_0_0,
	&GenInst_RaycastHit_t2126573776_0_0_0,
	&GenInst_Rigidbody2D_t4047763079_0_0_0,
	&GenInst_RaycastHit2D_t3956163020_0_0_0,
	&GenInst_ContactPoint2D_t2112099150_0_0_0,
	&GenInst_AudioClipPlayable_t3721052735_0_0_0,
	&GenInst_AudioMixerPlayable_t1728225682_0_0_0,
	&GenInst_AnimationClipPlayable_t2781760252_0_0_0,
	&GenInst_AnimationLayerMixerPlayable_t4269461007_0_0_0,
	&GenInst_AnimationMixerPlayable_t2857389361_0_0_0,
	&GenInst_AnimationOffsetPlayable_t3129069933_0_0_0,
	&GenInst_AnimatorControllerPlayable_t4108006533_0_0_0,
	&GenInst_AnimatorClipInfo_t3943754594_0_0_0,
	&GenInst_AnimatorControllerParameter_t2432615499_0_0_0,
	&GenInst_UIVertex_t475044788_0_0_0,
	&GenInst_UICharInfo_t3450488657_0_0_0,
	&GenInst_UILineInfo_t4132474705_0_0_0,
	&GenInst_Font_t1312336880_0_0_0,
	&GenInst_GUILayoutOption_t3257264162_0_0_0,
	&GenInst_GUILayoutEntry_t2519065403_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_LayoutCache_t851475779_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t2473691391_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_Int32_t4116043316_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2473691391_0_0_0,
	&GenInst_LayoutCache_t851475779_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_LayoutCache_t851475779_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t583263673_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_LayoutCache_t851475779_0_0_0_KeyValuePair_2_t583263673_0_0_0,
	&GenInst_GUIStyle_t2201526215_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t2308683074_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_KeyValuePair_2_t2308683074_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t2201526215_0_0_0_GUIStyle_t2201526215_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_IntPtr_t_0_0_0_Boolean_t1039239260_0_0_0,
	&GenInst_Exception_t2572292308_0_0_0_Boolean_t1039239260_0_0_0,
	&GenInst_UserProfile_t1495896542_0_0_0,
	&GenInst_IUserProfile_t1419151331_0_0_0,
	&GenInst_Boolean_t1039239260_0_0_0_String_t_0_0_0,
	&GenInst_Boolean_t1039239260_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AchievementDescription_t3623532732_0_0_0,
	&GenInst_IAchievementDescription_t2263541353_0_0_0,
	&GenInst_GcLeaderboard_t582328223_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t2404093396_0_0_0,
	&GenInst_IAchievementU5BU5D_t3105227985_0_0_0,
	&GenInst_IAchievement_t2790711152_0_0_0,
	&GenInst_GcAchievementData_t3971162273_0_0_0,
	&GenInst_Achievement_t1941630733_0_0_0,
	&GenInst_IScoreU5BU5D_t3128097251_0_0_0,
	&GenInst_IScore_t1577383494_0_0_0,
	&GenInst_GcScoreData_t3049994916_0_0_0,
	&GenInst_Score_t246219269_0_0_0,
	&GenInst_IUserProfileU5BU5D_t644998450_0_0_0,
	&GenInst_DisallowMultipleComponent_t2944104030_0_0_0,
	&GenInst_Attribute_t2169343654_0_0_0,
	&GenInst__Attribute_t156294154_0_0_0,
	&GenInst_ExecuteInEditMode_t767168347_0_0_0,
	&GenInst_RequireComponent_t1008017238_0_0_0,
	&GenInst_HitInfo_t2706273657_0_0_0,
	&GenInst_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_PersistentCall_t2119335335_0_0_0,
	&GenInst_BaseInvokableCall_t2985877092_0_0_0,
	&GenInst_WorkRequest_t2264216812_0_0_0,
	&GenInst_PlayableBinding_t150998691_0_0_0,
	&GenInst_MessageTypeSubscribers_t2381605113_0_0_0,
	&GenInst_MessageTypeSubscribers_t2381605113_0_0_0_Boolean_t1039239260_0_0_0,
	&GenInst_MessageEventArgs_t3322264806_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t449294333_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3050155971_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_IntPtr_t_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_IntPtr_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3050155971_0_0_0,
	&GenInst_WeakReference_t449294333_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t449294333_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t757546807_0_0_0,
	&GenInst_IntPtr_t_0_0_0_WeakReference_t449294333_0_0_0_KeyValuePair_2_t757546807_0_0_0,
	&GenInst_BaseInputModule_t4202312920_0_0_0,
	&GenInst_UIBehaviour_t3749981382_0_0_0,
	&GenInst_MonoBehaviour_t3755042618_0_0_0,
	&GenInst_RaycastResult_t2363482100_0_0_0,
	&GenInst_IDeselectHandler_t2317430855_0_0_0,
	&GenInst_IEventSystemHandler_t989066233_0_0_0,
	&GenInst_List_1_t3112481958_0_0_0,
	&GenInst_List_1_t570351926_0_0_0,
	&GenInst_List_1_t3976401388_0_0_0,
	&GenInst_ISelectHandler_t4128318009_0_0_0,
	&GenInst_BaseRaycaster_t3149871384_0_0_0,
	&GenInst_Entry_t1923502037_0_0_0,
	&GenInst_BaseEventData_t2200158663_0_0_0,
	&GenInst_IPointerEnterHandler_t1206321535_0_0_0,
	&GenInst_IPointerExitHandler_t563170456_0_0_0,
	&GenInst_IPointerDownHandler_t788657900_0_0_0,
	&GenInst_IPointerUpHandler_t3907224541_0_0_0,
	&GenInst_IPointerClickHandler_t4166377497_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t3159303760_0_0_0,
	&GenInst_IBeginDragHandler_t4197993221_0_0_0,
	&GenInst_IDragHandler_t4019865541_0_0_0,
	&GenInst_IEndDragHandler_t760648009_0_0_0,
	&GenInst_IDropHandler_t3618352481_0_0_0,
	&GenInst_IScrollHandler_t3841850943_0_0_0,
	&GenInst_IUpdateSelectedHandler_t3267774463_0_0_0,
	&GenInst_IMoveHandler_t2215200409_0_0_0,
	&GenInst_ISubmitHandler_t1318372016_0_0_0,
	&GenInst_ICancelHandler_t2259736822_0_0_0,
	&GenInst_Transform_t2735953680_0_0_0,
	&GenInst_GameObject_t3093992626_0_0_0,
	&GenInst_BaseInput_t1368120269_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0,
	&GenInst_PointerEventData_t1843710511_0_0_0,
	&GenInst_AbstractEventData_t3634143719_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t1575498405_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_KeyValuePair_2_t1575498405_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_PointerEventData_t1843710511_0_0_0_PointerEventData_t1843710511_0_0_0,
	&GenInst_ButtonState_t1423469858_0_0_0,
	&GenInst_ICanvasElement_t1064023118_0_0_0,
	&GenInst_ICanvasElement_t1064023118_0_0_0_Int32_t4116043316_0_0_0,
	&GenInst_ICanvasElement_t1064023118_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_ColorBlock_t2548562872_0_0_0,
	&GenInst_OptionData_t2269286103_0_0_0,
	&GenInst_DropdownItem_t706906876_0_0_0,
	&GenInst_FloatTween_t1018971280_0_0_0,
	&GenInst_Sprite_t360607379_0_0_0,
	&GenInst_Canvas_t452078485_0_0_0,
	&GenInst_List_1_t2575494210_0_0_0,
	&GenInst_Font_t1312336880_0_0_0_HashSet_1_t64826019_0_0_0,
	&GenInst_Text_t1469493361_0_0_0,
	&GenInst_Link_t79741401_0_0_0,
	&GenInst_ILayoutElement_t3747613007_0_0_0,
	&GenInst_MaskableGraphic_t2710636079_0_0_0,
	&GenInst_IClippable_t617981727_0_0_0,
	&GenInst_IMaskable_t865684376_0_0_0,
	&GenInst_IMaterialModifier_t3524121170_0_0_0,
	&GenInst_Graphic_t366741146_0_0_0,
	&GenInst_HashSet_1_t64826019_0_0_0,
	&GenInst_Font_t1312336880_0_0_0_HashSet_1_t64826019_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t3536216813_0_0_0,
	&GenInst_Font_t1312336880_0_0_0_HashSet_1_t64826019_0_0_0_KeyValuePair_2_t3536216813_0_0_0,
	&GenInst_ColorTween_t3858592911_0_0_0,
	&GenInst_Canvas_t452078485_0_0_0_IndexedSet_1_t3523870668_0_0_0,
	&GenInst_Graphic_t366741146_0_0_0_Int32_t4116043316_0_0_0,
	&GenInst_Graphic_t366741146_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_IndexedSet_1_t3523870668_0_0_0,
	&GenInst_Canvas_t452078485_0_0_0_IndexedSet_1_t3523870668_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t3649819709_0_0_0,
	&GenInst_Canvas_t452078485_0_0_0_IndexedSet_1_t3523870668_0_0_0_KeyValuePair_2_t3649819709_0_0_0,
	&GenInst_KeyValuePair_2_t241250796_0_0_0,
	&GenInst_Graphic_t366741146_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t241250796_0_0_0,
	&GenInst_KeyValuePair_2_t3657347816_0_0_0,
	&GenInst_ICanvasElement_t1064023118_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t3657347816_0_0_0,
	&GenInst_Type_t3593005433_0_0_0,
	&GenInst_FillMethod_t2569813474_0_0_0,
	&GenInst_ContentType_t2659363712_0_0_0,
	&GenInst_LineType_t1397773182_0_0_0,
	&GenInst_InputType_t1682781151_0_0_0,
	&GenInst_TouchScreenKeyboardType_t3909346333_0_0_0,
	&GenInst_CharacterValidation_t2407825395_0_0_0,
	&GenInst_Mask_t1104295890_0_0_0,
	&GenInst_ICanvasRaycastFilter_t640459987_0_0_0,
	&GenInst_List_1_t3227711615_0_0_0,
	&GenInst_RectMask2D_t475906393_0_0_0,
	&GenInst_IClipper_t4180951425_0_0_0,
	&GenInst_List_1_t2599322118_0_0_0,
	&GenInst_Navigation_t2074156770_0_0_0,
	&GenInst_Link_t3523197063_0_0_0,
	&GenInst_Direction_t2341802389_0_0_0,
	&GenInst_Selectable_t301191868_0_0_0,
	&GenInst_Transition_t2430538793_0_0_0,
	&GenInst_SpriteState_t4104548053_0_0_0,
	&GenInst_CanvasGroup_t514076778_0_0_0,
	&GenInst_Direction_t3623476438_0_0_0,
	&GenInst_MatEntry_t961266217_0_0_0,
	&GenInst_Toggle_t1097858442_0_0_0,
	&GenInst_Toggle_t1097858442_0_0_0_Boolean_t1039239260_0_0_0,
	&GenInst_IClipper_t4180951425_0_0_0_Int32_t4116043316_0_0_0,
	&GenInst_IClipper_t4180951425_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t699909001_0_0_0,
	&GenInst_IClipper_t4180951425_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t699909001_0_0_0,
	&GenInst_AspectMode_t757271725_0_0_0,
	&GenInst_FitMode_t1874677476_0_0_0,
	&GenInst_RectTransform_t1087788885_0_0_0,
	&GenInst_LayoutRebuilder_t3161446486_0_0_0,
	&GenInst_ILayoutElement_t3747613007_0_0_0_Single_t1534300504_0_0_0,
	&GenInst_RuntimeObject_0_0_0_Single_t1534300504_0_0_0,
	&GenInst_List_1_t654123220_0_0_0,
	&GenInst_List_1_t239736144_0_0_0,
	&GenInst_List_1_t3188465817_0_0_0,
	&GenInst_List_1_t479652782_0_0_0,
	&GenInst_List_1_t1944491745_0_0_0,
	&GenInst_List_1_t2598460513_0_0_0,
	&GenInst_AtlasAsset_t2525633250_0_0_0,
	&GenInst_ScriptableObject_t229319923_0_0_0,
	&GenInst_SlotRegionPair_t2265310044_0_0_0,
	&GenInst_Texture_t4046637001_0_0_0_AtlasPage_t3729449121_0_0_0,
	&GenInst_AtlasPage_t3729449121_0_0_0,
	&GenInst_Texture_t4046637001_0_0_0_AtlasPage_t3729449121_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t1286815118_0_0_0,
	&GenInst_Texture_t4046637001_0_0_0_AtlasPage_t3729449121_0_0_0_KeyValuePair_2_t1286815118_0_0_0,
	&GenInst_Animation_t2067143511_0_0_0_SkeletonAnimation_t2012766265_0_0_0,
	&GenInst_Animation_t2067143511_0_0_0,
	&GenInst_SkeletonAnimation_t2012766265_0_0_0,
	&GenInst_ISkeletonAnimation_t2072256521_0_0_0,
	&GenInst_IAnimationStateComponent_t2249916977_0_0_0,
	&GenInst_SkeletonRenderer_t1681389628_0_0_0,
	&GenInst_ISkeletonComponent_t3641217750_0_0_0,
	&GenInst_ISkeletonDataAssetComponent_t3477846014_0_0_0,
	&GenInst_Animation_t2067143511_0_0_0_SkeletonAnimation_t2012766265_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t851776416_0_0_0,
	&GenInst_Animation_t2067143511_0_0_0_SkeletonAnimation_t2012766265_0_0_0_KeyValuePair_2_t851776416_0_0_0,
	&GenInst_String_t_0_0_0_Animation_t2067143511_0_0_0,
	&GenInst_String_t_0_0_0_Animation_t2067143511_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t2174300370_0_0_0,
	&GenInst_String_t_0_0_0_Animation_t2067143511_0_0_0_KeyValuePair_2_t2174300370_0_0_0,
	&GenInst_SkeletonDataAsset_t1706375087_0_0_0,
	&GenInst_SlotSettings_t3151105826_0_0_0,
	&GenInst_EventPair_t1359900303_0_0_0,
	&GenInst_Event_t27715519_0_0_0,
	&GenInst_Timeline_t3562138029_0_0_0,
	&GenInst_Bone_t1763862836_0_0_0,
	&GenInst_IUpdatable_t125710159_0_0_0,
	&GenInst_Slot_t1804116343_0_0_0,
	&GenInst_SingleU5BU5D_t3989243465_0_0_0,
	&GenInst_IList_1_t4235090879_0_0_0,
	&GenInst_ICollection_1_t2002696951_0_0_0,
	&GenInst_IEnumerable_1_t3168598293_0_0_0,
	&GenInst_Int32U5BU5D_t281224829_0_0_0,
	&GenInst_IList_1_t2521866395_0_0_0,
	&GenInst_ICollection_1_t289472467_0_0_0,
	&GenInst_IEnumerable_1_t1455373809_0_0_0,
	&GenInst_IkConstraint_t2514691977_0_0_0,
	&GenInst_IConstraint_t900275133_0_0_0,
	&GenInst_TransformConstraint_t509539004_0_0_0,
	&GenInst_PathConstraint_t1854663405_0_0_0,
	&GenInst_TrackEntry_t4062297782_0_0_0,
	&GenInst_IPoolable_t612200214_0_0_0,
	&GenInst_Link_t2726291356_0_0_0,
	&GenInst_EventQueueEntry_t3756679084_0_0_0,
	&GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0,
	&GenInst_KeyValuePair_2_t914961172_0_0_0,
	&GenInst_AnimationPair_t2435898854_0_0_0,
	&GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_AnimationPair_t2435898854_0_0_0,
	&GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_Single_t1534300504_0_0_0,
	&GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_AnimationPair_t2435898854_0_0_0_Single_t1534300504_0_0_0_KeyValuePair_2_t914961172_0_0_0,
	&GenInst_AtlasRegion_t2476064513_0_0_0,
	&GenInst_Atlas_t604244430_0_0_0,
	&GenInst_BoneData_t1194659452_0_0_0,
	&GenInst_String_t_0_0_0_RuntimeObject_0_0_0,
	&GenInst_String_t_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t2849060356_0_0_0,
	&GenInst_String_t_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2849060356_0_0_0,
	&GenInst_SlotData_t587781850_0_0_0,
	&GenInst_IkConstraintData_t3215615709_0_0_0,
	&GenInst_TransformConstraintData_t1381040281_0_0_0,
	&GenInst_PathConstraintData_t3185350348_0_0_0,
	&GenInst_Skin_t1695237131_0_0_0,
	&GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0,
	&GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t2697487084_0_0_0,
	&GenInst_AttachmentKeyTuple_t2283295499_0_0_0,
	&GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_AttachmentKeyTuple_t2283295499_0_0_0,
	&GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_AttachmentKeyTuple_t2283295499_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t2697487084_0_0_0,
	&GenInst_Attachment_t397756874_0_0_0,
	&GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t353340461_0_0_0,
	&GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_KeyValuePair_2_t353340461_0_0_0,
	&GenInst_LinkedMesh_t3424637972_0_0_0,
	&GenInst_TransformMode_t1529364024_0_0_0,
	&GenInst_EventData_t295981108_0_0_0,
	&GenInst_BoundingBoxAttachment_t1026836171_0_0_0,
	&GenInst_VertexAttachment_t1776545046_0_0_0,
	&GenInst_Polygon_t3410609907_0_0_0,
	&GenInst_ExposedList_1_t3384296343_0_0_0,
	&GenInst_AttachmentKeyTuple_t2283295499_0_0_0_Attachment_t397756874_0_0_0_AttachmentKeyTuple_t2283295499_0_0_0,
	&GenInst_ExposedList_1_t1671071859_0_0_0,
	&GenInst_SubmeshInstruction_t3765917328_0_0_0,
	&GenInst_Slot_t1804116343_0_0_0_Material_t2681358023_0_0_0,
	&GenInst_Slot_t1804116343_0_0_0_Material_t2681358023_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t2774005646_0_0_0,
	&GenInst_Slot_t1804116343_0_0_0_Material_t2681358023_0_0_0_KeyValuePair_2_t2774005646_0_0_0,
	&GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0,
	&GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t722797310_0_0_0,
	&GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_KeyValuePair_2_t722797310_0_0_0,
	&GenInst_SmartMesh_t2574154657_0_0_0,
	&GenInst_AtlasRegion_t2476064513_0_0_0_Texture2D_t1431210461_0_0_0,
	&GenInst_AtlasRegion_t2476064513_0_0_0_Texture2D_t1431210461_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t3682718642_0_0_0,
	&GenInst_AtlasRegion_t2476064513_0_0_0_Texture2D_t1431210461_0_0_0_KeyValuePair_2_t3682718642_0_0_0,
	&GenInst_AtlasRegion_t2476064513_0_0_0_Int32_t4116043316_0_0_0,
	&GenInst_AtlasRegion_t2476064513_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t2072584201_0_0_0,
	&GenInst_AtlasRegion_t2476064513_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t2072584201_0_0_0,
	&GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0,
	&GenInst_PolygonCollider2D_t542995269_0_0_0,
	&GenInst_Collider2D_t179611260_0_0_0,
	&GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t3328228072_0_0_0,
	&GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_KeyValuePair_2_t3328228072_0_0_0,
	&GenInst_BoundingBoxAttachment_t1026836171_0_0_0_String_t_0_0_0,
	&GenInst_BoundingBoxAttachment_t1026836171_0_0_0_String_t_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t35644278_0_0_0,
	&GenInst_BoundingBoxAttachment_t1026836171_0_0_0_String_t_0_0_0_KeyValuePair_2_t35644278_0_0_0,
	&GenInst_BoundingBoxAttachment_t1026836171_0_0_0_PolygonCollider2D_t542995269_0_0_0_PolygonCollider2D_t542995269_0_0_0,
	&GenInst_SlotMaterialOverride_t3080213539_0_0_0,
	&GenInst_AtlasMaterialOverride_t4245281532_0_0_0,
	&GenInst_SkeletonGhostRenderer_t108436892_0_0_0,
	&GenInst_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0_Material_t2681358023_0_0_0,
	&GenInst_Rigidbody_t2418109227_0_0_0,
	&GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0,
	&GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t2787016838_0_0_0,
	&GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_KeyValuePair_2_t2787016838_0_0_0,
	&GenInst_Bone_t1763862836_0_0_0_Transform_t2735953680_0_0_0_Transform_t2735953680_0_0_0,
	&GenInst_Collider_t4062789335_0_0_0,
	&GenInst_SkeletonUtilityBone_t1490890952_0_0_0,
	&GenInst_SkeletonPartsRenderer_t3336947722_0_0_0,
	&GenInst_TransformPair_t1730302243_0_0_0,
	&GenInst_Joint_t59536310_0_0_0,
	&GenInst_MaterialTexturePair_t2729914733_0_0_0_Material_t2681358023_0_0_0,
	&GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3542000834_0_0_0,
	&GenInst_MaterialTexturePair_t2729914733_0_0_0,
	&GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_MaterialTexturePair_t2729914733_0_0_0,
	&GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_RuntimeObject_0_0_0,
	&GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_MaterialTexturePair_t2729914733_0_0_0_RuntimeObject_0_0_0_KeyValuePair_2_t3542000834_0_0_0,
	&GenInst_MaterialTexturePair_t2729914733_0_0_0_Material_t2681358023_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t3481455360_0_0_0,
	&GenInst_MaterialTexturePair_t2729914733_0_0_0_Material_t2681358023_0_0_0_KeyValuePair_2_t3481455360_0_0_0,
	&GenInst_MixMode_t2985542706_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_Animation_t2067143511_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_Animation_t2067143511_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t1798931405_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_Animation_t2067143511_0_0_0_KeyValuePair_2_t1798931405_0_0_0,
	&GenInst_AnimationClip_t3806409204_0_0_0_Int32_t4116043316_0_0_0,
	&GenInst_AnimationClip_t3806409204_0_0_0,
	&GenInst_Motion_t2361590416_0_0_0,
	&GenInst_AnimationClip_t3806409204_0_0_0_Int32_t4116043316_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_KeyValuePair_2_t3844094954_0_0_0,
	&GenInst_AnimationClip_t3806409204_0_0_0_Int32_t4116043316_0_0_0_KeyValuePair_2_t3844094954_0_0_0,
	&GenInst_SkeletonUtilityConstraint_t2014257453_0_0_0,
	&GenInst_IEnumerable_1_t3156408824_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m3085334648_gp_0_0_0_0,
	&GenInst_Array_Sort_m2713204366_gp_0_0_0_0_Array_Sort_m2713204366_gp_0_0_0_0,
	&GenInst_Array_Sort_m482111105_gp_0_0_0_0_Array_Sort_m482111105_gp_1_0_0_0,
	&GenInst_Array_Sort_m2832668269_gp_0_0_0_0,
	&GenInst_Array_Sort_m2832668269_gp_0_0_0_0_Array_Sort_m2832668269_gp_0_0_0_0,
	&GenInst_Array_Sort_m1728532693_gp_0_0_0_0,
	&GenInst_Array_Sort_m1728532693_gp_0_0_0_0_Array_Sort_m1728532693_gp_1_0_0_0,
	&GenInst_Array_Sort_m3755495371_gp_0_0_0_0_Array_Sort_m3755495371_gp_0_0_0_0,
	&GenInst_Array_Sort_m748731730_gp_0_0_0_0_Array_Sort_m748731730_gp_1_0_0_0,
	&GenInst_Array_Sort_m2310720347_gp_0_0_0_0,
	&GenInst_Array_Sort_m2310720347_gp_0_0_0_0_Array_Sort_m2310720347_gp_0_0_0_0,
	&GenInst_Array_Sort_m2925804184_gp_0_0_0_0,
	&GenInst_Array_Sort_m2925804184_gp_1_0_0_0,
	&GenInst_Array_Sort_m2925804184_gp_0_0_0_0_Array_Sort_m2925804184_gp_1_0_0_0,
	&GenInst_Array_Sort_m3742156574_gp_0_0_0_0,
	&GenInst_Array_Sort_m2614704753_gp_0_0_0_0,
	&GenInst_Array_qsort_m4171624513_gp_0_0_0_0,
	&GenInst_Array_qsort_m4171624513_gp_0_0_0_0_Array_qsort_m4171624513_gp_1_0_0_0,
	&GenInst_Array_compare_m4383310_gp_0_0_0_0,
	&GenInst_Array_qsort_m3103677270_gp_0_0_0_0,
	&GenInst_Array_Resize_m277214561_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m513891374_gp_0_0_0_0,
	&GenInst_Array_ForEach_m466051874_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m1422568442_gp_0_0_0_0_Array_ConvertAll_m1422568442_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m2989453766_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m1230572012_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m411634194_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m3912172757_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m2906578126_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m1157017374_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3771917272_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m3734383632_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m2735412359_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m977584100_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m4293893642_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m3131945657_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m523691678_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m3813191867_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m148270828_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m477455076_gp_0_0_0_0,
	&GenInst_Array_FindAll_m310413631_gp_0_0_0_0,
	&GenInst_Array_Exists_m865355114_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m290138908_gp_0_0_0_0,
	&GenInst_Array_Find_m2601461597_gp_0_0_0_0,
	&GenInst_Array_FindLast_m1619294651_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t481779380_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t1645599289_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t1910536622_gp_0_0_0_0,
	&GenInst_IList_1_t3190236850_gp_0_0_0_0,
	&GenInst_ICollection_1_t1858752682_gp_0_0_0_0,
	&GenInst_Nullable_1_t3509863868_gp_0_0_0_0,
	&GenInst_Comparer_1_t1586122693_gp_0_0_0_0,
	&GenInst_DefaultComparer_t3317074146_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t612734070_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3826160452_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t1051722143_0_0_0,
	&GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m1678136693_gp_0_0_0_0,
	&GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m3850636111_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m3850636111_gp_0_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_ShimEnumerator_t1628784215_gp_0_0_0_0_ShimEnumerator_t1628784215_gp_1_0_0_0,
	&GenInst_Enumerator_t1833107221_gp_0_0_0_0_Enumerator_t1833107221_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2047929147_0_0_0,
	&GenInst_KeyCollection_t3967759248_gp_0_0_0_0_KeyCollection_t3967759248_gp_1_0_0_0,
	&GenInst_KeyCollection_t3967759248_gp_0_0_0_0,
	&GenInst_Enumerator_t457868420_gp_0_0_0_0_Enumerator_t457868420_gp_1_0_0_0,
	&GenInst_Enumerator_t457868420_gp_0_0_0_0,
	&GenInst_KeyCollection_t3967759248_gp_0_0_0_0_KeyCollection_t3967759248_gp_1_0_0_0_KeyCollection_t3967759248_gp_0_0_0_0,
	&GenInst_KeyCollection_t3967759248_gp_0_0_0_0_KeyCollection_t3967759248_gp_0_0_0_0,
	&GenInst_ValueCollection_t1986136921_gp_0_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0,
	&GenInst_ValueCollection_t1986136921_gp_1_0_0_0,
	&GenInst_Enumerator_t1246502289_gp_0_0_0_0_Enumerator_t1246502289_gp_1_0_0_0,
	&GenInst_Enumerator_t1246502289_gp_1_0_0_0,
	&GenInst_ValueCollection_t1986136921_gp_0_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0,
	&GenInst_ValueCollection_t1986136921_gp_1_0_0_0_ValueCollection_t1986136921_gp_1_0_0_0,
	&GenInst_DictionaryEntry_t2732573845_0_0_0_DictionaryEntry_t2732573845_0_0_0,
	&GenInst_Dictionary_2_t3826160452_gp_0_0_0_0_Dictionary_2_t3826160452_gp_1_0_0_0_KeyValuePair_2_t1051722143_0_0_0,
	&GenInst_KeyValuePair_2_t1051722143_0_0_0_KeyValuePair_2_t1051722143_0_0_0,
	&GenInst_Dictionary_2_t3826160452_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t376817520_gp_0_0_0_0,
	&GenInst_DefaultComparer_t371954493_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t510648398_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t3857675627_0_0_0,
	&GenInst_IDictionary_2_t2146790633_gp_0_0_0_0_IDictionary_2_t2146790633_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2275830193_gp_0_0_0_0_KeyValuePair_2_t2275830193_gp_1_0_0_0,
	&GenInst_List_1_t1241771112_gp_0_0_0_0,
	&GenInst_Enumerator_t930421405_gp_0_0_0_0,
	&GenInst_Collection_1_t3355046425_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t2677599621_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m3692694854_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m3692694854_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m2072945890_gp_0_0_0_0,
	&GenInst_Queue_1_t167895599_gp_0_0_0_0,
	&GenInst_Enumerator_t1878021519_gp_0_0_0_0,
	&GenInst_Stack_1_t2037534615_gp_0_0_0_0,
	&GenInst_Enumerator_t3227029231_gp_0_0_0_0,
	&GenInst_HashSet_1_t3110041205_gp_0_0_0_0,
	&GenInst_Enumerator_t2912145968_gp_0_0_0_0,
	&GenInst_PrimeHelper_t363312612_gp_0_0_0_0,
	&GenInst_Enumerable_Any_m795394156_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2840543242_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m2840543242_gp_0_0_0_0_Boolean_t1039239260_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2042176342_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m2042176342_gp_0_0_0_0_Boolean_t1039239260_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2323895991_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2323895991_gp_0_0_0_0_Boolean_t1039239260_0_0_0,
	&GenInst_Component_GetComponentInChildren_m476264693_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m896983223_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m3176757661_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m2940377740_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m1572369961_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m3185394231_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2454520785_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInParent_m2474810776_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m945396275_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m4225586958_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentInChildren_m3366407555_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m3404916630_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m58698754_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m2855193608_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m3595539389_gp_0_0_0_0,
	&GenInst_Mesh_GetAllocArrayFromChannel_m3442725572_gp_0_0_0_0,
	&GenInst_Mesh_SafeLength_m4026736427_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m2097026643_gp_0_0_0_0,
	&GenInst_Mesh_SetListForChannel_m4081195211_gp_0_0_0_0,
	&GenInst_Mesh_SetUvsImpl_m2170684948_gp_0_0_0_0,
	&GenInst_Object_Instantiate_m3989777074_gp_0_0_0_0,
	&GenInst_Object_FindObjectsOfType_m3148200899_gp_0_0_0_0,
	&GenInst_Playable_IsPlayableOfType_m258057198_gp_0_0_0_0,
	&GenInst_PlayableOutput_IsPlayableOutputOfType_m4020851067_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t3974913308_gp_0_0_0_0,
	&GenInst_UnityAction_1_t1195038063_0_0_0,
	&GenInst_InvokableCall_2_t2355787272_gp_0_0_0_0_InvokableCall_2_t2355787272_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t2355787272_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2355787272_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t1710801379_gp_0_0_0_0_InvokableCall_3_t1710801379_gp_1_0_0_0_InvokableCall_3_t1710801379_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t1710801379_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t1710801379_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t1710801379_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t827458010_gp_0_0_0_0_InvokableCall_4_t827458010_gp_1_0_0_0_InvokableCall_4_t827458010_gp_2_0_0_0_InvokableCall_4_t827458010_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t827458010_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t827458010_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t827458010_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t827458010_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t1469715872_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t3652835655_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t329765484_gp_0_0_0_0_UnityEvent_2_t329765484_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t461102170_gp_0_0_0_0_UnityEvent_3_t461102170_gp_1_0_0_0_UnityEvent_3_t461102170_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t2903829384_gp_0_0_0_0_UnityEvent_4_t2903829384_gp_1_0_0_0_UnityEvent_4_t2903829384_gp_2_0_0_0_UnityEvent_4_t2903829384_gp_3_0_0_0,
	&GenInst_ExecuteEvents_Execute_m3479605520_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m3277601682_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m3888996684_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m797943676_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m4096618946_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t433795729_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m2235731956_gp_0_0_0_0,
	&GenInst_SetPropertyUtility_SetStruct_m3364939634_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t86117749_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t86117749_gp_0_0_0_0_Int32_t4116043316_0_0_0,
	&GenInst_ListPool_1_t4073877284_gp_0_0_0_0,
	&GenInst_List_1_t2549958162_0_0_0,
	&GenInst_ObjectPool_1_t2076978748_gp_0_0_0_0,
	&GenInst_Pool_1_t337449648_gp_0_0_0_0,
	&GenInst_ExposedList_1_t1803397706_gp_0_0_0_0,
	&GenInst_ExposedList_1_t1803397706_gp_0_0_0_0_ExposedList_1_ConvertAll_m4061944730_gp_0_0_0_0,
	&GenInst_ExposedList_1_ConvertAll_m4061944730_gp_0_0_0_0,
	&GenInst_Enumerator_t1863178257_gp_0_0_0_0,
	&GenInst_DoubleBuffered_1_t1464434538_gp_0_0_0_0,
	&GenInst_SkeletonRenderer_NewSpineGameObject_m853295412_gp_0_0_0_0,
	&GenInst_SkeletonRenderer_AddSpineComponent_m4245276095_gp_0_0_0_0,
	&GenInst_AnimationPlayableOutput_t2354988367_0_0_0,
	&GenInst_DefaultExecutionOrder_t135071803_0_0_0,
	&GenInst_AudioPlayableOutput_t108233817_0_0_0,
	&GenInst_PlayerConnection_t939829292_0_0_0,
	&GenInst_ScriptPlayableOutput_t3200927392_0_0_0,
	&GenInst_GUILayer_t3566394258_0_0_0,
	&GenInst_EventSystem_t1402709230_0_0_0,
	&GenInst_AxisEventData_t1890216861_0_0_0,
	&GenInst_SpriteRenderer_t1526302145_0_0_0,
	&GenInst_Image_t2836066662_0_0_0,
	&GenInst_Button_t2390191360_0_0_0,
	&GenInst_RawImage_t2407390601_0_0_0,
	&GenInst_Slider_t3490919315_0_0_0,
	&GenInst_Scrollbar_t2916731442_0_0_0,
	&GenInst_InputField_t2184098916_0_0_0,
	&GenInst_ScrollRect_t3671999215_0_0_0,
	&GenInst_Dropdown_t520728739_0_0_0,
	&GenInst_GraphicRaycaster_t4012288718_0_0_0,
	&GenInst_CanvasRenderer_t123344510_0_0_0,
	&GenInst_Corner_t1996352389_0_0_0,
	&GenInst_Axis_t2661399177_0_0_0,
	&GenInst_Constraint_t2532561711_0_0_0,
	&GenInst_SubmitEvent_t1927180329_0_0_0,
	&GenInst_OnChangeEvent_t1963771712_0_0_0,
	&GenInst_OnValidateInput_t1653235578_0_0_0,
	&GenInst_LayoutElement_t63293772_0_0_0,
	&GenInst_RectOffset_t2305860064_0_0_0,
	&GenInst_TextAnchor_t1106282459_0_0_0,
	&GenInst_AnimationTriggers_t1030489365_0_0_0,
	&GenInst_Animator_t126418246_0_0_0,
	&GenInst_MeshRenderer_t1621355309_0_0_0,
	&GenInst_CharacterController_t3006247655_0_0_0,
	&GenInst_SkeletonGraphic_t1867541356_0_0_0,
	&GenInst_SkeletonRagdoll2D_t3940222091_0_0_0,
	&GenInst_SpineboyBeginnerModel_t865536388_0_0_0,
	&GenInst_MeshFilter_t36322911_0_0_0,
	&GenInst_Mesh_t3526862104_0_0_0,
	&GenInst_HingeJoint_t2932727570_0_0_0,
	&GenInst_SphereCollider_t3968896157_0_0_0,
	&GenInst_BoxCollider_t779584950_0_0_0,
	&GenInst_HingeJoint2D_t2351416406_0_0_0,
	&GenInst_CircleCollider2D_t3428612651_0_0_0,
	&GenInst_BoxCollider2D_t3639716847_0_0_0,
	&GenInst_SkeletonRenderSeparator_t3391021243_0_0_0,
	&GenInst_SkeletonUtilityKinematicShadow_t2751461930_0_0_0,
	&GenInst_SkeletonAnimator_t3794034912_0_0_0,
	&GenInst_SkeletonUtility_t373332496_0_0_0,
	&GenInst_EventQueueEntry_t3756679084_0_0_0_EventQueueEntry_t3756679084_0_0_0,
	&GenInst_AtlasMaterialOverride_t4245281532_0_0_0_AtlasMaterialOverride_t4245281532_0_0_0,
	&GenInst_SlotMaterialOverride_t3080213539_0_0_0_SlotMaterialOverride_t3080213539_0_0_0,
	&GenInst_TransformPair_t1730302243_0_0_0_TransformPair_t1730302243_0_0_0,
	&GenInst_SubmeshInstruction_t3765917328_0_0_0_SubmeshInstruction_t3765917328_0_0_0,
	&GenInst_Boolean_t1039239260_0_0_0_Boolean_t1039239260_0_0_0,
	&GenInst_Int32_t4116043316_0_0_0_Int32_t4116043316_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t2964753189_0_0_0_CustomAttributeNamedArgument_t2964753189_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1279663203_0_0_0_CustomAttributeTypedArgument_t1279663203_0_0_0,
	&GenInst_Single_t1534300504_0_0_0_Single_t1534300504_0_0_0,
	&GenInst_AnimatorClipInfo_t3943754594_0_0_0_AnimatorClipInfo_t3943754594_0_0_0,
	&GenInst_Color32_t2411287715_0_0_0_Color32_t2411287715_0_0_0,
	&GenInst_RaycastResult_t2363482100_0_0_0_RaycastResult_t2363482100_0_0_0,
	&GenInst_UICharInfo_t3450488657_0_0_0_UICharInfo_t3450488657_0_0_0,
	&GenInst_UILineInfo_t4132474705_0_0_0_UILineInfo_t4132474705_0_0_0,
	&GenInst_UIVertex_t475044788_0_0_0_UIVertex_t475044788_0_0_0,
	&GenInst_Vector2_t1065050092_0_0_0_Vector2_t1065050092_0_0_0,
	&GenInst_Vector3_t2825674791_0_0_0_Vector3_t2825674791_0_0_0,
	&GenInst_Vector4_t2651204353_0_0_0_Vector4_t2651204353_0_0_0,
	&GenInst_AnimationPair_t2435898854_0_0_0_AnimationPair_t2435898854_0_0_0,
	&GenInst_AnimationPair_t2435898854_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t914961172_0_0_0_KeyValuePair_2_t914961172_0_0_0,
	&GenInst_KeyValuePair_2_t914961172_0_0_0_RuntimeObject_0_0_0,
	&GenInst_Single_t1534300504_0_0_0_RuntimeObject_0_0_0,
	&GenInst_AttachmentKeyTuple_t2283295499_0_0_0_AttachmentKeyTuple_t2283295499_0_0_0,
	&GenInst_KeyValuePair_2_t2697487084_0_0_0_KeyValuePair_2_t2697487084_0_0_0,
	&GenInst_KeyValuePair_2_t2697487084_0_0_0_RuntimeObject_0_0_0,
	&GenInst_MaterialTexturePair_t2729914733_0_0_0_MaterialTexturePair_t2729914733_0_0_0,
	&GenInst_KeyValuePair_2_t3542000834_0_0_0_KeyValuePair_2_t3542000834_0_0_0,
	&GenInst_KeyValuePair_2_t3542000834_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t2473691391_0_0_0_KeyValuePair_2_t2473691391_0_0_0,
	&GenInst_KeyValuePair_2_t2473691391_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3050155971_0_0_0_KeyValuePair_2_t3050155971_0_0_0,
	&GenInst_KeyValuePair_2_t3050155971_0_0_0_RuntimeObject_0_0_0,
	&GenInst_IntPtr_t_0_0_0_IntPtr_t_0_0_0,
	&GenInst_KeyValuePair_2_t1590186377_0_0_0_KeyValuePair_2_t1590186377_0_0_0,
	&GenInst_KeyValuePair_2_t1590186377_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t372023137_0_0_0_KeyValuePair_2_t372023137_0_0_0,
	&GenInst_KeyValuePair_2_t372023137_0_0_0_RuntimeObject_0_0_0,
	&GenInst_KeyValuePair_2_t3292850614_0_0_0_KeyValuePair_2_t3292850614_0_0_0,
	&GenInst_KeyValuePair_2_t3292850614_0_0_0_RuntimeObject_0_0_0,
};
