﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_t891337786;
// UnityEngine.ScriptableObject
struct ScriptableObject_t229319923;
// UnityEngine.Animator
struct Animator_t126418246;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t1598070258;
// UnityEngine.PropertyAttribute
struct PropertyAttribute_t1226012101;
// UnityEngine.TextAsset
struct TextAsset_t3707725843;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t371269159;
// UnityEngine.TextEditor
struct TextEditor_t3996366928;
// UnityEngine.GUIStyle
struct GUIStyle_t2201526215;
// UnityEngine.GUIContent
struct GUIContent_t1852996497;
// UnityEngine.Object
struct Object_t350248726;
// UnityEngine.TextGenerationSettings
struct TextGenerationSettings_t781948148;
// UnityEngine.TextGenerator
struct TextGenerator_t1842200301;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t2598460513;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t1278937086;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t1960923134;
// UnityEngine.Font
struct Font_t1312336880;
// System.Object[]
struct ObjectU5BU5D_t1100384052;
// UnityEngine.GameObject
struct GameObject_t3093992626;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t3175835163;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t1856311736;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t2538297784;
// UnityEngine.Texture
struct Texture_t4046637001;
// UnityEngine.Texture2D
struct Texture2D_t1431210461;
// UnityEngine.Color[]
struct ColorU5BU5D_t349694303;
// UnityEngine.Rect[]
struct RectU5BU5D_t75527478;
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2067778064;
// UnityEngine.ThreadAndSerializationSafeAttribute
struct ThreadAndSerializationSafeAttribute_t3127544772;
// System.Attribute
struct Attribute_t2169343654;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t1130662838;
// UnityEngine.TouchScreenKeyboard
struct TouchScreenKeyboard_t1544112657;
// UnityEngine.TrackedReference
struct TrackedReference_t4182582575;
// UnityEngine.Transform
struct Transform_t2735953680;
// System.Collections.IEnumerator
struct IEnumerator_t2667876566;
// UnityEngine.Transform/Enumerator
struct Enumerator_t1540455786;
// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct Action_1_t2233756553;
// System.Action`1<System.Object>
struct Action_1_t2925337540;
// UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback
struct RequestAtlasCallback_t2148362272;
// UnityEngine.U2D.SpriteAtlas
struct SpriteAtlas_t2050322510;
// System.IAsyncResult
struct IAsyncResult_t911908533;
// System.AsyncCallback
struct AsyncCallback_t3972436406;
// System.AppDomain
struct AppDomain_t794471768;
// System.UnhandledExceptionEventHandler
struct UnhandledExceptionEventHandler_t3425235907;
// System.UnhandledExceptionEventArgs
struct UnhandledExceptionEventArgs_t2750508014;
// System.Exception
struct Exception_t2572292308;
// System.Type
struct Type_t;
// UnityEngine.UnityException
struct UnityException_t1120912047;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1085725973;
// UnityEngine.UnityLogWriter
struct UnityLogWriter_t646926076;
// System.IO.TextWriter
struct TextWriter_t2423646586;
// UnityEngine.UnitySynchronizationContext
struct UnitySynchronizationContext_t3253602121;
// System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct Queue_1_t2108291272;
// System.Threading.Thread
struct Thread_t2629334981;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t155019481;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t3197245846;
// System.Threading.EventWaitHandle
struct EventWaitHandle_t2896330895;
// System.IndexOutOfRangeException
struct IndexOutOfRangeException_t481375535;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1533502071;
// UnityEngine.YieldInstruction
struct YieldInstruction_t1400753137;
// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t3543075621;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t4091331090;
// UnityEngine.WaitForSecondsRealtime
struct WaitForSecondsRealtime_t4106866811;
// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t1129172254;
// UnityEngine.WritableAttribute
struct WritableAttribute_t902445776;
// UnityEngineInternal.GenericStack
struct GenericStack_t1699001310;
// System.Collections.Stack
struct Stack_t1463244948;
// System.Delegate
struct Delegate_t1310841479;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngineInternal.TypeInferenceRuleAttribute
struct TypeInferenceRuleAttribute_t1740219836;
// System.Uri
struct Uri_t3689888142;
// System.Text.RegularExpressions.Regex
struct Regex_t1806904682;
// System.Uri/UriScheme[]
struct UriSchemeU5BU5D_t11359493;
// System.UriParser
struct UriParser_t1891152421;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t1463974856;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t4182055677;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t853951564;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t2192956492;
// UnityEngine.UnitySynchronizationContext/WorkRequest[]
struct WorkRequestU5BU5D_t498703845;
// System.Char[]
struct CharU5BU5D_t3669675803;
// System.Collections.Hashtable
struct Hashtable_t42495838;
// System.Collections.ArrayList
struct ArrayList_t2255692983;
// System.Runtime.Serialization.IFormatterConverter
struct IFormatterConverter_t1746829950;
// System.IntPtr[]
struct IntPtrU5BU5D_t857689409;
// System.Collections.IDictionary
struct IDictionary_t3785973992;
// System.Runtime.Remoting.ServerIdentity
struct ServerIdentity_t141605491;
// System.Threading.ManualResetEvent
struct ManualResetEvent_t562888503;
// System.Byte
struct Byte_t1072372818;
// System.Double
struct Double_t385876152;
// System.UInt16
struct UInt16_t243830035;
// System.Void
struct Void_t3157035008;
// Microsoft.Win32.SafeHandles.SafeWaitHandle
struct SafeWaitHandle_t3914948630;
// System.DelegateData
struct DelegateData_t878649875;
// System.Text.RegularExpressions.FactoryCache
struct FactoryCache_t3440100452;
// System.Text.RegularExpressions.IMachineFactory
struct IMachineFactory_t502273672;
// System.String[]
struct StringU5BU5D_t656593794;
// System.Int32[]
struct Int32U5BU5D_t281224829;
// UnityEngine.GUIStyleState
struct GUIStyleState_t60144046;
// UnityEngine.RectOffset
struct RectOffset_t2305860064;
// System.Security.Policy.Evidence
struct Evidence_t2017448698;
// System.Security.PermissionSet
struct PermissionSet_t1513179846;
// System.Security.Principal.IPrincipal
struct IPrincipal_t1358288656;
// System.AppDomainManager
struct AppDomainManager_t4198241168;
// System.ActivationContext
struct ActivationContext_t2417885991;
// System.ApplicationIdentity
struct ApplicationIdentity_t106405273;
// System.AssemblyLoadEventHandler
struct AssemblyLoadEventHandler_t3668403773;
// System.ResolveEventHandler
struct ResolveEventHandler_t548862186;
// System.EventHandler
struct EventHandler_t1537570410;
// System.Type[]
struct TypeU5BU5D_t2532561753;
// System.Reflection.MemberFilter
struct MemberFilter_t277763270;
// System.Action`1<UnityEngine.Font>
struct Action_1_t1495770923;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t3954027152;
// System.Threading.ExecutionContext
struct ExecutionContext_t445209008;
// System.MulticastDelegate
struct MulticastDelegate_t4207739222;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t3700320170;

extern RuntimeClass* GUIStyle_t2201526215_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t1065050092_il2cpp_TypeInfo_var;
extern RuntimeClass* GUIContent_t1852996497_il2cpp_TypeInfo_var;
extern const uint32_t TextEditor__ctor_m3456547775_MetadataUsageId;
extern RuntimeClass* Mathf_t1076584549_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerationSettings_CompareColors_m2069958116_MetadataUsageId;
extern const uint32_t TextGenerationSettings_CompareVector2_m2250474453_MetadataUsageId;
extern RuntimeClass* Object_t350248726_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerationSettings_Equals_m4120249280_MetadataUsageId;
struct TextGenerationSettings_t781948148_marshaled_pinvoke;
struct TextGenerationSettings_t781948148;;
struct TextGenerationSettings_t781948148_marshaled_pinvoke;;
struct TextGenerationSettings_t781948148_marshaled_com;
struct TextGenerationSettings_t781948148_marshaled_com;;
extern RuntimeClass* List_1_t2598460513_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1278937086_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t1960923134_il2cpp_TypeInfo_var;
extern const RuntimeMethod* List_1__ctor_m2708340509_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2803144634_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m543064086_RuntimeMethod_var;
extern const uint32_t TextGenerator__ctor_m3077917799_MetadataUsageId;
extern RuntimeClass* IDisposable_t2565118400_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerator_Finalize_m546403643_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t1100384052_il2cpp_TypeInfo_var;
extern RuntimeClass* Debug_t16286340_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4106943742;
extern Il2CppCodeGenString* _stringLiteral2828802952;
extern const uint32_t TextGenerator_ValidatedSettings_m2277398987_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1728124395;
extern Il2CppCodeGenString* _stringLiteral3221043172;
extern const uint32_t TextGenerator_PopulateWithErrors_m4249505679_MetadataUsageId;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t TextGenerator_PopulateWithError_m596263034_MetadataUsageId;
extern const uint32_t TextGenerator_Populate_Internal_m3120602123_MetadataUsageId;
extern const uint32_t Texture__ctor_m3453153216_MetadataUsageId;
extern RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
extern const uint32_t Texture2D__ctor_m2096256980_MetadataUsageId;
extern const uint32_t Texture2D__ctor_m2739356446_MetadataUsageId;
extern RuntimeClass* TouchScreenKeyboard_InternalConstructorHelperArguments_t857896789_il2cpp_TypeInfo_var;
extern RuntimeClass* TouchScreenKeyboardType_t3909346333_il2cpp_TypeInfo_var;
extern RuntimeClass* Convert_t4030317850_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard__ctor_m3584002309_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral5347058;
extern const uint32_t TouchScreenKeyboard_Open_m508191034_MetadataUsageId;
extern const uint32_t TouchScreenKeyboard_Open_m1092144858_MetadataUsageId;
extern RuntimeClass* TouchScreenKeyboard_t1544112657_il2cpp_TypeInfo_var;
extern const uint32_t TouchScreenKeyboard_Open_m1817258185_MetadataUsageId;
extern const uint32_t TrackedReference_op_Equality_m2363965236_MetadataUsageId;
extern RuntimeClass* TrackedReference_t4182582575_il2cpp_TypeInfo_var;
extern const uint32_t TrackedReference_Equals_m3588214792_MetadataUsageId;
extern RuntimeClass* Vector3_t2825674791_il2cpp_TypeInfo_var;
extern RuntimeClass* Quaternion_t580650070_il2cpp_TypeInfo_var;
extern const uint32_t Transform_get_right_m3355289283_MetadataUsageId;
extern const uint32_t Transform_get_up_m2269106334_MetadataUsageId;
extern const uint32_t Transform_get_forward_m1737462907_MetadataUsageId;
extern RuntimeClass* RectTransform_t1087788885_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3763031511;
extern const uint32_t Transform_set_parent_m907535055_MetadataUsageId;
extern const uint32_t Transform_Translate_m1089578796_MetadataUsageId;
extern const uint32_t Transform_Rotate_m546776242_MetadataUsageId;
extern RuntimeClass* Enumerator_t1540455786_il2cpp_TypeInfo_var;
extern const uint32_t Transform_GetEnumerator_m3370647177_MetadataUsageId;
extern RuntimeClass* SpriteAtlasManager_t1317640999_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_t2233756553_il2cpp_TypeInfo_var;
extern const RuntimeMethod* SpriteAtlasManager_Register_m2642020738_RuntimeMethod_var;
extern const RuntimeMethod* Action_1__ctor_m2443092149_RuntimeMethod_var;
extern const uint32_t SpriteAtlasManager_RequestAtlas_m3464105809_MetadataUsageId;
extern const uint32_t SpriteAtlasManager__cctor_m3369627042_MetadataUsageId;
extern RuntimeClass* UIVertex_t475044788_il2cpp_TypeInfo_var;
extern const uint32_t UIVertex__cctor_m1241900269_MetadataUsageId;
extern RuntimeClass* UnhandledExceptionHandler_t832165685_il2cpp_TypeInfo_var;
extern RuntimeClass* UnhandledExceptionEventHandler_t3425235907_il2cpp_TypeInfo_var;
extern const RuntimeMethod* UnhandledExceptionHandler_HandleUnhandledException_m1321381177_RuntimeMethod_var;
extern const uint32_t UnhandledExceptionHandler_RegisterUECatcher_m954252354_MetadataUsageId;
extern RuntimeClass* Exception_t2572292308_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2818396465;
extern const uint32_t UnhandledExceptionHandler_HandleUnhandledException_m1321381177_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2808402998;
extern const uint32_t UnhandledExceptionHandler_PrintException_m3598350440_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral374778884;
extern const uint32_t UnityException__ctor_m143873871_MetadataUsageId;
extern RuntimeClass* TextWriter_t2423646586_il2cpp_TypeInfo_var;
extern const uint32_t UnityLogWriter__ctor_m4110637147_MetadataUsageId;
extern RuntimeClass* UnityLogWriter_t646926076_il2cpp_TypeInfo_var;
extern RuntimeClass* Console_t4121508648_il2cpp_TypeInfo_var;
extern const uint32_t UnityLogWriter_Init_m2479369668_MetadataUsageId;
extern const uint32_t UnityString_Format_m3782610777_MetadataUsageId;
extern RuntimeClass* Queue_1_t2108291272_il2cpp_TypeInfo_var;
extern RuntimeClass* Thread_t2629334981_il2cpp_TypeInfo_var;
extern const RuntimeMethod* Queue_1__ctor_m2523417422_RuntimeMethod_var;
extern const uint32_t UnitySynchronizationContext__ctor_m3792825602_MetadataUsageId;
extern const RuntimeMethod* Queue_1_Dequeue_m3333247629_RuntimeMethod_var;
extern const RuntimeMethod* Queue_1_get_Count_m4197321750_RuntimeMethod_var;
extern const uint32_t UnitySynchronizationContext_Exec_m1681198408_MetadataUsageId;
extern RuntimeClass* UnitySynchronizationContext_t3253602121_il2cpp_TypeInfo_var;
extern const uint32_t UnitySynchronizationContext_InitializeSynchronizationContext_m109945450_MetadataUsageId;
extern const uint32_t UnitySynchronizationContext_ExecuteTasks_m2686422545_MetadataUsageId;
extern RuntimeClass* IndexOutOfRangeException_t481375535_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2676045397;
extern const uint32_t Vector2_get_Item_m904236465_MetadataUsageId;
extern const uint32_t Vector2_set_Item_m1154146684_MetadataUsageId;
extern RuntimeClass* Single_t1534300504_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3845830707;
extern const uint32_t Vector2_ToString_m1150693065_MetadataUsageId;
extern const uint32_t Vector2_Equals_m1183599621_MetadataUsageId;
extern const uint32_t Vector2_get_magnitude_m1391329218_MetadataUsageId;
extern const uint32_t Vector2_op_Equality_m2618668614_MetadataUsageId;
extern const uint32_t Vector2_op_Inequality_m3452301622_MetadataUsageId;
extern const uint32_t Vector2_get_zero_m644371544_MetadataUsageId;
extern const uint32_t Vector2_get_one_m748887559_MetadataUsageId;
extern const uint32_t Vector2_get_up_m4164366462_MetadataUsageId;
extern const uint32_t Vector2_get_right_m2371833040_MetadataUsageId;
extern const uint32_t Vector2__cctor_m3322260765_MetadataUsageId;
extern const uint32_t Vector3_Lerp_m4176566525_MetadataUsageId;
extern const uint32_t Vector3_MoveTowards_m1607912389_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1627034674;
extern const uint32_t Vector3_get_Item_m2529581307_MetadataUsageId;
extern const uint32_t Vector3_set_Item_m729529755_MetadataUsageId;
extern const uint32_t Vector3_Equals_m745331672_MetadataUsageId;
extern const uint32_t Vector3_Normalize_m407020897_MetadataUsageId;
extern const uint32_t Vector3_Normalize_m3124542329_MetadataUsageId;
extern const uint32_t Vector3_get_normalized_m146301709_MetadataUsageId;
extern const uint32_t Vector3_Distance_m2357219148_MetadataUsageId;
extern const uint32_t Vector3_Magnitude_m341851577_MetadataUsageId;
extern const uint32_t Vector3_get_magnitude_m59141406_MetadataUsageId;
extern const uint32_t Vector3_Min_m2986409069_MetadataUsageId;
extern const uint32_t Vector3_Max_m3682725703_MetadataUsageId;
extern const uint32_t Vector3_get_zero_m1721653641_MetadataUsageId;
extern const uint32_t Vector3_get_one_m1243491037_MetadataUsageId;
extern const uint32_t Vector3_get_forward_m3907749096_MetadataUsageId;
extern const uint32_t Vector3_get_back_m3191415170_MetadataUsageId;
extern const uint32_t Vector3_get_up_m2143830244_MetadataUsageId;
extern const uint32_t Vector3_get_down_m2933426780_MetadataUsageId;
extern const uint32_t Vector3_get_left_m3420204028_MetadataUsageId;
extern const uint32_t Vector3_get_right_m372445178_MetadataUsageId;
extern const uint32_t Vector3_op_Equality_m3932752819_MetadataUsageId;
extern const uint32_t Vector3_op_Inequality_m3309832647_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3570444392;
extern const uint32_t Vector3_ToString_m1883038722_MetadataUsageId;
extern const uint32_t Vector3__cctor_m2217123019_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3543909149;
extern const uint32_t Vector4_get_Item_m1609178195_MetadataUsageId;
extern const uint32_t Vector4_set_Item_m57064049_MetadataUsageId;
extern RuntimeClass* Vector4_t2651204353_il2cpp_TypeInfo_var;
extern const uint32_t Vector4_Equals_m3717557000_MetadataUsageId;
extern const uint32_t Vector4_get_sqrMagnitude_m3645389319_MetadataUsageId;
extern const uint32_t Vector4_get_zero_m1006770021_MetadataUsageId;
extern const uint32_t Vector4_op_Equality_m3505556570_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1736253540;
extern const uint32_t Vector4_ToString_m2329116964_MetadataUsageId;
extern const uint32_t Vector4_SqrMagnitude_m3184081721_MetadataUsageId;
extern const uint32_t Vector4__cctor_m1064385982_MetadataUsageId;
extern RuntimeClass* MathfInternal_t2540608617_il2cpp_TypeInfo_var;
extern const uint32_t MathfInternal__cctor_m3090305122_MetadataUsageId;
extern RuntimeClass* TypeInferenceRules_t2608634147_il2cpp_TypeInfo_var;
extern const uint32_t TypeInferenceRuleAttribute__ctor_m643179438_MetadataUsageId;
extern RuntimeClass* Uri_t3689888142_il2cpp_TypeInfo_var;
extern const uint32_t WebRequestUtils_RedirectTo_m493488564_MetadataUsageId;
extern RuntimeClass* Regex_t1806904682_il2cpp_TypeInfo_var;
extern RuntimeClass* WebRequestUtils_t2232581255_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2803585452;
extern const uint32_t WebRequestUtils__cctor_m3336698454_MetadataUsageId;
struct GUIStyleState_t60144046_marshaled_pinvoke;
struct GUIStyleState_t60144046_marshaled_com;
struct RectOffset_t2305860064_marshaled_com;

struct ByteU5BU5D_t371269159;
struct ObjectU5BU5D_t1100384052;
struct ColorU5BU5D_t349694303;
struct RectU5BU5D_t75527478;
struct Texture2DU5BU5D_t2067778064;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef URI_T3689888142_H
#define URI_T3689888142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_t3689888142  : public RuntimeObject
{
public:
	// System.Boolean System.Uri::isUnixFilePath
	bool ___isUnixFilePath_0;
	// System.String System.Uri::source
	String_t* ___source_1;
	// System.String System.Uri::scheme
	String_t* ___scheme_2;
	// System.String System.Uri::host
	String_t* ___host_3;
	// System.Int32 System.Uri::port
	int32_t ___port_4;
	// System.String System.Uri::path
	String_t* ___path_5;
	// System.String System.Uri::query
	String_t* ___query_6;
	// System.String System.Uri::fragment
	String_t* ___fragment_7;
	// System.String System.Uri::userinfo
	String_t* ___userinfo_8;
	// System.Boolean System.Uri::isUnc
	bool ___isUnc_9;
	// System.Boolean System.Uri::isOpaquePart
	bool ___isOpaquePart_10;
	// System.Boolean System.Uri::isAbsoluteUri
	bool ___isAbsoluteUri_11;
	// System.Boolean System.Uri::userEscaped
	bool ___userEscaped_12;
	// System.String System.Uri::cachedAbsoluteUri
	String_t* ___cachedAbsoluteUri_13;
	// System.String System.Uri::cachedToString
	String_t* ___cachedToString_14;
	// System.Int32 System.Uri::cachedHashCode
	int32_t ___cachedHashCode_15;
	// System.UriParser System.Uri::parser
	UriParser_t1891152421 * ___parser_29;

public:
	inline static int32_t get_offset_of_isUnixFilePath_0() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___isUnixFilePath_0)); }
	inline bool get_isUnixFilePath_0() const { return ___isUnixFilePath_0; }
	inline bool* get_address_of_isUnixFilePath_0() { return &___isUnixFilePath_0; }
	inline void set_isUnixFilePath_0(bool value)
	{
		___isUnixFilePath_0 = value;
	}

	inline static int32_t get_offset_of_source_1() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___source_1)); }
	inline String_t* get_source_1() const { return ___source_1; }
	inline String_t** get_address_of_source_1() { return &___source_1; }
	inline void set_source_1(String_t* value)
	{
		___source_1 = value;
		Il2CppCodeGenWriteBarrier((&___source_1), value);
	}

	inline static int32_t get_offset_of_scheme_2() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___scheme_2)); }
	inline String_t* get_scheme_2() const { return ___scheme_2; }
	inline String_t** get_address_of_scheme_2() { return &___scheme_2; }
	inline void set_scheme_2(String_t* value)
	{
		___scheme_2 = value;
		Il2CppCodeGenWriteBarrier((&___scheme_2), value);
	}

	inline static int32_t get_offset_of_host_3() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___host_3)); }
	inline String_t* get_host_3() const { return ___host_3; }
	inline String_t** get_address_of_host_3() { return &___host_3; }
	inline void set_host_3(String_t* value)
	{
		___host_3 = value;
		Il2CppCodeGenWriteBarrier((&___host_3), value);
	}

	inline static int32_t get_offset_of_port_4() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___port_4)); }
	inline int32_t get_port_4() const { return ___port_4; }
	inline int32_t* get_address_of_port_4() { return &___port_4; }
	inline void set_port_4(int32_t value)
	{
		___port_4 = value;
	}

	inline static int32_t get_offset_of_path_5() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___path_5)); }
	inline String_t* get_path_5() const { return ___path_5; }
	inline String_t** get_address_of_path_5() { return &___path_5; }
	inline void set_path_5(String_t* value)
	{
		___path_5 = value;
		Il2CppCodeGenWriteBarrier((&___path_5), value);
	}

	inline static int32_t get_offset_of_query_6() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___query_6)); }
	inline String_t* get_query_6() const { return ___query_6; }
	inline String_t** get_address_of_query_6() { return &___query_6; }
	inline void set_query_6(String_t* value)
	{
		___query_6 = value;
		Il2CppCodeGenWriteBarrier((&___query_6), value);
	}

	inline static int32_t get_offset_of_fragment_7() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___fragment_7)); }
	inline String_t* get_fragment_7() const { return ___fragment_7; }
	inline String_t** get_address_of_fragment_7() { return &___fragment_7; }
	inline void set_fragment_7(String_t* value)
	{
		___fragment_7 = value;
		Il2CppCodeGenWriteBarrier((&___fragment_7), value);
	}

	inline static int32_t get_offset_of_userinfo_8() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___userinfo_8)); }
	inline String_t* get_userinfo_8() const { return ___userinfo_8; }
	inline String_t** get_address_of_userinfo_8() { return &___userinfo_8; }
	inline void set_userinfo_8(String_t* value)
	{
		___userinfo_8 = value;
		Il2CppCodeGenWriteBarrier((&___userinfo_8), value);
	}

	inline static int32_t get_offset_of_isUnc_9() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___isUnc_9)); }
	inline bool get_isUnc_9() const { return ___isUnc_9; }
	inline bool* get_address_of_isUnc_9() { return &___isUnc_9; }
	inline void set_isUnc_9(bool value)
	{
		___isUnc_9 = value;
	}

	inline static int32_t get_offset_of_isOpaquePart_10() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___isOpaquePart_10)); }
	inline bool get_isOpaquePart_10() const { return ___isOpaquePart_10; }
	inline bool* get_address_of_isOpaquePart_10() { return &___isOpaquePart_10; }
	inline void set_isOpaquePart_10(bool value)
	{
		___isOpaquePart_10 = value;
	}

	inline static int32_t get_offset_of_isAbsoluteUri_11() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___isAbsoluteUri_11)); }
	inline bool get_isAbsoluteUri_11() const { return ___isAbsoluteUri_11; }
	inline bool* get_address_of_isAbsoluteUri_11() { return &___isAbsoluteUri_11; }
	inline void set_isAbsoluteUri_11(bool value)
	{
		___isAbsoluteUri_11 = value;
	}

	inline static int32_t get_offset_of_userEscaped_12() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___userEscaped_12)); }
	inline bool get_userEscaped_12() const { return ___userEscaped_12; }
	inline bool* get_address_of_userEscaped_12() { return &___userEscaped_12; }
	inline void set_userEscaped_12(bool value)
	{
		___userEscaped_12 = value;
	}

	inline static int32_t get_offset_of_cachedAbsoluteUri_13() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___cachedAbsoluteUri_13)); }
	inline String_t* get_cachedAbsoluteUri_13() const { return ___cachedAbsoluteUri_13; }
	inline String_t** get_address_of_cachedAbsoluteUri_13() { return &___cachedAbsoluteUri_13; }
	inline void set_cachedAbsoluteUri_13(String_t* value)
	{
		___cachedAbsoluteUri_13 = value;
		Il2CppCodeGenWriteBarrier((&___cachedAbsoluteUri_13), value);
	}

	inline static int32_t get_offset_of_cachedToString_14() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___cachedToString_14)); }
	inline String_t* get_cachedToString_14() const { return ___cachedToString_14; }
	inline String_t** get_address_of_cachedToString_14() { return &___cachedToString_14; }
	inline void set_cachedToString_14(String_t* value)
	{
		___cachedToString_14 = value;
		Il2CppCodeGenWriteBarrier((&___cachedToString_14), value);
	}

	inline static int32_t get_offset_of_cachedHashCode_15() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___cachedHashCode_15)); }
	inline int32_t get_cachedHashCode_15() const { return ___cachedHashCode_15; }
	inline int32_t* get_address_of_cachedHashCode_15() { return &___cachedHashCode_15; }
	inline void set_cachedHashCode_15(int32_t value)
	{
		___cachedHashCode_15 = value;
	}

	inline static int32_t get_offset_of_parser_29() { return static_cast<int32_t>(offsetof(Uri_t3689888142, ___parser_29)); }
	inline UriParser_t1891152421 * get_parser_29() const { return ___parser_29; }
	inline UriParser_t1891152421 ** get_address_of_parser_29() { return &___parser_29; }
	inline void set_parser_29(UriParser_t1891152421 * value)
	{
		___parser_29 = value;
		Il2CppCodeGenWriteBarrier((&___parser_29), value);
	}
};

struct Uri_t3689888142_StaticFields
{
public:
	// System.String System.Uri::hexUpperChars
	String_t* ___hexUpperChars_16;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_17;
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_18;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_19;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_20;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_21;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_22;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_23;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_24;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_25;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_26;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_27;
	// System.Uri/UriScheme[] System.Uri::schemes
	UriSchemeU5BU5D_t11359493* ___schemes_28;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map14
	Dictionary_2_t1463974856 * ___U3CU3Ef__switchU24map14_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map15
	Dictionary_2_t1463974856 * ___U3CU3Ef__switchU24map15_31;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Uri::<>f__switch$map16
	Dictionary_2_t1463974856 * ___U3CU3Ef__switchU24map16_32;

public:
	inline static int32_t get_offset_of_hexUpperChars_16() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___hexUpperChars_16)); }
	inline String_t* get_hexUpperChars_16() const { return ___hexUpperChars_16; }
	inline String_t** get_address_of_hexUpperChars_16() { return &___hexUpperChars_16; }
	inline void set_hexUpperChars_16(String_t* value)
	{
		___hexUpperChars_16 = value;
		Il2CppCodeGenWriteBarrier((&___hexUpperChars_16), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_17() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___SchemeDelimiter_17)); }
	inline String_t* get_SchemeDelimiter_17() const { return ___SchemeDelimiter_17; }
	inline String_t** get_address_of_SchemeDelimiter_17() { return &___SchemeDelimiter_17; }
	inline void set_SchemeDelimiter_17(String_t* value)
	{
		___SchemeDelimiter_17 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_17), value);
	}

	inline static int32_t get_offset_of_UriSchemeFile_18() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___UriSchemeFile_18)); }
	inline String_t* get_UriSchemeFile_18() const { return ___UriSchemeFile_18; }
	inline String_t** get_address_of_UriSchemeFile_18() { return &___UriSchemeFile_18; }
	inline void set_UriSchemeFile_18(String_t* value)
	{
		___UriSchemeFile_18 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_18), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_19() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___UriSchemeFtp_19)); }
	inline String_t* get_UriSchemeFtp_19() const { return ___UriSchemeFtp_19; }
	inline String_t** get_address_of_UriSchemeFtp_19() { return &___UriSchemeFtp_19; }
	inline void set_UriSchemeFtp_19(String_t* value)
	{
		___UriSchemeFtp_19 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_19), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_20() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___UriSchemeGopher_20)); }
	inline String_t* get_UriSchemeGopher_20() const { return ___UriSchemeGopher_20; }
	inline String_t** get_address_of_UriSchemeGopher_20() { return &___UriSchemeGopher_20; }
	inline void set_UriSchemeGopher_20(String_t* value)
	{
		___UriSchemeGopher_20 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_20), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_21() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___UriSchemeHttp_21)); }
	inline String_t* get_UriSchemeHttp_21() const { return ___UriSchemeHttp_21; }
	inline String_t** get_address_of_UriSchemeHttp_21() { return &___UriSchemeHttp_21; }
	inline void set_UriSchemeHttp_21(String_t* value)
	{
		___UriSchemeHttp_21 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_21), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_22() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___UriSchemeHttps_22)); }
	inline String_t* get_UriSchemeHttps_22() const { return ___UriSchemeHttps_22; }
	inline String_t** get_address_of_UriSchemeHttps_22() { return &___UriSchemeHttps_22; }
	inline void set_UriSchemeHttps_22(String_t* value)
	{
		___UriSchemeHttps_22 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_22), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_23() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___UriSchemeMailto_23)); }
	inline String_t* get_UriSchemeMailto_23() const { return ___UriSchemeMailto_23; }
	inline String_t** get_address_of_UriSchemeMailto_23() { return &___UriSchemeMailto_23; }
	inline void set_UriSchemeMailto_23(String_t* value)
	{
		___UriSchemeMailto_23 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_23), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_24() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___UriSchemeNews_24)); }
	inline String_t* get_UriSchemeNews_24() const { return ___UriSchemeNews_24; }
	inline String_t** get_address_of_UriSchemeNews_24() { return &___UriSchemeNews_24; }
	inline void set_UriSchemeNews_24(String_t* value)
	{
		___UriSchemeNews_24 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_24), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_25() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___UriSchemeNntp_25)); }
	inline String_t* get_UriSchemeNntp_25() const { return ___UriSchemeNntp_25; }
	inline String_t** get_address_of_UriSchemeNntp_25() { return &___UriSchemeNntp_25; }
	inline void set_UriSchemeNntp_25(String_t* value)
	{
		___UriSchemeNntp_25 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_25), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_26() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___UriSchemeNetPipe_26)); }
	inline String_t* get_UriSchemeNetPipe_26() const { return ___UriSchemeNetPipe_26; }
	inline String_t** get_address_of_UriSchemeNetPipe_26() { return &___UriSchemeNetPipe_26; }
	inline void set_UriSchemeNetPipe_26(String_t* value)
	{
		___UriSchemeNetPipe_26 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_26), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_27() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___UriSchemeNetTcp_27)); }
	inline String_t* get_UriSchemeNetTcp_27() const { return ___UriSchemeNetTcp_27; }
	inline String_t** get_address_of_UriSchemeNetTcp_27() { return &___UriSchemeNetTcp_27; }
	inline void set_UriSchemeNetTcp_27(String_t* value)
	{
		___UriSchemeNetTcp_27 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_27), value);
	}

	inline static int32_t get_offset_of_schemes_28() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___schemes_28)); }
	inline UriSchemeU5BU5D_t11359493* get_schemes_28() const { return ___schemes_28; }
	inline UriSchemeU5BU5D_t11359493** get_address_of_schemes_28() { return &___schemes_28; }
	inline void set_schemes_28(UriSchemeU5BU5D_t11359493* value)
	{
		___schemes_28 = value;
		Il2CppCodeGenWriteBarrier((&___schemes_28), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map14_30() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___U3CU3Ef__switchU24map14_30)); }
	inline Dictionary_2_t1463974856 * get_U3CU3Ef__switchU24map14_30() const { return ___U3CU3Ef__switchU24map14_30; }
	inline Dictionary_2_t1463974856 ** get_address_of_U3CU3Ef__switchU24map14_30() { return &___U3CU3Ef__switchU24map14_30; }
	inline void set_U3CU3Ef__switchU24map14_30(Dictionary_2_t1463974856 * value)
	{
		___U3CU3Ef__switchU24map14_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map14_30), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map15_31() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___U3CU3Ef__switchU24map15_31)); }
	inline Dictionary_2_t1463974856 * get_U3CU3Ef__switchU24map15_31() const { return ___U3CU3Ef__switchU24map15_31; }
	inline Dictionary_2_t1463974856 ** get_address_of_U3CU3Ef__switchU24map15_31() { return &___U3CU3Ef__switchU24map15_31; }
	inline void set_U3CU3Ef__switchU24map15_31(Dictionary_2_t1463974856 * value)
	{
		___U3CU3Ef__switchU24map15_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map15_31), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__switchU24map16_32() { return static_cast<int32_t>(offsetof(Uri_t3689888142_StaticFields, ___U3CU3Ef__switchU24map16_32)); }
	inline Dictionary_2_t1463974856 * get_U3CU3Ef__switchU24map16_32() const { return ___U3CU3Ef__switchU24map16_32; }
	inline Dictionary_2_t1463974856 ** get_address_of_U3CU3Ef__switchU24map16_32() { return &___U3CU3Ef__switchU24map16_32; }
	inline void set_U3CU3Ef__switchU24map16_32(Dictionary_2_t1463974856 * value)
	{
		___U3CU3Ef__switchU24map16_32 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map16_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_T3689888142_H
#ifndef WEBREQUESTUTILS_T2232581255_H
#define WEBREQUESTUTILS_T2232581255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.WebRequestUtils
struct  WebRequestUtils_t2232581255  : public RuntimeObject
{
public:

public:
};

struct WebRequestUtils_t2232581255_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex UnityEngineInternal.WebRequestUtils::domainRegex
	Regex_t1806904682 * ___domainRegex_0;

public:
	inline static int32_t get_offset_of_domainRegex_0() { return static_cast<int32_t>(offsetof(WebRequestUtils_t2232581255_StaticFields, ___domainRegex_0)); }
	inline Regex_t1806904682 * get_domainRegex_0() const { return ___domainRegex_0; }
	inline Regex_t1806904682 ** get_address_of_domainRegex_0() { return &___domainRegex_0; }
	inline void set_domainRegex_0(Regex_t1806904682 * value)
	{
		___domainRegex_0 = value;
		Il2CppCodeGenWriteBarrier((&___domainRegex_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WEBREQUESTUTILS_T2232581255_H
#ifndef SCRIPTINGUTILS_T2530100749_H
#define SCRIPTINGUTILS_T2530100749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.ScriptingUtils
struct  ScriptingUtils_t2530100749  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINGUTILS_T2530100749_H
#ifndef LIST_1_T2598460513_H
#define LIST_1_T2598460513_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct  List_1_t2598460513  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UIVertexU5BU5D_t4182055677* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2598460513, ____items_1)); }
	inline UIVertexU5BU5D_t4182055677* get__items_1() const { return ____items_1; }
	inline UIVertexU5BU5D_t4182055677** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UIVertexU5BU5D_t4182055677* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2598460513, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2598460513, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t2598460513_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	UIVertexU5BU5D_t4182055677* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t2598460513_StaticFields, ___EmptyArray_4)); }
	inline UIVertexU5BU5D_t4182055677* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline UIVertexU5BU5D_t4182055677** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(UIVertexU5BU5D_t4182055677* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2598460513_H
#ifndef LIST_1_T1278937086_H
#define LIST_1_T1278937086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct  List_1_t1278937086  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UICharInfoU5BU5D_t853951564* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1278937086, ____items_1)); }
	inline UICharInfoU5BU5D_t853951564* get__items_1() const { return ____items_1; }
	inline UICharInfoU5BU5D_t853951564** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UICharInfoU5BU5D_t853951564* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1278937086, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1278937086, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1278937086_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	UICharInfoU5BU5D_t853951564* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1278937086_StaticFields, ___EmptyArray_4)); }
	inline UICharInfoU5BU5D_t853951564* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline UICharInfoU5BU5D_t853951564** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(UICharInfoU5BU5D_t853951564* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1278937086_H
#ifndef LIST_1_T1960923134_H
#define LIST_1_T1960923134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct  List_1_t1960923134  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UILineInfoU5BU5D_t2192956492* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t1960923134, ____items_1)); }
	inline UILineInfoU5BU5D_t2192956492* get__items_1() const { return ____items_1; }
	inline UILineInfoU5BU5D_t2192956492** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UILineInfoU5BU5D_t2192956492* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t1960923134, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t1960923134, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

struct List_1_t1960923134_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::EmptyArray
	UILineInfoU5BU5D_t2192956492* ___EmptyArray_4;

public:
	inline static int32_t get_offset_of_EmptyArray_4() { return static_cast<int32_t>(offsetof(List_1_t1960923134_StaticFields, ___EmptyArray_4)); }
	inline UILineInfoU5BU5D_t2192956492* get_EmptyArray_4() const { return ___EmptyArray_4; }
	inline UILineInfoU5BU5D_t2192956492** get_address_of_EmptyArray_4() { return &___EmptyArray_4; }
	inline void set_EmptyArray_4(UILineInfoU5BU5D_t2192956492* value)
	{
		___EmptyArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T1960923134_H
#ifndef NETFXCOREEXTENSIONS_T1787233485_H
#define NETFXCOREEXTENSIONS_T1787233485_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.NetFxCoreExtensions
struct  NetFxCoreExtensions_t1787233485  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NETFXCOREEXTENSIONS_T1787233485_H
#ifndef STACK_T1463244948_H
#define STACK_T1463244948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Stack
struct  Stack_t1463244948  : public RuntimeObject
{
public:
	// System.Object[] System.Collections.Stack::contents
	ObjectU5BU5D_t1100384052* ___contents_0;
	// System.Int32 System.Collections.Stack::current
	int32_t ___current_1;
	// System.Int32 System.Collections.Stack::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Stack::capacity
	int32_t ___capacity_3;
	// System.Int32 System.Collections.Stack::modCount
	int32_t ___modCount_4;

public:
	inline static int32_t get_offset_of_contents_0() { return static_cast<int32_t>(offsetof(Stack_t1463244948, ___contents_0)); }
	inline ObjectU5BU5D_t1100384052* get_contents_0() const { return ___contents_0; }
	inline ObjectU5BU5D_t1100384052** get_address_of_contents_0() { return &___contents_0; }
	inline void set_contents_0(ObjectU5BU5D_t1100384052* value)
	{
		___contents_0 = value;
		Il2CppCodeGenWriteBarrier((&___contents_0), value);
	}

	inline static int32_t get_offset_of_current_1() { return static_cast<int32_t>(offsetof(Stack_t1463244948, ___current_1)); }
	inline int32_t get_current_1() const { return ___current_1; }
	inline int32_t* get_address_of_current_1() { return &___current_1; }
	inline void set_current_1(int32_t value)
	{
		___current_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Stack_t1463244948, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_capacity_3() { return static_cast<int32_t>(offsetof(Stack_t1463244948, ___capacity_3)); }
	inline int32_t get_capacity_3() const { return ___capacity_3; }
	inline int32_t* get_address_of_capacity_3() { return &___capacity_3; }
	inline void set_capacity_3(int32_t value)
	{
		___capacity_3 = value;
	}

	inline static int32_t get_offset_of_modCount_4() { return static_cast<int32_t>(offsetof(Stack_t1463244948, ___modCount_4)); }
	inline int32_t get_modCount_4() const { return ___modCount_4; }
	inline int32_t* get_address_of_modCount_4() { return &___modCount_4; }
	inline void set_modCount_4(int32_t value)
	{
		___modCount_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_T1463244948_H
#ifndef ATTRIBUTE_T2169343654_H
#define ATTRIBUTE_T2169343654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t2169343654  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T2169343654_H
#ifndef TIME_T278910633_H
#define TIME_T278910633_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Time
struct  Time_t278910633  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIME_T278910633_H
#ifndef VALUETYPE_T2697047229_H
#define VALUETYPE_T2697047229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2697047229  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2697047229_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2697047229_marshaled_com
{
};
#endif // VALUETYPE_T2697047229_H
#ifndef CUSTOMYIELDINSTRUCTION_T1129172254_H
#define CUSTOMYIELDINSTRUCTION_T1129172254_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.CustomYieldInstruction
struct  CustomYieldInstruction_t1129172254  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CUSTOMYIELDINSTRUCTION_T1129172254_H
#ifndef ENUMERATOR_T1540455786_H
#define ENUMERATOR_T1540455786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform/Enumerator
struct  Enumerator_t1540455786  : public RuntimeObject
{
public:
	// UnityEngine.Transform UnityEngine.Transform/Enumerator::outer
	Transform_t2735953680 * ___outer_0;
	// System.Int32 UnityEngine.Transform/Enumerator::currentIndex
	int32_t ___currentIndex_1;

public:
	inline static int32_t get_offset_of_outer_0() { return static_cast<int32_t>(offsetof(Enumerator_t1540455786, ___outer_0)); }
	inline Transform_t2735953680 * get_outer_0() const { return ___outer_0; }
	inline Transform_t2735953680 ** get_address_of_outer_0() { return &___outer_0; }
	inline void set_outer_0(Transform_t2735953680 * value)
	{
		___outer_0 = value;
		Il2CppCodeGenWriteBarrier((&___outer_0), value);
	}

	inline static int32_t get_offset_of_currentIndex_1() { return static_cast<int32_t>(offsetof(Enumerator_t1540455786, ___currentIndex_1)); }
	inline int32_t get_currentIndex_1() const { return ___currentIndex_1; }
	inline int32_t* get_address_of_currentIndex_1() { return &___currentIndex_1; }
	inline void set_currentIndex_1(int32_t value)
	{
		___currentIndex_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T1540455786_H
#ifndef SYNCHRONIZATIONCONTEXT_T155019481_H
#define SYNCHRONIZATIONCONTEXT_T155019481_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SynchronizationContext
struct  SynchronizationContext_t155019481  : public RuntimeObject
{
public:

public:
};

struct SynchronizationContext_t155019481_ThreadStaticFields
{
public:
	// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::currentContext
	SynchronizationContext_t155019481 * ___currentContext_0;

public:
	inline static int32_t get_offset_of_currentContext_0() { return static_cast<int32_t>(offsetof(SynchronizationContext_t155019481_ThreadStaticFields, ___currentContext_0)); }
	inline SynchronizationContext_t155019481 * get_currentContext_0() const { return ___currentContext_0; }
	inline SynchronizationContext_t155019481 ** get_address_of_currentContext_0() { return &___currentContext_0; }
	inline void set_currentContext_0(SynchronizationContext_t155019481 * value)
	{
		___currentContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNCHRONIZATIONCONTEXT_T155019481_H
#ifndef SPRITEATLASMANAGER_T1317640999_H
#define SPRITEATLASMANAGER_T1317640999_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.U2D.SpriteAtlasManager
struct  SpriteAtlasManager_t1317640999  : public RuntimeObject
{
public:

public:
};

struct SpriteAtlasManager_t1317640999_StaticFields
{
public:
	// UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback UnityEngine.U2D.SpriteAtlasManager::atlasRequested
	RequestAtlasCallback_t2148362272 * ___atlasRequested_0;
	// System.Action`1<UnityEngine.U2D.SpriteAtlas> UnityEngine.U2D.SpriteAtlasManager::<>f__mg$cache0
	Action_1_t2233756553 * ___U3CU3Ef__mgU24cache0_1;

public:
	inline static int32_t get_offset_of_atlasRequested_0() { return static_cast<int32_t>(offsetof(SpriteAtlasManager_t1317640999_StaticFields, ___atlasRequested_0)); }
	inline RequestAtlasCallback_t2148362272 * get_atlasRequested_0() const { return ___atlasRequested_0; }
	inline RequestAtlasCallback_t2148362272 ** get_address_of_atlasRequested_0() { return &___atlasRequested_0; }
	inline void set_atlasRequested_0(RequestAtlasCallback_t2148362272 * value)
	{
		___atlasRequested_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasRequested_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_1() { return static_cast<int32_t>(offsetof(SpriteAtlasManager_t1317640999_StaticFields, ___U3CU3Ef__mgU24cache0_1)); }
	inline Action_1_t2233756553 * get_U3CU3Ef__mgU24cache0_1() const { return ___U3CU3Ef__mgU24cache0_1; }
	inline Action_1_t2233756553 ** get_address_of_U3CU3Ef__mgU24cache0_1() { return &___U3CU3Ef__mgU24cache0_1; }
	inline void set_U3CU3Ef__mgU24cache0_1(Action_1_t2233756553 * value)
	{
		___U3CU3Ef__mgU24cache0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEATLASMANAGER_T1317640999_H
#ifndef QUEUE_1_T2108291272_H
#define QUEUE_1_T2108291272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>
struct  Queue_1_t2108291272  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	WorkRequestU5BU5D_t498703845* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_t2108291272, ____array_0)); }
	inline WorkRequestU5BU5D_t498703845* get__array_0() const { return ____array_0; }
	inline WorkRequestU5BU5D_t498703845** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(WorkRequestU5BU5D_t498703845* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_t2108291272, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(Queue_1_t2108291272, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(Queue_1_t2108291272, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUEUE_1_T2108291272_H
#ifndef UNITYSTRING_T3521877697_H
#define UNITYSTRING_T3521877697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnityString
struct  UnityString_t3521877697  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYSTRING_T3521877697_H
#ifndef UISYSTEMPROFILERAPI_T977285471_H
#define UISYSTEMPROFILERAPI_T977285471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi
struct  UISystemProfilerApi_t977285471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UISYSTEMPROFILERAPI_T977285471_H
#ifndef TEXTWRITER_T2423646586_H
#define TEXTWRITER_T2423646586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.TextWriter
struct  TextWriter_t2423646586  : public RuntimeObject
{
public:
	// System.Char[] System.IO.TextWriter::CoreNewLine
	CharU5BU5D_t3669675803* ___CoreNewLine_0;

public:
	inline static int32_t get_offset_of_CoreNewLine_0() { return static_cast<int32_t>(offsetof(TextWriter_t2423646586, ___CoreNewLine_0)); }
	inline CharU5BU5D_t3669675803* get_CoreNewLine_0() const { return ___CoreNewLine_0; }
	inline CharU5BU5D_t3669675803** get_address_of_CoreNewLine_0() { return &___CoreNewLine_0; }
	inline void set_CoreNewLine_0(CharU5BU5D_t3669675803* value)
	{
		___CoreNewLine_0 = value;
		Il2CppCodeGenWriteBarrier((&___CoreNewLine_0), value);
	}
};

struct TextWriter_t2423646586_StaticFields
{
public:
	// System.IO.TextWriter System.IO.TextWriter::Null
	TextWriter_t2423646586 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(TextWriter_t2423646586_StaticFields, ___Null_1)); }
	inline TextWriter_t2423646586 * get_Null_1() const { return ___Null_1; }
	inline TextWriter_t2423646586 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(TextWriter_t2423646586 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTWRITER_T2423646586_H
#ifndef UNHANDLEDEXCEPTIONHANDLER_T832165685_H
#define UNHANDLEDEXCEPTIONHANDLER_T832165685_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnhandledExceptionHandler
struct  UnhandledExceptionHandler_t832165685  : public RuntimeObject
{
public:

public:
};

struct UnhandledExceptionHandler_t832165685_StaticFields
{
public:
	// System.UnhandledExceptionEventHandler UnityEngine.UnhandledExceptionHandler::<>f__mg$cache0
	UnhandledExceptionEventHandler_t3425235907 * ___U3CU3Ef__mgU24cache0_0;

public:
	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_0() { return static_cast<int32_t>(offsetof(UnhandledExceptionHandler_t832165685_StaticFields, ___U3CU3Ef__mgU24cache0_0)); }
	inline UnhandledExceptionEventHandler_t3425235907 * get_U3CU3Ef__mgU24cache0_0() const { return ___U3CU3Ef__mgU24cache0_0; }
	inline UnhandledExceptionEventHandler_t3425235907 ** get_address_of_U3CU3Ef__mgU24cache0_0() { return &___U3CU3Ef__mgU24cache0_0; }
	inline void set_U3CU3Ef__mgU24cache0_0(UnhandledExceptionEventHandler_t3425235907 * value)
	{
		___U3CU3Ef__mgU24cache0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNHANDLEDEXCEPTIONHANDLER_T832165685_H
#ifndef SERIALIZATIONINFO_T1085725973_H
#define SERIALIZATIONINFO_T1085725973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationInfo
struct  SerializationInfo_t1085725973  : public RuntimeObject
{
public:
	// System.Collections.Hashtable System.Runtime.Serialization.SerializationInfo::serialized
	Hashtable_t42495838 * ___serialized_0;
	// System.Collections.ArrayList System.Runtime.Serialization.SerializationInfo::values
	ArrayList_t2255692983 * ___values_1;
	// System.String System.Runtime.Serialization.SerializationInfo::assemblyName
	String_t* ___assemblyName_2;
	// System.String System.Runtime.Serialization.SerializationInfo::fullTypeName
	String_t* ___fullTypeName_3;
	// System.Runtime.Serialization.IFormatterConverter System.Runtime.Serialization.SerializationInfo::converter
	RuntimeObject* ___converter_4;

public:
	inline static int32_t get_offset_of_serialized_0() { return static_cast<int32_t>(offsetof(SerializationInfo_t1085725973, ___serialized_0)); }
	inline Hashtable_t42495838 * get_serialized_0() const { return ___serialized_0; }
	inline Hashtable_t42495838 ** get_address_of_serialized_0() { return &___serialized_0; }
	inline void set_serialized_0(Hashtable_t42495838 * value)
	{
		___serialized_0 = value;
		Il2CppCodeGenWriteBarrier((&___serialized_0), value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(SerializationInfo_t1085725973, ___values_1)); }
	inline ArrayList_t2255692983 * get_values_1() const { return ___values_1; }
	inline ArrayList_t2255692983 ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(ArrayList_t2255692983 * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_assemblyName_2() { return static_cast<int32_t>(offsetof(SerializationInfo_t1085725973, ___assemblyName_2)); }
	inline String_t* get_assemblyName_2() const { return ___assemblyName_2; }
	inline String_t** get_address_of_assemblyName_2() { return &___assemblyName_2; }
	inline void set_assemblyName_2(String_t* value)
	{
		___assemblyName_2 = value;
		Il2CppCodeGenWriteBarrier((&___assemblyName_2), value);
	}

	inline static int32_t get_offset_of_fullTypeName_3() { return static_cast<int32_t>(offsetof(SerializationInfo_t1085725973, ___fullTypeName_3)); }
	inline String_t* get_fullTypeName_3() const { return ___fullTypeName_3; }
	inline String_t** get_address_of_fullTypeName_3() { return &___fullTypeName_3; }
	inline void set_fullTypeName_3(String_t* value)
	{
		___fullTypeName_3 = value;
		Il2CppCodeGenWriteBarrier((&___fullTypeName_3), value);
	}

	inline static int32_t get_offset_of_converter_4() { return static_cast<int32_t>(offsetof(SerializationInfo_t1085725973, ___converter_4)); }
	inline RuntimeObject* get_converter_4() const { return ___converter_4; }
	inline RuntimeObject** get_address_of_converter_4() { return &___converter_4; }
	inline void set_converter_4(RuntimeObject* value)
	{
		___converter_4 = value;
		Il2CppCodeGenWriteBarrier((&___converter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONINFO_T1085725973_H
#ifndef EXCEPTION_T2572292308_H
#define EXCEPTION_T2572292308_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t2572292308  : public RuntimeObject
{
public:
	// System.IntPtr[] System.Exception::trace_ips
	IntPtrU5BU5D_t857689409* ___trace_ips_0;
	// System.Exception System.Exception::inner_exception
	Exception_t2572292308 * ___inner_exception_1;
	// System.String System.Exception::message
	String_t* ___message_2;
	// System.String System.Exception::help_link
	String_t* ___help_link_3;
	// System.String System.Exception::class_name
	String_t* ___class_name_4;
	// System.String System.Exception::stack_trace
	String_t* ___stack_trace_5;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_6;
	// System.Int32 System.Exception::remote_stack_index
	int32_t ___remote_stack_index_7;
	// System.Int32 System.Exception::hresult
	int32_t ___hresult_8;
	// System.String System.Exception::source
	String_t* ___source_9;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_10;

public:
	inline static int32_t get_offset_of_trace_ips_0() { return static_cast<int32_t>(offsetof(Exception_t2572292308, ___trace_ips_0)); }
	inline IntPtrU5BU5D_t857689409* get_trace_ips_0() const { return ___trace_ips_0; }
	inline IntPtrU5BU5D_t857689409** get_address_of_trace_ips_0() { return &___trace_ips_0; }
	inline void set_trace_ips_0(IntPtrU5BU5D_t857689409* value)
	{
		___trace_ips_0 = value;
		Il2CppCodeGenWriteBarrier((&___trace_ips_0), value);
	}

	inline static int32_t get_offset_of_inner_exception_1() { return static_cast<int32_t>(offsetof(Exception_t2572292308, ___inner_exception_1)); }
	inline Exception_t2572292308 * get_inner_exception_1() const { return ___inner_exception_1; }
	inline Exception_t2572292308 ** get_address_of_inner_exception_1() { return &___inner_exception_1; }
	inline void set_inner_exception_1(Exception_t2572292308 * value)
	{
		___inner_exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___inner_exception_1), value);
	}

	inline static int32_t get_offset_of_message_2() { return static_cast<int32_t>(offsetof(Exception_t2572292308, ___message_2)); }
	inline String_t* get_message_2() const { return ___message_2; }
	inline String_t** get_address_of_message_2() { return &___message_2; }
	inline void set_message_2(String_t* value)
	{
		___message_2 = value;
		Il2CppCodeGenWriteBarrier((&___message_2), value);
	}

	inline static int32_t get_offset_of_help_link_3() { return static_cast<int32_t>(offsetof(Exception_t2572292308, ___help_link_3)); }
	inline String_t* get_help_link_3() const { return ___help_link_3; }
	inline String_t** get_address_of_help_link_3() { return &___help_link_3; }
	inline void set_help_link_3(String_t* value)
	{
		___help_link_3 = value;
		Il2CppCodeGenWriteBarrier((&___help_link_3), value);
	}

	inline static int32_t get_offset_of_class_name_4() { return static_cast<int32_t>(offsetof(Exception_t2572292308, ___class_name_4)); }
	inline String_t* get_class_name_4() const { return ___class_name_4; }
	inline String_t** get_address_of_class_name_4() { return &___class_name_4; }
	inline void set_class_name_4(String_t* value)
	{
		___class_name_4 = value;
		Il2CppCodeGenWriteBarrier((&___class_name_4), value);
	}

	inline static int32_t get_offset_of_stack_trace_5() { return static_cast<int32_t>(offsetof(Exception_t2572292308, ___stack_trace_5)); }
	inline String_t* get_stack_trace_5() const { return ___stack_trace_5; }
	inline String_t** get_address_of_stack_trace_5() { return &___stack_trace_5; }
	inline void set_stack_trace_5(String_t* value)
	{
		___stack_trace_5 = value;
		Il2CppCodeGenWriteBarrier((&___stack_trace_5), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_6() { return static_cast<int32_t>(offsetof(Exception_t2572292308, ____remoteStackTraceString_6)); }
	inline String_t* get__remoteStackTraceString_6() const { return ____remoteStackTraceString_6; }
	inline String_t** get_address_of__remoteStackTraceString_6() { return &____remoteStackTraceString_6; }
	inline void set__remoteStackTraceString_6(String_t* value)
	{
		____remoteStackTraceString_6 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_6), value);
	}

	inline static int32_t get_offset_of_remote_stack_index_7() { return static_cast<int32_t>(offsetof(Exception_t2572292308, ___remote_stack_index_7)); }
	inline int32_t get_remote_stack_index_7() const { return ___remote_stack_index_7; }
	inline int32_t* get_address_of_remote_stack_index_7() { return &___remote_stack_index_7; }
	inline void set_remote_stack_index_7(int32_t value)
	{
		___remote_stack_index_7 = value;
	}

	inline static int32_t get_offset_of_hresult_8() { return static_cast<int32_t>(offsetof(Exception_t2572292308, ___hresult_8)); }
	inline int32_t get_hresult_8() const { return ___hresult_8; }
	inline int32_t* get_address_of_hresult_8() { return &___hresult_8; }
	inline void set_hresult_8(int32_t value)
	{
		___hresult_8 = value;
	}

	inline static int32_t get_offset_of_source_9() { return static_cast<int32_t>(offsetof(Exception_t2572292308, ___source_9)); }
	inline String_t* get_source_9() const { return ___source_9; }
	inline String_t** get_address_of_source_9() { return &___source_9; }
	inline void set_source_9(String_t* value)
	{
		___source_9 = value;
		Il2CppCodeGenWriteBarrier((&___source_9), value);
	}

	inline static int32_t get_offset_of__data_10() { return static_cast<int32_t>(offsetof(Exception_t2572292308, ____data_10)); }
	inline RuntimeObject* get__data_10() const { return ____data_10; }
	inline RuntimeObject** get_address_of__data_10() { return &____data_10; }
	inline void set__data_10(RuntimeObject* value)
	{
		____data_10 = value;
		Il2CppCodeGenWriteBarrier((&____data_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXCEPTION_T2572292308_H
#ifndef YIELDINSTRUCTION_T1400753137_H
#define YIELDINSTRUCTION_T1400753137_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t1400753137  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t1400753137_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t1400753137_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T1400753137_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef GUICONTENT_T1852996497_H
#define GUICONTENT_T1852996497_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIContent
struct  GUIContent_t1852996497  : public RuntimeObject
{
public:
	// System.String UnityEngine.GUIContent::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Texture UnityEngine.GUIContent::m_Image
	Texture_t4046637001 * ___m_Image_1;
	// System.String UnityEngine.GUIContent::m_Tooltip
	String_t* ___m_Tooltip_2;

public:
	inline static int32_t get_offset_of_m_Text_0() { return static_cast<int32_t>(offsetof(GUIContent_t1852996497, ___m_Text_0)); }
	inline String_t* get_m_Text_0() const { return ___m_Text_0; }
	inline String_t** get_address_of_m_Text_0() { return &___m_Text_0; }
	inline void set_m_Text_0(String_t* value)
	{
		___m_Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_0), value);
	}

	inline static int32_t get_offset_of_m_Image_1() { return static_cast<int32_t>(offsetof(GUIContent_t1852996497, ___m_Image_1)); }
	inline Texture_t4046637001 * get_m_Image_1() const { return ___m_Image_1; }
	inline Texture_t4046637001 ** get_address_of_m_Image_1() { return &___m_Image_1; }
	inline void set_m_Image_1(Texture_t4046637001 * value)
	{
		___m_Image_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(GUIContent_t1852996497, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}
};

struct GUIContent_t1852996497_StaticFields
{
public:
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Text
	GUIContent_t1852996497 * ___s_Text_3;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Image
	GUIContent_t1852996497 * ___s_Image_4;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_TextImage
	GUIContent_t1852996497 * ___s_TextImage_5;
	// UnityEngine.GUIContent UnityEngine.GUIContent::none
	GUIContent_t1852996497 * ___none_6;

public:
	inline static int32_t get_offset_of_s_Text_3() { return static_cast<int32_t>(offsetof(GUIContent_t1852996497_StaticFields, ___s_Text_3)); }
	inline GUIContent_t1852996497 * get_s_Text_3() const { return ___s_Text_3; }
	inline GUIContent_t1852996497 ** get_address_of_s_Text_3() { return &___s_Text_3; }
	inline void set_s_Text_3(GUIContent_t1852996497 * value)
	{
		___s_Text_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Text_3), value);
	}

	inline static int32_t get_offset_of_s_Image_4() { return static_cast<int32_t>(offsetof(GUIContent_t1852996497_StaticFields, ___s_Image_4)); }
	inline GUIContent_t1852996497 * get_s_Image_4() const { return ___s_Image_4; }
	inline GUIContent_t1852996497 ** get_address_of_s_Image_4() { return &___s_Image_4; }
	inline void set_s_Image_4(GUIContent_t1852996497 * value)
	{
		___s_Image_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Image_4), value);
	}

	inline static int32_t get_offset_of_s_TextImage_5() { return static_cast<int32_t>(offsetof(GUIContent_t1852996497_StaticFields, ___s_TextImage_5)); }
	inline GUIContent_t1852996497 * get_s_TextImage_5() const { return ___s_TextImage_5; }
	inline GUIContent_t1852996497 ** get_address_of_s_TextImage_5() { return &___s_TextImage_5; }
	inline void set_s_TextImage_5(GUIContent_t1852996497 * value)
	{
		___s_TextImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_TextImage_5), value);
	}

	inline static int32_t get_offset_of_none_6() { return static_cast<int32_t>(offsetof(GUIContent_t1852996497_StaticFields, ___none_6)); }
	inline GUIContent_t1852996497 * get_none_6() const { return ___none_6; }
	inline GUIContent_t1852996497 ** get_address_of_none_6() { return &___none_6; }
	inline void set_none_6(GUIContent_t1852996497 * value)
	{
		___none_6 = value;
		Il2CppCodeGenWriteBarrier((&___none_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIContent
struct GUIContent_t1852996497_marshaled_pinvoke
{
	char* ___m_Text_0;
	Texture_t4046637001 * ___m_Image_1;
	char* ___m_Tooltip_2;
};
// Native definition for COM marshalling of UnityEngine.GUIContent
struct GUIContent_t1852996497_marshaled_com
{
	Il2CppChar* ___m_Text_0;
	Texture_t4046637001 * ___m_Image_1;
	Il2CppChar* ___m_Tooltip_2;
};
#endif // GUICONTENT_T1852996497_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3669675803* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3669675803* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3669675803** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3669675803* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef CRITICALFINALIZEROBJECT_T1170206086_H
#define CRITICALFINALIZEROBJECT_T1170206086_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.ConstrainedExecution.CriticalFinalizerObject
struct  CriticalFinalizerObject_t1170206086  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CRITICALFINALIZEROBJECT_T1170206086_H
#ifndef MARSHALBYREFOBJECT_T4073014582_H
#define MARSHALBYREFOBJECT_T4073014582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_t4073014582  : public RuntimeObject
{
public:
	// System.Runtime.Remoting.ServerIdentity System.MarshalByRefObject::_identity
	ServerIdentity_t141605491 * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_t4073014582, ____identity_0)); }
	inline ServerIdentity_t141605491 * get__identity_0() const { return ____identity_0; }
	inline ServerIdentity_t141605491 ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(ServerIdentity_t141605491 * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MARSHALBYREFOBJECT_T4073014582_H
#ifndef SYSTEMINFO_T489585619_H
#define SYSTEMINFO_T489585619_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.SystemInfo
struct  SystemInfo_t489585619  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMINFO_T489585619_H
#ifndef EVENTARGS_T3852870227_H
#define EVENTARGS_T3852870227_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t3852870227  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t3852870227_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t3852870227 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t3852870227_StaticFields, ___Empty_0)); }
	inline EventArgs_t3852870227 * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t3852870227 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t3852870227 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T3852870227_H
#ifndef ENUM_T1912704450_H
#define ENUM_T1912704450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t1912704450  : public ValueType_t2697047229
{
public:

public:
};

struct Enum_t1912704450_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3669675803* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t1912704450_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3669675803* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3669675803** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3669675803* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t1912704450_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t1912704450_marshaled_com
{
};
#endif // ENUM_T1912704450_H
#ifndef WAITFORENDOFFRAME_T1533502071_H
#define WAITFORENDOFFRAME_T1533502071_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForEndOfFrame
struct  WaitForEndOfFrame_t1533502071  : public YieldInstruction_t1400753137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORENDOFFRAME_T1533502071_H
#ifndef VECTOR3_T2825674791_H
#define VECTOR3_T2825674791_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t2825674791 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t2825674791, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t2825674791, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t2825674791, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t2825674791_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t2825674791  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t2825674791  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t2825674791  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t2825674791  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t2825674791  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t2825674791  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t2825674791  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t2825674791  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t2825674791  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t2825674791  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___zeroVector_4)); }
	inline Vector3_t2825674791  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t2825674791 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t2825674791  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___oneVector_5)); }
	inline Vector3_t2825674791  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t2825674791 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t2825674791  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___upVector_6)); }
	inline Vector3_t2825674791  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t2825674791 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t2825674791  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___downVector_7)); }
	inline Vector3_t2825674791  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t2825674791 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t2825674791  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___leftVector_8)); }
	inline Vector3_t2825674791  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t2825674791 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t2825674791  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___rightVector_9)); }
	inline Vector3_t2825674791  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t2825674791 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t2825674791  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___forwardVector_10)); }
	inline Vector3_t2825674791  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t2825674791 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t2825674791  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___backVector_11)); }
	inline Vector3_t2825674791  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t2825674791 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t2825674791  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t2825674791  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t2825674791 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t2825674791  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t2825674791_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t2825674791  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t2825674791 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t2825674791  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T2825674791_H
#ifndef QUATERNION_T580650070_H
#define QUATERNION_T580650070_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t580650070 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t580650070, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t580650070, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t580650070, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t580650070, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t580650070_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t580650070  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t580650070_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t580650070  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t580650070 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t580650070  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T580650070_H
#ifndef MATRIX4X4_T3574440610_H
#define MATRIX4X4_T3574440610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t3574440610 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t3574440610_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t3574440610  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t3574440610  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t3574440610  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t3574440610 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t3574440610  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t3574440610_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t3574440610  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t3574440610 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t3574440610  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T3574440610_H
#ifndef WORKREQUEST_T2264216812_H
#define WORKREQUEST_T2264216812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnitySynchronizationContext/WorkRequest
struct  WorkRequest_t2264216812 
{
public:
	// System.Threading.SendOrPostCallback UnityEngine.UnitySynchronizationContext/WorkRequest::m_DelagateCallback
	SendOrPostCallback_t3197245846 * ___m_DelagateCallback_0;
	// System.Object UnityEngine.UnitySynchronizationContext/WorkRequest::m_DelagateState
	RuntimeObject * ___m_DelagateState_1;
	// System.Threading.ManualResetEvent UnityEngine.UnitySynchronizationContext/WorkRequest::m_WaitHandle
	ManualResetEvent_t562888503 * ___m_WaitHandle_2;

public:
	inline static int32_t get_offset_of_m_DelagateCallback_0() { return static_cast<int32_t>(offsetof(WorkRequest_t2264216812, ___m_DelagateCallback_0)); }
	inline SendOrPostCallback_t3197245846 * get_m_DelagateCallback_0() const { return ___m_DelagateCallback_0; }
	inline SendOrPostCallback_t3197245846 ** get_address_of_m_DelagateCallback_0() { return &___m_DelagateCallback_0; }
	inline void set_m_DelagateCallback_0(SendOrPostCallback_t3197245846 * value)
	{
		___m_DelagateCallback_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_DelagateCallback_0), value);
	}

	inline static int32_t get_offset_of_m_DelagateState_1() { return static_cast<int32_t>(offsetof(WorkRequest_t2264216812, ___m_DelagateState_1)); }
	inline RuntimeObject * get_m_DelagateState_1() const { return ___m_DelagateState_1; }
	inline RuntimeObject ** get_address_of_m_DelagateState_1() { return &___m_DelagateState_1; }
	inline void set_m_DelagateState_1(RuntimeObject * value)
	{
		___m_DelagateState_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_DelagateState_1), value);
	}

	inline static int32_t get_offset_of_m_WaitHandle_2() { return static_cast<int32_t>(offsetof(WorkRequest_t2264216812, ___m_WaitHandle_2)); }
	inline ManualResetEvent_t562888503 * get_m_WaitHandle_2() const { return ___m_WaitHandle_2; }
	inline ManualResetEvent_t562888503 ** get_address_of_m_WaitHandle_2() { return &___m_WaitHandle_2; }
	inline void set_m_WaitHandle_2(ManualResetEvent_t562888503 * value)
	{
		___m_WaitHandle_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_WaitHandle_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.UnitySynchronizationContext/WorkRequest
struct WorkRequest_t2264216812_marshaled_pinvoke
{
	Il2CppMethodPointer ___m_DelagateCallback_0;
	Il2CppIUnknown* ___m_DelagateState_1;
	ManualResetEvent_t562888503 * ___m_WaitHandle_2;
};
// Native definition for COM marshalling of UnityEngine.UnitySynchronizationContext/WorkRequest
struct WorkRequest_t2264216812_marshaled_com
{
	Il2CppMethodPointer ___m_DelagateCallback_0;
	Il2CppIUnknown* ___m_DelagateState_1;
	ManualResetEvent_t562888503 * ___m_WaitHandle_2;
};
#endif // WORKREQUEST_T2264216812_H
#ifndef UNITYSYNCHRONIZATIONCONTEXT_T3253602121_H
#define UNITYSYNCHRONIZATIONCONTEXT_T3253602121_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnitySynchronizationContext
struct  UnitySynchronizationContext_t3253602121  : public SynchronizationContext_t155019481
{
public:
	// System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest> UnityEngine.UnitySynchronizationContext::m_AsyncWorkQueue
	Queue_1_t2108291272 * ___m_AsyncWorkQueue_1;
	// System.Int32 UnityEngine.UnitySynchronizationContext::m_MainThreadID
	int32_t ___m_MainThreadID_2;

public:
	inline static int32_t get_offset_of_m_AsyncWorkQueue_1() { return static_cast<int32_t>(offsetof(UnitySynchronizationContext_t3253602121, ___m_AsyncWorkQueue_1)); }
	inline Queue_1_t2108291272 * get_m_AsyncWorkQueue_1() const { return ___m_AsyncWorkQueue_1; }
	inline Queue_1_t2108291272 ** get_address_of_m_AsyncWorkQueue_1() { return &___m_AsyncWorkQueue_1; }
	inline void set_m_AsyncWorkQueue_1(Queue_1_t2108291272 * value)
	{
		___m_AsyncWorkQueue_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_AsyncWorkQueue_1), value);
	}

	inline static int32_t get_offset_of_m_MainThreadID_2() { return static_cast<int32_t>(offsetof(UnitySynchronizationContext_t3253602121, ___m_MainThreadID_2)); }
	inline int32_t get_m_MainThreadID_2() const { return ___m_MainThreadID_2; }
	inline int32_t* get_address_of_m_MainThreadID_2() { return &___m_MainThreadID_2; }
	inline void set_m_MainThreadID_2(int32_t value)
	{
		___m_MainThreadID_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYSYNCHRONIZATIONCONTEXT_T3253602121_H
#ifndef METHODBASE_T4283069302_H
#define METHODBASE_T4283069302_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodBase
struct  MethodBase_t4283069302  : public MemberInfo_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODBASE_T4283069302_H
#ifndef INT32_T4116043316_H
#define INT32_T4116043316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t4116043316 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t4116043316, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T4116043316_H
#ifndef UILINEINFO_T4132474705_H
#define UILINEINFO_T4132474705_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UILineInfo
struct  UILineInfo_t4132474705 
{
public:
	// System.Int32 UnityEngine.UILineInfo::startCharIdx
	int32_t ___startCharIdx_0;
	// System.Int32 UnityEngine.UILineInfo::height
	int32_t ___height_1;
	// System.Single UnityEngine.UILineInfo::topY
	float ___topY_2;
	// System.Single UnityEngine.UILineInfo::leading
	float ___leading_3;

public:
	inline static int32_t get_offset_of_startCharIdx_0() { return static_cast<int32_t>(offsetof(UILineInfo_t4132474705, ___startCharIdx_0)); }
	inline int32_t get_startCharIdx_0() const { return ___startCharIdx_0; }
	inline int32_t* get_address_of_startCharIdx_0() { return &___startCharIdx_0; }
	inline void set_startCharIdx_0(int32_t value)
	{
		___startCharIdx_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(UILineInfo_t4132474705, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}

	inline static int32_t get_offset_of_topY_2() { return static_cast<int32_t>(offsetof(UILineInfo_t4132474705, ___topY_2)); }
	inline float get_topY_2() const { return ___topY_2; }
	inline float* get_address_of_topY_2() { return &___topY_2; }
	inline void set_topY_2(float value)
	{
		___topY_2 = value;
	}

	inline static int32_t get_offset_of_leading_3() { return static_cast<int32_t>(offsetof(UILineInfo_t4132474705, ___leading_3)); }
	inline float get_leading_3() const { return ___leading_3; }
	inline float* get_address_of_leading_3() { return &___leading_3; }
	inline void set_leading_3(float value)
	{
		___leading_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UILINEINFO_T4132474705_H
#ifndef ANIMATORSTATEINFO_T4150098719_H
#define ANIMATORSTATEINFO_T4150098719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_t4150098719 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4150098719, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4150098719, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4150098719, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4150098719, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4150098719, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4150098719, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4150098719, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4150098719, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t4150098719, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORSTATEINFO_T4150098719_H
#ifndef CHAR_T3956936558_H
#define CHAR_T3956936558_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3956936558 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Char_t3956936558, ___m_value_2)); }
	inline Il2CppChar get_m_value_2() const { return ___m_value_2; }
	inline Il2CppChar* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(Il2CppChar value)
	{
		___m_value_2 = value;
	}
};

struct Char_t3956936558_StaticFields
{
public:
	// System.Byte* System.Char::category_data
	uint8_t* ___category_data_3;
	// System.Byte* System.Char::numeric_data
	uint8_t* ___numeric_data_4;
	// System.Double* System.Char::numeric_data_values
	double* ___numeric_data_values_5;
	// System.UInt16* System.Char::to_lower_data_low
	uint16_t* ___to_lower_data_low_6;
	// System.UInt16* System.Char::to_lower_data_high
	uint16_t* ___to_lower_data_high_7;
	// System.UInt16* System.Char::to_upper_data_low
	uint16_t* ___to_upper_data_low_8;
	// System.UInt16* System.Char::to_upper_data_high
	uint16_t* ___to_upper_data_high_9;

public:
	inline static int32_t get_offset_of_category_data_3() { return static_cast<int32_t>(offsetof(Char_t3956936558_StaticFields, ___category_data_3)); }
	inline uint8_t* get_category_data_3() const { return ___category_data_3; }
	inline uint8_t** get_address_of_category_data_3() { return &___category_data_3; }
	inline void set_category_data_3(uint8_t* value)
	{
		___category_data_3 = value;
	}

	inline static int32_t get_offset_of_numeric_data_4() { return static_cast<int32_t>(offsetof(Char_t3956936558_StaticFields, ___numeric_data_4)); }
	inline uint8_t* get_numeric_data_4() const { return ___numeric_data_4; }
	inline uint8_t** get_address_of_numeric_data_4() { return &___numeric_data_4; }
	inline void set_numeric_data_4(uint8_t* value)
	{
		___numeric_data_4 = value;
	}

	inline static int32_t get_offset_of_numeric_data_values_5() { return static_cast<int32_t>(offsetof(Char_t3956936558_StaticFields, ___numeric_data_values_5)); }
	inline double* get_numeric_data_values_5() const { return ___numeric_data_values_5; }
	inline double** get_address_of_numeric_data_values_5() { return &___numeric_data_values_5; }
	inline void set_numeric_data_values_5(double* value)
	{
		___numeric_data_values_5 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_low_6() { return static_cast<int32_t>(offsetof(Char_t3956936558_StaticFields, ___to_lower_data_low_6)); }
	inline uint16_t* get_to_lower_data_low_6() const { return ___to_lower_data_low_6; }
	inline uint16_t** get_address_of_to_lower_data_low_6() { return &___to_lower_data_low_6; }
	inline void set_to_lower_data_low_6(uint16_t* value)
	{
		___to_lower_data_low_6 = value;
	}

	inline static int32_t get_offset_of_to_lower_data_high_7() { return static_cast<int32_t>(offsetof(Char_t3956936558_StaticFields, ___to_lower_data_high_7)); }
	inline uint16_t* get_to_lower_data_high_7() const { return ___to_lower_data_high_7; }
	inline uint16_t** get_address_of_to_lower_data_high_7() { return &___to_lower_data_high_7; }
	inline void set_to_lower_data_high_7(uint16_t* value)
	{
		___to_lower_data_high_7 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_low_8() { return static_cast<int32_t>(offsetof(Char_t3956936558_StaticFields, ___to_upper_data_low_8)); }
	inline uint16_t* get_to_upper_data_low_8() const { return ___to_upper_data_low_8; }
	inline uint16_t** get_address_of_to_upper_data_low_8() { return &___to_upper_data_low_8; }
	inline void set_to_upper_data_low_8(uint16_t* value)
	{
		___to_upper_data_low_8 = value;
	}

	inline static int32_t get_offset_of_to_upper_data_high_9() { return static_cast<int32_t>(offsetof(Char_t3956936558_StaticFields, ___to_upper_data_high_9)); }
	inline uint16_t* get_to_upper_data_high_9() const { return ___to_upper_data_high_9; }
	inline uint16_t** get_address_of_to_upper_data_high_9() { return &___to_upper_data_high_9; }
	inline void set_to_upper_data_high_9(uint16_t* value)
	{
		___to_upper_data_high_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3956936558_H
#ifndef UINTPTR_T_H
#define UINTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UIntPtr
struct  UIntPtr_t 
{
public:
	// System.Void* System.UIntPtr::_pointer
	void* ____pointer_1;

public:
	inline static int32_t get_offset_of__pointer_1() { return static_cast<int32_t>(offsetof(UIntPtr_t, ____pointer_1)); }
	inline void* get__pointer_1() const { return ____pointer_1; }
	inline void** get_address_of__pointer_1() { return &____pointer_1; }
	inline void set__pointer_1(void* value)
	{
		____pointer_1 = value;
	}
};

struct UIntPtr_t_StaticFields
{
public:
	// System.UIntPtr System.UIntPtr::Zero
	UIntPtr_t  ___Zero_0;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(UIntPtr_t_StaticFields, ___Zero_0)); }
	inline UIntPtr_t  get_Zero_0() const { return ___Zero_0; }
	inline UIntPtr_t * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(UIntPtr_t  value)
	{
		___Zero_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINTPTR_T_H
#ifndef COLOR32_T2411287715_H
#define COLOR32_T2411287715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct ALIGN_TYPE(4) Color32_t2411287715 
{
public:
	// System.Byte UnityEngine.Color32::r
	uint8_t ___r_0;
	// System.Byte UnityEngine.Color32::g
	uint8_t ___g_1;
	// System.Byte UnityEngine.Color32::b
	uint8_t ___b_2;
	// System.Byte UnityEngine.Color32::a
	uint8_t ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color32_t2411287715, ___r_0)); }
	inline uint8_t get_r_0() const { return ___r_0; }
	inline uint8_t* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(uint8_t value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color32_t2411287715, ___g_1)); }
	inline uint8_t get_g_1() const { return ___g_1; }
	inline uint8_t* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(uint8_t value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color32_t2411287715, ___b_2)); }
	inline uint8_t get_b_2() const { return ___b_2; }
	inline uint8_t* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(uint8_t value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color32_t2411287715, ___a_3)); }
	inline uint8_t get_a_3() const { return ___a_3; }
	inline uint8_t* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(uint8_t value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2411287715_H
#ifndef VECTOR4_T2651204353_H
#define VECTOR4_T2651204353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector4
struct  Vector4_t2651204353 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_t2651204353, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_t2651204353, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_t2651204353, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_t2651204353, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_t2651204353_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_t2651204353  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_t2651204353  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_t2651204353  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_t2651204353  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_t2651204353_StaticFields, ___zeroVector_5)); }
	inline Vector4_t2651204353  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_t2651204353 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_t2651204353  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_t2651204353_StaticFields, ___oneVector_6)); }
	inline Vector4_t2651204353  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_t2651204353 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_t2651204353  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_t2651204353_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_t2651204353  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_t2651204353 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_t2651204353  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_t2651204353_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_t2651204353  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_t2651204353 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_t2651204353  value)
	{
		___negativeInfinityVector_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR4_T2651204353_H
#ifndef UNITYLOGWRITER_T646926076_H
#define UNITYLOGWRITER_T646926076_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnityLogWriter
struct  UnityLogWriter_t646926076  : public TextWriter_t2423646586
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYLOGWRITER_T646926076_H
#ifndef VOID_T3157035008_H
#define VOID_T3157035008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t3157035008 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T3157035008_H
#ifndef UNHANDLEDEXCEPTIONEVENTARGS_T2750508014_H
#define UNHANDLEDEXCEPTIONEVENTARGS_T2750508014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UnhandledExceptionEventArgs
struct  UnhandledExceptionEventArgs_t2750508014  : public EventArgs_t3852870227
{
public:
	// System.Object System.UnhandledExceptionEventArgs::exception
	RuntimeObject * ___exception_1;
	// System.Boolean System.UnhandledExceptionEventArgs::m_isTerminating
	bool ___m_isTerminating_2;

public:
	inline static int32_t get_offset_of_exception_1() { return static_cast<int32_t>(offsetof(UnhandledExceptionEventArgs_t2750508014, ___exception_1)); }
	inline RuntimeObject * get_exception_1() const { return ___exception_1; }
	inline RuntimeObject ** get_address_of_exception_1() { return &___exception_1; }
	inline void set_exception_1(RuntimeObject * value)
	{
		___exception_1 = value;
		Il2CppCodeGenWriteBarrier((&___exception_1), value);
	}

	inline static int32_t get_offset_of_m_isTerminating_2() { return static_cast<int32_t>(offsetof(UnhandledExceptionEventArgs_t2750508014, ___m_isTerminating_2)); }
	inline bool get_m_isTerminating_2() const { return ___m_isTerminating_2; }
	inline bool* get_address_of_m_isTerminating_2() { return &___m_isTerminating_2; }
	inline void set_m_isTerminating_2(bool value)
	{
		___m_isTerminating_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNHANDLEDEXCEPTIONEVENTARGS_T2750508014_H
#ifndef SYSTEMEXCEPTION_T2449741166_H
#define SYSTEMEXCEPTION_T2449741166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t2449741166  : public Exception_t2572292308
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T2449741166_H
#ifndef RANGEINT_T4007359427_H
#define RANGEINT_T4007359427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RangeInt
struct  RangeInt_t4007359427 
{
public:
	// System.Int32 UnityEngine.RangeInt::start
	int32_t ___start_0;
	// System.Int32 UnityEngine.RangeInt::length
	int32_t ___length_1;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(RangeInt_t4007359427, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_length_1() { return static_cast<int32_t>(offsetof(RangeInt_t4007359427, ___length_1)); }
	inline int32_t get_length_1() const { return ___length_1; }
	inline int32_t* get_address_of_length_1() { return &___length_1; }
	inline void set_length_1(int32_t value)
	{
		___length_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEINT_T4007359427_H
#ifndef WAITFORFIXEDUPDATE_T3543075621_H
#define WAITFORFIXEDUPDATE_T3543075621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForFixedUpdate
struct  WaitForFixedUpdate_t3543075621  : public YieldInstruction_t1400753137
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORFIXEDUPDATE_T3543075621_H
#ifndef TOUCHSCREENKEYBOARD_INTERNALCONSTRUCTORHELPERARGUMENTS_T857896789_H
#define TOUCHSCREENKEYBOARD_INTERNALCONSTRUCTORHELPERARGUMENTS_T857896789_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments
struct  TouchScreenKeyboard_InternalConstructorHelperArguments_t857896789 
{
public:
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::keyboardType
	uint32_t ___keyboardType_0;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::autocorrection
	uint32_t ___autocorrection_1;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::multiline
	uint32_t ___multiline_2;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::secure
	uint32_t ___secure_3;
	// System.UInt32 UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments::alert
	uint32_t ___alert_4;

public:
	inline static int32_t get_offset_of_keyboardType_0() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t857896789, ___keyboardType_0)); }
	inline uint32_t get_keyboardType_0() const { return ___keyboardType_0; }
	inline uint32_t* get_address_of_keyboardType_0() { return &___keyboardType_0; }
	inline void set_keyboardType_0(uint32_t value)
	{
		___keyboardType_0 = value;
	}

	inline static int32_t get_offset_of_autocorrection_1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t857896789, ___autocorrection_1)); }
	inline uint32_t get_autocorrection_1() const { return ___autocorrection_1; }
	inline uint32_t* get_address_of_autocorrection_1() { return &___autocorrection_1; }
	inline void set_autocorrection_1(uint32_t value)
	{
		___autocorrection_1 = value;
	}

	inline static int32_t get_offset_of_multiline_2() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t857896789, ___multiline_2)); }
	inline uint32_t get_multiline_2() const { return ___multiline_2; }
	inline uint32_t* get_address_of_multiline_2() { return &___multiline_2; }
	inline void set_multiline_2(uint32_t value)
	{
		___multiline_2 = value;
	}

	inline static int32_t get_offset_of_secure_3() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t857896789, ___secure_3)); }
	inline uint32_t get_secure_3() const { return ___secure_3; }
	inline uint32_t* get_address_of_secure_3() { return &___secure_3; }
	inline void set_secure_3(uint32_t value)
	{
		___secure_3 = value;
	}

	inline static int32_t get_offset_of_alert_4() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_InternalConstructorHelperArguments_t857896789, ___alert_4)); }
	inline uint32_t get_alert_4() const { return ___alert_4; }
	inline uint32_t* get_address_of_alert_4() { return &___alert_4; }
	inline void set_alert_4(uint32_t value)
	{
		___alert_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARD_INTERNALCONSTRUCTORHELPERARGUMENTS_T857896789_H
#ifndef UNITYEXCEPTION_T1120912047_H
#define UNITYEXCEPTION_T1120912047_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UnityException
struct  UnityException_t1120912047  : public Exception_t2572292308
{
public:
	// System.String UnityEngine.UnityException::unityStackTrace
	String_t* ___unityStackTrace_12;

public:
	inline static int32_t get_offset_of_unityStackTrace_12() { return static_cast<int32_t>(offsetof(UnityException_t1120912047, ___unityStackTrace_12)); }
	inline String_t* get_unityStackTrace_12() const { return ___unityStackTrace_12; }
	inline String_t** get_address_of_unityStackTrace_12() { return &___unityStackTrace_12; }
	inline void set_unityStackTrace_12(String_t* value)
	{
		___unityStackTrace_12 = value;
		Il2CppCodeGenWriteBarrier((&___unityStackTrace_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEXCEPTION_T1120912047_H
#ifndef RECT_T82402607_H
#define RECT_T82402607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t82402607 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t82402607, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t82402607, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t82402607, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t82402607, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T82402607_H
#ifndef TYPEINFERENCERULEATTRIBUTE_T1740219836_H
#define TYPEINFERENCERULEATTRIBUTE_T1740219836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.TypeInferenceRuleAttribute
struct  TypeInferenceRuleAttribute_t1740219836  : public Attribute_t2169343654
{
public:
	// System.String UnityEngineInternal.TypeInferenceRuleAttribute::_rule
	String_t* ____rule_0;

public:
	inline static int32_t get_offset_of__rule_0() { return static_cast<int32_t>(offsetof(TypeInferenceRuleAttribute_t1740219836, ____rule_0)); }
	inline String_t* get__rule_0() const { return ____rule_0; }
	inline String_t** get_address_of__rule_0() { return &____rule_0; }
	inline void set__rule_0(String_t* value)
	{
		____rule_0 = value;
		Il2CppCodeGenWriteBarrier((&____rule_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFERENCERULEATTRIBUTE_T1740219836_H
#ifndef UINT32_T1552934845_H
#define UINT32_T1552934845_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t1552934845 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t1552934845, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T1552934845_H
#ifndef WAITFORSECONDS_T4091331090_H
#define WAITFORSECONDS_T4091331090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t4091331090  : public YieldInstruction_t1400753137
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t4091331090, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t4091331090_marshaled_pinvoke : public YieldInstruction_t1400753137_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t4091331090_marshaled_com : public YieldInstruction_t1400753137_marshaled_com
{
	float ___m_Seconds_0;
};
#endif // WAITFORSECONDS_T4091331090_H
#ifndef SINGLE_T1534300504_H
#define SINGLE_T1534300504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1534300504 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1534300504, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1534300504_H
#ifndef BYTE_T1072372818_H
#define BYTE_T1072372818_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1072372818 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Byte_t1072372818, ___m_value_2)); }
	inline uint8_t get_m_value_2() const { return ___m_value_2; }
	inline uint8_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(uint8_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1072372818_H
#ifndef THREADANDSERIALIZATIONSAFEATTRIBUTE_T3127544772_H
#define THREADANDSERIALIZATIONSAFEATTRIBUTE_T3127544772_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ThreadAndSerializationSafeAttribute
struct  ThreadAndSerializationSafeAttribute_t3127544772  : public Attribute_t2169343654
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADANDSERIALIZATIONSAFEATTRIBUTE_T3127544772_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef GENERICSTACK_T1699001310_H
#define GENERICSTACK_T1699001310_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.GenericStack
struct  GenericStack_t1699001310  : public Stack_t1463244948
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICSTACK_T1699001310_H
#ifndef WRITABLEATTRIBUTE_T902445776_H
#define WRITABLEATTRIBUTE_T902445776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WritableAttribute
struct  WritableAttribute_t902445776  : public Attribute_t2169343654
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITABLEATTRIBUTE_T902445776_H
#ifndef COLOR_T4183816058_H
#define COLOR_T4183816058_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t4183816058 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t4183816058, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t4183816058, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t4183816058, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t4183816058, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T4183816058_H
#ifndef PROPERTYATTRIBUTE_T1226012101_H
#define PROPERTYATTRIBUTE_T1226012101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t1226012101  : public Attribute_t2169343654
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T1226012101_H
#ifndef WAITFORSECONDSREALTIME_T4106866811_H
#define WAITFORSECONDSREALTIME_T4106866811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForSecondsRealtime
struct  WaitForSecondsRealtime_t4106866811  : public CustomYieldInstruction_t1129172254
{
public:
	// System.Single UnityEngine.WaitForSecondsRealtime::waitTime
	float ___waitTime_0;

public:
	inline static int32_t get_offset_of_waitTime_0() { return static_cast<int32_t>(offsetof(WaitForSecondsRealtime_t4106866811, ___waitTime_0)); }
	inline float get_waitTime_0() const { return ___waitTime_0; }
	inline float* get_address_of_waitTime_0() { return &___waitTime_0; }
	inline void set_waitTime_0(float value)
	{
		___waitTime_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSECONDSREALTIME_T4106866811_H
#ifndef BOOLEAN_T1039239260_H
#define BOOLEAN_T1039239260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t1039239260 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t1039239260, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t1039239260_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t1039239260_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t1039239260_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T1039239260_H
#ifndef VECTOR2_T1065050092_H
#define VECTOR2_T1065050092_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t1065050092 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t1065050092, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t1065050092, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t1065050092_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t1065050092  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t1065050092  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t1065050092  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t1065050092  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t1065050092  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t1065050092  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t1065050092  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t1065050092  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___zeroVector_2)); }
	inline Vector2_t1065050092  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t1065050092 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t1065050092  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___oneVector_3)); }
	inline Vector2_t1065050092  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t1065050092 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t1065050092  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___upVector_4)); }
	inline Vector2_t1065050092  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t1065050092 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t1065050092  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___downVector_5)); }
	inline Vector2_t1065050092  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t1065050092 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t1065050092  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___leftVector_6)); }
	inline Vector2_t1065050092  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t1065050092 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t1065050092  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___rightVector_7)); }
	inline Vector2_t1065050092  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t1065050092 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t1065050092  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t1065050092  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t1065050092 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t1065050092  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t1065050092_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t1065050092  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t1065050092 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t1065050092  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T1065050092_H
#ifndef RECTOFFSET_T2305860064_H
#define RECTOFFSET_T2305860064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_t2305860064  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	IntPtr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_t2305860064, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_t2305860064, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t2305860064_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t2305860064_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_T2305860064_H
#ifndef TYPEINFERENCERULES_T2608634147_H
#define TYPEINFERENCERULES_T2608634147_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.TypeInferenceRules
struct  TypeInferenceRules_t2608634147 
{
public:
	// System.Int32 UnityEngineInternal.TypeInferenceRules::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TypeInferenceRules_t2608634147, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEINFERENCERULES_T2608634147_H
#ifndef URIKIND_T23954514_H
#define URIKIND_T23954514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriKind
struct  UriKind_t23954514 
{
public:
	// System.Int32 System.UriKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(UriKind_t23954514, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIKIND_T23954514_H
#ifndef WAITHANDLE_T3740277255_H
#define WAITHANDLE_T3740277255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.WaitHandle
struct  WaitHandle_t3740277255  : public MarshalByRefObject_t4073014582
{
public:
	// Microsoft.Win32.SafeHandles.SafeWaitHandle System.Threading.WaitHandle::safe_wait_handle
	SafeWaitHandle_t3914948630 * ___safe_wait_handle_2;
	// System.Boolean System.Threading.WaitHandle::disposed
	bool ___disposed_4;

public:
	inline static int32_t get_offset_of_safe_wait_handle_2() { return static_cast<int32_t>(offsetof(WaitHandle_t3740277255, ___safe_wait_handle_2)); }
	inline SafeWaitHandle_t3914948630 * get_safe_wait_handle_2() const { return ___safe_wait_handle_2; }
	inline SafeWaitHandle_t3914948630 ** get_address_of_safe_wait_handle_2() { return &___safe_wait_handle_2; }
	inline void set_safe_wait_handle_2(SafeWaitHandle_t3914948630 * value)
	{
		___safe_wait_handle_2 = value;
		Il2CppCodeGenWriteBarrier((&___safe_wait_handle_2), value);
	}

	inline static int32_t get_offset_of_disposed_4() { return static_cast<int32_t>(offsetof(WaitHandle_t3740277255, ___disposed_4)); }
	inline bool get_disposed_4() const { return ___disposed_4; }
	inline bool* get_address_of_disposed_4() { return &___disposed_4; }
	inline void set_disposed_4(bool value)
	{
		___disposed_4 = value;
	}
};

struct WaitHandle_t3740277255_StaticFields
{
public:
	// System.IntPtr System.Threading.WaitHandle::InvalidHandle
	IntPtr_t ___InvalidHandle_3;

public:
	inline static int32_t get_offset_of_InvalidHandle_3() { return static_cast<int32_t>(offsetof(WaitHandle_t3740277255_StaticFields, ___InvalidHandle_3)); }
	inline IntPtr_t get_InvalidHandle_3() const { return ___InvalidHandle_3; }
	inline IntPtr_t* get_address_of_InvalidHandle_3() { return &___InvalidHandle_3; }
	inline void set_InvalidHandle_3(IntPtr_t value)
	{
		___InvalidHandle_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITHANDLE_T3740277255_H
#ifndef PLAYABLEHANDLE_T3645500743_H
#define PLAYABLEHANDLE_T3645500743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Playables.PlayableHandle
struct  PlayableHandle_t3645500743 
{
public:
	// System.IntPtr UnityEngine.Playables.PlayableHandle::m_Handle
	IntPtr_t ___m_Handle_0;
	// System.Int32 UnityEngine.Playables.PlayableHandle::m_Version
	int32_t ___m_Version_1;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(PlayableHandle_t3645500743, ___m_Handle_0)); }
	inline IntPtr_t get_m_Handle_0() const { return ___m_Handle_0; }
	inline IntPtr_t* get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(IntPtr_t value)
	{
		___m_Handle_0 = value;
	}

	inline static int32_t get_offset_of_m_Version_1() { return static_cast<int32_t>(offsetof(PlayableHandle_t3645500743, ___m_Version_1)); }
	inline int32_t get_m_Version_1() const { return ___m_Version_1; }
	inline int32_t* get_address_of_m_Version_1() { return &___m_Version_1; }
	inline void set_m_Version_1(int32_t value)
	{
		___m_Version_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYABLEHANDLE_T3645500743_H
#ifndef DELEGATE_T1310841479_H
#define DELEGATE_T1310841479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1310841479  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t878649875 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___data_8)); }
	inline DelegateData_t878649875 * get_data_8() const { return ___data_8; }
	inline DelegateData_t878649875 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t878649875 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1310841479_H
#ifndef THREADSTATE_T1187635506_H
#define THREADSTATE_T1187635506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ThreadState
struct  ThreadState_t1187635506 
{
public:
	// System.Int32 System.Threading.ThreadState::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ThreadState_t1187635506, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREADSTATE_T1187635506_H
#ifndef MATHFINTERNAL_T2540608617_H
#define MATHFINTERNAL_T2540608617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngineInternal.MathfInternal
struct  MathfInternal_t2540608617 
{
public:
	union
	{
		struct
		{
		};
		uint8_t MathfInternal_t2540608617__padding[1];
	};

public:
};

struct MathfInternal_t2540608617_StaticFields
{
public:
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngineInternal.MathfInternal::FloatMinNormal
	float ___FloatMinNormal_0;
	// System.Single modreq(System.Runtime.CompilerServices.IsVolatile) UnityEngineInternal.MathfInternal::FloatMinDenormal
	float ___FloatMinDenormal_1;
	// System.Boolean UnityEngineInternal.MathfInternal::IsFlushToZeroEnabled
	bool ___IsFlushToZeroEnabled_2;

public:
	inline static int32_t get_offset_of_FloatMinNormal_0() { return static_cast<int32_t>(offsetof(MathfInternal_t2540608617_StaticFields, ___FloatMinNormal_0)); }
	inline float get_FloatMinNormal_0() const { return ___FloatMinNormal_0; }
	inline float* get_address_of_FloatMinNormal_0() { return &___FloatMinNormal_0; }
	inline void set_FloatMinNormal_0(float value)
	{
		___FloatMinNormal_0 = value;
	}

	inline static int32_t get_offset_of_FloatMinDenormal_1() { return static_cast<int32_t>(offsetof(MathfInternal_t2540608617_StaticFields, ___FloatMinDenormal_1)); }
	inline float get_FloatMinDenormal_1() const { return ___FloatMinDenormal_1; }
	inline float* get_address_of_FloatMinDenormal_1() { return &___FloatMinDenormal_1; }
	inline void set_FloatMinDenormal_1(float value)
	{
		___FloatMinDenormal_1 = value;
	}

	inline static int32_t get_offset_of_IsFlushToZeroEnabled_2() { return static_cast<int32_t>(offsetof(MathfInternal_t2540608617_StaticFields, ___IsFlushToZeroEnabled_2)); }
	inline bool get_IsFlushToZeroEnabled_2() const { return ___IsFlushToZeroEnabled_2; }
	inline bool* get_address_of_IsFlushToZeroEnabled_2() { return &___IsFlushToZeroEnabled_2; }
	inline void set_IsFlushToZeroEnabled_2(bool value)
	{
		___IsFlushToZeroEnabled_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHFINTERNAL_T2540608617_H
#ifndef STREAMINGCONTEXTSTATES_T574785530_H
#define STREAMINGCONTEXTSTATES_T574785530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t574785530 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(StreamingContextStates_t574785530, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T574785530_H
#ifndef INDEXOUTOFRANGEEXCEPTION_T481375535_H
#define INDEXOUTOFRANGEEXCEPTION_T481375535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IndexOutOfRangeException
struct  IndexOutOfRangeException_t481375535  : public SystemException_t2449741166
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXOUTOFRANGEEXCEPTION_T481375535_H
#ifndef BINDINGFLAGS_T2456351567_H
#define BINDINGFLAGS_T2456351567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2456351567 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(BindingFlags_t2456351567, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2456351567_H
#ifndef RUNTIMETYPEHANDLE_T2719897271_H
#define RUNTIMETYPEHANDLE_T2719897271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t2719897271 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	IntPtr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t2719897271, ___value_0)); }
	inline IntPtr_t get_value_0() const { return ___value_0; }
	inline IntPtr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(IntPtr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T2719897271_H
#ifndef PRINCIPALPOLICY_T2161523496_H
#define PRINCIPALPOLICY_T2161523496_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Security.Principal.PrincipalPolicy
struct  PrincipalPolicy_t2161523496 
{
public:
	// System.Int32 System.Security.Principal.PrincipalPolicy::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(PrincipalPolicy_t2161523496, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRINCIPALPOLICY_T2161523496_H
#ifndef METHODINFO_T_H
#define METHODINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MethodInfo
struct  MethodInfo_t  : public MethodBase_t4283069302
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFO_T_H
#ifndef REGEXOPTIONS_T401094059_H
#define REGEXOPTIONS_T401094059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.RegexOptions
struct  RegexOptions_t401094059 
{
public:
	// System.Int32 System.Text.RegularExpressions.RegexOptions::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RegexOptions_t401094059, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXOPTIONS_T401094059_H
#ifndef TRACKEDREFERENCE_T4182582575_H
#define TRACKEDREFERENCE_T4182582575_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TrackedReference
struct  TrackedReference_t4182582575  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TrackedReference::m_Ptr
	IntPtr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TrackedReference_t4182582575, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TrackedReference
struct TrackedReference_t4182582575_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.TrackedReference
struct TrackedReference_t4182582575_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // TRACKEDREFERENCE_T4182582575_H
#ifndef HORIZONTALWRAPMODE_T1545581228_H
#define HORIZONTALWRAPMODE_T1545581228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HorizontalWrapMode
struct  HorizontalWrapMode_t1545581228 
{
public:
	// System.Int32 UnityEngine.HorizontalWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HorizontalWrapMode_t1545581228, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HORIZONTALWRAPMODE_T1545581228_H
#ifndef OBJECT_T350248726_H
#define OBJECT_T350248726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t350248726  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t350248726, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t350248726_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t350248726_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t350248726_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t350248726_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T350248726_H
#ifndef TEXTGENERATIONERROR_T1463722410_H
#define TEXTGENERATIONERROR_T1463722410_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextGenerationError
struct  TextGenerationError_t1463722410 
{
public:
	// System.Int32 UnityEngine.TextGenerationError::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextGenerationError_t1463722410, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTGENERATIONERROR_T1463722410_H
#ifndef TEXTUREWRAPMODE_T1628895098_H
#define TEXTUREWRAPMODE_T1628895098_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t1628895098 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrapMode_t1628895098, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T1628895098_H
#ifndef DBLCLICKSNAPPING_T2355467480_H
#define DBLCLICKSNAPPING_T2355467480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextEditor/DblClickSnapping
struct  DblClickSnapping_t2355467480 
{
public:
	// System.Byte UnityEngine.TextEditor/DblClickSnapping::value__
	uint8_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DblClickSnapping_t2355467480, ___value___1)); }
	inline uint8_t get_value___1() const { return ___value___1; }
	inline uint8_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(uint8_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DBLCLICKSNAPPING_T2355467480_H
#ifndef TEXTUREFORMAT_T2543749494_H
#define TEXTUREFORMAT_T2543749494_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2543749494 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureFormat_t2543749494, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2543749494_H
#ifndef TOOLTIPATTRIBUTE_T1130662838_H
#define TOOLTIPATTRIBUTE_T1130662838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TooltipAttribute
struct  TooltipAttribute_t1130662838  : public PropertyAttribute_t1226012101
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t1130662838, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((&___tooltip_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOOLTIPATTRIBUTE_T1130662838_H
#ifndef TOUCHPHASE_T3191674067_H
#define TOUCHPHASE_T3191674067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t3191674067 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchPhase_t3191674067, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T3191674067_H
#ifndef TOUCHTYPE_T67114849_H
#define TOUCHTYPE_T67114849_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t67114849 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchType_t67114849, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T67114849_H
#ifndef TOUCHSCREENKEYBOARDTYPE_T3909346333_H
#define TOUCHSCREENKEYBOARDTYPE_T3909346333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboardType
struct  TouchScreenKeyboardType_t3909346333 
{
public:
	// System.Int32 UnityEngine.TouchScreenKeyboardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TouchScreenKeyboardType_t3909346333, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARDTYPE_T3909346333_H
#ifndef RUNTIMEPLATFORM_T963209161_H
#define RUNTIMEPLATFORM_T963209161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RuntimePlatform
struct  RuntimePlatform_t963209161 
{
public:
	// System.Int32 UnityEngine.RuntimePlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RuntimePlatform_t963209161, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEPLATFORM_T963209161_H
#ifndef VERTICALWRAPMODE_T937939834_H
#define VERTICALWRAPMODE_T937939834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.VerticalWrapMode
struct  VerticalWrapMode_t937939834 
{
public:
	// System.Int32 UnityEngine.VerticalWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(VerticalWrapMode_t937939834, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTICALWRAPMODE_T937939834_H
#ifndef FONTSTYLE_T1504396548_H
#define FONTSTYLE_T1504396548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.FontStyle
struct  FontStyle_t1504396548 
{
public:
	// System.Int32 UnityEngine.FontStyle::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FontStyle_t1504396548, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONTSTYLE_T1504396548_H
#ifndef OPERATINGSYSTEMFAMILY_T1918812390_H
#define OPERATINGSYSTEMFAMILY_T1918812390_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.OperatingSystemFamily
struct  OperatingSystemFamily_t1918812390 
{
public:
	// System.Int32 UnityEngine.OperatingSystemFamily::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OperatingSystemFamily_t1918812390, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OPERATINGSYSTEMFAMILY_T1918812390_H
#ifndef TEXTCLIPPING_T1467294064_H
#define TEXTCLIPPING_T1467294064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextClipping
struct  TextClipping_t1467294064 
{
public:
	// System.Int32 UnityEngine.TextClipping::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextClipping_t1467294064, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTCLIPPING_T1467294064_H
#ifndef SPACE_T3856664523_H
#define SPACE_T3856664523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t3856664523 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Space_t3856664523, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T3856664523_H
#ifndef TEXTAREAATTRIBUTE_T1598070258_H
#define TEXTAREAATTRIBUTE_T1598070258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAreaAttribute
struct  TextAreaAttribute_t1598070258  : public PropertyAttribute_t1226012101
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t1598070258, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t1598070258, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTAREAATTRIBUTE_T1598070258_H
#ifndef TEXTANCHOR_T1106282459_H
#define TEXTANCHOR_T1106282459_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t1106282459 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextAnchor_t1106282459, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T1106282459_H
#ifndef TOUCHSCREENKEYBOARD_T1544112657_H
#define TOUCHSCREENKEYBOARD_T1544112657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchScreenKeyboard
struct  TouchScreenKeyboard_t1544112657  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TouchScreenKeyboard::m_Ptr
	IntPtr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TouchScreenKeyboard_t1544112657, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHSCREENKEYBOARD_T1544112657_H
#ifndef UICHARINFO_T3450488657_H
#define UICHARINFO_T3450488657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UICharInfo
struct  UICharInfo_t3450488657 
{
public:
	// UnityEngine.Vector2 UnityEngine.UICharInfo::cursorPos
	Vector2_t1065050092  ___cursorPos_0;
	// System.Single UnityEngine.UICharInfo::charWidth
	float ___charWidth_1;

public:
	inline static int32_t get_offset_of_cursorPos_0() { return static_cast<int32_t>(offsetof(UICharInfo_t3450488657, ___cursorPos_0)); }
	inline Vector2_t1065050092  get_cursorPos_0() const { return ___cursorPos_0; }
	inline Vector2_t1065050092 * get_address_of_cursorPos_0() { return &___cursorPos_0; }
	inline void set_cursorPos_0(Vector2_t1065050092  value)
	{
		___cursorPos_0 = value;
	}

	inline static int32_t get_offset_of_charWidth_1() { return static_cast<int32_t>(offsetof(UICharInfo_t3450488657, ___charWidth_1)); }
	inline float get_charWidth_1() const { return ___charWidth_1; }
	inline float* get_address_of_charWidth_1() { return &___charWidth_1; }
	inline void set_charWidth_1(float value)
	{
		___charWidth_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UICHARINFO_T3450488657_H
#ifndef SAMPLETYPE_T1356735607_H
#define SAMPLETYPE_T1356735607_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UISystemProfilerApi/SampleType
struct  SampleType_t1356735607 
{
public:
	// System.Int32 UnityEngine.UISystemProfilerApi/SampleType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SampleType_t1356735607, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SAMPLETYPE_T1356735607_H
#ifndef UIVERTEX_T475044788_H
#define UIVERTEX_T475044788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.UIVertex
struct  UIVertex_t475044788 
{
public:
	// UnityEngine.Vector3 UnityEngine.UIVertex::position
	Vector3_t2825674791  ___position_0;
	// UnityEngine.Vector3 UnityEngine.UIVertex::normal
	Vector3_t2825674791  ___normal_1;
	// UnityEngine.Color32 UnityEngine.UIVertex::color
	Color32_t2411287715  ___color_2;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv0
	Vector2_t1065050092  ___uv0_3;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv1
	Vector2_t1065050092  ___uv1_4;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv2
	Vector2_t1065050092  ___uv2_5;
	// UnityEngine.Vector2 UnityEngine.UIVertex::uv3
	Vector2_t1065050092  ___uv3_6;
	// UnityEngine.Vector4 UnityEngine.UIVertex::tangent
	Vector4_t2651204353  ___tangent_7;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(UIVertex_t475044788, ___position_0)); }
	inline Vector3_t2825674791  get_position_0() const { return ___position_0; }
	inline Vector3_t2825674791 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t2825674791  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_normal_1() { return static_cast<int32_t>(offsetof(UIVertex_t475044788, ___normal_1)); }
	inline Vector3_t2825674791  get_normal_1() const { return ___normal_1; }
	inline Vector3_t2825674791 * get_address_of_normal_1() { return &___normal_1; }
	inline void set_normal_1(Vector3_t2825674791  value)
	{
		___normal_1 = value;
	}

	inline static int32_t get_offset_of_color_2() { return static_cast<int32_t>(offsetof(UIVertex_t475044788, ___color_2)); }
	inline Color32_t2411287715  get_color_2() const { return ___color_2; }
	inline Color32_t2411287715 * get_address_of_color_2() { return &___color_2; }
	inline void set_color_2(Color32_t2411287715  value)
	{
		___color_2 = value;
	}

	inline static int32_t get_offset_of_uv0_3() { return static_cast<int32_t>(offsetof(UIVertex_t475044788, ___uv0_3)); }
	inline Vector2_t1065050092  get_uv0_3() const { return ___uv0_3; }
	inline Vector2_t1065050092 * get_address_of_uv0_3() { return &___uv0_3; }
	inline void set_uv0_3(Vector2_t1065050092  value)
	{
		___uv0_3 = value;
	}

	inline static int32_t get_offset_of_uv1_4() { return static_cast<int32_t>(offsetof(UIVertex_t475044788, ___uv1_4)); }
	inline Vector2_t1065050092  get_uv1_4() const { return ___uv1_4; }
	inline Vector2_t1065050092 * get_address_of_uv1_4() { return &___uv1_4; }
	inline void set_uv1_4(Vector2_t1065050092  value)
	{
		___uv1_4 = value;
	}

	inline static int32_t get_offset_of_uv2_5() { return static_cast<int32_t>(offsetof(UIVertex_t475044788, ___uv2_5)); }
	inline Vector2_t1065050092  get_uv2_5() const { return ___uv2_5; }
	inline Vector2_t1065050092 * get_address_of_uv2_5() { return &___uv2_5; }
	inline void set_uv2_5(Vector2_t1065050092  value)
	{
		___uv2_5 = value;
	}

	inline static int32_t get_offset_of_uv3_6() { return static_cast<int32_t>(offsetof(UIVertex_t475044788, ___uv3_6)); }
	inline Vector2_t1065050092  get_uv3_6() const { return ___uv3_6; }
	inline Vector2_t1065050092 * get_address_of_uv3_6() { return &___uv3_6; }
	inline void set_uv3_6(Vector2_t1065050092  value)
	{
		___uv3_6 = value;
	}

	inline static int32_t get_offset_of_tangent_7() { return static_cast<int32_t>(offsetof(UIVertex_t475044788, ___tangent_7)); }
	inline Vector4_t2651204353  get_tangent_7() const { return ___tangent_7; }
	inline Vector4_t2651204353 * get_address_of_tangent_7() { return &___tangent_7; }
	inline void set_tangent_7(Vector4_t2651204353  value)
	{
		___tangent_7 = value;
	}
};

struct UIVertex_t475044788_StaticFields
{
public:
	// UnityEngine.Color32 UnityEngine.UIVertex::s_DefaultColor
	Color32_t2411287715  ___s_DefaultColor_8;
	// UnityEngine.Vector4 UnityEngine.UIVertex::s_DefaultTangent
	Vector4_t2651204353  ___s_DefaultTangent_9;
	// UnityEngine.UIVertex UnityEngine.UIVertex::simpleVert
	UIVertex_t475044788  ___simpleVert_10;

public:
	inline static int32_t get_offset_of_s_DefaultColor_8() { return static_cast<int32_t>(offsetof(UIVertex_t475044788_StaticFields, ___s_DefaultColor_8)); }
	inline Color32_t2411287715  get_s_DefaultColor_8() const { return ___s_DefaultColor_8; }
	inline Color32_t2411287715 * get_address_of_s_DefaultColor_8() { return &___s_DefaultColor_8; }
	inline void set_s_DefaultColor_8(Color32_t2411287715  value)
	{
		___s_DefaultColor_8 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(UIVertex_t475044788_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_t2651204353  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_t2651204353 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_t2651204353  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_simpleVert_10() { return static_cast<int32_t>(offsetof(UIVertex_t475044788_StaticFields, ___simpleVert_10)); }
	inline UIVertex_t475044788  get_simpleVert_10() const { return ___simpleVert_10; }
	inline UIVertex_t475044788 * get_address_of_simpleVert_10() { return &___simpleVert_10; }
	inline void set_simpleVert_10(UIVertex_t475044788  value)
	{
		___simpleVert_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UIVERTEX_T475044788_H
#ifndef SCRIPTABLEOBJECT_T229319923_H
#define SCRIPTABLEOBJECT_T229319923_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t229319923  : public Object_t350248726
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t229319923_marshaled_pinvoke : public Object_t350248726_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t229319923_marshaled_com : public Object_t350248726_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T229319923_H
#ifndef ANIMATORCONTROLLERPLAYABLE_T4108006533_H
#define ANIMATORCONTROLLERPLAYABLE_T4108006533_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animations.AnimatorControllerPlayable
struct  AnimatorControllerPlayable_t4108006533 
{
public:
	// UnityEngine.Playables.PlayableHandle UnityEngine.Animations.AnimatorControllerPlayable::m_Handle
	PlayableHandle_t3645500743  ___m_Handle_0;

public:
	inline static int32_t get_offset_of_m_Handle_0() { return static_cast<int32_t>(offsetof(AnimatorControllerPlayable_t4108006533, ___m_Handle_0)); }
	inline PlayableHandle_t3645500743  get_m_Handle_0() const { return ___m_Handle_0; }
	inline PlayableHandle_t3645500743 * get_address_of_m_Handle_0() { return &___m_Handle_0; }
	inline void set_m_Handle_0(PlayableHandle_t3645500743  value)
	{
		___m_Handle_0 = value;
	}
};

struct AnimatorControllerPlayable_t4108006533_StaticFields
{
public:
	// UnityEngine.Animations.AnimatorControllerPlayable UnityEngine.Animations.AnimatorControllerPlayable::m_NullPlayable
	AnimatorControllerPlayable_t4108006533  ___m_NullPlayable_1;

public:
	inline static int32_t get_offset_of_m_NullPlayable_1() { return static_cast<int32_t>(offsetof(AnimatorControllerPlayable_t4108006533_StaticFields, ___m_NullPlayable_1)); }
	inline AnimatorControllerPlayable_t4108006533  get_m_NullPlayable_1() const { return ___m_NullPlayable_1; }
	inline AnimatorControllerPlayable_t4108006533 * get_address_of_m_NullPlayable_1() { return &___m_NullPlayable_1; }
	inline void set_m_NullPlayable_1(AnimatorControllerPlayable_t4108006533  value)
	{
		___m_NullPlayable_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATORCONTROLLERPLAYABLE_T4108006533_H
#ifndef REGEX_T1806904682_H
#define REGEX_T1806904682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Text.RegularExpressions.Regex
struct  Regex_t1806904682  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.IMachineFactory System.Text.RegularExpressions.Regex::machineFactory
	RuntimeObject* ___machineFactory_1;
	// System.Collections.IDictionary System.Text.RegularExpressions.Regex::mapping
	RuntimeObject* ___mapping_2;
	// System.Int32 System.Text.RegularExpressions.Regex::group_count
	int32_t ___group_count_3;
	// System.Int32 System.Text.RegularExpressions.Regex::gap
	int32_t ___gap_4;
	// System.String[] System.Text.RegularExpressions.Regex::group_names
	StringU5BU5D_t656593794* ___group_names_5;
	// System.Int32[] System.Text.RegularExpressions.Regex::group_numbers
	Int32U5BU5D_t281224829* ___group_numbers_6;
	// System.String System.Text.RegularExpressions.Regex::pattern
	String_t* ___pattern_7;
	// System.Text.RegularExpressions.RegexOptions System.Text.RegularExpressions.Regex::roptions
	int32_t ___roptions_8;

public:
	inline static int32_t get_offset_of_machineFactory_1() { return static_cast<int32_t>(offsetof(Regex_t1806904682, ___machineFactory_1)); }
	inline RuntimeObject* get_machineFactory_1() const { return ___machineFactory_1; }
	inline RuntimeObject** get_address_of_machineFactory_1() { return &___machineFactory_1; }
	inline void set_machineFactory_1(RuntimeObject* value)
	{
		___machineFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&___machineFactory_1), value);
	}

	inline static int32_t get_offset_of_mapping_2() { return static_cast<int32_t>(offsetof(Regex_t1806904682, ___mapping_2)); }
	inline RuntimeObject* get_mapping_2() const { return ___mapping_2; }
	inline RuntimeObject** get_address_of_mapping_2() { return &___mapping_2; }
	inline void set_mapping_2(RuntimeObject* value)
	{
		___mapping_2 = value;
		Il2CppCodeGenWriteBarrier((&___mapping_2), value);
	}

	inline static int32_t get_offset_of_group_count_3() { return static_cast<int32_t>(offsetof(Regex_t1806904682, ___group_count_3)); }
	inline int32_t get_group_count_3() const { return ___group_count_3; }
	inline int32_t* get_address_of_group_count_3() { return &___group_count_3; }
	inline void set_group_count_3(int32_t value)
	{
		___group_count_3 = value;
	}

	inline static int32_t get_offset_of_gap_4() { return static_cast<int32_t>(offsetof(Regex_t1806904682, ___gap_4)); }
	inline int32_t get_gap_4() const { return ___gap_4; }
	inline int32_t* get_address_of_gap_4() { return &___gap_4; }
	inline void set_gap_4(int32_t value)
	{
		___gap_4 = value;
	}

	inline static int32_t get_offset_of_group_names_5() { return static_cast<int32_t>(offsetof(Regex_t1806904682, ___group_names_5)); }
	inline StringU5BU5D_t656593794* get_group_names_5() const { return ___group_names_5; }
	inline StringU5BU5D_t656593794** get_address_of_group_names_5() { return &___group_names_5; }
	inline void set_group_names_5(StringU5BU5D_t656593794* value)
	{
		___group_names_5 = value;
		Il2CppCodeGenWriteBarrier((&___group_names_5), value);
	}

	inline static int32_t get_offset_of_group_numbers_6() { return static_cast<int32_t>(offsetof(Regex_t1806904682, ___group_numbers_6)); }
	inline Int32U5BU5D_t281224829* get_group_numbers_6() const { return ___group_numbers_6; }
	inline Int32U5BU5D_t281224829** get_address_of_group_numbers_6() { return &___group_numbers_6; }
	inline void set_group_numbers_6(Int32U5BU5D_t281224829* value)
	{
		___group_numbers_6 = value;
		Il2CppCodeGenWriteBarrier((&___group_numbers_6), value);
	}

	inline static int32_t get_offset_of_pattern_7() { return static_cast<int32_t>(offsetof(Regex_t1806904682, ___pattern_7)); }
	inline String_t* get_pattern_7() const { return ___pattern_7; }
	inline String_t** get_address_of_pattern_7() { return &___pattern_7; }
	inline void set_pattern_7(String_t* value)
	{
		___pattern_7 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_7), value);
	}

	inline static int32_t get_offset_of_roptions_8() { return static_cast<int32_t>(offsetof(Regex_t1806904682, ___roptions_8)); }
	inline int32_t get_roptions_8() const { return ___roptions_8; }
	inline int32_t* get_address_of_roptions_8() { return &___roptions_8; }
	inline void set_roptions_8(int32_t value)
	{
		___roptions_8 = value;
	}
};

struct Regex_t1806904682_StaticFields
{
public:
	// System.Text.RegularExpressions.FactoryCache System.Text.RegularExpressions.Regex::cache
	FactoryCache_t3440100452 * ___cache_0;

public:
	inline static int32_t get_offset_of_cache_0() { return static_cast<int32_t>(offsetof(Regex_t1806904682_StaticFields, ___cache_0)); }
	inline FactoryCache_t3440100452 * get_cache_0() const { return ___cache_0; }
	inline FactoryCache_t3440100452 ** get_address_of_cache_0() { return &___cache_0; }
	inline void set_cache_0(FactoryCache_t3440100452 * value)
	{
		___cache_0 = value;
		Il2CppCodeGenWriteBarrier((&___cache_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEX_T1806904682_H
#ifndef TEXTGENERATIONSETTINGS_T781948148_H
#define TEXTGENERATIONSETTINGS_T781948148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextGenerationSettings
struct  TextGenerationSettings_t781948148 
{
public:
	// UnityEngine.Font UnityEngine.TextGenerationSettings::font
	Font_t1312336880 * ___font_0;
	// UnityEngine.Color UnityEngine.TextGenerationSettings::color
	Color_t4183816058  ___color_1;
	// System.Int32 UnityEngine.TextGenerationSettings::fontSize
	int32_t ___fontSize_2;
	// System.Single UnityEngine.TextGenerationSettings::lineSpacing
	float ___lineSpacing_3;
	// System.Boolean UnityEngine.TextGenerationSettings::richText
	bool ___richText_4;
	// System.Single UnityEngine.TextGenerationSettings::scaleFactor
	float ___scaleFactor_5;
	// UnityEngine.FontStyle UnityEngine.TextGenerationSettings::fontStyle
	int32_t ___fontStyle_6;
	// UnityEngine.TextAnchor UnityEngine.TextGenerationSettings::textAnchor
	int32_t ___textAnchor_7;
	// System.Boolean UnityEngine.TextGenerationSettings::alignByGeometry
	bool ___alignByGeometry_8;
	// System.Boolean UnityEngine.TextGenerationSettings::resizeTextForBestFit
	bool ___resizeTextForBestFit_9;
	// System.Int32 UnityEngine.TextGenerationSettings::resizeTextMinSize
	int32_t ___resizeTextMinSize_10;
	// System.Int32 UnityEngine.TextGenerationSettings::resizeTextMaxSize
	int32_t ___resizeTextMaxSize_11;
	// System.Boolean UnityEngine.TextGenerationSettings::updateBounds
	bool ___updateBounds_12;
	// UnityEngine.VerticalWrapMode UnityEngine.TextGenerationSettings::verticalOverflow
	int32_t ___verticalOverflow_13;
	// UnityEngine.HorizontalWrapMode UnityEngine.TextGenerationSettings::horizontalOverflow
	int32_t ___horizontalOverflow_14;
	// UnityEngine.Vector2 UnityEngine.TextGenerationSettings::generationExtents
	Vector2_t1065050092  ___generationExtents_15;
	// UnityEngine.Vector2 UnityEngine.TextGenerationSettings::pivot
	Vector2_t1065050092  ___pivot_16;
	// System.Boolean UnityEngine.TextGenerationSettings::generateOutOfBounds
	bool ___generateOutOfBounds_17;

public:
	inline static int32_t get_offset_of_font_0() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___font_0)); }
	inline Font_t1312336880 * get_font_0() const { return ___font_0; }
	inline Font_t1312336880 ** get_address_of_font_0() { return &___font_0; }
	inline void set_font_0(Font_t1312336880 * value)
	{
		___font_0 = value;
		Il2CppCodeGenWriteBarrier((&___font_0), value);
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___color_1)); }
	inline Color_t4183816058  get_color_1() const { return ___color_1; }
	inline Color_t4183816058 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_t4183816058  value)
	{
		___color_1 = value;
	}

	inline static int32_t get_offset_of_fontSize_2() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___fontSize_2)); }
	inline int32_t get_fontSize_2() const { return ___fontSize_2; }
	inline int32_t* get_address_of_fontSize_2() { return &___fontSize_2; }
	inline void set_fontSize_2(int32_t value)
	{
		___fontSize_2 = value;
	}

	inline static int32_t get_offset_of_lineSpacing_3() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___lineSpacing_3)); }
	inline float get_lineSpacing_3() const { return ___lineSpacing_3; }
	inline float* get_address_of_lineSpacing_3() { return &___lineSpacing_3; }
	inline void set_lineSpacing_3(float value)
	{
		___lineSpacing_3 = value;
	}

	inline static int32_t get_offset_of_richText_4() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___richText_4)); }
	inline bool get_richText_4() const { return ___richText_4; }
	inline bool* get_address_of_richText_4() { return &___richText_4; }
	inline void set_richText_4(bool value)
	{
		___richText_4 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_5() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___scaleFactor_5)); }
	inline float get_scaleFactor_5() const { return ___scaleFactor_5; }
	inline float* get_address_of_scaleFactor_5() { return &___scaleFactor_5; }
	inline void set_scaleFactor_5(float value)
	{
		___scaleFactor_5 = value;
	}

	inline static int32_t get_offset_of_fontStyle_6() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___fontStyle_6)); }
	inline int32_t get_fontStyle_6() const { return ___fontStyle_6; }
	inline int32_t* get_address_of_fontStyle_6() { return &___fontStyle_6; }
	inline void set_fontStyle_6(int32_t value)
	{
		___fontStyle_6 = value;
	}

	inline static int32_t get_offset_of_textAnchor_7() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___textAnchor_7)); }
	inline int32_t get_textAnchor_7() const { return ___textAnchor_7; }
	inline int32_t* get_address_of_textAnchor_7() { return &___textAnchor_7; }
	inline void set_textAnchor_7(int32_t value)
	{
		___textAnchor_7 = value;
	}

	inline static int32_t get_offset_of_alignByGeometry_8() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___alignByGeometry_8)); }
	inline bool get_alignByGeometry_8() const { return ___alignByGeometry_8; }
	inline bool* get_address_of_alignByGeometry_8() { return &___alignByGeometry_8; }
	inline void set_alignByGeometry_8(bool value)
	{
		___alignByGeometry_8 = value;
	}

	inline static int32_t get_offset_of_resizeTextForBestFit_9() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___resizeTextForBestFit_9)); }
	inline bool get_resizeTextForBestFit_9() const { return ___resizeTextForBestFit_9; }
	inline bool* get_address_of_resizeTextForBestFit_9() { return &___resizeTextForBestFit_9; }
	inline void set_resizeTextForBestFit_9(bool value)
	{
		___resizeTextForBestFit_9 = value;
	}

	inline static int32_t get_offset_of_resizeTextMinSize_10() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___resizeTextMinSize_10)); }
	inline int32_t get_resizeTextMinSize_10() const { return ___resizeTextMinSize_10; }
	inline int32_t* get_address_of_resizeTextMinSize_10() { return &___resizeTextMinSize_10; }
	inline void set_resizeTextMinSize_10(int32_t value)
	{
		___resizeTextMinSize_10 = value;
	}

	inline static int32_t get_offset_of_resizeTextMaxSize_11() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___resizeTextMaxSize_11)); }
	inline int32_t get_resizeTextMaxSize_11() const { return ___resizeTextMaxSize_11; }
	inline int32_t* get_address_of_resizeTextMaxSize_11() { return &___resizeTextMaxSize_11; }
	inline void set_resizeTextMaxSize_11(int32_t value)
	{
		___resizeTextMaxSize_11 = value;
	}

	inline static int32_t get_offset_of_updateBounds_12() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___updateBounds_12)); }
	inline bool get_updateBounds_12() const { return ___updateBounds_12; }
	inline bool* get_address_of_updateBounds_12() { return &___updateBounds_12; }
	inline void set_updateBounds_12(bool value)
	{
		___updateBounds_12 = value;
	}

	inline static int32_t get_offset_of_verticalOverflow_13() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___verticalOverflow_13)); }
	inline int32_t get_verticalOverflow_13() const { return ___verticalOverflow_13; }
	inline int32_t* get_address_of_verticalOverflow_13() { return &___verticalOverflow_13; }
	inline void set_verticalOverflow_13(int32_t value)
	{
		___verticalOverflow_13 = value;
	}

	inline static int32_t get_offset_of_horizontalOverflow_14() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___horizontalOverflow_14)); }
	inline int32_t get_horizontalOverflow_14() const { return ___horizontalOverflow_14; }
	inline int32_t* get_address_of_horizontalOverflow_14() { return &___horizontalOverflow_14; }
	inline void set_horizontalOverflow_14(int32_t value)
	{
		___horizontalOverflow_14 = value;
	}

	inline static int32_t get_offset_of_generationExtents_15() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___generationExtents_15)); }
	inline Vector2_t1065050092  get_generationExtents_15() const { return ___generationExtents_15; }
	inline Vector2_t1065050092 * get_address_of_generationExtents_15() { return &___generationExtents_15; }
	inline void set_generationExtents_15(Vector2_t1065050092  value)
	{
		___generationExtents_15 = value;
	}

	inline static int32_t get_offset_of_pivot_16() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___pivot_16)); }
	inline Vector2_t1065050092  get_pivot_16() const { return ___pivot_16; }
	inline Vector2_t1065050092 * get_address_of_pivot_16() { return &___pivot_16; }
	inline void set_pivot_16(Vector2_t1065050092  value)
	{
		___pivot_16 = value;
	}

	inline static int32_t get_offset_of_generateOutOfBounds_17() { return static_cast<int32_t>(offsetof(TextGenerationSettings_t781948148, ___generateOutOfBounds_17)); }
	inline bool get_generateOutOfBounds_17() const { return ___generateOutOfBounds_17; }
	inline bool* get_address_of_generateOutOfBounds_17() { return &___generateOutOfBounds_17; }
	inline void set_generateOutOfBounds_17(bool value)
	{
		___generateOutOfBounds_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TextGenerationSettings
struct TextGenerationSettings_t781948148_marshaled_pinvoke
{
	Font_t1312336880 * ___font_0;
	Color_t4183816058  ___color_1;
	int32_t ___fontSize_2;
	float ___lineSpacing_3;
	int32_t ___richText_4;
	float ___scaleFactor_5;
	int32_t ___fontStyle_6;
	int32_t ___textAnchor_7;
	int32_t ___alignByGeometry_8;
	int32_t ___resizeTextForBestFit_9;
	int32_t ___resizeTextMinSize_10;
	int32_t ___resizeTextMaxSize_11;
	int32_t ___updateBounds_12;
	int32_t ___verticalOverflow_13;
	int32_t ___horizontalOverflow_14;
	Vector2_t1065050092  ___generationExtents_15;
	Vector2_t1065050092  ___pivot_16;
	int32_t ___generateOutOfBounds_17;
};
// Native definition for COM marshalling of UnityEngine.TextGenerationSettings
struct TextGenerationSettings_t781948148_marshaled_com
{
	Font_t1312336880 * ___font_0;
	Color_t4183816058  ___color_1;
	int32_t ___fontSize_2;
	float ___lineSpacing_3;
	int32_t ___richText_4;
	float ___scaleFactor_5;
	int32_t ___fontStyle_6;
	int32_t ___textAnchor_7;
	int32_t ___alignByGeometry_8;
	int32_t ___resizeTextForBestFit_9;
	int32_t ___resizeTextMinSize_10;
	int32_t ___resizeTextMaxSize_11;
	int32_t ___updateBounds_12;
	int32_t ___verticalOverflow_13;
	int32_t ___horizontalOverflow_14;
	Vector2_t1065050092  ___generationExtents_15;
	Vector2_t1065050092  ___pivot_16;
	int32_t ___generateOutOfBounds_17;
};
#endif // TEXTGENERATIONSETTINGS_T781948148_H
#ifndef MULTICASTDELEGATE_T4207739222_H
#define MULTICASTDELEGATE_T4207739222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t4207739222  : public Delegate_t1310841479
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t4207739222 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t4207739222 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4207739222, ___prev_9)); }
	inline MulticastDelegate_t4207739222 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t4207739222 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t4207739222 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4207739222, ___kpm_next_10)); }
	inline MulticastDelegate_t4207739222 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t4207739222 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t4207739222 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T4207739222_H
#ifndef TEXTASSET_T3707725843_H
#define TEXTASSET_T3707725843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAsset
struct  TextAsset_t3707725843  : public Object_t350248726
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTASSET_T3707725843_H
#ifndef TEXTEDITOR_T3996366928_H
#define TEXTEDITOR_T3996366928_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextEditor
struct  TextEditor_t3996366928  : public RuntimeObject
{
public:
	// UnityEngine.TouchScreenKeyboard UnityEngine.TextEditor::keyboardOnScreen
	TouchScreenKeyboard_t1544112657 * ___keyboardOnScreen_0;
	// System.Int32 UnityEngine.TextEditor::controlID
	int32_t ___controlID_1;
	// UnityEngine.GUIStyle UnityEngine.TextEditor::style
	GUIStyle_t2201526215 * ___style_2;
	// System.Boolean UnityEngine.TextEditor::multiline
	bool ___multiline_3;
	// System.Boolean UnityEngine.TextEditor::hasHorizontalCursorPos
	bool ___hasHorizontalCursorPos_4;
	// System.Boolean UnityEngine.TextEditor::isPasswordField
	bool ___isPasswordField_5;
	// UnityEngine.Vector2 UnityEngine.TextEditor::scrollOffset
	Vector2_t1065050092  ___scrollOffset_6;
	// UnityEngine.GUIContent UnityEngine.TextEditor::m_Content
	GUIContent_t1852996497 * ___m_Content_7;
	// System.Int32 UnityEngine.TextEditor::m_CursorIndex
	int32_t ___m_CursorIndex_8;
	// System.Int32 UnityEngine.TextEditor::m_SelectIndex
	int32_t ___m_SelectIndex_9;
	// System.Boolean UnityEngine.TextEditor::m_RevealCursor
	bool ___m_RevealCursor_10;
	// System.Boolean UnityEngine.TextEditor::m_MouseDragSelectsWholeWords
	bool ___m_MouseDragSelectsWholeWords_11;
	// System.Int32 UnityEngine.TextEditor::m_DblClickInitPos
	int32_t ___m_DblClickInitPos_12;
	// UnityEngine.TextEditor/DblClickSnapping UnityEngine.TextEditor::m_DblClickSnap
	uint8_t ___m_DblClickSnap_13;
	// System.Boolean UnityEngine.TextEditor::m_bJustSelected
	bool ___m_bJustSelected_14;
	// System.Int32 UnityEngine.TextEditor::m_iAltCursorPos
	int32_t ___m_iAltCursorPos_15;

public:
	inline static int32_t get_offset_of_keyboardOnScreen_0() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___keyboardOnScreen_0)); }
	inline TouchScreenKeyboard_t1544112657 * get_keyboardOnScreen_0() const { return ___keyboardOnScreen_0; }
	inline TouchScreenKeyboard_t1544112657 ** get_address_of_keyboardOnScreen_0() { return &___keyboardOnScreen_0; }
	inline void set_keyboardOnScreen_0(TouchScreenKeyboard_t1544112657 * value)
	{
		___keyboardOnScreen_0 = value;
		Il2CppCodeGenWriteBarrier((&___keyboardOnScreen_0), value);
	}

	inline static int32_t get_offset_of_controlID_1() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___controlID_1)); }
	inline int32_t get_controlID_1() const { return ___controlID_1; }
	inline int32_t* get_address_of_controlID_1() { return &___controlID_1; }
	inline void set_controlID_1(int32_t value)
	{
		___controlID_1 = value;
	}

	inline static int32_t get_offset_of_style_2() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___style_2)); }
	inline GUIStyle_t2201526215 * get_style_2() const { return ___style_2; }
	inline GUIStyle_t2201526215 ** get_address_of_style_2() { return &___style_2; }
	inline void set_style_2(GUIStyle_t2201526215 * value)
	{
		___style_2 = value;
		Il2CppCodeGenWriteBarrier((&___style_2), value);
	}

	inline static int32_t get_offset_of_multiline_3() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___multiline_3)); }
	inline bool get_multiline_3() const { return ___multiline_3; }
	inline bool* get_address_of_multiline_3() { return &___multiline_3; }
	inline void set_multiline_3(bool value)
	{
		___multiline_3 = value;
	}

	inline static int32_t get_offset_of_hasHorizontalCursorPos_4() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___hasHorizontalCursorPos_4)); }
	inline bool get_hasHorizontalCursorPos_4() const { return ___hasHorizontalCursorPos_4; }
	inline bool* get_address_of_hasHorizontalCursorPos_4() { return &___hasHorizontalCursorPos_4; }
	inline void set_hasHorizontalCursorPos_4(bool value)
	{
		___hasHorizontalCursorPos_4 = value;
	}

	inline static int32_t get_offset_of_isPasswordField_5() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___isPasswordField_5)); }
	inline bool get_isPasswordField_5() const { return ___isPasswordField_5; }
	inline bool* get_address_of_isPasswordField_5() { return &___isPasswordField_5; }
	inline void set_isPasswordField_5(bool value)
	{
		___isPasswordField_5 = value;
	}

	inline static int32_t get_offset_of_scrollOffset_6() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___scrollOffset_6)); }
	inline Vector2_t1065050092  get_scrollOffset_6() const { return ___scrollOffset_6; }
	inline Vector2_t1065050092 * get_address_of_scrollOffset_6() { return &___scrollOffset_6; }
	inline void set_scrollOffset_6(Vector2_t1065050092  value)
	{
		___scrollOffset_6 = value;
	}

	inline static int32_t get_offset_of_m_Content_7() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___m_Content_7)); }
	inline GUIContent_t1852996497 * get_m_Content_7() const { return ___m_Content_7; }
	inline GUIContent_t1852996497 ** get_address_of_m_Content_7() { return &___m_Content_7; }
	inline void set_m_Content_7(GUIContent_t1852996497 * value)
	{
		___m_Content_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_7), value);
	}

	inline static int32_t get_offset_of_m_CursorIndex_8() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___m_CursorIndex_8)); }
	inline int32_t get_m_CursorIndex_8() const { return ___m_CursorIndex_8; }
	inline int32_t* get_address_of_m_CursorIndex_8() { return &___m_CursorIndex_8; }
	inline void set_m_CursorIndex_8(int32_t value)
	{
		___m_CursorIndex_8 = value;
	}

	inline static int32_t get_offset_of_m_SelectIndex_9() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___m_SelectIndex_9)); }
	inline int32_t get_m_SelectIndex_9() const { return ___m_SelectIndex_9; }
	inline int32_t* get_address_of_m_SelectIndex_9() { return &___m_SelectIndex_9; }
	inline void set_m_SelectIndex_9(int32_t value)
	{
		___m_SelectIndex_9 = value;
	}

	inline static int32_t get_offset_of_m_RevealCursor_10() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___m_RevealCursor_10)); }
	inline bool get_m_RevealCursor_10() const { return ___m_RevealCursor_10; }
	inline bool* get_address_of_m_RevealCursor_10() { return &___m_RevealCursor_10; }
	inline void set_m_RevealCursor_10(bool value)
	{
		___m_RevealCursor_10 = value;
	}

	inline static int32_t get_offset_of_m_MouseDragSelectsWholeWords_11() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___m_MouseDragSelectsWholeWords_11)); }
	inline bool get_m_MouseDragSelectsWholeWords_11() const { return ___m_MouseDragSelectsWholeWords_11; }
	inline bool* get_address_of_m_MouseDragSelectsWholeWords_11() { return &___m_MouseDragSelectsWholeWords_11; }
	inline void set_m_MouseDragSelectsWholeWords_11(bool value)
	{
		___m_MouseDragSelectsWholeWords_11 = value;
	}

	inline static int32_t get_offset_of_m_DblClickInitPos_12() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___m_DblClickInitPos_12)); }
	inline int32_t get_m_DblClickInitPos_12() const { return ___m_DblClickInitPos_12; }
	inline int32_t* get_address_of_m_DblClickInitPos_12() { return &___m_DblClickInitPos_12; }
	inline void set_m_DblClickInitPos_12(int32_t value)
	{
		___m_DblClickInitPos_12 = value;
	}

	inline static int32_t get_offset_of_m_DblClickSnap_13() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___m_DblClickSnap_13)); }
	inline uint8_t get_m_DblClickSnap_13() const { return ___m_DblClickSnap_13; }
	inline uint8_t* get_address_of_m_DblClickSnap_13() { return &___m_DblClickSnap_13; }
	inline void set_m_DblClickSnap_13(uint8_t value)
	{
		___m_DblClickSnap_13 = value;
	}

	inline static int32_t get_offset_of_m_bJustSelected_14() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___m_bJustSelected_14)); }
	inline bool get_m_bJustSelected_14() const { return ___m_bJustSelected_14; }
	inline bool* get_address_of_m_bJustSelected_14() { return &___m_bJustSelected_14; }
	inline void set_m_bJustSelected_14(bool value)
	{
		___m_bJustSelected_14 = value;
	}

	inline static int32_t get_offset_of_m_iAltCursorPos_15() { return static_cast<int32_t>(offsetof(TextEditor_t3996366928, ___m_iAltCursorPos_15)); }
	inline int32_t get_m_iAltCursorPos_15() const { return ___m_iAltCursorPos_15; }
	inline int32_t* get_address_of_m_iAltCursorPos_15() { return &___m_iAltCursorPos_15; }
	inline void set_m_iAltCursorPos_15(int32_t value)
	{
		___m_iAltCursorPos_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTEDITOR_T3996366928_H
#ifndef COMPONENT_T1852985663_H
#define COMPONENT_T1852985663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1852985663  : public Object_t350248726
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1852985663_H
#ifndef GUISTYLE_T2201526215_H
#define GUISTYLE_T2201526215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyle
struct  GUIStyle_t2201526215  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	IntPtr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t60144046 * ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t60144046 * ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t60144046 * ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t60144046 * ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t60144046 * ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t60144046 * ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t60144046 * ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t60144046 * ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_t2305860064 * ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_t2305860064 * ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_t2305860064 * ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_t2305860064 * ___m_Overflow_12;
	// UnityEngine.Font UnityEngine.GUIStyle::m_FontInternal
	Font_t1312336880 * ___m_FontInternal_13;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_Normal_1)); }
	inline GUIStyleState_t60144046 * get_m_Normal_1() const { return ___m_Normal_1; }
	inline GUIStyleState_t60144046 ** get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(GUIStyleState_t60144046 * value)
	{
		___m_Normal_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normal_1), value);
	}

	inline static int32_t get_offset_of_m_Hover_2() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_Hover_2)); }
	inline GUIStyleState_t60144046 * get_m_Hover_2() const { return ___m_Hover_2; }
	inline GUIStyleState_t60144046 ** get_address_of_m_Hover_2() { return &___m_Hover_2; }
	inline void set_m_Hover_2(GUIStyleState_t60144046 * value)
	{
		___m_Hover_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hover_2), value);
	}

	inline static int32_t get_offset_of_m_Active_3() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_Active_3)); }
	inline GUIStyleState_t60144046 * get_m_Active_3() const { return ___m_Active_3; }
	inline GUIStyleState_t60144046 ** get_address_of_m_Active_3() { return &___m_Active_3; }
	inline void set_m_Active_3(GUIStyleState_t60144046 * value)
	{
		___m_Active_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Active_3), value);
	}

	inline static int32_t get_offset_of_m_Focused_4() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_Focused_4)); }
	inline GUIStyleState_t60144046 * get_m_Focused_4() const { return ___m_Focused_4; }
	inline GUIStyleState_t60144046 ** get_address_of_m_Focused_4() { return &___m_Focused_4; }
	inline void set_m_Focused_4(GUIStyleState_t60144046 * value)
	{
		___m_Focused_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Focused_4), value);
	}

	inline static int32_t get_offset_of_m_OnNormal_5() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_OnNormal_5)); }
	inline GUIStyleState_t60144046 * get_m_OnNormal_5() const { return ___m_OnNormal_5; }
	inline GUIStyleState_t60144046 ** get_address_of_m_OnNormal_5() { return &___m_OnNormal_5; }
	inline void set_m_OnNormal_5(GUIStyleState_t60144046 * value)
	{
		___m_OnNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnNormal_5), value);
	}

	inline static int32_t get_offset_of_m_OnHover_6() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_OnHover_6)); }
	inline GUIStyleState_t60144046 * get_m_OnHover_6() const { return ___m_OnHover_6; }
	inline GUIStyleState_t60144046 ** get_address_of_m_OnHover_6() { return &___m_OnHover_6; }
	inline void set_m_OnHover_6(GUIStyleState_t60144046 * value)
	{
		___m_OnHover_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnHover_6), value);
	}

	inline static int32_t get_offset_of_m_OnActive_7() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_OnActive_7)); }
	inline GUIStyleState_t60144046 * get_m_OnActive_7() const { return ___m_OnActive_7; }
	inline GUIStyleState_t60144046 ** get_address_of_m_OnActive_7() { return &___m_OnActive_7; }
	inline void set_m_OnActive_7(GUIStyleState_t60144046 * value)
	{
		___m_OnActive_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnActive_7), value);
	}

	inline static int32_t get_offset_of_m_OnFocused_8() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_OnFocused_8)); }
	inline GUIStyleState_t60144046 * get_m_OnFocused_8() const { return ___m_OnFocused_8; }
	inline GUIStyleState_t60144046 ** get_address_of_m_OnFocused_8() { return &___m_OnFocused_8; }
	inline void set_m_OnFocused_8(GUIStyleState_t60144046 * value)
	{
		___m_OnFocused_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnFocused_8), value);
	}

	inline static int32_t get_offset_of_m_Border_9() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_Border_9)); }
	inline RectOffset_t2305860064 * get_m_Border_9() const { return ___m_Border_9; }
	inline RectOffset_t2305860064 ** get_address_of_m_Border_9() { return &___m_Border_9; }
	inline void set_m_Border_9(RectOffset_t2305860064 * value)
	{
		___m_Border_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Border_9), value);
	}

	inline static int32_t get_offset_of_m_Padding_10() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_Padding_10)); }
	inline RectOffset_t2305860064 * get_m_Padding_10() const { return ___m_Padding_10; }
	inline RectOffset_t2305860064 ** get_address_of_m_Padding_10() { return &___m_Padding_10; }
	inline void set_m_Padding_10(RectOffset_t2305860064 * value)
	{
		___m_Padding_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_10), value);
	}

	inline static int32_t get_offset_of_m_Margin_11() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_Margin_11)); }
	inline RectOffset_t2305860064 * get_m_Margin_11() const { return ___m_Margin_11; }
	inline RectOffset_t2305860064 ** get_address_of_m_Margin_11() { return &___m_Margin_11; }
	inline void set_m_Margin_11(RectOffset_t2305860064 * value)
	{
		___m_Margin_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Margin_11), value);
	}

	inline static int32_t get_offset_of_m_Overflow_12() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_Overflow_12)); }
	inline RectOffset_t2305860064 * get_m_Overflow_12() const { return ___m_Overflow_12; }
	inline RectOffset_t2305860064 ** get_address_of_m_Overflow_12() { return &___m_Overflow_12; }
	inline void set_m_Overflow_12(RectOffset_t2305860064 * value)
	{
		___m_Overflow_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Overflow_12), value);
	}

	inline static int32_t get_offset_of_m_FontInternal_13() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215, ___m_FontInternal_13)); }
	inline Font_t1312336880 * get_m_FontInternal_13() const { return ___m_FontInternal_13; }
	inline Font_t1312336880 ** get_address_of_m_FontInternal_13() { return &___m_FontInternal_13; }
	inline void set_m_FontInternal_13(Font_t1312336880 * value)
	{
		___m_FontInternal_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontInternal_13), value);
	}
};

struct GUIStyle_t2201526215_StaticFields
{
public:
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_14;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t2201526215 * ___s_None_15;

public:
	inline static int32_t get_offset_of_showKeyboardFocus_14() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215_StaticFields, ___showKeyboardFocus_14)); }
	inline bool get_showKeyboardFocus_14() const { return ___showKeyboardFocus_14; }
	inline bool* get_address_of_showKeyboardFocus_14() { return &___showKeyboardFocus_14; }
	inline void set_showKeyboardFocus_14(bool value)
	{
		___showKeyboardFocus_14 = value;
	}

	inline static int32_t get_offset_of_s_None_15() { return static_cast<int32_t>(offsetof(GUIStyle_t2201526215_StaticFields, ___s_None_15)); }
	inline GUIStyle_t2201526215 * get_s_None_15() const { return ___s_None_15; }
	inline GUIStyle_t2201526215 ** get_address_of_s_None_15() { return &___s_None_15; }
	inline void set_s_None_15(GUIStyle_t2201526215 * value)
	{
		___s_None_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_None_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t2201526215_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t60144046_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t60144046_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t60144046_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t60144046_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t60144046_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t60144046_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t60144046_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t60144046_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_t2305860064_marshaled_pinvoke ___m_Border_9;
	RectOffset_t2305860064_marshaled_pinvoke ___m_Padding_10;
	RectOffset_t2305860064_marshaled_pinvoke ___m_Margin_11;
	RectOffset_t2305860064_marshaled_pinvoke ___m_Overflow_12;
	Font_t1312336880 * ___m_FontInternal_13;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t2201526215_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t60144046_marshaled_com* ___m_Normal_1;
	GUIStyleState_t60144046_marshaled_com* ___m_Hover_2;
	GUIStyleState_t60144046_marshaled_com* ___m_Active_3;
	GUIStyleState_t60144046_marshaled_com* ___m_Focused_4;
	GUIStyleState_t60144046_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t60144046_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t60144046_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t60144046_marshaled_com* ___m_OnFocused_8;
	RectOffset_t2305860064_marshaled_com* ___m_Border_9;
	RectOffset_t2305860064_marshaled_com* ___m_Padding_10;
	RectOffset_t2305860064_marshaled_com* ___m_Margin_11;
	RectOffset_t2305860064_marshaled_com* ___m_Overflow_12;
	Font_t1312336880 * ___m_FontInternal_13;
};
#endif // GUISTYLE_T2201526215_H
#ifndef APPDOMAIN_T794471768_H
#define APPDOMAIN_T794471768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AppDomain
struct  AppDomain_t794471768  : public MarshalByRefObject_t4073014582
{
public:
	// System.IntPtr System.AppDomain::_mono_app_domain
	IntPtr_t ____mono_app_domain_1;
	// System.Security.Policy.Evidence System.AppDomain::_evidence
	Evidence_t2017448698 * ____evidence_6;
	// System.Security.PermissionSet System.AppDomain::_granted
	PermissionSet_t1513179846 * ____granted_7;
	// System.Security.Principal.PrincipalPolicy System.AppDomain::_principalPolicy
	int32_t ____principalPolicy_8;
	// System.AppDomainManager System.AppDomain::_domain_manager
	AppDomainManager_t4198241168 * ____domain_manager_11;
	// System.ActivationContext System.AppDomain::_activation
	ActivationContext_t2417885991 * ____activation_12;
	// System.ApplicationIdentity System.AppDomain::_applicationIdentity
	ApplicationIdentity_t106405273 * ____applicationIdentity_13;
	// System.AssemblyLoadEventHandler System.AppDomain::AssemblyLoad
	AssemblyLoadEventHandler_t3668403773 * ___AssemblyLoad_14;
	// System.ResolveEventHandler System.AppDomain::AssemblyResolve
	ResolveEventHandler_t548862186 * ___AssemblyResolve_15;
	// System.EventHandler System.AppDomain::DomainUnload
	EventHandler_t1537570410 * ___DomainUnload_16;
	// System.EventHandler System.AppDomain::ProcessExit
	EventHandler_t1537570410 * ___ProcessExit_17;
	// System.ResolveEventHandler System.AppDomain::ResourceResolve
	ResolveEventHandler_t548862186 * ___ResourceResolve_18;
	// System.ResolveEventHandler System.AppDomain::TypeResolve
	ResolveEventHandler_t548862186 * ___TypeResolve_19;
	// System.UnhandledExceptionEventHandler System.AppDomain::UnhandledException
	UnhandledExceptionEventHandler_t3425235907 * ___UnhandledException_20;
	// System.ResolveEventHandler System.AppDomain::ReflectionOnlyAssemblyResolve
	ResolveEventHandler_t548862186 * ___ReflectionOnlyAssemblyResolve_21;

public:
	inline static int32_t get_offset_of__mono_app_domain_1() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ____mono_app_domain_1)); }
	inline IntPtr_t get__mono_app_domain_1() const { return ____mono_app_domain_1; }
	inline IntPtr_t* get_address_of__mono_app_domain_1() { return &____mono_app_domain_1; }
	inline void set__mono_app_domain_1(IntPtr_t value)
	{
		____mono_app_domain_1 = value;
	}

	inline static int32_t get_offset_of__evidence_6() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ____evidence_6)); }
	inline Evidence_t2017448698 * get__evidence_6() const { return ____evidence_6; }
	inline Evidence_t2017448698 ** get_address_of__evidence_6() { return &____evidence_6; }
	inline void set__evidence_6(Evidence_t2017448698 * value)
	{
		____evidence_6 = value;
		Il2CppCodeGenWriteBarrier((&____evidence_6), value);
	}

	inline static int32_t get_offset_of__granted_7() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ____granted_7)); }
	inline PermissionSet_t1513179846 * get__granted_7() const { return ____granted_7; }
	inline PermissionSet_t1513179846 ** get_address_of__granted_7() { return &____granted_7; }
	inline void set__granted_7(PermissionSet_t1513179846 * value)
	{
		____granted_7 = value;
		Il2CppCodeGenWriteBarrier((&____granted_7), value);
	}

	inline static int32_t get_offset_of__principalPolicy_8() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ____principalPolicy_8)); }
	inline int32_t get__principalPolicy_8() const { return ____principalPolicy_8; }
	inline int32_t* get_address_of__principalPolicy_8() { return &____principalPolicy_8; }
	inline void set__principalPolicy_8(int32_t value)
	{
		____principalPolicy_8 = value;
	}

	inline static int32_t get_offset_of__domain_manager_11() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ____domain_manager_11)); }
	inline AppDomainManager_t4198241168 * get__domain_manager_11() const { return ____domain_manager_11; }
	inline AppDomainManager_t4198241168 ** get_address_of__domain_manager_11() { return &____domain_manager_11; }
	inline void set__domain_manager_11(AppDomainManager_t4198241168 * value)
	{
		____domain_manager_11 = value;
		Il2CppCodeGenWriteBarrier((&____domain_manager_11), value);
	}

	inline static int32_t get_offset_of__activation_12() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ____activation_12)); }
	inline ActivationContext_t2417885991 * get__activation_12() const { return ____activation_12; }
	inline ActivationContext_t2417885991 ** get_address_of__activation_12() { return &____activation_12; }
	inline void set__activation_12(ActivationContext_t2417885991 * value)
	{
		____activation_12 = value;
		Il2CppCodeGenWriteBarrier((&____activation_12), value);
	}

	inline static int32_t get_offset_of__applicationIdentity_13() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ____applicationIdentity_13)); }
	inline ApplicationIdentity_t106405273 * get__applicationIdentity_13() const { return ____applicationIdentity_13; }
	inline ApplicationIdentity_t106405273 ** get_address_of__applicationIdentity_13() { return &____applicationIdentity_13; }
	inline void set__applicationIdentity_13(ApplicationIdentity_t106405273 * value)
	{
		____applicationIdentity_13 = value;
		Il2CppCodeGenWriteBarrier((&____applicationIdentity_13), value);
	}

	inline static int32_t get_offset_of_AssemblyLoad_14() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ___AssemblyLoad_14)); }
	inline AssemblyLoadEventHandler_t3668403773 * get_AssemblyLoad_14() const { return ___AssemblyLoad_14; }
	inline AssemblyLoadEventHandler_t3668403773 ** get_address_of_AssemblyLoad_14() { return &___AssemblyLoad_14; }
	inline void set_AssemblyLoad_14(AssemblyLoadEventHandler_t3668403773 * value)
	{
		___AssemblyLoad_14 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyLoad_14), value);
	}

	inline static int32_t get_offset_of_AssemblyResolve_15() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ___AssemblyResolve_15)); }
	inline ResolveEventHandler_t548862186 * get_AssemblyResolve_15() const { return ___AssemblyResolve_15; }
	inline ResolveEventHandler_t548862186 ** get_address_of_AssemblyResolve_15() { return &___AssemblyResolve_15; }
	inline void set_AssemblyResolve_15(ResolveEventHandler_t548862186 * value)
	{
		___AssemblyResolve_15 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyResolve_15), value);
	}

	inline static int32_t get_offset_of_DomainUnload_16() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ___DomainUnload_16)); }
	inline EventHandler_t1537570410 * get_DomainUnload_16() const { return ___DomainUnload_16; }
	inline EventHandler_t1537570410 ** get_address_of_DomainUnload_16() { return &___DomainUnload_16; }
	inline void set_DomainUnload_16(EventHandler_t1537570410 * value)
	{
		___DomainUnload_16 = value;
		Il2CppCodeGenWriteBarrier((&___DomainUnload_16), value);
	}

	inline static int32_t get_offset_of_ProcessExit_17() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ___ProcessExit_17)); }
	inline EventHandler_t1537570410 * get_ProcessExit_17() const { return ___ProcessExit_17; }
	inline EventHandler_t1537570410 ** get_address_of_ProcessExit_17() { return &___ProcessExit_17; }
	inline void set_ProcessExit_17(EventHandler_t1537570410 * value)
	{
		___ProcessExit_17 = value;
		Il2CppCodeGenWriteBarrier((&___ProcessExit_17), value);
	}

	inline static int32_t get_offset_of_ResourceResolve_18() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ___ResourceResolve_18)); }
	inline ResolveEventHandler_t548862186 * get_ResourceResolve_18() const { return ___ResourceResolve_18; }
	inline ResolveEventHandler_t548862186 ** get_address_of_ResourceResolve_18() { return &___ResourceResolve_18; }
	inline void set_ResourceResolve_18(ResolveEventHandler_t548862186 * value)
	{
		___ResourceResolve_18 = value;
		Il2CppCodeGenWriteBarrier((&___ResourceResolve_18), value);
	}

	inline static int32_t get_offset_of_TypeResolve_19() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ___TypeResolve_19)); }
	inline ResolveEventHandler_t548862186 * get_TypeResolve_19() const { return ___TypeResolve_19; }
	inline ResolveEventHandler_t548862186 ** get_address_of_TypeResolve_19() { return &___TypeResolve_19; }
	inline void set_TypeResolve_19(ResolveEventHandler_t548862186 * value)
	{
		___TypeResolve_19 = value;
		Il2CppCodeGenWriteBarrier((&___TypeResolve_19), value);
	}

	inline static int32_t get_offset_of_UnhandledException_20() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ___UnhandledException_20)); }
	inline UnhandledExceptionEventHandler_t3425235907 * get_UnhandledException_20() const { return ___UnhandledException_20; }
	inline UnhandledExceptionEventHandler_t3425235907 ** get_address_of_UnhandledException_20() { return &___UnhandledException_20; }
	inline void set_UnhandledException_20(UnhandledExceptionEventHandler_t3425235907 * value)
	{
		___UnhandledException_20 = value;
		Il2CppCodeGenWriteBarrier((&___UnhandledException_20), value);
	}

	inline static int32_t get_offset_of_ReflectionOnlyAssemblyResolve_21() { return static_cast<int32_t>(offsetof(AppDomain_t794471768, ___ReflectionOnlyAssemblyResolve_21)); }
	inline ResolveEventHandler_t548862186 * get_ReflectionOnlyAssemblyResolve_21() const { return ___ReflectionOnlyAssemblyResolve_21; }
	inline ResolveEventHandler_t548862186 ** get_address_of_ReflectionOnlyAssemblyResolve_21() { return &___ReflectionOnlyAssemblyResolve_21; }
	inline void set_ReflectionOnlyAssemblyResolve_21(ResolveEventHandler_t548862186 * value)
	{
		___ReflectionOnlyAssemblyResolve_21 = value;
		Il2CppCodeGenWriteBarrier((&___ReflectionOnlyAssemblyResolve_21), value);
	}
};

struct AppDomain_t794471768_StaticFields
{
public:
	// System.String System.AppDomain::_process_guid
	String_t* ____process_guid_2;
	// System.AppDomain System.AppDomain::default_domain
	AppDomain_t794471768 * ___default_domain_10;

public:
	inline static int32_t get_offset_of__process_guid_2() { return static_cast<int32_t>(offsetof(AppDomain_t794471768_StaticFields, ____process_guid_2)); }
	inline String_t* get__process_guid_2() const { return ____process_guid_2; }
	inline String_t** get_address_of__process_guid_2() { return &____process_guid_2; }
	inline void set__process_guid_2(String_t* value)
	{
		____process_guid_2 = value;
		Il2CppCodeGenWriteBarrier((&____process_guid_2), value);
	}

	inline static int32_t get_offset_of_default_domain_10() { return static_cast<int32_t>(offsetof(AppDomain_t794471768_StaticFields, ___default_domain_10)); }
	inline AppDomain_t794471768 * get_default_domain_10() const { return ___default_domain_10; }
	inline AppDomain_t794471768 ** get_address_of_default_domain_10() { return &___default_domain_10; }
	inline void set_default_domain_10(AppDomain_t794471768 * value)
	{
		___default_domain_10 = value;
		Il2CppCodeGenWriteBarrier((&___default_domain_10), value);
	}
};

struct AppDomain_t794471768_ThreadStaticFields
{
public:
	// System.Collections.Hashtable System.AppDomain::type_resolve_in_progress
	Hashtable_t42495838 * ___type_resolve_in_progress_3;
	// System.Collections.Hashtable System.AppDomain::assembly_resolve_in_progress
	Hashtable_t42495838 * ___assembly_resolve_in_progress_4;
	// System.Collections.Hashtable System.AppDomain::assembly_resolve_in_progress_refonly
	Hashtable_t42495838 * ___assembly_resolve_in_progress_refonly_5;
	// System.Security.Principal.IPrincipal System.AppDomain::_principal
	RuntimeObject* ____principal_9;

public:
	inline static int32_t get_offset_of_type_resolve_in_progress_3() { return static_cast<int32_t>(offsetof(AppDomain_t794471768_ThreadStaticFields, ___type_resolve_in_progress_3)); }
	inline Hashtable_t42495838 * get_type_resolve_in_progress_3() const { return ___type_resolve_in_progress_3; }
	inline Hashtable_t42495838 ** get_address_of_type_resolve_in_progress_3() { return &___type_resolve_in_progress_3; }
	inline void set_type_resolve_in_progress_3(Hashtable_t42495838 * value)
	{
		___type_resolve_in_progress_3 = value;
		Il2CppCodeGenWriteBarrier((&___type_resolve_in_progress_3), value);
	}

	inline static int32_t get_offset_of_assembly_resolve_in_progress_4() { return static_cast<int32_t>(offsetof(AppDomain_t794471768_ThreadStaticFields, ___assembly_resolve_in_progress_4)); }
	inline Hashtable_t42495838 * get_assembly_resolve_in_progress_4() const { return ___assembly_resolve_in_progress_4; }
	inline Hashtable_t42495838 ** get_address_of_assembly_resolve_in_progress_4() { return &___assembly_resolve_in_progress_4; }
	inline void set_assembly_resolve_in_progress_4(Hashtable_t42495838 * value)
	{
		___assembly_resolve_in_progress_4 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_resolve_in_progress_4), value);
	}

	inline static int32_t get_offset_of_assembly_resolve_in_progress_refonly_5() { return static_cast<int32_t>(offsetof(AppDomain_t794471768_ThreadStaticFields, ___assembly_resolve_in_progress_refonly_5)); }
	inline Hashtable_t42495838 * get_assembly_resolve_in_progress_refonly_5() const { return ___assembly_resolve_in_progress_refonly_5; }
	inline Hashtable_t42495838 ** get_address_of_assembly_resolve_in_progress_refonly_5() { return &___assembly_resolve_in_progress_refonly_5; }
	inline void set_assembly_resolve_in_progress_refonly_5(Hashtable_t42495838 * value)
	{
		___assembly_resolve_in_progress_refonly_5 = value;
		Il2CppCodeGenWriteBarrier((&___assembly_resolve_in_progress_refonly_5), value);
	}

	inline static int32_t get_offset_of__principal_9() { return static_cast<int32_t>(offsetof(AppDomain_t794471768_ThreadStaticFields, ____principal_9)); }
	inline RuntimeObject* get__principal_9() const { return ____principal_9; }
	inline RuntimeObject** get_address_of__principal_9() { return &____principal_9; }
	inline void set__principal_9(RuntimeObject* value)
	{
		____principal_9 = value;
		Il2CppCodeGenWriteBarrier((&____principal_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPDOMAIN_T794471768_H
#ifndef GAMEOBJECT_T3093992626_H
#define GAMEOBJECT_T3093992626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GameObject
struct  GameObject_t3093992626  : public Object_t350248726
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECT_T3093992626_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t2719897271  ____impl_1;

public:
	inline static int32_t get_offset_of__impl_1() { return static_cast<int32_t>(offsetof(Type_t, ____impl_1)); }
	inline RuntimeTypeHandle_t2719897271  get__impl_1() const { return ____impl_1; }
	inline RuntimeTypeHandle_t2719897271 * get_address_of__impl_1() { return &____impl_1; }
	inline void set__impl_1(RuntimeTypeHandle_t2719897271  value)
	{
		____impl_1 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_2;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t2532561753* ___EmptyTypes_3;
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t277763270 * ___FilterAttribute_4;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t277763270 * ___FilterName_5;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t277763270 * ___FilterNameIgnoreCase_6;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_7;

public:
	inline static int32_t get_offset_of_Delimiter_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_2)); }
	inline Il2CppChar get_Delimiter_2() const { return ___Delimiter_2; }
	inline Il2CppChar* get_address_of_Delimiter_2() { return &___Delimiter_2; }
	inline void set_Delimiter_2(Il2CppChar value)
	{
		___Delimiter_2 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_3)); }
	inline TypeU5BU5D_t2532561753* get_EmptyTypes_3() const { return ___EmptyTypes_3; }
	inline TypeU5BU5D_t2532561753** get_address_of_EmptyTypes_3() { return &___EmptyTypes_3; }
	inline void set_EmptyTypes_3(TypeU5BU5D_t2532561753* value)
	{
		___EmptyTypes_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_3), value);
	}

	inline static int32_t get_offset_of_FilterAttribute_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_4)); }
	inline MemberFilter_t277763270 * get_FilterAttribute_4() const { return ___FilterAttribute_4; }
	inline MemberFilter_t277763270 ** get_address_of_FilterAttribute_4() { return &___FilterAttribute_4; }
	inline void set_FilterAttribute_4(MemberFilter_t277763270 * value)
	{
		___FilterAttribute_4 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_4), value);
	}

	inline static int32_t get_offset_of_FilterName_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_5)); }
	inline MemberFilter_t277763270 * get_FilterName_5() const { return ___FilterName_5; }
	inline MemberFilter_t277763270 ** get_address_of_FilterName_5() { return &___FilterName_5; }
	inline void set_FilterName_5(MemberFilter_t277763270 * value)
	{
		___FilterName_5 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_5), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_6)); }
	inline MemberFilter_t277763270 * get_FilterNameIgnoreCase_6() const { return ___FilterNameIgnoreCase_6; }
	inline MemberFilter_t277763270 ** get_address_of_FilterNameIgnoreCase_6() { return &___FilterNameIgnoreCase_6; }
	inline void set_FilterNameIgnoreCase_6(MemberFilter_t277763270 * value)
	{
		___FilterNameIgnoreCase_6 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_6), value);
	}

	inline static int32_t get_offset_of_Missing_7() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_7)); }
	inline RuntimeObject * get_Missing_7() const { return ___Missing_7; }
	inline RuntimeObject ** get_address_of_Missing_7() { return &___Missing_7; }
	inline void set_Missing_7(RuntimeObject * value)
	{
		___Missing_7 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef TEXTURE_T4046637001_H
#define TEXTURE_T4046637001_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t4046637001  : public Object_t350248726
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T4046637001_H
#ifndef STREAMINGCONTEXT_T4227869630_H
#define STREAMINGCONTEXT_T4227869630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t4227869630 
{
public:
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::state
	int32_t ___state_0;
	// System.Object System.Runtime.Serialization.StreamingContext::additional
	RuntimeObject * ___additional_1;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(StreamingContext_t4227869630, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}

	inline static int32_t get_offset_of_additional_1() { return static_cast<int32_t>(offsetof(StreamingContext_t4227869630, ___additional_1)); }
	inline RuntimeObject * get_additional_1() const { return ___additional_1; }
	inline RuntimeObject ** get_address_of_additional_1() { return &___additional_1; }
	inline void set_additional_1(RuntimeObject * value)
	{
		___additional_1 = value;
		Il2CppCodeGenWriteBarrier((&___additional_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t4227869630_marshaled_pinvoke
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t4227869630_marshaled_com
{
	int32_t ___state_0;
	Il2CppIUnknown* ___additional_1;
};
#endif // STREAMINGCONTEXT_T4227869630_H
#ifndef TOUCH_T3995534465_H
#define TOUCH_T3995534465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t3995534465 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t1065050092  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t1065050092  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t1065050092  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_Position_1)); }
	inline Vector2_t1065050092  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t1065050092 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t1065050092  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_RawPosition_2)); }
	inline Vector2_t1065050092  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t1065050092 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t1065050092  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_PositionDelta_3)); }
	inline Vector2_t1065050092  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t1065050092 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t1065050092  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t3995534465, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T3995534465_H
#ifndef EVENTWAITHANDLE_T2896330895_H
#define EVENTWAITHANDLE_T2896330895_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.EventWaitHandle
struct  EventWaitHandle_t2896330895  : public WaitHandle_t3740277255
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTWAITHANDLE_T2896330895_H
#ifndef FONT_T1312336880_H
#define FONT_T1312336880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Font
struct  Font_t1312336880  : public Object_t350248726
{
public:
	// UnityEngine.Font/FontTextureRebuildCallback UnityEngine.Font::m_FontTextureRebuildCallback
	FontTextureRebuildCallback_t3954027152 * ___m_FontTextureRebuildCallback_3;

public:
	inline static int32_t get_offset_of_m_FontTextureRebuildCallback_3() { return static_cast<int32_t>(offsetof(Font_t1312336880, ___m_FontTextureRebuildCallback_3)); }
	inline FontTextureRebuildCallback_t3954027152 * get_m_FontTextureRebuildCallback_3() const { return ___m_FontTextureRebuildCallback_3; }
	inline FontTextureRebuildCallback_t3954027152 ** get_address_of_m_FontTextureRebuildCallback_3() { return &___m_FontTextureRebuildCallback_3; }
	inline void set_m_FontTextureRebuildCallback_3(FontTextureRebuildCallback_t3954027152 * value)
	{
		___m_FontTextureRebuildCallback_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_FontTextureRebuildCallback_3), value);
	}
};

struct Font_t1312336880_StaticFields
{
public:
	// System.Action`1<UnityEngine.Font> UnityEngine.Font::textureRebuilt
	Action_1_t1495770923 * ___textureRebuilt_2;

public:
	inline static int32_t get_offset_of_textureRebuilt_2() { return static_cast<int32_t>(offsetof(Font_t1312336880_StaticFields, ___textureRebuilt_2)); }
	inline Action_1_t1495770923 * get_textureRebuilt_2() const { return ___textureRebuilt_2; }
	inline Action_1_t1495770923 ** get_address_of_textureRebuilt_2() { return &___textureRebuilt_2; }
	inline void set_textureRebuilt_2(Action_1_t1495770923 * value)
	{
		___textureRebuilt_2 = value;
		Il2CppCodeGenWriteBarrier((&___textureRebuilt_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FONT_T1312336880_H
#ifndef SPRITEATLAS_T2050322510_H
#define SPRITEATLAS_T2050322510_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.U2D.SpriteAtlas
struct  SpriteAtlas_t2050322510  : public Object_t350248726
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEATLAS_T2050322510_H
#ifndef THREAD_T2629334981_H
#define THREAD_T2629334981_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.Thread
struct  Thread_t2629334981  : public CriticalFinalizerObject_t1170206086
{
public:
	// System.Int32 System.Threading.Thread::lock_thread_id
	int32_t ___lock_thread_id_0;
	// System.IntPtr System.Threading.Thread::system_thread_handle
	IntPtr_t ___system_thread_handle_1;
	// System.Object System.Threading.Thread::cached_culture_info
	RuntimeObject * ___cached_culture_info_2;
	// System.IntPtr System.Threading.Thread::unused0
	IntPtr_t ___unused0_3;
	// System.Boolean System.Threading.Thread::threadpool_thread
	bool ___threadpool_thread_4;
	// System.IntPtr System.Threading.Thread::name
	IntPtr_t ___name_5;
	// System.Int32 System.Threading.Thread::name_len
	int32_t ___name_len_6;
	// System.Threading.ThreadState System.Threading.Thread::state
	int32_t ___state_7;
	// System.Object System.Threading.Thread::abort_exc
	RuntimeObject * ___abort_exc_8;
	// System.Int32 System.Threading.Thread::abort_state_handle
	int32_t ___abort_state_handle_9;
	// System.Int64 System.Threading.Thread::thread_id
	int64_t ___thread_id_10;
	// System.IntPtr System.Threading.Thread::start_notify
	IntPtr_t ___start_notify_11;
	// System.IntPtr System.Threading.Thread::stack_ptr
	IntPtr_t ___stack_ptr_12;
	// System.UIntPtr System.Threading.Thread::static_data
	UIntPtr_t  ___static_data_13;
	// System.IntPtr System.Threading.Thread::jit_data
	IntPtr_t ___jit_data_14;
	// System.IntPtr System.Threading.Thread::lock_data
	IntPtr_t ___lock_data_15;
	// System.Object System.Threading.Thread::current_appcontext
	RuntimeObject * ___current_appcontext_16;
	// System.Int32 System.Threading.Thread::stack_size
	int32_t ___stack_size_17;
	// System.Object System.Threading.Thread::start_obj
	RuntimeObject * ___start_obj_18;
	// System.IntPtr System.Threading.Thread::appdomain_refs
	IntPtr_t ___appdomain_refs_19;
	// System.Int32 System.Threading.Thread::interruption_requested
	int32_t ___interruption_requested_20;
	// System.IntPtr System.Threading.Thread::suspend_event
	IntPtr_t ___suspend_event_21;
	// System.IntPtr System.Threading.Thread::suspended_event
	IntPtr_t ___suspended_event_22;
	// System.IntPtr System.Threading.Thread::resume_event
	IntPtr_t ___resume_event_23;
	// System.IntPtr System.Threading.Thread::synch_cs
	IntPtr_t ___synch_cs_24;
	// System.IntPtr System.Threading.Thread::serialized_culture_info
	IntPtr_t ___serialized_culture_info_25;
	// System.Int32 System.Threading.Thread::serialized_culture_info_len
	int32_t ___serialized_culture_info_len_26;
	// System.IntPtr System.Threading.Thread::serialized_ui_culture_info
	IntPtr_t ___serialized_ui_culture_info_27;
	// System.Int32 System.Threading.Thread::serialized_ui_culture_info_len
	int32_t ___serialized_ui_culture_info_len_28;
	// System.Boolean System.Threading.Thread::thread_dump_requested
	bool ___thread_dump_requested_29;
	// System.IntPtr System.Threading.Thread::end_stack
	IntPtr_t ___end_stack_30;
	// System.Boolean System.Threading.Thread::thread_interrupt_requested
	bool ___thread_interrupt_requested_31;
	// System.Byte System.Threading.Thread::apartment_state
	uint8_t ___apartment_state_32;
	// System.Int32 modreq(System.Runtime.CompilerServices.IsVolatile) System.Threading.Thread::critical_region_level
	int32_t ___critical_region_level_33;
	// System.Int32 System.Threading.Thread::small_id
	int32_t ___small_id_34;
	// System.IntPtr System.Threading.Thread::manage_callback
	IntPtr_t ___manage_callback_35;
	// System.Object System.Threading.Thread::pending_exception
	RuntimeObject * ___pending_exception_36;
	// System.Threading.ExecutionContext System.Threading.Thread::ec_to_set
	ExecutionContext_t445209008 * ___ec_to_set_37;
	// System.IntPtr System.Threading.Thread::interrupt_on_stop
	IntPtr_t ___interrupt_on_stop_38;
	// System.IntPtr System.Threading.Thread::unused3
	IntPtr_t ___unused3_39;
	// System.IntPtr System.Threading.Thread::unused4
	IntPtr_t ___unused4_40;
	// System.IntPtr System.Threading.Thread::unused5
	IntPtr_t ___unused5_41;
	// System.IntPtr System.Threading.Thread::unused6
	IntPtr_t ___unused6_42;
	// System.MulticastDelegate System.Threading.Thread::threadstart
	MulticastDelegate_t4207739222 * ___threadstart_45;
	// System.Int32 System.Threading.Thread::managed_id
	int32_t ___managed_id_46;
	// System.Security.Principal.IPrincipal System.Threading.Thread::_principal
	RuntimeObject* ____principal_47;
	// System.Boolean System.Threading.Thread::in_currentculture
	bool ___in_currentculture_50;

public:
	inline static int32_t get_offset_of_lock_thread_id_0() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___lock_thread_id_0)); }
	inline int32_t get_lock_thread_id_0() const { return ___lock_thread_id_0; }
	inline int32_t* get_address_of_lock_thread_id_0() { return &___lock_thread_id_0; }
	inline void set_lock_thread_id_0(int32_t value)
	{
		___lock_thread_id_0 = value;
	}

	inline static int32_t get_offset_of_system_thread_handle_1() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___system_thread_handle_1)); }
	inline IntPtr_t get_system_thread_handle_1() const { return ___system_thread_handle_1; }
	inline IntPtr_t* get_address_of_system_thread_handle_1() { return &___system_thread_handle_1; }
	inline void set_system_thread_handle_1(IntPtr_t value)
	{
		___system_thread_handle_1 = value;
	}

	inline static int32_t get_offset_of_cached_culture_info_2() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___cached_culture_info_2)); }
	inline RuntimeObject * get_cached_culture_info_2() const { return ___cached_culture_info_2; }
	inline RuntimeObject ** get_address_of_cached_culture_info_2() { return &___cached_culture_info_2; }
	inline void set_cached_culture_info_2(RuntimeObject * value)
	{
		___cached_culture_info_2 = value;
		Il2CppCodeGenWriteBarrier((&___cached_culture_info_2), value);
	}

	inline static int32_t get_offset_of_unused0_3() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___unused0_3)); }
	inline IntPtr_t get_unused0_3() const { return ___unused0_3; }
	inline IntPtr_t* get_address_of_unused0_3() { return &___unused0_3; }
	inline void set_unused0_3(IntPtr_t value)
	{
		___unused0_3 = value;
	}

	inline static int32_t get_offset_of_threadpool_thread_4() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___threadpool_thread_4)); }
	inline bool get_threadpool_thread_4() const { return ___threadpool_thread_4; }
	inline bool* get_address_of_threadpool_thread_4() { return &___threadpool_thread_4; }
	inline void set_threadpool_thread_4(bool value)
	{
		___threadpool_thread_4 = value;
	}

	inline static int32_t get_offset_of_name_5() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___name_5)); }
	inline IntPtr_t get_name_5() const { return ___name_5; }
	inline IntPtr_t* get_address_of_name_5() { return &___name_5; }
	inline void set_name_5(IntPtr_t value)
	{
		___name_5 = value;
	}

	inline static int32_t get_offset_of_name_len_6() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___name_len_6)); }
	inline int32_t get_name_len_6() const { return ___name_len_6; }
	inline int32_t* get_address_of_name_len_6() { return &___name_len_6; }
	inline void set_name_len_6(int32_t value)
	{
		___name_len_6 = value;
	}

	inline static int32_t get_offset_of_state_7() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___state_7)); }
	inline int32_t get_state_7() const { return ___state_7; }
	inline int32_t* get_address_of_state_7() { return &___state_7; }
	inline void set_state_7(int32_t value)
	{
		___state_7 = value;
	}

	inline static int32_t get_offset_of_abort_exc_8() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___abort_exc_8)); }
	inline RuntimeObject * get_abort_exc_8() const { return ___abort_exc_8; }
	inline RuntimeObject ** get_address_of_abort_exc_8() { return &___abort_exc_8; }
	inline void set_abort_exc_8(RuntimeObject * value)
	{
		___abort_exc_8 = value;
		Il2CppCodeGenWriteBarrier((&___abort_exc_8), value);
	}

	inline static int32_t get_offset_of_abort_state_handle_9() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___abort_state_handle_9)); }
	inline int32_t get_abort_state_handle_9() const { return ___abort_state_handle_9; }
	inline int32_t* get_address_of_abort_state_handle_9() { return &___abort_state_handle_9; }
	inline void set_abort_state_handle_9(int32_t value)
	{
		___abort_state_handle_9 = value;
	}

	inline static int32_t get_offset_of_thread_id_10() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___thread_id_10)); }
	inline int64_t get_thread_id_10() const { return ___thread_id_10; }
	inline int64_t* get_address_of_thread_id_10() { return &___thread_id_10; }
	inline void set_thread_id_10(int64_t value)
	{
		___thread_id_10 = value;
	}

	inline static int32_t get_offset_of_start_notify_11() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___start_notify_11)); }
	inline IntPtr_t get_start_notify_11() const { return ___start_notify_11; }
	inline IntPtr_t* get_address_of_start_notify_11() { return &___start_notify_11; }
	inline void set_start_notify_11(IntPtr_t value)
	{
		___start_notify_11 = value;
	}

	inline static int32_t get_offset_of_stack_ptr_12() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___stack_ptr_12)); }
	inline IntPtr_t get_stack_ptr_12() const { return ___stack_ptr_12; }
	inline IntPtr_t* get_address_of_stack_ptr_12() { return &___stack_ptr_12; }
	inline void set_stack_ptr_12(IntPtr_t value)
	{
		___stack_ptr_12 = value;
	}

	inline static int32_t get_offset_of_static_data_13() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___static_data_13)); }
	inline UIntPtr_t  get_static_data_13() const { return ___static_data_13; }
	inline UIntPtr_t * get_address_of_static_data_13() { return &___static_data_13; }
	inline void set_static_data_13(UIntPtr_t  value)
	{
		___static_data_13 = value;
	}

	inline static int32_t get_offset_of_jit_data_14() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___jit_data_14)); }
	inline IntPtr_t get_jit_data_14() const { return ___jit_data_14; }
	inline IntPtr_t* get_address_of_jit_data_14() { return &___jit_data_14; }
	inline void set_jit_data_14(IntPtr_t value)
	{
		___jit_data_14 = value;
	}

	inline static int32_t get_offset_of_lock_data_15() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___lock_data_15)); }
	inline IntPtr_t get_lock_data_15() const { return ___lock_data_15; }
	inline IntPtr_t* get_address_of_lock_data_15() { return &___lock_data_15; }
	inline void set_lock_data_15(IntPtr_t value)
	{
		___lock_data_15 = value;
	}

	inline static int32_t get_offset_of_current_appcontext_16() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___current_appcontext_16)); }
	inline RuntimeObject * get_current_appcontext_16() const { return ___current_appcontext_16; }
	inline RuntimeObject ** get_address_of_current_appcontext_16() { return &___current_appcontext_16; }
	inline void set_current_appcontext_16(RuntimeObject * value)
	{
		___current_appcontext_16 = value;
		Il2CppCodeGenWriteBarrier((&___current_appcontext_16), value);
	}

	inline static int32_t get_offset_of_stack_size_17() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___stack_size_17)); }
	inline int32_t get_stack_size_17() const { return ___stack_size_17; }
	inline int32_t* get_address_of_stack_size_17() { return &___stack_size_17; }
	inline void set_stack_size_17(int32_t value)
	{
		___stack_size_17 = value;
	}

	inline static int32_t get_offset_of_start_obj_18() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___start_obj_18)); }
	inline RuntimeObject * get_start_obj_18() const { return ___start_obj_18; }
	inline RuntimeObject ** get_address_of_start_obj_18() { return &___start_obj_18; }
	inline void set_start_obj_18(RuntimeObject * value)
	{
		___start_obj_18 = value;
		Il2CppCodeGenWriteBarrier((&___start_obj_18), value);
	}

	inline static int32_t get_offset_of_appdomain_refs_19() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___appdomain_refs_19)); }
	inline IntPtr_t get_appdomain_refs_19() const { return ___appdomain_refs_19; }
	inline IntPtr_t* get_address_of_appdomain_refs_19() { return &___appdomain_refs_19; }
	inline void set_appdomain_refs_19(IntPtr_t value)
	{
		___appdomain_refs_19 = value;
	}

	inline static int32_t get_offset_of_interruption_requested_20() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___interruption_requested_20)); }
	inline int32_t get_interruption_requested_20() const { return ___interruption_requested_20; }
	inline int32_t* get_address_of_interruption_requested_20() { return &___interruption_requested_20; }
	inline void set_interruption_requested_20(int32_t value)
	{
		___interruption_requested_20 = value;
	}

	inline static int32_t get_offset_of_suspend_event_21() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___suspend_event_21)); }
	inline IntPtr_t get_suspend_event_21() const { return ___suspend_event_21; }
	inline IntPtr_t* get_address_of_suspend_event_21() { return &___suspend_event_21; }
	inline void set_suspend_event_21(IntPtr_t value)
	{
		___suspend_event_21 = value;
	}

	inline static int32_t get_offset_of_suspended_event_22() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___suspended_event_22)); }
	inline IntPtr_t get_suspended_event_22() const { return ___suspended_event_22; }
	inline IntPtr_t* get_address_of_suspended_event_22() { return &___suspended_event_22; }
	inline void set_suspended_event_22(IntPtr_t value)
	{
		___suspended_event_22 = value;
	}

	inline static int32_t get_offset_of_resume_event_23() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___resume_event_23)); }
	inline IntPtr_t get_resume_event_23() const { return ___resume_event_23; }
	inline IntPtr_t* get_address_of_resume_event_23() { return &___resume_event_23; }
	inline void set_resume_event_23(IntPtr_t value)
	{
		___resume_event_23 = value;
	}

	inline static int32_t get_offset_of_synch_cs_24() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___synch_cs_24)); }
	inline IntPtr_t get_synch_cs_24() const { return ___synch_cs_24; }
	inline IntPtr_t* get_address_of_synch_cs_24() { return &___synch_cs_24; }
	inline void set_synch_cs_24(IntPtr_t value)
	{
		___synch_cs_24 = value;
	}

	inline static int32_t get_offset_of_serialized_culture_info_25() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___serialized_culture_info_25)); }
	inline IntPtr_t get_serialized_culture_info_25() const { return ___serialized_culture_info_25; }
	inline IntPtr_t* get_address_of_serialized_culture_info_25() { return &___serialized_culture_info_25; }
	inline void set_serialized_culture_info_25(IntPtr_t value)
	{
		___serialized_culture_info_25 = value;
	}

	inline static int32_t get_offset_of_serialized_culture_info_len_26() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___serialized_culture_info_len_26)); }
	inline int32_t get_serialized_culture_info_len_26() const { return ___serialized_culture_info_len_26; }
	inline int32_t* get_address_of_serialized_culture_info_len_26() { return &___serialized_culture_info_len_26; }
	inline void set_serialized_culture_info_len_26(int32_t value)
	{
		___serialized_culture_info_len_26 = value;
	}

	inline static int32_t get_offset_of_serialized_ui_culture_info_27() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___serialized_ui_culture_info_27)); }
	inline IntPtr_t get_serialized_ui_culture_info_27() const { return ___serialized_ui_culture_info_27; }
	inline IntPtr_t* get_address_of_serialized_ui_culture_info_27() { return &___serialized_ui_culture_info_27; }
	inline void set_serialized_ui_culture_info_27(IntPtr_t value)
	{
		___serialized_ui_culture_info_27 = value;
	}

	inline static int32_t get_offset_of_serialized_ui_culture_info_len_28() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___serialized_ui_culture_info_len_28)); }
	inline int32_t get_serialized_ui_culture_info_len_28() const { return ___serialized_ui_culture_info_len_28; }
	inline int32_t* get_address_of_serialized_ui_culture_info_len_28() { return &___serialized_ui_culture_info_len_28; }
	inline void set_serialized_ui_culture_info_len_28(int32_t value)
	{
		___serialized_ui_culture_info_len_28 = value;
	}

	inline static int32_t get_offset_of_thread_dump_requested_29() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___thread_dump_requested_29)); }
	inline bool get_thread_dump_requested_29() const { return ___thread_dump_requested_29; }
	inline bool* get_address_of_thread_dump_requested_29() { return &___thread_dump_requested_29; }
	inline void set_thread_dump_requested_29(bool value)
	{
		___thread_dump_requested_29 = value;
	}

	inline static int32_t get_offset_of_end_stack_30() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___end_stack_30)); }
	inline IntPtr_t get_end_stack_30() const { return ___end_stack_30; }
	inline IntPtr_t* get_address_of_end_stack_30() { return &___end_stack_30; }
	inline void set_end_stack_30(IntPtr_t value)
	{
		___end_stack_30 = value;
	}

	inline static int32_t get_offset_of_thread_interrupt_requested_31() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___thread_interrupt_requested_31)); }
	inline bool get_thread_interrupt_requested_31() const { return ___thread_interrupt_requested_31; }
	inline bool* get_address_of_thread_interrupt_requested_31() { return &___thread_interrupt_requested_31; }
	inline void set_thread_interrupt_requested_31(bool value)
	{
		___thread_interrupt_requested_31 = value;
	}

	inline static int32_t get_offset_of_apartment_state_32() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___apartment_state_32)); }
	inline uint8_t get_apartment_state_32() const { return ___apartment_state_32; }
	inline uint8_t* get_address_of_apartment_state_32() { return &___apartment_state_32; }
	inline void set_apartment_state_32(uint8_t value)
	{
		___apartment_state_32 = value;
	}

	inline static int32_t get_offset_of_critical_region_level_33() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___critical_region_level_33)); }
	inline int32_t get_critical_region_level_33() const { return ___critical_region_level_33; }
	inline int32_t* get_address_of_critical_region_level_33() { return &___critical_region_level_33; }
	inline void set_critical_region_level_33(int32_t value)
	{
		___critical_region_level_33 = value;
	}

	inline static int32_t get_offset_of_small_id_34() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___small_id_34)); }
	inline int32_t get_small_id_34() const { return ___small_id_34; }
	inline int32_t* get_address_of_small_id_34() { return &___small_id_34; }
	inline void set_small_id_34(int32_t value)
	{
		___small_id_34 = value;
	}

	inline static int32_t get_offset_of_manage_callback_35() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___manage_callback_35)); }
	inline IntPtr_t get_manage_callback_35() const { return ___manage_callback_35; }
	inline IntPtr_t* get_address_of_manage_callback_35() { return &___manage_callback_35; }
	inline void set_manage_callback_35(IntPtr_t value)
	{
		___manage_callback_35 = value;
	}

	inline static int32_t get_offset_of_pending_exception_36() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___pending_exception_36)); }
	inline RuntimeObject * get_pending_exception_36() const { return ___pending_exception_36; }
	inline RuntimeObject ** get_address_of_pending_exception_36() { return &___pending_exception_36; }
	inline void set_pending_exception_36(RuntimeObject * value)
	{
		___pending_exception_36 = value;
		Il2CppCodeGenWriteBarrier((&___pending_exception_36), value);
	}

	inline static int32_t get_offset_of_ec_to_set_37() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___ec_to_set_37)); }
	inline ExecutionContext_t445209008 * get_ec_to_set_37() const { return ___ec_to_set_37; }
	inline ExecutionContext_t445209008 ** get_address_of_ec_to_set_37() { return &___ec_to_set_37; }
	inline void set_ec_to_set_37(ExecutionContext_t445209008 * value)
	{
		___ec_to_set_37 = value;
		Il2CppCodeGenWriteBarrier((&___ec_to_set_37), value);
	}

	inline static int32_t get_offset_of_interrupt_on_stop_38() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___interrupt_on_stop_38)); }
	inline IntPtr_t get_interrupt_on_stop_38() const { return ___interrupt_on_stop_38; }
	inline IntPtr_t* get_address_of_interrupt_on_stop_38() { return &___interrupt_on_stop_38; }
	inline void set_interrupt_on_stop_38(IntPtr_t value)
	{
		___interrupt_on_stop_38 = value;
	}

	inline static int32_t get_offset_of_unused3_39() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___unused3_39)); }
	inline IntPtr_t get_unused3_39() const { return ___unused3_39; }
	inline IntPtr_t* get_address_of_unused3_39() { return &___unused3_39; }
	inline void set_unused3_39(IntPtr_t value)
	{
		___unused3_39 = value;
	}

	inline static int32_t get_offset_of_unused4_40() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___unused4_40)); }
	inline IntPtr_t get_unused4_40() const { return ___unused4_40; }
	inline IntPtr_t* get_address_of_unused4_40() { return &___unused4_40; }
	inline void set_unused4_40(IntPtr_t value)
	{
		___unused4_40 = value;
	}

	inline static int32_t get_offset_of_unused5_41() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___unused5_41)); }
	inline IntPtr_t get_unused5_41() const { return ___unused5_41; }
	inline IntPtr_t* get_address_of_unused5_41() { return &___unused5_41; }
	inline void set_unused5_41(IntPtr_t value)
	{
		___unused5_41 = value;
	}

	inline static int32_t get_offset_of_unused6_42() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___unused6_42)); }
	inline IntPtr_t get_unused6_42() const { return ___unused6_42; }
	inline IntPtr_t* get_address_of_unused6_42() { return &___unused6_42; }
	inline void set_unused6_42(IntPtr_t value)
	{
		___unused6_42 = value;
	}

	inline static int32_t get_offset_of_threadstart_45() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___threadstart_45)); }
	inline MulticastDelegate_t4207739222 * get_threadstart_45() const { return ___threadstart_45; }
	inline MulticastDelegate_t4207739222 ** get_address_of_threadstart_45() { return &___threadstart_45; }
	inline void set_threadstart_45(MulticastDelegate_t4207739222 * value)
	{
		___threadstart_45 = value;
		Il2CppCodeGenWriteBarrier((&___threadstart_45), value);
	}

	inline static int32_t get_offset_of_managed_id_46() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___managed_id_46)); }
	inline int32_t get_managed_id_46() const { return ___managed_id_46; }
	inline int32_t* get_address_of_managed_id_46() { return &___managed_id_46; }
	inline void set_managed_id_46(int32_t value)
	{
		___managed_id_46 = value;
	}

	inline static int32_t get_offset_of__principal_47() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ____principal_47)); }
	inline RuntimeObject* get__principal_47() const { return ____principal_47; }
	inline RuntimeObject** get_address_of__principal_47() { return &____principal_47; }
	inline void set__principal_47(RuntimeObject* value)
	{
		____principal_47 = value;
		Il2CppCodeGenWriteBarrier((&____principal_47), value);
	}

	inline static int32_t get_offset_of_in_currentculture_50() { return static_cast<int32_t>(offsetof(Thread_t2629334981, ___in_currentculture_50)); }
	inline bool get_in_currentculture_50() const { return ___in_currentculture_50; }
	inline bool* get_address_of_in_currentculture_50() { return &___in_currentculture_50; }
	inline void set_in_currentculture_50(bool value)
	{
		___in_currentculture_50 = value;
	}
};

struct Thread_t2629334981_StaticFields
{
public:
	// System.Collections.Hashtable System.Threading.Thread::datastorehash
	Hashtable_t42495838 * ___datastorehash_48;
	// System.Object System.Threading.Thread::datastore_lock
	RuntimeObject * ___datastore_lock_49;
	// System.Object System.Threading.Thread::culture_lock
	RuntimeObject * ___culture_lock_51;

public:
	inline static int32_t get_offset_of_datastorehash_48() { return static_cast<int32_t>(offsetof(Thread_t2629334981_StaticFields, ___datastorehash_48)); }
	inline Hashtable_t42495838 * get_datastorehash_48() const { return ___datastorehash_48; }
	inline Hashtable_t42495838 ** get_address_of_datastorehash_48() { return &___datastorehash_48; }
	inline void set_datastorehash_48(Hashtable_t42495838 * value)
	{
		___datastorehash_48 = value;
		Il2CppCodeGenWriteBarrier((&___datastorehash_48), value);
	}

	inline static int32_t get_offset_of_datastore_lock_49() { return static_cast<int32_t>(offsetof(Thread_t2629334981_StaticFields, ___datastore_lock_49)); }
	inline RuntimeObject * get_datastore_lock_49() const { return ___datastore_lock_49; }
	inline RuntimeObject ** get_address_of_datastore_lock_49() { return &___datastore_lock_49; }
	inline void set_datastore_lock_49(RuntimeObject * value)
	{
		___datastore_lock_49 = value;
		Il2CppCodeGenWriteBarrier((&___datastore_lock_49), value);
	}

	inline static int32_t get_offset_of_culture_lock_51() { return static_cast<int32_t>(offsetof(Thread_t2629334981_StaticFields, ___culture_lock_51)); }
	inline RuntimeObject * get_culture_lock_51() const { return ___culture_lock_51; }
	inline RuntimeObject ** get_address_of_culture_lock_51() { return &___culture_lock_51; }
	inline void set_culture_lock_51(RuntimeObject * value)
	{
		___culture_lock_51 = value;
		Il2CppCodeGenWriteBarrier((&___culture_lock_51), value);
	}
};

struct Thread_t2629334981_ThreadStaticFields
{
public:
	// System.Object[] System.Threading.Thread::local_slots
	ObjectU5BU5D_t1100384052* ___local_slots_43;
	// System.Threading.ExecutionContext System.Threading.Thread::_ec
	ExecutionContext_t445209008 * ____ec_44;

public:
	inline static int32_t get_offset_of_local_slots_43() { return static_cast<int32_t>(offsetof(Thread_t2629334981_ThreadStaticFields, ___local_slots_43)); }
	inline ObjectU5BU5D_t1100384052* get_local_slots_43() const { return ___local_slots_43; }
	inline ObjectU5BU5D_t1100384052** get_address_of_local_slots_43() { return &___local_slots_43; }
	inline void set_local_slots_43(ObjectU5BU5D_t1100384052* value)
	{
		___local_slots_43 = value;
		Il2CppCodeGenWriteBarrier((&___local_slots_43), value);
	}

	inline static int32_t get_offset_of__ec_44() { return static_cast<int32_t>(offsetof(Thread_t2629334981_ThreadStaticFields, ____ec_44)); }
	inline ExecutionContext_t445209008 * get__ec_44() const { return ____ec_44; }
	inline ExecutionContext_t445209008 ** get_address_of__ec_44() { return &____ec_44; }
	inline void set__ec_44(ExecutionContext_t445209008 * value)
	{
		____ec_44 = value;
		Il2CppCodeGenWriteBarrier((&____ec_44), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THREAD_T2629334981_H
#ifndef TEXTGENERATOR_T1842200301_H
#define TEXTGENERATOR_T1842200301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextGenerator
struct  TextGenerator_t1842200301  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.TextGenerator::m_Ptr
	IntPtr_t ___m_Ptr_0;
	// System.String UnityEngine.TextGenerator::m_LastString
	String_t* ___m_LastString_1;
	// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::m_LastSettings
	TextGenerationSettings_t781948148  ___m_LastSettings_2;
	// System.Boolean UnityEngine.TextGenerator::m_HasGenerated
	bool ___m_HasGenerated_3;
	// UnityEngine.TextGenerationError UnityEngine.TextGenerator::m_LastValid
	int32_t ___m_LastValid_4;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::m_Verts
	List_1_t2598460513 * ___m_Verts_5;
	// System.Collections.Generic.List`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::m_Characters
	List_1_t1278937086 * ___m_Characters_6;
	// System.Collections.Generic.List`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::m_Lines
	List_1_t1960923134 * ___m_Lines_7;
	// System.Boolean UnityEngine.TextGenerator::m_CachedVerts
	bool ___m_CachedVerts_8;
	// System.Boolean UnityEngine.TextGenerator::m_CachedCharacters
	bool ___m_CachedCharacters_9;
	// System.Boolean UnityEngine.TextGenerator::m_CachedLines
	bool ___m_CachedLines_10;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(TextGenerator_t1842200301, ___m_Ptr_0)); }
	inline IntPtr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline IntPtr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(IntPtr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_LastString_1() { return static_cast<int32_t>(offsetof(TextGenerator_t1842200301, ___m_LastString_1)); }
	inline String_t* get_m_LastString_1() const { return ___m_LastString_1; }
	inline String_t** get_address_of_m_LastString_1() { return &___m_LastString_1; }
	inline void set_m_LastString_1(String_t* value)
	{
		___m_LastString_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_LastString_1), value);
	}

	inline static int32_t get_offset_of_m_LastSettings_2() { return static_cast<int32_t>(offsetof(TextGenerator_t1842200301, ___m_LastSettings_2)); }
	inline TextGenerationSettings_t781948148  get_m_LastSettings_2() const { return ___m_LastSettings_2; }
	inline TextGenerationSettings_t781948148 * get_address_of_m_LastSettings_2() { return &___m_LastSettings_2; }
	inline void set_m_LastSettings_2(TextGenerationSettings_t781948148  value)
	{
		___m_LastSettings_2 = value;
	}

	inline static int32_t get_offset_of_m_HasGenerated_3() { return static_cast<int32_t>(offsetof(TextGenerator_t1842200301, ___m_HasGenerated_3)); }
	inline bool get_m_HasGenerated_3() const { return ___m_HasGenerated_3; }
	inline bool* get_address_of_m_HasGenerated_3() { return &___m_HasGenerated_3; }
	inline void set_m_HasGenerated_3(bool value)
	{
		___m_HasGenerated_3 = value;
	}

	inline static int32_t get_offset_of_m_LastValid_4() { return static_cast<int32_t>(offsetof(TextGenerator_t1842200301, ___m_LastValid_4)); }
	inline int32_t get_m_LastValid_4() const { return ___m_LastValid_4; }
	inline int32_t* get_address_of_m_LastValid_4() { return &___m_LastValid_4; }
	inline void set_m_LastValid_4(int32_t value)
	{
		___m_LastValid_4 = value;
	}

	inline static int32_t get_offset_of_m_Verts_5() { return static_cast<int32_t>(offsetof(TextGenerator_t1842200301, ___m_Verts_5)); }
	inline List_1_t2598460513 * get_m_Verts_5() const { return ___m_Verts_5; }
	inline List_1_t2598460513 ** get_address_of_m_Verts_5() { return &___m_Verts_5; }
	inline void set_m_Verts_5(List_1_t2598460513 * value)
	{
		___m_Verts_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Verts_5), value);
	}

	inline static int32_t get_offset_of_m_Characters_6() { return static_cast<int32_t>(offsetof(TextGenerator_t1842200301, ___m_Characters_6)); }
	inline List_1_t1278937086 * get_m_Characters_6() const { return ___m_Characters_6; }
	inline List_1_t1278937086 ** get_address_of_m_Characters_6() { return &___m_Characters_6; }
	inline void set_m_Characters_6(List_1_t1278937086 * value)
	{
		___m_Characters_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Characters_6), value);
	}

	inline static int32_t get_offset_of_m_Lines_7() { return static_cast<int32_t>(offsetof(TextGenerator_t1842200301, ___m_Lines_7)); }
	inline List_1_t1960923134 * get_m_Lines_7() const { return ___m_Lines_7; }
	inline List_1_t1960923134 ** get_address_of_m_Lines_7() { return &___m_Lines_7; }
	inline void set_m_Lines_7(List_1_t1960923134 * value)
	{
		___m_Lines_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_Lines_7), value);
	}

	inline static int32_t get_offset_of_m_CachedVerts_8() { return static_cast<int32_t>(offsetof(TextGenerator_t1842200301, ___m_CachedVerts_8)); }
	inline bool get_m_CachedVerts_8() const { return ___m_CachedVerts_8; }
	inline bool* get_address_of_m_CachedVerts_8() { return &___m_CachedVerts_8; }
	inline void set_m_CachedVerts_8(bool value)
	{
		___m_CachedVerts_8 = value;
	}

	inline static int32_t get_offset_of_m_CachedCharacters_9() { return static_cast<int32_t>(offsetof(TextGenerator_t1842200301, ___m_CachedCharacters_9)); }
	inline bool get_m_CachedCharacters_9() const { return ___m_CachedCharacters_9; }
	inline bool* get_address_of_m_CachedCharacters_9() { return &___m_CachedCharacters_9; }
	inline void set_m_CachedCharacters_9(bool value)
	{
		___m_CachedCharacters_9 = value;
	}

	inline static int32_t get_offset_of_m_CachedLines_10() { return static_cast<int32_t>(offsetof(TextGenerator_t1842200301, ___m_CachedLines_10)); }
	inline bool get_m_CachedLines_10() const { return ___m_CachedLines_10; }
	inline bool* get_address_of_m_CachedLines_10() { return &___m_CachedLines_10; }
	inline void set_m_CachedLines_10(bool value)
	{
		___m_CachedLines_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.TextGenerator
struct TextGenerator_t1842200301_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	char* ___m_LastString_1;
	TextGenerationSettings_t781948148_marshaled_pinvoke ___m_LastSettings_2;
	int32_t ___m_HasGenerated_3;
	int32_t ___m_LastValid_4;
	List_1_t2598460513 * ___m_Verts_5;
	List_1_t1278937086 * ___m_Characters_6;
	List_1_t1960923134 * ___m_Lines_7;
	int32_t ___m_CachedVerts_8;
	int32_t ___m_CachedCharacters_9;
	int32_t ___m_CachedLines_10;
};
// Native definition for COM marshalling of UnityEngine.TextGenerator
struct TextGenerator_t1842200301_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppChar* ___m_LastString_1;
	TextGenerationSettings_t781948148_marshaled_com ___m_LastSettings_2;
	int32_t ___m_HasGenerated_3;
	int32_t ___m_LastValid_4;
	List_1_t2598460513 * ___m_Verts_5;
	List_1_t1278937086 * ___m_Characters_6;
	List_1_t1960923134 * ___m_Lines_7;
	int32_t ___m_CachedVerts_8;
	int32_t ___m_CachedCharacters_9;
	int32_t ___m_CachedLines_10;
};
#endif // TEXTGENERATOR_T1842200301_H
#ifndef STATEMACHINEBEHAVIOUR_T891337786_H
#define STATEMACHINEBEHAVIOUR_T891337786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.StateMachineBehaviour
struct  StateMachineBehaviour_t891337786  : public ScriptableObject_t229319923
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMACHINEBEHAVIOUR_T891337786_H
#ifndef UNHANDLEDEXCEPTIONEVENTHANDLER_T3425235907_H
#define UNHANDLEDEXCEPTIONEVENTHANDLER_T3425235907_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UnhandledExceptionEventHandler
struct  UnhandledExceptionEventHandler_t3425235907  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNHANDLEDEXCEPTIONEVENTHANDLER_T3425235907_H
#ifndef ACTION_1_T2233756553_H
#define ACTION_1_T2233756553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<UnityEngine.U2D.SpriteAtlas>
struct  Action_1_t2233756553  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T2233756553_H
#ifndef REQUESTATLASCALLBACK_T2148362272_H
#define REQUESTATLASCALLBACK_T2148362272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback
struct  RequestAtlasCallback_t2148362272  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUESTATLASCALLBACK_T2148362272_H
#ifndef SENDORPOSTCALLBACK_T3197245846_H
#define SENDORPOSTCALLBACK_T3197245846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SendOrPostCallback
struct  SendOrPostCallback_t3197245846  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SENDORPOSTCALLBACK_T3197245846_H
#ifndef TRANSFORM_T2735953680_H
#define TRANSFORM_T2735953680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Transform
struct  Transform_t2735953680  : public Component_t1852985663
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORM_T2735953680_H
#ifndef TEXTURE2D_T1431210461_H
#define TEXTURE2D_T1431210461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t1431210461  : public Texture_t4046637001
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T1431210461_H
#ifndef BEHAVIOUR_T2905267502_H
#define BEHAVIOUR_T2905267502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2905267502  : public Component_t1852985663
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2905267502_H
#ifndef MANUALRESETEVENT_T562888503_H
#define MANUALRESETEVENT_T562888503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.ManualResetEvent
struct  ManualResetEvent_t562888503  : public EventWaitHandle_t2896330895
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANUALRESETEVENT_T562888503_H
#ifndef ASYNCCALLBACK_T3972436406_H
#define ASYNCCALLBACK_T3972436406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3972436406  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3972436406_H
#ifndef RECTTRANSFORM_T1087788885_H
#define RECTTRANSFORM_T1087788885_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectTransform
struct  RectTransform_t1087788885  : public Transform_t2735953680
{
public:

public:
};

struct RectTransform_t1087788885_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t3700320170 * ___reapplyDrivenProperties_2;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_2() { return static_cast<int32_t>(offsetof(RectTransform_t1087788885_StaticFields, ___reapplyDrivenProperties_2)); }
	inline ReapplyDrivenProperties_t3700320170 * get_reapplyDrivenProperties_2() const { return ___reapplyDrivenProperties_2; }
	inline ReapplyDrivenProperties_t3700320170 ** get_address_of_reapplyDrivenProperties_2() { return &___reapplyDrivenProperties_2; }
	inline void set_reapplyDrivenProperties_2(ReapplyDrivenProperties_t3700320170 * value)
	{
		___reapplyDrivenProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___reapplyDrivenProperties_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTTRANSFORM_T1087788885_H
#ifndef ANIMATOR_T126418246_H
#define ANIMATOR_T126418246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Animator
struct  Animator_t126418246  : public Behaviour_t2905267502
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATOR_T126418246_H
// System.Byte[]
struct ByteU5BU5D_t371269159  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_t1100384052  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t349694303  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Color_t4183816058  m_Items[1];

public:
	inline Color_t4183816058  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t4183816058 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t4183816058  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t4183816058  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t4183816058 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t4183816058  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Rect[]
struct RectU5BU5D_t75527478  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Rect_t82402607  m_Items[1];

public:
	inline Rect_t82402607  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Rect_t82402607 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Rect_t82402607  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Rect_t82402607  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Rect_t82402607 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Rect_t82402607  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Texture2D[]
struct Texture2DU5BU5D_t2067778064  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Texture2D_t1431210461 * m_Items[1];

public:
	inline Texture2D_t1431210461 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Texture2D_t1431210461 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Texture2D_t1431210461 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Texture2D_t1431210461 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Texture2D_t1431210461 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Texture2D_t1431210461 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};

extern "C" void TextGenerationSettings_t781948148_marshal_pinvoke(const TextGenerationSettings_t781948148& unmarshaled, TextGenerationSettings_t781948148_marshaled_pinvoke& marshaled);
extern "C" void TextGenerationSettings_t781948148_marshal_pinvoke_back(const TextGenerationSettings_t781948148_marshaled_pinvoke& marshaled, TextGenerationSettings_t781948148& unmarshaled);
extern "C" void TextGenerationSettings_t781948148_marshal_pinvoke_cleanup(TextGenerationSettings_t781948148_marshaled_pinvoke& marshaled);
extern "C" void TextGenerationSettings_t781948148_marshal_com(const TextGenerationSettings_t781948148& unmarshaled, TextGenerationSettings_t781948148_marshaled_com& marshaled);
extern "C" void TextGenerationSettings_t781948148_marshal_com_back(const TextGenerationSettings_t781948148_marshaled_com& marshaled, TextGenerationSettings_t781948148& unmarshaled);
extern "C" void TextGenerationSettings_t781948148_marshal_com_cleanup(TextGenerationSettings_t781948148_marshaled_com& marshaled);

// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2708340509_gshared (List_1_t2598460513 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m2803144634_gshared (List_1_t1278937086 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Int32)
extern "C"  void List_1__ctor_m543064086_gshared (List_1_t1960923134 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Action_1__ctor_m1715545874_gshared (Action_1_t2925337540 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::.ctor(System.Int32)
extern "C"  void Queue_1__ctor_m2523417422_gshared (Queue_1_t2108291272 * __this, int32_t p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::Dequeue()
extern "C"  WorkRequest_t2264216812  Queue_1_Dequeue_m3333247629_gshared (Queue_1_t2108291272 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::get_Count()
extern "C"  int32_t Queue_1_get_Count_m4197321750_gshared (Queue_1_t2108291272 * __this, const RuntimeMethod* method);

// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C"  void ScriptableObject__ctor_m3459841701 (ScriptableObject_t229319923 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PropertyAttribute::.ctor()
extern "C"  void PropertyAttribute__ctor_m824268376 (PropertyAttribute_t1226012101 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.TextAsset::get_text()
extern "C"  String_t* TextAsset_get_text_m2651831433 (TextAsset_t3707725843 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GUIStyle UnityEngine.GUIStyle::get_none()
extern "C"  GUIStyle_t2201526215 * GUIStyle_get_none_m1197593709 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t1065050092  Vector2_get_zero_m644371544 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.GUIContent::.ctor()
extern "C"  void GUIContent__ctor_m705666553 (GUIContent_t1852996497 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m1723520498 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern "C"  bool Mathf_Approximately_m1817064303 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool TextGenerationSettings_CompareColors_m2069958116 (TextGenerationSettings_t781948148 * __this, Color_t4183816058  ___left0, Color_t4183816058  ___right1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool TextGenerationSettings_CompareVector2_m2250474453 (TextGenerationSettings_t781948148 * __this, Vector2_t1065050092  ___left0, Vector2_t1065050092  ___right1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3076063157 (RuntimeObject * __this /* static, unused */, Object_t350248726 * ___x0, Object_t350248726 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern "C"  bool TextGenerationSettings_Equals_m4120249280 (TextGenerationSettings_t781948148 * __this, TextGenerationSettings_t781948148  ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextGenerator::.ctor(System.Int32)
extern "C"  void TextGenerator__ctor_m3077917799 (TextGenerator_t1842200301 * __this, int32_t ___initialCapacity0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
#define List_1__ctor_m2708340509(__this, p0, method) ((  void (*) (List_1_t2598460513 *, int32_t, const RuntimeMethod*))List_1__ctor_m2708340509_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Int32)
#define List_1__ctor_m2803144634(__this, p0, method) ((  void (*) (List_1_t1278937086 *, int32_t, const RuntimeMethod*))List_1__ctor_m2803144634_gshared)(__this, p0, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UILineInfo>::.ctor(System.Int32)
#define List_1__ctor_m543064086(__this, p0, method) ((  void (*) (List_1_t1960923134 *, int32_t, const RuntimeMethod*))List_1__ctor_m543064086_gshared)(__this, p0, method)
// System.Void UnityEngine.TextGenerator::Init()
extern "C"  void TextGenerator_Init_m2914765083 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::Finalize()
extern "C"  void Object_Finalize_m2119677907 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextGenerator::Dispose_cpp()
extern "C"  void TextGenerator_Dispose_cpp_m1756287717 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m1058116622 (RuntimeObject * __this /* static, unused */, Object_t350248726 * ___x0, Object_t350248726 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Font::get_dynamic()
extern "C"  bool Font_get_dynamic_m1680016448 (Font_t1312336880 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m3459674631 (Object_t350248726 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarningFormat(UnityEngine.Object,System.String,System.Object[])
extern "C"  void Debug_LogWarningFormat_m2358042178 (RuntimeObject * __this /* static, unused */, Object_t350248726 * ___context0, String_t* ___format1, ObjectU5BU5D_t1100384052* ___args2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
extern "C"  void TextGenerator_GetCharactersInternal_m4017496931 (TextGenerator_t1842200301 * __this, RuntimeObject * ___characters0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
extern "C"  void TextGenerator_GetLinesInternal_m3509038285 (TextGenerator_t1842200301 * __this, RuntimeObject * ___lines0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
extern "C"  void TextGenerator_GetVerticesInternal_m800426062 (TextGenerator_t1842200301 * __this, RuntimeObject * ___vertices0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TextGenerator::Populate(System.String,UnityEngine.TextGenerationSettings)
extern "C"  bool TextGenerator_Populate_m66504053 (TextGenerator_t1842200301 * __this, String_t* ___str0, TextGenerationSettings_t781948148  ___settings1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
extern "C"  Rect_t82402607  TextGenerator_get_rectExtents_m1815013003 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_width()
extern "C"  float Rect_get_width_m2058356953 (Rect_t82402607 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Rect::get_height()
extern "C"  float Rect_get_height_m132400328 (Rect_t82402607 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextGenerationError UnityEngine.TextGenerator::PopulateWithError(System.String,UnityEngine.TextGenerationSettings)
extern "C"  int32_t TextGenerator_PopulateWithError_m596263034 (TextGenerator_t1842200301 * __this, String_t* ___str0, TextGenerationSettings_t781948148  ___settings1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogErrorFormat(UnityEngine.Object,System.String,System.Object[])
extern "C"  void Debug_LogErrorFormat_m2563142185 (RuntimeObject * __this /* static, unused */, Object_t350248726 * ___context0, String_t* ___format1, ObjectU5BU5D_t1100384052* ___args2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1348988297 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextGenerationError UnityEngine.TextGenerator::PopulateAlways(System.String,UnityEngine.TextGenerationSettings)
extern "C"  int32_t TextGenerator_PopulateAlways_m223265224 (TextGenerator_t1842200301 * __this, String_t* ___str0, TextGenerationSettings_t781948148  ___settings1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::ValidatedSettings(UnityEngine.TextGenerationSettings)
extern "C"  TextGenerationSettings_t781948148  TextGenerator_ValidatedSettings_m2277398987 (TextGenerator_t1842200301 * __this, TextGenerationSettings_t781948148  ___settings0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.VerticalWrapMode,UnityEngine.HorizontalWrapMode,System.Boolean,UnityEngine.TextAnchor,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean,System.Boolean,UnityEngine.TextGenerationError&)
extern "C"  bool TextGenerator_Populate_Internal_m3120602123 (TextGenerator_t1842200301 * __this, String_t* ___str0, Font_t1312336880 * ___font1, Color_t4183816058  ___color2, int32_t ___fontSize3, float ___scaleFactor4, float ___lineSpacing5, int32_t ___style6, bool ___richText7, bool ___resizeTextForBestFit8, int32_t ___resizeTextMinSize9, int32_t ___resizeTextMaxSize10, int32_t ___verticalOverFlow11, int32_t ___horizontalOverflow12, bool ___updateBounds13, int32_t ___anchor14, Vector2_t1065050092  ___extents15, Vector2_t1065050092  ___pivot16, bool ___generateOutOfBounds17, bool ___alignByGeometry18, int32_t* ___error19, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextGenerator::GetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C"  void TextGenerator_GetVertices_m2322127786 (TextGenerator_t1842200301 * __this, List_1_t2598460513 * ___vertices0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextGenerator::GetCharacters(System.Collections.Generic.List`1<UnityEngine.UICharInfo>)
extern "C"  void TextGenerator_GetCharacters_m4195124020 (TextGenerator_t1842200301 * __this, List_1_t1278937086 * ___characters0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextGenerator::GetLines(System.Collections.Generic.List`1<UnityEngine.UILineInfo>)
extern "C"  void TextGenerator_GetLines_m1093343725 (TextGenerator_t1842200301 * __this, List_1_t1960923134 * ___lines0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TextGenerator::Populate_Internal_cpp(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,System.UInt32&)
extern "C"  bool TextGenerator_Populate_Internal_cpp_m4181349894 (TextGenerator_t1842200301 * __this, String_t* ___str0, Font_t1312336880 * ___font1, Color_t4183816058  ___color2, int32_t ___fontSize3, float ___scaleFactor4, float ___lineSpacing5, int32_t ___style6, bool ___richText7, bool ___resizeTextForBestFit8, int32_t ___resizeTextMinSize9, int32_t ___resizeTextMaxSize10, int32_t ___verticalOverFlow11, int32_t ___horizontalOverflow12, bool ___updateBounds13, int32_t ___anchor14, float ___extentsX15, float ___extentsY16, float ___pivotX17, float ___pivotY18, bool ___generateOutOfBounds19, bool ___alignByGeometry20, uint32_t* ___error21, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,System.UInt32&)
extern "C"  bool TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m3646187300 (RuntimeObject * __this /* static, unused */, TextGenerator_t1842200301 * ___self0, String_t* ___str1, Font_t1312336880 * ___font2, Color_t4183816058 * ___color3, int32_t ___fontSize4, float ___scaleFactor5, float ___lineSpacing6, int32_t ___style7, bool ___richText8, bool ___resizeTextForBestFit9, int32_t ___resizeTextMinSize10, int32_t ___resizeTextMaxSize11, int32_t ___verticalOverFlow12, int32_t ___horizontalOverflow13, bool ___updateBounds14, int32_t ___anchor15, float ___extentsX16, float ___extentsY17, float ___pivotX18, float ___pivotY19, bool ___generateOutOfBounds20, bool ___alignByGeometry21, uint32_t* ___error22, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
extern "C"  void TextGenerator_INTERNAL_get_rectExtents_m2087090843 (TextGenerator_t1842200301 * __this, Rect_t82402607 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.TextGenerator::get_characterCount()
extern "C"  int32_t TextGenerator_get_characterCount_m2769027000 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::.ctor()
extern "C"  void Object__ctor_m3951288964 (Object_t350248726 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetWidth_m1868691910 (RuntimeObject * __this /* static, unused */, Texture_t4046637001 * ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetHeight_m3343530108 (RuntimeObject * __this /* static, unused */, Texture_t4046637001 * ___t0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
extern "C"  void Texture_INTERNAL_get_texelSize_m3795909375 (Texture_t4046637001 * __this, Vector2_t1065050092 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m3453153216 (Texture_t4046637001 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D_Internal_Create_m267453861 (RuntimeObject * __this /* static, unused */, Texture2D_t1431210461 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___format3, bool ___mipmap4, bool ___linear5, IntPtr_t ___nativeTex6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_GetPixelBilinear_m743625832 (RuntimeObject * __this /* static, unused */, Texture2D_t1431210461 * ___self0, float ___u1, float ___v2, Color_t4183816058 * ___value3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m547240586 (Texture2D_t1431210461 * __this, ColorU5BU5D_t349694303* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m3460391273 (Texture2D_t1431210461 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, ColorU5BU5D_t349694303* ___colors4, int32_t ___miplevel5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32)
extern "C"  ColorU5BU5D_t349694303* Texture2D_GetPixels_m3355391893 (Texture2D_t1431210461 * __this, int32_t ___miplevel0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  ColorU5BU5D_t349694303* Texture2D_GetPixels_m4219050946 (Texture2D_t1431210461 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, int32_t ___miplevel4, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture2D_Apply_m2015313753 (Texture2D_t1431210461 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32,System.Int32,System.Boolean)
extern "C"  RectU5BU5D_t75527478* Texture2D_PackTextures_m3868321432 (Texture2D_t1431210461 * __this, Texture2DU5BU5D_t2067778064* ___textures0, int32_t ___padding1, int32_t ___maximumAtlasSize2, bool ___makeNoLongerReadable3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Attribute::.ctor()
extern "C"  void Attribute__ctor_m959732044 (Attribute_t2169343654 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m3778479957 (Touch_t3995534465 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t1065050092  Touch_get_position_m1944077023 (Touch_t3995534465 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m1588321714 (Touch_t3995534465 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchType UnityEngine.Touch::get_type()
extern "C"  int32_t Touch_get_type_m3088692135 (Touch_t3995534465 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Convert::ToUInt32(System.Object)
extern "C"  uint32_t Convert_ToUInt32_m3960554041 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.UInt32 System.Convert::ToUInt32(System.Boolean)
extern "C"  uint32_t Convert_ToUInt32_m4274822710 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C"  void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2651191005 (TouchScreenKeyboard_t1544112657 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t857896789 * ___arguments0, String_t* ___text1, String_t* ___textPlaceholder2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C"  void TouchScreenKeyboard_Destroy_m1267789774 (TouchScreenKeyboard_t1544112657 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern "C"  int32_t Application_get_platform_m3065878097 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  TouchScreenKeyboard_t1544112657 * TouchScreenKeyboard_Open_m1817258185 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  void TouchScreenKeyboard__ctor_m3584002309 (TouchScreenKeyboard_t1544112657 * __this, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)
extern "C"  void TouchScreenKeyboard_GetSelectionInternal_m1537931821 (TouchScreenKeyboard_t1544112657 * __this, int32_t* ___start0, int32_t* ___length1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
extern "C"  bool IntPtr_op_Equality_m3855036101 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, IntPtr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern "C"  bool TrackedReference_op_Equality_m2363965236 (RuntimeObject * __this /* static, unused */, TrackedReference_t4182582575 * ___x0, TrackedReference_t4182582575 * ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.IntPtr::op_Explicit(System.IntPtr)
extern "C"  int32_t IntPtr_op_Explicit_m2301659926 (RuntimeObject * __this /* static, unused */, IntPtr_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_position_m3045837290 (Transform_t2735953680 * __this, Vector3_t2825674791 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_position_m433280170 (Transform_t2735953680 * __this, Vector3_t2825674791 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localPosition_m3942343510 (Transform_t2735953680 * __this, Vector3_t2825674791 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localPosition_m3932234355 (Transform_t2735953680 * __this, Vector3_t2825674791 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t580650070  Transform_get_rotation_m1403455390 (Transform_t2735953680 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C"  Vector3_t2825674791  Vector3_get_right_m372445178 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Quaternion_op_Multiply_m3137134439 (RuntimeObject * __this /* static, unused */, Quaternion_t580650070  ___rotation0, Vector3_t2825674791  ___point1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t2825674791  Vector3_get_up_m2143830244 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t2825674791  Vector3_get_forward_m3907749096 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_rotation_m2559117592 (Transform_t2735953680 * __this, Quaternion_t580650070 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_rotation_m3346348661 (Transform_t2735953680 * __this, Quaternion_t580650070 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_localRotation_m523486095 (Transform_t2735953680 * __this, Quaternion_t580650070 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_localRotation_m3517956161 (Transform_t2735953680 * __this, Quaternion_t580650070 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localScale_m21643413 (Transform_t2735953680 * __this, Vector3_t2825674791 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localScale_m1990679844 (Transform_t2735953680 * __this, Vector3_t2825674791 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C"  Transform_t2735953680 * Transform_get_parentInternal_m596331430 (Transform_t2735953680 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogWarning_m415376909 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___message0, Object_t350248726 * ___context1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C"  void Transform_set_parentInternal_m1203231964 (Transform_t2735953680 * __this, Transform_t2735953680 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m3800356349 (Transform_t2735953680 * __this, Transform_t2735953680 * ___parent0, bool ___worldPositionStays1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_worldToLocalMatrix_m1218008629 (Transform_t2735953680 * __this, Matrix4x4_t3574440610 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_localToWorldMatrix_m2457902037 (Transform_t2735953680 * __this, Matrix4x4_t3574440610 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_SetPositionAndRotation(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_CALL_SetPositionAndRotation_m1696884030 (RuntimeObject * __this /* static, unused */, Transform_t2735953680 * ___self0, Vector3_t2825674791 * ___position1, Quaternion_t580650070 * ___rotation2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Translate_m1089578796 (Transform_t2735953680 * __this, Vector3_t2825674791  ___translation0, int32_t ___relativeTo1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2825674791  Transform_get_position_m1871351526 (Transform_t2735953680 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Vector3_op_Addition_m1434256537 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___a0, Vector3_t2825674791  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m219509932 (Transform_t2735953680 * __this, Vector3_t2825674791  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Transform_TransformDirection_m3357137791 (Transform_t2735953680 * __this, Vector3_t2825674791  ___direction0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern "C"  void Transform_Translate_m4108251285 (Transform_t2735953680 * __this, float ___x0, float ___y1, float ___z2, int32_t ___relativeTo3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m874567701 (Vector3_t2825674791 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Rotate_m546776242 (Transform_t2735953680 * __this, Vector3_t2825674791  ___eulerAngles0, int32_t ___relativeTo1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern "C"  Quaternion_t580650070  Quaternion_Euler_m327605361 (RuntimeObject * __this /* static, unused */, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t580650070  Transform_get_localRotation_m1212773390 (Transform_t2735953680 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern "C"  Quaternion_t580650070  Quaternion_op_Multiply_m2046237407 (RuntimeObject * __this /* static, unused */, Quaternion_t580650070  ___lhs0, Quaternion_t580650070  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m1234546214 (Transform_t2735953680 * __this, Quaternion_t580650070  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern "C"  Quaternion_t580650070  Quaternion_Inverse_m758774898 (RuntimeObject * __this /* static, unused */, Quaternion_t580650070  ___rotation0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m1708311517 (Transform_t2735953680 * __this, Quaternion_t580650070  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformDirection_m3118760480 (RuntimeObject * __this /* static, unused */, Transform_t2735953680 * ___self0, Vector3_t2825674791 * ___direction1, Vector3_t2825674791 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformDirection_m2248881875 (RuntimeObject * __this /* static, unused */, Transform_t2735953680 * ___self0, Vector3_t2825674791 * ___direction1, Vector3_t2825674791 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformPoint_m3182684986 (RuntimeObject * __this /* static, unused */, Transform_t2735953680 * ___self0, Vector3_t2825674791 * ___position1, Vector3_t2825674791 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformPoint_m269031554 (RuntimeObject * __this /* static, unused */, Transform_t2735953680 * ___self0, Vector3_t2825674791 * ___position1, Vector3_t2825674791 * ___value2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m3975348791 (Enumerator_t1540455786 * __this, Transform_t2735953680 * ___outer0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t2735953680 * Transform_GetChild_m2012975997 (Transform_t2735953680 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m987201346 (Transform_t2735953680 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Action`1<UnityEngine.U2D.SpriteAtlas>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m2443092149(__this, p0, p1, method) ((  void (*) (Action_1_t2233756553 *, RuntimeObject *, IntPtr_t, const RuntimeMethod*))Action_1__ctor_m1715545874_gshared)(__this, p0, p1, method)
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::Invoke(System.String,System.Action`1<UnityEngine.U2D.SpriteAtlas>)
extern "C"  void RequestAtlasCallback_Invoke_m1786080242 (RequestAtlasCallback_t2148362272 * __this, String_t* ___tag0, Action_1_t2233756553 * ___action1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern "C"  void Color32__ctor_m787656177 (Color32_t2411287715 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, uint8_t ___a3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m704222614 (Vector4_t2651204353 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2825674791  Vector3_get_zero_m1721653641 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C"  Vector3_t2825674791  Vector3_get_back_m3191415170 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.AppDomain System.AppDomain::get_CurrentDomain()
extern "C"  AppDomain_t794471768 * AppDomain_get_CurrentDomain_m2935809104 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.UnhandledExceptionEventHandler::.ctor(System.Object,System.IntPtr)
extern "C"  void UnhandledExceptionEventHandler__ctor_m3089028188 (UnhandledExceptionEventHandler_t3425235907 * __this, RuntimeObject * p0, IntPtr_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.AppDomain::add_UnhandledException(System.UnhandledExceptionEventHandler)
extern "C"  void AppDomain_add_UnhandledException_m2707088924 (AppDomain_t794471768 * __this, UnhandledExceptionEventHandler_t3425235907 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Object System.UnhandledExceptionEventArgs::get_ExceptionObject()
extern "C"  RuntimeObject * UnhandledExceptionEventArgs_get_ExceptionObject_m416069261 (UnhandledExceptionEventArgs_t2750508014 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern "C"  void UnhandledExceptionHandler_PrintException_m3598350440 (RuntimeObject * __this /* static, unused */, String_t* ___title0, Exception_t2572292308 * ___e1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Type System.Exception::GetType()
extern "C"  Type_t * Exception_GetType_m1269163561 (Exception_t2572292308 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)
extern "C"  void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1575531855 (RuntimeObject * __this /* static, unused */, String_t* ___managedExceptionType0, String_t* ___managedExceptionMessage1, String_t* ___managedExceptionStack2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogException(System.Exception)
extern "C"  void Debug_LogException_m3669325632 (RuntimeObject * __this /* static, unused */, Exception_t2572292308 * ___exception0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Exception System.Exception::get_InnerException()
extern "C"  Exception_t2572292308 * Exception_get_InnerException_m3768527343 (Exception_t2572292308 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String)
extern "C"  void Exception__ctor_m225896743 (Exception_t2572292308 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::set_HResult(System.Int32)
extern "C"  void Exception_set_HResult_m2285642882 (Exception_t2572292308 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.String,System.Exception)
extern "C"  void Exception__ctor_m290184388 (Exception_t2572292308 * __this, String_t* p0, Exception_t2572292308 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Exception::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void Exception__ctor_m3696836514 (Exception_t2572292308 * __this, SerializationInfo_t1085725973 * p0, StreamingContext_t4227869630  p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IO.TextWriter::.ctor()
extern "C"  void TextWriter__ctor_m3514236646 (TextWriter_t2423646586 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnityLogWriter::.ctor()
extern "C"  void UnityLogWriter__ctor_m4110637147 (UnityLogWriter_t646926076 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Console::SetOut(System.IO.TextWriter)
extern "C"  void Console_SetOut_m1131447748 (RuntimeObject * __this /* static, unused */, TextWriter_t2423646586 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Char::ToString()
extern "C"  String_t* Char_ToString_m801101901 (Il2CppChar* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)
extern "C"  void UnityLogWriter_WriteStringToUnityLog_m3468757458 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m345500613 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t1100384052* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::.ctor(System.Int32)
#define Queue_1__ctor_m2523417422(__this, p0, method) ((  void (*) (Queue_1_t2108291272 *, int32_t, const RuntimeMethod*))Queue_1__ctor_m2523417422_gshared)(__this, p0, method)
// System.Threading.Thread System.Threading.Thread::get_CurrentThread()
extern "C"  Thread_t2629334981 * Thread_get_CurrentThread_m1378268693 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Thread::get_ManagedThreadId()
extern "C"  int32_t Thread_get_ManagedThreadId_m1526988511 (Thread_t2629334981 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::.ctor()
extern "C"  void SynchronizationContext__ctor_m1825192790 (SynchronizationContext_t155019481 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m3163704715 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::Dequeue()
#define Queue_1_Dequeue_m3333247629(__this, method) ((  WorkRequest_t2264216812  (*) (Queue_1_t2108291272 *, const RuntimeMethod*))Queue_1_Dequeue_m3333247629_gshared)(__this, method)
// System.Void UnityEngine.UnitySynchronizationContext/WorkRequest::Invoke()
extern "C"  void WorkRequest_Invoke_m2879977395 (WorkRequest_t2264216812 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.Queue`1<UnityEngine.UnitySynchronizationContext/WorkRequest>::get_Count()
#define Queue_1_get_Count_m4197321750(__this, method) ((  int32_t (*) (Queue_1_t2108291272 *, const RuntimeMethod*))Queue_1_get_Count_m4197321750_gshared)(__this, method)
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m366607352 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Threading.SynchronizationContext System.Threading.SynchronizationContext::get_Current()
extern "C"  SynchronizationContext_t155019481 * SynchronizationContext_get_Current_m3430941772 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnitySynchronizationContext::.ctor()
extern "C"  void UnitySynchronizationContext__ctor_m3792825602 (UnitySynchronizationContext_t3253602121 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SynchronizationContext::SetSynchronizationContext(System.Threading.SynchronizationContext)
extern "C"  void SynchronizationContext_SetSynchronizationContext_m901783352 (RuntimeObject * __this /* static, unused */, SynchronizationContext_t155019481 * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UnitySynchronizationContext::Exec()
extern "C"  void UnitySynchronizationContext_Exec_m1681198408 (UnitySynchronizationContext_t3253602121 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.SendOrPostCallback::Invoke(System.Object)
extern "C"  void SendOrPostCallback_Invoke_m2222920495 (SendOrPostCallback_t3197245846 * __this, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Threading.EventWaitHandle::Set()
extern "C"  bool EventWaitHandle_Set_m269094399 (EventWaitHandle_t2896330895 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m4204458150 (Vector2_t1065050092 * __this, float ___x0, float ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.IndexOutOfRangeException::.ctor(System.String)
extern "C"  void IndexOutOfRangeException__ctor_m3252438219 (IndexOutOfRangeException_t481375535 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern "C"  float Vector2_get_Item_m904236465 (Vector2_t1065050092 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern "C"  void Vector2_set_Item_m1154146684 (Vector2_t1065050092 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m3782610777 (RuntimeObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t1100384052* ___args1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector2::ToString()
extern "C"  String_t* Vector2_ToString_m1150693065 (Vector2_t1065050092 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m1220171593 (float* __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m1564755462 (Vector2_t1065050092 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Single::Equals(System.Single)
extern "C"  bool Single_Equals_m4010867313 (float* __this, float p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern "C"  bool Vector2_Equals_m1183599621 (Vector2_t1065050092 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C"  float Vector2_get_magnitude_m1391329218 (Vector2_t1065050092 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m1831367612 (Vector2_t1065050092 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t1065050092  Vector2_op_Subtraction_m1110678165 (RuntimeObject * __this /* static, unused */, Vector2_t1065050092  ___a0, Vector2_t1065050092  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Equality_m2618668614 (RuntimeObject * __this /* static, unused */, Vector2_t1065050092  ___lhs0, Vector2_t1065050092  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C"  void Vector3__ctor_m2674852412 (Vector3_t2825674791 * __this, float ___x0, float ___y1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern "C"  float Mathf_Clamp01_m3679321964 (RuntimeObject * __this /* static, unused */, float ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Vector3_op_Subtraction_m2076583184 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___a0, Vector3_t2825674791  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C"  float Vector3_get_magnitude_m59141406 (Vector3_t2825674791 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2825674791  Vector3_op_Division_m2525477617 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___a0, float ___d1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2825674791  Vector3_op_Multiply_m544015589 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___a0, float ___d1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern "C"  float Vector3_get_Item_m2529581307 (Vector3_t2825674791 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
extern "C"  void Vector3_set_Item_m729529755 (Vector3_t2825674791 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m930170997 (Vector3_t2825674791 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern "C"  bool Vector3_Equals_m745331672 (Vector3_t2825674791 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern "C"  float Vector3_Magnitude_m341851577 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___vector0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::Normalize()
extern "C"  void Vector3_Normalize_m3124542329 (Vector3_t2825674791 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Vector3_Normalize_m407020897 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t2825674791  Vector3_get_normalized_m146301709 (Vector3_t2825674791 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C"  float Vector3_get_sqrMagnitude_m1156660751 (Vector3_t2825674791 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern "C"  float Mathf_Min_m1373908152 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern "C"  float Mathf_Max_m3185906259 (RuntimeObject * __this /* static, unused */, float ___a0, float ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
extern "C"  float Vector3_SqrMagnitude_m3584580626 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___vector0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m3932752819 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___lhs0, Vector3_t2825674791  ___rhs1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector3::ToString()
extern "C"  String_t* Vector3_ToString_m1883038722 (Vector3_t2825674791 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern "C"  float Vector4_get_Item_m1609178195 (Vector4_t2651204353 * __this, int32_t ___index0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
extern "C"  void Vector4_set_Item_m57064049 (Vector4_t2651204353 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m1317289159 (Vector4_t2651204353 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern "C"  bool Vector4_Equals_m3717557000 (Vector4_t2651204353 * __this, RuntimeObject * ___other0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Dot_m2098979705 (RuntimeObject * __this /* static, unused */, Vector4_t2651204353  ___a0, Vector4_t2651204353  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::get_sqrMagnitude()
extern "C"  float Vector4_get_sqrMagnitude_m3645389319 (Vector4_t2651204353 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t2651204353  Vector4_op_Subtraction_m2274273165 (RuntimeObject * __this /* static, unused */, Vector4_t2651204353  ___a0, Vector4_t2651204353  ___b1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Vector4::SqrMagnitude(UnityEngine.Vector4)
extern "C"  float Vector4_SqrMagnitude_m3184081721 (RuntimeObject * __this /* static, unused */, Vector4_t2651204353  ___a0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Vector4::ToString()
extern "C"  String_t* Vector4_ToString_m2329116964 (Vector4_t2651204353 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C"  void YieldInstruction__ctor_m1436237666 (YieldInstruction_t1400753137 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.CustomYieldInstruction::.ctor()
extern "C"  void CustomYieldInstruction__ctor_m879225322 (CustomYieldInstruction_t1129172254 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C"  float Time_get_realtimeSinceStartup_m1699101199 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Stack::.ctor()
extern "C"  void Stack__ctor_m2016713434 (Stack_t1463244948 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::CreateDelegate(System.Type,System.Object,System.Reflection.MethodInfo)
extern "C"  Delegate_t1310841479 * Delegate_CreateDelegate_m3508464446 (RuntimeObject * __this /* static, unused */, Type_t * p0, RuntimeObject * p1, MethodInfo_t * p2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Reflection.MethodInfo System.Delegate::get_Method()
extern "C"  MethodInfo_t * Delegate_get_Method_m3777026472 (Delegate_t1310841479 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.Delegate::CreateDelegate(System.Type,System.Reflection.MethodInfo)
extern "C"  Delegate_t1310841479 * Delegate_CreateDelegate_m3714199633 (RuntimeObject * __this /* static, unused */, Type_t * p0, MethodInfo_t * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern "C"  void TypeInferenceRuleAttribute__ctor_m3182420183 (TypeInferenceRuleAttribute_t1740219836 * __this, String_t* ___rule0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Char System.String::get_Chars(System.Int32)
extern "C"  Il2CppChar String_get_Chars_m2121776380 (String_t* __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Uri::.ctor(System.String,System.UriKind)
extern "C"  void Uri__ctor_m2370721803 (Uri_t3689888142 * __this, String_t* p0, int32_t p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Uri::get_IsAbsoluteUri()
extern "C"  bool Uri_get_IsAbsoluteUri_m2908407282 (Uri_t3689888142 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.Uri::get_AbsoluteUri()
extern "C"  String_t* Uri_get_AbsoluteUri_m2801286058 (Uri_t3689888142 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Uri::.ctor(System.Uri,System.Uri)
extern "C"  void Uri__ctor_m1216760077 (Uri_t3689888142 * __this, Uri_t3689888142 * p0, Uri_t3689888142 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.RegularExpressions.Regex::.ctor(System.String)
extern "C"  void Regex__ctor_m3638139776 (Regex_t1806904682 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
extern "C"  void StateMachineBehaviour__ctor_m3010037963 (StateMachineBehaviour_t891337786 * __this, const RuntimeMethod* method)
{
	{
		ScriptableObject__ctor_m3459841701(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateEnter_m665181389 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, AnimatorStateInfo_t4150098719  ___stateInfo1, int32_t ___layerIndex2, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateUpdate_m403770521 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, AnimatorStateInfo_t4150098719  ___stateInfo1, int32_t ___layerIndex2, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateExit_m368886458 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, AnimatorStateInfo_t4150098719  ___stateInfo1, int32_t ___layerIndex2, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateMove_m2127583204 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, AnimatorStateInfo_t4150098719  ___stateInfo1, int32_t ___layerIndex2, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateIK_m554747226 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, AnimatorStateInfo_t4150098719  ___stateInfo1, int32_t ___layerIndex2, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateMachineEnter_m1043305602 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, int32_t ___stateMachinePathHash1, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32)
extern "C"  void StateMachineBehaviour_OnStateMachineExit_m1895708448 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, int32_t ___stateMachinePathHash1, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Animations.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateEnter_m3804964016 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, AnimatorStateInfo_t4150098719  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t4108006533  ___controller3, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateUpdate(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Animations.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateUpdate_m1421925242 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, AnimatorStateInfo_t4150098719  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t4108006533  ___controller3, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Animations.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateExit_m1919802941 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, AnimatorStateInfo_t4150098719  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t4108006533  ___controller3, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMove(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Animations.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateMove_m2596053886 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, AnimatorStateInfo_t4150098719  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t4108006533  ___controller3, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateIK(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32,UnityEngine.Animations.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateIK_m1900491902 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, AnimatorStateInfo_t4150098719  ___stateInfo1, int32_t ___layerIndex2, AnimatorControllerPlayable_t4108006533  ___controller3, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineEnter(UnityEngine.Animator,System.Int32,UnityEngine.Animations.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateMachineEnter_m2630042076 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, int32_t ___stateMachinePathHash1, AnimatorControllerPlayable_t4108006533  ___controller2, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void UnityEngine.StateMachineBehaviour::OnStateMachineExit(UnityEngine.Animator,System.Int32,UnityEngine.Animations.AnimatorControllerPlayable)
extern "C"  void StateMachineBehaviour_OnStateMachineExit_m2104338748 (StateMachineBehaviour_t891337786 * __this, Animator_t126418246 * ___animator0, int32_t ___stateMachinePathHash1, AnimatorControllerPlayable_t4108006533  ___controller2, const RuntimeMethod* method)
{
	{
		return;
	}
}
// UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::get_operatingSystemFamily()
extern "C"  int32_t SystemInfo_get_operatingSystemFamily_m1439091821 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef int32_t (*SystemInfo_get_operatingSystemFamily_m1439091821_ftn) ();
	static SystemInfo_get_operatingSystemFamily_m1439091821_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SystemInfo_get_operatingSystemFamily_m1439091821_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.SystemInfo::get_operatingSystemFamily()");
	int32_t retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern "C"  void TextAreaAttribute__ctor_m3229320737 (TextAreaAttribute_t1598070258 * __this, int32_t ___minLines0, int32_t ___maxLines1, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m824268376(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___minLines0;
		__this->set_minLines_0(L_0);
		int32_t L_1 = ___maxLines1;
		__this->set_maxLines_1(L_1);
		return;
	}
}
// System.String UnityEngine.TextAsset::get_text()
extern "C"  String_t* TextAsset_get_text_m2651831433 (TextAsset_t3707725843 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*TextAsset_get_text_m2651831433_ftn) (TextAsset_t3707725843 *);
	static TextAsset_get_text_m2651831433_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextAsset_get_text_m2651831433_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextAsset::get_text()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Byte[] UnityEngine.TextAsset::get_bytes()
extern "C"  ByteU5BU5D_t371269159* TextAsset_get_bytes_m951066892 (TextAsset_t3707725843 * __this, const RuntimeMethod* method)
{
	typedef ByteU5BU5D_t371269159* (*TextAsset_get_bytes_m951066892_ftn) (TextAsset_t3707725843 *);
	static TextAsset_get_bytes_m951066892_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextAsset_get_bytes_m951066892_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextAsset::get_bytes()");
	ByteU5BU5D_t371269159* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.String UnityEngine.TextAsset::ToString()
extern "C"  String_t* TextAsset_ToString_m4162082594 (TextAsset_t3707725843 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = TextAsset_get_text_m2651831433(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.TextEditor::.ctor()
extern "C"  void TextEditor__ctor_m3456547775 (TextEditor_t3996366928 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextEditor__ctor_m3456547775_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_keyboardOnScreen_0((TouchScreenKeyboard_t1544112657 *)NULL);
		__this->set_controlID_1(0);
		IL2CPP_RUNTIME_CLASS_INIT(GUIStyle_t2201526215_il2cpp_TypeInfo_var);
		GUIStyle_t2201526215 * L_0 = GUIStyle_get_none_m1197593709(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_style_2(L_0);
		__this->set_multiline_3((bool)0);
		__this->set_hasHorizontalCursorPos_4((bool)0);
		__this->set_isPasswordField_5((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t1065050092_il2cpp_TypeInfo_var);
		Vector2_t1065050092  L_1 = Vector2_get_zero_m644371544(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scrollOffset_6(L_1);
		GUIContent_t1852996497 * L_2 = (GUIContent_t1852996497 *)il2cpp_codegen_object_new(GUIContent_t1852996497_il2cpp_TypeInfo_var);
		GUIContent__ctor_m705666553(L_2, /*hidden argument*/NULL);
		__this->set_m_Content_7(L_2);
		__this->set_m_CursorIndex_8(0);
		__this->set_m_SelectIndex_9(0);
		__this->set_m_RevealCursor_10((bool)0);
		__this->set_m_MouseDragSelectsWholeWords_11((bool)0);
		__this->set_m_DblClickInitPos_12(0);
		__this->set_m_DblClickSnap_13(0);
		__this->set_m_bJustSelected_14((bool)0);
		__this->set_m_iAltCursorPos_15((-1));
		Object__ctor_m1723520498(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.TextGenerationSettings
extern "C" void TextGenerationSettings_t781948148_marshal_pinvoke(const TextGenerationSettings_t781948148& unmarshaled, TextGenerationSettings_t781948148_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___font_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'font' of type 'TextGenerationSettings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___font_0Exception);
}
extern "C" void TextGenerationSettings_t781948148_marshal_pinvoke_back(const TextGenerationSettings_t781948148_marshaled_pinvoke& marshaled, TextGenerationSettings_t781948148& unmarshaled)
{
	Il2CppCodeGenException* ___font_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'font' of type 'TextGenerationSettings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___font_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.TextGenerationSettings
extern "C" void TextGenerationSettings_t781948148_marshal_pinvoke_cleanup(TextGenerationSettings_t781948148_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TextGenerationSettings
extern "C" void TextGenerationSettings_t781948148_marshal_com(const TextGenerationSettings_t781948148& unmarshaled, TextGenerationSettings_t781948148_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___font_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'font' of type 'TextGenerationSettings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___font_0Exception);
}
extern "C" void TextGenerationSettings_t781948148_marshal_com_back(const TextGenerationSettings_t781948148_marshaled_com& marshaled, TextGenerationSettings_t781948148& unmarshaled)
{
	Il2CppCodeGenException* ___font_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'font' of type 'TextGenerationSettings': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___font_0Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.TextGenerationSettings
extern "C" void TextGenerationSettings_t781948148_marshal_com_cleanup(TextGenerationSettings_t781948148_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareColors(UnityEngine.Color,UnityEngine.Color)
extern "C"  bool TextGenerationSettings_CompareColors_m2069958116 (TextGenerationSettings_t781948148 * __this, Color_t4183816058  ___left0, Color_t4183816058  ___right1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextGenerationSettings_CompareColors_m2069958116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	{
		float L_0 = (&___left0)->get_r_0();
		float L_1 = (&___right1)->get_r_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m1817064303(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_005e;
		}
	}
	{
		float L_3 = (&___left0)->get_g_1();
		float L_4 = (&___right1)->get_g_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		bool L_5 = Mathf_Approximately_m1817064303(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_005e;
		}
	}
	{
		float L_6 = (&___left0)->get_b_2();
		float L_7 = (&___right1)->get_b_2();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		bool L_8 = Mathf_Approximately_m1817064303(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_005e;
		}
	}
	{
		float L_9 = (&___left0)->get_a_3();
		float L_10 = (&___right1)->get_a_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		bool L_11 = Mathf_Approximately_m1817064303(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_005f;
	}

IL_005e:
	{
		G_B5_0 = 0;
	}

IL_005f:
	{
		V_0 = (bool)G_B5_0;
		goto IL_0065;
	}

IL_0065:
	{
		bool L_12 = V_0;
		return L_12;
	}
}
extern "C"  bool TextGenerationSettings_CompareColors_m2069958116_AdjustorThunk (RuntimeObject * __this, Color_t4183816058  ___left0, Color_t4183816058  ___right1, const RuntimeMethod* method)
{
	TextGenerationSettings_t781948148 * _thisAdjusted = reinterpret_cast<TextGenerationSettings_t781948148 *>(__this + 1);
	return TextGenerationSettings_CompareColors_m2069958116(_thisAdjusted, ___left0, ___right1, method);
}
// System.Boolean UnityEngine.TextGenerationSettings::CompareVector2(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool TextGenerationSettings_CompareVector2_m2250474453 (TextGenerationSettings_t781948148 * __this, Vector2_t1065050092  ___left0, Vector2_t1065050092  ___right1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextGenerationSettings_CompareVector2_m2250474453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		float L_0 = (&___left0)->get_x_0();
		float L_1 = (&___right1)->get_x_0();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		bool L_2 = Mathf_Approximately_m1817064303(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002e;
		}
	}
	{
		float L_3 = (&___left0)->get_y_1();
		float L_4 = (&___right1)->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		bool L_5 = Mathf_Approximately_m1817064303(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_002f;
	}

IL_002e:
	{
		G_B3_0 = 0;
	}

IL_002f:
	{
		V_0 = (bool)G_B3_0;
		goto IL_0035;
	}

IL_0035:
	{
		bool L_6 = V_0;
		return L_6;
	}
}
extern "C"  bool TextGenerationSettings_CompareVector2_m2250474453_AdjustorThunk (RuntimeObject * __this, Vector2_t1065050092  ___left0, Vector2_t1065050092  ___right1, const RuntimeMethod* method)
{
	TextGenerationSettings_t781948148 * _thisAdjusted = reinterpret_cast<TextGenerationSettings_t781948148 *>(__this + 1);
	return TextGenerationSettings_CompareVector2_m2250474453(_thisAdjusted, ___left0, ___right1, method);
}
// System.Boolean UnityEngine.TextGenerationSettings::Equals(UnityEngine.TextGenerationSettings)
extern "C"  bool TextGenerationSettings_Equals_m4120249280 (TextGenerationSettings_t781948148 * __this, TextGenerationSettings_t781948148  ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextGenerationSettings_Equals_m4120249280_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B21_0 = 0;
	{
		Color_t4183816058  L_0 = __this->get_color_1();
		Color_t4183816058  L_1 = (&___other0)->get_color_1();
		bool L_2 = TextGenerationSettings_CompareColors_m2069958116(__this, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0187;
		}
	}
	{
		int32_t L_3 = __this->get_fontSize_2();
		int32_t L_4 = (&___other0)->get_fontSize_2();
		if ((!(((uint32_t)L_3) == ((uint32_t)L_4))))
		{
			goto IL_0187;
		}
	}
	{
		float L_5 = __this->get_scaleFactor_5();
		float L_6 = (&___other0)->get_scaleFactor_5();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		bool L_7 = Mathf_Approximately_m1817064303(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0187;
		}
	}
	{
		int32_t L_8 = __this->get_resizeTextMinSize_10();
		int32_t L_9 = (&___other0)->get_resizeTextMinSize_10();
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_0187;
		}
	}
	{
		int32_t L_10 = __this->get_resizeTextMaxSize_11();
		int32_t L_11 = (&___other0)->get_resizeTextMaxSize_11();
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_0187;
		}
	}
	{
		float L_12 = __this->get_lineSpacing_3();
		float L_13 = (&___other0)->get_lineSpacing_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		bool L_14 = Mathf_Approximately_m1817064303(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_0187;
		}
	}
	{
		int32_t L_15 = __this->get_fontStyle_6();
		int32_t L_16 = (&___other0)->get_fontStyle_6();
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_0187;
		}
	}
	{
		bool L_17 = __this->get_richText_4();
		bool L_18 = (&___other0)->get_richText_4();
		if ((!(((uint32_t)L_17) == ((uint32_t)L_18))))
		{
			goto IL_0187;
		}
	}
	{
		int32_t L_19 = __this->get_textAnchor_7();
		int32_t L_20 = (&___other0)->get_textAnchor_7();
		if ((!(((uint32_t)L_19) == ((uint32_t)L_20))))
		{
			goto IL_0187;
		}
	}
	{
		bool L_21 = __this->get_alignByGeometry_8();
		bool L_22 = (&___other0)->get_alignByGeometry_8();
		if ((!(((uint32_t)L_21) == ((uint32_t)L_22))))
		{
			goto IL_0187;
		}
	}
	{
		bool L_23 = __this->get_resizeTextForBestFit_9();
		bool L_24 = (&___other0)->get_resizeTextForBestFit_9();
		if ((!(((uint32_t)L_23) == ((uint32_t)L_24))))
		{
			goto IL_0187;
		}
	}
	{
		int32_t L_25 = __this->get_resizeTextMinSize_10();
		int32_t L_26 = (&___other0)->get_resizeTextMinSize_10();
		if ((!(((uint32_t)L_25) == ((uint32_t)L_26))))
		{
			goto IL_0187;
		}
	}
	{
		int32_t L_27 = __this->get_resizeTextMaxSize_11();
		int32_t L_28 = (&___other0)->get_resizeTextMaxSize_11();
		if ((!(((uint32_t)L_27) == ((uint32_t)L_28))))
		{
			goto IL_0187;
		}
	}
	{
		bool L_29 = __this->get_resizeTextForBestFit_9();
		bool L_30 = (&___other0)->get_resizeTextForBestFit_9();
		if ((!(((uint32_t)L_29) == ((uint32_t)L_30))))
		{
			goto IL_0187;
		}
	}
	{
		bool L_31 = __this->get_updateBounds_12();
		bool L_32 = (&___other0)->get_updateBounds_12();
		if ((!(((uint32_t)L_31) == ((uint32_t)L_32))))
		{
			goto IL_0187;
		}
	}
	{
		int32_t L_33 = __this->get_horizontalOverflow_14();
		int32_t L_34 = (&___other0)->get_horizontalOverflow_14();
		if ((!(((uint32_t)L_33) == ((uint32_t)L_34))))
		{
			goto IL_0187;
		}
	}
	{
		int32_t L_35 = __this->get_verticalOverflow_13();
		int32_t L_36 = (&___other0)->get_verticalOverflow_13();
		if ((!(((uint32_t)L_35) == ((uint32_t)L_36))))
		{
			goto IL_0187;
		}
	}
	{
		Vector2_t1065050092  L_37 = __this->get_generationExtents_15();
		Vector2_t1065050092  L_38 = (&___other0)->get_generationExtents_15();
		bool L_39 = TextGenerationSettings_CompareVector2_m2250474453(__this, L_37, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0187;
		}
	}
	{
		Vector2_t1065050092  L_40 = __this->get_pivot_16();
		Vector2_t1065050092  L_41 = (&___other0)->get_pivot_16();
		bool L_42 = TextGenerationSettings_CompareVector2_m2250474453(__this, L_40, L_41, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_0187;
		}
	}
	{
		Font_t1312336880 * L_43 = __this->get_font_0();
		Font_t1312336880 * L_44 = (&___other0)->get_font_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t350248726_il2cpp_TypeInfo_var);
		bool L_45 = Object_op_Equality_m3076063157(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		G_B21_0 = ((int32_t)(L_45));
		goto IL_0188;
	}

IL_0187:
	{
		G_B21_0 = 0;
	}

IL_0188:
	{
		V_0 = (bool)G_B21_0;
		goto IL_018e;
	}

IL_018e:
	{
		bool L_46 = V_0;
		return L_46;
	}
}
extern "C"  bool TextGenerationSettings_Equals_m4120249280_AdjustorThunk (RuntimeObject * __this, TextGenerationSettings_t781948148  ___other0, const RuntimeMethod* method)
{
	TextGenerationSettings_t781948148 * _thisAdjusted = reinterpret_cast<TextGenerationSettings_t781948148 *>(__this + 1);
	return TextGenerationSettings_Equals_m4120249280(_thisAdjusted, ___other0, method);
}


// Conversion methods for marshalling of: UnityEngine.TextGenerator
extern "C" void TextGenerator_t1842200301_marshal_pinvoke(const TextGenerator_t1842200301& unmarshaled, TextGenerator_t1842200301_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_LastSettings_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LastSettings' of type 'TextGenerator'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LastSettings_2Exception);
}
extern "C" void TextGenerator_t1842200301_marshal_pinvoke_back(const TextGenerator_t1842200301_marshaled_pinvoke& marshaled, TextGenerator_t1842200301& unmarshaled)
{
	Il2CppCodeGenException* ___m_LastSettings_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LastSettings' of type 'TextGenerator'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LastSettings_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.TextGenerator
extern "C" void TextGenerator_t1842200301_marshal_pinvoke_cleanup(TextGenerator_t1842200301_marshaled_pinvoke& marshaled)
{
}


// Conversion methods for marshalling of: UnityEngine.TextGenerator
extern "C" void TextGenerator_t1842200301_marshal_com(const TextGenerator_t1842200301& unmarshaled, TextGenerator_t1842200301_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_LastSettings_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LastSettings' of type 'TextGenerator'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LastSettings_2Exception);
}
extern "C" void TextGenerator_t1842200301_marshal_com_back(const TextGenerator_t1842200301_marshaled_com& marshaled, TextGenerator_t1842200301& unmarshaled)
{
	Il2CppCodeGenException* ___m_LastSettings_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_LastSettings' of type 'TextGenerator'.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_LastSettings_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.TextGenerator
extern "C" void TextGenerator_t1842200301_marshal_com_cleanup(TextGenerator_t1842200301_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.TextGenerator::.ctor()
extern "C"  void TextGenerator__ctor_m862294228 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	{
		TextGenerator__ctor_m3077917799(__this, ((int32_t)50), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::.ctor(System.Int32)
extern "C"  void TextGenerator__ctor_m3077917799 (TextGenerator_t1842200301 * __this, int32_t ___initialCapacity0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator__ctor_m3077917799_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m1723520498(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___initialCapacity0;
		List_1_t2598460513 * L_1 = (List_1_t2598460513 *)il2cpp_codegen_object_new(List_1_t2598460513_il2cpp_TypeInfo_var);
		List_1__ctor_m2708340509(L_1, ((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))*(int32_t)4)), /*hidden argument*/List_1__ctor_m2708340509_RuntimeMethod_var);
		__this->set_m_Verts_5(L_1);
		int32_t L_2 = ___initialCapacity0;
		List_1_t1278937086 * L_3 = (List_1_t1278937086 *)il2cpp_codegen_object_new(List_1_t1278937086_il2cpp_TypeInfo_var);
		List_1__ctor_m2803144634(L_3, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/List_1__ctor_m2803144634_RuntimeMethod_var);
		__this->set_m_Characters_6(L_3);
		List_1_t1960923134 * L_4 = (List_1_t1960923134 *)il2cpp_codegen_object_new(List_1_t1960923134_il2cpp_TypeInfo_var);
		List_1__ctor_m543064086(L_4, ((int32_t)20), /*hidden argument*/List_1__ctor_m543064086_RuntimeMethod_var);
		__this->set_m_Lines_7(L_4);
		TextGenerator_Init_m2914765083(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::Finalize()
extern "C"  void TextGenerator_Finalize_m546403643 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_Finalize_m546403643_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t2572292308 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2572292308 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2565118400_il2cpp_TypeInfo_var, __this);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2572292308 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2119677907(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2572292308 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.TextGenerator::System.IDisposable.Dispose()
extern "C"  void TextGenerator_System_IDisposable_Dispose_m3315247026 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	{
		TextGenerator_Dispose_cpp_m1756287717(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::ValidatedSettings(UnityEngine.TextGenerationSettings)
extern "C"  TextGenerationSettings_t781948148  TextGenerator_ValidatedSettings_m2277398987 (TextGenerator_t1842200301 * __this, TextGenerationSettings_t781948148  ___settings0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_ValidatedSettings_m2277398987_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TextGenerationSettings_t781948148  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Font_t1312336880 * L_0 = (&___settings0)->get_font_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t350248726_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m1058116622(NULL /*static, unused*/, L_0, (Object_t350248726 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		Font_t1312336880 * L_2 = (&___settings0)->get_font_0();
		NullCheck(L_2);
		bool L_3 = Font_get_dynamic_m1680016448(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		TextGenerationSettings_t781948148  L_4 = ___settings0;
		V_0 = L_4;
		goto IL_00e2;
	}

IL_002b:
	{
		int32_t L_5 = (&___settings0)->get_fontSize_2();
		if (L_5)
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_6 = (&___settings0)->get_fontStyle_6();
		if (!L_6)
		{
			goto IL_008d;
		}
	}

IL_0043:
	{
		Font_t1312336880 * L_7 = (&___settings0)->get_font_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t350248726_il2cpp_TypeInfo_var);
		bool L_8 = Object_op_Inequality_m1058116622(NULL /*static, unused*/, L_7, (Object_t350248726 *)NULL, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_007c;
		}
	}
	{
		Font_t1312336880 * L_9 = (&___settings0)->get_font_0();
		ObjectU5BU5D_t1100384052* L_10 = ((ObjectU5BU5D_t1100384052*)SZArrayNew(ObjectU5BU5D_t1100384052_il2cpp_TypeInfo_var, (uint32_t)1));
		Font_t1312336880 * L_11 = (&___settings0)->get_font_0();
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m3459674631(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_12);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t16286340_il2cpp_TypeInfo_var);
		Debug_LogWarningFormat_m2358042178(NULL /*static, unused*/, L_9, _stringLiteral4106943742, L_10, /*hidden argument*/NULL);
	}

IL_007c:
	{
		(&___settings0)->set_fontSize_2(0);
		(&___settings0)->set_fontStyle_6(0);
	}

IL_008d:
	{
		bool L_13 = (&___settings0)->get_resizeTextForBestFit_9();
		if (!L_13)
		{
			goto IL_00db;
		}
	}
	{
		Font_t1312336880 * L_14 = (&___settings0)->get_font_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t350248726_il2cpp_TypeInfo_var);
		bool L_15 = Object_op_Inequality_m1058116622(NULL /*static, unused*/, L_14, (Object_t350248726 *)NULL, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00d2;
		}
	}
	{
		Font_t1312336880 * L_16 = (&___settings0)->get_font_0();
		ObjectU5BU5D_t1100384052* L_17 = ((ObjectU5BU5D_t1100384052*)SZArrayNew(ObjectU5BU5D_t1100384052_il2cpp_TypeInfo_var, (uint32_t)1));
		Font_t1312336880 * L_18 = (&___settings0)->get_font_0();
		NullCheck(L_18);
		String_t* L_19 = Object_get_name_m3459674631(L_18, /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_19);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t16286340_il2cpp_TypeInfo_var);
		Debug_LogWarningFormat_m2358042178(NULL /*static, unused*/, L_16, _stringLiteral2828802952, L_17, /*hidden argument*/NULL);
	}

IL_00d2:
	{
		(&___settings0)->set_resizeTextForBestFit_9((bool)0);
	}

IL_00db:
	{
		TextGenerationSettings_t781948148  L_20 = ___settings0;
		V_0 = L_20;
		goto IL_00e2;
	}

IL_00e2:
	{
		TextGenerationSettings_t781948148  L_21 = V_0;
		return L_21;
	}
}
// System.Void UnityEngine.TextGenerator::Invalidate()
extern "C"  void TextGenerator_Invalidate_m3943469332 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_HasGenerated_3((bool)0);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharacters(System.Collections.Generic.List`1<UnityEngine.UICharInfo>)
extern "C"  void TextGenerator_GetCharacters_m4195124020 (TextGenerator_t1842200301 * __this, List_1_t1278937086 * ___characters0, const RuntimeMethod* method)
{
	{
		List_1_t1278937086 * L_0 = ___characters0;
		TextGenerator_GetCharactersInternal_m4017496931(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetLines(System.Collections.Generic.List`1<UnityEngine.UILineInfo>)
extern "C"  void TextGenerator_GetLines_m1093343725 (TextGenerator_t1842200301 * __this, List_1_t1960923134 * ___lines0, const RuntimeMethod* method)
{
	{
		List_1_t1960923134 * L_0 = ___lines0;
		TextGenerator_GetLinesInternal_m3509038285(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C"  void TextGenerator_GetVertices_m2322127786 (TextGenerator_t1842200301 * __this, List_1_t2598460513 * ___vertices0, const RuntimeMethod* method)
{
	{
		List_1_t2598460513 * L_0 = ___vertices0;
		TextGenerator_GetVerticesInternal_m800426062(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredWidth(System.String,UnityEngine.TextGenerationSettings)
extern "C"  float TextGenerator_GetPreferredWidth_m4228266224 (TextGenerator_t1842200301 * __this, String_t* ___str0, TextGenerationSettings_t781948148  ___settings1, const RuntimeMethod* method)
{
	Rect_t82402607  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		(&___settings1)->set_horizontalOverflow_14(1);
		(&___settings1)->set_verticalOverflow_13(1);
		(&___settings1)->set_updateBounds_12((bool)1);
		String_t* L_0 = ___str0;
		TextGenerationSettings_t781948148  L_1 = ___settings1;
		TextGenerator_Populate_m66504053(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t82402607  L_2 = TextGenerator_get_rectExtents_m1815013003(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m2058356953((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0036;
	}

IL_0036:
	{
		float L_4 = V_1;
		return L_4;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredHeight(System.String,UnityEngine.TextGenerationSettings)
extern "C"  float TextGenerator_GetPreferredHeight_m379436897 (TextGenerator_t1842200301 * __this, String_t* ___str0, TextGenerationSettings_t781948148  ___settings1, const RuntimeMethod* method)
{
	Rect_t82402607  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		(&___settings1)->set_verticalOverflow_13(1);
		(&___settings1)->set_updateBounds_12((bool)1);
		String_t* L_0 = ___str0;
		TextGenerationSettings_t781948148  L_1 = ___settings1;
		TextGenerator_Populate_m66504053(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t82402607  L_2 = TextGenerator_get_rectExtents_m1815013003(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_height_m132400328((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_002e;
	}

IL_002e:
	{
		float L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.TextGenerator::PopulateWithErrors(System.String,UnityEngine.TextGenerationSettings,UnityEngine.GameObject)
extern "C"  bool TextGenerator_PopulateWithErrors_m4249505679 (TextGenerator_t1842200301 * __this, String_t* ___str0, TextGenerationSettings_t781948148  ___settings1, GameObject_t3093992626 * ___context2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_PopulateWithErrors_m4249505679_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		String_t* L_0 = ___str0;
		TextGenerationSettings_t781948148  L_1 = ___settings1;
		int32_t L_2 = TextGenerator_PopulateWithError_m596263034(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3)
		{
			goto IL_0017;
		}
	}
	{
		V_1 = (bool)1;
		goto IL_0064;
	}

IL_0017:
	{
		int32_t L_4 = V_0;
		if (!((int32_t)((int32_t)L_4&(int32_t)1)))
		{
			goto IL_003a;
		}
	}
	{
		GameObject_t3093992626 * L_5 = ___context2;
		ObjectU5BU5D_t1100384052* L_6 = ((ObjectU5BU5D_t1100384052*)SZArrayNew(ObjectU5BU5D_t1100384052_il2cpp_TypeInfo_var, (uint32_t)1));
		Font_t1312336880 * L_7 = (&___settings1)->get_font_0();
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_7);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_7);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t16286340_il2cpp_TypeInfo_var);
		Debug_LogErrorFormat_m2563142185(NULL /*static, unused*/, L_5, _stringLiteral1728124395, L_6, /*hidden argument*/NULL);
	}

IL_003a:
	{
		int32_t L_8 = V_0;
		if (!((int32_t)((int32_t)L_8&(int32_t)2)))
		{
			goto IL_005d;
		}
	}
	{
		GameObject_t3093992626 * L_9 = ___context2;
		ObjectU5BU5D_t1100384052* L_10 = ((ObjectU5BU5D_t1100384052*)SZArrayNew(ObjectU5BU5D_t1100384052_il2cpp_TypeInfo_var, (uint32_t)1));
		Font_t1312336880 * L_11 = (&___settings1)->get_font_0();
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_11);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_11);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t16286340_il2cpp_TypeInfo_var);
		Debug_LogErrorFormat_m2563142185(NULL /*static, unused*/, L_9, _stringLiteral3221043172, L_10, /*hidden argument*/NULL);
	}

IL_005d:
	{
		V_1 = (bool)0;
		goto IL_0064;
	}

IL_0064:
	{
		bool L_12 = V_1;
		return L_12;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate(System.String,UnityEngine.TextGenerationSettings)
extern "C"  bool TextGenerator_Populate_m66504053 (TextGenerator_t1842200301 * __this, String_t* ___str0, TextGenerationSettings_t781948148  ___settings1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		String_t* L_0 = ___str0;
		TextGenerationSettings_t781948148  L_1 = ___settings1;
		int32_t L_2 = TextGenerator_PopulateWithError_m596263034(__this, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		V_1 = (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		goto IL_0014;
	}

IL_0014:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.TextGenerationError UnityEngine.TextGenerator::PopulateWithError(System.String,UnityEngine.TextGenerationSettings)
extern "C"  int32_t TextGenerator_PopulateWithError_m596263034 (TextGenerator_t1842200301 * __this, String_t* ___str0, TextGenerationSettings_t781948148  ___settings1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_PopulateWithError_m596263034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = __this->get_m_HasGenerated_3();
		if (!L_0)
		{
			goto IL_003b;
		}
	}
	{
		String_t* L_1 = ___str0;
		String_t* L_2 = __this->get_m_LastString_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1348988297(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003b;
		}
	}
	{
		TextGenerationSettings_t781948148  L_4 = __this->get_m_LastSettings_2();
		bool L_5 = TextGenerationSettings_Equals_m4120249280((&___settings1), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_003b;
		}
	}
	{
		int32_t L_6 = __this->get_m_LastValid_4();
		V_0 = L_6;
		goto IL_0055;
	}

IL_003b:
	{
		String_t* L_7 = ___str0;
		TextGenerationSettings_t781948148  L_8 = ___settings1;
		int32_t L_9 = TextGenerator_PopulateAlways_m223265224(__this, L_7, L_8, /*hidden argument*/NULL);
		__this->set_m_LastValid_4(L_9);
		int32_t L_10 = __this->get_m_LastValid_4();
		V_0 = L_10;
		goto IL_0055;
	}

IL_0055:
	{
		int32_t L_11 = V_0;
		return L_11;
	}
}
// UnityEngine.TextGenerationError UnityEngine.TextGenerator::PopulateAlways(System.String,UnityEngine.TextGenerationSettings)
extern "C"  int32_t TextGenerator_PopulateAlways_m223265224 (TextGenerator_t1842200301 * __this, String_t* ___str0, TextGenerationSettings_t781948148  ___settings1, const RuntimeMethod* method)
{
	TextGenerationSettings_t781948148  V_0;
	memset(&V_0, 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ___str0;
		__this->set_m_LastString_1(L_0);
		__this->set_m_HasGenerated_3((bool)1);
		__this->set_m_CachedVerts_8((bool)0);
		__this->set_m_CachedCharacters_9((bool)0);
		__this->set_m_CachedLines_10((bool)0);
		TextGenerationSettings_t781948148  L_1 = ___settings1;
		__this->set_m_LastSettings_2(L_1);
		TextGenerationSettings_t781948148  L_2 = ___settings1;
		TextGenerationSettings_t781948148  L_3 = TextGenerator_ValidatedSettings_m2277398987(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ___str0;
		Font_t1312336880 * L_5 = (&V_0)->get_font_0();
		Color_t4183816058  L_6 = (&V_0)->get_color_1();
		int32_t L_7 = (&V_0)->get_fontSize_2();
		float L_8 = (&V_0)->get_scaleFactor_5();
		float L_9 = (&V_0)->get_lineSpacing_3();
		int32_t L_10 = (&V_0)->get_fontStyle_6();
		bool L_11 = (&V_0)->get_richText_4();
		bool L_12 = (&V_0)->get_resizeTextForBestFit_9();
		int32_t L_13 = (&V_0)->get_resizeTextMinSize_10();
		int32_t L_14 = (&V_0)->get_resizeTextMaxSize_11();
		int32_t L_15 = (&V_0)->get_verticalOverflow_13();
		int32_t L_16 = (&V_0)->get_horizontalOverflow_14();
		bool L_17 = (&V_0)->get_updateBounds_12();
		int32_t L_18 = (&V_0)->get_textAnchor_7();
		Vector2_t1065050092  L_19 = (&V_0)->get_generationExtents_15();
		Vector2_t1065050092  L_20 = (&V_0)->get_pivot_16();
		bool L_21 = (&V_0)->get_generateOutOfBounds_17();
		bool L_22 = (&V_0)->get_alignByGeometry_8();
		TextGenerator_Populate_Internal_m3120602123(__this, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, L_22, (&V_1), /*hidden argument*/NULL);
		int32_t L_23 = V_1;
		__this->set_m_LastValid_4(L_23);
		int32_t L_24 = V_1;
		V_2 = L_24;
		goto IL_00c9;
	}

IL_00c9:
	{
		int32_t L_25 = V_2;
		return L_25;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::get_verts()
extern "C"  RuntimeObject* TextGenerator_get_verts_m1571254480 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		bool L_0 = __this->get_m_CachedVerts_8();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		List_1_t2598460513 * L_1 = __this->get_m_Verts_5();
		TextGenerator_GetVertices_m2322127786(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_CachedVerts_8((bool)1);
	}

IL_0021:
	{
		List_1_t2598460513 * L_2 = __this->get_m_Verts_5();
		V_0 = (RuntimeObject*)L_2;
		goto IL_002d;
	}

IL_002d:
	{
		RuntimeObject* L_3 = V_0;
		return L_3;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::get_characters()
extern "C"  RuntimeObject* TextGenerator_get_characters_m2893875107 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		bool L_0 = __this->get_m_CachedCharacters_9();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		List_1_t1278937086 * L_1 = __this->get_m_Characters_6();
		TextGenerator_GetCharacters_m4195124020(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_CachedCharacters_9((bool)1);
	}

IL_0021:
	{
		List_1_t1278937086 * L_2 = __this->get_m_Characters_6();
		V_0 = (RuntimeObject*)L_2;
		goto IL_002d;
	}

IL_002d:
	{
		RuntimeObject* L_3 = V_0;
		return L_3;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::get_lines()
extern "C"  RuntimeObject* TextGenerator_get_lines_m3736153356 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	RuntimeObject* V_0 = NULL;
	{
		bool L_0 = __this->get_m_CachedLines_10();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		List_1_t1960923134 * L_1 = __this->get_m_Lines_7();
		TextGenerator_GetLines_m1093343725(__this, L_1, /*hidden argument*/NULL);
		__this->set_m_CachedLines_10((bool)1);
	}

IL_0021:
	{
		List_1_t1960923134 * L_2 = __this->get_m_Lines_7();
		V_0 = (RuntimeObject*)L_2;
		goto IL_002d;
	}

IL_002d:
	{
		RuntimeObject* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.TextGenerator::Init()
extern "C"  void TextGenerator_Init_m2914765083 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	typedef void (*TextGenerator_Init_m2914765083_ftn) (TextGenerator_t1842200301 *);
	static TextGenerator_Init_m2914765083_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Init_m2914765083_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Dispose_cpp()
extern "C"  void TextGenerator_Dispose_cpp_m1756287717 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	typedef void (*TextGenerator_Dispose_cpp_m1756287717_ftn) (TextGenerator_t1842200301 *);
	static TextGenerator_Dispose_cpp_m1756287717_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Dispose_cpp_m1756287717_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Dispose_cpp()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.VerticalWrapMode,UnityEngine.HorizontalWrapMode,System.Boolean,UnityEngine.TextAnchor,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean,System.Boolean,UnityEngine.TextGenerationError&)
extern "C"  bool TextGenerator_Populate_Internal_m3120602123 (TextGenerator_t1842200301 * __this, String_t* ___str0, Font_t1312336880 * ___font1, Color_t4183816058  ___color2, int32_t ___fontSize3, float ___scaleFactor4, float ___lineSpacing5, int32_t ___style6, bool ___richText7, bool ___resizeTextForBestFit8, int32_t ___resizeTextMinSize9, int32_t ___resizeTextMaxSize10, int32_t ___verticalOverFlow11, int32_t ___horizontalOverflow12, bool ___updateBounds13, int32_t ___anchor14, Vector2_t1065050092  ___extents15, Vector2_t1065050092  ___pivot16, bool ___generateOutOfBounds17, bool ___alignByGeometry18, int32_t* ___error19, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TextGenerator_Populate_Internal_m3120602123_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	{
		V_0 = 0;
		Font_t1312336880 * L_0 = ___font1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t350248726_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3076063157(NULL /*static, unused*/, L_0, (Object_t350248726 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		int32_t* L_2 = ___error19;
		*((int32_t*)(L_2)) = (int32_t)4;
		V_1 = (bool)0;
		goto IL_006b;
	}

IL_001b:
	{
		String_t* L_3 = ___str0;
		Font_t1312336880 * L_4 = ___font1;
		Color_t4183816058  L_5 = ___color2;
		int32_t L_6 = ___fontSize3;
		float L_7 = ___scaleFactor4;
		float L_8 = ___lineSpacing5;
		int32_t L_9 = ___style6;
		bool L_10 = ___richText7;
		bool L_11 = ___resizeTextForBestFit8;
		int32_t L_12 = ___resizeTextMinSize9;
		int32_t L_13 = ___resizeTextMaxSize10;
		int32_t L_14 = ___verticalOverFlow11;
		int32_t L_15 = ___horizontalOverflow12;
		bool L_16 = ___updateBounds13;
		int32_t L_17 = ___anchor14;
		float L_18 = (&___extents15)->get_x_0();
		float L_19 = (&___extents15)->get_y_1();
		float L_20 = (&___pivot16)->get_x_0();
		float L_21 = (&___pivot16)->get_y_1();
		bool L_22 = ___generateOutOfBounds17;
		bool L_23 = ___alignByGeometry18;
		bool L_24 = TextGenerator_Populate_Internal_cpp_m4181349894(__this, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, L_22, L_23, (&V_0), /*hidden argument*/NULL);
		V_2 = L_24;
		int32_t* L_25 = ___error19;
		uint32_t L_26 = V_0;
		*((int32_t*)(L_25)) = (int32_t)L_26;
		bool L_27 = V_2;
		V_1 = L_27;
		goto IL_006b;
	}

IL_006b:
	{
		bool L_28 = V_1;
		return L_28;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal_cpp(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,System.UInt32&)
extern "C"  bool TextGenerator_Populate_Internal_cpp_m4181349894 (TextGenerator_t1842200301 * __this, String_t* ___str0, Font_t1312336880 * ___font1, Color_t4183816058  ___color2, int32_t ___fontSize3, float ___scaleFactor4, float ___lineSpacing5, int32_t ___style6, bool ___richText7, bool ___resizeTextForBestFit8, int32_t ___resizeTextMinSize9, int32_t ___resizeTextMaxSize10, int32_t ___verticalOverFlow11, int32_t ___horizontalOverflow12, bool ___updateBounds13, int32_t ___anchor14, float ___extentsX15, float ___extentsY16, float ___pivotX17, float ___pivotY18, bool ___generateOutOfBounds19, bool ___alignByGeometry20, uint32_t* ___error21, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		String_t* L_0 = ___str0;
		Font_t1312336880 * L_1 = ___font1;
		int32_t L_2 = ___fontSize3;
		float L_3 = ___scaleFactor4;
		float L_4 = ___lineSpacing5;
		int32_t L_5 = ___style6;
		bool L_6 = ___richText7;
		bool L_7 = ___resizeTextForBestFit8;
		int32_t L_8 = ___resizeTextMinSize9;
		int32_t L_9 = ___resizeTextMaxSize10;
		int32_t L_10 = ___verticalOverFlow11;
		int32_t L_11 = ___horizontalOverflow12;
		bool L_12 = ___updateBounds13;
		int32_t L_13 = ___anchor14;
		float L_14 = ___extentsX15;
		float L_15 = ___extentsY16;
		float L_16 = ___pivotX17;
		float L_17 = ___pivotY18;
		bool L_18 = ___generateOutOfBounds19;
		bool L_19 = ___alignByGeometry20;
		uint32_t* L_20 = ___error21;
		bool L_21 = TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m3646187300(NULL /*static, unused*/, __this, L_0, L_1, (&___color2), L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, /*hidden argument*/NULL);
		V_0 = L_21;
		goto IL_0037;
	}

IL_0037:
	{
		bool L_22 = V_0;
		return L_22;
	}
}
// System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,System.UInt32&)
extern "C"  bool TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m3646187300 (RuntimeObject * __this /* static, unused */, TextGenerator_t1842200301 * ___self0, String_t* ___str1, Font_t1312336880 * ___font2, Color_t4183816058 * ___color3, int32_t ___fontSize4, float ___scaleFactor5, float ___lineSpacing6, int32_t ___style7, bool ___richText8, bool ___resizeTextForBestFit9, int32_t ___resizeTextMinSize10, int32_t ___resizeTextMaxSize11, int32_t ___verticalOverFlow12, int32_t ___horizontalOverflow13, bool ___updateBounds14, int32_t ___anchor15, float ___extentsX16, float ___extentsY17, float ___pivotX18, float ___pivotY19, bool ___generateOutOfBounds20, bool ___alignByGeometry21, uint32_t* ___error22, const RuntimeMethod* method)
{
	typedef bool (*TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m3646187300_ftn) (TextGenerator_t1842200301 *, String_t*, Font_t1312336880 *, Color_t4183816058 *, int32_t, float, float, int32_t, bool, bool, int32_t, int32_t, int32_t, int32_t, bool, int32_t, float, float, float, float, bool, bool, uint32_t*);
	static TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m3646187300_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m3646187300_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,System.UInt32&)");
	bool retVal = _il2cpp_icall_func(___self0, ___str1, ___font2, ___color3, ___fontSize4, ___scaleFactor5, ___lineSpacing6, ___style7, ___richText8, ___resizeTextForBestFit9, ___resizeTextMinSize10, ___resizeTextMaxSize11, ___verticalOverFlow12, ___horizontalOverflow13, ___updateBounds14, ___anchor15, ___extentsX16, ___extentsY17, ___pivotX18, ___pivotY19, ___generateOutOfBounds20, ___alignByGeometry21, ___error22);
	return retVal;
}
// UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
extern "C"  Rect_t82402607  TextGenerator_get_rectExtents_m1815013003 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	Rect_t82402607  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Rect_t82402607  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		TextGenerator_INTERNAL_get_rectExtents_m2087090843(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t82402607  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Rect_t82402607  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
extern "C"  void TextGenerator_INTERNAL_get_rectExtents_m2087090843 (TextGenerator_t1842200301 * __this, Rect_t82402607 * ___value0, const RuntimeMethod* method)
{
	typedef void (*TextGenerator_INTERNAL_get_rectExtents_m2087090843_ftn) (TextGenerator_t1842200301 *, Rect_t82402607 *);
	static TextGenerator_INTERNAL_get_rectExtents_m2087090843_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_get_rectExtents_m2087090843_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
extern "C"  void TextGenerator_GetVerticesInternal_m800426062 (TextGenerator_t1842200301 * __this, RuntimeObject * ___vertices0, const RuntimeMethod* method)
{
	typedef void (*TextGenerator_GetVerticesInternal_m800426062_ftn) (TextGenerator_t1842200301 *, RuntimeObject *);
	static TextGenerator_GetVerticesInternal_m800426062_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesInternal_m800426062_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices0);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCount()
extern "C"  int32_t TextGenerator_get_characterCount_m2769027000 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*TextGenerator_get_characterCount_m2769027000_ftn) (TextGenerator_t1842200301 *);
	static TextGenerator_get_characterCount_m2769027000_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_characterCount_m2769027000_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_characterCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.TextGenerator::get_characterCountVisible()
extern "C"  int32_t TextGenerator_get_characterCountVisible_m806423748 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = TextGenerator_get_characterCount_m2769027000(__this, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_0-(int32_t)1));
		goto IL_000f;
	}

IL_000f:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
extern "C"  void TextGenerator_GetCharactersInternal_m4017496931 (TextGenerator_t1842200301 * __this, RuntimeObject * ___characters0, const RuntimeMethod* method)
{
	typedef void (*TextGenerator_GetCharactersInternal_m4017496931_ftn) (TextGenerator_t1842200301 *, RuntimeObject *);
	static TextGenerator_GetCharactersInternal_m4017496931_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersInternal_m4017496931_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersInternal(System.Object)");
	_il2cpp_icall_func(__this, ___characters0);
}
// System.Int32 UnityEngine.TextGenerator::get_lineCount()
extern "C"  int32_t TextGenerator_get_lineCount_m1802886944 (TextGenerator_t1842200301 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*TextGenerator_get_lineCount_m1802886944_ftn) (TextGenerator_t1842200301 *);
	static TextGenerator_get_lineCount_m1802886944_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_lineCount_m1802886944_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_lineCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
extern "C"  void TextGenerator_GetLinesInternal_m3509038285 (TextGenerator_t1842200301 * __this, RuntimeObject * ___lines0, const RuntimeMethod* method)
{
	typedef void (*TextGenerator_GetLinesInternal_m3509038285_ftn) (TextGenerator_t1842200301 *, RuntimeObject *);
	static TextGenerator_GetLinesInternal_m3509038285_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesInternal_m3509038285_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___lines0);
}
// System.Void UnityEngine.Texture::.ctor()
extern "C"  void Texture__ctor_m3453153216 (Texture_t4046637001 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture__ctor_m3453153216_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t350248726_il2cpp_TypeInfo_var);
		Object__ctor_m3951288964(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetWidth_m1868691910 (RuntimeObject * __this /* static, unused */, Texture_t4046637001 * ___t0, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_Internal_GetWidth_m1868691910_ftn) (Texture_t4046637001 *);
	static Texture_Internal_GetWidth_m1868691910_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetWidth_m1868691910_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetWidth(UnityEngine.Texture)");
	int32_t retVal = _il2cpp_icall_func(___t0);
	return retVal;
}
// System.Int32 UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)
extern "C"  int32_t Texture_Internal_GetHeight_m3343530108 (RuntimeObject * __this /* static, unused */, Texture_t4046637001 * ___t0, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_Internal_GetHeight_m3343530108_ftn) (Texture_t4046637001 *);
	static Texture_Internal_GetHeight_m3343530108_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_Internal_GetHeight_m3343530108_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::Internal_GetHeight(UnityEngine.Texture)");
	int32_t retVal = _il2cpp_icall_func(___t0);
	return retVal;
}
// System.Int32 UnityEngine.Texture::get_width()
extern "C"  int32_t Texture_get_width_m1962989142 (Texture_t4046637001 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Texture_Internal_GetWidth_m1868691910(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Texture::get_height()
extern "C"  int32_t Texture_get_height_m359272041 (Texture_t4046637001 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = Texture_Internal_GetHeight_m3343530108(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 UnityEngine.Texture::get_anisoLevel()
extern "C"  int32_t Texture_get_anisoLevel_m2943572572 (Texture_t4046637001 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_get_anisoLevel_m2943572572_ftn) (Texture_t4046637001 *);
	static Texture_get_anisoLevel_m2943572572_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_get_anisoLevel_m2943572572_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::get_anisoLevel()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Texture::set_anisoLevel(System.Int32)
extern "C"  void Texture_set_anisoLevel_m863059448 (Texture_t4046637001 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_set_anisoLevel_m863059448_ftn) (Texture_t4046637001 *, int32_t);
	static Texture_set_anisoLevel_m863059448_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_anisoLevel_m863059448_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_anisoLevel(System.Int32)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
extern "C"  int32_t Texture_get_wrapMode_m2056295353 (Texture_t4046637001 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Texture_get_wrapMode_m2056295353_ftn) (Texture_t4046637001 *);
	static Texture_get_wrapMode_m2056295353_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_get_wrapMode_m2056295353_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::get_wrapMode()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Texture::set_mipMapBias(System.Single)
extern "C"  void Texture_set_mipMapBias_m1967354255 (Texture_t4046637001 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_set_mipMapBias_m1967354255_ftn) (Texture_t4046637001 *, float);
	static Texture_set_mipMapBias_m1967354255_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_set_mipMapBias_m1967354255_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::set_mipMapBias(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector2 UnityEngine.Texture::get_texelSize()
extern "C"  Vector2_t1065050092  Texture_get_texelSize_m791776016 (Texture_t4046637001 * __this, const RuntimeMethod* method)
{
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t1065050092  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Texture_INTERNAL_get_texelSize_m3795909375(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t1065050092  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector2_t1065050092  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)
extern "C"  void Texture_INTERNAL_get_texelSize_m3795909375 (Texture_t4046637001 * __this, Vector2_t1065050092 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Texture_INTERNAL_get_texelSize_m3795909375_ftn) (Texture_t4046637001 *, Vector2_t1065050092 *);
	static Texture_INTERNAL_get_texelSize_m3795909375_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture_INTERNAL_get_texelSize_m3795909375_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture::INTERNAL_get_texelSize(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern "C"  void Texture2D__ctor_m2096256980 (Texture2D_t1431210461 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m2096256980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture__ctor_m3453153216(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		Texture2D_Internal_Create_m267453861(NULL /*static, unused*/, __this, L_0, L_1, 4, (bool)1, (bool)0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C"  void Texture2D__ctor_m2739356446 (Texture2D_t1431210461 * __this, int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipmap3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Texture2D__ctor_m2739356446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Texture__ctor_m3453153216(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		int32_t L_1 = ___height1;
		int32_t L_2 = ___format2;
		bool L_3 = ___mipmap3;
		IntPtr_t L_4 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		Texture2D_Internal_Create_m267453861(NULL /*static, unused*/, __this, L_0, L_1, L_2, L_3, (bool)0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
extern "C"  void Texture2D_Internal_Create_m267453861 (RuntimeObject * __this /* static, unused */, Texture2D_t1431210461 * ___mono0, int32_t ___width1, int32_t ___height2, int32_t ___format3, bool ___mipmap4, bool ___linear5, IntPtr_t ___nativeTex6, const RuntimeMethod* method)
{
	typedef void (*Texture2D_Internal_Create_m267453861_ftn) (Texture2D_t1431210461 *, int32_t, int32_t, int32_t, bool, bool, IntPtr_t);
	static Texture2D_Internal_Create_m267453861_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Internal_Create_m267453861_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)");
	_il2cpp_icall_func(___mono0, ___width1, ___height2, ___format3, ___mipmap4, ___linear5, ___nativeTex6);
}
// UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern "C"  Texture2D_t1431210461 * Texture2D_get_whiteTexture_m1234143055 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef Texture2D_t1431210461 * (*Texture2D_get_whiteTexture_m1234143055_ftn) ();
	static Texture2D_get_whiteTexture_m1234143055_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_get_whiteTexture_m1234143055_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::get_whiteTexture()");
	Texture2D_t1431210461 * retVal = _il2cpp_icall_func();
	return retVal;
}
// UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
extern "C"  Color_t4183816058  Texture2D_GetPixelBilinear_m1832870413 (Texture2D_t1431210461 * __this, float ___u0, float ___v1, const RuntimeMethod* method)
{
	Color_t4183816058  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t4183816058  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		float L_0 = ___u0;
		float L_1 = ___v1;
		Texture2D_INTERNAL_CALL_GetPixelBilinear_m743625832(NULL /*static, unused*/, __this, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		Color_t4183816058  L_2 = V_0;
		V_1 = L_2;
		goto IL_0012;
	}

IL_0012:
	{
		Color_t4183816058  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)
extern "C"  void Texture2D_INTERNAL_CALL_GetPixelBilinear_m743625832 (RuntimeObject * __this /* static, unused */, Texture2D_t1431210461 * ___self0, float ___u1, float ___v2, Color_t4183816058 * ___value3, const RuntimeMethod* method)
{
	typedef void (*Texture2D_INTERNAL_CALL_GetPixelBilinear_m743625832_ftn) (Texture2D_t1431210461 *, float, float, Color_t4183816058 *);
	static Texture2D_INTERNAL_CALL_GetPixelBilinear_m743625832_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_INTERNAL_CALL_GetPixelBilinear_m743625832_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::INTERNAL_CALL_GetPixelBilinear(UnityEngine.Texture2D,System.Single,System.Single,UnityEngine.Color&)");
	_il2cpp_icall_func(___self0, ___u1, ___v2, ___value3);
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern "C"  void Texture2D_SetPixels_m401486771 (Texture2D_t1431210461 * __this, ColorU5BU5D_t349694303* ___colors0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		ColorU5BU5D_t349694303* L_0 = ___colors0;
		int32_t L_1 = V_0;
		Texture2D_SetPixels_m547240586(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m547240586 (Texture2D_t1431210461 * __this, ColorU5BU5D_t349694303* ___colors0, int32_t ___miplevel1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel1;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0016;
		}
	}
	{
		V_0 = 1;
	}

IL_0016:
	{
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel1;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002b;
		}
	}
	{
		V_1 = 1;
	}

IL_002b:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		ColorU5BU5D_t349694303* L_8 = ___colors0;
		int32_t L_9 = ___miplevel1;
		Texture2D_SetPixels_m3460391273(__this, 0, 0, L_6, L_7, L_8, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern "C"  void Texture2D_SetPixels_m3460391273 (Texture2D_t1431210461 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, ColorU5BU5D_t349694303* ___colors4, int32_t ___miplevel5, const RuntimeMethod* method)
{
	typedef void (*Texture2D_SetPixels_m3460391273_ftn) (Texture2D_t1431210461 *, int32_t, int32_t, int32_t, int32_t, ColorU5BU5D_t349694303*, int32_t);
	static Texture2D_SetPixels_m3460391273_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_SetPixels_m3460391273_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)");
	_il2cpp_icall_func(__this, ___x0, ___y1, ___blockWidth2, ___blockHeight3, ___colors4, ___miplevel5);
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels()
extern "C"  ColorU5BU5D_t349694303* Texture2D_GetPixels_m3567383466 (Texture2D_t1431210461 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	ColorU5BU5D_t349694303* V_1 = NULL;
	{
		V_0 = 0;
		int32_t L_0 = V_0;
		ColorU5BU5D_t349694303* L_1 = Texture2D_GetPixels_m3355391893(__this, L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		goto IL_0010;
	}

IL_0010:
	{
		ColorU5BU5D_t349694303* L_2 = V_1;
		return L_2;
	}
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32)
extern "C"  ColorU5BU5D_t349694303* Texture2D_GetPixels_m3355391893 (Texture2D_t1431210461 * __this, int32_t ___miplevel0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ColorU5BU5D_t349694303* V_2 = NULL;
	{
		int32_t L_0 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, __this);
		int32_t L_1 = ___miplevel0;
		V_0 = ((int32_t)((int32_t)L_0>>(int32_t)((int32_t)((int32_t)L_1&(int32_t)((int32_t)31)))));
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) >= ((int32_t)1)))
		{
			goto IL_0016;
		}
	}
	{
		V_0 = 1;
	}

IL_0016:
	{
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, __this);
		int32_t L_4 = ___miplevel0;
		V_1 = ((int32_t)((int32_t)L_3>>(int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)31)))));
		int32_t L_5 = V_1;
		if ((((int32_t)L_5) >= ((int32_t)1)))
		{
			goto IL_002b;
		}
	}
	{
		V_1 = 1;
	}

IL_002b:
	{
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		int32_t L_8 = ___miplevel0;
		ColorU5BU5D_t349694303* L_9 = Texture2D_GetPixels_m4219050946(__this, 0, 0, L_6, L_7, L_8, /*hidden argument*/NULL);
		V_2 = L_9;
		goto IL_003c;
	}

IL_003c:
	{
		ColorU5BU5D_t349694303* L_10 = V_2;
		return L_10;
	}
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  ColorU5BU5D_t349694303* Texture2D_GetPixels_m4219050946 (Texture2D_t1431210461 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, int32_t ___miplevel4, const RuntimeMethod* method)
{
	typedef ColorU5BU5D_t349694303* (*Texture2D_GetPixels_m4219050946_ftn) (Texture2D_t1431210461 *, int32_t, int32_t, int32_t, int32_t, int32_t);
	static Texture2D_GetPixels_m4219050946_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_GetPixels_m4219050946_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)");
	ColorU5BU5D_t349694303* retVal = _il2cpp_icall_func(__this, ___x0, ___y1, ___blockWidth2, ___blockHeight3, ___miplevel4);
	return retVal;
}
// UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C"  ColorU5BU5D_t349694303* Texture2D_GetPixels_m3152914379 (Texture2D_t1431210461 * __this, int32_t ___x0, int32_t ___y1, int32_t ___blockWidth2, int32_t ___blockHeight3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	ColorU5BU5D_t349694303* V_1 = NULL;
	{
		V_0 = 0;
		int32_t L_0 = ___x0;
		int32_t L_1 = ___y1;
		int32_t L_2 = ___blockWidth2;
		int32_t L_3 = ___blockHeight3;
		int32_t L_4 = V_0;
		ColorU5BU5D_t349694303* L_5 = Texture2D_GetPixels_m4219050946(__this, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_0015;
	}

IL_0015:
	{
		ColorU5BU5D_t349694303* L_6 = V_1;
		return L_6;
	}
}
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern "C"  void Texture2D_Apply_m2015313753 (Texture2D_t1431210461 * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method)
{
	typedef void (*Texture2D_Apply_m2015313753_ftn) (Texture2D_t1431210461 *, bool, bool);
	static Texture2D_Apply_m2015313753_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_Apply_m2015313753_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)");
	_il2cpp_icall_func(__this, ___updateMipmaps0, ___makeNoLongerReadable1);
}
// System.Void UnityEngine.Texture2D::Apply()
extern "C"  void Texture2D_Apply_m136269680 (Texture2D_t1431210461 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		V_0 = (bool)0;
		V_1 = (bool)1;
		bool L_0 = V_1;
		bool L_1 = V_0;
		Texture2D_Apply_m2015313753(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32,System.Int32,System.Boolean)
extern "C"  RectU5BU5D_t75527478* Texture2D_PackTextures_m3868321432 (Texture2D_t1431210461 * __this, Texture2DU5BU5D_t2067778064* ___textures0, int32_t ___padding1, int32_t ___maximumAtlasSize2, bool ___makeNoLongerReadable3, const RuntimeMethod* method)
{
	typedef RectU5BU5D_t75527478* (*Texture2D_PackTextures_m3868321432_ftn) (Texture2D_t1431210461 *, Texture2DU5BU5D_t2067778064*, int32_t, int32_t, bool);
	static Texture2D_PackTextures_m3868321432_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Texture2D_PackTextures_m3868321432_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32,System.Int32,System.Boolean)");
	RectU5BU5D_t75527478* retVal = _il2cpp_icall_func(__this, ___textures0, ___padding1, ___maximumAtlasSize2, ___makeNoLongerReadable3);
	return retVal;
}
// UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32,System.Int32)
extern "C"  RectU5BU5D_t75527478* Texture2D_PackTextures_m4227007492 (Texture2D_t1431210461 * __this, Texture2DU5BU5D_t2067778064* ___textures0, int32_t ___padding1, int32_t ___maximumAtlasSize2, const RuntimeMethod* method)
{
	bool V_0 = false;
	RectU5BU5D_t75527478* V_1 = NULL;
	{
		V_0 = (bool)0;
		Texture2DU5BU5D_t2067778064* L_0 = ___textures0;
		int32_t L_1 = ___padding1;
		int32_t L_2 = ___maximumAtlasSize2;
		bool L_3 = V_0;
		RectU5BU5D_t75527478* L_4 = Texture2D_PackTextures_m3868321432(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		goto IL_0013;
	}

IL_0013:
	{
		RectU5BU5D_t75527478* L_5 = V_1;
		return L_5;
	}
}
// System.Void UnityEngine.ThreadAndSerializationSafeAttribute::.ctor()
extern "C"  void ThreadAndSerializationSafeAttribute__ctor_m2762749996 (ThreadAndSerializationSafeAttribute_t3127544772 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m959732044(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.Time::get_time()
extern "C"  float Time_get_time_m1252053291 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_time_m1252053291_ftn) ();
	static Time_get_time_m1252053291_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_time_m1252053291_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_time()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m3859058717 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_deltaTime_m3859058717_ftn) ();
	static Time_get_deltaTime_m3859058717_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m3859058717_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C"  float Time_get_unscaledTime_m1132266999 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_unscaledTime_m1132266999_ftn) ();
	static Time_get_unscaledTime_m1132266999_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledTime_m1132266999_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C"  float Time_get_unscaledDeltaTime_m174567466 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_unscaledDeltaTime_m174567466_ftn) ();
	static Time_get_unscaledDeltaTime_m174567466_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledDeltaTime_m174567466_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern "C"  float Time_get_realtimeSinceStartup_m1699101199 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef float (*Time_get_realtimeSinceStartup_m1699101199_ftn) ();
	static Time_get_realtimeSinceStartup_m1699101199_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_realtimeSinceStartup_m1699101199_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_realtimeSinceStartup()");
	float retVal = _il2cpp_icall_func();
	return retVal;
}
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern "C"  void TooltipAttribute__ctor_m1627964037 (TooltipAttribute_t1130662838 * __this, String_t* ___tooltip0, const RuntimeMethod* method)
{
	{
		PropertyAttribute__ctor_m824268376(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___tooltip0;
		__this->set_tooltip_0(L_0);
		return;
	}
}
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C"  int32_t Touch_get_fingerId_m3778479957 (Touch_t3995534465 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_FingerId_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Touch_get_fingerId_m3778479957_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t3995534465 * _thisAdjusted = reinterpret_cast<Touch_t3995534465 *>(__this + 1);
	return Touch_get_fingerId_m3778479957(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C"  Vector2_t1065050092  Touch_get_position_m1944077023 (Touch_t3995534465 * __this, const RuntimeMethod* method)
{
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector2_t1065050092  L_0 = __this->get_m_Position_1();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Vector2_t1065050092  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector2_t1065050092  Touch_get_position_m1944077023_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t3995534465 * _thisAdjusted = reinterpret_cast<Touch_t3995534465 *>(__this + 1);
	return Touch_get_position_m1944077023(_thisAdjusted, method);
}
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C"  int32_t Touch_get_phase_m1588321714 (Touch_t3995534465 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Phase_6();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Touch_get_phase_m1588321714_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t3995534465 * _thisAdjusted = reinterpret_cast<Touch_t3995534465 *>(__this + 1);
	return Touch_get_phase_m1588321714(_thisAdjusted, method);
}
// UnityEngine.TouchType UnityEngine.Touch::get_type()
extern "C"  int32_t Touch_get_type_m3088692135 (Touch_t3995534465 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_m_Type_7();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
extern "C"  int32_t Touch_get_type_m3088692135_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Touch_t3995534465 * _thisAdjusted = reinterpret_cast<Touch_t3995534465 *>(__this + 1);
	return Touch_get_type_m3088692135(_thisAdjusted, method);
}
// System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  void TouchScreenKeyboard__ctor_m3584002309 (TouchScreenKeyboard_t1544112657 * __this, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard__ctor_m3584002309_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TouchScreenKeyboard_InternalConstructorHelperArguments_t857896789  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Object__ctor_m1723520498(__this, /*hidden argument*/NULL);
		Initobj (TouchScreenKeyboard_InternalConstructorHelperArguments_t857896789_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_0 = ___keyboardType1;
		int32_t L_1 = L_0;
		RuntimeObject * L_2 = Box(TouchScreenKeyboardType_t3909346333_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t4030317850_il2cpp_TypeInfo_var);
		uint32_t L_3 = Convert_ToUInt32_m3960554041(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		(&V_0)->set_keyboardType_0(L_3);
		bool L_4 = ___autocorrection2;
		uint32_t L_5 = Convert_ToUInt32_m4274822710(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		(&V_0)->set_autocorrection_1(L_5);
		bool L_6 = ___multiline3;
		uint32_t L_7 = Convert_ToUInt32_m4274822710(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		(&V_0)->set_multiline_2(L_7);
		bool L_8 = ___secure4;
		uint32_t L_9 = Convert_ToUInt32_m4274822710(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		(&V_0)->set_secure_3(L_9);
		bool L_10 = ___alert5;
		uint32_t L_11 = Convert_ToUInt32_m4274822710(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		(&V_0)->set_alert_4(L_11);
		String_t* L_12 = ___text0;
		String_t* L_13 = ___textPlaceholder6;
		TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2651191005(__this, (&V_0), L_12, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern "C"  void TouchScreenKeyboard_Destroy_m1267789774 (TouchScreenKeyboard_t1544112657 * __this, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_Destroy_m1267789774_ftn) (TouchScreenKeyboard_t1544112657 *);
	static TouchScreenKeyboard_Destroy_m1267789774_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_Destroy_m1267789774_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::Destroy()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern "C"  void TouchScreenKeyboard_Finalize_m958348491 (TouchScreenKeyboard_t1544112657 * __this, const RuntimeMethod* method)
{
	Exception_t2572292308 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2572292308 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
	}

IL_0001:
	try
	{ // begin try (depth: 1)
		TouchScreenKeyboard_Destroy_m1267789774(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x13, FINALLY_000c);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2572292308 *)e.ex;
		goto FINALLY_000c;
	}

FINALLY_000c:
	{ // begin finally (depth: 1)
		Object_Finalize_m2119677907(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(12)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(12)
	{
		IL2CPP_JUMP_TBL(0x13, IL_0013)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2572292308 *)
	}

IL_0013:
	{
		return;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern "C"  void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2651191005 (TouchScreenKeyboard_t1544112657 * __this, TouchScreenKeyboard_InternalConstructorHelperArguments_t857896789 * ___arguments0, String_t* ___text1, String_t* ___textPlaceholder2, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2651191005_ftn) (TouchScreenKeyboard_t1544112657 *, TouchScreenKeyboard_InternalConstructorHelperArguments_t857896789 *, String_t*, String_t*);
	static TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2651191005_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2651191005_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)");
	_il2cpp_icall_func(__this, ___arguments0, ___text1, ___textPlaceholder2);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_isSupported()
extern "C"  bool TouchScreenKeyboard_get_isSupported_m3681943622 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = Application_get_platform_m3065878097(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)((int32_t)18))))
		{
			case 0:
			{
				goto IL_006d;
			}
			case 1:
			{
				goto IL_006d;
			}
			case 2:
			{
				goto IL_006d;
			}
			case 3:
			{
				goto IL_0034;
			}
			case 4:
			{
				goto IL_0034;
			}
			case 5:
			{
				goto IL_0066;
			}
			case 6:
			{
				goto IL_0034;
			}
			case 7:
			{
				goto IL_0034;
			}
			case 8:
			{
				goto IL_0066;
			}
		}
	}

IL_0034:
	{
		int32_t L_2 = V_0;
		switch (((int32_t)((int32_t)L_2-(int32_t)((int32_t)30))))
		{
			case 0:
			{
				goto IL_0066;
			}
			case 1:
			{
				goto IL_0066;
			}
			case 2:
			{
				goto IL_0066;
			}
		}
	}
	{
		int32_t L_3 = V_0;
		switch (((int32_t)((int32_t)L_3-(int32_t)8)))
		{
			case 0:
			{
				goto IL_0066;
			}
			case 1:
			{
				goto IL_0074;
			}
			case 2:
			{
				goto IL_0074;
			}
			case 3:
			{
				goto IL_0066;
			}
		}
	}
	{
		goto IL_0074;
	}

IL_0066:
	{
		V_1 = (bool)1;
		goto IL_007b;
	}

IL_006d:
	{
		V_1 = (bool)0;
		goto IL_007b;
	}

IL_0074:
	{
		V_1 = (bool)0;
		goto IL_007b;
	}

IL_007b:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean)
extern "C"  TouchScreenKeyboard_t1544112657 * TouchScreenKeyboard_Open_m508191034 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m508191034_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	TouchScreenKeyboard_t1544112657 * V_2 = NULL;
	{
		V_0 = _stringLiteral5347058;
		V_1 = (bool)0;
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = ___secure4;
		bool L_5 = V_1;
		String_t* L_6 = V_0;
		TouchScreenKeyboard_t1544112657 * L_7 = TouchScreenKeyboard_Open_m1817258185(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_2 = L_7;
		goto IL_001c;
	}

IL_001c:
	{
		TouchScreenKeyboard_t1544112657 * L_8 = V_2;
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean)
extern "C"  TouchScreenKeyboard_t1544112657 * TouchScreenKeyboard_Open_m1092144858 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m1092144858_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	bool V_1 = false;
	bool V_2 = false;
	TouchScreenKeyboard_t1544112657 * V_3 = NULL;
	{
		V_0 = _stringLiteral5347058;
		V_1 = (bool)0;
		V_2 = (bool)0;
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = V_2;
		bool L_5 = V_1;
		String_t* L_6 = V_0;
		TouchScreenKeyboard_t1544112657 * L_7 = TouchScreenKeyboard_Open_m1817258185(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_3 = L_7;
		goto IL_001d;
	}

IL_001d:
	{
		TouchScreenKeyboard_t1544112657 * L_8 = V_3;
		return L_8;
	}
}
// UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String)
extern "C"  TouchScreenKeyboard_t1544112657 * TouchScreenKeyboard_Open_m1817258185 (RuntimeObject * __this /* static, unused */, String_t* ___text0, int32_t ___keyboardType1, bool ___autocorrection2, bool ___multiline3, bool ___secure4, bool ___alert5, String_t* ___textPlaceholder6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TouchScreenKeyboard_Open_m1817258185_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TouchScreenKeyboard_t1544112657 * V_0 = NULL;
	{
		String_t* L_0 = ___text0;
		int32_t L_1 = ___keyboardType1;
		bool L_2 = ___autocorrection2;
		bool L_3 = ___multiline3;
		bool L_4 = ___secure4;
		bool L_5 = ___alert5;
		String_t* L_6 = ___textPlaceholder6;
		TouchScreenKeyboard_t1544112657 * L_7 = (TouchScreenKeyboard_t1544112657 *)il2cpp_codegen_object_new(TouchScreenKeyboard_t1544112657_il2cpp_TypeInfo_var);
		TouchScreenKeyboard__ctor_m3584002309(L_7, L_0, L_1, L_2, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		goto IL_0016;
	}

IL_0016:
	{
		TouchScreenKeyboard_t1544112657 * L_8 = V_0;
		return L_8;
	}
}
// System.String UnityEngine.TouchScreenKeyboard::get_text()
extern "C"  String_t* TouchScreenKeyboard_get_text_m1554725892 (TouchScreenKeyboard_t1544112657 * __this, const RuntimeMethod* method)
{
	typedef String_t* (*TouchScreenKeyboard_get_text_m1554725892_ftn) (TouchScreenKeyboard_t1544112657 *);
	static TouchScreenKeyboard_get_text_m1554725892_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_text_m1554725892_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_text()");
	String_t* retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
extern "C"  void TouchScreenKeyboard_set_text_m2522012283 (TouchScreenKeyboard_t1544112657 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_set_text_m2522012283_ftn) (TouchScreenKeyboard_t1544112657 *, String_t*);
	static TouchScreenKeyboard_set_text_m2522012283_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_text_m2522012283_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_text(System.String)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
extern "C"  void TouchScreenKeyboard_set_hideInput_m1899734362 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_set_hideInput_m1899734362_ftn) (bool);
	static TouchScreenKeyboard_set_hideInput_m1899734362_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_hideInput_m1899734362_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)");
	_il2cpp_icall_func(___value0);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
extern "C"  bool TouchScreenKeyboard_get_active_m1289155846 (TouchScreenKeyboard_t1544112657 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_active_m1289155846_ftn) (TouchScreenKeyboard_t1544112657 *);
	static TouchScreenKeyboard_get_active_m1289155846_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_active_m1289155846_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_active()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
extern "C"  void TouchScreenKeyboard_set_active_m1966899231 (TouchScreenKeyboard_t1544112657 * __this, bool ___value0, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_set_active_m1966899231_ftn) (TouchScreenKeyboard_t1544112657 *, bool);
	static TouchScreenKeyboard_set_active_m1966899231_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_set_active_m1966899231_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_done()
extern "C"  bool TouchScreenKeyboard_get_done_m2103305624 (TouchScreenKeyboard_t1544112657 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_done_m2103305624_ftn) (TouchScreenKeyboard_t1544112657 *);
	static TouchScreenKeyboard_get_done_m2103305624_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_done_m2103305624_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_done()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_wasCanceled()
extern "C"  bool TouchScreenKeyboard_get_wasCanceled_m1868500418 (TouchScreenKeyboard_t1544112657 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_wasCanceled_m1868500418_ftn) (TouchScreenKeyboard_t1544112657 *);
	static TouchScreenKeyboard_get_wasCanceled_m1868500418_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_wasCanceled_m1868500418_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_wasCanceled()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Boolean UnityEngine.TouchScreenKeyboard::get_canGetSelection()
extern "C"  bool TouchScreenKeyboard_get_canGetSelection_m3879802255 (TouchScreenKeyboard_t1544112657 * __this, const RuntimeMethod* method)
{
	typedef bool (*TouchScreenKeyboard_get_canGetSelection_m3879802255_ftn) (TouchScreenKeyboard_t1544112657 *);
	static TouchScreenKeyboard_get_canGetSelection_m3879802255_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_get_canGetSelection_m3879802255_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::get_canGetSelection()");
	bool retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// UnityEngine.RangeInt UnityEngine.TouchScreenKeyboard::get_selection()
extern "C"  RangeInt_t4007359427  TouchScreenKeyboard_get_selection_m2900796855 (TouchScreenKeyboard_t1544112657 * __this, const RuntimeMethod* method)
{
	RangeInt_t4007359427  V_0;
	memset(&V_0, 0, sizeof(V_0));
	RangeInt_t4007359427  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		int32_t* L_0 = (&V_0)->get_address_of_start_0();
		int32_t* L_1 = (&V_0)->get_address_of_length_1();
		TouchScreenKeyboard_GetSelectionInternal_m1537931821(__this, L_0, L_1, /*hidden argument*/NULL);
		RangeInt_t4007359427  L_2 = V_0;
		V_1 = L_2;
		goto IL_001c;
	}

IL_001c:
	{
		RangeInt_t4007359427  L_3 = V_1;
		return L_3;
	}
}
// System.Void UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)
extern "C"  void TouchScreenKeyboard_GetSelectionInternal_m1537931821 (TouchScreenKeyboard_t1544112657 * __this, int32_t* ___start0, int32_t* ___length1, const RuntimeMethod* method)
{
	typedef void (*TouchScreenKeyboard_GetSelectionInternal_m1537931821_ftn) (TouchScreenKeyboard_t1544112657 *, int32_t*, int32_t*);
	static TouchScreenKeyboard_GetSelectionInternal_m1537931821_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TouchScreenKeyboard_GetSelectionInternal_m1537931821_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TouchScreenKeyboard::GetSelectionInternal(System.Int32&,System.Int32&)");
	_il2cpp_icall_func(__this, ___start0, ___length1);
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t4182582575_marshal_pinvoke(const TrackedReference_t4182582575& unmarshaled, TrackedReference_t4182582575_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void TrackedReference_t4182582575_marshal_pinvoke_back(const TrackedReference_t4182582575_marshaled_pinvoke& marshaled, TrackedReference_t4182582575& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t4182582575_marshal_pinvoke_cleanup(TrackedReference_t4182582575_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t4182582575_marshal_com(const TrackedReference_t4182582575& unmarshaled, TrackedReference_t4182582575_marshaled_com& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.get_m_Ptr_0()).get_m_value_0());
}
extern "C" void TrackedReference_t4182582575_marshal_com_back(const TrackedReference_t4182582575_marshaled_com& marshaled, TrackedReference_t4182582575& unmarshaled)
{
	IntPtr_t unmarshaled_m_Ptr_temp_0;
	memset(&unmarshaled_m_Ptr_temp_0, 0, sizeof(unmarshaled_m_Ptr_temp_0));
	IntPtr_t unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled_m_Ptr_temp_0_temp.set_m_value_0(reinterpret_cast<void*>((intptr_t)(marshaled.___m_Ptr_0)));
	unmarshaled_m_Ptr_temp_0 = unmarshaled_m_Ptr_temp_0_temp;
	unmarshaled.set_m_Ptr_0(unmarshaled_m_Ptr_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.TrackedReference
extern "C" void TrackedReference_t4182582575_marshal_com_cleanup(TrackedReference_t4182582575_marshaled_com& marshaled)
{
}
// System.Boolean UnityEngine.TrackedReference::op_Equality(UnityEngine.TrackedReference,UnityEngine.TrackedReference)
extern "C"  bool TrackedReference_op_Equality_m2363965236 (RuntimeObject * __this /* static, unused */, TrackedReference_t4182582575 * ___x0, TrackedReference_t4182582575 * ___y1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_op_Equality_m2363965236_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	RuntimeObject * V_1 = NULL;
	bool V_2 = false;
	{
		TrackedReference_t4182582575 * L_0 = ___x0;
		V_0 = L_0;
		TrackedReference_t4182582575 * L_1 = ___y1;
		V_1 = L_1;
		RuntimeObject * L_2 = V_1;
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		RuntimeObject * L_3 = V_0;
		if (L_3)
		{
			goto IL_0018;
		}
	}
	{
		V_2 = (bool)1;
		goto IL_0067;
	}

IL_0018:
	{
		RuntimeObject * L_4 = V_1;
		if (L_4)
		{
			goto IL_0034;
		}
	}
	{
		TrackedReference_t4182582575 * L_5 = ___x0;
		NullCheck(L_5);
		IntPtr_t L_6 = L_5->get_m_Ptr_0();
		IntPtr_t L_7 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		bool L_8 = IntPtr_op_Equality_m3855036101(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		goto IL_0067;
	}

IL_0034:
	{
		RuntimeObject * L_9 = V_0;
		if (L_9)
		{
			goto IL_0050;
		}
	}
	{
		TrackedReference_t4182582575 * L_10 = ___y1;
		NullCheck(L_10);
		IntPtr_t L_11 = L_10->get_m_Ptr_0();
		IntPtr_t L_12 = ((IntPtr_t_StaticFields*)il2cpp_codegen_static_fields_for(IntPtr_t_il2cpp_TypeInfo_var))->get_Zero_1();
		bool L_13 = IntPtr_op_Equality_m3855036101(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		goto IL_0067;
	}

IL_0050:
	{
		TrackedReference_t4182582575 * L_14 = ___x0;
		NullCheck(L_14);
		IntPtr_t L_15 = L_14->get_m_Ptr_0();
		TrackedReference_t4182582575 * L_16 = ___y1;
		NullCheck(L_16);
		IntPtr_t L_17 = L_16->get_m_Ptr_0();
		bool L_18 = IntPtr_op_Equality_m3855036101(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		V_2 = L_18;
		goto IL_0067;
	}

IL_0067:
	{
		bool L_19 = V_2;
		return L_19;
	}
}
// System.Boolean UnityEngine.TrackedReference::Equals(System.Object)
extern "C"  bool TrackedReference_Equals_m3588214792 (TrackedReference_t4182582575 * __this, RuntimeObject * ___o0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TrackedReference_Equals_m3588214792_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		RuntimeObject * L_0 = ___o0;
		bool L_1 = TrackedReference_op_Equality_m2363965236(NULL /*static, unused*/, ((TrackedReference_t4182582575 *)IsInstClass((RuntimeObject*)L_0, TrackedReference_t4182582575_il2cpp_TypeInfo_var)), __this, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0013;
	}

IL_0013:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Int32 UnityEngine.TrackedReference::GetHashCode()
extern "C"  int32_t TrackedReference_GetHashCode_m4240780278 (TrackedReference_t4182582575 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		IntPtr_t L_0 = __this->get_m_Ptr_0();
		int32_t L_1 = IntPtr_op_Explicit_m2301659926(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0012;
	}

IL_0012:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2825674791  Transform_get_position_m1871351526 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2825674791  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_position_m3045837290(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2825674791  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t2825674791  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m219509932 (Transform_t2735953680 * __this, Vector3_t2825674791  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_position_m433280170(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_position_m3045837290 (Transform_t2735953680 * __this, Vector3_t2825674791 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_position_m3045837290_ftn) (Transform_t2735953680 *, Vector3_t2825674791 *);
	static Transform_INTERNAL_get_position_m3045837290_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m3045837290_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_position_m433280170 (Transform_t2735953680 * __this, Vector3_t2825674791 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_position_m433280170_ftn) (Transform_t2735953680 *, Vector3_t2825674791 *);
	static Transform_INTERNAL_set_position_m433280170_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m433280170_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t2825674791  Transform_get_localPosition_m2770389148 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2825674791  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localPosition_m3942343510(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2825674791  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t2825674791  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C"  void Transform_set_localPosition_m1596244799 (Transform_t2735953680 * __this, Vector3_t2825674791  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_localPosition_m3932234355(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localPosition_m3942343510 (Transform_t2735953680 * __this, Vector3_t2825674791 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m3942343510_ftn) (Transform_t2735953680 *, Vector3_t2825674791 *);
	static Transform_INTERNAL_get_localPosition_m3942343510_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m3942343510_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localPosition_m3932234355 (Transform_t2735953680 * __this, Vector3_t2825674791 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m3932234355_ftn) (Transform_t2735953680 *, Vector3_t2825674791 *);
	static Transform_INTERNAL_set_localPosition_m3932234355_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m3932234355_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern "C"  Vector3_t2825674791  Transform_get_right_m3355289283 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_get_right_m3355289283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t580650070  L_0 = Transform_get_rotation_m1403455390(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_1 = Vector3_get_right_m372445178(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t580650070_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_2 = Quaternion_op_Multiply_m3137134439(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Vector3_t2825674791  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern "C"  Vector3_t2825674791  Transform_get_up_m2269106334 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_get_up_m2269106334_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t580650070  L_0 = Transform_get_rotation_m1403455390(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_1 = Vector3_get_up_m2143830244(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t580650070_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_2 = Quaternion_op_Multiply_m3137134439(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Vector3_t2825674791  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C"  Vector3_t2825674791  Transform_get_forward_m1737462907 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_get_forward_m1737462907_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Quaternion_t580650070  L_0 = Transform_get_rotation_m1403455390(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_1 = Vector3_get_forward_m3907749096(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t580650070_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_2 = Quaternion_op_Multiply_m3137134439(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0017;
	}

IL_0017:
	{
		Vector3_t2825674791  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C"  Quaternion_t580650070  Transform_get_rotation_m1403455390 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	Quaternion_t580650070  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t580650070  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_rotation_m2559117592(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t580650070  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Quaternion_t580650070  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_rotation_m1708311517 (Transform_t2735953680 * __this, Quaternion_t580650070  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_rotation_m3346348661(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_rotation_m2559117592 (Transform_t2735953680 * __this, Quaternion_t580650070 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m2559117592_ftn) (Transform_t2735953680 *, Quaternion_t580650070 *);
	static Transform_INTERNAL_get_rotation_m2559117592_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m2559117592_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_rotation_m3346348661 (Transform_t2735953680 * __this, Quaternion_t580650070 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_rotation_m3346348661_ftn) (Transform_t2735953680 *, Quaternion_t580650070 *);
	static Transform_INTERNAL_set_rotation_m3346348661_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_rotation_m3346348661_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C"  Quaternion_t580650070  Transform_get_localRotation_m1212773390 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	Quaternion_t580650070  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Quaternion_t580650070  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localRotation_m523486095(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t580650070  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Quaternion_t580650070  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C"  void Transform_set_localRotation_m1234546214 (Transform_t2735953680 * __this, Quaternion_t580650070  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_localRotation_m3517956161(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_get_localRotation_m523486095 (Transform_t2735953680 * __this, Quaternion_t580650070 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m523486095_ftn) (Transform_t2735953680 *, Quaternion_t580650070 *);
	static Transform_INTERNAL_get_localRotation_m523486095_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m523486095_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_set_localRotation_m3517956161 (Transform_t2735953680 * __this, Quaternion_t580650070 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m3517956161_ftn) (Transform_t2735953680 *, Quaternion_t580650070 *);
	static Transform_INTERNAL_set_localRotation_m3517956161_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m3517956161_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C"  Vector3_t2825674791  Transform_get_localScale_m2987916711 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2825674791  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localScale_m21643413(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t2825674791  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Vector3_t2825674791  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C"  void Transform_set_localScale_m752137719 (Transform_t2735953680 * __this, Vector3_t2825674791  ___value0, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_set_localScale_m1990679844(__this, (&___value0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_get_localScale_m21643413 (Transform_t2735953680 * __this, Vector3_t2825674791 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m21643413_ftn) (Transform_t2735953680 *, Vector3_t2825674791 *);
	static Transform_INTERNAL_get_localScale_m21643413_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m21643413_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_set_localScale_m1990679844 (Transform_t2735953680 * __this, Vector3_t2825674791 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m1990679844_ftn) (Transform_t2735953680 *, Vector3_t2825674791 *);
	static Transform_INTERNAL_set_localScale_m1990679844_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m1990679844_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t2735953680 * Transform_get_parent_m1672250917 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	Transform_t2735953680 * V_0 = NULL;
	{
		Transform_t2735953680 * L_0 = Transform_get_parentInternal_m596331430(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		Transform_t2735953680 * L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern "C"  void Transform_set_parent_m907535055 (Transform_t2735953680 * __this, Transform_t2735953680 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_set_parent_m907535055_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		if (!((RectTransform_t1087788885 *)IsInstSealed((RuntimeObject*)__this, RectTransform_t1087788885_il2cpp_TypeInfo_var)))
		{
			goto IL_0017;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t16286340_il2cpp_TypeInfo_var);
		Debug_LogWarning_m415376909(NULL /*static, unused*/, _stringLiteral3763031511, __this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		Transform_t2735953680 * L_0 = ___value0;
		Transform_set_parentInternal_m1203231964(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C"  Transform_t2735953680 * Transform_get_parentInternal_m596331430 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	typedef Transform_t2735953680 * (*Transform_get_parentInternal_m596331430_ftn) (Transform_t2735953680 *);
	static Transform_get_parentInternal_m596331430_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m596331430_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	Transform_t2735953680 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C"  void Transform_set_parentInternal_m1203231964 (Transform_t2735953680 * __this, Transform_t2735953680 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_set_parentInternal_m1203231964_ftn) (Transform_t2735953680 *, Transform_t2735953680 *);
	static Transform_set_parentInternal_m1203231964_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_set_parentInternal_m1203231964_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C"  void Transform_SetParent_m86943706 (Transform_t2735953680 * __this, Transform_t2735953680 * ___parent0, const RuntimeMethod* method)
{
	{
		Transform_t2735953680 * L_0 = ___parent0;
		Transform_SetParent_m3800356349(__this, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C"  void Transform_SetParent_m3800356349 (Transform_t2735953680 * __this, Transform_t2735953680 * ___parent0, bool ___worldPositionStays1, const RuntimeMethod* method)
{
	typedef void (*Transform_SetParent_m3800356349_ftn) (Transform_t2735953680 *, Transform_t2735953680 *, bool);
	static Transform_SetParent_m3800356349_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetParent_m3800356349_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___parent0, ___worldPositionStays1);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C"  Matrix4x4_t3574440610  Transform_get_worldToLocalMatrix_m1188629296 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	Matrix4x4_t3574440610  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t3574440610  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_worldToLocalMatrix_m1218008629(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t3574440610  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Matrix4x4_t3574440610  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_worldToLocalMatrix_m1218008629 (Transform_t2735953680 * __this, Matrix4x4_t3574440610 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_worldToLocalMatrix_m1218008629_ftn) (Transform_t2735953680 *, Matrix4x4_t3574440610 *);
	static Transform_INTERNAL_get_worldToLocalMatrix_m1218008629_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_worldToLocalMatrix_m1218008629_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern "C"  Matrix4x4_t3574440610  Transform_get_localToWorldMatrix_m2304868047 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	Matrix4x4_t3574440610  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Matrix4x4_t3574440610  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_get_localToWorldMatrix_m2457902037(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t3574440610  L_0 = V_0;
		V_1 = L_0;
		goto IL_0010;
	}

IL_0010:
	{
		Matrix4x4_t3574440610  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)
extern "C"  void Transform_INTERNAL_get_localToWorldMatrix_m2457902037 (Transform_t2735953680 * __this, Matrix4x4_t3574440610 * ___value0, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_get_localToWorldMatrix_m2457902037_ftn) (Transform_t2735953680 *, Matrix4x4_t3574440610 *);
	static Transform_INTERNAL_get_localToWorldMatrix_m2457902037_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localToWorldMatrix_m2457902037_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localToWorldMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.Transform::SetPositionAndRotation(UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C"  void Transform_SetPositionAndRotation_m3995123162 (Transform_t2735953680 * __this, Vector3_t2825674791  ___position0, Quaternion_t580650070  ___rotation1, const RuntimeMethod* method)
{
	{
		Transform_INTERNAL_CALL_SetPositionAndRotation_m1696884030(NULL /*static, unused*/, __this, (&___position0), (&___rotation1), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_SetPositionAndRotation(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C"  void Transform_INTERNAL_CALL_SetPositionAndRotation_m1696884030 (RuntimeObject * __this /* static, unused */, Transform_t2735953680 * ___self0, Vector3_t2825674791 * ___position1, Quaternion_t580650070 * ___rotation2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_SetPositionAndRotation_m1696884030_ftn) (Transform_t2735953680 *, Vector3_t2825674791 *, Quaternion_t580650070 *);
	static Transform_INTERNAL_CALL_SetPositionAndRotation_m1696884030_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_SetPositionAndRotation_m1696884030_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_SetPositionAndRotation(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	_il2cpp_icall_func(___self0, ___position1, ___rotation2);
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
extern "C"  void Transform_Translate_m581880437 (Transform_t2735953680 * __this, Vector3_t2825674791  ___translation0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		Vector3_t2825674791  L_0 = ___translation0;
		int32_t L_1 = V_0;
		Transform_Translate_m1089578796(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Translate_m1089578796 (Transform_t2735953680 * __this, Vector3_t2825674791  ___translation0, int32_t ___relativeTo1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_Translate_m1089578796_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___relativeTo1;
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		Vector3_t2825674791  L_1 = Transform_get_position_m1871351526(__this, /*hidden argument*/NULL);
		Vector3_t2825674791  L_2 = ___translation0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_3 = Vector3_op_Addition_m1434256537(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		Transform_set_position_m219509932(__this, L_3, /*hidden argument*/NULL);
		goto IL_0036;
	}

IL_001e:
	{
		Vector3_t2825674791  L_4 = Transform_get_position_m1871351526(__this, /*hidden argument*/NULL);
		Vector3_t2825674791  L_5 = ___translation0;
		Vector3_t2825674791  L_6 = Transform_TransformDirection_m3357137791(__this, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_7 = Vector3_op_Addition_m1434256537(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		Transform_set_position_m219509932(__this, L_7, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single)
extern "C"  void Transform_Translate_m2410252070 (Transform_t2735953680 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		int32_t L_3 = V_0;
		Transform_Translate_m4108251285(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern "C"  void Transform_Translate_m4108251285 (Transform_t2735953680 * __this, float ___x0, float ___y1, float ___z2, int32_t ___relativeTo3, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		float L_1 = ___y1;
		float L_2 = ___z2;
		Vector3_t2825674791  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m874567701((&L_3), L_0, L_1, L_2, /*hidden argument*/NULL);
		int32_t L_4 = ___relativeTo3;
		Transform_Translate_m1089578796(__this, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern "C"  void Transform_Rotate_m1869440073 (Transform_t2735953680 * __this, Vector3_t2825674791  ___eulerAngles0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 1;
		Vector3_t2825674791  L_0 = ___eulerAngles0;
		int32_t L_1 = V_0;
		Transform_Rotate_m546776242(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern "C"  void Transform_Rotate_m546776242 (Transform_t2735953680 * __this, Vector3_t2825674791  ___eulerAngles0, int32_t ___relativeTo1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_Rotate_m546776242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Quaternion_t580650070  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___eulerAngles0)->get_x_1();
		float L_1 = (&___eulerAngles0)->get_y_2();
		float L_2 = (&___eulerAngles0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t580650070_il2cpp_TypeInfo_var);
		Quaternion_t580650070  L_3 = Quaternion_Euler_m327605361(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = ___relativeTo1;
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_003a;
		}
	}
	{
		Quaternion_t580650070  L_5 = Transform_get_localRotation_m1212773390(__this, /*hidden argument*/NULL);
		Quaternion_t580650070  L_6 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t580650070_il2cpp_TypeInfo_var);
		Quaternion_t580650070  L_7 = Quaternion_op_Multiply_m2046237407(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Transform_set_localRotation_m1234546214(__this, L_7, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_003a:
	{
		Quaternion_t580650070  L_8 = Transform_get_rotation_m1403455390(__this, /*hidden argument*/NULL);
		Quaternion_t580650070  L_9 = Transform_get_rotation_m1403455390(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Quaternion_t580650070_il2cpp_TypeInfo_var);
		Quaternion_t580650070  L_10 = Quaternion_Inverse_m758774898(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		Quaternion_t580650070  L_11 = V_0;
		Quaternion_t580650070  L_12 = Quaternion_op_Multiply_m2046237407(NULL /*static, unused*/, L_10, L_11, /*hidden argument*/NULL);
		Quaternion_t580650070  L_13 = Transform_get_rotation_m1403455390(__this, /*hidden argument*/NULL);
		Quaternion_t580650070  L_14 = Quaternion_op_Multiply_m2046237407(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Quaternion_t580650070  L_15 = Quaternion_op_Multiply_m2046237407(NULL /*static, unused*/, L_8, L_14, /*hidden argument*/NULL);
		Transform_set_rotation_m1708311517(__this, L_15, /*hidden argument*/NULL);
	}

IL_0069:
	{
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Transform_TransformDirection_m3357137791 (Transform_t2735953680 * __this, Vector3_t2825674791  ___direction0, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2825674791  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_TransformDirection_m3118760480(NULL /*static, unused*/, __this, (&___direction0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2825674791  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t2825674791  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformDirection_m3118760480 (RuntimeObject * __this /* static, unused */, Transform_t2735953680 * ___self0, Vector3_t2825674791 * ___direction1, Vector3_t2825674791 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_TransformDirection_m3118760480_ftn) (Transform_t2735953680 *, Vector3_t2825674791 *, Vector3_t2825674791 *);
	static Transform_INTERNAL_CALL_TransformDirection_m3118760480_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformDirection_m3118760480_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___direction1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformDirection(UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Transform_InverseTransformDirection_m3834254437 (Transform_t2735953680 * __this, Vector3_t2825674791  ___direction0, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2825674791  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_InverseTransformDirection_m2248881875(NULL /*static, unused*/, __this, (&___direction0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2825674791  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t2825674791  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformDirection_m2248881875 (RuntimeObject * __this /* static, unused */, Transform_t2735953680 * ___self0, Vector3_t2825674791 * ___direction1, Vector3_t2825674791 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_InverseTransformDirection_m2248881875_ftn) (Transform_t2735953680 *, Vector3_t2825674791 *, Vector3_t2825674791 *);
	static Transform_INTERNAL_CALL_InverseTransformDirection_m2248881875_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformDirection_m2248881875_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformDirection(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___direction1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Transform_TransformPoint_m3961285739 (Transform_t2735953680 * __this, Vector3_t2825674791  ___position0, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2825674791  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_TransformPoint_m3182684986(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2825674791  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t2825674791  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_TransformPoint_m3182684986 (RuntimeObject * __this /* static, unused */, Transform_t2735953680 * ___self0, Vector3_t2825674791 * ___position1, Vector3_t2825674791 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_TransformPoint_m3182684986_ftn) (Transform_t2735953680 *, Vector3_t2825674791 *, Vector3_t2825674791 *);
	static Transform_INTERNAL_CALL_TransformPoint_m3182684986_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m3182684986_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Transform_InverseTransformPoint_m2512676342 (Transform_t2735953680 * __this, Vector3_t2825674791  ___position0, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2825674791  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Transform_INTERNAL_CALL_InverseTransformPoint_m269031554(NULL /*static, unused*/, __this, (&___position0), (&V_0), /*hidden argument*/NULL);
		Vector3_t2825674791  L_0 = V_0;
		V_1 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t2825674791  L_1 = V_1;
		return L_1;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern "C"  void Transform_INTERNAL_CALL_InverseTransformPoint_m269031554 (RuntimeObject * __this /* static, unused */, Transform_t2735953680 * ___self0, Vector3_t2825674791 * ___position1, Vector3_t2825674791 * ___value2, const RuntimeMethod* method)
{
	typedef void (*Transform_INTERNAL_CALL_InverseTransformPoint_m269031554_ftn) (Transform_t2735953680 *, Vector3_t2825674791 *, Vector3_t2825674791 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m269031554_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m269031554_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Vector3&)");
	_il2cpp_icall_func(___self0, ___position1, ___value2);
}
// UnityEngine.Transform UnityEngine.Transform::get_root()
extern "C"  Transform_t2735953680 * Transform_get_root_m3363608483 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	typedef Transform_t2735953680 * (*Transform_get_root_m3363608483_ftn) (Transform_t2735953680 *);
	static Transform_get_root_m3363608483_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_root_m3363608483_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_root()");
	Transform_t2735953680 * retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C"  int32_t Transform_get_childCount_m987201346 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	typedef int32_t (*Transform_get_childCount_m987201346_ftn) (Transform_t2735953680 *);
	static Transform_get_childCount_m987201346_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m987201346_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	int32_t retVal = _il2cpp_icall_func(__this);
	return retVal;
}
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C"  void Transform_SetAsFirstSibling_m4032410536 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	typedef void (*Transform_SetAsFirstSibling_m4032410536_ftn) (Transform_t2735953680 *);
	static Transform_SetAsFirstSibling_m4032410536_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsFirstSibling_m4032410536_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern "C"  bool Transform_IsChildOf_m485072707 (Transform_t2735953680 * __this, Transform_t2735953680 * ___parent0, const RuntimeMethod* method)
{
	typedef bool (*Transform_IsChildOf_m485072707_ftn) (Transform_t2735953680 *, Transform_t2735953680 *);
	static Transform_IsChildOf_m485072707_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_IsChildOf_m485072707_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::IsChildOf(UnityEngine.Transform)");
	bool retVal = _il2cpp_icall_func(__this, ___parent0);
	return retVal;
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C"  RuntimeObject* Transform_GetEnumerator_m3370647177 (Transform_t2735953680 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Transform_GetEnumerator_m3370647177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	{
		Enumerator_t1540455786 * L_0 = (Enumerator_t1540455786 *)il2cpp_codegen_object_new(Enumerator_t1540455786_il2cpp_TypeInfo_var);
		Enumerator__ctor_m3975348791(L_0, __this, /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		RuntimeObject* L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C"  Transform_t2735953680 * Transform_GetChild_m2012975997 (Transform_t2735953680 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	typedef Transform_t2735953680 * (*Transform_GetChild_m2012975997_ftn) (Transform_t2735953680 *, int32_t);
	static Transform_GetChild_m2012975997_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m2012975997_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	Transform_t2735953680 * retVal = _il2cpp_icall_func(__this, ___index0);
	return retVal;
}
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C"  void Enumerator__ctor_m3975348791 (Enumerator_t1540455786 * __this, Transform_t2735953680 * ___outer0, const RuntimeMethod* method)
{
	{
		__this->set_currentIndex_1((-1));
		Object__ctor_m1723520498(__this, /*hidden argument*/NULL);
		Transform_t2735953680 * L_0 = ___outer0;
		__this->set_outer_0(L_0);
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C"  RuntimeObject * Enumerator_get_Current_m1658020720 (Enumerator_t1540455786 * __this, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	{
		Transform_t2735953680 * L_0 = __this->get_outer_0();
		int32_t L_1 = __this->get_currentIndex_1();
		NullCheck(L_0);
		Transform_t2735953680 * L_2 = Transform_GetChild_m2012975997(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_0018;
	}

IL_0018:
	{
		RuntimeObject * L_3 = V_0;
		return L_3;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3056081658 (Enumerator_t1540455786 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		Transform_t2735953680 * L_0 = __this->get_outer_0();
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m987201346(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = __this->get_currentIndex_1();
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->set_currentIndex_1(L_3);
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		V_2 = (bool)((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
		goto IL_0027;
	}

IL_0027:
	{
		bool L_6 = V_2;
		return L_6;
	}
}
// System.Void UnityEngine.Transform/Enumerator::Reset()
extern "C"  void Enumerator_Reset_m3728423735 (Enumerator_t1540455786 * __this, const RuntimeMethod* method)
{
	{
		__this->set_currentIndex_1((-1));
		return;
	}
}
// System.Boolean UnityEngine.U2D.SpriteAtlasManager::RequestAtlas(System.String)
extern "C"  bool SpriteAtlasManager_RequestAtlas_m3464105809 (RuntimeObject * __this /* static, unused */, String_t* ___tag0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteAtlasManager_RequestAtlas_m3464105809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	String_t* G_B3_0 = NULL;
	RequestAtlasCallback_t2148362272 * G_B3_1 = NULL;
	String_t* G_B2_0 = NULL;
	RequestAtlasCallback_t2148362272 * G_B2_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t1317640999_il2cpp_TypeInfo_var);
		RequestAtlasCallback_t2148362272 * L_0 = ((SpriteAtlasManager_t1317640999_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t1317640999_il2cpp_TypeInfo_var))->get_atlasRequested_0();
		if (!L_0)
		{
			goto IL_003b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t1317640999_il2cpp_TypeInfo_var);
		RequestAtlasCallback_t2148362272 * L_1 = ((SpriteAtlasManager_t1317640999_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t1317640999_il2cpp_TypeInfo_var))->get_atlasRequested_0();
		String_t* L_2 = ___tag0;
		Action_1_t2233756553 * L_3 = ((SpriteAtlasManager_t1317640999_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t1317640999_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_1();
		G_B2_0 = L_2;
		G_B2_1 = L_1;
		if (L_3)
		{
			G_B3_0 = L_2;
			G_B3_1 = L_1;
			goto IL_002a;
		}
	}
	{
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)SpriteAtlasManager_Register_m2642020738_RuntimeMethod_var);
		Action_1_t2233756553 * L_5 = (Action_1_t2233756553 *)il2cpp_codegen_object_new(Action_1_t2233756553_il2cpp_TypeInfo_var);
		Action_1__ctor_m2443092149(L_5, NULL, L_4, /*hidden argument*/Action_1__ctor_m2443092149_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t1317640999_il2cpp_TypeInfo_var);
		((SpriteAtlasManager_t1317640999_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t1317640999_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_1(L_5);
		G_B3_0 = G_B2_0;
		G_B3_1 = G_B2_1;
	}

IL_002a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(SpriteAtlasManager_t1317640999_il2cpp_TypeInfo_var);
		Action_1_t2233756553 * L_6 = ((SpriteAtlasManager_t1317640999_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t1317640999_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_1();
		NullCheck(G_B3_1);
		RequestAtlasCallback_Invoke_m1786080242(G_B3_1, G_B3_0, L_6, /*hidden argument*/NULL);
		V_0 = (bool)1;
		goto IL_0042;
	}

IL_003b:
	{
		V_0 = (bool)0;
		goto IL_0042;
	}

IL_0042:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.Void UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)
extern "C"  void SpriteAtlasManager_Register_m2642020738 (RuntimeObject * __this /* static, unused */, SpriteAtlas_t2050322510 * ___spriteAtlas0, const RuntimeMethod* method)
{
	typedef void (*SpriteAtlasManager_Register_m2642020738_ftn) (SpriteAtlas_t2050322510 *);
	static SpriteAtlasManager_Register_m2642020738_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (SpriteAtlasManager_Register_m2642020738_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)");
	_il2cpp_icall_func(___spriteAtlas0);
}
// System.Void UnityEngine.U2D.SpriteAtlasManager::.cctor()
extern "C"  void SpriteAtlasManager__cctor_m3369627042 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpriteAtlasManager__cctor_m3369627042_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((SpriteAtlasManager_t1317640999_StaticFields*)il2cpp_codegen_static_fields_for(SpriteAtlasManager_t1317640999_il2cpp_TypeInfo_var))->set_atlasRequested_0((RequestAtlasCallback_t2148362272 *)NULL);
		return;
	}
}
extern "C"  void DelegatePInvokeWrapper_RequestAtlasCallback_t2148362272 (RequestAtlasCallback_t2148362272 * __this, String_t* ___tag0, Action_1_t2233756553 * ___action1, const RuntimeMethod* method)
{
	typedef void (STDCALL *PInvokeFunc)(char*, Il2CppMethodPointer);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((Il2CppDelegate*)__this)->method));

	// Marshaling of parameter '___tag0' to native representation
	char* ____tag0_marshaled = NULL;
	____tag0_marshaled = il2cpp_codegen_marshal_string(___tag0);

	// Marshaling of parameter '___action1' to native representation
	Il2CppMethodPointer ____action1_marshaled = NULL;
	____action1_marshaled = il2cpp_codegen_marshal_delegate(reinterpret_cast<Il2CppCodeGenMulticastDelegate*>(___action1));

	// Native function invocation
	il2cppPInvokeFunc(____tag0_marshaled, ____action1_marshaled);

	// Marshaling cleanup of parameter '___tag0' native representation
	il2cpp_codegen_marshal_free(____tag0_marshaled);
	____tag0_marshaled = NULL;

}
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void RequestAtlasCallback__ctor_m4038037514 (RequestAtlasCallback_t2148362272 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::Invoke(System.String,System.Action`1<UnityEngine.U2D.SpriteAtlas>)
extern "C"  void RequestAtlasCallback_Invoke_m1786080242 (RequestAtlasCallback_t2148362272 * __this, String_t* ___tag0, Action_1_t2233756553 * ___action1, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		RequestAtlasCallback_Invoke_m1786080242((RequestAtlasCallback_t2148362272 *)__this->get_prev_9(),___tag0, ___action1, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, String_t* ___tag0, Action_1_t2233756553 * ___action1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___tag0, ___action1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, String_t* ___tag0, Action_1_t2233756553 * ___action1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___tag0, ___action1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, Action_1_t2233756553 * ___action1, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___tag0, ___action1,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::BeginInvoke(System.String,System.Action`1<UnityEngine.U2D.SpriteAtlas>,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* RequestAtlasCallback_BeginInvoke_m3568102205 (RequestAtlasCallback_t2148362272 * __this, String_t* ___tag0, Action_1_t2233756553 * ___action1, AsyncCallback_t3972436406 * ___callback2, RuntimeObject * ___object3, const RuntimeMethod* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___tag0;
	__d_args[1] = ___action1;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback2, (RuntimeObject*)___object3);
}
// System.Void UnityEngine.U2D.SpriteAtlasManager/RequestAtlasCallback::EndInvoke(System.IAsyncResult)
extern "C"  void RequestAtlasCallback_EndInvoke_m2783875591 (RequestAtlasCallback_t2148362272 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void UnityEngine.UISystemProfilerApi::BeginSample(UnityEngine.UISystemProfilerApi/SampleType)
extern "C"  void UISystemProfilerApi_BeginSample_m1563088371 (RuntimeObject * __this /* static, unused */, int32_t ___type0, const RuntimeMethod* method)
{
	typedef void (*UISystemProfilerApi_BeginSample_m1563088371_ftn) (int32_t);
	static UISystemProfilerApi_BeginSample_m1563088371_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UISystemProfilerApi_BeginSample_m1563088371_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UISystemProfilerApi::BeginSample(UnityEngine.UISystemProfilerApi/SampleType)");
	_il2cpp_icall_func(___type0);
}
// System.Void UnityEngine.UISystemProfilerApi::EndSample(UnityEngine.UISystemProfilerApi/SampleType)
extern "C"  void UISystemProfilerApi_EndSample_m1328887039 (RuntimeObject * __this /* static, unused */, int32_t ___type0, const RuntimeMethod* method)
{
	typedef void (*UISystemProfilerApi_EndSample_m1328887039_ftn) (int32_t);
	static UISystemProfilerApi_EndSample_m1328887039_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UISystemProfilerApi_EndSample_m1328887039_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UISystemProfilerApi::EndSample(UnityEngine.UISystemProfilerApi/SampleType)");
	_il2cpp_icall_func(___type0);
}
// System.Void UnityEngine.UISystemProfilerApi::AddMarker(System.String,UnityEngine.Object)
extern "C"  void UISystemProfilerApi_AddMarker_m122347140 (RuntimeObject * __this /* static, unused */, String_t* ___name0, Object_t350248726 * ___obj1, const RuntimeMethod* method)
{
	typedef void (*UISystemProfilerApi_AddMarker_m122347140_ftn) (String_t*, Object_t350248726 *);
	static UISystemProfilerApi_AddMarker_m122347140_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UISystemProfilerApi_AddMarker_m122347140_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UISystemProfilerApi::AddMarker(System.String,UnityEngine.Object)");
	_il2cpp_icall_func(___name0, ___obj1);
}
// System.Void UnityEngine.UIVertex::.cctor()
extern "C"  void UIVertex__cctor_m1241900269 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UIVertex__cctor_m1241900269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UIVertex_t475044788  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Color32_t2411287715  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Color32__ctor_m787656177((&L_0), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		((UIVertex_t475044788_StaticFields*)il2cpp_codegen_static_fields_for(UIVertex_t475044788_il2cpp_TypeInfo_var))->set_s_DefaultColor_8(L_0);
		Vector4_t2651204353  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector4__ctor_m704222614((&L_1), (1.0f), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((UIVertex_t475044788_StaticFields*)il2cpp_codegen_static_fields_for(UIVertex_t475044788_il2cpp_TypeInfo_var))->set_s_DefaultTangent_9(L_1);
		Initobj (UIVertex_t475044788_il2cpp_TypeInfo_var, (&V_0));
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_2 = Vector3_get_zero_m1721653641(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_position_0(L_2);
		Vector3_t2825674791  L_3 = Vector3_get_back_m3191415170(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_normal_1(L_3);
		Vector4_t2651204353  L_4 = ((UIVertex_t475044788_StaticFields*)il2cpp_codegen_static_fields_for(UIVertex_t475044788_il2cpp_TypeInfo_var))->get_s_DefaultTangent_9();
		(&V_0)->set_tangent_7(L_4);
		Color32_t2411287715  L_5 = ((UIVertex_t475044788_StaticFields*)il2cpp_codegen_static_fields_for(UIVertex_t475044788_il2cpp_TypeInfo_var))->get_s_DefaultColor_8();
		(&V_0)->set_color_2(L_5);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t1065050092_il2cpp_TypeInfo_var);
		Vector2_t1065050092  L_6 = Vector2_get_zero_m644371544(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_uv0_3(L_6);
		Vector2_t1065050092  L_7 = Vector2_get_zero_m644371544(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_uv1_4(L_7);
		Vector2_t1065050092  L_8 = Vector2_get_zero_m644371544(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_uv2_5(L_8);
		Vector2_t1065050092  L_9 = Vector2_get_zero_m644371544(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->set_uv3_6(L_9);
		UIVertex_t475044788  L_10 = V_0;
		((UIVertex_t475044788_StaticFields*)il2cpp_codegen_static_fields_for(UIVertex_t475044788_il2cpp_TypeInfo_var))->set_simpleVert_10(L_10);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern "C"  void UnhandledExceptionHandler_RegisterUECatcher_m954252354 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_RegisterUECatcher_m954252354_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	AppDomain_t794471768 * G_B2_0 = NULL;
	AppDomain_t794471768 * G_B1_0 = NULL;
	{
		AppDomain_t794471768 * L_0 = AppDomain_get_CurrentDomain_m2935809104(NULL /*static, unused*/, /*hidden argument*/NULL);
		UnhandledExceptionEventHandler_t3425235907 * L_1 = ((UnhandledExceptionHandler_t832165685_StaticFields*)il2cpp_codegen_static_fields_for(UnhandledExceptionHandler_t832165685_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_0();
		G_B1_0 = L_0;
		if (L_1)
		{
			G_B2_0 = L_0;
			goto IL_001e;
		}
	}
	{
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)UnhandledExceptionHandler_HandleUnhandledException_m1321381177_RuntimeMethod_var);
		UnhandledExceptionEventHandler_t3425235907 * L_3 = (UnhandledExceptionEventHandler_t3425235907 *)il2cpp_codegen_object_new(UnhandledExceptionEventHandler_t3425235907_il2cpp_TypeInfo_var);
		UnhandledExceptionEventHandler__ctor_m3089028188(L_3, NULL, L_2, /*hidden argument*/NULL);
		((UnhandledExceptionHandler_t832165685_StaticFields*)il2cpp_codegen_static_fields_for(UnhandledExceptionHandler_t832165685_il2cpp_TypeInfo_var))->set_U3CU3Ef__mgU24cache0_0(L_3);
		G_B2_0 = G_B1_0;
	}

IL_001e:
	{
		UnhandledExceptionEventHandler_t3425235907 * L_4 = ((UnhandledExceptionHandler_t832165685_StaticFields*)il2cpp_codegen_static_fields_for(UnhandledExceptionHandler_t832165685_il2cpp_TypeInfo_var))->get_U3CU3Ef__mgU24cache0_0();
		NullCheck(G_B2_0);
		AppDomain_add_UnhandledException_m2707088924(G_B2_0, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::HandleUnhandledException(System.Object,System.UnhandledExceptionEventArgs)
extern "C"  void UnhandledExceptionHandler_HandleUnhandledException_m1321381177 (RuntimeObject * __this /* static, unused */, RuntimeObject * ___sender0, UnhandledExceptionEventArgs_t2750508014 * ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_HandleUnhandledException_m1321381177_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Exception_t2572292308 * V_0 = NULL;
	{
		UnhandledExceptionEventArgs_t2750508014 * L_0 = ___args1;
		NullCheck(L_0);
		RuntimeObject * L_1 = UnhandledExceptionEventArgs_get_ExceptionObject_m416069261(L_0, /*hidden argument*/NULL);
		V_0 = ((Exception_t2572292308 *)IsInstClass((RuntimeObject*)L_1, Exception_t2572292308_il2cpp_TypeInfo_var));
		Exception_t2572292308 * L_2 = V_0;
		if (!L_2)
		{
			goto IL_0041;
		}
	}
	{
		Exception_t2572292308 * L_3 = V_0;
		UnhandledExceptionHandler_PrintException_m3598350440(NULL /*static, unused*/, _stringLiteral2818396465, L_3, /*hidden argument*/NULL);
		Exception_t2572292308 * L_4 = V_0;
		NullCheck(L_4);
		Type_t * L_5 = Exception_GetType_m1269163561(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_5);
		Exception_t2572292308 * L_7 = V_0;
		NullCheck(L_7);
		String_t* L_8 = VirtFuncInvoker0< String_t* >::Invoke(6 /* System.String System.Exception::get_Message() */, L_7);
		Exception_t2572292308 * L_9 = V_0;
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Exception::get_StackTrace() */, L_9);
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1575531855(NULL /*static, unused*/, L_6, L_8, L_10, /*hidden argument*/NULL);
		goto IL_004b;
	}

IL_0041:
	{
		UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1575531855(NULL /*static, unused*/, (String_t*)NULL, (String_t*)NULL, (String_t*)NULL, /*hidden argument*/NULL);
	}

IL_004b:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::PrintException(System.String,System.Exception)
extern "C"  void UnhandledExceptionHandler_PrintException_m3598350440 (RuntimeObject * __this /* static, unused */, String_t* ___title0, Exception_t2572292308 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnhandledExceptionHandler_PrintException_m3598350440_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception_t2572292308 * L_0 = ___e1;
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t16286340_il2cpp_TypeInfo_var);
		Debug_LogException_m3669325632(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		Exception_t2572292308 * L_1 = ___e1;
		NullCheck(L_1);
		Exception_t2572292308 * L_2 = Exception_get_InnerException_m3768527343(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Exception_t2572292308 * L_3 = ___e1;
		NullCheck(L_3);
		Exception_t2572292308 * L_4 = Exception_get_InnerException_m3768527343(L_3, /*hidden argument*/NULL);
		UnhandledExceptionHandler_PrintException_m3598350440(NULL /*static, unused*/, _stringLiteral2808402998, L_4, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)
extern "C"  void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1575531855 (RuntimeObject * __this /* static, unused */, String_t* ___managedExceptionType0, String_t* ___managedExceptionMessage1, String_t* ___managedExceptionStack2, const RuntimeMethod* method)
{
	typedef void (*UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1575531855_ftn) (String_t*, String_t*, String_t*);
	static UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1575531855_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m1575531855_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnhandledExceptionHandler::NativeUnhandledExceptionHandler(System.String,System.String,System.String)");
	_il2cpp_icall_func(___managedExceptionType0, ___managedExceptionMessage1, ___managedExceptionStack2);
}
// System.Void UnityEngine.UnityException::.ctor()
extern "C"  void UnityException__ctor_m143873871 (UnityException_t1120912047 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityException__ctor_m143873871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Exception__ctor_m225896743(__this, _stringLiteral374778884, /*hidden argument*/NULL);
		Exception_set_HResult_m2285642882(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String)
extern "C"  void UnityException__ctor_m802568085 (UnityException_t1120912047 * __this, String_t* ___message0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception__ctor_m225896743(__this, L_0, /*hidden argument*/NULL);
		Exception_set_HResult_m2285642882(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.String,System.Exception)
extern "C"  void UnityException__ctor_m787601220 (UnityException_t1120912047 * __this, String_t* ___message0, Exception_t2572292308 * ___innerException1, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___message0;
		Exception_t2572292308 * L_1 = ___innerException1;
		Exception__ctor_m290184388(__this, L_0, L_1, /*hidden argument*/NULL);
		Exception_set_HResult_m2285642882(__this, ((int32_t)-2147467261), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C"  void UnityException__ctor_m3697500254 (UnityException_t1120912047 * __this, SerializationInfo_t1085725973 * ___info0, StreamingContext_t4227869630  ___context1, const RuntimeMethod* method)
{
	{
		SerializationInfo_t1085725973 * L_0 = ___info0;
		StreamingContext_t4227869630  L_1 = ___context1;
		Exception__ctor_m3696836514(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityLogWriter::.ctor()
extern "C"  void UnityLogWriter__ctor_m4110637147 (UnityLogWriter_t646926076 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityLogWriter__ctor_m4110637147_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(TextWriter_t2423646586_il2cpp_TypeInfo_var);
		TextWriter__ctor_m3514236646(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)
extern "C"  void UnityLogWriter_WriteStringToUnityLog_m3468757458 (RuntimeObject * __this /* static, unused */, String_t* ___s0, const RuntimeMethod* method)
{
	typedef void (*UnityLogWriter_WriteStringToUnityLog_m3468757458_ftn) (String_t*);
	static UnityLogWriter_WriteStringToUnityLog_m3468757458_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (UnityLogWriter_WriteStringToUnityLog_m3468757458_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)");
	_il2cpp_icall_func(___s0);
}
// System.Void UnityEngine.UnityLogWriter::Init()
extern "C"  void UnityLogWriter_Init_m2479369668 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityLogWriter_Init_m2479369668_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		UnityLogWriter_t646926076 * L_0 = (UnityLogWriter_t646926076 *)il2cpp_codegen_object_new(UnityLogWriter_t646926076_il2cpp_TypeInfo_var);
		UnityLogWriter__ctor_m4110637147(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Console_t4121508648_il2cpp_TypeInfo_var);
		Console_SetOut_m1131447748(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityLogWriter::Write(System.Char)
extern "C"  void UnityLogWriter_Write_m4240167183 (UnityLogWriter_t646926076 * __this, Il2CppChar ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = Char_ToString_m801101901((&___value0), /*hidden argument*/NULL);
		UnityLogWriter_WriteStringToUnityLog_m3468757458(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnityLogWriter::Write(System.String)
extern "C"  void UnityLogWriter_Write_m2689893232 (UnityLogWriter_t646926076 * __this, String_t* ___s0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___s0;
		UnityLogWriter_WriteStringToUnityLog_m3468757458(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.UnityString::Format(System.String,System.Object[])
extern "C"  String_t* UnityString_Format_m3782610777 (RuntimeObject * __this /* static, unused */, String_t* ___fmt0, ObjectU5BU5D_t1100384052* ___args1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityString_Format_m3782610777_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = ___fmt0;
		ObjectU5BU5D_t1100384052* L_1 = ___args1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Format_m345500613(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		String_t* L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.UnitySynchronizationContext::.ctor()
extern "C"  void UnitySynchronizationContext__ctor_m3792825602 (UnitySynchronizationContext_t3253602121 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext__ctor_m3792825602_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Queue_1_t2108291272 * L_0 = (Queue_1_t2108291272 *)il2cpp_codegen_object_new(Queue_1_t2108291272_il2cpp_TypeInfo_var);
		Queue_1__ctor_m2523417422(L_0, ((int32_t)20), /*hidden argument*/Queue_1__ctor_m2523417422_RuntimeMethod_var);
		__this->set_m_AsyncWorkQueue_1(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Thread_t2629334981_il2cpp_TypeInfo_var);
		Thread_t2629334981 * L_1 = Thread_get_CurrentThread_m1378268693(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Thread_get_ManagedThreadId_m1526988511(L_1, /*hidden argument*/NULL);
		__this->set_m_MainThreadID_2(L_2);
		SynchronizationContext__ctor_m1825192790(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.UnitySynchronizationContext::Exec()
extern "C"  void UnitySynchronizationContext_Exec_m1681198408 (UnitySynchronizationContext_t3253602121 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_Exec_m1681198408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	WorkRequest_t2264216812  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t2572292308 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2572292308 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Queue_1_t2108291272 * L_0 = __this->get_m_AsyncWorkQueue_1();
		V_0 = L_0;
		RuntimeObject * L_1 = V_0;
		Monitor_Enter_m3163704715(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0029;
		}

IL_0014:
		{
			Queue_1_t2108291272 * L_2 = __this->get_m_AsyncWorkQueue_1();
			NullCheck(L_2);
			WorkRequest_t2264216812  L_3 = Queue_1_Dequeue_m3333247629(L_2, /*hidden argument*/Queue_1_Dequeue_m3333247629_RuntimeMethod_var);
			V_1 = L_3;
			WorkRequest_Invoke_m2879977395((&V_1), /*hidden argument*/NULL);
		}

IL_0029:
		{
			Queue_1_t2108291272 * L_4 = __this->get_m_AsyncWorkQueue_1();
			NullCheck(L_4);
			int32_t L_5 = Queue_1_get_Count_m4197321750(L_4, /*hidden argument*/Queue_1_get_Count_m4197321750_RuntimeMethod_var);
			if ((((int32_t)L_5) > ((int32_t)0)))
			{
				goto IL_0014;
			}
		}

IL_003a:
		{
			IL2CPP_LEAVE(0x47, FINALLY_0040);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2572292308 *)e.ex;
		goto FINALLY_0040;
	}

FINALLY_0040:
	{ // begin finally (depth: 1)
		RuntimeObject * L_6 = V_0;
		Monitor_Exit_m366607352(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(64)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(64)
	{
		IL2CPP_JUMP_TBL(0x47, IL_0047)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2572292308 *)
	}

IL_0047:
	{
		return;
	}
}
// System.Void UnityEngine.UnitySynchronizationContext::InitializeSynchronizationContext()
extern "C"  void UnitySynchronizationContext_InitializeSynchronizationContext_m109945450 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_InitializeSynchronizationContext_m109945450_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SynchronizationContext_t155019481 * L_0 = SynchronizationContext_get_Current_m3430941772(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		UnitySynchronizationContext_t3253602121 * L_1 = (UnitySynchronizationContext_t3253602121 *)il2cpp_codegen_object_new(UnitySynchronizationContext_t3253602121_il2cpp_TypeInfo_var);
		UnitySynchronizationContext__ctor_m3792825602(L_1, /*hidden argument*/NULL);
		SynchronizationContext_SetSynchronizationContext_m901783352(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.UnitySynchronizationContext::ExecuteTasks()
extern "C"  void UnitySynchronizationContext_ExecuteTasks_m2686422545 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnitySynchronizationContext_ExecuteTasks_m2686422545_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UnitySynchronizationContext_t3253602121 * V_0 = NULL;
	{
		SynchronizationContext_t155019481 * L_0 = SynchronizationContext_get_Current_m3430941772(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = ((UnitySynchronizationContext_t3253602121 *)IsInstSealed((RuntimeObject*)L_0, UnitySynchronizationContext_t3253602121_il2cpp_TypeInfo_var));
		UnitySynchronizationContext_t3253602121 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0018;
		}
	}
	{
		UnitySynchronizationContext_t3253602121 * L_2 = V_0;
		NullCheck(L_2);
		UnitySynchronizationContext_Exec_m1681198408(L_2, /*hidden argument*/NULL);
	}

IL_0018:
	{
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.UnitySynchronizationContext/WorkRequest
extern "C" void WorkRequest_t2264216812_marshal_pinvoke(const WorkRequest_t2264216812& unmarshaled, WorkRequest_t2264216812_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___m_WaitHandle_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_WaitHandle' of type 'WorkRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_WaitHandle_2Exception);
}
extern "C" void WorkRequest_t2264216812_marshal_pinvoke_back(const WorkRequest_t2264216812_marshaled_pinvoke& marshaled, WorkRequest_t2264216812& unmarshaled)
{
	Il2CppCodeGenException* ___m_WaitHandle_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_WaitHandle' of type 'WorkRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_WaitHandle_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.UnitySynchronizationContext/WorkRequest
extern "C" void WorkRequest_t2264216812_marshal_pinvoke_cleanup(WorkRequest_t2264216812_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.UnitySynchronizationContext/WorkRequest
extern "C" void WorkRequest_t2264216812_marshal_com(const WorkRequest_t2264216812& unmarshaled, WorkRequest_t2264216812_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___m_WaitHandle_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_WaitHandle' of type 'WorkRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_WaitHandle_2Exception);
}
extern "C" void WorkRequest_t2264216812_marshal_com_back(const WorkRequest_t2264216812_marshaled_com& marshaled, WorkRequest_t2264216812& unmarshaled)
{
	Il2CppCodeGenException* ___m_WaitHandle_2Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'm_WaitHandle' of type 'WorkRequest': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___m_WaitHandle_2Exception);
}
// Conversion method for clean up from marshalling of: UnityEngine.UnitySynchronizationContext/WorkRequest
extern "C" void WorkRequest_t2264216812_marshal_com_cleanup(WorkRequest_t2264216812_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.UnitySynchronizationContext/WorkRequest::Invoke()
extern "C"  void WorkRequest_Invoke_m2879977395 (WorkRequest_t2264216812 * __this, const RuntimeMethod* method)
{
	{
		SendOrPostCallback_t3197245846 * L_0 = __this->get_m_DelagateCallback_0();
		RuntimeObject * L_1 = __this->get_m_DelagateState_1();
		NullCheck(L_0);
		SendOrPostCallback_Invoke_m2222920495(L_0, L_1, /*hidden argument*/NULL);
		ManualResetEvent_t562888503 * L_2 = __this->get_m_WaitHandle_2();
		if (!L_2)
		{
			goto IL_0029;
		}
	}
	{
		ManualResetEvent_t562888503 * L_3 = __this->get_m_WaitHandle_2();
		NullCheck(L_3);
		EventWaitHandle_Set_m269094399(L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
extern "C"  void WorkRequest_Invoke_m2879977395_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	WorkRequest_t2264216812 * _thisAdjusted = reinterpret_cast<WorkRequest_t2264216812 *>(__this + 1);
	WorkRequest_Invoke_m2879977395(_thisAdjusted, method);
}
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m4204458150 (Vector2_t1065050092 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
extern "C"  void Vector2__ctor_m4204458150_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	Vector2_t1065050092 * _thisAdjusted = reinterpret_cast<Vector2_t1065050092 *>(__this + 1);
	Vector2__ctor_m4204458150(_thisAdjusted, ___x0, ___y1, method);
}
// System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern "C"  float Vector2_get_Item_m904236465 (Vector2_t1065050092 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_Item_m904236465_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002b;
	}

IL_0013:
	{
		float L_2 = __this->get_x_0();
		V_0 = L_2;
		goto IL_0036;
	}

IL_001f:
	{
		float L_3 = __this->get_y_1();
		V_0 = L_3;
		goto IL_0036;
	}

IL_002b:
	{
		IndexOutOfRangeException_t481375535 * L_4 = (IndexOutOfRangeException_t481375535 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t481375535_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3252438219(L_4, _stringLiteral2676045397, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0036:
	{
		float L_5 = V_0;
		return L_5;
	}
}
extern "C"  float Vector2_get_Item_m904236465_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Vector2_t1065050092 * _thisAdjusted = reinterpret_cast<Vector2_t1065050092 *>(__this + 1);
	return Vector2_get_Item_m904236465(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern "C"  void Vector2_set_Item_m1154146684 (Vector2_t1065050092 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_set_Item_m1154146684_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		int32_t L_1 = ___index0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002b;
	}

IL_0013:
	{
		float L_2 = ___value1;
		__this->set_x_0(L_2);
		goto IL_0036;
	}

IL_001f:
	{
		float L_3 = ___value1;
		__this->set_y_1(L_3);
		goto IL_0036;
	}

IL_002b:
	{
		IndexOutOfRangeException_t481375535 * L_4 = (IndexOutOfRangeException_t481375535 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t481375535_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3252438219(L_4, _stringLiteral2676045397, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0036:
	{
		return;
	}
}
extern "C"  void Vector2_set_Item_m1154146684_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	Vector2_t1065050092 * _thisAdjusted = reinterpret_cast<Vector2_t1065050092 *>(__this + 1);
	Vector2_set_Item_m1154146684(_thisAdjusted, ___index0, ___value1, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t1065050092  Vector2_Scale_m755135258 (RuntimeObject * __this /* static, unused */, Vector2_t1065050092  ___a0, Vector2_t1065050092  ___b1, const RuntimeMethod* method)
{
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		Vector2_t1065050092  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m4204458150((&L_4), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002a;
	}

IL_002a:
	{
		Vector2_t1065050092  L_5 = V_0;
		return L_5;
	}
}
// System.String UnityEngine.Vector2::ToString()
extern "C"  String_t* Vector2_ToString_m1150693065 (Vector2_t1065050092 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_ToString_m1150693065_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1100384052* L_0 = ((ObjectU5BU5D_t1100384052*)SZArrayNew(ObjectU5BU5D_t1100384052_il2cpp_TypeInfo_var, (uint32_t)2));
		float L_1 = __this->get_x_0();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t1534300504_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1100384052* L_4 = L_0;
		float L_5 = __this->get_y_1();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t1534300504_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		String_t* L_8 = UnityString_Format_m3782610777(NULL /*static, unused*/, _stringLiteral3845830707, L_4, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0033;
	}

IL_0033:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
extern "C"  String_t* Vector2_ToString_m1150693065_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t1065050092 * _thisAdjusted = reinterpret_cast<Vector2_t1065050092 *>(__this + 1);
	return Vector2_ToString_m1150693065(_thisAdjusted, method);
}
// System.Int32 UnityEngine.Vector2::GetHashCode()
extern "C"  int32_t Vector2_GetHashCode_m1564755462 (Vector2_t1065050092 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_0();
		int32_t L_1 = Single_GetHashCode_m1220171593(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_1();
		int32_t L_3 = Single_GetHashCode_m1220171593(L_2, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))));
		goto IL_002c;
	}

IL_002c:
	{
		int32_t L_4 = V_0;
		return L_4;
	}
}
extern "C"  int32_t Vector2_GetHashCode_m1564755462_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t1065050092 * _thisAdjusted = reinterpret_cast<Vector2_t1065050092 *>(__this + 1);
	return Vector2_GetHashCode_m1564755462(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern "C"  bool Vector2_Equals_m1183599621 (Vector2_t1065050092 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_Equals_m1183599621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Vector2_t1065050092  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B5_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Vector2_t1065050092_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_004c;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Vector2_t1065050092 *)((Vector2_t1065050092 *)UnBox(L_1, Vector2_t1065050092_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_0();
		float L_3 = (&V_1)->get_x_0();
		bool L_4 = Single_Equals_m4010867313(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0045;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_1();
		float L_6 = (&V_1)->get_y_1();
		bool L_7 = Single_Equals_m4010867313(L_5, L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
		goto IL_0046;
	}

IL_0045:
	{
		G_B5_0 = 0;
	}

IL_0046:
	{
		V_0 = (bool)G_B5_0;
		goto IL_004c;
	}

IL_004c:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
extern "C"  bool Vector2_Equals_m1183599621_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Vector2_t1065050092 * _thisAdjusted = reinterpret_cast<Vector2_t1065050092 *>(__this + 1);
	return Vector2_Equals_m1183599621(_thisAdjusted, ___other0, method);
}
// System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  float Vector2_Dot_m4177666705 (RuntimeObject * __this /* static, unused */, Vector2_t1065050092  ___lhs0, Vector2_t1065050092  ___rhs1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___lhs0)->get_x_0();
		float L_1 = (&___rhs1)->get_x_0();
		float L_2 = (&___lhs0)->get_y_1();
		float L_3 = (&___rhs1)->get_y_1();
		V_0 = ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
		goto IL_0026;
	}

IL_0026:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Single UnityEngine.Vector2::get_magnitude()
extern "C"  float Vector2_get_magnitude_m1391329218 (Vector2_t1065050092 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_magnitude_m1391329218_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_x_0();
		float L_2 = __this->get_y_1();
		float L_3 = __this->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		float L_4 = sqrtf(((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3)))));
		V_0 = L_4;
		goto IL_0027;
	}

IL_0027:
	{
		float L_5 = V_0;
		return L_5;
	}
}
extern "C"  float Vector2_get_magnitude_m1391329218_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t1065050092 * _thisAdjusted = reinterpret_cast<Vector2_t1065050092 *>(__this + 1);
	return Vector2_get_magnitude_m1391329218(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern "C"  float Vector2_get_sqrMagnitude_m1831367612 (Vector2_t1065050092 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_x_0();
		float L_1 = __this->get_x_0();
		float L_2 = __this->get_y_1();
		float L_3 = __this->get_y_1();
		V_0 = ((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
		goto IL_0022;
	}

IL_0022:
	{
		float L_4 = V_0;
		return L_4;
	}
}
extern "C"  float Vector2_get_sqrMagnitude_m1831367612_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector2_t1065050092 * _thisAdjusted = reinterpret_cast<Vector2_t1065050092 *>(__this + 1);
	return Vector2_get_sqrMagnitude_m1831367612(_thisAdjusted, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t1065050092  Vector2_op_Addition_m1200337024 (RuntimeObject * __this /* static, unused */, Vector2_t1065050092  ___a0, Vector2_t1065050092  ___b1, const RuntimeMethod* method)
{
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		Vector2_t1065050092  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m4204458150((&L_4), ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002a;
	}

IL_002a:
	{
		Vector2_t1065050092  L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  Vector2_t1065050092  Vector2_op_Subtraction_m1110678165 (RuntimeObject * __this /* static, unused */, Vector2_t1065050092  ___a0, Vector2_t1065050092  ___b1, const RuntimeMethod* method)
{
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = (&___b1)->get_x_0();
		float L_2 = (&___a0)->get_y_1();
		float L_3 = (&___b1)->get_y_1();
		Vector2_t1065050092  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m4204458150((&L_4), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_002a;
	}

IL_002a:
	{
		Vector2_t1065050092  L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t1065050092  Vector2_op_Multiply_m2920977953 (RuntimeObject * __this /* static, unused */, Vector2_t1065050092  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_1();
		float L_3 = ___d1;
		Vector2_t1065050092  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m4204458150((&L_4), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		Vector2_t1065050092  L_5 = V_0;
		return L_5;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern "C"  Vector2_t1065050092  Vector2_op_Division_m2760197201 (RuntimeObject * __this /* static, unused */, Vector2_t1065050092  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_0();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_1();
		float L_3 = ___d1;
		Vector2_t1065050092  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m4204458150((&L_4), ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_001e;
	}

IL_001e:
	{
		Vector2_t1065050092  L_5 = V_0;
		return L_5;
	}
}
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Equality_m2618668614 (RuntimeObject * __this /* static, unused */, Vector2_t1065050092  ___lhs0, Vector2_t1065050092  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_op_Equality_m2618668614_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Vector2_t1065050092  L_0 = ___lhs0;
		Vector2_t1065050092  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t1065050092_il2cpp_TypeInfo_var);
		Vector2_t1065050092  L_2 = Vector2_op_Subtraction_m1110678165(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector2_get_sqrMagnitude_m1831367612((&V_0), /*hidden argument*/NULL);
		V_1 = (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_001d;
	}

IL_001d:
	{
		bool L_4 = V_1;
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
extern "C"  bool Vector2_op_Inequality_m3452301622 (RuntimeObject * __this /* static, unused */, Vector2_t1065050092  ___lhs0, Vector2_t1065050092  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_op_Inequality_m3452301622_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Vector2_t1065050092  L_0 = ___lhs0;
		Vector2_t1065050092  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t1065050092_il2cpp_TypeInfo_var);
		bool L_2 = Vector2_op_Equality_m2618668614(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t1065050092  Vector2_op_Implicit_m3867385973 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___v0, const RuntimeMethod* method)
{
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___v0)->get_x_1();
		float L_1 = (&___v0)->get_y_2();
		Vector2_t1065050092  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m4204458150((&L_2), L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001a;
	}

IL_001a:
	{
		Vector2_t1065050092  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t2825674791  Vector2_op_Implicit_m2444984928 (RuntimeObject * __this /* static, unused */, Vector2_t1065050092  ___v0, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___v0)->get_x_0();
		float L_1 = (&___v0)->get_y_1();
		Vector3_t2825674791  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m874567701((&L_2), L_0, L_1, (0.0f), /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_001f;
	}

IL_001f:
	{
		Vector3_t2825674791  L_3 = V_0;
		return L_3;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C"  Vector2_t1065050092  Vector2_get_zero_m644371544 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_zero_m644371544_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t1065050092_il2cpp_TypeInfo_var);
		Vector2_t1065050092  L_0 = ((Vector2_t1065050092_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1065050092_il2cpp_TypeInfo_var))->get_zeroVector_2();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector2_t1065050092  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern "C"  Vector2_t1065050092  Vector2_get_one_m748887559 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_one_m748887559_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t1065050092_il2cpp_TypeInfo_var);
		Vector2_t1065050092  L_0 = ((Vector2_t1065050092_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1065050092_il2cpp_TypeInfo_var))->get_oneVector_3();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector2_t1065050092  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern "C"  Vector2_t1065050092  Vector2_get_up_m4164366462 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_up_m4164366462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t1065050092_il2cpp_TypeInfo_var);
		Vector2_t1065050092  L_0 = ((Vector2_t1065050092_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1065050092_il2cpp_TypeInfo_var))->get_upVector_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector2_t1065050092  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_right()
extern "C"  Vector2_t1065050092  Vector2_get_right_m2371833040 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2_get_right_m2371833040_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_t1065050092  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t1065050092_il2cpp_TypeInfo_var);
		Vector2_t1065050092  L_0 = ((Vector2_t1065050092_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1065050092_il2cpp_TypeInfo_var))->get_rightVector_7();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector2_t1065050092  L_1 = V_0;
		return L_1;
	}
}
// System.Void UnityEngine.Vector2::.cctor()
extern "C"  void Vector2__cctor_m3322260765 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector2__cctor_m3322260765_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector2_t1065050092  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector2__ctor_m4204458150((&L_0), (0.0f), (0.0f), /*hidden argument*/NULL);
		((Vector2_t1065050092_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1065050092_il2cpp_TypeInfo_var))->set_zeroVector_2(L_0);
		Vector2_t1065050092  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector2__ctor_m4204458150((&L_1), (1.0f), (1.0f), /*hidden argument*/NULL);
		((Vector2_t1065050092_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1065050092_il2cpp_TypeInfo_var))->set_oneVector_3(L_1);
		Vector2_t1065050092  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector2__ctor_m4204458150((&L_2), (0.0f), (1.0f), /*hidden argument*/NULL);
		((Vector2_t1065050092_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1065050092_il2cpp_TypeInfo_var))->set_upVector_4(L_2);
		Vector2_t1065050092  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector2__ctor_m4204458150((&L_3), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((Vector2_t1065050092_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1065050092_il2cpp_TypeInfo_var))->set_downVector_5(L_3);
		Vector2_t1065050092  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector2__ctor_m4204458150((&L_4), (-1.0f), (0.0f), /*hidden argument*/NULL);
		((Vector2_t1065050092_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1065050092_il2cpp_TypeInfo_var))->set_leftVector_6(L_4);
		Vector2_t1065050092  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector2__ctor_m4204458150((&L_5), (1.0f), (0.0f), /*hidden argument*/NULL);
		((Vector2_t1065050092_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1065050092_il2cpp_TypeInfo_var))->set_rightVector_7(L_5);
		Vector2_t1065050092  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector2__ctor_m4204458150((&L_6), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector2_t1065050092_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1065050092_il2cpp_TypeInfo_var))->set_positiveInfinityVector_8(L_6);
		Vector2_t1065050092  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector2__ctor_m4204458150((&L_7), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector2_t1065050092_StaticFields*)il2cpp_codegen_static_fields_for(Vector2_t1065050092_il2cpp_TypeInfo_var))->set_negativeInfinityVector_9(L_7);
		return;
	}
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m874567701 (Vector3_t2825674791 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		return;
	}
}
extern "C"  void Vector3__ctor_m874567701_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	Vector3_t2825674791 * _thisAdjusted = reinterpret_cast<Vector3_t2825674791 *>(__this + 1);
	Vector3__ctor_m874567701(_thisAdjusted, ___x0, ___y1, ___z2, method);
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern "C"  void Vector3__ctor_m2674852412 (Vector3_t2825674791 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		__this->set_z_3((0.0f));
		return;
	}
}
extern "C"  void Vector3__ctor_m2674852412_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	Vector3_t2825674791 * _thisAdjusted = reinterpret_cast<Vector3_t2825674791 *>(__this + 1);
	Vector3__ctor_m2674852412(_thisAdjusted, ___x0, ___y1, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2825674791  Vector3_Lerp_m4176566525 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___a0, Vector3_t2825674791  ___b1, float ___t2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Lerp_m4176566525_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = ___t2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		float L_1 = Mathf_Clamp01_m3679321964(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		___t2 = L_1;
		float L_2 = (&___a0)->get_x_1();
		float L_3 = (&___b1)->get_x_1();
		float L_4 = (&___a0)->get_x_1();
		float L_5 = ___t2;
		float L_6 = (&___a0)->get_y_2();
		float L_7 = (&___b1)->get_y_2();
		float L_8 = (&___a0)->get_y_2();
		float L_9 = ___t2;
		float L_10 = (&___a0)->get_z_3();
		float L_11 = (&___b1)->get_z_3();
		float L_12 = (&___a0)->get_z_3();
		float L_13 = ___t2;
		Vector3_t2825674791  L_14;
		memset(&L_14, 0, sizeof(L_14));
		Vector3__ctor_m874567701((&L_14), ((float)((float)L_2+(float)((float)((float)((float)((float)L_3-(float)L_4))*(float)L_5)))), ((float)((float)L_6+(float)((float)((float)((float)((float)L_7-(float)L_8))*(float)L_9)))), ((float)((float)L_10+(float)((float)((float)((float)((float)L_11-(float)L_12))*(float)L_13)))), /*hidden argument*/NULL);
		V_0 = L_14;
		goto IL_005f;
	}

IL_005f:
	{
		Vector3_t2825674791  L_15 = V_0;
		return L_15;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::MoveTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2825674791  Vector3_MoveTowards_m1607912389 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___current0, Vector3_t2825674791  ___target1, float ___maxDistanceDelta2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_MoveTowards_m1607912389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Vector3_t2825674791  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Vector3_t2825674791  L_0 = ___target1;
		Vector3_t2825674791  L_1 = ___current0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_2 = Vector3_op_Subtraction_m2076583184(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Vector3_get_magnitude_m59141406((&V_0), /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = V_1;
		float L_5 = ___maxDistanceDelta2;
		if ((((float)L_4) <= ((float)L_5)))
		{
			goto IL_0023;
		}
	}
	{
		float L_6 = V_1;
		if ((!(((float)L_6) < ((float)(1.401298E-45f)))))
		{
			goto IL_002a;
		}
	}

IL_0023:
	{
		Vector3_t2825674791  L_7 = ___target1;
		V_2 = L_7;
		goto IL_0043;
	}

IL_002a:
	{
		Vector3_t2825674791  L_8 = ___current0;
		Vector3_t2825674791  L_9 = V_0;
		float L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_11 = Vector3_op_Division_m2525477617(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		float L_12 = ___maxDistanceDelta2;
		Vector3_t2825674791  L_13 = Vector3_op_Multiply_m544015589(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Vector3_t2825674791  L_14 = Vector3_op_Addition_m1434256537(NULL /*static, unused*/, L_8, L_13, /*hidden argument*/NULL);
		V_2 = L_14;
		goto IL_0043;
	}

IL_0043:
	{
		Vector3_t2825674791  L_15 = V_2;
		return L_15;
	}
}
// System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern "C"  float Vector3_get_Item_m2529581307 (Vector3_t2825674791 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_Item_m2529581307_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0018;
			}
			case 1:
			{
				goto IL_0024;
			}
			case 2:
			{
				goto IL_0030;
			}
		}
	}
	{
		goto IL_003c;
	}

IL_0018:
	{
		float L_1 = __this->get_x_1();
		V_0 = L_1;
		goto IL_0047;
	}

IL_0024:
	{
		float L_2 = __this->get_y_2();
		V_0 = L_2;
		goto IL_0047;
	}

IL_0030:
	{
		float L_3 = __this->get_z_3();
		V_0 = L_3;
		goto IL_0047;
	}

IL_003c:
	{
		IndexOutOfRangeException_t481375535 * L_4 = (IndexOutOfRangeException_t481375535 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t481375535_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3252438219(L_4, _stringLiteral1627034674, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0047:
	{
		float L_5 = V_0;
		return L_5;
	}
}
extern "C"  float Vector3_get_Item_m2529581307_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Vector3_t2825674791 * _thisAdjusted = reinterpret_cast<Vector3_t2825674791 *>(__this + 1);
	return Vector3_get_Item_m2529581307(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
extern "C"  void Vector3_set_Item_m729529755 (Vector3_t2825674791 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_set_Item_m729529755_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_0018;
			}
			case 1:
			{
				goto IL_0024;
			}
			case 2:
			{
				goto IL_0030;
			}
		}
	}
	{
		goto IL_003c;
	}

IL_0018:
	{
		float L_1 = ___value1;
		__this->set_x_1(L_1);
		goto IL_0047;
	}

IL_0024:
	{
		float L_2 = ___value1;
		__this->set_y_2(L_2);
		goto IL_0047;
	}

IL_0030:
	{
		float L_3 = ___value1;
		__this->set_z_3(L_3);
		goto IL_0047;
	}

IL_003c:
	{
		IndexOutOfRangeException_t481375535 * L_4 = (IndexOutOfRangeException_t481375535 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t481375535_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3252438219(L_4, _stringLiteral1627034674, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Vector3_set_Item_m729529755_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	Vector3_t2825674791 * _thisAdjusted = reinterpret_cast<Vector3_t2825674791 *>(__this + 1);
	Vector3_set_Item_m729529755(_thisAdjusted, ___index0, ___value1, method);
}
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m930170997 (Vector3_t2825674791 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m1220171593(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m1220171593(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m1220171593(L_4, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))));
		goto IL_0040;
	}

IL_0040:
	{
		int32_t L_6 = V_0;
		return L_6;
	}
}
extern "C"  int32_t Vector3_GetHashCode_m930170997_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector3_t2825674791 * _thisAdjusted = reinterpret_cast<Vector3_t2825674791 *>(__this + 1);
	return Vector3_GetHashCode_m930170997(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern "C"  bool Vector3_Equals_m745331672 (Vector3_t2825674791 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Equals_m745331672_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Vector3_t2825674791  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B6_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Vector3_t2825674791_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_0063;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Vector3_t2825674791 *)((Vector3_t2825674791 *)UnBox(L_1, Vector3_t2825674791_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_1)->get_x_1();
		bool L_4 = Single_Equals_m4010867313(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_005c;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_1)->get_y_2();
		bool L_7 = Single_Equals_m4010867313(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_005c;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_1)->get_z_3();
		bool L_10 = Single_Equals_m4010867313(L_8, L_9, /*hidden argument*/NULL);
		G_B6_0 = ((int32_t)(L_10));
		goto IL_005d;
	}

IL_005c:
	{
		G_B6_0 = 0;
	}

IL_005d:
	{
		V_0 = (bool)G_B6_0;
		goto IL_0063;
	}

IL_0063:
	{
		bool L_11 = V_0;
		return L_11;
	}
}
extern "C"  bool Vector3_Equals_m745331672_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Vector3_t2825674791 * _thisAdjusted = reinterpret_cast<Vector3_t2825674791 *>(__this + 1);
	return Vector3_Equals_m745331672(_thisAdjusted, ___other0, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Vector3_Normalize_m407020897 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Normalize_m407020897_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t2825674791  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t2825674791  L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		float L_1 = Vector3_Magnitude_m341851577(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = V_0;
		if ((!(((float)L_2) > ((float)(1.0E-05f)))))
		{
			goto IL_0020;
		}
	}
	{
		Vector3_t2825674791  L_3 = ___value0;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_5 = Vector3_op_Division_m2525477617(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		goto IL_002b;
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_6 = Vector3_get_zero_m1721653641(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		goto IL_002b;
	}

IL_002b:
	{
		Vector3_t2825674791  L_7 = V_1;
		return L_7;
	}
}
// System.Void UnityEngine.Vector3::Normalize()
extern "C"  void Vector3_Normalize_m3124542329 (Vector3_t2825674791 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Normalize_m3124542329_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		float L_0 = Vector3_Magnitude_m341851577(NULL /*static, unused*/, (*(Vector3_t2825674791 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = V_0;
		if ((!(((float)L_1) > ((float)(1.0E-05f)))))
		{
			goto IL_002f;
		}
	}
	{
		float L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_3 = Vector3_op_Division_m2525477617(NULL /*static, unused*/, (*(Vector3_t2825674791 *)__this), L_2, /*hidden argument*/NULL);
		*(Vector3_t2825674791 *)__this = L_3;
		goto IL_003a;
	}

IL_002f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_4 = Vector3_get_zero_m1721653641(NULL /*static, unused*/, /*hidden argument*/NULL);
		*(Vector3_t2825674791 *)__this = L_4;
	}

IL_003a:
	{
		return;
	}
}
extern "C"  void Vector3_Normalize_m3124542329_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector3_t2825674791 * _thisAdjusted = reinterpret_cast<Vector3_t2825674791 *>(__this + 1);
	Vector3_Normalize_m3124542329(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern "C"  Vector3_t2825674791  Vector3_get_normalized_m146301709 (Vector3_t2825674791 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_normalized_m146301709_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_0 = Vector3_Normalize_m407020897(NULL /*static, unused*/, (*(Vector3_t2825674791 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0012;
	}

IL_0012:
	{
		Vector3_t2825674791  L_1 = V_0;
		return L_1;
	}
}
extern "C"  Vector3_t2825674791  Vector3_get_normalized_m146301709_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector3_t2825674791 * _thisAdjusted = reinterpret_cast<Vector3_t2825674791 *>(__this + 1);
	return Vector3_get_normalized_m146301709(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Dot_m2778157544 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___lhs0, Vector3_t2825674791  ___rhs1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		float L_2 = (&___lhs0)->get_y_2();
		float L_3 = (&___rhs1)->get_y_2();
		float L_4 = (&___lhs0)->get_z_3();
		float L_5 = (&___rhs1)->get_z_3();
		V_0 = ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
		goto IL_0036;
	}

IL_0036:
	{
		float L_6 = V_0;
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  float Vector3_Distance_m2357219148 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___a0, Vector3_t2825674791  ___b1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Distance_m2357219148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3__ctor_m874567701((&V_0), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		float L_6 = (&V_0)->get_x_1();
		float L_7 = (&V_0)->get_x_1();
		float L_8 = (&V_0)->get_y_2();
		float L_9 = (&V_0)->get_y_2();
		float L_10 = (&V_0)->get_z_3();
		float L_11 = (&V_0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		float L_12 = sqrtf(((float)((float)((float)((float)((float)((float)L_6*(float)L_7))+(float)((float)((float)L_8*(float)L_9))))+(float)((float)((float)L_10*(float)L_11)))));
		V_1 = L_12;
		goto IL_006f;
	}

IL_006f:
	{
		float L_13 = V_1;
		return L_13;
	}
}
// System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern "C"  float Vector3_Magnitude_m341851577 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___vector0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Magnitude_m341851577_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = (&___vector0)->get_x_1();
		float L_1 = (&___vector0)->get_x_1();
		float L_2 = (&___vector0)->get_y_2();
		float L_3 = (&___vector0)->get_y_2();
		float L_4 = (&___vector0)->get_z_3();
		float L_5 = (&___vector0)->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		V_0 = L_6;
		goto IL_003b;
	}

IL_003b:
	{
		float L_7 = V_0;
		return L_7;
	}
}
// System.Single UnityEngine.Vector3::get_magnitude()
extern "C"  float Vector3_get_magnitude_m59141406 (Vector3_t2825674791 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_magnitude_m59141406_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		float L_4 = __this->get_z_3();
		float L_5 = __this->get_z_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		float L_6 = sqrtf(((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5)))));
		V_0 = L_6;
		goto IL_0035;
	}

IL_0035:
	{
		float L_7 = V_0;
		return L_7;
	}
}
extern "C"  float Vector3_get_magnitude_m59141406_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector3_t2825674791 * _thisAdjusted = reinterpret_cast<Vector3_t2825674791 *>(__this + 1);
	return Vector3_get_magnitude_m59141406(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector3::SqrMagnitude(UnityEngine.Vector3)
extern "C"  float Vector3_SqrMagnitude_m3584580626 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___vector0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___vector0)->get_x_1();
		float L_1 = (&___vector0)->get_x_1();
		float L_2 = (&___vector0)->get_y_2();
		float L_3 = (&___vector0)->get_y_2();
		float L_4 = (&___vector0)->get_z_3();
		float L_5 = (&___vector0)->get_z_3();
		V_0 = ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
		goto IL_0036;
	}

IL_0036:
	{
		float L_6 = V_0;
		return L_6;
	}
}
// System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern "C"  float Vector3_get_sqrMagnitude_m1156660751 (Vector3_t2825674791 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_x_1();
		float L_1 = __this->get_x_1();
		float L_2 = __this->get_y_2();
		float L_3 = __this->get_y_2();
		float L_4 = __this->get_z_3();
		float L_5 = __this->get_z_3();
		V_0 = ((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))));
		goto IL_0030;
	}

IL_0030:
	{
		float L_6 = V_0;
		return L_6;
	}
}
extern "C"  float Vector3_get_sqrMagnitude_m1156660751_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector3_t2825674791 * _thisAdjusted = reinterpret_cast<Vector3_t2825674791 *>(__this + 1);
	return Vector3_get_sqrMagnitude_m1156660751(_thisAdjusted, method);
}
// UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Vector3_Min_m2986409069 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___lhs0, Vector3_t2825674791  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Min_m2986409069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Min_m1373908152(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = (&___lhs0)->get_y_2();
		float L_4 = (&___rhs1)->get_y_2();
		float L_5 = Mathf_Min_m1373908152(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_z_3();
		float L_8 = Mathf_Min_m1373908152(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t2825674791  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m874567701((&L_9), L_2, L_5, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0045;
	}

IL_0045:
	{
		Vector3_t2825674791  L_10 = V_0;
		return L_10;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Vector3_Max_m3682725703 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___lhs0, Vector3_t2825674791  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_Max_m3682725703_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___lhs0)->get_x_1();
		float L_1 = (&___rhs1)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t1076584549_il2cpp_TypeInfo_var);
		float L_2 = Mathf_Max_m3185906259(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = (&___lhs0)->get_y_2();
		float L_4 = (&___rhs1)->get_y_2();
		float L_5 = Mathf_Max_m3185906259(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		float L_6 = (&___lhs0)->get_z_3();
		float L_7 = (&___rhs1)->get_z_3();
		float L_8 = Mathf_Max_m3185906259(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		Vector3_t2825674791  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m874567701((&L_9), L_2, L_5, L_8, /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0045;
	}

IL_0045:
	{
		Vector3_t2825674791  L_10 = V_0;
		return L_10;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2825674791  Vector3_get_zero_m1721653641 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_zero_m1721653641_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_0 = ((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->get_zeroVector_4();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t2825674791  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern "C"  Vector3_t2825674791  Vector3_get_one_m1243491037 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_one_m1243491037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_0 = ((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->get_oneVector_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t2825674791  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t2825674791  Vector3_get_forward_m3907749096 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_forward_m3907749096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_0 = ((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->get_forwardVector_10();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t2825674791  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern "C"  Vector3_t2825674791  Vector3_get_back_m3191415170 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_back_m3191415170_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_0 = ((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->get_backVector_11();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t2825674791  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t2825674791  Vector3_get_up_m2143830244 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_up_m2143830244_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_0 = ((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->get_upVector_6();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t2825674791  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern "C"  Vector3_t2825674791  Vector3_get_down_m2933426780 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_down_m2933426780_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_0 = ((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->get_downVector_7();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t2825674791  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_left()
extern "C"  Vector3_t2825674791  Vector3_get_left_m3420204028 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_left_m3420204028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_0 = ((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->get_leftVector_8();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t2825674791  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern "C"  Vector3_t2825674791  Vector3_get_right_m372445178 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_get_right_m372445178_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_0 = ((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->get_rightVector_9();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector3_t2825674791  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Vector3_op_Addition_m1434256537 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___a0, Vector3_t2825674791  ___b1, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t2825674791  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m874567701((&L_6), ((float)((float)L_0+(float)L_1)), ((float)((float)L_2+(float)L_3)), ((float)((float)L_4+(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		Vector3_t2825674791  L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Vector3_op_Subtraction_m2076583184 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___a0, Vector3_t2825674791  ___b1, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		Vector3_t2825674791  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m874567701((&L_6), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0039;
	}

IL_0039:
	{
		Vector3_t2825674791  L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2825674791  Vector3_op_Multiply_m544015589 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		Vector3_t2825674791  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m874567701((&L_6), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0027;
	}

IL_0027:
	{
		Vector3_t2825674791  L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern "C"  Vector3_t2825674791  Vector3_op_Multiply_m714631592 (RuntimeObject * __this /* static, unused */, float ___d0, Vector3_t2825674791  ___a1, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a1)->get_x_1();
		float L_1 = ___d0;
		float L_2 = (&___a1)->get_y_2();
		float L_3 = ___d0;
		float L_4 = (&___a1)->get_z_3();
		float L_5 = ___d0;
		Vector3_t2825674791  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m874567701((&L_6), ((float)((float)L_0*(float)L_1)), ((float)((float)L_2*(float)L_3)), ((float)((float)L_4*(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0027;
	}

IL_0027:
	{
		Vector3_t2825674791  L_7 = V_0;
		return L_7;
	}
}
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t2825674791  Vector3_op_Division_m2525477617 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t2825674791  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		Vector3_t2825674791  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m874567701((&L_6), ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0027;
	}

IL_0027:
	{
		Vector3_t2825674791  L_7 = V_0;
		return L_7;
	}
}
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m3932752819 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___lhs0, Vector3_t2825674791  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_op_Equality_m3932752819_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Vector3_t2825674791  L_0 = ___lhs0;
		Vector3_t2825674791  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		Vector3_t2825674791  L_2 = Vector3_op_Subtraction_m2076583184(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector3_SqrMagnitude_m3584580626(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_001a;
	}

IL_001a:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Inequality_m3309832647 (RuntimeObject * __this /* static, unused */, Vector3_t2825674791  ___lhs0, Vector3_t2825674791  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_op_Inequality_m3309832647_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Vector3_t2825674791  L_0 = ___lhs0;
		Vector3_t2825674791  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_t2825674791_il2cpp_TypeInfo_var);
		bool L_2 = Vector3_op_Equality_m3932752819(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
		goto IL_0011;
	}

IL_0011:
	{
		bool L_3 = V_0;
		return L_3;
	}
}
// System.String UnityEngine.Vector3::ToString()
extern "C"  String_t* Vector3_ToString_m1883038722 (Vector3_t2825674791 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3_ToString_m1883038722_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1100384052* L_0 = ((ObjectU5BU5D_t1100384052*)SZArrayNew(ObjectU5BU5D_t1100384052_il2cpp_TypeInfo_var, (uint32_t)3));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t1534300504_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1100384052* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t1534300504_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t1100384052* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t1534300504_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		String_t* L_12 = UnityString_Format_m3782610777(NULL /*static, unused*/, _stringLiteral3570444392, L_8, /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0041;
	}

IL_0041:
	{
		String_t* L_13 = V_0;
		return L_13;
	}
}
extern "C"  String_t* Vector3_ToString_m1883038722_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector3_t2825674791 * _thisAdjusted = reinterpret_cast<Vector3_t2825674791 *>(__this + 1);
	return Vector3_ToString_m1883038722(_thisAdjusted, method);
}
// System.Void UnityEngine.Vector3::.cctor()
extern "C"  void Vector3__cctor_m2217123019 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3__cctor_m2217123019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector3_t2825674791  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m874567701((&L_0), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->set_zeroVector_4(L_0);
		Vector3_t2825674791  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector3__ctor_m874567701((&L_1), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->set_oneVector_5(L_1);
		Vector3_t2825674791  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector3__ctor_m874567701((&L_2), (0.0f), (1.0f), (0.0f), /*hidden argument*/NULL);
		((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->set_upVector_6(L_2);
		Vector3_t2825674791  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector3__ctor_m874567701((&L_3), (0.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->set_downVector_7(L_3);
		Vector3_t2825674791  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Vector3__ctor_m874567701((&L_4), (-1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->set_leftVector_8(L_4);
		Vector3_t2825674791  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Vector3__ctor_m874567701((&L_5), (1.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->set_rightVector_9(L_5);
		Vector3_t2825674791  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Vector3__ctor_m874567701((&L_6), (0.0f), (0.0f), (1.0f), /*hidden argument*/NULL);
		((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->set_forwardVector_10(L_6);
		Vector3_t2825674791  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m874567701((&L_7), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->set_backVector_11(L_7);
		Vector3_t2825674791  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector3__ctor_m874567701((&L_8), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->set_positiveInfinityVector_12(L_8);
		Vector3_t2825674791  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m874567701((&L_9), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector3_t2825674791_StaticFields*)il2cpp_codegen_static_fields_for(Vector3_t2825674791_il2cpp_TypeInfo_var))->set_negativeInfinityVector_13(L_9);
		return;
	}
}
// System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Vector4__ctor_m704222614 (Vector4_t2651204353 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_1(L_0);
		float L_1 = ___y1;
		__this->set_y_2(L_1);
		float L_2 = ___z2;
		__this->set_z_3(L_2);
		float L_3 = ___w3;
		__this->set_w_4(L_3);
		return;
	}
}
extern "C"  void Vector4__ctor_m704222614_AdjustorThunk (RuntimeObject * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	Vector4_t2651204353 * _thisAdjusted = reinterpret_cast<Vector4_t2651204353 *>(__this + 1);
	Vector4__ctor_m704222614(_thisAdjusted, ___x0, ___y1, ___z2, ___w3, method);
}
// System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern "C"  float Vector4_get_Item_m1609178195 (Vector4_t2651204353 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_get_Item_m1609178195_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_0028;
			}
			case 2:
			{
				goto IL_0034;
			}
			case 3:
			{
				goto IL_0040;
			}
		}
	}
	{
		goto IL_004c;
	}

IL_001c:
	{
		float L_1 = __this->get_x_1();
		V_0 = L_1;
		goto IL_0057;
	}

IL_0028:
	{
		float L_2 = __this->get_y_2();
		V_0 = L_2;
		goto IL_0057;
	}

IL_0034:
	{
		float L_3 = __this->get_z_3();
		V_0 = L_3;
		goto IL_0057;
	}

IL_0040:
	{
		float L_4 = __this->get_w_4();
		V_0 = L_4;
		goto IL_0057;
	}

IL_004c:
	{
		IndexOutOfRangeException_t481375535 * L_5 = (IndexOutOfRangeException_t481375535 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t481375535_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3252438219(L_5, _stringLiteral3543909149, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0057:
	{
		float L_6 = V_0;
		return L_6;
	}
}
extern "C"  float Vector4_get_Item_m1609178195_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, const RuntimeMethod* method)
{
	Vector4_t2651204353 * _thisAdjusted = reinterpret_cast<Vector4_t2651204353 *>(__this + 1);
	return Vector4_get_Item_m1609178195(_thisAdjusted, ___index0, method);
}
// System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
extern "C"  void Vector4_set_Item_m57064049 (Vector4_t2651204353 * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_set_Item_m57064049_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___index0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_0028;
			}
			case 2:
			{
				goto IL_0034;
			}
			case 3:
			{
				goto IL_0040;
			}
		}
	}
	{
		goto IL_004c;
	}

IL_001c:
	{
		float L_1 = ___value1;
		__this->set_x_1(L_1);
		goto IL_0057;
	}

IL_0028:
	{
		float L_2 = ___value1;
		__this->set_y_2(L_2);
		goto IL_0057;
	}

IL_0034:
	{
		float L_3 = ___value1;
		__this->set_z_3(L_3);
		goto IL_0057;
	}

IL_0040:
	{
		float L_4 = ___value1;
		__this->set_w_4(L_4);
		goto IL_0057;
	}

IL_004c:
	{
		IndexOutOfRangeException_t481375535 * L_5 = (IndexOutOfRangeException_t481375535 *)il2cpp_codegen_object_new(IndexOutOfRangeException_t481375535_il2cpp_TypeInfo_var);
		IndexOutOfRangeException__ctor_m3252438219(L_5, _stringLiteral3543909149, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0057:
	{
		return;
	}
}
extern "C"  void Vector4_set_Item_m57064049_AdjustorThunk (RuntimeObject * __this, int32_t ___index0, float ___value1, const RuntimeMethod* method)
{
	Vector4_t2651204353 * _thisAdjusted = reinterpret_cast<Vector4_t2651204353 *>(__this + 1);
	Vector4_set_Item_m57064049(_thisAdjusted, ___index0, ___value1, method);
}
// System.Int32 UnityEngine.Vector4::GetHashCode()
extern "C"  int32_t Vector4_GetHashCode_m1317289159 (Vector4_t2651204353 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		float* L_0 = __this->get_address_of_x_1();
		int32_t L_1 = Single_GetHashCode_m1220171593(L_0, /*hidden argument*/NULL);
		float* L_2 = __this->get_address_of_y_2();
		int32_t L_3 = Single_GetHashCode_m1220171593(L_2, /*hidden argument*/NULL);
		float* L_4 = __this->get_address_of_z_3();
		int32_t L_5 = Single_GetHashCode_m1220171593(L_4, /*hidden argument*/NULL);
		float* L_6 = __this->get_address_of_w_4();
		int32_t L_7 = Single_GetHashCode_m1220171593(L_6, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1^(int32_t)((int32_t)((int32_t)L_3<<(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_5>>(int32_t)2))))^(int32_t)((int32_t)((int32_t)L_7>>(int32_t)1))));
		goto IL_0054;
	}

IL_0054:
	{
		int32_t L_8 = V_0;
		return L_8;
	}
}
extern "C"  int32_t Vector4_GetHashCode_m1317289159_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector4_t2651204353 * _thisAdjusted = reinterpret_cast<Vector4_t2651204353 *>(__this + 1);
	return Vector4_GetHashCode_m1317289159(_thisAdjusted, method);
}
// System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern "C"  bool Vector4_Equals_m3717557000 (Vector4_t2651204353 * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_Equals_m3717557000_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	Vector4_t2651204353  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B7_0 = 0;
	{
		RuntimeObject * L_0 = ___other0;
		if (((RuntimeObject *)IsInstSealed((RuntimeObject*)L_0, Vector4_t2651204353_il2cpp_TypeInfo_var)))
		{
			goto IL_0013;
		}
	}
	{
		V_0 = (bool)0;
		goto IL_007a;
	}

IL_0013:
	{
		RuntimeObject * L_1 = ___other0;
		V_1 = ((*(Vector4_t2651204353 *)((Vector4_t2651204353 *)UnBox(L_1, Vector4_t2651204353_il2cpp_TypeInfo_var))));
		float* L_2 = __this->get_address_of_x_1();
		float L_3 = (&V_1)->get_x_1();
		bool L_4 = Single_Equals_m4010867313(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0073;
		}
	}
	{
		float* L_5 = __this->get_address_of_y_2();
		float L_6 = (&V_1)->get_y_2();
		bool L_7 = Single_Equals_m4010867313(L_5, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0073;
		}
	}
	{
		float* L_8 = __this->get_address_of_z_3();
		float L_9 = (&V_1)->get_z_3();
		bool L_10 = Single_Equals_m4010867313(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		float* L_11 = __this->get_address_of_w_4();
		float L_12 = (&V_1)->get_w_4();
		bool L_13 = Single_Equals_m4010867313(L_11, L_12, /*hidden argument*/NULL);
		G_B7_0 = ((int32_t)(L_13));
		goto IL_0074;
	}

IL_0073:
	{
		G_B7_0 = 0;
	}

IL_0074:
	{
		V_0 = (bool)G_B7_0;
		goto IL_007a;
	}

IL_007a:
	{
		bool L_14 = V_0;
		return L_14;
	}
}
extern "C"  bool Vector4_Equals_m3717557000_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___other0, const RuntimeMethod* method)
{
	Vector4_t2651204353 * _thisAdjusted = reinterpret_cast<Vector4_t2651204353 *>(__this + 1);
	return Vector4_Equals_m3717557000(_thisAdjusted, ___other0, method);
}
// System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  float Vector4_Dot_m2098979705 (RuntimeObject * __this /* static, unused */, Vector4_t2651204353  ___a0, Vector4_t2651204353  ___b1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		V_0 = ((float)((float)((float)((float)((float)((float)((float)((float)L_0*(float)L_1))+(float)((float)((float)L_2*(float)L_3))))+(float)((float)((float)L_4*(float)L_5))))+(float)((float)((float)L_6*(float)L_7))));
		goto IL_0046;
	}

IL_0046:
	{
		float L_8 = V_0;
		return L_8;
	}
}
// System.Single UnityEngine.Vector4::get_sqrMagnitude()
extern "C"  float Vector4_get_sqrMagnitude_m3645389319 (Vector4_t2651204353 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_get_sqrMagnitude_m3645389319_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2651204353_il2cpp_TypeInfo_var);
		float L_0 = Vector4_Dot_m2098979705(NULL /*static, unused*/, (*(Vector4_t2651204353 *)__this), (*(Vector4_t2651204353 *)__this), /*hidden argument*/NULL);
		V_0 = L_0;
		goto IL_0018;
	}

IL_0018:
	{
		float L_1 = V_0;
		return L_1;
	}
}
extern "C"  float Vector4_get_sqrMagnitude_m3645389319_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector4_t2651204353 * _thisAdjusted = reinterpret_cast<Vector4_t2651204353 *>(__this + 1);
	return Vector4_get_sqrMagnitude_m3645389319(_thisAdjusted, method);
}
// UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern "C"  Vector4_t2651204353  Vector4_get_zero_m1006770021 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_get_zero_m1006770021_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector4_t2651204353  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2651204353_il2cpp_TypeInfo_var);
		Vector4_t2651204353  L_0 = ((Vector4_t2651204353_StaticFields*)il2cpp_codegen_static_fields_for(Vector4_t2651204353_il2cpp_TypeInfo_var))->get_zeroVector_5();
		V_0 = L_0;
		goto IL_000c;
	}

IL_000c:
	{
		Vector4_t2651204353  L_1 = V_0;
		return L_1;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Subtraction(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  Vector4_t2651204353  Vector4_op_Subtraction_m2274273165 (RuntimeObject * __this /* static, unused */, Vector4_t2651204353  ___a0, Vector4_t2651204353  ___b1, const RuntimeMethod* method)
{
	Vector4_t2651204353  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = (&___b1)->get_x_1();
		float L_2 = (&___a0)->get_y_2();
		float L_3 = (&___b1)->get_y_2();
		float L_4 = (&___a0)->get_z_3();
		float L_5 = (&___b1)->get_z_3();
		float L_6 = (&___a0)->get_w_4();
		float L_7 = (&___b1)->get_w_4();
		Vector4_t2651204353  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m704222614((&L_8), ((float)((float)L_0-(float)L_1)), ((float)((float)L_2-(float)L_3)), ((float)((float)L_4-(float)L_5)), ((float)((float)L_6-(float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0048;
	}

IL_0048:
	{
		Vector4_t2651204353  L_9 = V_0;
		return L_9;
	}
}
// UnityEngine.Vector4 UnityEngine.Vector4::op_Division(UnityEngine.Vector4,System.Single)
extern "C"  Vector4_t2651204353  Vector4_op_Division_m1557775502 (RuntimeObject * __this /* static, unused */, Vector4_t2651204353  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector4_t2651204353  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		float L_0 = (&___a0)->get_x_1();
		float L_1 = ___d1;
		float L_2 = (&___a0)->get_y_2();
		float L_3 = ___d1;
		float L_4 = (&___a0)->get_z_3();
		float L_5 = ___d1;
		float L_6 = (&___a0)->get_w_4();
		float L_7 = ___d1;
		Vector4_t2651204353  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Vector4__ctor_m704222614((&L_8), ((float)((float)L_0/(float)L_1)), ((float)((float)L_2/(float)L_3)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_6/(float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0030;
	}

IL_0030:
	{
		Vector4_t2651204353  L_9 = V_0;
		return L_9;
	}
}
// System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern "C"  bool Vector4_op_Equality_m3505556570 (RuntimeObject * __this /* static, unused */, Vector4_t2651204353  ___lhs0, Vector4_t2651204353  ___rhs1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_op_Equality_m3505556570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		Vector4_t2651204353  L_0 = ___lhs0;
		Vector4_t2651204353  L_1 = ___rhs1;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2651204353_il2cpp_TypeInfo_var);
		Vector4_t2651204353  L_2 = Vector4_op_Subtraction_m2274273165(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		float L_3 = Vector4_SqrMagnitude_m3184081721(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		V_0 = (bool)((((float)L_3) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_001a;
	}

IL_001a:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.String UnityEngine.Vector4::ToString()
extern "C"  String_t* Vector4_ToString_m2329116964 (Vector4_t2651204353 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_ToString_m2329116964_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ObjectU5BU5D_t1100384052* L_0 = ((ObjectU5BU5D_t1100384052*)SZArrayNew(ObjectU5BU5D_t1100384052_il2cpp_TypeInfo_var, (uint32_t)4));
		float L_1 = __this->get_x_1();
		float L_2 = L_1;
		RuntimeObject * L_3 = Box(Single_t1534300504_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1100384052* L_4 = L_0;
		float L_5 = __this->get_y_2();
		float L_6 = L_5;
		RuntimeObject * L_7 = Box(Single_t1534300504_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t1100384052* L_8 = L_4;
		float L_9 = __this->get_z_3();
		float L_10 = L_9;
		RuntimeObject * L_11 = Box(Single_t1534300504_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_11);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_11);
		ObjectU5BU5D_t1100384052* L_12 = L_8;
		float L_13 = __this->get_w_4();
		float L_14 = L_13;
		RuntimeObject * L_15 = Box(Single_t1534300504_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_15);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_15);
		String_t* L_16 = UnityString_Format_m3782610777(NULL /*static, unused*/, _stringLiteral1736253540, L_12, /*hidden argument*/NULL);
		V_0 = L_16;
		goto IL_004f;
	}

IL_004f:
	{
		String_t* L_17 = V_0;
		return L_17;
	}
}
extern "C"  String_t* Vector4_ToString_m2329116964_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	Vector4_t2651204353 * _thisAdjusted = reinterpret_cast<Vector4_t2651204353 *>(__this + 1);
	return Vector4_ToString_m2329116964(_thisAdjusted, method);
}
// System.Single UnityEngine.Vector4::SqrMagnitude(UnityEngine.Vector4)
extern "C"  float Vector4_SqrMagnitude_m3184081721 (RuntimeObject * __this /* static, unused */, Vector4_t2651204353  ___a0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4_SqrMagnitude_m3184081721_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		Vector4_t2651204353  L_0 = ___a0;
		Vector4_t2651204353  L_1 = ___a0;
		IL2CPP_RUNTIME_CLASS_INIT(Vector4_t2651204353_il2cpp_TypeInfo_var);
		float L_2 = Vector4_Dot_m2098979705(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.Vector4::.cctor()
extern "C"  void Vector4__cctor_m1064385982 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector4__cctor_m1064385982_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Vector4_t2651204353  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector4__ctor_m704222614((&L_0), (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		((Vector4_t2651204353_StaticFields*)il2cpp_codegen_static_fields_for(Vector4_t2651204353_il2cpp_TypeInfo_var))->set_zeroVector_5(L_0);
		Vector4_t2651204353  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector4__ctor_m704222614((&L_1), (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((Vector4_t2651204353_StaticFields*)il2cpp_codegen_static_fields_for(Vector4_t2651204353_il2cpp_TypeInfo_var))->set_oneVector_6(L_1);
		Vector4_t2651204353  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Vector4__ctor_m704222614((&L_2), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), (std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector4_t2651204353_StaticFields*)il2cpp_codegen_static_fields_for(Vector4_t2651204353_il2cpp_TypeInfo_var))->set_positiveInfinityVector_7(L_2);
		Vector4_t2651204353  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Vector4__ctor_m704222614((&L_3), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), (-std::numeric_limits<float>::infinity()), /*hidden argument*/NULL);
		((Vector4_t2651204353_StaticFields*)il2cpp_codegen_static_fields_for(Vector4_t2651204353_il2cpp_TypeInfo_var))->set_negativeInfinityVector_8(L_3);
		return;
	}
}
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C"  void WaitForEndOfFrame__ctor_m2923242398 (WaitForEndOfFrame_t1533502071 * __this, const RuntimeMethod* method)
{
	{
		YieldInstruction__ctor_m1436237666(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C"  void WaitForFixedUpdate__ctor_m4017385068 (WaitForFixedUpdate_t3543075621 * __this, const RuntimeMethod* method)
{
	{
		YieldInstruction__ctor_m1436237666(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t4091331090_marshal_pinvoke(const WaitForSeconds_t4091331090& unmarshaled, WaitForSeconds_t4091331090_marshaled_pinvoke& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.get_m_Seconds_0();
}
extern "C" void WaitForSeconds_t4091331090_marshal_pinvoke_back(const WaitForSeconds_t4091331090_marshaled_pinvoke& marshaled, WaitForSeconds_t4091331090& unmarshaled)
{
	float unmarshaled_m_Seconds_temp_0 = 0.0f;
	unmarshaled_m_Seconds_temp_0 = marshaled.___m_Seconds_0;
	unmarshaled.set_m_Seconds_0(unmarshaled_m_Seconds_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t4091331090_marshal_pinvoke_cleanup(WaitForSeconds_t4091331090_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t4091331090_marshal_com(const WaitForSeconds_t4091331090& unmarshaled, WaitForSeconds_t4091331090_marshaled_com& marshaled)
{
	marshaled.___m_Seconds_0 = unmarshaled.get_m_Seconds_0();
}
extern "C" void WaitForSeconds_t4091331090_marshal_com_back(const WaitForSeconds_t4091331090_marshaled_com& marshaled, WaitForSeconds_t4091331090& unmarshaled)
{
	float unmarshaled_m_Seconds_temp_0 = 0.0f;
	unmarshaled_m_Seconds_temp_0 = marshaled.___m_Seconds_0;
	unmarshaled.set_m_Seconds_0(unmarshaled_m_Seconds_temp_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.WaitForSeconds
extern "C" void WaitForSeconds_t4091331090_marshal_com_cleanup(WaitForSeconds_t4091331090_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C"  void WaitForSeconds__ctor_m543814704 (WaitForSeconds_t4091331090 * __this, float ___seconds0, const RuntimeMethod* method)
{
	{
		YieldInstruction__ctor_m1436237666(__this, /*hidden argument*/NULL);
		float L_0 = ___seconds0;
		__this->set_m_Seconds_0(L_0);
		return;
	}
}
// System.Void UnityEngine.WaitForSecondsRealtime::.ctor(System.Single)
extern "C"  void WaitForSecondsRealtime__ctor_m3340799955 (WaitForSecondsRealtime_t4106866811 * __this, float ___time0, const RuntimeMethod* method)
{
	{
		CustomYieldInstruction__ctor_m879225322(__this, /*hidden argument*/NULL);
		float L_0 = Time_get_realtimeSinceStartup_m1699101199(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = ___time0;
		__this->set_waitTime_0(((float)((float)L_0+(float)L_1)));
		return;
	}
}
// System.Boolean UnityEngine.WaitForSecondsRealtime::get_keepWaiting()
extern "C"  bool WaitForSecondsRealtime_get_keepWaiting_m2219205928 (WaitForSecondsRealtime_t4106866811 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		float L_0 = Time_get_realtimeSinceStartup_m1699101199(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_waitTime_0();
		V_0 = (bool)((((float)L_0) < ((float)L_1))? 1 : 0);
		goto IL_0014;
	}

IL_0014:
	{
		bool L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityEngine.WritableAttribute::.ctor()
extern "C"  void WritableAttribute__ctor_m1650227649 (WritableAttribute_t902445776 * __this, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m959732044(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t1400753137_marshal_pinvoke(const YieldInstruction_t1400753137& unmarshaled, YieldInstruction_t1400753137_marshaled_pinvoke& marshaled)
{
}
extern "C" void YieldInstruction_t1400753137_marshal_pinvoke_back(const YieldInstruction_t1400753137_marshaled_pinvoke& marshaled, YieldInstruction_t1400753137& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t1400753137_marshal_pinvoke_cleanup(YieldInstruction_t1400753137_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t1400753137_marshal_com(const YieldInstruction_t1400753137& unmarshaled, YieldInstruction_t1400753137_marshaled_com& marshaled)
{
}
extern "C" void YieldInstruction_t1400753137_marshal_com_back(const YieldInstruction_t1400753137_marshaled_com& marshaled, YieldInstruction_t1400753137& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t1400753137_marshal_com_cleanup(YieldInstruction_t1400753137_marshaled_com& marshaled)
{
}
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C"  void YieldInstruction__ctor_m1436237666 (YieldInstruction_t1400753137 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m1723520498(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.GenericStack::.ctor()
extern "C"  void GenericStack__ctor_m2055329107 (GenericStack_t1699001310 * __this, const RuntimeMethod* method)
{
	{
		Stack__ctor_m2016713434(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.MathfInternal::.cctor()
extern "C"  void MathfInternal__cctor_m3090305122 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MathfInternal__cctor_m3090305122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t2540608617_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t2540608617_il2cpp_TypeInfo_var))->set_FloatMinNormal_0((1.17549435E-38f));
		il2cpp_codegen_memory_barrier();
		((MathfInternal_t2540608617_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t2540608617_il2cpp_TypeInfo_var))->set_FloatMinDenormal_1((1.401298E-45f));
		((MathfInternal_t2540608617_StaticFields*)il2cpp_codegen_static_fields_for(MathfInternal_t2540608617_il2cpp_TypeInfo_var))->set_IsFlushToZeroEnabled_2((bool)1);
		return;
	}
}
// System.Delegate UnityEngineInternal.NetFxCoreExtensions::CreateDelegate(System.Reflection.MethodInfo,System.Type,System.Object)
extern "C"  Delegate_t1310841479 * NetFxCoreExtensions_CreateDelegate_m781162957 (RuntimeObject * __this /* static, unused */, MethodInfo_t * ___self0, Type_t * ___delegateType1, RuntimeObject * ___target2, const RuntimeMethod* method)
{
	Delegate_t1310841479 * V_0 = NULL;
	{
		Type_t * L_0 = ___delegateType1;
		RuntimeObject * L_1 = ___target2;
		MethodInfo_t * L_2 = ___self0;
		Delegate_t1310841479 * L_3 = Delegate_CreateDelegate_m3508464446(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_000f;
	}

IL_000f:
	{
		Delegate_t1310841479 * L_4 = V_0;
		return L_4;
	}
}
// System.Reflection.MethodInfo UnityEngineInternal.NetFxCoreExtensions::GetMethodInfo(System.Delegate)
extern "C"  MethodInfo_t * NetFxCoreExtensions_GetMethodInfo_m925993656 (RuntimeObject * __this /* static, unused */, Delegate_t1310841479 * ___self0, const RuntimeMethod* method)
{
	MethodInfo_t * V_0 = NULL;
	{
		Delegate_t1310841479 * L_0 = ___self0;
		NullCheck(L_0);
		MethodInfo_t * L_1 = Delegate_get_Method_m3777026472(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_000d;
	}

IL_000d:
	{
		MethodInfo_t * L_2 = V_0;
		return L_2;
	}
}
// System.Delegate UnityEngineInternal.ScriptingUtils::CreateDelegate(System.Type,System.Reflection.MethodInfo)
extern "C"  Delegate_t1310841479 * ScriptingUtils_CreateDelegate_m732690307 (RuntimeObject * __this /* static, unused */, Type_t * ___type0, MethodInfo_t * ___methodInfo1, const RuntimeMethod* method)
{
	Delegate_t1310841479 * V_0 = NULL;
	{
		Type_t * L_0 = ___type0;
		MethodInfo_t * L_1 = ___methodInfo1;
		Delegate_t1310841479 * L_2 = Delegate_CreateDelegate_m3714199633(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		goto IL_000e;
	}

IL_000e:
	{
		Delegate_t1310841479 * L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern "C"  void TypeInferenceRuleAttribute__ctor_m643179438 (TypeInferenceRuleAttribute_t1740219836 * __this, int32_t ___rule0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TypeInferenceRuleAttribute__ctor_m643179438_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = Box(TypeInferenceRules_t2608634147_il2cpp_TypeInfo_var, (&___rule0));
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		*(&___rule0) = *(int32_t*)UnBox(L_0);
		TypeInferenceRuleAttribute__ctor_m3182420183(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern "C"  void TypeInferenceRuleAttribute__ctor_m3182420183 (TypeInferenceRuleAttribute_t1740219836 * __this, String_t* ___rule0, const RuntimeMethod* method)
{
	{
		Attribute__ctor_m959732044(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___rule0;
		__this->set__rule_0(L_0);
		return;
	}
}
// System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern "C"  String_t* TypeInferenceRuleAttribute_ToString_m1094836576 (TypeInferenceRuleAttribute_t1740219836 * __this, const RuntimeMethod* method)
{
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get__rule_0();
		V_0 = L_0;
		goto IL_000d;
	}

IL_000d:
	{
		String_t* L_1 = V_0;
		return L_1;
	}
}
// System.String UnityEngineInternal.WebRequestUtils::RedirectTo(System.String,System.String)
extern "C"  String_t* WebRequestUtils_RedirectTo_m493488564 (RuntimeObject * __this /* static, unused */, String_t* ___baseUri0, String_t* ___redirectUri1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebRequestUtils_RedirectTo_m493488564_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Uri_t3689888142 * V_0 = NULL;
	String_t* V_1 = NULL;
	Uri_t3689888142 * V_2 = NULL;
	Uri_t3689888142 * V_3 = NULL;
	{
		String_t* L_0 = ___redirectUri1;
		NullCheck(L_0);
		Il2CppChar L_1 = String_get_Chars_m2121776380(L_0, 0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_2 = ___redirectUri1;
		Uri_t3689888142 * L_3 = (Uri_t3689888142 *)il2cpp_codegen_object_new(Uri_t3689888142_il2cpp_TypeInfo_var);
		Uri__ctor_m2370721803(L_3, L_2, 2, /*hidden argument*/NULL);
		V_0 = L_3;
		goto IL_0024;
	}

IL_001c:
	{
		String_t* L_4 = ___redirectUri1;
		Uri_t3689888142 * L_5 = (Uri_t3689888142 *)il2cpp_codegen_object_new(Uri_t3689888142_il2cpp_TypeInfo_var);
		Uri__ctor_m2370721803(L_5, L_4, 0, /*hidden argument*/NULL);
		V_0 = L_5;
	}

IL_0024:
	{
		Uri_t3689888142 * L_6 = V_0;
		NullCheck(L_6);
		bool L_7 = Uri_get_IsAbsoluteUri_m2908407282(L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		Uri_t3689888142 * L_8 = V_0;
		NullCheck(L_8);
		String_t* L_9 = Uri_get_AbsoluteUri_m2801286058(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		goto IL_0057;
	}

IL_003b:
	{
		String_t* L_10 = ___baseUri0;
		Uri_t3689888142 * L_11 = (Uri_t3689888142 *)il2cpp_codegen_object_new(Uri_t3689888142_il2cpp_TypeInfo_var);
		Uri__ctor_m2370721803(L_11, L_10, 1, /*hidden argument*/NULL);
		V_2 = L_11;
		Uri_t3689888142 * L_12 = V_2;
		Uri_t3689888142 * L_13 = V_0;
		Uri_t3689888142 * L_14 = (Uri_t3689888142 *)il2cpp_codegen_object_new(Uri_t3689888142_il2cpp_TypeInfo_var);
		Uri__ctor_m1216760077(L_14, L_12, L_13, /*hidden argument*/NULL);
		V_3 = L_14;
		Uri_t3689888142 * L_15 = V_3;
		NullCheck(L_15);
		String_t* L_16 = Uri_get_AbsoluteUri_m2801286058(L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		goto IL_0057;
	}

IL_0057:
	{
		String_t* L_17 = V_1;
		return L_17;
	}
}
// System.Void UnityEngineInternal.WebRequestUtils::.cctor()
extern "C"  void WebRequestUtils__cctor_m3336698454 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WebRequestUtils__cctor_m3336698454_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Regex_t1806904682 * L_0 = (Regex_t1806904682 *)il2cpp_codegen_object_new(Regex_t1806904682_il2cpp_TypeInfo_var);
		Regex__ctor_m3638139776(L_0, _stringLiteral2803585452, /*hidden argument*/NULL);
		((WebRequestUtils_t2232581255_StaticFields*)il2cpp_codegen_static_fields_for(WebRequestUtils_t2232581255_il2cpp_TypeInfo_var))->set_domainRegex_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
