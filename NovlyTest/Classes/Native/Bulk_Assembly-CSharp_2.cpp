﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// Spine.Unity.SpineEvent
struct SpineEvent_t1674004351;
// System.String
struct String_t;
// Spine.Unity.SpineAttributeBase
struct SpineAttributeBase_t3125339574;
// Spine.Unity.SpineIkConstraint
struct SpineIkConstraint_t203333819;
// UnityEngine.Mesh
struct Mesh_t3526862104;
// UnityEngine.Object
struct Object_t350248726;
// Spine.Unity.SpinePathConstraint
struct SpinePathConstraint_t1221980072;
// Spine.Unity.SpineSkin
struct SpineSkin_t1537368333;
// Spine.Unity.SpineSlot
struct SpineSlot_t3243618918;
// Spine.Unity.SpineTransformConstraint
struct SpineTransformConstraint_t2634477230;
// System.Object[]
struct ObjectU5BU5D_t1100384052;
// Spine.Unity.UpdateBonesDelegate
struct UpdateBonesDelegate_t500272946;
// Spine.Unity.ISkeletonAnimation
struct ISkeletonAnimation_t2072256521;
// System.IAsyncResult
struct IAsyncResult_t911908533;
// System.AsyncCallback
struct AsyncCallback_t3972436406;
// Spine.Unity.WaitForSpineAnimationComplete
struct WaitForSpineAnimationComplete_t694865537;
// Spine.TrackEntry
struct TrackEntry_t4062297782;
// Spine.AnimationState/TrackEntryDelegate
struct TrackEntryDelegate_t1807657294;
// Spine.Unity.WaitForSpineEvent
struct WaitForSpineEvent_t3387552986;
// Spine.AnimationState
struct AnimationState_t2858486651;
// Spine.EventData
struct EventData_t295981108;
// Spine.Unity.SkeletonAnimation
struct SkeletonAnimation_t2012766265;
// Spine.AnimationState/TrackEntryEventDelegate
struct TrackEntryEventDelegate_t471240152;
// Spine.Event
struct Event_t27715519;
// Spine.Unity.WaitForSpineTrackEntryEnd
struct WaitForSpineTrackEntryEnd_t2837745715;
// Spine.VertexAttachment
struct VertexAttachment_t1776545046;
// Spine.Attachment
struct Attachment_t397756874;
// System.Int32[]
struct Int32U5BU5D_t281224829;
// System.Single[]
struct SingleU5BU5D_t3989243465;
// Spine.Slot
struct Slot_t1804116343;
// Spine.Bone
struct Bone_t1763862836;
// Spine.Bone[]
struct BoneU5BU5D_t600500093;
// Spine.BoneData
struct BoneData_t1194659452;
// Spine.Skeleton
struct Skeleton_t1045203634;
// Spine.ExposedList`1<Spine.Bone>
struct ExposedList_1_t3613858675;
// Spine.SlotData
struct SlotData_t587781850;
// Spine.ExposedList`1<System.Single>
struct ExposedList_1_t3384296343;
// Spine.Animation
struct Animation_t2067143511;
// Spine.AnimationStateData
struct AnimationStateData_t1644285503;
// Spine.Pool`1<Spine.TrackEntry>
struct Pool_1_t4075013739;
// Spine.ExposedList`1<Spine.TrackEntry>
struct ExposedList_1_t1617326325;
// Spine.ExposedList`1<Spine.Event>
struct ExposedList_1_t1877711358;
// Spine.EventQueue
struct EventQueue_t2605283308;
// System.Collections.Generic.HashSet`1<System.Int32>
struct HashSet_1_t2711375974;
// Spine.SkeletonData
struct SkeletonData_t2665604974;
// Spine.ExposedList`1<Spine.Slot>
struct ExposedList_1_t3654112182;
// Spine.ExposedList`1<Spine.IkConstraint>
struct ExposedList_1_t69720520;
// Spine.ExposedList`1<Spine.TransformConstraint>
struct ExposedList_1_t2359534843;
// Spine.ExposedList`1<Spine.PathConstraint>
struct ExposedList_1_t3704659244;
// Spine.ExposedList`1<Spine.IUpdatable>
struct ExposedList_1_t1975705998;
// Spine.Skin
struct Skin_t1695237131;
// Spine.ExposedList`1<System.Int32>
struct ExposedList_1_t1671071859;
// System.Char[]
struct CharU5BU5D_t3669675803;
// System.Void
struct Void_t3157035008;
// UnityEngine.Material
struct Material_t2681358023;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t878649875;
// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate
struct SkeletonRendererDelegate_t878065760;
// Spine.Unity.MeshGeneratorDelegate
struct MeshGeneratorDelegate_t2067952305;
// Spine.Unity.SkeletonDataAsset
struct SkeletonDataAsset_t1706375087;
// System.String[]
struct StringU5BU5D_t656593794;
// System.Collections.Generic.List`1<Spine.Slot>
struct List_1_t3927532068;
// Spine.Unity.SkeletonRenderer/InstructionDelegate
struct InstructionDelegate_t1951844748;
// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material>
struct Dictionary_2_t2258539287;
// System.Collections.Generic.Dictionary`2<Spine.Slot,UnityEngine.Material>
struct Dictionary_2_t14780327;
// UnityEngine.MeshRenderer
struct MeshRenderer_t1621355309;
// UnityEngine.MeshFilter
struct MeshFilter_t36322911;
// Spine.Unity.SkeletonRendererInstruction
struct SkeletonRendererInstruction_t672271390;
// Spine.Unity.MeshGenerator
struct MeshGenerator_t557654067;
// Spine.Unity.MeshRendererBuffers
struct MeshRendererBuffers_t384051837;

extern RuntimeClass* Mesh_t3526862104_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral556233983;
extern const uint32_t SpineMesh_NewMesh_m1415254290_MetadataUsageId;
extern RuntimeClass* ObjectU5BU5D_t1100384052_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t4116043316_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t350248726_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2205639819;
extern Il2CppCodeGenString* _stringLiteral1418032621;
extern const uint32_t SubmeshInstruction_ToString_m1474118231_MetadataUsageId;
extern RuntimeClass* Debug_t16286340_il2cpp_TypeInfo_var;
extern RuntimeClass* TrackEntryDelegate_t1807657294_il2cpp_TypeInfo_var;
extern const RuntimeMethod* WaitForSpineAnimationComplete_HandleComplete_m1383943572_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2631168059;
extern const uint32_t WaitForSpineAnimationComplete_SafeSubscribe_m3415404190_MetadataUsageId;
extern RuntimeClass* IEnumerator_t2667876566_il2cpp_TypeInfo_var;
extern const uint32_t WaitForSpineAnimationComplete_System_Collections_IEnumerator_MoveNext_m3327595576_MetadataUsageId;
extern RuntimeClass* TrackEntryEventDelegate_t471240152_il2cpp_TypeInfo_var;
extern const RuntimeMethod* WaitForSpineEvent_HandleAnimationStateEvent_m3129679492_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2001166017;
extern Il2CppCodeGenString* _stringLiteral350640354;
extern const uint32_t WaitForSpineEvent_Subscribe_m1024680374_MetadataUsageId;
extern const RuntimeMethod* WaitForSpineEvent_HandleAnimationStateEventByName_m504300077_RuntimeMethod_var;
extern Il2CppCodeGenString* _stringLiteral2937246701;
extern const uint32_t WaitForSpineEvent_SubscribeByName_m2257425455_MetadataUsageId;
extern const uint32_t WaitForSpineEvent_HandleAnimationStateEventByName_m504300077_MetadataUsageId;
extern const uint32_t WaitForSpineEvent_HandleAnimationStateEvent_m3129679492_MetadataUsageId;
extern const uint32_t WaitForSpineEvent_NowWaitFor_m1383645710_MetadataUsageId;
extern const uint32_t WaitForSpineEvent_NowWaitFor_m1799525501_MetadataUsageId;
extern const uint32_t WaitForSpineEvent_Clear_m1777236221_MetadataUsageId;
extern const uint32_t WaitForSpineEvent_System_Collections_IEnumerator_MoveNext_m3660352932_MetadataUsageId;
extern const RuntimeMethod* WaitForSpineTrackEntryEnd_HandleEnd_m3109162439_RuntimeMethod_var;
extern const uint32_t WaitForSpineTrackEntryEnd_SafeSubscribe_m952987264_MetadataUsageId;
extern const uint32_t WaitForSpineTrackEntryEnd_System_Collections_IEnumerator_MoveNext_m1365914462_MetadataUsageId;
extern RuntimeClass* VertexAttachment_t1776545046_il2cpp_TypeInfo_var;
extern const uint32_t VertexAttachment__ctor_m2208203245_MetadataUsageId;
extern RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
extern const uint32_t VertexAttachment__cctor_m3545296824_MetadataUsageId;

struct ObjectU5BU5D_t1100384052;
struct Int32U5BU5D_t281224829;
struct SingleU5BU5D_t3989243465;
struct BoneU5BU5D_t600500093;


#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef ATTRIBUTE_T2169343654_H
#define ATTRIBUTE_T2169343654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t2169343654  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T2169343654_H
#ifndef VALUETYPE_T2697047229_H
#define VALUETYPE_T2697047229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2697047229  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2697047229_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2697047229_marshaled_com
{
};
#endif // VALUETYPE_T2697047229_H
#ifndef EXPOSEDLIST_1_T3613858675_H
#define EXPOSEDLIST_1_T3613858675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ExposedList`1<Spine.Bone>
struct  ExposedList_1_t3613858675  : public RuntimeObject
{
public:
	// T[] Spine.ExposedList`1::Items
	BoneU5BU5D_t600500093* ___Items_0;
	// System.Int32 Spine.ExposedList`1::Count
	int32_t ___Count_1;
	// System.Int32 Spine.ExposedList`1::version
	int32_t ___version_4;

public:
	inline static int32_t get_offset_of_Items_0() { return static_cast<int32_t>(offsetof(ExposedList_1_t3613858675, ___Items_0)); }
	inline BoneU5BU5D_t600500093* get_Items_0() const { return ___Items_0; }
	inline BoneU5BU5D_t600500093** get_address_of_Items_0() { return &___Items_0; }
	inline void set_Items_0(BoneU5BU5D_t600500093* value)
	{
		___Items_0 = value;
		Il2CppCodeGenWriteBarrier((&___Items_0), value);
	}

	inline static int32_t get_offset_of_Count_1() { return static_cast<int32_t>(offsetof(ExposedList_1_t3613858675, ___Count_1)); }
	inline int32_t get_Count_1() const { return ___Count_1; }
	inline int32_t* get_address_of_Count_1() { return &___Count_1; }
	inline void set_Count_1(int32_t value)
	{
		___Count_1 = value;
	}

	inline static int32_t get_offset_of_version_4() { return static_cast<int32_t>(offsetof(ExposedList_1_t3613858675, ___version_4)); }
	inline int32_t get_version_4() const { return ___version_4; }
	inline int32_t* get_address_of_version_4() { return &___version_4; }
	inline void set_version_4(int32_t value)
	{
		___version_4 = value;
	}
};

struct ExposedList_1_t3613858675_StaticFields
{
public:
	// T[] Spine.ExposedList`1::EmptyArray
	BoneU5BU5D_t600500093* ___EmptyArray_3;

public:
	inline static int32_t get_offset_of_EmptyArray_3() { return static_cast<int32_t>(offsetof(ExposedList_1_t3613858675_StaticFields, ___EmptyArray_3)); }
	inline BoneU5BU5D_t600500093* get_EmptyArray_3() const { return ___EmptyArray_3; }
	inline BoneU5BU5D_t600500093** get_address_of_EmptyArray_3() { return &___EmptyArray_3; }
	inline void set_EmptyArray_3(BoneU5BU5D_t600500093* value)
	{
		___EmptyArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPOSEDLIST_1_T3613858675_H
#ifndef BONE_T1763862836_H
#define BONE_T1763862836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Bone
struct  Bone_t1763862836  : public RuntimeObject
{
public:
	// Spine.BoneData Spine.Bone::data
	BoneData_t1194659452 * ___data_1;
	// Spine.Skeleton Spine.Bone::skeleton
	Skeleton_t1045203634 * ___skeleton_2;
	// Spine.Bone Spine.Bone::parent
	Bone_t1763862836 * ___parent_3;
	// Spine.ExposedList`1<Spine.Bone> Spine.Bone::children
	ExposedList_1_t3613858675 * ___children_4;
	// System.Single Spine.Bone::x
	float ___x_5;
	// System.Single Spine.Bone::y
	float ___y_6;
	// System.Single Spine.Bone::rotation
	float ___rotation_7;
	// System.Single Spine.Bone::scaleX
	float ___scaleX_8;
	// System.Single Spine.Bone::scaleY
	float ___scaleY_9;
	// System.Single Spine.Bone::shearX
	float ___shearX_10;
	// System.Single Spine.Bone::shearY
	float ___shearY_11;
	// System.Single Spine.Bone::ax
	float ___ax_12;
	// System.Single Spine.Bone::ay
	float ___ay_13;
	// System.Single Spine.Bone::arotation
	float ___arotation_14;
	// System.Single Spine.Bone::ascaleX
	float ___ascaleX_15;
	// System.Single Spine.Bone::ascaleY
	float ___ascaleY_16;
	// System.Single Spine.Bone::ashearX
	float ___ashearX_17;
	// System.Single Spine.Bone::ashearY
	float ___ashearY_18;
	// System.Boolean Spine.Bone::appliedValid
	bool ___appliedValid_19;
	// System.Single Spine.Bone::a
	float ___a_20;
	// System.Single Spine.Bone::b
	float ___b_21;
	// System.Single Spine.Bone::worldX
	float ___worldX_22;
	// System.Single Spine.Bone::c
	float ___c_23;
	// System.Single Spine.Bone::d
	float ___d_24;
	// System.Single Spine.Bone::worldY
	float ___worldY_25;
	// System.Boolean Spine.Bone::sorted
	bool ___sorted_26;

public:
	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___data_1)); }
	inline BoneData_t1194659452 * get_data_1() const { return ___data_1; }
	inline BoneData_t1194659452 ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(BoneData_t1194659452 * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}

	inline static int32_t get_offset_of_skeleton_2() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___skeleton_2)); }
	inline Skeleton_t1045203634 * get_skeleton_2() const { return ___skeleton_2; }
	inline Skeleton_t1045203634 ** get_address_of_skeleton_2() { return &___skeleton_2; }
	inline void set_skeleton_2(Skeleton_t1045203634 * value)
	{
		___skeleton_2 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_2), value);
	}

	inline static int32_t get_offset_of_parent_3() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___parent_3)); }
	inline Bone_t1763862836 * get_parent_3() const { return ___parent_3; }
	inline Bone_t1763862836 ** get_address_of_parent_3() { return &___parent_3; }
	inline void set_parent_3(Bone_t1763862836 * value)
	{
		___parent_3 = value;
		Il2CppCodeGenWriteBarrier((&___parent_3), value);
	}

	inline static int32_t get_offset_of_children_4() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___children_4)); }
	inline ExposedList_1_t3613858675 * get_children_4() const { return ___children_4; }
	inline ExposedList_1_t3613858675 ** get_address_of_children_4() { return &___children_4; }
	inline void set_children_4(ExposedList_1_t3613858675 * value)
	{
		___children_4 = value;
		Il2CppCodeGenWriteBarrier((&___children_4), value);
	}

	inline static int32_t get_offset_of_x_5() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___x_5)); }
	inline float get_x_5() const { return ___x_5; }
	inline float* get_address_of_x_5() { return &___x_5; }
	inline void set_x_5(float value)
	{
		___x_5 = value;
	}

	inline static int32_t get_offset_of_y_6() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___y_6)); }
	inline float get_y_6() const { return ___y_6; }
	inline float* get_address_of_y_6() { return &___y_6; }
	inline void set_y_6(float value)
	{
		___y_6 = value;
	}

	inline static int32_t get_offset_of_rotation_7() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___rotation_7)); }
	inline float get_rotation_7() const { return ___rotation_7; }
	inline float* get_address_of_rotation_7() { return &___rotation_7; }
	inline void set_rotation_7(float value)
	{
		___rotation_7 = value;
	}

	inline static int32_t get_offset_of_scaleX_8() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___scaleX_8)); }
	inline float get_scaleX_8() const { return ___scaleX_8; }
	inline float* get_address_of_scaleX_8() { return &___scaleX_8; }
	inline void set_scaleX_8(float value)
	{
		___scaleX_8 = value;
	}

	inline static int32_t get_offset_of_scaleY_9() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___scaleY_9)); }
	inline float get_scaleY_9() const { return ___scaleY_9; }
	inline float* get_address_of_scaleY_9() { return &___scaleY_9; }
	inline void set_scaleY_9(float value)
	{
		___scaleY_9 = value;
	}

	inline static int32_t get_offset_of_shearX_10() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___shearX_10)); }
	inline float get_shearX_10() const { return ___shearX_10; }
	inline float* get_address_of_shearX_10() { return &___shearX_10; }
	inline void set_shearX_10(float value)
	{
		___shearX_10 = value;
	}

	inline static int32_t get_offset_of_shearY_11() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___shearY_11)); }
	inline float get_shearY_11() const { return ___shearY_11; }
	inline float* get_address_of_shearY_11() { return &___shearY_11; }
	inline void set_shearY_11(float value)
	{
		___shearY_11 = value;
	}

	inline static int32_t get_offset_of_ax_12() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___ax_12)); }
	inline float get_ax_12() const { return ___ax_12; }
	inline float* get_address_of_ax_12() { return &___ax_12; }
	inline void set_ax_12(float value)
	{
		___ax_12 = value;
	}

	inline static int32_t get_offset_of_ay_13() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___ay_13)); }
	inline float get_ay_13() const { return ___ay_13; }
	inline float* get_address_of_ay_13() { return &___ay_13; }
	inline void set_ay_13(float value)
	{
		___ay_13 = value;
	}

	inline static int32_t get_offset_of_arotation_14() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___arotation_14)); }
	inline float get_arotation_14() const { return ___arotation_14; }
	inline float* get_address_of_arotation_14() { return &___arotation_14; }
	inline void set_arotation_14(float value)
	{
		___arotation_14 = value;
	}

	inline static int32_t get_offset_of_ascaleX_15() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___ascaleX_15)); }
	inline float get_ascaleX_15() const { return ___ascaleX_15; }
	inline float* get_address_of_ascaleX_15() { return &___ascaleX_15; }
	inline void set_ascaleX_15(float value)
	{
		___ascaleX_15 = value;
	}

	inline static int32_t get_offset_of_ascaleY_16() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___ascaleY_16)); }
	inline float get_ascaleY_16() const { return ___ascaleY_16; }
	inline float* get_address_of_ascaleY_16() { return &___ascaleY_16; }
	inline void set_ascaleY_16(float value)
	{
		___ascaleY_16 = value;
	}

	inline static int32_t get_offset_of_ashearX_17() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___ashearX_17)); }
	inline float get_ashearX_17() const { return ___ashearX_17; }
	inline float* get_address_of_ashearX_17() { return &___ashearX_17; }
	inline void set_ashearX_17(float value)
	{
		___ashearX_17 = value;
	}

	inline static int32_t get_offset_of_ashearY_18() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___ashearY_18)); }
	inline float get_ashearY_18() const { return ___ashearY_18; }
	inline float* get_address_of_ashearY_18() { return &___ashearY_18; }
	inline void set_ashearY_18(float value)
	{
		___ashearY_18 = value;
	}

	inline static int32_t get_offset_of_appliedValid_19() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___appliedValid_19)); }
	inline bool get_appliedValid_19() const { return ___appliedValid_19; }
	inline bool* get_address_of_appliedValid_19() { return &___appliedValid_19; }
	inline void set_appliedValid_19(bool value)
	{
		___appliedValid_19 = value;
	}

	inline static int32_t get_offset_of_a_20() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___a_20)); }
	inline float get_a_20() const { return ___a_20; }
	inline float* get_address_of_a_20() { return &___a_20; }
	inline void set_a_20(float value)
	{
		___a_20 = value;
	}

	inline static int32_t get_offset_of_b_21() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___b_21)); }
	inline float get_b_21() const { return ___b_21; }
	inline float* get_address_of_b_21() { return &___b_21; }
	inline void set_b_21(float value)
	{
		___b_21 = value;
	}

	inline static int32_t get_offset_of_worldX_22() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___worldX_22)); }
	inline float get_worldX_22() const { return ___worldX_22; }
	inline float* get_address_of_worldX_22() { return &___worldX_22; }
	inline void set_worldX_22(float value)
	{
		___worldX_22 = value;
	}

	inline static int32_t get_offset_of_c_23() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___c_23)); }
	inline float get_c_23() const { return ___c_23; }
	inline float* get_address_of_c_23() { return &___c_23; }
	inline void set_c_23(float value)
	{
		___c_23 = value;
	}

	inline static int32_t get_offset_of_d_24() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___d_24)); }
	inline float get_d_24() const { return ___d_24; }
	inline float* get_address_of_d_24() { return &___d_24; }
	inline void set_d_24(float value)
	{
		___d_24 = value;
	}

	inline static int32_t get_offset_of_worldY_25() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___worldY_25)); }
	inline float get_worldY_25() const { return ___worldY_25; }
	inline float* get_address_of_worldY_25() { return &___worldY_25; }
	inline void set_worldY_25(float value)
	{
		___worldY_25 = value;
	}

	inline static int32_t get_offset_of_sorted_26() { return static_cast<int32_t>(offsetof(Bone_t1763862836, ___sorted_26)); }
	inline bool get_sorted_26() const { return ___sorted_26; }
	inline bool* get_address_of_sorted_26() { return &___sorted_26; }
	inline void set_sorted_26(bool value)
	{
		___sorted_26 = value;
	}
};

struct Bone_t1763862836_StaticFields
{
public:
	// System.Boolean Spine.Bone::yDown
	bool ___yDown_0;

public:
	inline static int32_t get_offset_of_yDown_0() { return static_cast<int32_t>(offsetof(Bone_t1763862836_StaticFields, ___yDown_0)); }
	inline bool get_yDown_0() const { return ___yDown_0; }
	inline bool* get_address_of_yDown_0() { return &___yDown_0; }
	inline void set_yDown_0(bool value)
	{
		___yDown_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BONE_T1763862836_H
#ifndef EXPOSEDLIST_1_T3384296343_H
#define EXPOSEDLIST_1_T3384296343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.ExposedList`1<System.Single>
struct  ExposedList_1_t3384296343  : public RuntimeObject
{
public:
	// T[] Spine.ExposedList`1::Items
	SingleU5BU5D_t3989243465* ___Items_0;
	// System.Int32 Spine.ExposedList`1::Count
	int32_t ___Count_1;
	// System.Int32 Spine.ExposedList`1::version
	int32_t ___version_4;

public:
	inline static int32_t get_offset_of_Items_0() { return static_cast<int32_t>(offsetof(ExposedList_1_t3384296343, ___Items_0)); }
	inline SingleU5BU5D_t3989243465* get_Items_0() const { return ___Items_0; }
	inline SingleU5BU5D_t3989243465** get_address_of_Items_0() { return &___Items_0; }
	inline void set_Items_0(SingleU5BU5D_t3989243465* value)
	{
		___Items_0 = value;
		Il2CppCodeGenWriteBarrier((&___Items_0), value);
	}

	inline static int32_t get_offset_of_Count_1() { return static_cast<int32_t>(offsetof(ExposedList_1_t3384296343, ___Count_1)); }
	inline int32_t get_Count_1() const { return ___Count_1; }
	inline int32_t* get_address_of_Count_1() { return &___Count_1; }
	inline void set_Count_1(int32_t value)
	{
		___Count_1 = value;
	}

	inline static int32_t get_offset_of_version_4() { return static_cast<int32_t>(offsetof(ExposedList_1_t3384296343, ___version_4)); }
	inline int32_t get_version_4() const { return ___version_4; }
	inline int32_t* get_address_of_version_4() { return &___version_4; }
	inline void set_version_4(int32_t value)
	{
		___version_4 = value;
	}
};

struct ExposedList_1_t3384296343_StaticFields
{
public:
	// T[] Spine.ExposedList`1::EmptyArray
	SingleU5BU5D_t3989243465* ___EmptyArray_3;

public:
	inline static int32_t get_offset_of_EmptyArray_3() { return static_cast<int32_t>(offsetof(ExposedList_1_t3384296343_StaticFields, ___EmptyArray_3)); }
	inline SingleU5BU5D_t3989243465* get_EmptyArray_3() const { return ___EmptyArray_3; }
	inline SingleU5BU5D_t3989243465** get_address_of_EmptyArray_3() { return &___EmptyArray_3; }
	inline void set_EmptyArray_3(SingleU5BU5D_t3989243465* value)
	{
		___EmptyArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyArray_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPOSEDLIST_1_T3384296343_H
#ifndef SLOT_T1804116343_H
#define SLOT_T1804116343_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Slot
struct  Slot_t1804116343  : public RuntimeObject
{
public:
	// Spine.SlotData Spine.Slot::data
	SlotData_t587781850 * ___data_0;
	// Spine.Bone Spine.Slot::bone
	Bone_t1763862836 * ___bone_1;
	// System.Single Spine.Slot::r
	float ___r_2;
	// System.Single Spine.Slot::g
	float ___g_3;
	// System.Single Spine.Slot::b
	float ___b_4;
	// System.Single Spine.Slot::a
	float ___a_5;
	// System.Single Spine.Slot::r2
	float ___r2_6;
	// System.Single Spine.Slot::g2
	float ___g2_7;
	// System.Single Spine.Slot::b2
	float ___b2_8;
	// System.Boolean Spine.Slot::hasSecondColor
	bool ___hasSecondColor_9;
	// Spine.Attachment Spine.Slot::attachment
	Attachment_t397756874 * ___attachment_10;
	// System.Single Spine.Slot::attachmentTime
	float ___attachmentTime_11;
	// Spine.ExposedList`1<System.Single> Spine.Slot::attachmentVertices
	ExposedList_1_t3384296343 * ___attachmentVertices_12;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___data_0)); }
	inline SlotData_t587781850 * get_data_0() const { return ___data_0; }
	inline SlotData_t587781850 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(SlotData_t587781850 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bone_1() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___bone_1)); }
	inline Bone_t1763862836 * get_bone_1() const { return ___bone_1; }
	inline Bone_t1763862836 ** get_address_of_bone_1() { return &___bone_1; }
	inline void set_bone_1(Bone_t1763862836 * value)
	{
		___bone_1 = value;
		Il2CppCodeGenWriteBarrier((&___bone_1), value);
	}

	inline static int32_t get_offset_of_r_2() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___r_2)); }
	inline float get_r_2() const { return ___r_2; }
	inline float* get_address_of_r_2() { return &___r_2; }
	inline void set_r_2(float value)
	{
		___r_2 = value;
	}

	inline static int32_t get_offset_of_g_3() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___g_3)); }
	inline float get_g_3() const { return ___g_3; }
	inline float* get_address_of_g_3() { return &___g_3; }
	inline void set_g_3(float value)
	{
		___g_3 = value;
	}

	inline static int32_t get_offset_of_b_4() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___b_4)); }
	inline float get_b_4() const { return ___b_4; }
	inline float* get_address_of_b_4() { return &___b_4; }
	inline void set_b_4(float value)
	{
		___b_4 = value;
	}

	inline static int32_t get_offset_of_a_5() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___a_5)); }
	inline float get_a_5() const { return ___a_5; }
	inline float* get_address_of_a_5() { return &___a_5; }
	inline void set_a_5(float value)
	{
		___a_5 = value;
	}

	inline static int32_t get_offset_of_r2_6() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___r2_6)); }
	inline float get_r2_6() const { return ___r2_6; }
	inline float* get_address_of_r2_6() { return &___r2_6; }
	inline void set_r2_6(float value)
	{
		___r2_6 = value;
	}

	inline static int32_t get_offset_of_g2_7() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___g2_7)); }
	inline float get_g2_7() const { return ___g2_7; }
	inline float* get_address_of_g2_7() { return &___g2_7; }
	inline void set_g2_7(float value)
	{
		___g2_7 = value;
	}

	inline static int32_t get_offset_of_b2_8() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___b2_8)); }
	inline float get_b2_8() const { return ___b2_8; }
	inline float* get_address_of_b2_8() { return &___b2_8; }
	inline void set_b2_8(float value)
	{
		___b2_8 = value;
	}

	inline static int32_t get_offset_of_hasSecondColor_9() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___hasSecondColor_9)); }
	inline bool get_hasSecondColor_9() const { return ___hasSecondColor_9; }
	inline bool* get_address_of_hasSecondColor_9() { return &___hasSecondColor_9; }
	inline void set_hasSecondColor_9(bool value)
	{
		___hasSecondColor_9 = value;
	}

	inline static int32_t get_offset_of_attachment_10() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___attachment_10)); }
	inline Attachment_t397756874 * get_attachment_10() const { return ___attachment_10; }
	inline Attachment_t397756874 ** get_address_of_attachment_10() { return &___attachment_10; }
	inline void set_attachment_10(Attachment_t397756874 * value)
	{
		___attachment_10 = value;
		Il2CppCodeGenWriteBarrier((&___attachment_10), value);
	}

	inline static int32_t get_offset_of_attachmentTime_11() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___attachmentTime_11)); }
	inline float get_attachmentTime_11() const { return ___attachmentTime_11; }
	inline float* get_address_of_attachmentTime_11() { return &___attachmentTime_11; }
	inline void set_attachmentTime_11(float value)
	{
		___attachmentTime_11 = value;
	}

	inline static int32_t get_offset_of_attachmentVertices_12() { return static_cast<int32_t>(offsetof(Slot_t1804116343, ___attachmentVertices_12)); }
	inline ExposedList_1_t3384296343 * get_attachmentVertices_12() const { return ___attachmentVertices_12; }
	inline ExposedList_1_t3384296343 ** get_address_of_attachmentVertices_12() { return &___attachmentVertices_12; }
	inline void set_attachmentVertices_12(ExposedList_1_t3384296343 * value)
	{
		___attachmentVertices_12 = value;
		Il2CppCodeGenWriteBarrier((&___attachmentVertices_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOT_T1804116343_H
#ifndef ATTACHMENT_T397756874_H
#define ATTACHMENT_T397756874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Attachment
struct  Attachment_t397756874  : public RuntimeObject
{
public:
	// System.String Spine.Attachment::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Attachment_t397756874, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACHMENT_T397756874_H
#ifndef WAITFORSPINETRACKENTRYEND_T2837745715_H
#define WAITFORSPINETRACKENTRYEND_T2837745715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineTrackEntryEnd
struct  WaitForSpineTrackEntryEnd_t2837745715  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.WaitForSpineTrackEntryEnd::m_WasFired
	bool ___m_WasFired_0;

public:
	inline static int32_t get_offset_of_m_WasFired_0() { return static_cast<int32_t>(offsetof(WaitForSpineTrackEntryEnd_t2837745715, ___m_WasFired_0)); }
	inline bool get_m_WasFired_0() const { return ___m_WasFired_0; }
	inline bool* get_address_of_m_WasFired_0() { return &___m_WasFired_0; }
	inline void set_m_WasFired_0(bool value)
	{
		___m_WasFired_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINETRACKENTRYEND_T2837745715_H
#ifndef EVENT_T27715519_H
#define EVENT_T27715519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Event
struct  Event_t27715519  : public RuntimeObject
{
public:
	// Spine.EventData Spine.Event::data
	EventData_t295981108 * ___data_0;
	// System.Single Spine.Event::time
	float ___time_1;
	// System.Int32 Spine.Event::intValue
	int32_t ___intValue_2;
	// System.Single Spine.Event::floatValue
	float ___floatValue_3;
	// System.String Spine.Event::stringValue
	String_t* ___stringValue_4;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Event_t27715519, ___data_0)); }
	inline EventData_t295981108 * get_data_0() const { return ___data_0; }
	inline EventData_t295981108 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(EventData_t295981108 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_time_1() { return static_cast<int32_t>(offsetof(Event_t27715519, ___time_1)); }
	inline float get_time_1() const { return ___time_1; }
	inline float* get_address_of_time_1() { return &___time_1; }
	inline void set_time_1(float value)
	{
		___time_1 = value;
	}

	inline static int32_t get_offset_of_intValue_2() { return static_cast<int32_t>(offsetof(Event_t27715519, ___intValue_2)); }
	inline int32_t get_intValue_2() const { return ___intValue_2; }
	inline int32_t* get_address_of_intValue_2() { return &___intValue_2; }
	inline void set_intValue_2(int32_t value)
	{
		___intValue_2 = value;
	}

	inline static int32_t get_offset_of_floatValue_3() { return static_cast<int32_t>(offsetof(Event_t27715519, ___floatValue_3)); }
	inline float get_floatValue_3() const { return ___floatValue_3; }
	inline float* get_address_of_floatValue_3() { return &___floatValue_3; }
	inline void set_floatValue_3(float value)
	{
		___floatValue_3 = value;
	}

	inline static int32_t get_offset_of_stringValue_4() { return static_cast<int32_t>(offsetof(Event_t27715519, ___stringValue_4)); }
	inline String_t* get_stringValue_4() const { return ___stringValue_4; }
	inline String_t** get_address_of_stringValue_4() { return &___stringValue_4; }
	inline void set_stringValue_4(String_t* value)
	{
		___stringValue_4 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENT_T27715519_H
#ifndef EVENTDATA_T295981108_H
#define EVENTDATA_T295981108_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.EventData
struct  EventData_t295981108  : public RuntimeObject
{
public:
	// System.String Spine.EventData::name
	String_t* ___name_0;
	// System.Int32 Spine.EventData::<Int>k__BackingField
	int32_t ___U3CIntU3Ek__BackingField_1;
	// System.Single Spine.EventData::<Float>k__BackingField
	float ___U3CFloatU3Ek__BackingField_2;
	// System.String Spine.EventData::<String>k__BackingField
	String_t* ___U3CStringU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(EventData_t295981108, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_U3CIntU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EventData_t295981108, ___U3CIntU3Ek__BackingField_1)); }
	inline int32_t get_U3CIntU3Ek__BackingField_1() const { return ___U3CIntU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CIntU3Ek__BackingField_1() { return &___U3CIntU3Ek__BackingField_1; }
	inline void set_U3CIntU3Ek__BackingField_1(int32_t value)
	{
		___U3CIntU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CFloatU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EventData_t295981108, ___U3CFloatU3Ek__BackingField_2)); }
	inline float get_U3CFloatU3Ek__BackingField_2() const { return ___U3CFloatU3Ek__BackingField_2; }
	inline float* get_address_of_U3CFloatU3Ek__BackingField_2() { return &___U3CFloatU3Ek__BackingField_2; }
	inline void set_U3CFloatU3Ek__BackingField_2(float value)
	{
		___U3CFloatU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CStringU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(EventData_t295981108, ___U3CStringU3Ek__BackingField_3)); }
	inline String_t* get_U3CStringU3Ek__BackingField_3() const { return ___U3CStringU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CStringU3Ek__BackingField_3() { return &___U3CStringU3Ek__BackingField_3; }
	inline void set_U3CStringU3Ek__BackingField_3(String_t* value)
	{
		___U3CStringU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CStringU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTDATA_T295981108_H
#ifndef ANIMATIONSTATE_T2858486651_H
#define ANIMATIONSTATE_T2858486651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState
struct  AnimationState_t2858486651  : public RuntimeObject
{
public:
	// Spine.AnimationStateData Spine.AnimationState::data
	AnimationStateData_t1644285503 * ___data_5;
	// Spine.Pool`1<Spine.TrackEntry> Spine.AnimationState::trackEntryPool
	Pool_1_t4075013739 * ___trackEntryPool_6;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.AnimationState::tracks
	ExposedList_1_t1617326325 * ___tracks_7;
	// Spine.ExposedList`1<Spine.Event> Spine.AnimationState::events
	ExposedList_1_t1877711358 * ___events_8;
	// Spine.EventQueue Spine.AnimationState::queue
	EventQueue_t2605283308 * ___queue_9;
	// System.Collections.Generic.HashSet`1<System.Int32> Spine.AnimationState::propertyIDs
	HashSet_1_t2711375974 * ___propertyIDs_10;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.AnimationState::mixingTo
	ExposedList_1_t1617326325 * ___mixingTo_11;
	// System.Boolean Spine.AnimationState::animationsChanged
	bool ___animationsChanged_12;
	// System.Single Spine.AnimationState::timeScale
	float ___timeScale_13;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Interrupt
	TrackEntryDelegate_t1807657294 * ___Interrupt_14;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::End
	TrackEntryDelegate_t1807657294 * ___End_15;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Dispose
	TrackEntryDelegate_t1807657294 * ___Dispose_16;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Complete
	TrackEntryDelegate_t1807657294 * ___Complete_17;
	// Spine.AnimationState/TrackEntryDelegate Spine.AnimationState::Start
	TrackEntryDelegate_t1807657294 * ___Start_18;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.AnimationState::Event
	TrackEntryEventDelegate_t471240152 * ___Event_19;

public:
	inline static int32_t get_offset_of_data_5() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___data_5)); }
	inline AnimationStateData_t1644285503 * get_data_5() const { return ___data_5; }
	inline AnimationStateData_t1644285503 ** get_address_of_data_5() { return &___data_5; }
	inline void set_data_5(AnimationStateData_t1644285503 * value)
	{
		___data_5 = value;
		Il2CppCodeGenWriteBarrier((&___data_5), value);
	}

	inline static int32_t get_offset_of_trackEntryPool_6() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___trackEntryPool_6)); }
	inline Pool_1_t4075013739 * get_trackEntryPool_6() const { return ___trackEntryPool_6; }
	inline Pool_1_t4075013739 ** get_address_of_trackEntryPool_6() { return &___trackEntryPool_6; }
	inline void set_trackEntryPool_6(Pool_1_t4075013739 * value)
	{
		___trackEntryPool_6 = value;
		Il2CppCodeGenWriteBarrier((&___trackEntryPool_6), value);
	}

	inline static int32_t get_offset_of_tracks_7() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___tracks_7)); }
	inline ExposedList_1_t1617326325 * get_tracks_7() const { return ___tracks_7; }
	inline ExposedList_1_t1617326325 ** get_address_of_tracks_7() { return &___tracks_7; }
	inline void set_tracks_7(ExposedList_1_t1617326325 * value)
	{
		___tracks_7 = value;
		Il2CppCodeGenWriteBarrier((&___tracks_7), value);
	}

	inline static int32_t get_offset_of_events_8() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___events_8)); }
	inline ExposedList_1_t1877711358 * get_events_8() const { return ___events_8; }
	inline ExposedList_1_t1877711358 ** get_address_of_events_8() { return &___events_8; }
	inline void set_events_8(ExposedList_1_t1877711358 * value)
	{
		___events_8 = value;
		Il2CppCodeGenWriteBarrier((&___events_8), value);
	}

	inline static int32_t get_offset_of_queue_9() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___queue_9)); }
	inline EventQueue_t2605283308 * get_queue_9() const { return ___queue_9; }
	inline EventQueue_t2605283308 ** get_address_of_queue_9() { return &___queue_9; }
	inline void set_queue_9(EventQueue_t2605283308 * value)
	{
		___queue_9 = value;
		Il2CppCodeGenWriteBarrier((&___queue_9), value);
	}

	inline static int32_t get_offset_of_propertyIDs_10() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___propertyIDs_10)); }
	inline HashSet_1_t2711375974 * get_propertyIDs_10() const { return ___propertyIDs_10; }
	inline HashSet_1_t2711375974 ** get_address_of_propertyIDs_10() { return &___propertyIDs_10; }
	inline void set_propertyIDs_10(HashSet_1_t2711375974 * value)
	{
		___propertyIDs_10 = value;
		Il2CppCodeGenWriteBarrier((&___propertyIDs_10), value);
	}

	inline static int32_t get_offset_of_mixingTo_11() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___mixingTo_11)); }
	inline ExposedList_1_t1617326325 * get_mixingTo_11() const { return ___mixingTo_11; }
	inline ExposedList_1_t1617326325 ** get_address_of_mixingTo_11() { return &___mixingTo_11; }
	inline void set_mixingTo_11(ExposedList_1_t1617326325 * value)
	{
		___mixingTo_11 = value;
		Il2CppCodeGenWriteBarrier((&___mixingTo_11), value);
	}

	inline static int32_t get_offset_of_animationsChanged_12() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___animationsChanged_12)); }
	inline bool get_animationsChanged_12() const { return ___animationsChanged_12; }
	inline bool* get_address_of_animationsChanged_12() { return &___animationsChanged_12; }
	inline void set_animationsChanged_12(bool value)
	{
		___animationsChanged_12 = value;
	}

	inline static int32_t get_offset_of_timeScale_13() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___timeScale_13)); }
	inline float get_timeScale_13() const { return ___timeScale_13; }
	inline float* get_address_of_timeScale_13() { return &___timeScale_13; }
	inline void set_timeScale_13(float value)
	{
		___timeScale_13 = value;
	}

	inline static int32_t get_offset_of_Interrupt_14() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___Interrupt_14)); }
	inline TrackEntryDelegate_t1807657294 * get_Interrupt_14() const { return ___Interrupt_14; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Interrupt_14() { return &___Interrupt_14; }
	inline void set_Interrupt_14(TrackEntryDelegate_t1807657294 * value)
	{
		___Interrupt_14 = value;
		Il2CppCodeGenWriteBarrier((&___Interrupt_14), value);
	}

	inline static int32_t get_offset_of_End_15() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___End_15)); }
	inline TrackEntryDelegate_t1807657294 * get_End_15() const { return ___End_15; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_End_15() { return &___End_15; }
	inline void set_End_15(TrackEntryDelegate_t1807657294 * value)
	{
		___End_15 = value;
		Il2CppCodeGenWriteBarrier((&___End_15), value);
	}

	inline static int32_t get_offset_of_Dispose_16() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___Dispose_16)); }
	inline TrackEntryDelegate_t1807657294 * get_Dispose_16() const { return ___Dispose_16; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Dispose_16() { return &___Dispose_16; }
	inline void set_Dispose_16(TrackEntryDelegate_t1807657294 * value)
	{
		___Dispose_16 = value;
		Il2CppCodeGenWriteBarrier((&___Dispose_16), value);
	}

	inline static int32_t get_offset_of_Complete_17() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___Complete_17)); }
	inline TrackEntryDelegate_t1807657294 * get_Complete_17() const { return ___Complete_17; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Complete_17() { return &___Complete_17; }
	inline void set_Complete_17(TrackEntryDelegate_t1807657294 * value)
	{
		___Complete_17 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_17), value);
	}

	inline static int32_t get_offset_of_Start_18() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___Start_18)); }
	inline TrackEntryDelegate_t1807657294 * get_Start_18() const { return ___Start_18; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Start_18() { return &___Start_18; }
	inline void set_Start_18(TrackEntryDelegate_t1807657294 * value)
	{
		___Start_18 = value;
		Il2CppCodeGenWriteBarrier((&___Start_18), value);
	}

	inline static int32_t get_offset_of_Event_19() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651, ___Event_19)); }
	inline TrackEntryEventDelegate_t471240152 * get_Event_19() const { return ___Event_19; }
	inline TrackEntryEventDelegate_t471240152 ** get_address_of_Event_19() { return &___Event_19; }
	inline void set_Event_19(TrackEntryEventDelegate_t471240152 * value)
	{
		___Event_19 = value;
		Il2CppCodeGenWriteBarrier((&___Event_19), value);
	}
};

struct AnimationState_t2858486651_StaticFields
{
public:
	// Spine.Animation Spine.AnimationState::EmptyAnimation
	Animation_t2067143511 * ___EmptyAnimation_0;

public:
	inline static int32_t get_offset_of_EmptyAnimation_0() { return static_cast<int32_t>(offsetof(AnimationState_t2858486651_StaticFields, ___EmptyAnimation_0)); }
	inline Animation_t2067143511 * get_EmptyAnimation_0() const { return ___EmptyAnimation_0; }
	inline Animation_t2067143511 ** get_address_of_EmptyAnimation_0() { return &___EmptyAnimation_0; }
	inline void set_EmptyAnimation_0(Animation_t2067143511 * value)
	{
		___EmptyAnimation_0 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyAnimation_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATIONSTATE_T2858486651_H
#ifndef WAITFORSPINEEVENT_T3387552986_H
#define WAITFORSPINEEVENT_T3387552986_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineEvent
struct  WaitForSpineEvent_t3387552986  : public RuntimeObject
{
public:
	// Spine.EventData Spine.Unity.WaitForSpineEvent::m_TargetEvent
	EventData_t295981108 * ___m_TargetEvent_0;
	// System.String Spine.Unity.WaitForSpineEvent::m_EventName
	String_t* ___m_EventName_1;
	// Spine.AnimationState Spine.Unity.WaitForSpineEvent::m_AnimationState
	AnimationState_t2858486651 * ___m_AnimationState_2;
	// System.Boolean Spine.Unity.WaitForSpineEvent::m_WasFired
	bool ___m_WasFired_3;
	// System.Boolean Spine.Unity.WaitForSpineEvent::m_unsubscribeAfterFiring
	bool ___m_unsubscribeAfterFiring_4;

public:
	inline static int32_t get_offset_of_m_TargetEvent_0() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3387552986, ___m_TargetEvent_0)); }
	inline EventData_t295981108 * get_m_TargetEvent_0() const { return ___m_TargetEvent_0; }
	inline EventData_t295981108 ** get_address_of_m_TargetEvent_0() { return &___m_TargetEvent_0; }
	inline void set_m_TargetEvent_0(EventData_t295981108 * value)
	{
		___m_TargetEvent_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetEvent_0), value);
	}

	inline static int32_t get_offset_of_m_EventName_1() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3387552986, ___m_EventName_1)); }
	inline String_t* get_m_EventName_1() const { return ___m_EventName_1; }
	inline String_t** get_address_of_m_EventName_1() { return &___m_EventName_1; }
	inline void set_m_EventName_1(String_t* value)
	{
		___m_EventName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_EventName_1), value);
	}

	inline static int32_t get_offset_of_m_AnimationState_2() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3387552986, ___m_AnimationState_2)); }
	inline AnimationState_t2858486651 * get_m_AnimationState_2() const { return ___m_AnimationState_2; }
	inline AnimationState_t2858486651 ** get_address_of_m_AnimationState_2() { return &___m_AnimationState_2; }
	inline void set_m_AnimationState_2(AnimationState_t2858486651 * value)
	{
		___m_AnimationState_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_AnimationState_2), value);
	}

	inline static int32_t get_offset_of_m_WasFired_3() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3387552986, ___m_WasFired_3)); }
	inline bool get_m_WasFired_3() const { return ___m_WasFired_3; }
	inline bool* get_address_of_m_WasFired_3() { return &___m_WasFired_3; }
	inline void set_m_WasFired_3(bool value)
	{
		___m_WasFired_3 = value;
	}

	inline static int32_t get_offset_of_m_unsubscribeAfterFiring_4() { return static_cast<int32_t>(offsetof(WaitForSpineEvent_t3387552986, ___m_unsubscribeAfterFiring_4)); }
	inline bool get_m_unsubscribeAfterFiring_4() const { return ___m_unsubscribeAfterFiring_4; }
	inline bool* get_address_of_m_unsubscribeAfterFiring_4() { return &___m_unsubscribeAfterFiring_4; }
	inline void set_m_unsubscribeAfterFiring_4(bool value)
	{
		___m_unsubscribeAfterFiring_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINEEVENT_T3387552986_H
#ifndef WAITFORSPINEANIMATIONCOMPLETE_T694865537_H
#define WAITFORSPINEANIMATIONCOMPLETE_T694865537_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.WaitForSpineAnimationComplete
struct  WaitForSpineAnimationComplete_t694865537  : public RuntimeObject
{
public:
	// System.Boolean Spine.Unity.WaitForSpineAnimationComplete::m_WasFired
	bool ___m_WasFired_0;

public:
	inline static int32_t get_offset_of_m_WasFired_0() { return static_cast<int32_t>(offsetof(WaitForSpineAnimationComplete_t694865537, ___m_WasFired_0)); }
	inline bool get_m_WasFired_0() const { return ___m_WasFired_0; }
	inline bool* get_address_of_m_WasFired_0() { return &___m_WasFired_0; }
	inline void set_m_WasFired_0(bool value)
	{
		___m_WasFired_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORSPINEANIMATIONCOMPLETE_T694865537_H
#ifndef SKELETON_T1045203634_H
#define SKELETON_T1045203634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Skeleton
struct  Skeleton_t1045203634  : public RuntimeObject
{
public:
	// Spine.SkeletonData Spine.Skeleton::data
	SkeletonData_t2665604974 * ___data_0;
	// Spine.ExposedList`1<Spine.Bone> Spine.Skeleton::bones
	ExposedList_1_t3613858675 * ___bones_1;
	// Spine.ExposedList`1<Spine.Slot> Spine.Skeleton::slots
	ExposedList_1_t3654112182 * ___slots_2;
	// Spine.ExposedList`1<Spine.Slot> Spine.Skeleton::drawOrder
	ExposedList_1_t3654112182 * ___drawOrder_3;
	// Spine.ExposedList`1<Spine.IkConstraint> Spine.Skeleton::ikConstraints
	ExposedList_1_t69720520 * ___ikConstraints_4;
	// Spine.ExposedList`1<Spine.TransformConstraint> Spine.Skeleton::transformConstraints
	ExposedList_1_t2359534843 * ___transformConstraints_5;
	// Spine.ExposedList`1<Spine.PathConstraint> Spine.Skeleton::pathConstraints
	ExposedList_1_t3704659244 * ___pathConstraints_6;
	// Spine.ExposedList`1<Spine.IUpdatable> Spine.Skeleton::updateCache
	ExposedList_1_t1975705998 * ___updateCache_7;
	// Spine.ExposedList`1<Spine.Bone> Spine.Skeleton::updateCacheReset
	ExposedList_1_t3613858675 * ___updateCacheReset_8;
	// Spine.Skin Spine.Skeleton::skin
	Skin_t1695237131 * ___skin_9;
	// System.Single Spine.Skeleton::r
	float ___r_10;
	// System.Single Spine.Skeleton::g
	float ___g_11;
	// System.Single Spine.Skeleton::b
	float ___b_12;
	// System.Single Spine.Skeleton::a
	float ___a_13;
	// System.Single Spine.Skeleton::time
	float ___time_14;
	// System.Boolean Spine.Skeleton::flipX
	bool ___flipX_15;
	// System.Boolean Spine.Skeleton::flipY
	bool ___flipY_16;
	// System.Single Spine.Skeleton::x
	float ___x_17;
	// System.Single Spine.Skeleton::y
	float ___y_18;

public:
	inline static int32_t get_offset_of_data_0() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___data_0)); }
	inline SkeletonData_t2665604974 * get_data_0() const { return ___data_0; }
	inline SkeletonData_t2665604974 ** get_address_of_data_0() { return &___data_0; }
	inline void set_data_0(SkeletonData_t2665604974 * value)
	{
		___data_0 = value;
		Il2CppCodeGenWriteBarrier((&___data_0), value);
	}

	inline static int32_t get_offset_of_bones_1() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___bones_1)); }
	inline ExposedList_1_t3613858675 * get_bones_1() const { return ___bones_1; }
	inline ExposedList_1_t3613858675 ** get_address_of_bones_1() { return &___bones_1; }
	inline void set_bones_1(ExposedList_1_t3613858675 * value)
	{
		___bones_1 = value;
		Il2CppCodeGenWriteBarrier((&___bones_1), value);
	}

	inline static int32_t get_offset_of_slots_2() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___slots_2)); }
	inline ExposedList_1_t3654112182 * get_slots_2() const { return ___slots_2; }
	inline ExposedList_1_t3654112182 ** get_address_of_slots_2() { return &___slots_2; }
	inline void set_slots_2(ExposedList_1_t3654112182 * value)
	{
		___slots_2 = value;
		Il2CppCodeGenWriteBarrier((&___slots_2), value);
	}

	inline static int32_t get_offset_of_drawOrder_3() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___drawOrder_3)); }
	inline ExposedList_1_t3654112182 * get_drawOrder_3() const { return ___drawOrder_3; }
	inline ExposedList_1_t3654112182 ** get_address_of_drawOrder_3() { return &___drawOrder_3; }
	inline void set_drawOrder_3(ExposedList_1_t3654112182 * value)
	{
		___drawOrder_3 = value;
		Il2CppCodeGenWriteBarrier((&___drawOrder_3), value);
	}

	inline static int32_t get_offset_of_ikConstraints_4() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___ikConstraints_4)); }
	inline ExposedList_1_t69720520 * get_ikConstraints_4() const { return ___ikConstraints_4; }
	inline ExposedList_1_t69720520 ** get_address_of_ikConstraints_4() { return &___ikConstraints_4; }
	inline void set_ikConstraints_4(ExposedList_1_t69720520 * value)
	{
		___ikConstraints_4 = value;
		Il2CppCodeGenWriteBarrier((&___ikConstraints_4), value);
	}

	inline static int32_t get_offset_of_transformConstraints_5() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___transformConstraints_5)); }
	inline ExposedList_1_t2359534843 * get_transformConstraints_5() const { return ___transformConstraints_5; }
	inline ExposedList_1_t2359534843 ** get_address_of_transformConstraints_5() { return &___transformConstraints_5; }
	inline void set_transformConstraints_5(ExposedList_1_t2359534843 * value)
	{
		___transformConstraints_5 = value;
		Il2CppCodeGenWriteBarrier((&___transformConstraints_5), value);
	}

	inline static int32_t get_offset_of_pathConstraints_6() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___pathConstraints_6)); }
	inline ExposedList_1_t3704659244 * get_pathConstraints_6() const { return ___pathConstraints_6; }
	inline ExposedList_1_t3704659244 ** get_address_of_pathConstraints_6() { return &___pathConstraints_6; }
	inline void set_pathConstraints_6(ExposedList_1_t3704659244 * value)
	{
		___pathConstraints_6 = value;
		Il2CppCodeGenWriteBarrier((&___pathConstraints_6), value);
	}

	inline static int32_t get_offset_of_updateCache_7() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___updateCache_7)); }
	inline ExposedList_1_t1975705998 * get_updateCache_7() const { return ___updateCache_7; }
	inline ExposedList_1_t1975705998 ** get_address_of_updateCache_7() { return &___updateCache_7; }
	inline void set_updateCache_7(ExposedList_1_t1975705998 * value)
	{
		___updateCache_7 = value;
		Il2CppCodeGenWriteBarrier((&___updateCache_7), value);
	}

	inline static int32_t get_offset_of_updateCacheReset_8() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___updateCacheReset_8)); }
	inline ExposedList_1_t3613858675 * get_updateCacheReset_8() const { return ___updateCacheReset_8; }
	inline ExposedList_1_t3613858675 ** get_address_of_updateCacheReset_8() { return &___updateCacheReset_8; }
	inline void set_updateCacheReset_8(ExposedList_1_t3613858675 * value)
	{
		___updateCacheReset_8 = value;
		Il2CppCodeGenWriteBarrier((&___updateCacheReset_8), value);
	}

	inline static int32_t get_offset_of_skin_9() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___skin_9)); }
	inline Skin_t1695237131 * get_skin_9() const { return ___skin_9; }
	inline Skin_t1695237131 ** get_address_of_skin_9() { return &___skin_9; }
	inline void set_skin_9(Skin_t1695237131 * value)
	{
		___skin_9 = value;
		Il2CppCodeGenWriteBarrier((&___skin_9), value);
	}

	inline static int32_t get_offset_of_r_10() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___r_10)); }
	inline float get_r_10() const { return ___r_10; }
	inline float* get_address_of_r_10() { return &___r_10; }
	inline void set_r_10(float value)
	{
		___r_10 = value;
	}

	inline static int32_t get_offset_of_g_11() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___g_11)); }
	inline float get_g_11() const { return ___g_11; }
	inline float* get_address_of_g_11() { return &___g_11; }
	inline void set_g_11(float value)
	{
		___g_11 = value;
	}

	inline static int32_t get_offset_of_b_12() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___b_12)); }
	inline float get_b_12() const { return ___b_12; }
	inline float* get_address_of_b_12() { return &___b_12; }
	inline void set_b_12(float value)
	{
		___b_12 = value;
	}

	inline static int32_t get_offset_of_a_13() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___a_13)); }
	inline float get_a_13() const { return ___a_13; }
	inline float* get_address_of_a_13() { return &___a_13; }
	inline void set_a_13(float value)
	{
		___a_13 = value;
	}

	inline static int32_t get_offset_of_time_14() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___time_14)); }
	inline float get_time_14() const { return ___time_14; }
	inline float* get_address_of_time_14() { return &___time_14; }
	inline void set_time_14(float value)
	{
		___time_14 = value;
	}

	inline static int32_t get_offset_of_flipX_15() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___flipX_15)); }
	inline bool get_flipX_15() const { return ___flipX_15; }
	inline bool* get_address_of_flipX_15() { return &___flipX_15; }
	inline void set_flipX_15(bool value)
	{
		___flipX_15 = value;
	}

	inline static int32_t get_offset_of_flipY_16() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___flipY_16)); }
	inline bool get_flipY_16() const { return ___flipY_16; }
	inline bool* get_address_of_flipY_16() { return &___flipY_16; }
	inline void set_flipY_16(bool value)
	{
		___flipY_16 = value;
	}

	inline static int32_t get_offset_of_x_17() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___x_17)); }
	inline float get_x_17() const { return ___x_17; }
	inline float* get_address_of_x_17() { return &___x_17; }
	inline void set_x_17(float value)
	{
		___x_17 = value;
	}

	inline static int32_t get_offset_of_y_18() { return static_cast<int32_t>(offsetof(Skeleton_t1045203634, ___y_18)); }
	inline float get_y_18() const { return ___y_18; }
	inline float* get_address_of_y_18() { return &___y_18; }
	inline void set_y_18(float value)
	{
		___y_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETON_T1045203634_H
#ifndef TRACKENTRY_T4062297782_H
#define TRACKENTRY_T4062297782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.TrackEntry
struct  TrackEntry_t4062297782  : public RuntimeObject
{
public:
	// Spine.Animation Spine.TrackEntry::animation
	Animation_t2067143511 * ___animation_0;
	// Spine.TrackEntry Spine.TrackEntry::next
	TrackEntry_t4062297782 * ___next_1;
	// Spine.TrackEntry Spine.TrackEntry::mixingFrom
	TrackEntry_t4062297782 * ___mixingFrom_2;
	// System.Int32 Spine.TrackEntry::trackIndex
	int32_t ___trackIndex_3;
	// System.Boolean Spine.TrackEntry::loop
	bool ___loop_4;
	// System.Single Spine.TrackEntry::eventThreshold
	float ___eventThreshold_5;
	// System.Single Spine.TrackEntry::attachmentThreshold
	float ___attachmentThreshold_6;
	// System.Single Spine.TrackEntry::drawOrderThreshold
	float ___drawOrderThreshold_7;
	// System.Single Spine.TrackEntry::animationStart
	float ___animationStart_8;
	// System.Single Spine.TrackEntry::animationEnd
	float ___animationEnd_9;
	// System.Single Spine.TrackEntry::animationLast
	float ___animationLast_10;
	// System.Single Spine.TrackEntry::nextAnimationLast
	float ___nextAnimationLast_11;
	// System.Single Spine.TrackEntry::delay
	float ___delay_12;
	// System.Single Spine.TrackEntry::trackTime
	float ___trackTime_13;
	// System.Single Spine.TrackEntry::trackLast
	float ___trackLast_14;
	// System.Single Spine.TrackEntry::nextTrackLast
	float ___nextTrackLast_15;
	// System.Single Spine.TrackEntry::trackEnd
	float ___trackEnd_16;
	// System.Single Spine.TrackEntry::timeScale
	float ___timeScale_17;
	// System.Single Spine.TrackEntry::alpha
	float ___alpha_18;
	// System.Single Spine.TrackEntry::mixTime
	float ___mixTime_19;
	// System.Single Spine.TrackEntry::mixDuration
	float ___mixDuration_20;
	// System.Single Spine.TrackEntry::interruptAlpha
	float ___interruptAlpha_21;
	// System.Single Spine.TrackEntry::totalAlpha
	float ___totalAlpha_22;
	// Spine.ExposedList`1<System.Int32> Spine.TrackEntry::timelineData
	ExposedList_1_t1671071859 * ___timelineData_23;
	// Spine.ExposedList`1<Spine.TrackEntry> Spine.TrackEntry::timelineDipMix
	ExposedList_1_t1617326325 * ___timelineDipMix_24;
	// Spine.ExposedList`1<System.Single> Spine.TrackEntry::timelinesRotation
	ExposedList_1_t3384296343 * ___timelinesRotation_25;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Interrupt
	TrackEntryDelegate_t1807657294 * ___Interrupt_26;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::End
	TrackEntryDelegate_t1807657294 * ___End_27;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Dispose
	TrackEntryDelegate_t1807657294 * ___Dispose_28;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Complete
	TrackEntryDelegate_t1807657294 * ___Complete_29;
	// Spine.AnimationState/TrackEntryDelegate Spine.TrackEntry::Start
	TrackEntryDelegate_t1807657294 * ___Start_30;
	// Spine.AnimationState/TrackEntryEventDelegate Spine.TrackEntry::Event
	TrackEntryEventDelegate_t471240152 * ___Event_31;

public:
	inline static int32_t get_offset_of_animation_0() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___animation_0)); }
	inline Animation_t2067143511 * get_animation_0() const { return ___animation_0; }
	inline Animation_t2067143511 ** get_address_of_animation_0() { return &___animation_0; }
	inline void set_animation_0(Animation_t2067143511 * value)
	{
		___animation_0 = value;
		Il2CppCodeGenWriteBarrier((&___animation_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___next_1)); }
	inline TrackEntry_t4062297782 * get_next_1() const { return ___next_1; }
	inline TrackEntry_t4062297782 ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(TrackEntry_t4062297782 * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((&___next_1), value);
	}

	inline static int32_t get_offset_of_mixingFrom_2() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___mixingFrom_2)); }
	inline TrackEntry_t4062297782 * get_mixingFrom_2() const { return ___mixingFrom_2; }
	inline TrackEntry_t4062297782 ** get_address_of_mixingFrom_2() { return &___mixingFrom_2; }
	inline void set_mixingFrom_2(TrackEntry_t4062297782 * value)
	{
		___mixingFrom_2 = value;
		Il2CppCodeGenWriteBarrier((&___mixingFrom_2), value);
	}

	inline static int32_t get_offset_of_trackIndex_3() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___trackIndex_3)); }
	inline int32_t get_trackIndex_3() const { return ___trackIndex_3; }
	inline int32_t* get_address_of_trackIndex_3() { return &___trackIndex_3; }
	inline void set_trackIndex_3(int32_t value)
	{
		___trackIndex_3 = value;
	}

	inline static int32_t get_offset_of_loop_4() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___loop_4)); }
	inline bool get_loop_4() const { return ___loop_4; }
	inline bool* get_address_of_loop_4() { return &___loop_4; }
	inline void set_loop_4(bool value)
	{
		___loop_4 = value;
	}

	inline static int32_t get_offset_of_eventThreshold_5() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___eventThreshold_5)); }
	inline float get_eventThreshold_5() const { return ___eventThreshold_5; }
	inline float* get_address_of_eventThreshold_5() { return &___eventThreshold_5; }
	inline void set_eventThreshold_5(float value)
	{
		___eventThreshold_5 = value;
	}

	inline static int32_t get_offset_of_attachmentThreshold_6() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___attachmentThreshold_6)); }
	inline float get_attachmentThreshold_6() const { return ___attachmentThreshold_6; }
	inline float* get_address_of_attachmentThreshold_6() { return &___attachmentThreshold_6; }
	inline void set_attachmentThreshold_6(float value)
	{
		___attachmentThreshold_6 = value;
	}

	inline static int32_t get_offset_of_drawOrderThreshold_7() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___drawOrderThreshold_7)); }
	inline float get_drawOrderThreshold_7() const { return ___drawOrderThreshold_7; }
	inline float* get_address_of_drawOrderThreshold_7() { return &___drawOrderThreshold_7; }
	inline void set_drawOrderThreshold_7(float value)
	{
		___drawOrderThreshold_7 = value;
	}

	inline static int32_t get_offset_of_animationStart_8() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___animationStart_8)); }
	inline float get_animationStart_8() const { return ___animationStart_8; }
	inline float* get_address_of_animationStart_8() { return &___animationStart_8; }
	inline void set_animationStart_8(float value)
	{
		___animationStart_8 = value;
	}

	inline static int32_t get_offset_of_animationEnd_9() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___animationEnd_9)); }
	inline float get_animationEnd_9() const { return ___animationEnd_9; }
	inline float* get_address_of_animationEnd_9() { return &___animationEnd_9; }
	inline void set_animationEnd_9(float value)
	{
		___animationEnd_9 = value;
	}

	inline static int32_t get_offset_of_animationLast_10() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___animationLast_10)); }
	inline float get_animationLast_10() const { return ___animationLast_10; }
	inline float* get_address_of_animationLast_10() { return &___animationLast_10; }
	inline void set_animationLast_10(float value)
	{
		___animationLast_10 = value;
	}

	inline static int32_t get_offset_of_nextAnimationLast_11() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___nextAnimationLast_11)); }
	inline float get_nextAnimationLast_11() const { return ___nextAnimationLast_11; }
	inline float* get_address_of_nextAnimationLast_11() { return &___nextAnimationLast_11; }
	inline void set_nextAnimationLast_11(float value)
	{
		___nextAnimationLast_11 = value;
	}

	inline static int32_t get_offset_of_delay_12() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___delay_12)); }
	inline float get_delay_12() const { return ___delay_12; }
	inline float* get_address_of_delay_12() { return &___delay_12; }
	inline void set_delay_12(float value)
	{
		___delay_12 = value;
	}

	inline static int32_t get_offset_of_trackTime_13() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___trackTime_13)); }
	inline float get_trackTime_13() const { return ___trackTime_13; }
	inline float* get_address_of_trackTime_13() { return &___trackTime_13; }
	inline void set_trackTime_13(float value)
	{
		___trackTime_13 = value;
	}

	inline static int32_t get_offset_of_trackLast_14() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___trackLast_14)); }
	inline float get_trackLast_14() const { return ___trackLast_14; }
	inline float* get_address_of_trackLast_14() { return &___trackLast_14; }
	inline void set_trackLast_14(float value)
	{
		___trackLast_14 = value;
	}

	inline static int32_t get_offset_of_nextTrackLast_15() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___nextTrackLast_15)); }
	inline float get_nextTrackLast_15() const { return ___nextTrackLast_15; }
	inline float* get_address_of_nextTrackLast_15() { return &___nextTrackLast_15; }
	inline void set_nextTrackLast_15(float value)
	{
		___nextTrackLast_15 = value;
	}

	inline static int32_t get_offset_of_trackEnd_16() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___trackEnd_16)); }
	inline float get_trackEnd_16() const { return ___trackEnd_16; }
	inline float* get_address_of_trackEnd_16() { return &___trackEnd_16; }
	inline void set_trackEnd_16(float value)
	{
		___trackEnd_16 = value;
	}

	inline static int32_t get_offset_of_timeScale_17() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___timeScale_17)); }
	inline float get_timeScale_17() const { return ___timeScale_17; }
	inline float* get_address_of_timeScale_17() { return &___timeScale_17; }
	inline void set_timeScale_17(float value)
	{
		___timeScale_17 = value;
	}

	inline static int32_t get_offset_of_alpha_18() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___alpha_18)); }
	inline float get_alpha_18() const { return ___alpha_18; }
	inline float* get_address_of_alpha_18() { return &___alpha_18; }
	inline void set_alpha_18(float value)
	{
		___alpha_18 = value;
	}

	inline static int32_t get_offset_of_mixTime_19() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___mixTime_19)); }
	inline float get_mixTime_19() const { return ___mixTime_19; }
	inline float* get_address_of_mixTime_19() { return &___mixTime_19; }
	inline void set_mixTime_19(float value)
	{
		___mixTime_19 = value;
	}

	inline static int32_t get_offset_of_mixDuration_20() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___mixDuration_20)); }
	inline float get_mixDuration_20() const { return ___mixDuration_20; }
	inline float* get_address_of_mixDuration_20() { return &___mixDuration_20; }
	inline void set_mixDuration_20(float value)
	{
		___mixDuration_20 = value;
	}

	inline static int32_t get_offset_of_interruptAlpha_21() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___interruptAlpha_21)); }
	inline float get_interruptAlpha_21() const { return ___interruptAlpha_21; }
	inline float* get_address_of_interruptAlpha_21() { return &___interruptAlpha_21; }
	inline void set_interruptAlpha_21(float value)
	{
		___interruptAlpha_21 = value;
	}

	inline static int32_t get_offset_of_totalAlpha_22() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___totalAlpha_22)); }
	inline float get_totalAlpha_22() const { return ___totalAlpha_22; }
	inline float* get_address_of_totalAlpha_22() { return &___totalAlpha_22; }
	inline void set_totalAlpha_22(float value)
	{
		___totalAlpha_22 = value;
	}

	inline static int32_t get_offset_of_timelineData_23() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___timelineData_23)); }
	inline ExposedList_1_t1671071859 * get_timelineData_23() const { return ___timelineData_23; }
	inline ExposedList_1_t1671071859 ** get_address_of_timelineData_23() { return &___timelineData_23; }
	inline void set_timelineData_23(ExposedList_1_t1671071859 * value)
	{
		___timelineData_23 = value;
		Il2CppCodeGenWriteBarrier((&___timelineData_23), value);
	}

	inline static int32_t get_offset_of_timelineDipMix_24() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___timelineDipMix_24)); }
	inline ExposedList_1_t1617326325 * get_timelineDipMix_24() const { return ___timelineDipMix_24; }
	inline ExposedList_1_t1617326325 ** get_address_of_timelineDipMix_24() { return &___timelineDipMix_24; }
	inline void set_timelineDipMix_24(ExposedList_1_t1617326325 * value)
	{
		___timelineDipMix_24 = value;
		Il2CppCodeGenWriteBarrier((&___timelineDipMix_24), value);
	}

	inline static int32_t get_offset_of_timelinesRotation_25() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___timelinesRotation_25)); }
	inline ExposedList_1_t3384296343 * get_timelinesRotation_25() const { return ___timelinesRotation_25; }
	inline ExposedList_1_t3384296343 ** get_address_of_timelinesRotation_25() { return &___timelinesRotation_25; }
	inline void set_timelinesRotation_25(ExposedList_1_t3384296343 * value)
	{
		___timelinesRotation_25 = value;
		Il2CppCodeGenWriteBarrier((&___timelinesRotation_25), value);
	}

	inline static int32_t get_offset_of_Interrupt_26() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___Interrupt_26)); }
	inline TrackEntryDelegate_t1807657294 * get_Interrupt_26() const { return ___Interrupt_26; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Interrupt_26() { return &___Interrupt_26; }
	inline void set_Interrupt_26(TrackEntryDelegate_t1807657294 * value)
	{
		___Interrupt_26 = value;
		Il2CppCodeGenWriteBarrier((&___Interrupt_26), value);
	}

	inline static int32_t get_offset_of_End_27() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___End_27)); }
	inline TrackEntryDelegate_t1807657294 * get_End_27() const { return ___End_27; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_End_27() { return &___End_27; }
	inline void set_End_27(TrackEntryDelegate_t1807657294 * value)
	{
		___End_27 = value;
		Il2CppCodeGenWriteBarrier((&___End_27), value);
	}

	inline static int32_t get_offset_of_Dispose_28() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___Dispose_28)); }
	inline TrackEntryDelegate_t1807657294 * get_Dispose_28() const { return ___Dispose_28; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Dispose_28() { return &___Dispose_28; }
	inline void set_Dispose_28(TrackEntryDelegate_t1807657294 * value)
	{
		___Dispose_28 = value;
		Il2CppCodeGenWriteBarrier((&___Dispose_28), value);
	}

	inline static int32_t get_offset_of_Complete_29() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___Complete_29)); }
	inline TrackEntryDelegate_t1807657294 * get_Complete_29() const { return ___Complete_29; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Complete_29() { return &___Complete_29; }
	inline void set_Complete_29(TrackEntryDelegate_t1807657294 * value)
	{
		___Complete_29 = value;
		Il2CppCodeGenWriteBarrier((&___Complete_29), value);
	}

	inline static int32_t get_offset_of_Start_30() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___Start_30)); }
	inline TrackEntryDelegate_t1807657294 * get_Start_30() const { return ___Start_30; }
	inline TrackEntryDelegate_t1807657294 ** get_address_of_Start_30() { return &___Start_30; }
	inline void set_Start_30(TrackEntryDelegate_t1807657294 * value)
	{
		___Start_30 = value;
		Il2CppCodeGenWriteBarrier((&___Start_30), value);
	}

	inline static int32_t get_offset_of_Event_31() { return static_cast<int32_t>(offsetof(TrackEntry_t4062297782, ___Event_31)); }
	inline TrackEntryEventDelegate_t471240152 * get_Event_31() const { return ___Event_31; }
	inline TrackEntryEventDelegate_t471240152 ** get_address_of_Event_31() { return &___Event_31; }
	inline void set_Event_31(TrackEntryEventDelegate_t471240152 * value)
	{
		___Event_31 = value;
		Il2CppCodeGenWriteBarrier((&___Event_31), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRY_T4062297782_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::length
	int32_t ___length_0;
	// System.Char System.String::start_char
	Il2CppChar ___start_char_1;

public:
	inline static int32_t get_offset_of_length_0() { return static_cast<int32_t>(offsetof(String_t, ___length_0)); }
	inline int32_t get_length_0() const { return ___length_0; }
	inline int32_t* get_address_of_length_0() { return &___length_0; }
	inline void set_length_0(int32_t value)
	{
		___length_0 = value;
	}

	inline static int32_t get_offset_of_start_char_1() { return static_cast<int32_t>(offsetof(String_t, ___start_char_1)); }
	inline Il2CppChar get_start_char_1() const { return ___start_char_1; }
	inline Il2CppChar* get_address_of_start_char_1() { return &___start_char_1; }
	inline void set_start_char_1(Il2CppChar value)
	{
		___start_char_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_2;
	// System.Char[] System.String::WhiteChars
	CharU5BU5D_t3669675803* ___WhiteChars_3;

public:
	inline static int32_t get_offset_of_Empty_2() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_2)); }
	inline String_t* get_Empty_2() const { return ___Empty_2; }
	inline String_t** get_address_of_Empty_2() { return &___Empty_2; }
	inline void set_Empty_2(String_t* value)
	{
		___Empty_2 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_2), value);
	}

	inline static int32_t get_offset_of_WhiteChars_3() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___WhiteChars_3)); }
	inline CharU5BU5D_t3669675803* get_WhiteChars_3() const { return ___WhiteChars_3; }
	inline CharU5BU5D_t3669675803** get_address_of_WhiteChars_3() { return &___WhiteChars_3; }
	inline void set_WhiteChars_3(CharU5BU5D_t3669675803* value)
	{
		___WhiteChars_3 = value;
		Il2CppCodeGenWriteBarrier((&___WhiteChars_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef BOOLEAN_T1039239260_H
#define BOOLEAN_T1039239260_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t1039239260 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t1039239260, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t1039239260_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t1039239260_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t1039239260_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T1039239260_H
#ifndef VOID_T3157035008_H
#define VOID_T3157035008_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t3157035008 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T3157035008_H
#ifndef SINGLE_T1534300504_H
#define SINGLE_T1534300504_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1534300504 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_7;

public:
	inline static int32_t get_offset_of_m_value_7() { return static_cast<int32_t>(offsetof(Single_t1534300504, ___m_value_7)); }
	inline float get_m_value_7() const { return ___m_value_7; }
	inline float* get_address_of_m_value_7() { return &___m_value_7; }
	inline void set_m_value_7(float value)
	{
		___m_value_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1534300504_H
#ifndef PROPERTYATTRIBUTE_T1226012101_H
#define PROPERTYATTRIBUTE_T1226012101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t1226012101  : public Attribute_t2169343654
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T1226012101_H
#ifndef ENUM_T1912704450_H
#define ENUM_T1912704450_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t1912704450  : public ValueType_t2697047229
{
public:

public:
};

struct Enum_t1912704450_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3669675803* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t1912704450_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3669675803* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3669675803** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3669675803* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t1912704450_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t1912704450_marshaled_com
{
};
#endif // ENUM_T1912704450_H
#ifndef VERTEXATTACHMENT_T1776545046_H
#define VERTEXATTACHMENT_T1776545046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.VertexAttachment
struct  VertexAttachment_t1776545046  : public Attachment_t397756874
{
public:
	// System.Int32 Spine.VertexAttachment::id
	int32_t ___id_3;
	// System.Int32[] Spine.VertexAttachment::bones
	Int32U5BU5D_t281224829* ___bones_4;
	// System.Single[] Spine.VertexAttachment::vertices
	SingleU5BU5D_t3989243465* ___vertices_5;
	// System.Int32 Spine.VertexAttachment::worldVerticesLength
	int32_t ___worldVerticesLength_6;

public:
	inline static int32_t get_offset_of_id_3() { return static_cast<int32_t>(offsetof(VertexAttachment_t1776545046, ___id_3)); }
	inline int32_t get_id_3() const { return ___id_3; }
	inline int32_t* get_address_of_id_3() { return &___id_3; }
	inline void set_id_3(int32_t value)
	{
		___id_3 = value;
	}

	inline static int32_t get_offset_of_bones_4() { return static_cast<int32_t>(offsetof(VertexAttachment_t1776545046, ___bones_4)); }
	inline Int32U5BU5D_t281224829* get_bones_4() const { return ___bones_4; }
	inline Int32U5BU5D_t281224829** get_address_of_bones_4() { return &___bones_4; }
	inline void set_bones_4(Int32U5BU5D_t281224829* value)
	{
		___bones_4 = value;
		Il2CppCodeGenWriteBarrier((&___bones_4), value);
	}

	inline static int32_t get_offset_of_vertices_5() { return static_cast<int32_t>(offsetof(VertexAttachment_t1776545046, ___vertices_5)); }
	inline SingleU5BU5D_t3989243465* get_vertices_5() const { return ___vertices_5; }
	inline SingleU5BU5D_t3989243465** get_address_of_vertices_5() { return &___vertices_5; }
	inline void set_vertices_5(SingleU5BU5D_t3989243465* value)
	{
		___vertices_5 = value;
		Il2CppCodeGenWriteBarrier((&___vertices_5), value);
	}

	inline static int32_t get_offset_of_worldVerticesLength_6() { return static_cast<int32_t>(offsetof(VertexAttachment_t1776545046, ___worldVerticesLength_6)); }
	inline int32_t get_worldVerticesLength_6() const { return ___worldVerticesLength_6; }
	inline int32_t* get_address_of_worldVerticesLength_6() { return &___worldVerticesLength_6; }
	inline void set_worldVerticesLength_6(int32_t value)
	{
		___worldVerticesLength_6 = value;
	}
};

struct VertexAttachment_t1776545046_StaticFields
{
public:
	// System.Int32 Spine.VertexAttachment::nextID
	int32_t ___nextID_1;
	// System.Object Spine.VertexAttachment::nextIdLock
	RuntimeObject * ___nextIdLock_2;

public:
	inline static int32_t get_offset_of_nextID_1() { return static_cast<int32_t>(offsetof(VertexAttachment_t1776545046_StaticFields, ___nextID_1)); }
	inline int32_t get_nextID_1() const { return ___nextID_1; }
	inline int32_t* get_address_of_nextID_1() { return &___nextID_1; }
	inline void set_nextID_1(int32_t value)
	{
		___nextID_1 = value;
	}

	inline static int32_t get_offset_of_nextIdLock_2() { return static_cast<int32_t>(offsetof(VertexAttachment_t1776545046_StaticFields, ___nextIdLock_2)); }
	inline RuntimeObject * get_nextIdLock_2() const { return ___nextIdLock_2; }
	inline RuntimeObject ** get_address_of_nextIdLock_2() { return &___nextIdLock_2; }
	inline void set_nextIdLock_2(RuntimeObject * value)
	{
		___nextIdLock_2 = value;
		Il2CppCodeGenWriteBarrier((&___nextIdLock_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERTEXATTACHMENT_T1776545046_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	IntPtr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline IntPtr_t get_Zero_1() const { return ___Zero_1; }
	inline IntPtr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(IntPtr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef INT32_T4116043316_H
#define INT32_T4116043316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t4116043316 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Int32_t4116043316, ___m_value_2)); }
	inline int32_t get_m_value_2() const { return ___m_value_2; }
	inline int32_t* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(int32_t value)
	{
		___m_value_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T4116043316_H
#ifndef SUBMESHINSTRUCTION_T3765917328_H
#define SUBMESHINSTRUCTION_T3765917328_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SubmeshInstruction
struct  SubmeshInstruction_t3765917328 
{
public:
	// Spine.Skeleton Spine.Unity.SubmeshInstruction::skeleton
	Skeleton_t1045203634 * ___skeleton_0;
	// System.Int32 Spine.Unity.SubmeshInstruction::startSlot
	int32_t ___startSlot_1;
	// System.Int32 Spine.Unity.SubmeshInstruction::endSlot
	int32_t ___endSlot_2;
	// UnityEngine.Material Spine.Unity.SubmeshInstruction::material
	Material_t2681358023 * ___material_3;
	// System.Boolean Spine.Unity.SubmeshInstruction::forceSeparate
	bool ___forceSeparate_4;
	// System.Int32 Spine.Unity.SubmeshInstruction::preActiveClippingSlotSource
	int32_t ___preActiveClippingSlotSource_5;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawTriangleCount
	int32_t ___rawTriangleCount_6;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawVertexCount
	int32_t ___rawVertexCount_7;
	// System.Int32 Spine.Unity.SubmeshInstruction::rawFirstVertexIndex
	int32_t ___rawFirstVertexIndex_8;
	// System.Boolean Spine.Unity.SubmeshInstruction::hasClipping
	bool ___hasClipping_9;

public:
	inline static int32_t get_offset_of_skeleton_0() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___skeleton_0)); }
	inline Skeleton_t1045203634 * get_skeleton_0() const { return ___skeleton_0; }
	inline Skeleton_t1045203634 ** get_address_of_skeleton_0() { return &___skeleton_0; }
	inline void set_skeleton_0(Skeleton_t1045203634 * value)
	{
		___skeleton_0 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_0), value);
	}

	inline static int32_t get_offset_of_startSlot_1() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___startSlot_1)); }
	inline int32_t get_startSlot_1() const { return ___startSlot_1; }
	inline int32_t* get_address_of_startSlot_1() { return &___startSlot_1; }
	inline void set_startSlot_1(int32_t value)
	{
		___startSlot_1 = value;
	}

	inline static int32_t get_offset_of_endSlot_2() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___endSlot_2)); }
	inline int32_t get_endSlot_2() const { return ___endSlot_2; }
	inline int32_t* get_address_of_endSlot_2() { return &___endSlot_2; }
	inline void set_endSlot_2(int32_t value)
	{
		___endSlot_2 = value;
	}

	inline static int32_t get_offset_of_material_3() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___material_3)); }
	inline Material_t2681358023 * get_material_3() const { return ___material_3; }
	inline Material_t2681358023 ** get_address_of_material_3() { return &___material_3; }
	inline void set_material_3(Material_t2681358023 * value)
	{
		___material_3 = value;
		Il2CppCodeGenWriteBarrier((&___material_3), value);
	}

	inline static int32_t get_offset_of_forceSeparate_4() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___forceSeparate_4)); }
	inline bool get_forceSeparate_4() const { return ___forceSeparate_4; }
	inline bool* get_address_of_forceSeparate_4() { return &___forceSeparate_4; }
	inline void set_forceSeparate_4(bool value)
	{
		___forceSeparate_4 = value;
	}

	inline static int32_t get_offset_of_preActiveClippingSlotSource_5() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___preActiveClippingSlotSource_5)); }
	inline int32_t get_preActiveClippingSlotSource_5() const { return ___preActiveClippingSlotSource_5; }
	inline int32_t* get_address_of_preActiveClippingSlotSource_5() { return &___preActiveClippingSlotSource_5; }
	inline void set_preActiveClippingSlotSource_5(int32_t value)
	{
		___preActiveClippingSlotSource_5 = value;
	}

	inline static int32_t get_offset_of_rawTriangleCount_6() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___rawTriangleCount_6)); }
	inline int32_t get_rawTriangleCount_6() const { return ___rawTriangleCount_6; }
	inline int32_t* get_address_of_rawTriangleCount_6() { return &___rawTriangleCount_6; }
	inline void set_rawTriangleCount_6(int32_t value)
	{
		___rawTriangleCount_6 = value;
	}

	inline static int32_t get_offset_of_rawVertexCount_7() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___rawVertexCount_7)); }
	inline int32_t get_rawVertexCount_7() const { return ___rawVertexCount_7; }
	inline int32_t* get_address_of_rawVertexCount_7() { return &___rawVertexCount_7; }
	inline void set_rawVertexCount_7(int32_t value)
	{
		___rawVertexCount_7 = value;
	}

	inline static int32_t get_offset_of_rawFirstVertexIndex_8() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___rawFirstVertexIndex_8)); }
	inline int32_t get_rawFirstVertexIndex_8() const { return ___rawFirstVertexIndex_8; }
	inline int32_t* get_address_of_rawFirstVertexIndex_8() { return &___rawFirstVertexIndex_8; }
	inline void set_rawFirstVertexIndex_8(int32_t value)
	{
		___rawFirstVertexIndex_8 = value;
	}

	inline static int32_t get_offset_of_hasClipping_9() { return static_cast<int32_t>(offsetof(SubmeshInstruction_t3765917328, ___hasClipping_9)); }
	inline bool get_hasClipping_9() const { return ___hasClipping_9; }
	inline bool* get_address_of_hasClipping_9() { return &___hasClipping_9; }
	inline void set_hasClipping_9(bool value)
	{
		___hasClipping_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Spine.Unity.SubmeshInstruction
struct SubmeshInstruction_t3765917328_marshaled_pinvoke
{
	Skeleton_t1045203634 * ___skeleton_0;
	int32_t ___startSlot_1;
	int32_t ___endSlot_2;
	Material_t2681358023 * ___material_3;
	int32_t ___forceSeparate_4;
	int32_t ___preActiveClippingSlotSource_5;
	int32_t ___rawTriangleCount_6;
	int32_t ___rawVertexCount_7;
	int32_t ___rawFirstVertexIndex_8;
	int32_t ___hasClipping_9;
};
// Native definition for COM marshalling of Spine.Unity.SubmeshInstruction
struct SubmeshInstruction_t3765917328_marshaled_com
{
	Skeleton_t1045203634 * ___skeleton_0;
	int32_t ___startSlot_1;
	int32_t ___endSlot_2;
	Material_t2681358023 * ___material_3;
	int32_t ___forceSeparate_4;
	int32_t ___preActiveClippingSlotSource_5;
	int32_t ___rawTriangleCount_6;
	int32_t ___rawVertexCount_7;
	int32_t ___rawFirstVertexIndex_8;
	int32_t ___hasClipping_9;
};
#endif // SUBMESHINSTRUCTION_T3765917328_H
#ifndef DELEGATE_T1310841479_H
#define DELEGATE_T1310841479_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1310841479  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	IntPtr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	IntPtr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	IntPtr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	IntPtr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t878649875 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___invoke_impl_1)); }
	inline IntPtr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline IntPtr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(IntPtr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_3)); }
	inline IntPtr_t get_method_3() const { return ___method_3; }
	inline IntPtr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(IntPtr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___delegate_trampoline_4)); }
	inline IntPtr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline IntPtr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(IntPtr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_code_5)); }
	inline IntPtr_t get_method_code_5() const { return ___method_code_5; }
	inline IntPtr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(IntPtr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1310841479, ___data_8)); }
	inline DelegateData_t878649875 * get_data_8() const { return ___data_8; }
	inline DelegateData_t878649875 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t878649875 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1310841479_H
#ifndef OBJECT_T350248726_H
#define OBJECT_T350248726_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t350248726  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	IntPtr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t350248726, ___m_CachedPtr_0)); }
	inline IntPtr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline IntPtr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(IntPtr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t350248726_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t350248726_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t350248726_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t350248726_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T350248726_H
#ifndef SPINEATTRIBUTEBASE_T3125339574_H
#define SPINEATTRIBUTEBASE_T3125339574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttributeBase
struct  SpineAttributeBase_t3125339574  : public PropertyAttribute_t1226012101
{
public:
	// System.String Spine.Unity.SpineAttributeBase::dataField
	String_t* ___dataField_0;
	// System.String Spine.Unity.SpineAttributeBase::startsWith
	String_t* ___startsWith_1;
	// System.Boolean Spine.Unity.SpineAttributeBase::includeNone
	bool ___includeNone_2;
	// System.Boolean Spine.Unity.SpineAttributeBase::fallbackToTextField
	bool ___fallbackToTextField_3;

public:
	inline static int32_t get_offset_of_dataField_0() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t3125339574, ___dataField_0)); }
	inline String_t* get_dataField_0() const { return ___dataField_0; }
	inline String_t** get_address_of_dataField_0() { return &___dataField_0; }
	inline void set_dataField_0(String_t* value)
	{
		___dataField_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataField_0), value);
	}

	inline static int32_t get_offset_of_startsWith_1() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t3125339574, ___startsWith_1)); }
	inline String_t* get_startsWith_1() const { return ___startsWith_1; }
	inline String_t** get_address_of_startsWith_1() { return &___startsWith_1; }
	inline void set_startsWith_1(String_t* value)
	{
		___startsWith_1 = value;
		Il2CppCodeGenWriteBarrier((&___startsWith_1), value);
	}

	inline static int32_t get_offset_of_includeNone_2() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t3125339574, ___includeNone_2)); }
	inline bool get_includeNone_2() const { return ___includeNone_2; }
	inline bool* get_address_of_includeNone_2() { return &___includeNone_2; }
	inline void set_includeNone_2(bool value)
	{
		___includeNone_2 = value;
	}

	inline static int32_t get_offset_of_fallbackToTextField_3() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t3125339574, ___fallbackToTextField_3)); }
	inline bool get_fallbackToTextField_3() const { return ___fallbackToTextField_3; }
	inline bool* get_address_of_fallbackToTextField_3() { return &___fallbackToTextField_3; }
	inline void set_fallbackToTextField_3(bool value)
	{
		___fallbackToTextField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATTRIBUTEBASE_T3125339574_H
#ifndef HIDEFLAGS_T1317629733_H
#define HIDEFLAGS_T1317629733_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.HideFlags
struct  HideFlags_t1317629733 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HideFlags_t1317629733, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEFLAGS_T1317629733_H
#ifndef SPINEEVENT_T1674004351_H
#define SPINEEVENT_T1674004351_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineEvent
struct  SpineEvent_t1674004351  : public SpineAttributeBase_t3125339574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEEVENT_T1674004351_H
#ifndef SPINEIKCONSTRAINT_T203333819_H
#define SPINEIKCONSTRAINT_T203333819_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineIkConstraint
struct  SpineIkConstraint_t203333819  : public SpineAttributeBase_t3125339574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEIKCONSTRAINT_T203333819_H
#ifndef MULTICASTDELEGATE_T4207739222_H
#define MULTICASTDELEGATE_T4207739222_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t4207739222  : public Delegate_t1310841479
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t4207739222 * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t4207739222 * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4207739222, ___prev_9)); }
	inline MulticastDelegate_t4207739222 * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t4207739222 ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t4207739222 * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t4207739222, ___kpm_next_10)); }
	inline MulticastDelegate_t4207739222 * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t4207739222 ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t4207739222 * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T4207739222_H
#ifndef MATERIAL_T2681358023_H
#define MATERIAL_T2681358023_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Material
struct  Material_t2681358023  : public Object_t350248726
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIAL_T2681358023_H
#ifndef MESH_T3526862104_H
#define MESH_T3526862104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Mesh
struct  Mesh_t3526862104  : public Object_t350248726
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESH_T3526862104_H
#ifndef SPINEPATHCONSTRAINT_T1221980072_H
#define SPINEPATHCONSTRAINT_T1221980072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpinePathConstraint
struct  SpinePathConstraint_t1221980072  : public SpineAttributeBase_t3125339574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEPATHCONSTRAINT_T1221980072_H
#ifndef SPINESKIN_T1537368333_H
#define SPINESKIN_T1537368333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineSkin
struct  SpineSkin_t1537368333  : public SpineAttributeBase_t3125339574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESKIN_T1537368333_H
#ifndef SPINESLOT_T3243618918_H
#define SPINESLOT_T3243618918_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineSlot
struct  SpineSlot_t3243618918  : public SpineAttributeBase_t3125339574
{
public:
	// System.Boolean Spine.Unity.SpineSlot::containsBoundingBoxes
	bool ___containsBoundingBoxes_4;

public:
	inline static int32_t get_offset_of_containsBoundingBoxes_4() { return static_cast<int32_t>(offsetof(SpineSlot_t3243618918, ___containsBoundingBoxes_4)); }
	inline bool get_containsBoundingBoxes_4() const { return ___containsBoundingBoxes_4; }
	inline bool* get_address_of_containsBoundingBoxes_4() { return &___containsBoundingBoxes_4; }
	inline void set_containsBoundingBoxes_4(bool value)
	{
		___containsBoundingBoxes_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINESLOT_T3243618918_H
#ifndef SPINETRANSFORMCONSTRAINT_T2634477230_H
#define SPINETRANSFORMCONSTRAINT_T2634477230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineTransformConstraint
struct  SpineTransformConstraint_t2634477230  : public SpineAttributeBase_t3125339574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINETRANSFORMCONSTRAINT_T2634477230_H
#ifndef SPINEMESH_T4270445991_H
#define SPINEMESH_T4270445991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineMesh
struct  SpineMesh_t4270445991  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEMESH_T4270445991_H
#ifndef COMPONENT_T1852985663_H
#define COMPONENT_T1852985663_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1852985663  : public Object_t350248726
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1852985663_H
#ifndef TRACKENTRYEVENTDELEGATE_T471240152_H
#define TRACKENTRYEVENTDELEGATE_T471240152_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState/TrackEntryEventDelegate
struct  TrackEntryEventDelegate_t471240152  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRYEVENTDELEGATE_T471240152_H
#ifndef TRACKENTRYDELEGATE_T1807657294_H
#define TRACKENTRYDELEGATE_T1807657294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.AnimationState/TrackEntryDelegate
struct  TrackEntryDelegate_t1807657294  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKENTRYDELEGATE_T1807657294_H
#ifndef BEHAVIOUR_T2905267502_H
#define BEHAVIOUR_T2905267502_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t2905267502  : public Component_t1852985663
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T2905267502_H
#ifndef ASYNCCALLBACK_T3972436406_H
#define ASYNCCALLBACK_T3972436406_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3972436406  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3972436406_H
#ifndef UPDATEBONESDELEGATE_T500272946_H
#define UPDATEBONESDELEGATE_T500272946_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.UpdateBonesDelegate
struct  UpdateBonesDelegate_t500272946  : public MulticastDelegate_t4207739222
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEBONESDELEGATE_T500272946_H
#ifndef MONOBEHAVIOUR_T3755042618_H
#define MONOBEHAVIOUR_T3755042618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3755042618  : public Behaviour_t2905267502
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3755042618_H
#ifndef SKELETONRENDERER_T1681389628_H
#define SKELETONRENDERER_T1681389628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonRenderer
struct  SkeletonRenderer_t1681389628  : public MonoBehaviour_t3755042618
{
public:
	// Spine.Unity.SkeletonRenderer/SkeletonRendererDelegate Spine.Unity.SkeletonRenderer::OnRebuild
	SkeletonRendererDelegate_t878065760 * ___OnRebuild_2;
	// Spine.Unity.MeshGeneratorDelegate Spine.Unity.SkeletonRenderer::OnPostProcessVertices
	MeshGeneratorDelegate_t2067952305 * ___OnPostProcessVertices_3;
	// Spine.Unity.SkeletonDataAsset Spine.Unity.SkeletonRenderer::skeletonDataAsset
	SkeletonDataAsset_t1706375087 * ___skeletonDataAsset_4;
	// System.String Spine.Unity.SkeletonRenderer::initialSkinName
	String_t* ___initialSkinName_5;
	// System.Boolean Spine.Unity.SkeletonRenderer::initialFlipX
	bool ___initialFlipX_6;
	// System.Boolean Spine.Unity.SkeletonRenderer::initialFlipY
	bool ___initialFlipY_7;
	// System.String[] Spine.Unity.SkeletonRenderer::separatorSlotNames
	StringU5BU5D_t656593794* ___separatorSlotNames_8;
	// System.Collections.Generic.List`1<Spine.Slot> Spine.Unity.SkeletonRenderer::separatorSlots
	List_1_t3927532068 * ___separatorSlots_9;
	// System.Single Spine.Unity.SkeletonRenderer::zSpacing
	float ___zSpacing_10;
	// System.Boolean Spine.Unity.SkeletonRenderer::useClipping
	bool ___useClipping_11;
	// System.Boolean Spine.Unity.SkeletonRenderer::immutableTriangles
	bool ___immutableTriangles_12;
	// System.Boolean Spine.Unity.SkeletonRenderer::pmaVertexColors
	bool ___pmaVertexColors_13;
	// System.Boolean Spine.Unity.SkeletonRenderer::clearStateOnDisable
	bool ___clearStateOnDisable_14;
	// System.Boolean Spine.Unity.SkeletonRenderer::tintBlack
	bool ___tintBlack_15;
	// System.Boolean Spine.Unity.SkeletonRenderer::singleSubmesh
	bool ___singleSubmesh_16;
	// System.Boolean Spine.Unity.SkeletonRenderer::addNormals
	bool ___addNormals_17;
	// System.Boolean Spine.Unity.SkeletonRenderer::calculateTangents
	bool ___calculateTangents_18;
	// System.Boolean Spine.Unity.SkeletonRenderer::logErrors
	bool ___logErrors_19;
	// System.Boolean Spine.Unity.SkeletonRenderer::disableRenderingOnOverride
	bool ___disableRenderingOnOverride_20;
	// Spine.Unity.SkeletonRenderer/InstructionDelegate Spine.Unity.SkeletonRenderer::generateMeshOverride
	InstructionDelegate_t1951844748 * ___generateMeshOverride_21;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Material,UnityEngine.Material> Spine.Unity.SkeletonRenderer::customMaterialOverride
	Dictionary_2_t2258539287 * ___customMaterialOverride_22;
	// System.Collections.Generic.Dictionary`2<Spine.Slot,UnityEngine.Material> Spine.Unity.SkeletonRenderer::customSlotMaterials
	Dictionary_2_t14780327 * ___customSlotMaterials_23;
	// UnityEngine.MeshRenderer Spine.Unity.SkeletonRenderer::meshRenderer
	MeshRenderer_t1621355309 * ___meshRenderer_24;
	// UnityEngine.MeshFilter Spine.Unity.SkeletonRenderer::meshFilter
	MeshFilter_t36322911 * ___meshFilter_25;
	// System.Boolean Spine.Unity.SkeletonRenderer::valid
	bool ___valid_26;
	// Spine.Skeleton Spine.Unity.SkeletonRenderer::skeleton
	Skeleton_t1045203634 * ___skeleton_27;
	// Spine.Unity.SkeletonRendererInstruction Spine.Unity.SkeletonRenderer::currentInstructions
	SkeletonRendererInstruction_t672271390 * ___currentInstructions_28;
	// Spine.Unity.MeshGenerator Spine.Unity.SkeletonRenderer::meshGenerator
	MeshGenerator_t557654067 * ___meshGenerator_29;
	// Spine.Unity.MeshRendererBuffers Spine.Unity.SkeletonRenderer::rendererBuffers
	MeshRendererBuffers_t384051837 * ___rendererBuffers_30;

public:
	inline static int32_t get_offset_of_OnRebuild_2() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___OnRebuild_2)); }
	inline SkeletonRendererDelegate_t878065760 * get_OnRebuild_2() const { return ___OnRebuild_2; }
	inline SkeletonRendererDelegate_t878065760 ** get_address_of_OnRebuild_2() { return &___OnRebuild_2; }
	inline void set_OnRebuild_2(SkeletonRendererDelegate_t878065760 * value)
	{
		___OnRebuild_2 = value;
		Il2CppCodeGenWriteBarrier((&___OnRebuild_2), value);
	}

	inline static int32_t get_offset_of_OnPostProcessVertices_3() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___OnPostProcessVertices_3)); }
	inline MeshGeneratorDelegate_t2067952305 * get_OnPostProcessVertices_3() const { return ___OnPostProcessVertices_3; }
	inline MeshGeneratorDelegate_t2067952305 ** get_address_of_OnPostProcessVertices_3() { return &___OnPostProcessVertices_3; }
	inline void set_OnPostProcessVertices_3(MeshGeneratorDelegate_t2067952305 * value)
	{
		___OnPostProcessVertices_3 = value;
		Il2CppCodeGenWriteBarrier((&___OnPostProcessVertices_3), value);
	}

	inline static int32_t get_offset_of_skeletonDataAsset_4() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___skeletonDataAsset_4)); }
	inline SkeletonDataAsset_t1706375087 * get_skeletonDataAsset_4() const { return ___skeletonDataAsset_4; }
	inline SkeletonDataAsset_t1706375087 ** get_address_of_skeletonDataAsset_4() { return &___skeletonDataAsset_4; }
	inline void set_skeletonDataAsset_4(SkeletonDataAsset_t1706375087 * value)
	{
		___skeletonDataAsset_4 = value;
		Il2CppCodeGenWriteBarrier((&___skeletonDataAsset_4), value);
	}

	inline static int32_t get_offset_of_initialSkinName_5() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___initialSkinName_5)); }
	inline String_t* get_initialSkinName_5() const { return ___initialSkinName_5; }
	inline String_t** get_address_of_initialSkinName_5() { return &___initialSkinName_5; }
	inline void set_initialSkinName_5(String_t* value)
	{
		___initialSkinName_5 = value;
		Il2CppCodeGenWriteBarrier((&___initialSkinName_5), value);
	}

	inline static int32_t get_offset_of_initialFlipX_6() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___initialFlipX_6)); }
	inline bool get_initialFlipX_6() const { return ___initialFlipX_6; }
	inline bool* get_address_of_initialFlipX_6() { return &___initialFlipX_6; }
	inline void set_initialFlipX_6(bool value)
	{
		___initialFlipX_6 = value;
	}

	inline static int32_t get_offset_of_initialFlipY_7() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___initialFlipY_7)); }
	inline bool get_initialFlipY_7() const { return ___initialFlipY_7; }
	inline bool* get_address_of_initialFlipY_7() { return &___initialFlipY_7; }
	inline void set_initialFlipY_7(bool value)
	{
		___initialFlipY_7 = value;
	}

	inline static int32_t get_offset_of_separatorSlotNames_8() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___separatorSlotNames_8)); }
	inline StringU5BU5D_t656593794* get_separatorSlotNames_8() const { return ___separatorSlotNames_8; }
	inline StringU5BU5D_t656593794** get_address_of_separatorSlotNames_8() { return &___separatorSlotNames_8; }
	inline void set_separatorSlotNames_8(StringU5BU5D_t656593794* value)
	{
		___separatorSlotNames_8 = value;
		Il2CppCodeGenWriteBarrier((&___separatorSlotNames_8), value);
	}

	inline static int32_t get_offset_of_separatorSlots_9() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___separatorSlots_9)); }
	inline List_1_t3927532068 * get_separatorSlots_9() const { return ___separatorSlots_9; }
	inline List_1_t3927532068 ** get_address_of_separatorSlots_9() { return &___separatorSlots_9; }
	inline void set_separatorSlots_9(List_1_t3927532068 * value)
	{
		___separatorSlots_9 = value;
		Il2CppCodeGenWriteBarrier((&___separatorSlots_9), value);
	}

	inline static int32_t get_offset_of_zSpacing_10() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___zSpacing_10)); }
	inline float get_zSpacing_10() const { return ___zSpacing_10; }
	inline float* get_address_of_zSpacing_10() { return &___zSpacing_10; }
	inline void set_zSpacing_10(float value)
	{
		___zSpacing_10 = value;
	}

	inline static int32_t get_offset_of_useClipping_11() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___useClipping_11)); }
	inline bool get_useClipping_11() const { return ___useClipping_11; }
	inline bool* get_address_of_useClipping_11() { return &___useClipping_11; }
	inline void set_useClipping_11(bool value)
	{
		___useClipping_11 = value;
	}

	inline static int32_t get_offset_of_immutableTriangles_12() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___immutableTriangles_12)); }
	inline bool get_immutableTriangles_12() const { return ___immutableTriangles_12; }
	inline bool* get_address_of_immutableTriangles_12() { return &___immutableTriangles_12; }
	inline void set_immutableTriangles_12(bool value)
	{
		___immutableTriangles_12 = value;
	}

	inline static int32_t get_offset_of_pmaVertexColors_13() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___pmaVertexColors_13)); }
	inline bool get_pmaVertexColors_13() const { return ___pmaVertexColors_13; }
	inline bool* get_address_of_pmaVertexColors_13() { return &___pmaVertexColors_13; }
	inline void set_pmaVertexColors_13(bool value)
	{
		___pmaVertexColors_13 = value;
	}

	inline static int32_t get_offset_of_clearStateOnDisable_14() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___clearStateOnDisable_14)); }
	inline bool get_clearStateOnDisable_14() const { return ___clearStateOnDisable_14; }
	inline bool* get_address_of_clearStateOnDisable_14() { return &___clearStateOnDisable_14; }
	inline void set_clearStateOnDisable_14(bool value)
	{
		___clearStateOnDisable_14 = value;
	}

	inline static int32_t get_offset_of_tintBlack_15() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___tintBlack_15)); }
	inline bool get_tintBlack_15() const { return ___tintBlack_15; }
	inline bool* get_address_of_tintBlack_15() { return &___tintBlack_15; }
	inline void set_tintBlack_15(bool value)
	{
		___tintBlack_15 = value;
	}

	inline static int32_t get_offset_of_singleSubmesh_16() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___singleSubmesh_16)); }
	inline bool get_singleSubmesh_16() const { return ___singleSubmesh_16; }
	inline bool* get_address_of_singleSubmesh_16() { return &___singleSubmesh_16; }
	inline void set_singleSubmesh_16(bool value)
	{
		___singleSubmesh_16 = value;
	}

	inline static int32_t get_offset_of_addNormals_17() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___addNormals_17)); }
	inline bool get_addNormals_17() const { return ___addNormals_17; }
	inline bool* get_address_of_addNormals_17() { return &___addNormals_17; }
	inline void set_addNormals_17(bool value)
	{
		___addNormals_17 = value;
	}

	inline static int32_t get_offset_of_calculateTangents_18() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___calculateTangents_18)); }
	inline bool get_calculateTangents_18() const { return ___calculateTangents_18; }
	inline bool* get_address_of_calculateTangents_18() { return &___calculateTangents_18; }
	inline void set_calculateTangents_18(bool value)
	{
		___calculateTangents_18 = value;
	}

	inline static int32_t get_offset_of_logErrors_19() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___logErrors_19)); }
	inline bool get_logErrors_19() const { return ___logErrors_19; }
	inline bool* get_address_of_logErrors_19() { return &___logErrors_19; }
	inline void set_logErrors_19(bool value)
	{
		___logErrors_19 = value;
	}

	inline static int32_t get_offset_of_disableRenderingOnOverride_20() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___disableRenderingOnOverride_20)); }
	inline bool get_disableRenderingOnOverride_20() const { return ___disableRenderingOnOverride_20; }
	inline bool* get_address_of_disableRenderingOnOverride_20() { return &___disableRenderingOnOverride_20; }
	inline void set_disableRenderingOnOverride_20(bool value)
	{
		___disableRenderingOnOverride_20 = value;
	}

	inline static int32_t get_offset_of_generateMeshOverride_21() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___generateMeshOverride_21)); }
	inline InstructionDelegate_t1951844748 * get_generateMeshOverride_21() const { return ___generateMeshOverride_21; }
	inline InstructionDelegate_t1951844748 ** get_address_of_generateMeshOverride_21() { return &___generateMeshOverride_21; }
	inline void set_generateMeshOverride_21(InstructionDelegate_t1951844748 * value)
	{
		___generateMeshOverride_21 = value;
		Il2CppCodeGenWriteBarrier((&___generateMeshOverride_21), value);
	}

	inline static int32_t get_offset_of_customMaterialOverride_22() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___customMaterialOverride_22)); }
	inline Dictionary_2_t2258539287 * get_customMaterialOverride_22() const { return ___customMaterialOverride_22; }
	inline Dictionary_2_t2258539287 ** get_address_of_customMaterialOverride_22() { return &___customMaterialOverride_22; }
	inline void set_customMaterialOverride_22(Dictionary_2_t2258539287 * value)
	{
		___customMaterialOverride_22 = value;
		Il2CppCodeGenWriteBarrier((&___customMaterialOverride_22), value);
	}

	inline static int32_t get_offset_of_customSlotMaterials_23() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___customSlotMaterials_23)); }
	inline Dictionary_2_t14780327 * get_customSlotMaterials_23() const { return ___customSlotMaterials_23; }
	inline Dictionary_2_t14780327 ** get_address_of_customSlotMaterials_23() { return &___customSlotMaterials_23; }
	inline void set_customSlotMaterials_23(Dictionary_2_t14780327 * value)
	{
		___customSlotMaterials_23 = value;
		Il2CppCodeGenWriteBarrier((&___customSlotMaterials_23), value);
	}

	inline static int32_t get_offset_of_meshRenderer_24() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___meshRenderer_24)); }
	inline MeshRenderer_t1621355309 * get_meshRenderer_24() const { return ___meshRenderer_24; }
	inline MeshRenderer_t1621355309 ** get_address_of_meshRenderer_24() { return &___meshRenderer_24; }
	inline void set_meshRenderer_24(MeshRenderer_t1621355309 * value)
	{
		___meshRenderer_24 = value;
		Il2CppCodeGenWriteBarrier((&___meshRenderer_24), value);
	}

	inline static int32_t get_offset_of_meshFilter_25() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___meshFilter_25)); }
	inline MeshFilter_t36322911 * get_meshFilter_25() const { return ___meshFilter_25; }
	inline MeshFilter_t36322911 ** get_address_of_meshFilter_25() { return &___meshFilter_25; }
	inline void set_meshFilter_25(MeshFilter_t36322911 * value)
	{
		___meshFilter_25 = value;
		Il2CppCodeGenWriteBarrier((&___meshFilter_25), value);
	}

	inline static int32_t get_offset_of_valid_26() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___valid_26)); }
	inline bool get_valid_26() const { return ___valid_26; }
	inline bool* get_address_of_valid_26() { return &___valid_26; }
	inline void set_valid_26(bool value)
	{
		___valid_26 = value;
	}

	inline static int32_t get_offset_of_skeleton_27() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___skeleton_27)); }
	inline Skeleton_t1045203634 * get_skeleton_27() const { return ___skeleton_27; }
	inline Skeleton_t1045203634 ** get_address_of_skeleton_27() { return &___skeleton_27; }
	inline void set_skeleton_27(Skeleton_t1045203634 * value)
	{
		___skeleton_27 = value;
		Il2CppCodeGenWriteBarrier((&___skeleton_27), value);
	}

	inline static int32_t get_offset_of_currentInstructions_28() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___currentInstructions_28)); }
	inline SkeletonRendererInstruction_t672271390 * get_currentInstructions_28() const { return ___currentInstructions_28; }
	inline SkeletonRendererInstruction_t672271390 ** get_address_of_currentInstructions_28() { return &___currentInstructions_28; }
	inline void set_currentInstructions_28(SkeletonRendererInstruction_t672271390 * value)
	{
		___currentInstructions_28 = value;
		Il2CppCodeGenWriteBarrier((&___currentInstructions_28), value);
	}

	inline static int32_t get_offset_of_meshGenerator_29() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___meshGenerator_29)); }
	inline MeshGenerator_t557654067 * get_meshGenerator_29() const { return ___meshGenerator_29; }
	inline MeshGenerator_t557654067 ** get_address_of_meshGenerator_29() { return &___meshGenerator_29; }
	inline void set_meshGenerator_29(MeshGenerator_t557654067 * value)
	{
		___meshGenerator_29 = value;
		Il2CppCodeGenWriteBarrier((&___meshGenerator_29), value);
	}

	inline static int32_t get_offset_of_rendererBuffers_30() { return static_cast<int32_t>(offsetof(SkeletonRenderer_t1681389628, ___rendererBuffers_30)); }
	inline MeshRendererBuffers_t384051837 * get_rendererBuffers_30() const { return ___rendererBuffers_30; }
	inline MeshRendererBuffers_t384051837 ** get_address_of_rendererBuffers_30() { return &___rendererBuffers_30; }
	inline void set_rendererBuffers_30(MeshRendererBuffers_t384051837 * value)
	{
		___rendererBuffers_30 = value;
		Il2CppCodeGenWriteBarrier((&___rendererBuffers_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONRENDERER_T1681389628_H
#ifndef SKELETONANIMATION_T2012766265_H
#define SKELETONANIMATION_T2012766265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SkeletonAnimation
struct  SkeletonAnimation_t2012766265  : public SkeletonRenderer_t1681389628
{
public:
	// Spine.AnimationState Spine.Unity.SkeletonAnimation::state
	AnimationState_t2858486651 * ___state_31;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateLocal
	UpdateBonesDelegate_t500272946 * ____UpdateLocal_32;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateWorld
	UpdateBonesDelegate_t500272946 * ____UpdateWorld_33;
	// Spine.Unity.UpdateBonesDelegate Spine.Unity.SkeletonAnimation::_UpdateComplete
	UpdateBonesDelegate_t500272946 * ____UpdateComplete_34;
	// System.String Spine.Unity.SkeletonAnimation::_animationName
	String_t* ____animationName_35;
	// System.Boolean Spine.Unity.SkeletonAnimation::loop
	bool ___loop_36;
	// System.Single Spine.Unity.SkeletonAnimation::timeScale
	float ___timeScale_37;

public:
	inline static int32_t get_offset_of_state_31() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ___state_31)); }
	inline AnimationState_t2858486651 * get_state_31() const { return ___state_31; }
	inline AnimationState_t2858486651 ** get_address_of_state_31() { return &___state_31; }
	inline void set_state_31(AnimationState_t2858486651 * value)
	{
		___state_31 = value;
		Il2CppCodeGenWriteBarrier((&___state_31), value);
	}

	inline static int32_t get_offset_of__UpdateLocal_32() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ____UpdateLocal_32)); }
	inline UpdateBonesDelegate_t500272946 * get__UpdateLocal_32() const { return ____UpdateLocal_32; }
	inline UpdateBonesDelegate_t500272946 ** get_address_of__UpdateLocal_32() { return &____UpdateLocal_32; }
	inline void set__UpdateLocal_32(UpdateBonesDelegate_t500272946 * value)
	{
		____UpdateLocal_32 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateLocal_32), value);
	}

	inline static int32_t get_offset_of__UpdateWorld_33() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ____UpdateWorld_33)); }
	inline UpdateBonesDelegate_t500272946 * get__UpdateWorld_33() const { return ____UpdateWorld_33; }
	inline UpdateBonesDelegate_t500272946 ** get_address_of__UpdateWorld_33() { return &____UpdateWorld_33; }
	inline void set__UpdateWorld_33(UpdateBonesDelegate_t500272946 * value)
	{
		____UpdateWorld_33 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateWorld_33), value);
	}

	inline static int32_t get_offset_of__UpdateComplete_34() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ____UpdateComplete_34)); }
	inline UpdateBonesDelegate_t500272946 * get__UpdateComplete_34() const { return ____UpdateComplete_34; }
	inline UpdateBonesDelegate_t500272946 ** get_address_of__UpdateComplete_34() { return &____UpdateComplete_34; }
	inline void set__UpdateComplete_34(UpdateBonesDelegate_t500272946 * value)
	{
		____UpdateComplete_34 = value;
		Il2CppCodeGenWriteBarrier((&____UpdateComplete_34), value);
	}

	inline static int32_t get_offset_of__animationName_35() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ____animationName_35)); }
	inline String_t* get__animationName_35() const { return ____animationName_35; }
	inline String_t** get_address_of__animationName_35() { return &____animationName_35; }
	inline void set__animationName_35(String_t* value)
	{
		____animationName_35 = value;
		Il2CppCodeGenWriteBarrier((&____animationName_35), value);
	}

	inline static int32_t get_offset_of_loop_36() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ___loop_36)); }
	inline bool get_loop_36() const { return ___loop_36; }
	inline bool* get_address_of_loop_36() { return &___loop_36; }
	inline void set_loop_36(bool value)
	{
		___loop_36 = value;
	}

	inline static int32_t get_offset_of_timeScale_37() { return static_cast<int32_t>(offsetof(SkeletonAnimation_t2012766265, ___timeScale_37)); }
	inline float get_timeScale_37() const { return ___timeScale_37; }
	inline float* get_address_of_timeScale_37() { return &___timeScale_37; }
	inline void set_timeScale_37(float value)
	{
		___timeScale_37 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SKELETONANIMATION_T2012766265_H
// System.Object[]
struct ObjectU5BU5D_t1100384052  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t281224829  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Single[]
struct SingleU5BU5D_t3989243465  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// Spine.Bone[]
struct BoneU5BU5D_t600500093  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Bone_t1763862836 * m_Items[1];

public:
	inline Bone_t1763862836 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Bone_t1763862836 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Bone_t1763862836 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Bone_t1763862836 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Bone_t1763862836 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Bone_t1763862836 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};



// System.Void Spine.Unity.SpineAttributeBase::.ctor()
extern "C"  void SpineAttributeBase__ctor_m3078789882 (SpineAttributeBase_t3125339574 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::.ctor()
extern "C"  void Mesh__ctor_m1780981819 (Mesh_t3526862104 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Mesh::MarkDynamic()
extern "C"  void Mesh_MarkDynamic_m3230628123 (Mesh_t3526862104 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_name(System.String)
extern "C"  void Object_set_name_m2640076692 (Object_t350248726 * __this, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C"  void Object_set_hideFlags_m3967038876 (Object_t350248726 * __this, int32_t p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Int32 Spine.Unity.SubmeshInstruction::get_SlotCount()
extern "C"  int32_t SubmeshInstruction_get_SlotCount_m1260428231 (SubmeshInstruction_t3765917328 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3076063157 (RuntimeObject * __this /* static, unused */, Object_t350248726 * p0, Object_t350248726 * p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m3459674631 (Object_t350248726 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object[])
extern "C"  String_t* String_Format_m345500613 (RuntimeObject * __this /* static, unused */, String_t* p0, ObjectU5BU5D_t1100384052* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Spine.Unity.SubmeshInstruction::ToString()
extern "C"  String_t* SubmeshInstruction_ToString_m1474118231 (SubmeshInstruction_t3765917328 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.Unity.UpdateBonesDelegate::Invoke(Spine.Unity.ISkeletonAnimation)
extern "C"  void UpdateBonesDelegate_Invoke_m835921997 (UpdateBonesDelegate_t500272946 * __this, RuntimeObject* ___animated0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m1723520498 (RuntimeObject * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.Unity.WaitForSpineAnimationComplete::SafeSubscribe(Spine.TrackEntry)
extern "C"  void WaitForSpineAnimationComplete_SafeSubscribe_m3415404190 (WaitForSpineAnimationComplete_t694865537 * __this, TrackEntry_t4062297782 * ___trackEntry0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object)
extern "C"  void Debug_LogWarning_m434037919 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.AnimationState/TrackEntryDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void TrackEntryDelegate__ctor_m2315773476 (TrackEntryDelegate_t1807657294 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.TrackEntry::add_Complete(Spine.AnimationState/TrackEntryDelegate)
extern "C"  void TrackEntry_add_Complete_m1607620779 (TrackEntry_t4062297782 * __this, TrackEntryDelegate_t1807657294 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.Unity.WaitForSpineEvent::Subscribe(Spine.AnimationState,Spine.EventData,System.Boolean)
extern "C"  void WaitForSpineEvent_Subscribe_m1024680374 (WaitForSpineEvent_t3387552986 * __this, AnimationState_t2858486651 * ___state0, EventData_t295981108 * ___eventDataReference1, bool ___unsubscribe2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.Unity.WaitForSpineEvent::SubscribeByName(Spine.AnimationState,System.String,System.Boolean)
extern "C"  void WaitForSpineEvent_SubscribeByName_m2257425455 (WaitForSpineEvent_t3387552986 * __this, AnimationState_t2858486651 * ___state0, String_t* ___eventName1, bool ___unsubscribe2, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.AnimationState/TrackEntryEventDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void TrackEntryEventDelegate__ctor_m2180400158 (TrackEntryEventDelegate_t471240152 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.AnimationState::add_Event(Spine.AnimationState/TrackEntryEventDelegate)
extern "C"  void AnimationState_add_Event_m4238647668 (AnimationState_t2858486651 * __this, TrackEntryEventDelegate_t471240152 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m1700356867 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// Spine.EventData Spine.Event::get_Data()
extern "C"  EventData_t295981108 * Event_get_Data_m2819791637 (Event_t27715519 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.String Spine.EventData::get_Name()
extern "C"  String_t* EventData_get_Name_m122311786 (EventData_t295981108 * __this, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1348988297 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.AnimationState::remove_Event(Spine.AnimationState/TrackEntryEventDelegate)
extern "C"  void AnimationState_remove_Event_m3240949950 (AnimationState_t2858486651 * __this, TrackEntryEventDelegate_t471240152 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.Unity.WaitForSpineEvent::Clear(Spine.AnimationState)
extern "C"  void WaitForSpineEvent_Clear_m1777236221 (WaitForSpineEvent_t3387552986 * __this, AnimationState_t2858486651 * ___state0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.Unity.WaitForSpineTrackEntryEnd::SafeSubscribe(Spine.TrackEntry)
extern "C"  void WaitForSpineTrackEntryEnd_SafeSubscribe_m952987264 (WaitForSpineTrackEntryEnd_t2837745715 * __this, TrackEntry_t4062297782 * ___trackEntry0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.TrackEntry::add_End(Spine.AnimationState/TrackEntryDelegate)
extern "C"  void TrackEntry_add_End_m941712940 (TrackEntry_t4062297782 * __this, TrackEntryDelegate_t1807657294 * ___value0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.Attachment::.ctor(System.String)
extern "C"  void Attachment__ctor_m3611922211 (Attachment_t397756874 * __this, String_t* ___name0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Enter(System.Object)
extern "C"  void Monitor_Enter_m3163704715 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.Monitor::Exit(System.Object)
extern "C"  void Monitor_Exit_m366607352 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
// System.Void Spine.VertexAttachment::ComputeWorldVertices(Spine.Slot,System.Int32,System.Int32,System.Single[],System.Int32,System.Int32)
extern "C"  void VertexAttachment_ComputeWorldVertices_m3306759128 (VertexAttachment_t1776545046 * __this, Slot_t1804116343 * ___slot0, int32_t ___start1, int32_t ___count2, SingleU5BU5D_t3989243465* ___worldVertices3, int32_t ___offset4, int32_t ___stride5, const RuntimeMethod* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Spine.Unity.SpineEvent::.ctor(System.String,System.String,System.Boolean,System.Boolean)
extern "C"  void SpineEvent__ctor_m1701659193 (SpineEvent_t1674004351 * __this, String_t* ___startsWith0, String_t* ___dataField1, bool ___includeNone2, bool ___fallbackToTextField3, const RuntimeMethod* method)
{
	{
		SpineAttributeBase__ctor_m3078789882(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___startsWith0;
		((SpineAttributeBase_t3125339574 *)__this)->set_startsWith_1(L_0);
		String_t* L_1 = ___dataField1;
		((SpineAttributeBase_t3125339574 *)__this)->set_dataField_0(L_1);
		bool L_2 = ___includeNone2;
		((SpineAttributeBase_t3125339574 *)__this)->set_includeNone_2(L_2);
		bool L_3 = ___fallbackToTextField3;
		((SpineAttributeBase_t3125339574 *)__this)->set_fallbackToTextField_3(L_3);
		return;
	}
}
// System.Void Spine.Unity.SpineIkConstraint::.ctor(System.String,System.String,System.Boolean,System.Boolean)
extern "C"  void SpineIkConstraint__ctor_m286597209 (SpineIkConstraint_t203333819 * __this, String_t* ___startsWith0, String_t* ___dataField1, bool ___includeNone2, bool ___fallbackToTextField3, const RuntimeMethod* method)
{
	{
		SpineAttributeBase__ctor_m3078789882(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___startsWith0;
		((SpineAttributeBase_t3125339574 *)__this)->set_startsWith_1(L_0);
		String_t* L_1 = ___dataField1;
		((SpineAttributeBase_t3125339574 *)__this)->set_dataField_0(L_1);
		bool L_2 = ___includeNone2;
		((SpineAttributeBase_t3125339574 *)__this)->set_includeNone_2(L_2);
		bool L_3 = ___fallbackToTextField3;
		((SpineAttributeBase_t3125339574 *)__this)->set_fallbackToTextField_3(L_3);
		return;
	}
}
// UnityEngine.Mesh Spine.Unity.SpineMesh::NewMesh()
extern "C"  Mesh_t3526862104 * SpineMesh_NewMesh_m1415254290 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpineMesh_NewMesh_m1415254290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Mesh_t3526862104 * V_0 = NULL;
	{
		Mesh_t3526862104 * L_0 = (Mesh_t3526862104 *)il2cpp_codegen_object_new(Mesh_t3526862104_il2cpp_TypeInfo_var);
		Mesh__ctor_m1780981819(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		Mesh_t3526862104 * L_1 = V_0;
		NullCheck(L_1);
		Mesh_MarkDynamic_m3230628123(L_1, /*hidden argument*/NULL);
		Mesh_t3526862104 * L_2 = V_0;
		NullCheck(L_2);
		Object_set_name_m2640076692(L_2, _stringLiteral556233983, /*hidden argument*/NULL);
		Mesh_t3526862104 * L_3 = V_0;
		NullCheck(L_3);
		Object_set_hideFlags_m3967038876(L_3, ((int32_t)20), /*hidden argument*/NULL);
		Mesh_t3526862104 * L_4 = V_0;
		return L_4;
	}
}
// System.Void Spine.Unity.SpinePathConstraint::.ctor(System.String,System.String,System.Boolean,System.Boolean)
extern "C"  void SpinePathConstraint__ctor_m256755195 (SpinePathConstraint_t1221980072 * __this, String_t* ___startsWith0, String_t* ___dataField1, bool ___includeNone2, bool ___fallbackToTextField3, const RuntimeMethod* method)
{
	{
		SpineAttributeBase__ctor_m3078789882(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___startsWith0;
		((SpineAttributeBase_t3125339574 *)__this)->set_startsWith_1(L_0);
		String_t* L_1 = ___dataField1;
		((SpineAttributeBase_t3125339574 *)__this)->set_dataField_0(L_1);
		bool L_2 = ___includeNone2;
		((SpineAttributeBase_t3125339574 *)__this)->set_includeNone_2(L_2);
		bool L_3 = ___fallbackToTextField3;
		((SpineAttributeBase_t3125339574 *)__this)->set_fallbackToTextField_3(L_3);
		return;
	}
}
// System.Void Spine.Unity.SpineSkin::.ctor(System.String,System.String,System.Boolean,System.Boolean)
extern "C"  void SpineSkin__ctor_m1909729262 (SpineSkin_t1537368333 * __this, String_t* ___startsWith0, String_t* ___dataField1, bool ___includeNone2, bool ___fallbackToTextField3, const RuntimeMethod* method)
{
	{
		SpineAttributeBase__ctor_m3078789882(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___startsWith0;
		((SpineAttributeBase_t3125339574 *)__this)->set_startsWith_1(L_0);
		String_t* L_1 = ___dataField1;
		((SpineAttributeBase_t3125339574 *)__this)->set_dataField_0(L_1);
		bool L_2 = ___includeNone2;
		((SpineAttributeBase_t3125339574 *)__this)->set_includeNone_2(L_2);
		bool L_3 = ___fallbackToTextField3;
		((SpineAttributeBase_t3125339574 *)__this)->set_fallbackToTextField_3(L_3);
		return;
	}
}
// System.Void Spine.Unity.SpineSlot::.ctor(System.String,System.String,System.Boolean,System.Boolean,System.Boolean)
extern "C"  void SpineSlot__ctor_m770221184 (SpineSlot_t3243618918 * __this, String_t* ___startsWith0, String_t* ___dataField1, bool ___containsBoundingBoxes2, bool ___includeNone3, bool ___fallbackToTextField4, const RuntimeMethod* method)
{
	{
		SpineAttributeBase__ctor_m3078789882(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___startsWith0;
		((SpineAttributeBase_t3125339574 *)__this)->set_startsWith_1(L_0);
		String_t* L_1 = ___dataField1;
		((SpineAttributeBase_t3125339574 *)__this)->set_dataField_0(L_1);
		bool L_2 = ___containsBoundingBoxes2;
		__this->set_containsBoundingBoxes_4(L_2);
		bool L_3 = ___includeNone3;
		((SpineAttributeBase_t3125339574 *)__this)->set_includeNone_2(L_3);
		bool L_4 = ___fallbackToTextField4;
		((SpineAttributeBase_t3125339574 *)__this)->set_fallbackToTextField_3(L_4);
		return;
	}
}
// System.Void Spine.Unity.SpineTransformConstraint::.ctor(System.String,System.String,System.Boolean,System.Boolean)
extern "C"  void SpineTransformConstraint__ctor_m628726777 (SpineTransformConstraint_t2634477230 * __this, String_t* ___startsWith0, String_t* ___dataField1, bool ___includeNone2, bool ___fallbackToTextField3, const RuntimeMethod* method)
{
	{
		SpineAttributeBase__ctor_m3078789882(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___startsWith0;
		((SpineAttributeBase_t3125339574 *)__this)->set_startsWith_1(L_0);
		String_t* L_1 = ___dataField1;
		((SpineAttributeBase_t3125339574 *)__this)->set_dataField_0(L_1);
		bool L_2 = ___includeNone2;
		((SpineAttributeBase_t3125339574 *)__this)->set_includeNone_2(L_2);
		bool L_3 = ___fallbackToTextField3;
		((SpineAttributeBase_t3125339574 *)__this)->set_fallbackToTextField_3(L_3);
		return;
	}
}
// Conversion methods for marshalling of: Spine.Unity.SubmeshInstruction
extern "C" void SubmeshInstruction_t3765917328_marshal_pinvoke(const SubmeshInstruction_t3765917328& unmarshaled, SubmeshInstruction_t3765917328_marshaled_pinvoke& marshaled)
{
	Il2CppCodeGenException* ___skeleton_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'skeleton' of type 'SubmeshInstruction': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___skeleton_0Exception);
}
extern "C" void SubmeshInstruction_t3765917328_marshal_pinvoke_back(const SubmeshInstruction_t3765917328_marshaled_pinvoke& marshaled, SubmeshInstruction_t3765917328& unmarshaled)
{
	Il2CppCodeGenException* ___skeleton_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'skeleton' of type 'SubmeshInstruction': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___skeleton_0Exception);
}
// Conversion method for clean up from marshalling of: Spine.Unity.SubmeshInstruction
extern "C" void SubmeshInstruction_t3765917328_marshal_pinvoke_cleanup(SubmeshInstruction_t3765917328_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Spine.Unity.SubmeshInstruction
extern "C" void SubmeshInstruction_t3765917328_marshal_com(const SubmeshInstruction_t3765917328& unmarshaled, SubmeshInstruction_t3765917328_marshaled_com& marshaled)
{
	Il2CppCodeGenException* ___skeleton_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'skeleton' of type 'SubmeshInstruction': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___skeleton_0Exception);
}
extern "C" void SubmeshInstruction_t3765917328_marshal_com_back(const SubmeshInstruction_t3765917328_marshaled_com& marshaled, SubmeshInstruction_t3765917328& unmarshaled)
{
	Il2CppCodeGenException* ___skeleton_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'skeleton' of type 'SubmeshInstruction': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___skeleton_0Exception);
}
// Conversion method for clean up from marshalling of: Spine.Unity.SubmeshInstruction
extern "C" void SubmeshInstruction_t3765917328_marshal_com_cleanup(SubmeshInstruction_t3765917328_marshaled_com& marshaled)
{
}
// System.Int32 Spine.Unity.SubmeshInstruction::get_SlotCount()
extern "C"  int32_t SubmeshInstruction_get_SlotCount_m1260428231 (SubmeshInstruction_t3765917328 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_endSlot_2();
		int32_t L_1 = __this->get_startSlot_1();
		return ((int32_t)((int32_t)L_0-(int32_t)L_1));
	}
}
extern "C"  int32_t SubmeshInstruction_get_SlotCount_m1260428231_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	SubmeshInstruction_t3765917328 * _thisAdjusted = reinterpret_cast<SubmeshInstruction_t3765917328 *>(__this + 1);
	return SubmeshInstruction_get_SlotCount_m1260428231(_thisAdjusted, method);
}
// System.String Spine.Unity.SubmeshInstruction::ToString()
extern "C"  String_t* SubmeshInstruction_ToString_m1474118231 (SubmeshInstruction_t3765917328 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubmeshInstruction_ToString_m1474118231_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t1100384052* G_B2_1 = NULL;
	ObjectU5BU5D_t1100384052* G_B2_2 = NULL;
	String_t* G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t1100384052* G_B1_1 = NULL;
	ObjectU5BU5D_t1100384052* G_B1_2 = NULL;
	String_t* G_B1_3 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t1100384052* G_B3_2 = NULL;
	ObjectU5BU5D_t1100384052* G_B3_3 = NULL;
	String_t* G_B3_4 = NULL;
	{
		ObjectU5BU5D_t1100384052* L_0 = ((ObjectU5BU5D_t1100384052*)SZArrayNew(ObjectU5BU5D_t1100384052_il2cpp_TypeInfo_var, (uint32_t)4));
		int32_t L_1 = __this->get_startSlot_1();
		int32_t L_2 = L_1;
		RuntimeObject * L_3 = Box(Int32_t4116043316_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_0);
		ArrayElementTypeCheck (L_0, L_3);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_3);
		ObjectU5BU5D_t1100384052* L_4 = L_0;
		int32_t L_5 = __this->get_endSlot_2();
		int32_t L_6 = ((int32_t)((int32_t)L_5-(int32_t)1));
		RuntimeObject * L_7 = Box(Int32_t4116043316_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_7);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_t1100384052* L_8 = L_4;
		Material_t2681358023 * L_9 = __this->get_material_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t350248726_il2cpp_TypeInfo_var);
		bool L_10 = Object_op_Equality_m3076063157(NULL /*static, unused*/, L_9, (Object_t350248726 *)NULL, /*hidden argument*/NULL);
		G_B1_0 = 2;
		G_B1_1 = L_8;
		G_B1_2 = L_8;
		G_B1_3 = _stringLiteral2205639819;
		if (!L_10)
		{
			G_B2_0 = 2;
			G_B2_1 = L_8;
			G_B2_2 = L_8;
			G_B2_3 = _stringLiteral2205639819;
			goto IL_0046;
		}
	}
	{
		G_B3_0 = _stringLiteral1418032621;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0051;
	}

IL_0046:
	{
		Material_t2681358023 * L_11 = __this->get_material_3();
		NullCheck(L_11);
		String_t* L_12 = Object_get_name_m3459674631(L_11, /*hidden argument*/NULL);
		G_B3_0 = L_12;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0051:
	{
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (RuntimeObject *)G_B3_0);
		ObjectU5BU5D_t1100384052* L_13 = G_B3_3;
		int32_t L_14 = __this->get_preActiveClippingSlotSource_5();
		int32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(Int32_t4116043316_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = String_Format_m345500613(NULL /*static, unused*/, G_B3_4, L_13, /*hidden argument*/NULL);
		return L_17;
	}
}
extern "C"  String_t* SubmeshInstruction_ToString_m1474118231_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	SubmeshInstruction_t3765917328 * _thisAdjusted = reinterpret_cast<SubmeshInstruction_t3765917328 *>(__this + 1);
	return SubmeshInstruction_ToString_m1474118231(_thisAdjusted, method);
}
// System.Void Spine.Unity.UpdateBonesDelegate::.ctor(System.Object,System.IntPtr)
extern "C"  void UpdateBonesDelegate__ctor_m3661072719 (UpdateBonesDelegate_t500272946 * __this, RuntimeObject * ___object0, IntPtr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1.get_m_value_0()));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Spine.Unity.UpdateBonesDelegate::Invoke(Spine.Unity.ISkeletonAnimation)
extern "C"  void UpdateBonesDelegate_Invoke_m835921997 (UpdateBonesDelegate_t500272946 * __this, RuntimeObject* ___animated0, const RuntimeMethod* method)
{
	if(__this->get_prev_9() != NULL)
	{
		UpdateBonesDelegate_Invoke_m835921997((UpdateBonesDelegate_t500272946 *)__this->get_prev_9(),___animated0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (RuntimeObject *, void* __this, RuntimeObject* ___animated0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___animated0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (void* __this, RuntimeObject* ___animated0, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___animated0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef void (*FunctionPointerType) (void* __this, const RuntimeMethod* method);
		((FunctionPointerType)__this->get_method_ptr_0())(___animated0,(RuntimeMethod*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult Spine.Unity.UpdateBonesDelegate::BeginInvoke(Spine.Unity.ISkeletonAnimation,System.AsyncCallback,System.Object)
extern "C"  RuntimeObject* UpdateBonesDelegate_BeginInvoke_m1843317248 (UpdateBonesDelegate_t500272946 * __this, RuntimeObject* ___animated0, AsyncCallback_t3972436406 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___animated0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void Spine.Unity.UpdateBonesDelegate::EndInvoke(System.IAsyncResult)
extern "C"  void UpdateBonesDelegate_EndInvoke_m923805062 (UpdateBonesDelegate_t500272946 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
// System.Void Spine.Unity.WaitForSpineAnimationComplete::.ctor(Spine.TrackEntry)
extern "C"  void WaitForSpineAnimationComplete__ctor_m3315824172 (WaitForSpineAnimationComplete_t694865537 * __this, TrackEntry_t4062297782 * ___trackEntry0, const RuntimeMethod* method)
{
	{
		Object__ctor_m1723520498(__this, /*hidden argument*/NULL);
		TrackEntry_t4062297782 * L_0 = ___trackEntry0;
		WaitForSpineAnimationComplete_SafeSubscribe_m3415404190(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spine.Unity.WaitForSpineAnimationComplete::HandleComplete(Spine.TrackEntry)
extern "C"  void WaitForSpineAnimationComplete_HandleComplete_m1383943572 (WaitForSpineAnimationComplete_t694865537 * __this, TrackEntry_t4062297782 * ___trackEntry0, const RuntimeMethod* method)
{
	{
		__this->set_m_WasFired_0((bool)1);
		return;
	}
}
// System.Void Spine.Unity.WaitForSpineAnimationComplete::SafeSubscribe(Spine.TrackEntry)
extern "C"  void WaitForSpineAnimationComplete_SafeSubscribe_m3415404190 (WaitForSpineAnimationComplete_t694865537 * __this, TrackEntry_t4062297782 * ___trackEntry0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaitForSpineAnimationComplete_SafeSubscribe_m3415404190_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackEntry_t4062297782 * L_0 = ___trackEntry0;
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t16286340_il2cpp_TypeInfo_var);
		Debug_LogWarning_m434037919(NULL /*static, unused*/, _stringLiteral2631168059, /*hidden argument*/NULL);
		__this->set_m_WasFired_0((bool)1);
		goto IL_002e;
	}

IL_001c:
	{
		TrackEntry_t4062297782 * L_1 = ___trackEntry0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)WaitForSpineAnimationComplete_HandleComplete_m1383943572_RuntimeMethod_var);
		TrackEntryDelegate_t1807657294 * L_3 = (TrackEntryDelegate_t1807657294 *)il2cpp_codegen_object_new(TrackEntryDelegate_t1807657294_il2cpp_TypeInfo_var);
		TrackEntryDelegate__ctor_m2315773476(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		TrackEntry_add_Complete_m1607620779(L_1, L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// Spine.Unity.WaitForSpineAnimationComplete Spine.Unity.WaitForSpineAnimationComplete::NowWaitFor(Spine.TrackEntry)
extern "C"  WaitForSpineAnimationComplete_t694865537 * WaitForSpineAnimationComplete_NowWaitFor_m2365033906 (WaitForSpineAnimationComplete_t694865537 * __this, TrackEntry_t4062297782 * ___trackEntry0, const RuntimeMethod* method)
{
	{
		TrackEntry_t4062297782 * L_0 = ___trackEntry0;
		WaitForSpineAnimationComplete_SafeSubscribe_m3415404190(__this, L_0, /*hidden argument*/NULL);
		return __this;
	}
}
// System.Boolean Spine.Unity.WaitForSpineAnimationComplete::System.Collections.IEnumerator.MoveNext()
extern "C"  bool WaitForSpineAnimationComplete_System_Collections_IEnumerator_MoveNext_m3327595576 (WaitForSpineAnimationComplete_t694865537 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaitForSpineAnimationComplete_System_Collections_IEnumerator_MoveNext_m3327595576_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_WasFired_0();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t2667876566_il2cpp_TypeInfo_var, __this);
		return (bool)0;
	}

IL_0013:
	{
		return (bool)1;
	}
}
// System.Void Spine.Unity.WaitForSpineAnimationComplete::System.Collections.IEnumerator.Reset()
extern "C"  void WaitForSpineAnimationComplete_System_Collections_IEnumerator_Reset_m3089077843 (WaitForSpineAnimationComplete_t694865537 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_WasFired_0((bool)0);
		return;
	}
}
// System.Object Spine.Unity.WaitForSpineAnimationComplete::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * WaitForSpineAnimationComplete_System_Collections_IEnumerator_get_Current_m1986177086 (WaitForSpineAnimationComplete_t694865537 * __this, const RuntimeMethod* method)
{
	{
		return NULL;
	}
}
// System.Void Spine.Unity.WaitForSpineEvent::.ctor(Spine.AnimationState,Spine.EventData,System.Boolean)
extern "C"  void WaitForSpineEvent__ctor_m2712235303 (WaitForSpineEvent_t3387552986 * __this, AnimationState_t2858486651 * ___state0, EventData_t295981108 * ___eventDataReference1, bool ___unsubscribeAfterFiring2, const RuntimeMethod* method)
{
	{
		Object__ctor_m1723520498(__this, /*hidden argument*/NULL);
		AnimationState_t2858486651 * L_0 = ___state0;
		EventData_t295981108 * L_1 = ___eventDataReference1;
		bool L_2 = ___unsubscribeAfterFiring2;
		WaitForSpineEvent_Subscribe_m1024680374(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spine.Unity.WaitForSpineEvent::.ctor(Spine.Unity.SkeletonAnimation,Spine.EventData,System.Boolean)
extern "C"  void WaitForSpineEvent__ctor_m3549171829 (WaitForSpineEvent_t3387552986 * __this, SkeletonAnimation_t2012766265 * ___skeletonAnimation0, EventData_t295981108 * ___eventDataReference1, bool ___unsubscribeAfterFiring2, const RuntimeMethod* method)
{
	{
		Object__ctor_m1723520498(__this, /*hidden argument*/NULL);
		SkeletonAnimation_t2012766265 * L_0 = ___skeletonAnimation0;
		NullCheck(L_0);
		AnimationState_t2858486651 * L_1 = L_0->get_state_31();
		EventData_t295981108 * L_2 = ___eventDataReference1;
		bool L_3 = ___unsubscribeAfterFiring2;
		WaitForSpineEvent_Subscribe_m1024680374(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spine.Unity.WaitForSpineEvent::.ctor(Spine.AnimationState,System.String,System.Boolean)
extern "C"  void WaitForSpineEvent__ctor_m2583349641 (WaitForSpineEvent_t3387552986 * __this, AnimationState_t2858486651 * ___state0, String_t* ___eventName1, bool ___unsubscribeAfterFiring2, const RuntimeMethod* method)
{
	{
		Object__ctor_m1723520498(__this, /*hidden argument*/NULL);
		AnimationState_t2858486651 * L_0 = ___state0;
		String_t* L_1 = ___eventName1;
		bool L_2 = ___unsubscribeAfterFiring2;
		WaitForSpineEvent_SubscribeByName_m2257425455(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spine.Unity.WaitForSpineEvent::.ctor(Spine.Unity.SkeletonAnimation,System.String,System.Boolean)
extern "C"  void WaitForSpineEvent__ctor_m3602682043 (WaitForSpineEvent_t3387552986 * __this, SkeletonAnimation_t2012766265 * ___skeletonAnimation0, String_t* ___eventName1, bool ___unsubscribeAfterFiring2, const RuntimeMethod* method)
{
	{
		Object__ctor_m1723520498(__this, /*hidden argument*/NULL);
		SkeletonAnimation_t2012766265 * L_0 = ___skeletonAnimation0;
		NullCheck(L_0);
		AnimationState_t2858486651 * L_1 = L_0->get_state_31();
		String_t* L_2 = ___eventName1;
		bool L_3 = ___unsubscribeAfterFiring2;
		WaitForSpineEvent_SubscribeByName_m2257425455(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spine.Unity.WaitForSpineEvent::Subscribe(Spine.AnimationState,Spine.EventData,System.Boolean)
extern "C"  void WaitForSpineEvent_Subscribe_m1024680374 (WaitForSpineEvent_t3387552986 * __this, AnimationState_t2858486651 * ___state0, EventData_t295981108 * ___eventDataReference1, bool ___unsubscribe2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaitForSpineEvent_Subscribe_m1024680374_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AnimationState_t2858486651 * L_0 = ___state0;
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t16286340_il2cpp_TypeInfo_var);
		Debug_LogWarning_m434037919(NULL /*static, unused*/, _stringLiteral2001166017, /*hidden argument*/NULL);
		__this->set_m_WasFired_3((bool)1);
		return;
	}

IL_0018:
	{
		EventData_t295981108 * L_1 = ___eventDataReference1;
		if (L_1)
		{
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t16286340_il2cpp_TypeInfo_var);
		Debug_LogWarning_m434037919(NULL /*static, unused*/, _stringLiteral350640354, /*hidden argument*/NULL);
		__this->set_m_WasFired_3((bool)1);
		return;
	}

IL_0030:
	{
		AnimationState_t2858486651 * L_2 = ___state0;
		__this->set_m_AnimationState_2(L_2);
		EventData_t295981108 * L_3 = ___eventDataReference1;
		__this->set_m_TargetEvent_0(L_3);
		AnimationState_t2858486651 * L_4 = ___state0;
		IntPtr_t L_5;
		L_5.set_m_value_0((void*)(void*)WaitForSpineEvent_HandleAnimationStateEvent_m3129679492_RuntimeMethod_var);
		TrackEntryEventDelegate_t471240152 * L_6 = (TrackEntryEventDelegate_t471240152 *)il2cpp_codegen_object_new(TrackEntryEventDelegate_t471240152_il2cpp_TypeInfo_var);
		TrackEntryEventDelegate__ctor_m2180400158(L_6, __this, L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		AnimationState_add_Event_m4238647668(L_4, L_6, /*hidden argument*/NULL);
		bool L_7 = ___unsubscribe2;
		__this->set_m_unsubscribeAfterFiring_4(L_7);
		return;
	}
}
// System.Void Spine.Unity.WaitForSpineEvent::SubscribeByName(Spine.AnimationState,System.String,System.Boolean)
extern "C"  void WaitForSpineEvent_SubscribeByName_m2257425455 (WaitForSpineEvent_t3387552986 * __this, AnimationState_t2858486651 * ___state0, String_t* ___eventName1, bool ___unsubscribe2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaitForSpineEvent_SubscribeByName_m2257425455_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AnimationState_t2858486651 * L_0 = ___state0;
		if (L_0)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t16286340_il2cpp_TypeInfo_var);
		Debug_LogWarning_m434037919(NULL /*static, unused*/, _stringLiteral2001166017, /*hidden argument*/NULL);
		__this->set_m_WasFired_3((bool)1);
		return;
	}

IL_0018:
	{
		String_t* L_1 = ___eventName1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_2 = String_IsNullOrEmpty_m1700356867(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0035;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t16286340_il2cpp_TypeInfo_var);
		Debug_LogWarning_m434037919(NULL /*static, unused*/, _stringLiteral2937246701, /*hidden argument*/NULL);
		__this->set_m_WasFired_3((bool)1);
		return;
	}

IL_0035:
	{
		AnimationState_t2858486651 * L_3 = ___state0;
		__this->set_m_AnimationState_2(L_3);
		String_t* L_4 = ___eventName1;
		__this->set_m_EventName_1(L_4);
		AnimationState_t2858486651 * L_5 = ___state0;
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)WaitForSpineEvent_HandleAnimationStateEventByName_m504300077_RuntimeMethod_var);
		TrackEntryEventDelegate_t471240152 * L_7 = (TrackEntryEventDelegate_t471240152 *)il2cpp_codegen_object_new(TrackEntryEventDelegate_t471240152_il2cpp_TypeInfo_var);
		TrackEntryEventDelegate__ctor_m2180400158(L_7, __this, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		AnimationState_add_Event_m4238647668(L_5, L_7, /*hidden argument*/NULL);
		bool L_8 = ___unsubscribe2;
		__this->set_m_unsubscribeAfterFiring_4(L_8);
		return;
	}
}
// System.Void Spine.Unity.WaitForSpineEvent::HandleAnimationStateEventByName(Spine.TrackEntry,Spine.Event)
extern "C"  void WaitForSpineEvent_HandleAnimationStateEventByName_m504300077 (WaitForSpineEvent_t3387552986 * __this, TrackEntry_t4062297782 * ___trackEntry0, Event_t27715519 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaitForSpineEvent_HandleAnimationStateEventByName_m504300077_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_WasFired_3();
		Event_t27715519 * L_1 = ___e1;
		NullCheck(L_1);
		EventData_t295981108 * L_2 = Event_get_Data_m2819791637(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = EventData_get_Name_m122311786(L_2, /*hidden argument*/NULL);
		String_t* L_4 = __this->get_m_EventName_1();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m1348988297(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		__this->set_m_WasFired_3((bool)((int32_t)((int32_t)L_0|(int32_t)L_5)));
		bool L_6 = __this->get_m_WasFired_3();
		if (!L_6)
		{
			goto IL_0050;
		}
	}
	{
		bool L_7 = __this->get_m_unsubscribeAfterFiring_4();
		if (!L_7)
		{
			goto IL_0050;
		}
	}
	{
		AnimationState_t2858486651 * L_8 = __this->get_m_AnimationState_2();
		IntPtr_t L_9;
		L_9.set_m_value_0((void*)(void*)WaitForSpineEvent_HandleAnimationStateEventByName_m504300077_RuntimeMethod_var);
		TrackEntryEventDelegate_t471240152 * L_10 = (TrackEntryEventDelegate_t471240152 *)il2cpp_codegen_object_new(TrackEntryEventDelegate_t471240152_il2cpp_TypeInfo_var);
		TrackEntryEventDelegate__ctor_m2180400158(L_10, __this, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		AnimationState_remove_Event_m3240949950(L_8, L_10, /*hidden argument*/NULL);
	}

IL_0050:
	{
		return;
	}
}
// System.Void Spine.Unity.WaitForSpineEvent::HandleAnimationStateEvent(Spine.TrackEntry,Spine.Event)
extern "C"  void WaitForSpineEvent_HandleAnimationStateEvent_m3129679492 (WaitForSpineEvent_t3387552986 * __this, TrackEntry_t4062297782 * ___trackEntry0, Event_t27715519 * ___e1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaitForSpineEvent_HandleAnimationStateEvent_m3129679492_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_WasFired_3();
		Event_t27715519 * L_1 = ___e1;
		NullCheck(L_1);
		EventData_t295981108 * L_2 = Event_get_Data_m2819791637(L_1, /*hidden argument*/NULL);
		EventData_t295981108 * L_3 = __this->get_m_TargetEvent_0();
		__this->set_m_WasFired_3((bool)((int32_t)((int32_t)L_0|(int32_t)((((RuntimeObject*)(EventData_t295981108 *)L_2) == ((RuntimeObject*)(EventData_t295981108 *)L_3))? 1 : 0))));
		bool L_4 = __this->get_m_WasFired_3();
		if (!L_4)
		{
			goto IL_0048;
		}
	}
	{
		bool L_5 = __this->get_m_unsubscribeAfterFiring_4();
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		AnimationState_t2858486651 * L_6 = __this->get_m_AnimationState_2();
		IntPtr_t L_7;
		L_7.set_m_value_0((void*)(void*)WaitForSpineEvent_HandleAnimationStateEvent_m3129679492_RuntimeMethod_var);
		TrackEntryEventDelegate_t471240152 * L_8 = (TrackEntryEventDelegate_t471240152 *)il2cpp_codegen_object_new(TrackEntryEventDelegate_t471240152_il2cpp_TypeInfo_var);
		TrackEntryEventDelegate__ctor_m2180400158(L_8, __this, L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		AnimationState_remove_Event_m3240949950(L_6, L_8, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
// System.Boolean Spine.Unity.WaitForSpineEvent::get_WillUnsubscribeAfterFiring()
extern "C"  bool WaitForSpineEvent_get_WillUnsubscribeAfterFiring_m1538709046 (WaitForSpineEvent_t3387552986 * __this, const RuntimeMethod* method)
{
	{
		bool L_0 = __this->get_m_unsubscribeAfterFiring_4();
		return L_0;
	}
}
// System.Void Spine.Unity.WaitForSpineEvent::set_WillUnsubscribeAfterFiring(System.Boolean)
extern "C"  void WaitForSpineEvent_set_WillUnsubscribeAfterFiring_m525421781 (WaitForSpineEvent_t3387552986 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_unsubscribeAfterFiring_4(L_0);
		return;
	}
}
// Spine.Unity.WaitForSpineEvent Spine.Unity.WaitForSpineEvent::NowWaitFor(Spine.AnimationState,Spine.EventData,System.Boolean)
extern "C"  WaitForSpineEvent_t3387552986 * WaitForSpineEvent_NowWaitFor_m1383645710 (WaitForSpineEvent_t3387552986 * __this, AnimationState_t2858486651 * ___state0, EventData_t295981108 * ___eventDataReference1, bool ___unsubscribeAfterFiring2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaitForSpineEvent_NowWaitFor_m1383645710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t2667876566_il2cpp_TypeInfo_var, __this);
		AnimationState_t2858486651 * L_0 = ___state0;
		WaitForSpineEvent_Clear_m1777236221(__this, L_0, /*hidden argument*/NULL);
		AnimationState_t2858486651 * L_1 = ___state0;
		EventData_t295981108 * L_2 = ___eventDataReference1;
		bool L_3 = ___unsubscribeAfterFiring2;
		WaitForSpineEvent_Subscribe_m1024680374(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return __this;
	}
}
// Spine.Unity.WaitForSpineEvent Spine.Unity.WaitForSpineEvent::NowWaitFor(Spine.AnimationState,System.String,System.Boolean)
extern "C"  WaitForSpineEvent_t3387552986 * WaitForSpineEvent_NowWaitFor_m1799525501 (WaitForSpineEvent_t3387552986 * __this, AnimationState_t2858486651 * ___state0, String_t* ___eventName1, bool ___unsubscribeAfterFiring2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaitForSpineEvent_NowWaitFor_m1799525501_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t2667876566_il2cpp_TypeInfo_var, __this);
		AnimationState_t2858486651 * L_0 = ___state0;
		WaitForSpineEvent_Clear_m1777236221(__this, L_0, /*hidden argument*/NULL);
		AnimationState_t2858486651 * L_1 = ___state0;
		String_t* L_2 = ___eventName1;
		bool L_3 = ___unsubscribeAfterFiring2;
		WaitForSpineEvent_SubscribeByName_m2257425455(__this, L_1, L_2, L_3, /*hidden argument*/NULL);
		return __this;
	}
}
// System.Void Spine.Unity.WaitForSpineEvent::Clear(Spine.AnimationState)
extern "C"  void WaitForSpineEvent_Clear_m1777236221 (WaitForSpineEvent_t3387552986 * __this, AnimationState_t2858486651 * ___state0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaitForSpineEvent_Clear_m1777236221_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		AnimationState_t2858486651 * L_0 = ___state0;
		IntPtr_t L_1;
		L_1.set_m_value_0((void*)(void*)WaitForSpineEvent_HandleAnimationStateEvent_m3129679492_RuntimeMethod_var);
		TrackEntryEventDelegate_t471240152 * L_2 = (TrackEntryEventDelegate_t471240152 *)il2cpp_codegen_object_new(TrackEntryEventDelegate_t471240152_il2cpp_TypeInfo_var);
		TrackEntryEventDelegate__ctor_m2180400158(L_2, __this, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		AnimationState_remove_Event_m3240949950(L_0, L_2, /*hidden argument*/NULL);
		AnimationState_t2858486651 * L_3 = ___state0;
		IntPtr_t L_4;
		L_4.set_m_value_0((void*)(void*)WaitForSpineEvent_HandleAnimationStateEventByName_m504300077_RuntimeMethod_var);
		TrackEntryEventDelegate_t471240152 * L_5 = (TrackEntryEventDelegate_t471240152 *)il2cpp_codegen_object_new(TrackEntryEventDelegate_t471240152_il2cpp_TypeInfo_var);
		TrackEntryEventDelegate__ctor_m2180400158(L_5, __this, L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		AnimationState_remove_Event_m3240949950(L_3, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Spine.Unity.WaitForSpineEvent::System.Collections.IEnumerator.MoveNext()
extern "C"  bool WaitForSpineEvent_System_Collections_IEnumerator_MoveNext_m3660352932 (WaitForSpineEvent_t3387552986 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaitForSpineEvent_System_Collections_IEnumerator_MoveNext_m3660352932_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_WasFired_3();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t2667876566_il2cpp_TypeInfo_var, __this);
		return (bool)0;
	}

IL_0013:
	{
		return (bool)1;
	}
}
// System.Void Spine.Unity.WaitForSpineEvent::System.Collections.IEnumerator.Reset()
extern "C"  void WaitForSpineEvent_System_Collections_IEnumerator_Reset_m3912251351 (WaitForSpineEvent_t3387552986 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_WasFired_3((bool)0);
		return;
	}
}
// System.Object Spine.Unity.WaitForSpineEvent::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * WaitForSpineEvent_System_Collections_IEnumerator_get_Current_m1335522319 (WaitForSpineEvent_t3387552986 * __this, const RuntimeMethod* method)
{
	{
		return NULL;
	}
}
// System.Void Spine.Unity.WaitForSpineTrackEntryEnd::.ctor(Spine.TrackEntry)
extern "C"  void WaitForSpineTrackEntryEnd__ctor_m2472024536 (WaitForSpineTrackEntryEnd_t2837745715 * __this, TrackEntry_t4062297782 * ___trackEntry0, const RuntimeMethod* method)
{
	{
		Object__ctor_m1723520498(__this, /*hidden argument*/NULL);
		TrackEntry_t4062297782 * L_0 = ___trackEntry0;
		WaitForSpineTrackEntryEnd_SafeSubscribe_m952987264(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spine.Unity.WaitForSpineTrackEntryEnd::HandleEnd(Spine.TrackEntry)
extern "C"  void WaitForSpineTrackEntryEnd_HandleEnd_m3109162439 (WaitForSpineTrackEntryEnd_t2837745715 * __this, TrackEntry_t4062297782 * ___trackEntry0, const RuntimeMethod* method)
{
	{
		__this->set_m_WasFired_0((bool)1);
		return;
	}
}
// System.Void Spine.Unity.WaitForSpineTrackEntryEnd::SafeSubscribe(Spine.TrackEntry)
extern "C"  void WaitForSpineTrackEntryEnd_SafeSubscribe_m952987264 (WaitForSpineTrackEntryEnd_t2837745715 * __this, TrackEntry_t4062297782 * ___trackEntry0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaitForSpineTrackEntryEnd_SafeSubscribe_m952987264_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		TrackEntry_t4062297782 * L_0 = ___trackEntry0;
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t16286340_il2cpp_TypeInfo_var);
		Debug_LogWarning_m434037919(NULL /*static, unused*/, _stringLiteral2631168059, /*hidden argument*/NULL);
		__this->set_m_WasFired_0((bool)1);
		goto IL_002e;
	}

IL_001c:
	{
		TrackEntry_t4062297782 * L_1 = ___trackEntry0;
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)WaitForSpineTrackEntryEnd_HandleEnd_m3109162439_RuntimeMethod_var);
		TrackEntryDelegate_t1807657294 * L_3 = (TrackEntryDelegate_t1807657294 *)il2cpp_codegen_object_new(TrackEntryDelegate_t1807657294_il2cpp_TypeInfo_var);
		TrackEntryDelegate__ctor_m2315773476(L_3, __this, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		TrackEntry_add_End_m941712940(L_1, L_3, /*hidden argument*/NULL);
	}

IL_002e:
	{
		return;
	}
}
// Spine.Unity.WaitForSpineTrackEntryEnd Spine.Unity.WaitForSpineTrackEntryEnd::NowWaitFor(Spine.TrackEntry)
extern "C"  WaitForSpineTrackEntryEnd_t2837745715 * WaitForSpineTrackEntryEnd_NowWaitFor_m3049063314 (WaitForSpineTrackEntryEnd_t2837745715 * __this, TrackEntry_t4062297782 * ___trackEntry0, const RuntimeMethod* method)
{
	{
		TrackEntry_t4062297782 * L_0 = ___trackEntry0;
		WaitForSpineTrackEntryEnd_SafeSubscribe_m952987264(__this, L_0, /*hidden argument*/NULL);
		return __this;
	}
}
// System.Boolean Spine.Unity.WaitForSpineTrackEntryEnd::System.Collections.IEnumerator.MoveNext()
extern "C"  bool WaitForSpineTrackEntryEnd_System_Collections_IEnumerator_MoveNext_m1365914462 (WaitForSpineTrackEntryEnd_t2837745715 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaitForSpineTrackEntryEnd_System_Collections_IEnumerator_MoveNext_m1365914462_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_m_WasFired_0();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		InterfaceActionInvoker0::Invoke(2 /* System.Void System.Collections.IEnumerator::Reset() */, IEnumerator_t2667876566_il2cpp_TypeInfo_var, __this);
		return (bool)0;
	}

IL_0013:
	{
		return (bool)1;
	}
}
// System.Void Spine.Unity.WaitForSpineTrackEntryEnd::System.Collections.IEnumerator.Reset()
extern "C"  void WaitForSpineTrackEntryEnd_System_Collections_IEnumerator_Reset_m722450313 (WaitForSpineTrackEntryEnd_t2837745715 * __this, const RuntimeMethod* method)
{
	{
		__this->set_m_WasFired_0((bool)0);
		return;
	}
}
// System.Object Spine.Unity.WaitForSpineTrackEntryEnd::System.Collections.IEnumerator.get_Current()
extern "C"  RuntimeObject * WaitForSpineTrackEntryEnd_System_Collections_IEnumerator_get_Current_m3878618855 (WaitForSpineTrackEntryEnd_t2837745715 * __this, const RuntimeMethod* method)
{
	{
		return NULL;
	}
}
// System.Void Spine.VertexAttachment::.ctor(System.String)
extern "C"  void VertexAttachment__ctor_m2208203245 (VertexAttachment_t1776545046 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VertexAttachment__ctor_m2208203245_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t2572292308 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t2572292308 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		String_t* L_0 = ___name0;
		Attachment__ctor_m3611922211(__this, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(VertexAttachment_t1776545046_il2cpp_TypeInfo_var);
		RuntimeObject * L_1 = ((VertexAttachment_t1776545046_StaticFields*)il2cpp_codegen_static_fields_for(VertexAttachment_t1776545046_il2cpp_TypeInfo_var))->get_nextIdLock_2();
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m3163704715(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0013:
	try
	{ // begin try (depth: 1)
		IL2CPP_RUNTIME_CLASS_INIT(VertexAttachment_t1776545046_il2cpp_TypeInfo_var);
		int32_t L_3 = ((VertexAttachment_t1776545046_StaticFields*)il2cpp_codegen_static_fields_for(VertexAttachment_t1776545046_il2cpp_TypeInfo_var))->get_nextID_1();
		int32_t L_4 = L_3;
		((VertexAttachment_t1776545046_StaticFields*)il2cpp_codegen_static_fields_for(VertexAttachment_t1776545046_il2cpp_TypeInfo_var))->set_nextID_1(((int32_t)((int32_t)L_4+(int32_t)1)));
		__this->set_id_3(((int32_t)((int32_t)((int32_t)((int32_t)L_4&(int32_t)((int32_t)65535)))<<(int32_t)((int32_t)11))));
		IL2CPP_LEAVE(0x3B, FINALLY_0034);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t2572292308 *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		RuntimeObject * L_5 = V_0;
		Monitor_Exit_m366607352(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(52)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t2572292308 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Int32 Spine.VertexAttachment::get_Id()
extern "C"  int32_t VertexAttachment_get_Id_m1367375024 (VertexAttachment_t1776545046 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_id_3();
		return L_0;
	}
}
// System.Int32[] Spine.VertexAttachment::get_Bones()
extern "C"  Int32U5BU5D_t281224829* VertexAttachment_get_Bones_m3023111343 (VertexAttachment_t1776545046 * __this, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t281224829* L_0 = __this->get_bones_4();
		return L_0;
	}
}
// System.Void Spine.VertexAttachment::set_Bones(System.Int32[])
extern "C"  void VertexAttachment_set_Bones_m2969082317 (VertexAttachment_t1776545046 * __this, Int32U5BU5D_t281224829* ___value0, const RuntimeMethod* method)
{
	{
		Int32U5BU5D_t281224829* L_0 = ___value0;
		__this->set_bones_4(L_0);
		return;
	}
}
// System.Single[] Spine.VertexAttachment::get_Vertices()
extern "C"  SingleU5BU5D_t3989243465* VertexAttachment_get_Vertices_m60520502 (VertexAttachment_t1776545046 * __this, const RuntimeMethod* method)
{
	{
		SingleU5BU5D_t3989243465* L_0 = __this->get_vertices_5();
		return L_0;
	}
}
// System.Void Spine.VertexAttachment::set_Vertices(System.Single[])
extern "C"  void VertexAttachment_set_Vertices_m2796350129 (VertexAttachment_t1776545046 * __this, SingleU5BU5D_t3989243465* ___value0, const RuntimeMethod* method)
{
	{
		SingleU5BU5D_t3989243465* L_0 = ___value0;
		__this->set_vertices_5(L_0);
		return;
	}
}
// System.Int32 Spine.VertexAttachment::get_WorldVerticesLength()
extern "C"  int32_t VertexAttachment_get_WorldVerticesLength_m1725546651 (VertexAttachment_t1776545046 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_worldVerticesLength_6();
		return L_0;
	}
}
// System.Void Spine.VertexAttachment::set_WorldVerticesLength(System.Int32)
extern "C"  void VertexAttachment_set_WorldVerticesLength_m15935589 (VertexAttachment_t1776545046 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_worldVerticesLength_6(L_0);
		return;
	}
}
// System.Void Spine.VertexAttachment::ComputeWorldVertices(Spine.Slot,System.Single[])
extern "C"  void VertexAttachment_ComputeWorldVertices_m3589097420 (VertexAttachment_t1776545046 * __this, Slot_t1804116343 * ___slot0, SingleU5BU5D_t3989243465* ___worldVertices1, const RuntimeMethod* method)
{
	{
		Slot_t1804116343 * L_0 = ___slot0;
		int32_t L_1 = __this->get_worldVerticesLength_6();
		SingleU5BU5D_t3989243465* L_2 = ___worldVertices1;
		VertexAttachment_ComputeWorldVertices_m3306759128(__this, L_0, 0, L_1, L_2, 0, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Spine.VertexAttachment::ComputeWorldVertices(Spine.Slot,System.Int32,System.Int32,System.Single[],System.Int32,System.Int32)
extern "C"  void VertexAttachment_ComputeWorldVertices_m3306759128 (VertexAttachment_t1776545046 * __this, Slot_t1804116343 * ___slot0, int32_t ___start1, int32_t ___count2, SingleU5BU5D_t3989243465* ___worldVertices3, int32_t ___offset4, int32_t ___stride5, const RuntimeMethod* method)
{
	Skeleton_t1045203634 * V_0 = NULL;
	ExposedList_1_t3384296343 * V_1 = NULL;
	SingleU5BU5D_t3989243465* V_2 = NULL;
	Int32U5BU5D_t281224829* V_3 = NULL;
	Bone_t1763862836 * V_4 = NULL;
	float V_5 = 0.0f;
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float V_9 = 0.0f;
	float V_10 = 0.0f;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	BoneU5BU5D_t600500093* V_19 = NULL;
	int32_t V_20 = 0;
	int32_t V_21 = 0;
	float V_22 = 0.0f;
	float V_23 = 0.0f;
	int32_t V_24 = 0;
	Bone_t1763862836 * V_25 = NULL;
	float V_26 = 0.0f;
	float V_27 = 0.0f;
	float V_28 = 0.0f;
	SingleU5BU5D_t3989243465* V_29 = NULL;
	int32_t V_30 = 0;
	int32_t V_31 = 0;
	int32_t V_32 = 0;
	float V_33 = 0.0f;
	float V_34 = 0.0f;
	int32_t V_35 = 0;
	Bone_t1763862836 * V_36 = NULL;
	float V_37 = 0.0f;
	float V_38 = 0.0f;
	float V_39 = 0.0f;
	{
		int32_t L_0 = ___offset4;
		int32_t L_1 = ___count2;
		int32_t L_2 = ___stride5;
		___count2 = ((int32_t)((int32_t)L_0+(int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_1>>(int32_t)1))*(int32_t)L_2))));
		Slot_t1804116343 * L_3 = ___slot0;
		NullCheck(L_3);
		Bone_t1763862836 * L_4 = L_3->get_bone_1();
		NullCheck(L_4);
		Skeleton_t1045203634 * L_5 = L_4->get_skeleton_2();
		V_0 = L_5;
		Slot_t1804116343 * L_6 = ___slot0;
		NullCheck(L_6);
		ExposedList_1_t3384296343 * L_7 = L_6->get_attachmentVertices_12();
		V_1 = L_7;
		SingleU5BU5D_t3989243465* L_8 = __this->get_vertices_5();
		V_2 = L_8;
		Int32U5BU5D_t281224829* L_9 = __this->get_bones_4();
		V_3 = L_9;
		Int32U5BU5D_t281224829* L_10 = V_3;
		if (L_10)
		{
			goto IL_00db;
		}
	}
	{
		ExposedList_1_t3384296343 * L_11 = V_1;
		NullCheck(L_11);
		int32_t L_12 = L_11->get_Count_1();
		if ((((int32_t)L_12) <= ((int32_t)0)))
		{
			goto IL_0045;
		}
	}
	{
		ExposedList_1_t3384296343 * L_13 = V_1;
		NullCheck(L_13);
		SingleU5BU5D_t3989243465* L_14 = L_13->get_Items_0();
		V_2 = L_14;
	}

IL_0045:
	{
		Slot_t1804116343 * L_15 = ___slot0;
		NullCheck(L_15);
		Bone_t1763862836 * L_16 = L_15->get_bone_1();
		V_4 = L_16;
		Bone_t1763862836 * L_17 = V_4;
		NullCheck(L_17);
		float L_18 = L_17->get_worldX_22();
		V_5 = L_18;
		Bone_t1763862836 * L_19 = V_4;
		NullCheck(L_19);
		float L_20 = L_19->get_worldY_25();
		V_6 = L_20;
		Bone_t1763862836 * L_21 = V_4;
		NullCheck(L_21);
		float L_22 = L_21->get_a_20();
		V_7 = L_22;
		Bone_t1763862836 * L_23 = V_4;
		NullCheck(L_23);
		float L_24 = L_23->get_b_21();
		V_8 = L_24;
		Bone_t1763862836 * L_25 = V_4;
		NullCheck(L_25);
		float L_26 = L_25->get_c_23();
		V_9 = L_26;
		Bone_t1763862836 * L_27 = V_4;
		NullCheck(L_27);
		float L_28 = L_27->get_d_24();
		V_10 = L_28;
		int32_t L_29 = ___start1;
		V_11 = L_29;
		int32_t L_30 = ___offset4;
		V_12 = L_30;
		goto IL_00d2;
	}

IL_008f:
	{
		SingleU5BU5D_t3989243465* L_31 = V_2;
		int32_t L_32 = V_11;
		NullCheck(L_31);
		int32_t L_33 = L_32;
		float L_34 = (L_31)->GetAt(static_cast<il2cpp_array_size_t>(L_33));
		V_13 = L_34;
		SingleU5BU5D_t3989243465* L_35 = V_2;
		int32_t L_36 = V_11;
		NullCheck(L_35);
		int32_t L_37 = ((int32_t)((int32_t)L_36+(int32_t)1));
		float L_38 = (L_35)->GetAt(static_cast<il2cpp_array_size_t>(L_37));
		V_14 = L_38;
		SingleU5BU5D_t3989243465* L_39 = ___worldVertices3;
		int32_t L_40 = V_12;
		float L_41 = V_13;
		float L_42 = V_7;
		float L_43 = V_14;
		float L_44 = V_8;
		float L_45 = V_5;
		NullCheck(L_39);
		(L_39)->SetAt(static_cast<il2cpp_array_size_t>(L_40), (float)((float)((float)((float)((float)((float)((float)L_41*(float)L_42))+(float)((float)((float)L_43*(float)L_44))))+(float)L_45)));
		SingleU5BU5D_t3989243465* L_46 = ___worldVertices3;
		int32_t L_47 = V_12;
		float L_48 = V_13;
		float L_49 = V_9;
		float L_50 = V_14;
		float L_51 = V_10;
		float L_52 = V_6;
		NullCheck(L_46);
		(L_46)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_47+(int32_t)1))), (float)((float)((float)((float)((float)((float)((float)L_48*(float)L_49))+(float)((float)((float)L_50*(float)L_51))))+(float)L_52)));
		int32_t L_53 = V_11;
		V_11 = ((int32_t)((int32_t)L_53+(int32_t)2));
		int32_t L_54 = V_12;
		int32_t L_55 = ___stride5;
		V_12 = ((int32_t)((int32_t)L_54+(int32_t)L_55));
	}

IL_00d2:
	{
		int32_t L_56 = V_12;
		int32_t L_57 = ___count2;
		if ((((int32_t)L_56) < ((int32_t)L_57)))
		{
			goto IL_008f;
		}
	}
	{
		return;
	}

IL_00db:
	{
		V_15 = 0;
		V_16 = 0;
		V_17 = 0;
		goto IL_0105;
	}

IL_00e9:
	{
		Int32U5BU5D_t281224829* L_58 = V_3;
		int32_t L_59 = V_15;
		NullCheck(L_58);
		int32_t L_60 = L_59;
		int32_t L_61 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		V_18 = L_61;
		int32_t L_62 = V_15;
		int32_t L_63 = V_18;
		V_15 = ((int32_t)((int32_t)L_62+(int32_t)((int32_t)((int32_t)L_63+(int32_t)1))));
		int32_t L_64 = V_16;
		int32_t L_65 = V_18;
		V_16 = ((int32_t)((int32_t)L_64+(int32_t)L_65));
		int32_t L_66 = V_17;
		V_17 = ((int32_t)((int32_t)L_66+(int32_t)2));
	}

IL_0105:
	{
		int32_t L_67 = V_17;
		int32_t L_68 = ___start1;
		if ((((int32_t)L_67) < ((int32_t)L_68)))
		{
			goto IL_00e9;
		}
	}
	{
		Skeleton_t1045203634 * L_69 = V_0;
		NullCheck(L_69);
		ExposedList_1_t3613858675 * L_70 = L_69->get_bones_1();
		NullCheck(L_70);
		BoneU5BU5D_t600500093* L_71 = L_70->get_Items_0();
		V_19 = L_71;
		ExposedList_1_t3384296343 * L_72 = V_1;
		NullCheck(L_72);
		int32_t L_73 = L_72->get_Count_1();
		if (L_73)
		{
			goto IL_01fb;
		}
	}
	{
		int32_t L_74 = ___offset4;
		V_20 = L_74;
		int32_t L_75 = V_16;
		V_21 = ((int32_t)((int32_t)L_75*(int32_t)3));
		goto IL_01ee;
	}

IL_0134:
	{
		V_22 = (0.0f);
		V_23 = (0.0f);
		Int32U5BU5D_t281224829* L_76 = V_3;
		int32_t L_77 = V_15;
		int32_t L_78 = L_77;
		V_15 = ((int32_t)((int32_t)L_78+(int32_t)1));
		NullCheck(L_76);
		int32_t L_79 = L_78;
		int32_t L_80 = (L_76)->GetAt(static_cast<il2cpp_array_size_t>(L_79));
		V_24 = L_80;
		int32_t L_81 = V_24;
		int32_t L_82 = V_15;
		V_24 = ((int32_t)((int32_t)L_81+(int32_t)L_82));
		goto IL_01ce;
	}

IL_0159:
	{
		BoneU5BU5D_t600500093* L_83 = V_19;
		Int32U5BU5D_t281224829* L_84 = V_3;
		int32_t L_85 = V_15;
		NullCheck(L_84);
		int32_t L_86 = L_85;
		int32_t L_87 = (L_84)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		NullCheck(L_83);
		int32_t L_88 = L_87;
		Bone_t1763862836 * L_89 = (L_83)->GetAt(static_cast<il2cpp_array_size_t>(L_88));
		V_25 = L_89;
		SingleU5BU5D_t3989243465* L_90 = V_2;
		int32_t L_91 = V_21;
		NullCheck(L_90);
		int32_t L_92 = L_91;
		float L_93 = (L_90)->GetAt(static_cast<il2cpp_array_size_t>(L_92));
		V_26 = L_93;
		SingleU5BU5D_t3989243465* L_94 = V_2;
		int32_t L_95 = V_21;
		NullCheck(L_94);
		int32_t L_96 = ((int32_t)((int32_t)L_95+(int32_t)1));
		float L_97 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		V_27 = L_97;
		SingleU5BU5D_t3989243465* L_98 = V_2;
		int32_t L_99 = V_21;
		NullCheck(L_98);
		int32_t L_100 = ((int32_t)((int32_t)L_99+(int32_t)2));
		float L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		V_28 = L_101;
		float L_102 = V_22;
		float L_103 = V_26;
		Bone_t1763862836 * L_104 = V_25;
		NullCheck(L_104);
		float L_105 = L_104->get_a_20();
		float L_106 = V_27;
		Bone_t1763862836 * L_107 = V_25;
		NullCheck(L_107);
		float L_108 = L_107->get_b_21();
		Bone_t1763862836 * L_109 = V_25;
		NullCheck(L_109);
		float L_110 = L_109->get_worldX_22();
		float L_111 = V_28;
		V_22 = ((float)((float)L_102+(float)((float)((float)((float)((float)((float)((float)((float)((float)L_103*(float)L_105))+(float)((float)((float)L_106*(float)L_108))))+(float)L_110))*(float)L_111))));
		float L_112 = V_23;
		float L_113 = V_26;
		Bone_t1763862836 * L_114 = V_25;
		NullCheck(L_114);
		float L_115 = L_114->get_c_23();
		float L_116 = V_27;
		Bone_t1763862836 * L_117 = V_25;
		NullCheck(L_117);
		float L_118 = L_117->get_d_24();
		Bone_t1763862836 * L_119 = V_25;
		NullCheck(L_119);
		float L_120 = L_119->get_worldY_25();
		float L_121 = V_28;
		V_23 = ((float)((float)L_112+(float)((float)((float)((float)((float)((float)((float)((float)((float)L_113*(float)L_115))+(float)((float)((float)L_116*(float)L_118))))+(float)L_120))*(float)L_121))));
		int32_t L_122 = V_15;
		V_15 = ((int32_t)((int32_t)L_122+(int32_t)1));
		int32_t L_123 = V_21;
		V_21 = ((int32_t)((int32_t)L_123+(int32_t)3));
	}

IL_01ce:
	{
		int32_t L_124 = V_15;
		int32_t L_125 = V_24;
		if ((((int32_t)L_124) < ((int32_t)L_125)))
		{
			goto IL_0159;
		}
	}
	{
		SingleU5BU5D_t3989243465* L_126 = ___worldVertices3;
		int32_t L_127 = V_20;
		float L_128 = V_22;
		NullCheck(L_126);
		(L_126)->SetAt(static_cast<il2cpp_array_size_t>(L_127), (float)L_128);
		SingleU5BU5D_t3989243465* L_129 = ___worldVertices3;
		int32_t L_130 = V_20;
		float L_131 = V_23;
		NullCheck(L_129);
		(L_129)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_130+(int32_t)1))), (float)L_131);
		int32_t L_132 = V_20;
		int32_t L_133 = ___stride5;
		V_20 = ((int32_t)((int32_t)L_132+(int32_t)L_133));
	}

IL_01ee:
	{
		int32_t L_134 = V_20;
		int32_t L_135 = ___count2;
		if ((((int32_t)L_134) < ((int32_t)L_135)))
		{
			goto IL_0134;
		}
	}
	{
		goto IL_02ee;
	}

IL_01fb:
	{
		ExposedList_1_t3384296343 * L_136 = V_1;
		NullCheck(L_136);
		SingleU5BU5D_t3989243465* L_137 = L_136->get_Items_0();
		V_29 = L_137;
		int32_t L_138 = ___offset4;
		V_30 = L_138;
		int32_t L_139 = V_16;
		V_31 = ((int32_t)((int32_t)L_139*(int32_t)3));
		int32_t L_140 = V_16;
		V_32 = ((int32_t)((int32_t)L_140<<(int32_t)1));
		goto IL_02e6;
	}

IL_0218:
	{
		V_33 = (0.0f);
		V_34 = (0.0f);
		Int32U5BU5D_t281224829* L_141 = V_3;
		int32_t L_142 = V_15;
		int32_t L_143 = L_142;
		V_15 = ((int32_t)((int32_t)L_143+(int32_t)1));
		NullCheck(L_141);
		int32_t L_144 = L_143;
		int32_t L_145 = (L_141)->GetAt(static_cast<il2cpp_array_size_t>(L_144));
		V_35 = L_145;
		int32_t L_146 = V_35;
		int32_t L_147 = V_15;
		V_35 = ((int32_t)((int32_t)L_146+(int32_t)L_147));
		goto IL_02c6;
	}

IL_023d:
	{
		BoneU5BU5D_t600500093* L_148 = V_19;
		Int32U5BU5D_t281224829* L_149 = V_3;
		int32_t L_150 = V_15;
		NullCheck(L_149);
		int32_t L_151 = L_150;
		int32_t L_152 = (L_149)->GetAt(static_cast<il2cpp_array_size_t>(L_151));
		NullCheck(L_148);
		int32_t L_153 = L_152;
		Bone_t1763862836 * L_154 = (L_148)->GetAt(static_cast<il2cpp_array_size_t>(L_153));
		V_36 = L_154;
		SingleU5BU5D_t3989243465* L_155 = V_2;
		int32_t L_156 = V_31;
		NullCheck(L_155);
		int32_t L_157 = L_156;
		float L_158 = (L_155)->GetAt(static_cast<il2cpp_array_size_t>(L_157));
		SingleU5BU5D_t3989243465* L_159 = V_29;
		int32_t L_160 = V_32;
		NullCheck(L_159);
		int32_t L_161 = L_160;
		float L_162 = (L_159)->GetAt(static_cast<il2cpp_array_size_t>(L_161));
		V_37 = ((float)((float)L_158+(float)L_162));
		SingleU5BU5D_t3989243465* L_163 = V_2;
		int32_t L_164 = V_31;
		NullCheck(L_163);
		int32_t L_165 = ((int32_t)((int32_t)L_164+(int32_t)1));
		float L_166 = (L_163)->GetAt(static_cast<il2cpp_array_size_t>(L_165));
		SingleU5BU5D_t3989243465* L_167 = V_29;
		int32_t L_168 = V_32;
		NullCheck(L_167);
		int32_t L_169 = ((int32_t)((int32_t)L_168+(int32_t)1));
		float L_170 = (L_167)->GetAt(static_cast<il2cpp_array_size_t>(L_169));
		V_38 = ((float)((float)L_166+(float)L_170));
		SingleU5BU5D_t3989243465* L_171 = V_2;
		int32_t L_172 = V_31;
		NullCheck(L_171);
		int32_t L_173 = ((int32_t)((int32_t)L_172+(int32_t)2));
		float L_174 = (L_171)->GetAt(static_cast<il2cpp_array_size_t>(L_173));
		V_39 = L_174;
		float L_175 = V_33;
		float L_176 = V_37;
		Bone_t1763862836 * L_177 = V_36;
		NullCheck(L_177);
		float L_178 = L_177->get_a_20();
		float L_179 = V_38;
		Bone_t1763862836 * L_180 = V_36;
		NullCheck(L_180);
		float L_181 = L_180->get_b_21();
		Bone_t1763862836 * L_182 = V_36;
		NullCheck(L_182);
		float L_183 = L_182->get_worldX_22();
		float L_184 = V_39;
		V_33 = ((float)((float)L_175+(float)((float)((float)((float)((float)((float)((float)((float)((float)L_176*(float)L_178))+(float)((float)((float)L_179*(float)L_181))))+(float)L_183))*(float)L_184))));
		float L_185 = V_34;
		float L_186 = V_37;
		Bone_t1763862836 * L_187 = V_36;
		NullCheck(L_187);
		float L_188 = L_187->get_c_23();
		float L_189 = V_38;
		Bone_t1763862836 * L_190 = V_36;
		NullCheck(L_190);
		float L_191 = L_190->get_d_24();
		Bone_t1763862836 * L_192 = V_36;
		NullCheck(L_192);
		float L_193 = L_192->get_worldY_25();
		float L_194 = V_39;
		V_34 = ((float)((float)L_185+(float)((float)((float)((float)((float)((float)((float)((float)((float)L_186*(float)L_188))+(float)((float)((float)L_189*(float)L_191))))+(float)L_193))*(float)L_194))));
		int32_t L_195 = V_15;
		V_15 = ((int32_t)((int32_t)L_195+(int32_t)1));
		int32_t L_196 = V_31;
		V_31 = ((int32_t)((int32_t)L_196+(int32_t)3));
		int32_t L_197 = V_32;
		V_32 = ((int32_t)((int32_t)L_197+(int32_t)2));
	}

IL_02c6:
	{
		int32_t L_198 = V_15;
		int32_t L_199 = V_35;
		if ((((int32_t)L_198) < ((int32_t)L_199)))
		{
			goto IL_023d;
		}
	}
	{
		SingleU5BU5D_t3989243465* L_200 = ___worldVertices3;
		int32_t L_201 = V_30;
		float L_202 = V_33;
		NullCheck(L_200);
		(L_200)->SetAt(static_cast<il2cpp_array_size_t>(L_201), (float)L_202);
		SingleU5BU5D_t3989243465* L_203 = ___worldVertices3;
		int32_t L_204 = V_30;
		float L_205 = V_34;
		NullCheck(L_203);
		(L_203)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_204+(int32_t)1))), (float)L_205);
		int32_t L_206 = V_30;
		int32_t L_207 = ___stride5;
		V_30 = ((int32_t)((int32_t)L_206+(int32_t)L_207));
	}

IL_02e6:
	{
		int32_t L_208 = V_30;
		int32_t L_209 = ___count2;
		if ((((int32_t)L_208) < ((int32_t)L_209)))
		{
			goto IL_0218;
		}
	}

IL_02ee:
	{
		return;
	}
}
// System.Boolean Spine.VertexAttachment::ApplyDeform(Spine.VertexAttachment)
extern "C"  bool VertexAttachment_ApplyDeform_m2522801063 (VertexAttachment_t1776545046 * __this, VertexAttachment_t1776545046 * ___sourceAttachment0, const RuntimeMethod* method)
{
	{
		VertexAttachment_t1776545046 * L_0 = ___sourceAttachment0;
		return (bool)((((RuntimeObject*)(VertexAttachment_t1776545046 *)__this) == ((RuntimeObject*)(VertexAttachment_t1776545046 *)L_0))? 1 : 0);
	}
}
// System.Void Spine.VertexAttachment::.cctor()
extern "C"  void VertexAttachment__cctor_m3545296824 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VertexAttachment__cctor_m3545296824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((VertexAttachment_t1776545046_StaticFields*)il2cpp_codegen_static_fields_for(VertexAttachment_t1776545046_il2cpp_TypeInfo_var))->set_nextID_1(0);
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m1723520498(L_0, /*hidden argument*/NULL);
		((VertexAttachment_t1776545046_StaticFields*)il2cpp_codegen_static_fields_for(VertexAttachment_t1776545046_il2cpp_TypeInfo_var))->set_nextIdLock_2(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
