﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "object-internals.h"

// System.String
struct String_t;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T2697047229_H
#define VALUETYPE_T2697047229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t2697047229  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t2697047229_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t2697047229_marshaled_com
{
};
#endif // VALUETYPE_T2697047229_H
#ifndef ATTRIBUTE_T2169343654_H
#define ATTRIBUTE_T2169343654_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_t2169343654  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_T2169343654_H
#ifndef U24ARRAYTYPEU3D20_T288851340_H
#define U24ARRAYTYPEU3D20_T288851340_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=20
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D20_t288851340 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D20_t288851340__padding[20];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D20_T288851340_H
#ifndef U24ARRAYTYPEU3D24_T615848985_H
#define U24ARRAYTYPEU3D24_T615848985_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t615848985 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t615848985__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T615848985_H
#ifndef PROPERTYATTRIBUTE_T1226012101_H
#define PROPERTYATTRIBUTE_T1226012101_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t1226012101  : public Attribute_t2169343654
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYATTRIBUTE_T1226012101_H
#ifndef SPINEATLASREGION_T56832050_H
#define SPINEATLASREGION_T56832050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAtlasRegion
struct  SpineAtlasRegion_t56832050  : public PropertyAttribute_t1226012101
{
public:
	// System.String Spine.Unity.SpineAtlasRegion::atlasAssetField
	String_t* ___atlasAssetField_0;

public:
	inline static int32_t get_offset_of_atlasAssetField_0() { return static_cast<int32_t>(offsetof(SpineAtlasRegion_t56832050, ___atlasAssetField_0)); }
	inline String_t* get_atlasAssetField_0() const { return ___atlasAssetField_0; }
	inline String_t** get_address_of_atlasAssetField_0() { return &___atlasAssetField_0; }
	inline void set_atlasAssetField_0(String_t* value)
	{
		___atlasAssetField_0 = value;
		Il2CppCodeGenWriteBarrier((&___atlasAssetField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATLASREGION_T56832050_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1810756282_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1810756282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t1810756282  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t1810756282_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=20 <PrivateImplementationDetails>::$field-B6E6EA57C32297E83203480EE50A22C3581AA09C
	U24ArrayTypeU3D20_t288851340  ___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A
	U24ArrayTypeU3D24_t615848985  ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1;

public:
	inline static int32_t get_offset_of_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1810756282_StaticFields, ___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0)); }
	inline U24ArrayTypeU3D20_t288851340  get_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0() const { return ___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0; }
	inline U24ArrayTypeU3D20_t288851340 * get_address_of_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0() { return &___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0; }
	inline void set_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0(U24ArrayTypeU3D20_t288851340  value)
	{
		___U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t1810756282_StaticFields, ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1)); }
	inline U24ArrayTypeU3D24_t615848985  get_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() const { return ___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1; }
	inline U24ArrayTypeU3D24_t615848985 * get_address_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() { return &___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1; }
	inline void set_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1(U24ArrayTypeU3D24_t615848985  value)
	{
		___U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T1810756282_H
#ifndef SPINEATTRIBUTEBASE_T3125339574_H
#define SPINEATTRIBUTEBASE_T3125339574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineAttributeBase
struct  SpineAttributeBase_t3125339574  : public PropertyAttribute_t1226012101
{
public:
	// System.String Spine.Unity.SpineAttributeBase::dataField
	String_t* ___dataField_0;
	// System.String Spine.Unity.SpineAttributeBase::startsWith
	String_t* ___startsWith_1;
	// System.Boolean Spine.Unity.SpineAttributeBase::includeNone
	bool ___includeNone_2;
	// System.Boolean Spine.Unity.SpineAttributeBase::fallbackToTextField
	bool ___fallbackToTextField_3;

public:
	inline static int32_t get_offset_of_dataField_0() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t3125339574, ___dataField_0)); }
	inline String_t* get_dataField_0() const { return ___dataField_0; }
	inline String_t** get_address_of_dataField_0() { return &___dataField_0; }
	inline void set_dataField_0(String_t* value)
	{
		___dataField_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataField_0), value);
	}

	inline static int32_t get_offset_of_startsWith_1() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t3125339574, ___startsWith_1)); }
	inline String_t* get_startsWith_1() const { return ___startsWith_1; }
	inline String_t** get_address_of_startsWith_1() { return &___startsWith_1; }
	inline void set_startsWith_1(String_t* value)
	{
		___startsWith_1 = value;
		Il2CppCodeGenWriteBarrier((&___startsWith_1), value);
	}

	inline static int32_t get_offset_of_includeNone_2() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t3125339574, ___includeNone_2)); }
	inline bool get_includeNone_2() const { return ___includeNone_2; }
	inline bool* get_address_of_includeNone_2() { return &___includeNone_2; }
	inline void set_includeNone_2(bool value)
	{
		___includeNone_2 = value;
	}

	inline static int32_t get_offset_of_fallbackToTextField_3() { return static_cast<int32_t>(offsetof(SpineAttributeBase_t3125339574, ___fallbackToTextField_3)); }
	inline bool get_fallbackToTextField_3() const { return ___fallbackToTextField_3; }
	inline bool* get_address_of_fallbackToTextField_3() { return &___fallbackToTextField_3; }
	inline void set_fallbackToTextField_3(bool value)
	{
		___fallbackToTextField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEATTRIBUTEBASE_T3125339574_H
#ifndef SPINEBONE_T1147293090_H
#define SPINEBONE_T1147293090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Spine.Unity.SpineBone
struct  SpineBone_t1147293090  : public SpineAttributeBase_t3125339574
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINEBONE_T1147293090_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (SpineBone_t1147293090), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (SpineAtlasRegion_t56832050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2001[1] = 
{
	SpineAtlasRegion_t56832050::get_offset_of_atlasAssetField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (U3CPrivateImplementationDetailsU3E_t1810756282), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1810756282_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2002[2] = 
{
	U3CPrivateImplementationDetailsU3E_t1810756282_StaticFields::get_offset_of_U24fieldU2DB6E6EA57C32297E83203480EE50A22C3581AA09C_0(),
	U3CPrivateImplementationDetailsU3E_t1810756282_StaticFields::get_offset_of_U24fieldU2D6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (U24ArrayTypeU3D20_t288851340)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D20_t288851340 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (U24ArrayTypeU3D24_t615848985)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t615848985 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
